#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2006 David Belohrad
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street,
# Fifth Floor, Boston, MA  02110-1301, USA.
#
# You can dowload a copy of the GNU General Public License here:
# http://www.gnu.org/licenses/gpl.txt
#
# Author: David Belohrad
# Email:  david.belohrad@cern.ch
#

"""
This is UNITTEST for a VFC implementing MCOI. NOTE THAT THIS SCRIPT
HAS TO BE RUN ON FEC DIRECTLY, as it accesses real hardware. This is
not a simulation checking script, but true hardware checking
script. Another thing to note is, that FEC PYTHON IS SCRAP AND WAY TOO
OLD. At the present time 2.6.6, so we have to use unittest from 2.6.6
which differs from 2.7 version
"""

import sys
sys.path.append("libs/VFC-HD_System/sw/python")
import unittest
from Class_VfcHd_System import VfcHd_System

class MCRegisterSet(object):
    """ Class declares just bunch of constants which allow RAW-access
    to the registers of a single motor interface. This is used usually
    when registers are not fully deployed in the encore driver and we
    want to access address ranges outside of the register declared
    still by using their names
    """

    def setupRegisters(self):
        """ sets up register names. THESE NAMES HAVE TO CORRESPOND TO
        ENCOREDRIVER SINGLE CONTROLLER INTERFACE (although it is not
        always needed, it allows later on to easily jump to 'encore'
        version of the test suite)
        """

        # register names have to be in linear order, set 'dummy' where
        # address space is missing. Due to python 2.<scrapversion>
        # installed on FEC we cannot use fancy things like Enum classes
        self.regNames = [ "StepNumber",  "Direction",  "LowSpeed",
                          "HighSpeed",  "AccDeccRate",  "Trail",
                          "Config",  "Power",  "Trigger",  "Status",
                          "Slowdown",  "StepsAfterSwitch",
                          "OutputSwitches",  "PosCounter",
                          "StepsLeft",  "SteppingStatus"]
        # make enumerate out of this and convert to class attribute
        for index, regname in enumerate(self.regNames):
            setattr(self, regname, index)

    def getMotorAddress(self, link, motor):
        """ returns offset from AppMemSpace identifying start of the
        MOTOR CONTROLLER registers
        """
        # 0x2000 = basic offset, links start with alignment of 0x400,
        # each motor occupies 64registers address space
        return 0x2000 + link * 0x400 + motor * 0x20

    def getLinkRegistersAddress(self, link):
        """ returns offset from AppMemSpace identifying link
        """
        # 16th block in motor controller address space is always
        # reserved to link control
        return self.getMotorAddress(link, 16)


    def R(self, link, motor, name):
        """ returns offset address of given register for particular
        link and motor
        """
        return self.getMotorAddress(link, motor) + getattr(self, name)


class BetweenAssertMixin(object):
    def assertBetween(self, x, lo, hi):
        if not (lo <= x <= hi):
            raise AssertionError('%r not between %r and %r' % (x, lo, hi))

class MCOIFunctionality(unittest.TestCase, BetweenAssertMixin, MCRegisterSet):

    def setUp(self):
        """ opens one time the VFC MCOI class
        """
        self.setupRegisters()
        self.mcoi = VfcHd_System(
            Driver="vfc_hd_mcoi",
            Lun=3,
            Verbose=False)

    def test_appInfo(self):
        """ Checks application info string
        """
        info = self.mcoi.ReadAppInfo()
        self.assertTrue(info.startswith("VFC-MCOI-DRB"))

    def test_sysInfo(self):
        """ Checks system information string
        """
        info = self.mcoi.ReadSysInfo()
        self.assertTrue(info.startswith("VFC-HD"))

    def test_uniqueId(self):
        """ Reads unique ID and makes checks on it. As it is made by
        MAXIM, we can check the product code
        """
        SN = "%.16X" % (self.mcoi.ReadUniqueId())
        self.assertNotEqual(SN, "FFFFFFFFFFFFFFFF")
        self.assertNotEqual(SN, "0000000000000000")

    def test_voltages(self):
        """ reads voltages and tests if they give some common sense
        """
        voltages = self.mcoi.ReadAdc()
        # now check ranges
        # PRE_VADJ = 1.4 nominal
        self.assertBetween(voltages[0], 1.35, 1.45)
        # VCCPD_ADJ = 2.5
        self.assertBetween(voltages[1], 2.45, 2.6)
        # VIO_B_M2C 1.4V
        self.assertBetween(voltages[3], 1.35, 1.45)
        # 3V3 FPGA
        self.assertBetween(voltages[4], 3.29, 3.35)
        # 2.5V:
        self.assertBetween(voltages[6], 2.45, 2.6)
        # and all the other voltages are currently not important
        # and/or turned off (as VADJ or V3P3_FMC, which are currently
        # not used)


    def test_getLinks(self):
        """ verifies if links have proper link information. Each
        optical link has block of link registers, one of them contains
        bits identifying the link number. By reading those we assure
        that this is the firmware we're looking for. Now we do not
        want to use here names of registers, but rather 'generic'
        memory access as this will assure that those registers are at
        correct absolute address spaces. The registers-accesses are
        checked by different test_appInfo
        """
        # as this is counted in number of long words, the offset,
        # which is normally 0x8000 must be divided by 4 (so not VME
        # address, but register number). Setting offset to 2000 we
        # target the 0x8000 _from the application space_, which is at
        # 0x800000, hence addressing the blocks starting at
        # 0x808000. And this is the place where first link of the
        # first motor controller appears. Now we can iterate through
        # the registers to get links. Link is identified by reading
        # link status register and _ADDING 10_ to the link
        # number. Hence first link will read 0x0a, second 0x0b, third
        # 0x0c, fourth 0x0d. If not, that's fail
        for link in range(4):
            offset = self.getLinkRegistersAddress(link)
            self.assertEqual(
                self.mcoi.Read("AppMemSpace", Offset=offset),
                link + 10)

    def Read(self, link, motor, reg):
        """ Reads motor control register specified by link, motor and
        register name, returns value
        """
        return self.mcoi.Read("AppMemSpace",
                              Offset=self.R(link,
                                            motor,
                                            reg))

    def Write(self, link, motor, reg, data):
        """ Write using direct register name
        """
        self.mcoi.Write("AppMemSpace",
                        data,
                        Offset=self.R(link, motor, reg))

    def test_motorsIdentification(self):
        """ verifies identification number of motor for each link.
        """
        #self.mcoi.SetVerbose(True)
        for link in range(4):
            for motor in range(16):
                # offset is just pointing to given motor control, but
                # we have to read specific register out of this block
                # which is ... 15th
                stats = self.Read(link, motor, "SteppingStatus")
                # extract from status the motor identifier, which
                # starts at zero and has to finish at 0x0f
                mcinfo = (stats & 0xf000) >> 12
                self.assertEqual(motor, mcinfo)

    def test_irqlevels(self):
        """ IRQ levels should be set to 3 by default (transfer.ref)
        """
        self.assertEqual(self.mcoi.Read("Sys_IntLevel"),
                         0x03)

    def test_irqvector(self):
        """ irq vector can be anywhere between 0x80 to 0x80+21
        """
        self.assertBetween(self.mcoi.Read("Sys_IntVector"),
                           0x80,
                           0x80+21)

    def test_queing(self):
        """ adds commands into the queue and clears them out
        """
        # queue should be empty
        noqueue = self.mcoi.Read("AppMemSpace",
                             Offset=self.R(0, 0, "Status"))
        if noqueue:
            # clear queue:
            self.Write(0, 0, "Trigger", 0x10)
        noqueue = self.mcoi.Read("AppMemSpace",
                             Offset=self.R(0, 0, "Status"))
        self.assertEqual(noqueue, 0)
        # now write down something:
        self.Write(0, 0, "StepNumber", 10)
        self.assertEqual(
            self.Read(0, 0, "StepNumber"), 10)
        # queue command
        self.Write(0, 0, "Trigger", 8)
        noqueue = self.mcoi.Read("AppMemSpace",
                             Offset=self.R(0, 0, "Status"))
        # queue first
        self.assertEqual(noqueue, 1)


if __name__ == '__main__':
    unittest.main()
