#!/usr/bin/env tclsh

#==============================================================================#
#                                                                              #
#  VFC-HD pre-flow TCL script                                                  #
#                                                                              #
#  Author: Tom Levens <tom.levens@cern.ch>                                     #
#                                                                              #
#------------------------------------------------------------------------------#
#                                                                              #
#  Set the application version and release date in the QSF.                    #
#                                                                              #
#  NOTE: include in the QSF with:                                              #
#                                                                              #
#   set_global_assignment -name PRE_FLOW_SCRIPT_FILE "quartus_sh:pre.tcl"      #
#                                                                              #
#==============================================================================#

# Application instance name
set inst_application "VfcHdApplication:i_VfcHdApplication"

# Property names
set prop_version     "g_ApplicationVersion_b8"
set prop_day         "g_ApplicationReleaseDay_b8"
set prop_month       "g_ApplicationReleaseMonth_b8"
set prop_year        "g_ApplicationReleaseYear_b8"

#------------------------------------------------------------------------------#

set module        [lindex $quartus(args) 0]
set project_name  [lindex $quartus(args) 1]
set revision_name [lindex $quartus(args) 2]

# Open project
project_open $project_name -revision $revision_name

post_message "pre_flow.tcl: opened project $project_name, revision $revision_name"

# Read build number from QSF
set build [format %02x [expr [get_parameter -name $prop_version -to $inst_application] + 0]]

# Get current date
set day   [clock format [clock seconds] -format {%d}]
set month [clock format [clock seconds] -format {%m}]
set year  [clock format [clock seconds] -format {%y}]

# Get last build date
set prev_day   [format %02x [expr [get_parameter -name $prop_day   -to $inst_application] + 0]]
set prev_month [format %02x [expr [get_parameter -name $prop_month -to $inst_application] + 0]]
set prev_year  [format %02x [expr [get_parameter -name $prop_year  -to $inst_application] + 0]]

if {$day > $prev_day || $month > $prev_month || $year > $prev_year} {
    set build 00
}

post_message "pre_flow.tcl: build number is $build"

post_message "pre_flow.tcl: application release id is 0x$year$month$day$build"

# Set parameters to QSF
set_parameter -name $prop_version [expr 0x$build] -to $inst_application
set_parameter -name $prop_day     [expr 0x$day]   -to $inst_application
set_parameter -name $prop_month   [expr 0x$month] -to $inst_application
set_parameter -name $prop_year    [expr 0x$year]  -to $inst_application
# last thing is to setup jtag revision number so this one is visible in programmer. This is to avoid
# mismatches between versions
set_global_assignment -name STRATIX_JTAG_USER_CODE "0x$year$month$day$build"
# this has to be there otherwise usercode is not available
set_global_assignment -name USE_CHECKSUM_AS_USERCODE OFF

# Close project
project_close
