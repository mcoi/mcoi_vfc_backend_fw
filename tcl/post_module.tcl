#!/usr/bin/env tclsh

#==============================================================================#
#                                                                              #
#  VFC-HD post-module TCL script                                               #
#                                                                              #
#  Author: Tom Levens <tom.levens@cern.ch>                                     #
#                                                                              #
#------------------------------------------------------------------------------#
#                                                                              #
#  Automatically create the JIC/RPD programming files and increment the        #
#  application version number.                                                 #
#                                                                              #
#  06/20/17: Implemented GIT commit as a part of the compilation process. This #
# is needed in order to identify exactly the version running in the VFC against#
# the source code. After each successfull compilation, before the commit       #
# number is increased, the 'git commit' command is called and the commit text  #
# will show up the build number. This makes it easy to find using git log      #
# command exactly the version of the firmware. Drawback is, that _each_        #
# compilation makes a single commit even if no changes in the design are       #
# involved, this however is considered as a little issue compared to making the#
# design uniquely identifiable.                                                #
#                                                                              #
#  NOTE: include in the QSF with:                                              #
#                                                                              #
#   set_global_assignment -name POST_MODULE_SCRIPT_FILE "quartus_sh:post.tcl"  #
#                                                                              #
#==============================================================================#

# Application instance name
set inst_application "VfcHdApplication:i_VfcHdApplication"

# Property names
set prop_version     "g_ApplicationVersion_b8"
set prop_day         "g_ApplicationReleaseDay_b8"
set prop_month       "g_ApplicationReleaseMonth_b8"
set prop_year        "g_ApplicationReleaseYear_b8"

#------------------------------------------------------------------------------#

set module        [lindex $quartus(args) 0]
set project_name  [lindex $quartus(args) 1]
set revision_name [lindex $quartus(args) 2]

if {[string match "quartus_asm" $module]} {
    # Open project
    project_open $project_name -revision $revision_name

    post_message "post_module.tcl: opened project $project_name, revision $revision_name"

    # Convert programming file
    qexec "quartus_cpf -c initial_setup.cof"
    qexec "quartus_cpf -c mcoi.cof"
    qexec "quartus_cpf -c golden_only.cof"

    # get actual build number
    set build      [format %02x [expr [get_parameter -name $prop_version -to $inst_application] + 0]]
    set      day   [format %02x [expr [get_parameter -name $prop_day   -to $inst_application] + 0]]
    set      month [format %02x [expr [get_parameter -name $prop_month -to $inst_application] + 0]]
    set      year  [format %02x [expr [get_parameter -name $prop_year  -to $inst_application] + 0]]
    # setting LC_CTYPE otherwise GIT fails
    set env(LC_CTYPE) C
    # call commit with name, if failing, don't care as we might be under 'windows'
    if { [catch { exec git commit -a "-m\"Build: 0x$year$month$day$build\"" } msg] } {
	post_message "post_module.tcl: Something seems to have gone wrong:"
	post_message -type error "post_module.tcl: Information about the error: $::errorInfo"
    }

    # Increment build number in QSF
    set build [expr [get_parameter -name $prop_version -to $inst_application] + 1]

    post_message "post_module.tcl: next build number will be $build"

    set_parameter -name $prop_version $build -to $inst_application

    # Close project
    project_close
}
