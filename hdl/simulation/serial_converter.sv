//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) May 2018 CERN

//-----------------------------------------------------------------------------
// @file SERIAL_CONVERTER.SV
// @brief Converts serial data from GBT stub to parallel
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 14 May 2018
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module serial_converter
  (input ckrs_t ClkRs_ix,
   output logic        AppSfpRx1_ob,
   input logic 	       AppSfpTx1_ib,
   input logic 	       RxIsData_i,
   output logic        TxIsData_o,
   output logic [1:0]  ScEc_ob2,
   output logic [79:0] GBT_ob80,
   input logic [1:0]   ScEc_ib2,
   input logic [79:0]  GBT_ib80
   );

   // instantiates rx and tx for SINGLE channel. (anyways we'll
   // connect only one gefe)
   stransport_tx #(.g_numBits (83)) i_stransport_tx_1
     (.sdata_o				(AppSfpRx1_ob),
      .clk_ik				(ClkRs_ix.clk),
      .data_i				({RxIsData_i, ScEc_ib2, GBT_ib80}));

   stransport_rx #(.g_numBits (83)) i_stransport_rx
     (.data_o				({TxIsData_o, ScEc_ob2, GBT_ob80}),
      .clk_ik				(ClkRs_ix.clk),
      .sdata_i				(AppSfpTx1_ib));

endmodule // serial_converter
