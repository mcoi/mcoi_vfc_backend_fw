//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) May 2018 CERN

//-----------------------------------------------------------------------------
// @file STRANSPORT.SV
// @brief takes parallel data and transports them into serial
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 11 May 2018
// @details This is purely used for simulation of GBT stub: it takes
// input parallel data, and sends them such, that every 100ps one bit
// is sent, the rest is filled with 'x'. Such stream is casted away
// and restored into parallel bus on the other side
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module stransport_tx
  #(
    parameter   g_numBits  = 80
    )
  (input logic clk_ik,
   input logic [g_numBits-1:0] 	data_i,
   output logic 		sdata_o
   );

   timeunit 1ps;
   timeprecision 1ps;

   logic [g_numBits-1:0] 	latched;
   integer 			t;



   // we have to check for clocks otherwise this thing will break
   initial begin
      @(posedge clk_ik);
      t = $time;
      @(posedge clk_ik);
      // division by 100 to get in nanoseconds. $time returns value in
      // timeunit, hence returning 2000 means 2000*10ps = 20ns
      $display("stransport: clock period is %d", $time-t);
      $display("stransport: %d bits take %dps using 100ps clock",
	       g_numBits,
	       (g_numBits+2)*100);

      // we need roughly 4ns to transport coded data:
      assert ((g_numBits+2)*100 < ($time - t)) else
	$error("Clocks fed to stransport are too fast - cannot pass\
 data through");
   end

   ckrs_t ClkRs_ix;
   assign ClkRs_ix.reset = '0;
   // create artificial clock used to clock receiving side
   initial begin
      forever begin
	 ClkRs_ix.clk = 1;
	 #50ps;
	 ClkRs_ix.clk = 0;
	 #50ps;
      end
   end

   logic rising;

   get_edge
     i_get_edge
       (
	// Outputs
	.rising_o				(rising),
	.falling_o			(),
	.data_o				(),
	// Inputs
	.ClkRs_ix				(ClkRs_ix),
	.data_i				(clk_ik));

   logic [$clog2(g_numBits):0] counter;

   always_ff @(posedge ClkRs_ix.clk)
     if (rising) begin
	latched <= data_i;
	counter <= ($bits(counter))'(g_numBits);
     end else if (|counter) begin
	latched <= {1'bx, latched[g_numBits-1:1]};
	counter <= counter - ($bits(counter))'(1);
     end;

   assign sdata_o = latched[0];
endmodule // stransport_tx
