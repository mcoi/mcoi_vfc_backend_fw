//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) November 2017 CERN

//-----------------------------------------------------------------------------
// @file VFCHDGBTCOREX4_STUB.SV
// @brief Stub for GBTx module
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 06 November 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

// Stub for GBTx module
module VfcHdGbtCoreX4 #(
			parameter g_Synthesis           = (1'b1),
			g_GbtBankId           = (1'b0),
			g_TxLatOp             = (1'b0), // Comment: '0' -> Standard Latency   | '1' -> Latency Optimized
			g_RxLatOp             = (1'b0), // Comment: '0' -> Standard Latency   | '1' -> Latency Optimized
			g_TxWideBus           = (1'b0), // Comment: '0' -> GBT Frame Encoding | '1' -> WideBus Encoding
			g_RxWideBus           = (1'b0), // Comment: '0' -> GBT Frame Encoding | '1' -> WideBus Encoding
			g_TxElinksDataAlignEn = ({1'b0, 1'b0, 1'b0, 1'b0}),
			g_RxElinksDataAlignEn = ({1'b0, 1'b0, 1'b0, 1'b0}),
			g_TxRxMatchFlagsEn    = (1'b1))
   (
   // Outputs
   Dat_ob32, Ack_o, SfpTxDisable_oqb4, SfpRateSelect_oqb4, RxFrameClk40Mhz_okb4,
   MgtTx_ob4, RxDataScIc_o4b2, RxDataScEc_o4b2, RxData_o4b80, RxDataWb_o4b32,
   GbtTxReady_ob4, GbtRxReady_ob4, RxIsDataFlag_ob4, TxMatchFlag_o,
   RxMatchFlag_ob4,
   // Inouts
   TxFrameClk40Mhz_ik,
   // Inputs
   Clk_ik, Rst_ir, Cyc_i, Stb_i, We_i, Adr_ib7, Dat_ib32, SfpPresent_ib4,
   SfpTxFault_ib4, SfpLos_ib4, StatusSfpTxDisable_ib4, StatusSfpRateSelect_ib4,
   GbtRefClk120MHz_ik, GbtReset_ir, TxGbtReset_irb4, RxGbtReset_irb4, MgtRx_ib4,
   TxDataScIc_i4b2, TxDataScEc_i4b2, TxData_i4b80, TxDataWb_i4b32,
   TxIsDataSel_ib4
   );
   timeunit 10ps;
   timeprecision 1ps;



  output logic [31:0]	Dat_ob32;
  output logic		Ack_o;
  output logic [4:1]	SfpTxDisable_oqb4;
  output logic [4:1]	SfpRateSelect_oqb4;
  output logic [4:1]	RxFrameClk40Mhz_okb4;
  output logic [4:1]	MgtTx_ob4;
  output logic [1:0]	RxDataScIc_o4b2 [4:1];
  output logic [1:0]	RxDataScEc_o4b2 [4:1];
  output logic [79:0]	RxData_o4b80 [4:1];
  output logic [31:0]	RxDataWb_o4b32 [4:1];
  output logic [4:1]	GbtTxReady_ob4;
  output logic [4:1]	GbtRxReady_ob4;
  output logic [4:1]	RxIsDataFlag_ob4;
  output logic		TxMatchFlag_o;
  output logic [4:1]	RxMatchFlag_ob4;
  inout logic		TxFrameClk40Mhz_ik;
  input logic		Clk_ik;
  input logic		Rst_ir;
  input logic		Cyc_i;
  input logic		Stb_i;
  input logic		We_i;
  input logic [6:0]	Adr_ib7;
  input logic [31:0]	Dat_ib32;
  input logic [4:1]	SfpPresent_ib4;
  input logic [4:1]	SfpTxFault_ib4;
  input logic [4:1]	SfpLos_ib4;
  input logic [4:1]	StatusSfpTxDisable_ib4;
  input logic [4:1]	StatusSfpRateSelect_ib4;
  input logic		GbtRefClk120MHz_ik;
  input logic		GbtReset_ir;
  input logic [4:1]	TxGbtReset_irb4;
  input logic [4:1]	RxGbtReset_irb4;
  input logic [4:1]	MgtRx_ib4;
  input logic [1:0]	TxDataScIc_i4b2 [4:1];
  input logic [1:0]	TxDataScEc_i4b2 [4:1];
  input logic [79:0]	TxData_i4b80 [4:1];
  input logic [31:0]	TxDataWb_i4b32 [4:1];
  input logic [4:1]	TxIsDataSel_ib4;


   // clock pass through!
   assign RxFrameClk40Mhz_okb4 = {4{TxFrameClk40Mhz_ik}};
   assign SfpTxDisable_oqb4 = StatusSfpTxDisable_ib4;
   assign SfpRateSelect_oqb4 = StatusSfpRateSelect_ib4;
   assign GbtRxReady_ob4 = '1;


   // make loopback device on Sc such that serial_register can lock in
   // and works in pass-through mode. GEFE will send the data over
   // this link!
   genvar 		f;

   generate
      for (f = 1; f < 5; f++) begin
	 // widebus not used
	 assign RxDataWb_o4b32[f] = '0;

	 stransport_tx
	   #(
	     // Parameters
	     .g_numBits			(83))
	 i_stransport_tx
	   (
	    // Outputs
	    .sdata_o			(MgtTx_ob4[f]),
	    // Inputs
	    .clk_ik			(TxFrameClk40Mhz_ik),
	    .data_i			({TxIsDataSel_ib4[f], TxDataScEc_i4b2[f], TxData_i4b80[f]}));

	 stransport_rx
	   #(
	     // Parameters
	     .g_numBits			(83))
	 i_stransport_rx
	   (
	    // Outputs
	    .data_o			({RxIsDataFlag_ob4[f], RxDataScEc_o4b2[f], RxData_o4b80[f]}),
	    // Inputs
	    .clk_ik			(TxFrameClk40Mhz_ik),
	    .sdata_i			(MgtRx_ib4[f]));

      end
   endgenerate

endmodule // VfcHdGbtCoreX4
