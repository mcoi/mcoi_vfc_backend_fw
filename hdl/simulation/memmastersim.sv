//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file MEMMASTERSIM.SV
// @brief Instatiate this to act as master in simulation over mem_x interfaces
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 30 October 2017
// @details
// Implements read/write/reset on the memory interface
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;

module memmastersim
  #(
    // define clock speed in nanoseconds
    parameter   g_clockspeed  = 25
    )
  (output ckrs_t ClkRs_ox,
   mem_x memBus
   );

   timeunit 1ns;
   timeprecision 100ps;

   initial begin
      ClkRs_ox.reset = 0;
      ClkRs_ox.clk = 0;
   end

   // generate clock
   always forever #(g_clockspeed/2 * 1ns) ClkRs_ox.clk <= ~ClkRs_ox.clk;
   default clocking cb @(posedge ClkRs_ox.clk); endclocking

   task waitfor(input int cycles);
      ##(cycles);
   endtask // waitfor


   task write;
      input [31:0] Address_ib32;
      input [31:0] Data_ib32;
      begin
	 @(posedge ClkRs_ox.clk);
	 #1;
	 memBus.write = 1;
	 memBus.addrbus = Address_ib32;
	 memBus.datain = Data_ib32;
	 @(posedge ClkRs_ox.clk);
	 #1
	 memBus.write = 0;
	 @(posedge ClkRs_ox.clk);
      end
   endtask // WbWrite

   task writeConsecutive;
      input [31:0] Address_ib32;
      input [31:0] Data_ib32;
      begin
	 @(posedge ClkRs_ox.clk);
	 #1;
	 memBus.write = 1;
	 memBus.addrbus = Address_ib32;
	 memBus.datain = Data_ib32;
      end
   endtask // WbWrite

   task reset;
      memBus.datain = '0;
      memBus.addrbus = '0;
      memBus.write = '0;
      ClkRs_ox.reset = 1;
      ##10;
      ClkRs_ox.reset = 0;
      ##10;
      @(posedge ClkRs_ox.clk);

   endtask // Reset

   task read;
      input [31:0] Address_ib32;
      output [31:0] Data_ob32;
      begin
	 ##1;
	 #1;
	 memBus.write = 0;
	 memBus.addrbus = Address_ib32;
	 memBus.datain = 'X;
	 @(posedge ClkRs_ox.clk);
	 #1;
	 Data_ob32 = memBus.dataout;
	 // we always come out of this at the time of rising edge
	 @(posedge ClkRs_ox.clk);
end
endtask


endmodule // memmastersim
