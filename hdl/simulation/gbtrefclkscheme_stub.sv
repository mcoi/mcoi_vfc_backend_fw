//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) November 2017 CERN

//-----------------------------------------------------------------------------
// @file GBTREFCLKSCHEME_STUB.SV
// @brief GBT clocking as stub - we cannot simulate this
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 06 November 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

// GBT clocking as stub - we cannot simulate this
module GbtRefClkScheme
  #(  parameter g_BstRefClkSchemeEn = (1'b0))
   (/*AUTOARG*/
    // Outputs
    Dat_ob32, Ack_o, BstSfpTxDisable_o, BstSfpRateSelect_o,
    LockedFreeRunningPll_oa, LockedGbtTxFrameClkPll_oa,
    FreeRunningClk160MHzFb_ok, BstClkFb_ok, BstOrFreeRunningMuxClk_ok,
    TxFrameClk40Mhz_ok, GbtReset_or,
    // Inouts
    PllRefSda_ioz, PllRefScl_iokz,
    // Inputs
    Clk_ik, Rst_ir, Cyc_i, Stb_i, We_i, Adr_ib2, Dat_ib32, BstSfpPresent_i,
    BstSfpTxFault_i, BstSfpLos_i, StatusBstSfpTxDisable_i,
    StatusBstSfpRateSelect_i, BstOn_i, BunchClkFlag_i, SelBstClk_i,
    SyncTxFrameClkPll_i, FreeRunningClk20MHz_ik, FreeRunningClk160MHzFb_ik,
    BstClk_ik, BstClkFb_ik, GbtRefClk120MHz_ik
    );

   timeunit 1ns;
   timeprecision 1ps;

   /*AUTOINOUTMODULE("GbtRefClkScheme")*/
   // Beginning of automatic in/out/inouts (from specific module)
   output logic [31:0]	Dat_ob32;
   output logic		Ack_o;
   output logic		BstSfpTxDisable_o;
   output logic		BstSfpRateSelect_o;
   output logic		LockedFreeRunningPll_oa;
   output logic		LockedGbtTxFrameClkPll_oa;
   output logic		FreeRunningClk160MHzFb_ok;
   output logic		BstClkFb_ok;
   output logic		BstOrFreeRunningMuxClk_ok;
   output logic		TxFrameClk40Mhz_ok;
   output logic		GbtReset_or;
   inout logic		PllRefSda_ioz;
   inout logic		PllRefScl_iokz;
   input logic		Clk_ik;
   input logic		Rst_ir;
   input logic		Cyc_i;
   input logic		Stb_i;
   input logic		We_i;
   input logic [1:0] 	Adr_ib2;
   input logic [31:0] 	Dat_ib32;
   input logic		BstSfpPresent_i;
   input logic		BstSfpTxFault_i;
   input logic		BstSfpLos_i;
   input logic		StatusBstSfpTxDisable_i;
   input logic		StatusBstSfpRateSelect_i;
   input logic		BstOn_i;
   input logic		BunchClkFlag_i;
   input logic		SelBstClk_i;
   input logic		SyncTxFrameClkPll_i;
   input logic		FreeRunningClk20MHz_ik;
   input logic		FreeRunningClk160MHzFb_ik;
   input logic		BstClk_ik;
   input logic		BstClkFb_ik;
   input logic		GbtRefClk120MHz_ik;
   // End of automatics

   initial begin
      LockedGbtTxFrameClkPll_oa = 0;
      LockedFreeRunningPll_oa = 0;
      #1us;
      LockedFreeRunningPll_oa = 1;
      #1.2us;
      LockedGbtTxFrameClkPll_oa = 1;

   end


   initial begin
      forever begin
	 TxFrameClk40Mhz_ok <= 1;
	 #12ns;
	 TxFrameClk40Mhz_ok <= 0;
	 #12ns;
      end
   end

endmodule // GbtRefClkScheme

// Local Variables:
// verilog-library-files:("/home/belohrad/git/mcoi_vfc_backend_fw/libs/BI_HDL_Cores/cores_for_synthesis/bi_gbt_fpga/gbt_fpga_vfc/vfc_hd_gbt_refclk/GbtRefClkScheme.sv")
// End:
