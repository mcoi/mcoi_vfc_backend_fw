//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) May 2018 CERN

//-----------------------------------------------------------------------------
// @file GBT_PLL.SV
// @brief Implements fake PLL for GBT links
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 23 May 2018
// @details
// Takes 100Mhz input, generates 120MHz and 40MHz GBT clocks
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module gbt_pll

  (
   // output clock is 120MHz for GBT MGMT
   output bit 	outclk_0,
   // output clock is 40MHz for txframe
   output bit 	outclk_1,
   // output clock is 20MHz for 'other':
   output bit 	outclk_2,
   output logic locked,
   // reference clock is 100MHz from Si57x
   input logic 	refclk,
   input logic 	rst
   );

   timeunit 1ns;
   timeprecision 10ps;

   bit 		clk;
   bit [2:0] cnt;


   always forever #4.16666ns outclk_0 <= ~outclk_0;

   always_ff @(posedge refclk)
       cnt <= cnt + 2'd1;

   assign outclk_1 = cnt[1];
   always_ff @(posedge cnt[1])
     outclk_2 = ~outclk_2;

   assign locked = ~rst;


endmodule // gbt_pll
