`timescale 1 ns/100 ps

 module ds18b20_dynpll(
       POWERDOWN,
       CLKA,
       LOCK,
       GLA,
       SDIN,
       SCLK,
       SSHIFT,
       SUPDATE,
       MODE,
       SDOUT
    );
input  POWERDOWN;
input  CLKA;
output LOCK;
output GLA;
input  SDIN;
input  SCLK;
input  SSHIFT;
input  SUPDATE;
input  MODE;
output SDOUT;

endmodule
