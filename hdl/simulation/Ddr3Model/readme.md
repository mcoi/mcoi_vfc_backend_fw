# DDR3 Memory Simulation Model

The VFC-HD board was designed to be mounted with two 8 Gb modules (Micron [MT41K512M16HA-125:E](https://www.micron.com/parts/dram/ddr3-sdram/mt41k512m16ha-125), 64 Meg x 16 x 8 banks, [datasheet](https://www.micron.com/~/media/documents/products/data-sheet/dram/ddr3/8gb_ddr3l.pdf)), but some first cards were produced with 4 Gb modules (Micron MT41K256M16HA-125:E, 32 Meg x 16 x 8 banks, [datasheet](https://www.micron.com/~/media/documents/products/data-sheet/dram/ddr3/4gb_1_35v_ddr3l_addendum.pdf)).

She simulation model placed in this directory can be used for both cases and should be synthesized (in Modelsim) via these commands:

For 8 Gb version:

`vlog -sv +define+den8192Mb +define+sg125 +define+x16 ddr3.v`

For 4 Gb version:

`vlog -sv +define+den4096Mb +define+sg125 +define+x16 ddr3.v`