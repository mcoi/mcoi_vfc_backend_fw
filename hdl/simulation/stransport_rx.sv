//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) May 2018 CERN

//-----------------------------------------------------------------------------
// @file STRANSPORT.SV
// @brief takes parallel data and transports them into serial
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 11 May 2018
// @details This is purely used for simulation of GBT stub: it takes
// input parallel data, and sends them such, that every 100ps one bit
// is sent, the rest is filled with 'x'. Such stream is casted away
// and restored into parallel bus on the other side
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module stransport_rx
  #(
    parameter   g_numBits  = 80
    )
  (input logic clk_ik,
   input logic 			sdata_i,
   output logic [g_numBits-1:0] data_o
   );

   timeunit 1ps;
   timeprecision 1ps;

   logic [g_numBits-1:0] 	latched;

   ckrs_t ClkRs_ix;
   assign ClkRs_ix.reset = '0;
   // create artificial clock used to clock receiving side
   initial begin
      forever begin
	 ClkRs_ix.clk = 1;
	 #50ps;
	 ClkRs_ix.clk = 0;
	 #50ps;
      end
   end
   logic rising;

   get_edge
     i_get_edge
       (
	// Outputs
	.rising_o				(rising),
	.falling_o			(),
	.data_o				(),
	// Inputs
	.ClkRs_ix				(ClkRs_ix),
	.data_i				(clk_ik));

   logic [$clog2(g_numBits):0] counter;

   always_ff @(posedge ClkRs_ix.clk)
     if (rising) begin
	counter <= ($bits(counter))'(g_numBits);
     end else if (|counter) begin
	latched <= {sdata_i, latched[g_numBits-1:1]};
	counter <= counter - ($bits(counter))'(1);
     end else if (~(|counter))
       data_o <= latched;

endmodule // stransport_tx
