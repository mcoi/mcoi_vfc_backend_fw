// This package describes constants, which are declared for
// _SIMULATION ONLY_. This is because during the simulation we want to
// setup the system differently such, that simulator is able to
// simulate effectively the features. This file must be linked to the
// design ONLY WHEN USING MODELSIM. During the actual synthesis
// _ANOTHER FILE_ (of the same name) must be included into the design
package constants;

   // definition of led blinker in app - how fast and with what period
   // the blinking of the led_blinker triggers
   parameter LED_BLINKER_PERIOD = 64;
   parameter LED_BLINKER_ON_TIME = 5;
   parameter LED_BLINKER_OFF_TIME = 8;
   // gefe blinker simulation params
   parameter GEFE_LED_BLINKER_PERIOD = 64;
   parameter GEFE_LED_BLINKER_ON_TIME = 5;
   parameter GEFE_LED_BLINKER_OFF_TIME = 8;
   // definition of IRQ mco runner - how fast led diode blinks when
   // IRQ comes
   parameter LED_IRQ_MKO_BITS = 5;
   parameter LED_IRQ_MKO_VALUE = 18;
   // parameters defining glitch catcher blinking, let's define some
   // arbitrary values which permit efficient simulation
   parameter LED_GLICH_CATCHER_BITWIDTH = 7;
   parameter LED_GLICH_CATCHER_VALUE = 84;

   // stepper pulse width - we do here 32 pulses. this is at 40MHz
   // way too slow to drive the motor, but OK for simulation
   parameter STEPPER_PULSE_WIDTH = 1<<5;

   // following parameter is used in debounce module and identifies
   // how many clock cycles is needed to debounce. As this is
   // constant through the project, the constant is here. For
   // debugging purposes it is intentionally low.
   parameter g_DebouncingCounterWidth = 4;


endpackage // constants
