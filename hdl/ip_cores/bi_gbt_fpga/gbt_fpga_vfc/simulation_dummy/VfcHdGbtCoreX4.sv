// simulation dummy

`timescale 1ns/100ps 

module VfcHdGbtCoreX4  
//====================================  Global Parameters  ===================================\\   
#(  parameter g_Synthesis           = (1'b1), 
              g_GbtBankId           = (1'b0),
              g_TxLatOp             = (1'b0), // Comment: '0' -> Standard Latency   | '1' -> Latency Optimized
              g_RxLatOp             = (1'b0), // Comment: '0' -> Standard Latency   | '1' -> Latency Optimized
              g_TxWideBus           = (1'b0), // Comment: '0' -> GBT Frame Encoding | '1' -> WideBus Encoding
              g_RxWideBus           = (1'b0), // Comment: '0' -> GBT Frame Encoding | '1' -> WideBus Encoding
              g_TxElinksDataAlignEn = ({1'b0, 1'b0, 1'b0, 1'b0}),
              g_RxElinksDataAlignEn = ({1'b0, 1'b0, 1'b0, 1'b0}),
              g_TxRxMatchFlagsEn    = (1'b1))
//========================================  I/O ports  =======================================\\ 
(
    //==== WishBone Interface ====\\
    
    input         Clk_ik,   
    input         Rst_ir,
    input         Cyc_i,    
    input         Stb_i,    
    input         We_i,     
    input  [ 6:0] Adr_ib7,  
    input  [31:0] Dat_ib32, 
    output [31:0] Dat_ob32,
    output        Ack_o,

    //==== SFPs Control ====\\
    
    input  [ 4:1] SfpPresent_ib4,         
    input  [ 4:1] SfpTxFault_ib4,         
    input  [ 4:1] SfpLos_ib4,             
    output logic [ 4:1] SfpTxDisable_oqb4 = 4'h0,      
    output logic [ 4:1] SfpRateSelect_oqb4 = 4'hf,     
    input  [ 4:1] StatusSfpTxDisable_ib4, 
    input  [ 4:1] StatusSfpRateSelect_ib4,     
    
    //==== Clocks Scheme ====\\
    
    // GBT Bank Reference Clock (PllRef (Si5338)):
    input         GbtRefClk120MHz_ik,
    // User Clocks:
    inout         TxFrameClk40Mhz_ik,
    output logic [ 4:1] RxFrameClk40Mhz_okb4 = 0,
    
    //==== GBT Resets Scheme ====\\
   
    input         GbtReset_ir,
    input  [ 4:1] TxGbtReset_irb4,
    input  [ 4:1] RxGbtReset_irb4,
   
    //==== Serial Lanes ====\\
   
    output logic [ 4:1] MgtTx_ob4 = 0,
    input  [ 4:1] MgtRx_ib4,
   
    //==== Parallel Data ====\\
   
    input  [ 1:0] TxDataScIc_i4b2 [4:1],     
    input  [ 1:0] TxDataScEc_i4b2 [4:1],     
    input  [79:0] TxData_i4b80    [4:1],     
    input  [31:0] TxDataWb_i4b32  [4:1],
    //--    
    output logic [ 1:0] RxDataScIc_o4b2 [4:1] = '{4{2'(0)}},
    output logic [ 1:0] RxDataScEc_o4b2 [4:1] = '{4{2'(0)}},
    output logic [79:0] RxData_o4b80    [4:1] = '{4{2'(0)}},
    output logic [31:0] RxDataWb_o4b32  [4:1] = '{4{2'(0)}},
    
    //==== Control ====\\    
    
    output logic [ 4:1] GbtTxReady_ob4 = 0,
    output logic [ 4:1] GbtRxReady_ob4 = 0,
    input  [ 4:1] TxIsDataSel_ib4,
    output logic [ 4:1] RxIsDataFlag_ob4 = 0,

    //==== Latency Test Flags ====\\ 
    
    output logic        TxMatchFlag_o = 0,
    output logic [ 4:1] RxMatchFlag_ob4 = 0
   
);

    t_WbInterface #(.g_DataWidth(32), .g_AddressWidth(7)) Wb_t(Clk_ik, Rst_ir);

    assign Wb_t.Cyc = Cyc_i;
    assign Wb_t.Stb = Stb_i;
    assign Wb_t.Adr_b = Adr_ib7;
    assign Wb_t.Sel_b = (4)'(-1);
    assign Wb_t.We = We_i;
    assign Wb_t.DatMoSi_b = Dat_ib32;
    assign Ack_o = Wb_t.Ack;
    assign Dat_ob32 = Wb_t.DatMiSo_b;

    WbDummy c_WbDummy (
        .WbSlave_iot(Wb_t)
    );

endmodule