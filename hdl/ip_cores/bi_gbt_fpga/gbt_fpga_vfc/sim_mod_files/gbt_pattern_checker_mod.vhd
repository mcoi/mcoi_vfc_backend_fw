--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               CERN (PH-ESE-BE)                                                         
-- Engineer:              Manoel Barros Marin (manoel.barros.marin@cern.ch) (m.barros.marin@ieee.org)
--                        (Original design by F. Marin (CPPM))   
--                                                                                                 
-- Project Name:          GBT-FPGA                                                                
-- Module Name:           GBT pattern checker                                    
--                                                                                                 
-- Language:              VHDL'93                                                                  
--                                                                                                   
-- Target Device:         Device agnostic                                                     
-- Tool version:                                                                   
--                                                                                                   
-- Version:               3.5                                                                      
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--
--                        10/10/2008   0.1       F. Marin          First .vhd entity definition           
--
--                        07/07/2009   0.2       S. Baron          Cosmetic modifications
--
--                        17/06/2013   1.0       M. Barros Marin   - Only error checking when GBT RX ready                                                                    
--                                                                 - Added pattern selector multiplexor
--                                                                 - Added static pattern    
--                                                                 - Cosmetic modifications
--
--                        06/08/2013   1.1       M. Barros Marin   Added Wide-Bus extra data checking.       
--
--                        13/08/2013   3.0       M. Barros Marin   Added GBT-8b10b encoding
--
--                        05/10/2014   3.5       M. Barros Marin   Added SC-IC (constant "11") & SC-EC patterns
--
--                        05/10/2014   3.6       J. Mendez         - Removed 8b10b extra data generation
--
--                        27/05/2017   3.7       M. Barros Marin   - Added Clock as test pattern (DC balance data for Elinks training & Elinks bit delay checking)  
--                                                                 - Fixed static patter to "AAAAA" (Elinks bit swap checking)                                                                 
--                                                                 - Added Elink SC EC test enable
--                                                                 - Added reset to error and frame counters
--
-- Additional Comments: 
--
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity gbt_pattern_checker is
   port(
      
      --=================--
      -- Reset and Clock --
      --=================--
      
      -- Reset:
      ---------  
      
      RESET_I                                                    : in  std_logic;
      
      -- Clock:                                           
      ---------  
      
      RX_FRAMECLK_I                                              : in  std_logic;     
      
      --=========--                
      -- RX data --                
      --=========--                
      
      -- Common:                
      ----------                
      
      RX_DATA_I                                                  : in  std_logic_vector(83 downto 0);      
      
      -- Wide-Bus extra data:               
      -----------------------       
      
      RX_EXTRA_DATA_WIDEBUS_I                                    : in  std_logic_vector(31 downto 0);   
      
      --=========--                
      -- Control --                
      --=========--                
      
      -- GBT RX status:                
      -----------------
      
      GBT_RX_READY_I                                             : in std_logic;     

      -- Elink Slow Control (SC) External Control (EC) test enable:
      -------------------------------------------------------------
      
      SCEC_CHECK_EN_I                                            : in  std_logic;       
      
      -- Test encoding:
      -----------------
      
      RX_ENCODING_SEL_I                                          : in  std_logic_vector( 1 downto 0);
      
      -- Expected data pattern:
      -------------------------
      
      TEST_PATTERN_SEL_I                                         : in  std_logic_vector( 1 downto 0);
      
      -- Synchronize toggling data checker:
      -------------------------------------
      
      TOGGLING_DATA_CHECK_SYNC_I                                 : in  std_logic; 

      -- Error flags:
      ---------------

      RESET_GBTRXREADY_LOST_FLAG_I                               : in  std_logic;      
      RESET_DATA_ERRORS_I                                        : in  std_logic;
      
      GBTRXREADY_LOST_FLAG_O                                     : out std_logic;
      -----------------------------------------------------------
      RXDATA_ERRORSEEN_FLAG_O                                    : out std_logic;
      RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O                       : out std_logic;
      
      -- Counters:
      ------------
      RXDATA_ERROR_CNT                                           : out std_logic_vector(63 downto 0);
      RXDATA_FRAME_CNT                                           : out std_logic_vector(63 downto 0)
   );
end gbt_pattern_checker;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of gbt_pattern_checker is  
    
    signal TogglingBit             : std_logic;
    signal TogglingBit_d           : std_logic;
    signal TogglingBitSync         : std_logic;
    signal TogglingTestDataSeEc    : std_logic_vector( 1 downto 0);
    signal TogglingTestData        : std_logic_vector(79 downto 0);
    signal TogglingTestExtraDataWb : std_logic_vector(31 downto 0);
    signal RXDATA_ERROR_CNT_s      : unsigned(63 downto 0);
    signal RXDATA_FRAME_CNT_s      : unsigned(63 downto 0);
    
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   
   --==================================== User Logic =====================================--
 
   main: process(RESET_I, RX_FRAMECLK_I)   
      -- Comment: * LATENCY_DLY is used to allow data from the "pattern generator" to pass through the GBT Link.     
      --          * The value of LATENCY_DLY is a random number higher that the latency of the GBT Link.  
      constant LATENCY_DLY                     : integer := 40000000; -- Comment: Waits for 1s@40MHz                            
      type state_T                             is (s0_idle, s1_delay, s2_test);
      variable state                           : state_T;   
      variable timer                           : integer range 0 to LATENCY_DLY;
      variable previousScEc                    : unsigned( 1 downto 0);
      variable previousWord                    : unsigned(19 downto 0);
      variable previousWidebusWord             : unsigned(15 downto 0);
   begin                                                      
      if RESET_I = '1' then                                                  
         state                                 := s0_idle;
         timer                                 :=  0 ;
         previousScEc                          := (others => '0');
         previousWord                          := (others => '0');
         previousWidebusWord                   := (others => '0'); 
         TogglingBit                           <= '0';
         TogglingBit_d                         <= '0';
         TogglingBitSync                       <= '0';
         TogglingTestDataSeEc                  <= (others => '0');
         TogglingTestData                      <= (others => '0');
         TogglingTestExtraDataWb               <= (others => '0');
         GBTRXREADY_LOST_FLAG_O                <= '0';   
         RXDATA_ERRORSEEN_FLAG_O               <= '0';
         RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O  <= '0';  
         RXDATA_FRAME_CNT_s                    <= (others => '0');
         RXDATA_ERROR_CNT_s                    <= (others => '0');         
      elsif rising_edge(RX_FRAMECLK_I) then
         
         --====================--
         -- Error detector FSM --
         --====================--
         
         case state is   
         
            when s0_idle =>            
               -- Comment: Waits until GBT RX READY is asserted for first time after power up or reset for starting the test.                                                      
               if GBT_RX_READY_I = '1' then
                  state := s1_delay;               
               end if; 
            
            when s1_delay =>                                                                                                  
               if timer = LATENCY_DLY then                 
                  state := s2_test;
                  timer := 0;
               else     
                  timer := timer + 1;
               end if;  
               
            when s2_test =>
               -- Frames Counter:
               RXDATA_FRAME_CNT_s <= RXDATA_FRAME_CNT_s+ 1;                              
               --------------------------------------------
               case TEST_PATTERN_SEL_I is 
                  when "01" =>
                     -- Data counter pattern error detection:
                     ----------------------------------------
                     if SCEC_CHECK_EN_I = '1' then
                        if -- SC-IC:
                            RX_DATA_I(83 downto 82)    /= "11"            
                            -- SC-EC:                 
                            or RX_DATA_I(81 downto 80) /= std_logic_vector(previousScEc + 1)
                            -- Common data:           
                            or RX_DATA_I(79 downto 60) /= std_logic_vector(previousWord + 1)
                            or RX_DATA_I(79 downto 60) /= RX_DATA_I(59 downto 40)
                            or RX_DATA_I(79 downto 60) /= RX_DATA_I(39 downto 20)
                            or RX_DATA_I(79 downto 60) /= RX_DATA_I(19 downto 0) 
                        then                          
                            RXDATA_ERRORSEEN_FLAG_O    <= '1';
                            RXDATA_ERROR_CNT_s         <= RXDATA_ERROR_CNT_s + 1;
                        end if;                       
                     else                             
                        if -- SC-IC:                  
                            RX_DATA_I(83 downto 82)    /= "11"            
                            -- RX_DATA_I(81 downto 80) SC EC Not Checked  
                            -- Common data:           
                            or RX_DATA_I(79 downto 60) /= std_logic_vector(previousWord + 1)
                            or RX_DATA_I(79 downto 60) /= RX_DATA_I(59 downto 40)
                            or RX_DATA_I(79 downto 60) /= RX_DATA_I(39 downto 20)
                            or RX_DATA_I(79 downto 60) /= RX_DATA_I(19 downto 0) 
                        then                          
                            RXDATA_ERRORSEEN_FLAG_O    <= '1';
                            RXDATA_ERROR_CNT_s         <= RXDATA_ERROR_CNT_s + 1;
                        end if;   
                     end if;                        
                     -- Wide-Bus extra data counter pattern error detection:
                     -------------------------------------------------------
                     if RX_ENCODING_SEL_I = "01" then
                        if std_logic_vector(previousWidebusWord + 1) /= RX_EXTRA_DATA_WIDEBUS_I(31 downto 16)
                           or RX_EXTRA_DATA_WIDEBUS_I(31 downto 16)  /= RX_EXTRA_DATA_WIDEBUS_I(15 downto  0)
                        then               
                           RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O <= '1';
                           RXDATA_ERROR_CNT_s                   <= RXDATA_ERROR_CNT_s+ 1;
                        end if;
                     else
                        RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O    <= '0';
                     end if; 
                     
                  when "10" =>
                     -- Data static patter error detection:
                     --------------------------------------
                     if SCEC_CHECK_EN_I = '1' then
                        if -- SC-IC:
                              RX_DATA_I(83 downto 82) /= "11"   
                           -- SC-EC:  
                           or RX_DATA_I(81 downto 80) /= "10"
                           -- Common data:
                           or RX_DATA_I(79 downto  0) /= x"AAAAAAAAAAAAAAAAAAAA" 
                        then
                           RXDATA_ERRORSEEN_FLAG_O <= '1';
                           RXDATA_ERROR_CNT_s      <= RXDATA_ERROR_CNT_s + 1;
                        end if;   
                     else                        
                        if -- SC-IC:
                              RX_DATA_I(83 downto 82) /= "11"   
                           -- SC-EC:  
                           -- RX_DATA_I(81 downto 80) SC EC Not Checked
                           -- Common data:
                           or RX_DATA_I(79 downto  0) /= x"AAAAAAAAAAAAAAAAAAAA" 
                        then
                           RXDATA_ERRORSEEN_FLAG_O <= '1';
                           RXDATA_ERROR_CNT_s      <= RXDATA_ERROR_CNT_s + 1;
                        end if;  
                     end if;                        
                     -- Wide-Bus extra data static patter error detection:
                     -----------------------------------------------------
                     if RX_ENCODING_SEL_I = "01" then
                        if RX_EXTRA_DATA_WIDEBUS_I /= x"AAAAAAAA" then
                           RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O <= '1';
                           RXDATA_ERROR_CNT_s                   <= RXDATA_ERROR_CNT_s + 1;
                        end if;
                     else
                        RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O    <= '0';
                     end if; 
                     
                  when "11" =>
                     -- Data toggling bits error detection:
                     --------------------------------------                     
                     if SCEC_CHECK_EN_I = '1' then
                        if -- SC-IC:
                              RX_DATA_I(83 downto 82) /= "11"   
                           -- SC-EC:  
                           or RX_DATA_I(81 downto 80) /= TogglingTestDataSeEc
                           -- Common data:
                           or RX_DATA_I(79 downto  0) /= TogglingTestData 
                        then
                           RXDATA_ERRORSEEN_FLAG_O <= '1';
                           RXDATA_ERROR_CNT_s      <= RXDATA_ERROR_CNT_s + 1;
                        end if;   
                     else                        
                        if -- SC-IC:
                              RX_DATA_I(83 downto 82) /= "11"   
                           -- SC-EC:  
                           -- RX_DATA_I(81 downto 80)  SC EC Not Checked
                           -- Common data:
                           or RX_DATA_I(79 downto  0) /= TogglingTestData 
                        then
                           RXDATA_ERRORSEEN_FLAG_O <= '1';
                           RXDATA_ERROR_CNT_s      <= RXDATA_ERROR_CNT_s + 1;
                        end if;  
                     end if;                        
                     -- Wide-Bus extra data toggling bits error detection:
                     -----------------------------------------------------
                     if RX_ENCODING_SEL_I = "01" then
                        if RX_EXTRA_DATA_WIDEBUS_I /= TogglingTestExtraDataWb  then
                           RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O <= '1';
                           RXDATA_ERROR_CNT_s                   <= RXDATA_ERROR_CNT_s + 1;
                        end if;
                     else
                        RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O    <= '0';
                     end if;
                     
                  when others =>                      
                     -- Data error detection disabled:
                     ---------------------------------
                     RXDATA_ERRORSEEN_FLAG_O              <= '0';                     
                     RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O <= '0';                     
                     RXDATA_FRAME_CNT_s                   <= (others => '0'); 
                     RXDATA_ERROR_CNT_s                   <= (others => '0');  
                     
               end case;   
               
               -- Checking of GBT RX READY flag:
               ---------------------------------               
               if GBT_RX_READY_I = '0' then   
                  GBTRXREADY_LOST_FLAG_O <= '1';
               end if;
               
         end case; 
         
         --===========--
         -- Registers --
         --===========--
         
         previousScEc        := unsigned(RX_DATA_I(81 downto 80));  
         previousWord        := unsigned(RX_DATA_I(79 downto 60));                
         previousWidebusWord := unsigned(RX_EXTRA_DATA_WIDEBUS_I(31 downto 16));        
         
         TogglingBit             <= not TogglingBit;
         TogglingBit_d           <= TogglingBit;                     
         if TOGGLING_DATA_CHECK_SYNC_I = '1' then
            TogglingBitSync      <= TogglingBit_d;
         else
            TogglingBitSync      <= TogglingBit;
         end if;
         TogglingTestDataSeEc    <= (others => TogglingBitSync);
         TogglingTestData        <= (others => TogglingBitSync);
         TogglingTestExtraDataWb <= (others => TogglingBitSync);
         
         --==========================--
         -- BERT & Error Flags Reset --
         --==========================--
         
         -- Data Errors Reset:
         ---------------------
         
         if RESET_DATA_ERRORS_I = '1' then     
            RXDATA_ERRORSEEN_FLAG_O              <= '0';
            RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O <= '0';                     
            RXDATA_FRAME_CNT_s                   <= (others => '0');
            RXDATA_ERROR_CNT_s                   <= (others => '0');
         end if;
         
         -- GBT RX READY Flag Reset:
         ---------------------------
         
         if RESET_GBTRXREADY_LOST_FLAG_I = '1' then   
            GBTRXREADY_LOST_FLAG_O <= '0';  
         end if;
         
      end if;
   end process;

   RXDATA_FRAME_CNT <= std_logic_vector(RXDATA_FRAME_CNT_s);
   RXDATA_ERROR_CNT <= std_logic_vector(RXDATA_ERROR_CNT_s);
--========================================================================--
end behavioral;
--=================================================================================================--
--=================================================================================================--