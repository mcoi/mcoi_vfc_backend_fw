// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:43 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
I76O1zME2nPcaaTceMh3HMMn4DU2QMYxugYVCLvbZ6oT1/IKdS47u4QjdEnHd78U
GGS10vgvOoCHPEwDzBnLOaP4hgn/fkfg5AtCKIOKnplTPgauEs472URlYzc3KbgV
h38zDhKzJXKrJJOkb/WAIUutGLsTaYtT3dTykaIvpj8=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 5664)
4/zpMXsidda0lwnWLqMG8isAEzvcfGr1OsZ2BWjXZjD+NV52rYxlY3IuBudctruI
+LXgALRmaV8PgerJT2hY/IorroEShObif7IRD7bWKhIUGL58G++Kfw/kgimCVM56
/wtn/Rb1Ch/qbu7IxNJ7AZVNjgWFJxTHj6cYp31cLc9aOMDuvRRYrzG5xQUeVAGs
zin6uZOVa9zUPg0rBVfMfT6KTXBHVXkYckLOn73RcrI/8EPAVMvJfGrPBtyDYjAN
tE/wvi45FmC0GvORShhEVo5mSnjDL643GVHUfau/vJsCvKuEzbxZuhuBKfQwDvmP
If+zm8XpZzFTXXmD5K8JpX+ZPMR9sIalJJWU+tCcTvjd4HDrRUZu9gCLNN/bTjUB
cgnyb9BckprpHEy4Ci0ECOQAxJn6VVXWIlZBchHc2RNSh5/lKmmT6zt3JnCuUnoK
IPzJRjygjbScjWAksCFxguNdUn7n4LMetu7pcioVlG7rgM87CBmeH7/73s4cJJDe
7M6jW0M4udkAurg90LEqZIQRKqKluAzYzQyf6scUMD9wDcHh6DaXJ5+ZpU49iSlP
6pbIBXWA+AXizsfDDOVWFsavD1ScM7v6DnZYJybd11M+9Gn/wvrML/tY0AUkmJx2
HgoXv7thVajWUnKEqbc95R0zhKbxp2iJ4KphiblXgC9d0zEaCCIJv7BF2JvIJQ7j
Fl6lEOA9puKiBFS5ahRBHu7zZOH3RzEaOaEwaUEdPFJ06A6VWN/1PVv5kX52Mmf1
AwCDJ0Y1L9d9vfztS7IcARiuT/IBxoKLZtBkhuC0+PG/F0jV+p9O3MCOarUe8OPN
h6nUjLpSiUZhmSccQeadosfVbkUntAsvi+SVk3ZXjKCHV2egbobRbx70suX+8ia6
LcrxKE5NDczUmBBguu9D/Dq3spd39E4OiLuPfUqZjeeITvGRpNqaDF7waPBgATkR
hpmsBwVXI1H98MfHISs03rK8a2Slq+aqXkDNh+Fra3MYGF09R9M/tO8xGPB0nziK
uPxW7vcHJMmDoLSGWYNCdIe3yda3BM60RH3kKSC/mnWxO81gkpGkYPYQPOmDBjwg
+CgrccwV6cg7yOAny/aKaoRVBDjex8TMuOYcNvL0JwDUw6Aqw4kwhoAIaS9Vcclc
2YoyqMBz5wOh7W2sgkuZns/pMxonjGYEMZYNZW+C+YVthTKwn4jDKnnrxtDcfZ/Z
KBZX9d2TWVNpEwmq02TspSGIlZQZUxlZUhhx+82P9NR3c/K4nTf/3Iuu14LQ7lbr
aXfLWr5FUdvQNdVQ0rbHr2JNYp7M6qFzGdhMF81M9fY88BqV8my9lEmX/vzAWXsP
6nBakSyXrZaAuQPZXNSXXlkF+BOrlBz32G4FvithuGpHNnQcnFCQ8vF500jNTnqB
XIOffvn/e/ve+0/4L4aG6rLhiK24vugC4Hrq3DJztLiLqHby+HhRTVQy7l3m7UA0
EjTCD9Y9rzncwk/LiU93DO0YW+tDYGWfTwPJjPzPvECbM+qgtAPQVxigC4pt99G+
p1Z+L50+V2dSGcpF8nrYLo6lg9ks+l2esyutirS9z6eQZU3n+tuUvjds7f1FpPmf
LCLvcHk7V85uligLsBhPW/lawwuedP1X1P1xbou6J4IskPxK8UIpJpAWWmRcYAKI
RsmbWkNpGPyDp66FHliWZTHPuzBbTSNskIDz+qQMqBzBUd6XvgOku4qRYHbIXdUz
WxDg4DoomaAtHH2vMkiFnBHXdUw582V4r7n01qiqJbdOsnqQAeSMZP5PFFpRp2/o
pe5j1a4h47dzUZ1RirIjGJRbEUdJFpgq6ei0PPyeRulwYEcp/frSOz2y0+AG9IW3
AJ+h8zrAMN7yc9ChD07WV7u4OntHbdd1ThdxyhAlErOX1KU3Lk2vyawWKA95yGmj
M76FscglSk5ndbTog5exb5pZrOetkOiNORk/ef+Eih94LB2bkP90E25XiVw4Ou93
00TFzwGDANS8Y7wCJrw0seQR3zjpHN+F89zDx4us4EoHyxcOt8y3wYyn47pkPidE
Zt0FRRqa7t8jzahGlcZg8md+zvvUhoJFZ38Hxmu8wBxlipcmMz6nB6qMuhd1Sm77
jjkqpHIKYUK1VsleK+Lj60QEnWRyJfwvAIJGcj2a5leLagGKbwM1pV6Bc3z39/8W
GoBlnRILFuTVi2lHA2z1o8q2upfAm7qb3Nn4pqY0+SB8BVK7OrNST8RxWIG9mJH1
8K9s8iW7G189Er7Rk/fSgxxocVHUkqFF5LYY79Nn4gSe3jZoMe5IcI55p71wLSwY
Z0Tl+sYOwdKjKc18oK7PTlobxsGa7XXlgIlFgMZT8q3VH/w9crIaQLdUTcrak4pt
pIGis9yWVu9bQpnsqQX7RdjTQQRDEOmtEJs/gh5fCAVKHHO7cb5t/n9rHVDRx4s2
/cSVsGdzp1f/8Mc8vNpaxwpvFn2W99p6KARlldN5LCPT3pSrrY6BWGGRpT2ZgiUq
op5pTeVK4GJ1oR5vEHf8nu50G+wE2DJFJkF3VCx0EUdGDZotvV0ZDb2ZGV+ykTsf
mPopDAec+8j9EiYnUPX9KypVRKIYSrNbaZNU4KVtGybkWjmsAMAdSk/JH0kbqFC6
HbJDLm3sHAzjQP09IQH6OoVuk2HfHXiZ7oVuplZXDKt8FxOrJNCK7TQhA3uigTBz
PD+GGpwkQ/nFzfs/oUxdB2zXfsoGAtEhgnonVPp5eCO7TpW3Dis51jlwm/5YlOj/
wr6ciMngTR5S8LBRFnliRlHqckiLeAZvFhccGWp3KWJaWiZF/9iSBqjQoRMtCWne
Vp/ggMOk45zw5kppxGzaby11dgVvxWHhB+CEA09MVsSSgYYR1SWWBdC9Mkj/HKE7
VA0YODnlM8OuLwTgzEbUApxg6qgTrM9IQSX88iofMhSI1HITemyIZidHa7pgxW2B
u6zHMKjJj424k/j3u8ZhDsb9SbK1iekA9JmIiYDXi0lNBe1GANMoINIS9soyKH63
fAc42maduUWDAJgllj+pbEARSOILPA3HGpo6jYGR8W/874WrKpjt6EnZ0hh2xElN
STYovpoWrAiSISCu4OaKRYjEUnY2vx5mz76rigV1eR1V3a1nnsplGLGSzsXEAf95
LxMKpri500lDONvR8J7KoWiQbDWfHSiK5TpcQP3AjIzagJt4B1PXrbfChZVL5jkz
ufFEzahrLZuZn1SP8virWzKkffc0x6wGJsaeWD8qxODXT02y1hhPeib9csBAH5tZ
2MU5tFhe9SSZETFVp92e0eL3ZUTFkFmZOuTU0j7RDoDJbvzBxN/DeBhQPmXvX0hN
f89V2xcj2p4W6CefE+LKceXlfrE3dJEcmknp1g78F5JFeJM0mELigN0ZIQhWmyE3
up/7AFXYI2yZdHnVwHE9Uskys4kxUvDToGw3e0DswpJtoC8XJw1Bu9/ODbVMQRul
A6Cu+8CxbpBh+/MNDEq9BdgzNe1oUDo8UQZnBqLtW5XkCACiMk9gyFCY4T0By+tp
RDpj0LXE8rK5qV5oDEBV/ta2S95wdM5HXw3ljRvipoifNWDZ7XkJZ/mmiI09DbbR
qel41n90A7adrC54SoOZI/GLEUwaIIc8Qz6tvAwPn/1Nm+Ft5DXPlNWlbqXF2epd
iv+a+HSySQvMd6zBJViB+mKv0LQbpKbtsXvSStiJFXEhgNZkOSyOgCLVFiec0EMl
d7jHDtvVJSSk6GZJ9m8WFE+tuaD+BzrgFmITZ/pOsJsdW8+Sdagm5pucwIKM299V
Km6DknelF7EflfTnh9I4pJbWHafyu9TgN3VZubj1EI/tC282WZhM+dUXC2+1lSzu
vg5+/Ec3TBndfMuoVAdRY10llfhe207mvYBNgB0yvXvuQ1hLriaPur5KyS9TgzoT
SBN8zcuUPc1V+nJLa+Yy7HjavxSFuSyVn2M7S+lVSnYijo7L1gwV+NNkHL9Tjyro
EaRFvW4ZgwfEafAbQI0NdrjuBENJHPWdM9flICw6p0pcCW75VRn1W/1hbUnGQTno
7EhWkryp5XYDYyhBvNOVt+LMCtPCPG9XQIzA3iOnmevCF0+h+QPpNMBsWT3xsyXq
0FG8g7eSfAPkpYnHL/zNofMbhR7zVvj6W4AvoST4fDVW8u7aNb7XzaXPPXu4WiUo
+4t3aN0yDvzMib9Sy5sgC3/f6GF6DdQOyJ+lEHSzaJ4J6xtomkbxHcXp7mzJMCbI
81SSRhl4gEqQjbeLKArtPTYXl0raMNciTktufY3Q/lLYq6DDEbS3/+pwgKWDke/v
A2acbd+GnoEZBwykj0Pi7zCx6aSb7rSgXjT5WPRcXIfr9iNHRUVwpYXHtT0+Xofj
pkBZzD+lAjy5d4R8CcdRd17ppKo7hdq1ohrxpKJggFb3SGG1leHXNW8NWDDyKnw9
RYq3LC7sJEhuPRsusVD02c7MM3A/FC68PJ6zR6AaWTZz3EypqMzCV9K8w0RThcdZ
C6F4CFV0dfuDT6/ZcSNra/UGojcvPUZfC1on4p5S1U//ER53YohHvbVJ2gTobO8w
OiZPG7w0vYzquPTzfbUIp5ux1BSezENMznCy1pOnyFGrGEBR3m2W6+fX1Tm0NExv
+MoBOXUbVh4hr2uz1yPNasMsDftzNtCsf4vXyTZVn5LARJ7GhCu2pm991c5f0lah
8QNWwpJNFW3rtFova41UtiJvGG56G9UKZ8m7XdnIFbEEhm5+qZ+8Nmw9KRalii4d
lAOErGT2JZPWnsMdLXvo2EzokPRRTajRyRiVtebJsCMsoE1w5/PGRLTCeMuCuayH
cfsoJCdZs+XAiLN0eyOBsLBuG+Vavr+n1bgmx05oXQsiIiiSmiVE6JcL2Zn327I/
cWJc18Oo01fwVlh4x+vfCrfCVA6qiG0AWAxKgFeGd8wImvy3lK/ryh3RRR1oR9FM
XVXO0sIQ9WCO85Je7NvX323m6ITk5FJF7WG2b/n0rRkLu3rBzsJS3iECj2/Ovb39
A+1L2N/z6EcHZgrvEd4wcmI6ATuOYIzEtmHugwtDO3h7JILF8Up22OzAkKmHqvyR
rE0Cw/UTaJbILIFdaUcASudo0yi8TT6Uq3NEq4qeLdV1qpOx9MHq3SRLl7KBh/iB
Qr3Z+dnfo4noteotwxfhL/yQZvGuQlwk5k9irC0iPlG0/FMbCn3TiyUkQHXZYGcj
Ypt9VpF+/dlTLEIxogD247VFhXXOIBZGG9CurzgPx2rIEoFcogr3/UngsYj/XQV9
d3JlrlZEelYMzP99cdkJhsRB15fe1OXbeZt3ftaZY2PX4FjDX0PcfV1z1XnEFNvK
1kjCQd1yC7td4YBNG1vwQYwUePtsdSFlyO27dLgTq2Ke1HDTVX+9qHUNea8hA0kK
9hbNnL4LaBnNVdpOpYQnzYKCg7bv8iWO93bdVaxvtzpGVpY6H9Hcu5r3eV0KuSBM
4ZJ40coUTOCzjTSWunZyIpblRWGggoW56MSeuOJFtFLotzpHQRJRmcdsKnU04h8M
sUlI7I9gJ4V4doya3UEdtX89wRzUuksvN5Fu7Wm1WfCfksurh+v3h98lkOqzx+nl
S6Ejf2bp0/NGkbsOa0hnQXZEMYC9coS7yqo9g5QpFarr5XijCNObUiooHMtNrIy5
qhvS0jq12qnADmHeebsTWYcw+OmGyImLzco9jta22Y0i75Hau8vC9+EsYHMyevuN
F2b+KdCTWUlvqNKaJLKaCt4TeoezZjVUVVoC05mTdJXwVEPBIZ1goyIxMHyd1jal
iBHdJ2Tf90ErvR+BtQc1qEP/G3V5l8RphX5Z9zXPS2lEwnOqHeaDYzxts8rY4W+F
GEVe1PagmiQTvbdgEQtQigrGS78pOudcxosxyQmoKCsKiA+cxQkHjAoL6um2qjhl
s5BYO6dGttSNMTu9UAj77gIVM945nQ5uyufeZ7ts++9C9wjrgL9hY8blb+8b5o6y
jNMdSjRkmONL+bMyTCueUgQeQqm+aKP5b88aq/MVWIEh7br0dXfYQgdqOxMtuZuh
ZkmhyyN+lc6B77t3p3Bt0NMdJ5fwWiAf2CvLY8QrYyprWGBKe1+L9gJp0LlvIgYq
aQwqAX7eWql4O5uOJrEdUI9s3njyNNZDYOKf2/vIEfRubfHXoVPCwEweIg6YQAam
eWlsHbg48FxwaEnQumVGSJZjTlJHHQDAZGuG/CmDj3yaZgWwLVX9xuNxhhHGkoaI
Qec9Clh0534HbN6tjowJqlQ1bjSPXrnGqxjUWG7aWCec0nZaDH/CyJ5DnveK/6cz
5f1gC/N2ypygikoKsmoXp/O5AILGQxvyHyfHvwbhD9QnkCfXysUAKFxNTKrNqtyF
R6WxNdgFDJWUDmPtbB63A2C8qspBc9n5Peh69RFb0YAzncBM72zesq2DWSIM4OQM
folPDg0Ti7sYPslVZgM1MFP7BAXw7hRHmSiDN2lmBtLmirZ26B/AGd0bRWXUiqo2
ggewj7GWXz579nu1gFvs5m5hZM1Q794ELOGSGfoIqlKwEcvUpKOqkrbjHoGX/hDk
8yHLpn8Iy/q57q80OgZi9ll/NrskY1ldIlI4EGNf2fZlNkVXEGXyJOmHhXtGw6GQ
6ODuspsjtH5Asf1dedn/WBWLyLY1l/twimjPr4QE4dg0f6E5AJ5F4YmsbQxCIHWB
qc0cKo/EYHO7j+rjqVgZMuzy+RwDxzsAsCM89LgQggei0BcseFi074cLMgbi0oWc
tlNgT8qH065hkRc4KE7DxkdPUYmuzcna/eFtJb1QfyxyCfkDtJ6Ka5tN5kRf1sie
8jLowGyUP9d5yt26hw4uZBfRzH7xM7y3rL+EYZ6dqo4koN3jCaUB6C4Iav4OUnu7
av/XhSmj1xq0bUJofiNPP15tV7NtqeStPvvX5tS4Xw97cobeTUVpTEJjptJazv58
O/CqLys9STvoFNvjLTyRflDkEl2jmurOCOlldNVmXNtc9XmVL2SXW7NiClNarlml
mZEqlOPKW49PMovxM92oyh7DrdwNDOeNgoSMKdeV7cGXNfZVx4LXbFjLlOkmi/eb
PGbvO9ke/93soKhw4B1B7DlMIQ3MgNlvBz7+xu2Vyn+bsw+Z1/2hM+ot49MKSwvM
Em/7KkuSlVs821zZN07YHeA0f5fA7D61n9uZMzLIjYXLH27tCqrenUeiXGIUsK7c
JF3NhKN4gxacBREhIQMq3NpIe31IB0sTIdIH8zfI19Xlrf+YrkxiP7gE+bMHTMRA
wZtE7hUFWFU1j7GiijiYhJwcdfQ1PmTLo8g5fReANAaTCJyuvHkHf40gzE317KKn
oirJhpWJvu+G1T1XNmTZOTPJkzaHgcvLQib784nYVODoIBj0fwj1rhARZDRbTyAH
AWwZ7ztO/0r8NN9DgWXn/SNxoi/F5mR8wxjjZMSny+LOVlNJACmcwA6+3ksvi0wG
Urc+edoPSaE6ETaKGlfRyaL3gYOVp6apSoYK4brpLMA+yhndCnlwzRGIkEkeC8Lu
6xiMycXcFwY4UFUCJvTD3JLzKCFabNeWyjCiOE8QX11hrqG+9xWiBH8XFUX99FtA
`pragma protect end_protected
