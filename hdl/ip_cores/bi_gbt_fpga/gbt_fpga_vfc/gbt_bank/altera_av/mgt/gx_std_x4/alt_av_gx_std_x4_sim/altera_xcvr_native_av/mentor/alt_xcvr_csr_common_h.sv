// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:39 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
X3dP7PnUqkf6u8PH6AMUjgb9tOih+Rki02c9HYhlzPm224OJV+Gb1n5Zi/9DFbRw
X01h4O9TZU1b64hY9sLYJAaF2X1yuUT35IG5b46bcW+Q4Fc43r1LfJXknxKKhbdn
pqbTKUXJT1YEs4xwzaPN/2BXrynCfvDMTYpIa8GxX34=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3216)
REO3pmuF5mgToAs8jXl3JS6ye6Vm49WZNSe2BQGdumGO8YMlzLENVHL3RwfHi4pq
D1zZov9Czh21VJkQRK0YgHETHkFQgVs8mdugf5RFbpV9zS13R3Y6tcbH11gA1/5g
CHYATg7ldlYXlArJpt92+oJ4OxJeJ3uYnXwfnJDf6pTs4rZuBrxmYZKIZOC+Yave
UvVZnSmcaVWGkcfQDE+OAC4rsxn3ZW1fL6DynlLIdgbZBmEqV/orNjt2P7y74iO2
XiPTAruHQLHsdgljBmo6MLDSLoRUjq3+9pvZUIG+YlXfDSnxuAiQtEoIqaGAdlIe
Kkx0H3DXAaiw/JkqAoL7c7oPzjrivWLRErQ4yNTJnaMjFcH+6rsql87T4/BQn+SS
M9hZfb4X4VRn7dpFHmpHDnboVE+JtSNBLMmWBURP0QQ+3FN+kiT7jw1k0rVHAOm5
phhxAvkbW4TkaZ3PIZDFkd/NWyEaC6NaUs7pwy9Wod6OEBCX6/ufvh5pum692LY4
HGPEDXmng8WK5+vE6YMr/HkIEPaQbzGSx+XYBBuZ/nqG0MrHuKHvp0im9bi+nISq
oZyiD6T7r2ZL3nHZQhSi+TLRAklBOJd3dsnbztxyXwCcY3CCMN4RTfrVy+jtnopz
aw3R2Wx5VwJdnfgEGO0iYqvXgWD0x6Yg/OAV45HTqr5ogFTQdI3XBpyBbDpkyLlw
pp/64N+7+gGfMclbqB2BZyOKx7dnlGSu9LP0+pRKKsJXgrgyZ4LgRuPdTVUcIUvW
Sq71dddHjjPWjQPj8KhAR+ERTwx6MrTQptuYXgb8gnGwNTuHWLCxzorJTe9fNIwN
srPbaIfRvY2LZ0Kg87IQovJzzauE4MPcghk4MRs0CoC1hPRnSoPgLN90vBkHP2aM
l7aHFYlExEXiT/CM5KxaJT5+thekpQuBzAEBdQTc/2BuibLCp4P/Fk6UqiNeDKHg
pfOXj/+MGIDxE3aoDH/IiPD5Ct7WpcRWoSzPSd5DgP74elM9DSpz/RcmKWfuAE+9
LHTQdPbVEXDhg4I6BOk/Qp2mMft3yY5efB2nfLM50ZwqtBN1uq+wU1d4cMpc+dxy
XMGJytXXEjHrPC1LAzkjHG/uDXsW/EebFar5uv4FvbrJNV7iJBCewtzrvsg0nxta
E+EE7CbJwgtc9b9Zc0AlfS6iZ5BOUiYMJHmoJI2ASOvw54JB5UmSPKR4nkJ/aWSh
Al93R8FJqkUitYn0985POOGBC+n8gXYGzD70a5I0L9dprhW7xwt3eE0rj/o4X0H3
ZTWNebbqHaKLWtt8vONkLb49R2cSyTDFSaoDmQHEU8OdpSnbaV2mIleCKSYDNpGN
nozDq0IsIP5nl4CE/dA4MdC5+aIDqv6n2dMWkQsjQA7kY1hvxaXdMLm+XXsIOWII
ck2J85kd2xqZghAQAhXF7QL8zDvZjeC4tMMaQvanKWGfqwpehbsgMyIdBzgkWarc
Cn06Ur+hpuvgnhzbDJ3ttMLDToYvHaFo2zphjcRZFEiIrZ5R74Xlr7iRxzjXUBpN
QQ6ToHHIINxM4bI2x/Opn/h/o47R16WOOgvGAL17ZL7Emwa2zoD4MVVqlHoghPEH
bBG+V9EnYwNH/vJQa3JCGxFhw4/3j4hErJ7oPpdFk0sdR/Wf+LOttOcQ/plHCgKT
puRcLUoGo+/7UM0s2Cl7ZhrAREP1JRkEUICLtBNho0V6koYEL/BrRU3lVIx0kXb6
GHv4ICPb6cycy8L+LgfqoPZVek7+cLvdFK38q/zaile/m6n1fQPEy9LCpP/yhXr3
TL5MAc4dqgdbKl85+hp2TO0IjVC0LnYlgUpXH2QX+CIWDgMOmThv8GvcS8wuRzGa
axJZFMfZPhR6x0tleV/XSRLK3DOlOjIVt2gbZJsxGPTLKMpyCoUpPkBoX3Pftn8X
MMbkW7mVHQKuKEBub7jHLTSbsYqxHcmFx3sCE39e4xfgTrLS+nKMILt9gJzMSXg1
oTfhUGxCUHA6jXiq2fBGsibi/WnNhK1U9d1apja2A6IkfUp2qc9D5EMryUwkjawz
+XAmxKmsEAdv6QSw/DXV14QQeLvWePRLmhurLsGosyQCh144DJIbUkVURa6f8df2
dkUvS4/1LdjB4yKnHmz71y3/XGxeozG8mRnqCUiygyjfopvISptqYSGzjLCNptE3
jntRk+eqrvKF0OJJOmxCOZIFE+UvgzWdWOOZpDTnXWvrBK4tYu3bkZzlUDWqDmHN
uchzBOYWL6BmsAACZxbHWV3jIcGD0ccwcZ+2lsxcSNuWcQupccD45idOsKs7Ybx/
pSzYm30aPqM1WpcPJq73Zr5oax67YsTFlUEYitfOnm9NPDFIHeEGVk9Yf/QvA4Sz
rMKtABYlWDG0aXUJKaAenOZJILJpVtA6cgo5lHd3fncVSEvR+XR+jT4iV9Zaxaix
sbq3o4uQbwOuMGA3EdNwDddsD0YJ5BcYknZjxIbu8DTHD0yl602gjTfSKyK9Cpow
zpwCk587I18O48gNkcje1JvHl9MtyJrEUtza/6ClYENDcMc7rQdqOsNZMYi+cxiu
8AxkDZjx4AqINVrZAK09iMTZhrdHEr8VKLOmROnBXHxmDn7xrJngQmvcNng8PngJ
Assu/++6s1ykPxfYYvmyD2AIc5NXuW9xADcGSSDIwgp8D5dDGNoX2x7ICvghumUp
NVM0DBXoFZW/em1R1xR8nbeFTzVhqlgJkaCVol9ad3x7a1GRv6dROtyTd/wSamZN
KEKg82ycoO5qdt5IyOIkCB/MGXNlbsvwYBE6PGRovdU8dEZNGfC08NI5BqVvDH+O
OQaw+MzU8wPkKJs48Arn1ZBk1+ChvHBH7Z9djwKP/0HoQpDLkJhMLbmz6biVDR3O
R4eXqLZoSKVNaxMecnHz1JcZ1vv7KM4U1xSZBMxRrWl4Yax7sWY+6SeVKqPLr1Bg
tnLd86Gp/LflWH2KPh0u9WhuZ1Md6CjUvQf5HNFKvuvE2Ag793xdB8MJMTbR2XCK
LvmCGZxoukBv231rfMc7/F7JkagdH6hUFH1KIIaVZCIDl1DZzej8Ihs1xKk95BbW
tyRviVyL5ND0dYyx6UYrlKHmoP8wctEumWsHNNBaFe+0U0/2tSvXOFNPhUzaBhAd
M3nOMS8cgGl7JG8tH1Ay+VXsS50GUNqKfKpuiMdWqhlNiSwvuvn6weGdy1HRGtaa
fsseIRfl5mDZrznK/HPtU64VSfjVWaIv/kNsq+HZkTZdzE4LeiTS1xo/7IN+DLO2
xMP30u9dVXv9h6mO4UFJiyjDrHviEN31Cb79VRxHz3QohTd3EUQ98GhgbOzCUI81
0+4obZY+NHVI9OtBC/M+ggD4geKLW41l9om56VtZBVgVfYS6J3/n4a4pbOsXqEYy
BYhCoKAFjIK4pFsPPSSF6nDrWqExFzDkHnkGUx7iBRoxOojEu5uRJsmslykrPt0+
mgxcBUj58QFfKeCBH5qHjd/DL/QK5haNQzyz1hwMVWc7F6pbZ3Bvyz/1mft1Fgbg
Xf9SADMn3mD1CRHOBnZhzoBbhN6X4PueWX3wXWoR6NZ3dRrSU/+bhz2Lzg6W8WYv
qoVh4OTydJPg9hehJAivWKL6OCMqWm7uyFhL06Iws4+ZEl0reXToSIhFBf3vAsoT
LLkLktbG1W1njQgMlPbnV4FoYsG+jrnSerUoi3Gw+lJGNCGlRWldur9ulwN5oEV2
oSl1i/pA2EJSKHzt7RKsR6d0S87d2ik85LHJEMf/pt0CtTazoNpEsb3FKxQ1lHl/
jPjbBsqGChySgUztLxayXRmA/H5urjjV70VtwteNL9FDrXLdbtA3LuOMqjRF2KAY
xkpkPIg33Yqb7Xgqm5ZhcqPo5wHq2LEf4GXfLud3wUYLA9ZWEfj73NLYW3caIHiz
IEQAznnw2jR4mOuBDdtXboDuVCaOQpX1OWZaBCpEA21C5QhPcrulVwdNQsqGbiQ2
XYUbNZjv3Mq4fvZxy9RS9Ipm3/nyzNMVpaOTui1IrGRWtOu6nVd9G6Yp39UPBxgU
ohikYsCdvtgBbdddRRwHRRFAXN6xzJuNTrfSBi3JK2oOdIDJR9Rn7h1CRdFjxi/s
tUbz4vamq0BE0YIewB61ieUvNwx5Y4wQK8rgPcbcyPsyIrVxv+PI8hKz8yGCcNyO
FN2dGrduvGWJvQ+q/30ozkjf0Nwp6EQXKPqtDDFYdGW2c89hVpXM/iYjE+KWMAEI
PIKv/UBGNmHalpUg1Xl+KvxUCTNu+5w6UdYXvUGp9Ua3XBO4LMTRA5qyHOczRPbM
`pragma protect end_protected
