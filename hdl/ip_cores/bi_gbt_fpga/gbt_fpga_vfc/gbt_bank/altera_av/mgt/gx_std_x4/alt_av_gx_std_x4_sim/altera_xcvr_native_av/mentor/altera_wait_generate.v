// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:39 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
iD8V/fE6NzATc6mXp6LSXcPycsiJ3E0M+F4DfsdtZ7qA8GLZUiGpFgWd+aUrHMcy
GwfSmQr9K3o7axbzZ6Ryz8jpSyDOCSRJUpaGs0g+rB9k/SLqHz3FnCve+2aziMwS
lfQPApDaD3SUafuRoM5L3Z5fIbT2hlHW3QqIaI9owF8=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2112)
5ELpjKRgCNwW2LZSGZz+4BWvGBVC1yK0D38PY6gC4yK2j/YeHuLswkCoPyhQHFte
tLVaZXmKlk9fYT1auy1Sc54VFUPaaFS4NXiFsvq+Og+h+irHH7JqDcKRK5KEUlJo
+z4C9IF/ilu0zbonbOYEYnerx9zgb+OwQpy2d6D5OAOm2nkeQMb9yJ5DabnFRT2S
FXVhQU9ERszt9Oxcl5WjwG+oQYwhsWDjsYrMtagzhOMm0oVixo52m3LSBpZjCgZf
ebffMu/oMQxBHrlWUNDAksQmdC6tlcGRIjYbqQ9GIIpaK17TsIuE4Ux7vfu2CEJg
9T6c+HHUgG6IcnkisqBbybtUw8TaJL+/nLi5IdvTBV8DdMaPkgJadEj7z4sqiAjd
pqFu+VBb1jkDYdLbqxrZJVKSv/kVEH3tNEzT4ph6oL8Ln5NYlhyIThSZ5HqjcvwP
uE9yDfclwGZChmGQ3+j2laf2Dl9W9wcKdJor3jOKDNlVJ4Ra6N0Bgk/rg3yn9fwH
rxgHBUiUB/9GmT6aHuphachzHPF6la3eScLkt3Kp0lzqkZEPZwO9Oon2Rt/MxMdS
xPo4jM8Rfe9+TUwvzlZXYYe8qNZnjoXMZMo200jhpwxvcSekhFvda7Uj/fqkGTOV
7Ci4Y9wgoBk00EQjzYZdtMOWlQarL1kTest6nNC8W+BFFofJ3WqVwoc0NJi4ks3z
0uA4gDuCr/STJbuV/kp7Rnfe+bQoiGEJ13G2ohbfwl3ePxwGCmLEAqf7b14SUCS2
j4iteX3ZSzL2JTgtifmccfMcW06uwCzKhlue6IRWC3sim9gqrV7piD1khZxTXmnu
9D1NDJ5rHV/vfH/zohe79VvyHBw9dqh0hDH/Krl+J/9DY4RbdLn+pg40qynlmNDg
WyFKQhoONcnCNHZRbc88NQbP5Gu690iwUmgKT0tufdyfTmEVpQjAjGYfma0GndEt
cMX6tuDuP9O3dQwhcTlAKwtyqttCG64Vj3DswYFPKV/CB4bWvbbWnDNRyUtzIxxA
6bG9F44nrX+3zJZww9nOTnhvHCeasUhfCTKyv8UvzxOtgYEIVIkGZqWGoaTvp4iC
ptnnKZLGt3Z4a53d4W8UH9G6kw6eh7EIHNZY/2xs5o4zw9q2MmCgE/IdU/cIpsf/
peGiIy0Obj5ZF94OlJI3rp7gmfjYOy1mOVlIfbXEPGColKv+Bx612/1a90z3cT3b
JNFrVs9gmIOrVuZZ4IFl7VMI5SFDRmddyCTCNtICJfu76nB/mBxl5QGUDDOMTOgk
fyGlxblpUHiP19vzjSpF4EMndPHsrqaq/cgCKGFoyvoGuFHqkWBp0EDAwafXeVY3
ct5wONqthhpPgFjcPPUdxXmBkpBrbnptTN+PxhiSJ6SBRBvBe3wKDY4Agun1EM9O
KNheNmtnCxw8kVs1pfqJlLV6Y0VT01/5lWZuSjCRFkpHTjsztqnIK+gMaNOv/Hfm
aHHd/jaPsGMyE3k6J3K1sH29PVoBg+xMm/JD/5D5K807/nx6XPiNjNjzykXNomg+
dzRVikVkE7XolsBA8ExVNKADviZxMpE4MqXHoha0JWQRRsHb0i6sWqJPzlCutbpx
clIQVsM4MhPjV+Lpds/H92CLgWfRaYOnbHXlseyPn/23qUy4SPV+5oARmY83gvun
2chN0qlEc/ohJuz8nKU/DihXS04bDSmXj4BVeXRm8FschYilXMHLWPA9Jb7v/sC0
i1kB6FTr55QFdyx1GFHzI3lniznnS+hMW9tHzSl4qY5xOtW8U2Jzsy80NZD7dVrN
+gXMbYtjDAp5LIESb5hVBG2J+T1d4Gy4zBFBvB9/AVSBk1l+AzmEU0LGB3jyNApB
O/0wZJH7LtWxEAcMhoh+bQSCVh2nakkYa7Gan9x7Pt8xnQWCHBDpkDya+7gihNvi
Ahnh/ZS1zFlAZh75yML0pEAVwoF74PMiYF3Pf01+XtVeunuC7VUWO9uag+sA9N8Y
pKQHGMS7YWB+/oKsC1XA1P3uF10DAG06cp4hqaztqNO81PYwP89Ep6uX67Qzc95s
fWVBTjAuILKEJsZR5CeXJGpcCoFbrfJwFAvraH53Z6YPp46GuqXzqyIEu7KKxLq8
tm/QAR/Al9FXn6o8R5z7Gv1WzuE/+p9pa5YZx78c2fIcD31F5jkLNfkh7u43PWUE
F7mwwpdz5xdE21LGJr/ePJHRtYM9RI1UAiWyzExDelY3ZwlrP+szMR47E1+fMT/C
g8/Y5RhGxTIiAqgXqQkHluXjh1Pw2zxPjnG36vY15czkgSrEh/OEX+kCtfYE7Psd
xUsboWRjkY6EsiqKJTkvgdGDZXq52WPaZDXVb77KH4HtydhwmyS6IFCwKdqteJd2
fZ+ztiOtZKl9l+yu0rQ2LoJeI5J48jV9QNbLstwFnAH0u7oQ0/POImdthqTOMvNG
dolAG6pJ9UQPzttE+XNeogU9/HVPeNDppxFWLCIInc/WzfUPE3dsENBPkHX5piy8
5Rf+5FsxnWVRFU0EsVUp35oi5Hs909x9ahDe03iY1MK7m934MKAlIdIoeGBdoVGB
XXKnEjufHTlpt7r3U99gwucFfKJDNDZ+aRell0RVyARQgSM5wWViLWn/9mOy1to4
DtpYC35YTViUIvAHjSOfnLzZd/C09AHMV8qrqo0Dzgqxs55kgADKxhegBC5Y3ZV6
xLb3JR/47bIq1dMhSa6yZUxHAUTiPmshvjfwQl5YqoWF4mQBUPmYV/V63V0XLFWH
DLcxIf/We2UdzVhT/ZHkeyftD2v2i1CrPv7BUbCgn1CICjNtCg8G3kjUiBwP5ShL
`pragma protect end_protected
