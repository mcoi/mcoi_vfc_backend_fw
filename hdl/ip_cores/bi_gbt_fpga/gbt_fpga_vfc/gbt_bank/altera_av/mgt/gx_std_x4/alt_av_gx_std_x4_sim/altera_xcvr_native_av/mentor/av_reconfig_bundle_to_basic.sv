// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:39 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
tb6jGPfpv6DVRnwol5VWcxc2/HBpH/jMSB0ZBtZu4mDV/QUdqNyB1ucgdsdH3zgx
zL8kRp6+FUriO5DTu6zLPd5CekmXAZmqxbO0MxmUj5dazZrmlvWBsC8wZghTp0iF
kmT1gT12enrBDsSWs8Jhjj6t+64eVzsu+BuSCfKsPlY=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 5488)
SrBpJWAMbHo/Hu7dS0ybTbk09gWf0gr65l3fVh1gChlIANAbMS9xIioOZ56j7foX
NzaPHzzoqOeLAsDaArJdQTxJ1WVnHvkZsAhxdXQE9weqbpdmN8jiMPmCgQ0/9M0a
chMxorwAfMTZo9pM94h5BvpKdq0HKH2pV6mKn2BugNXPy33docalkH6bNw7Eeb+I
5cD2GDZtfcFw2K8J6MpiMcB0GLUEc/CPyzzZ9iPVzk8BBRUKbfVQ8WhHuraQMYEB
FKusbsj2CJ1vtJfhB3dABjeHLNjpo1BQWJhbIy2PFkkPAgqfkUkHqynvsskz9H5y
qD/kGxagwShgmlASYcMzdCzTQ6XbXQbosyfhife0VAjzXybAx3RXpIIQJNHD+eNL
q7Y51JcRUKg1bcblvHy/U7+tiB9z/CapmYnTL6mv+lgMznlm8ZkZx6X0Nlp5Wnf8
oDkzFrkP1HGaMo5DRNAU7ZNEKrP0Fq2w0kRMOKQssQ0MWnbMh/XnkRFzcxEhIf2M
8Wzb57VM79nK2ZLkyVstKy2/6g4XDoWcd17s1hg+ihjzsNNU9ahQfspRVVbXK+Sg
U0YOVhm8nVWZFeK8XP89x7/kJNNJuBfHwm00iG94SGy65hxJsVJm6Jn5Q5Vhjb8A
BI+MAVjp66GRpHrdt0a5EyoOpSftvy33UgBR7LX93/aB8EDnD3LjPo+7TiLmgZ3G
qlq+cUj1wAlMW/S7fbqiPX0qiwdmBlXd0dTmFPi7JhKVU8pLk+w6kwglQhBJmHqf
hpuDPOd1izwddOGHNSSVwipnyBhrTNnU4rjgR2SWhbaYpLFEdmZ2WVh+eKZlkgUT
neFh1Pk8/DFulN/Zf+HJ/oMhV/FeVqtNLdqQG9B+e+h1Tm1v5Ct5jNjnr7D03njH
dsj+HOXgdFeow+J+8zO4eYGS7MH7xDLNGNuhqYyt8K6jAvmmaGR27ySmGcH1Voho
6q0apZ16mZXY4OkvaM7Lm2gcjcPmoicvZwqDRwiOkWrdyxpWvxPP78dMAWHYOqWA
qzyek+5GRbRiPcLaM3ViLIJ81dfwWLk8SWsq+kcC2WPRyzH4LNg+OLFpmV7JZj1G
suptlllgYafB26Qq8yMEQ1V5FRJ0dsucIvNNQztJZrFyfr4niTU8HFYtKzWbAODr
FcdDL5ZwAoqnaQNObRohzDGAcN0SzNTurSF2zQ4FYZVUrMEF6GLVqHwDNIpcbwB0
koM9DeCvH1DEAtC/pF3tO2b3dj+xrBgK0waiyuSzWcy3WcPgMc3EsFBqk5gUploV
Ljeex+YdQCanq+tR3ff2ddjr7KjJYc6q96ocCqGIgtBZnsoYC5bofcfxV2Js/ey6
5sDF2QoUepFpYIat0f40iyTWquhIJJ70XcHQ5sPtjAr3q+ZIaiviADNnDQomfW1k
neJj8NINPeUPzCQQajOuOFnBBAZm5Z0PNanSq3aA8agGVlO9FMGFNy9+c5Rb/zNJ
rKBQsxewe3yMPWkZFrnHORH4vrUBGG5XxPEEQrrOtSZ3E68nfiSVquY2A6vBc19/
AGzVFLF1QZM9JQ1LhwmJN9zLaZL0dVLTMIiQvu6N9OpCs2fWUEjjK5a/cMeX05Dq
4L7BDZdRi5bUDwhcl/fNWo1u3f1mHi2r5QYnyAU07CmhmEA6sAFFO3qTp6MDKzH6
sXwfAqZyPNgOfw8ciQfhQ1u/uHOE+zlB+sBPHuLN12dCwuGsA6EiX1rAhxsL46bO
bT2QwiCJ20jiFnUPQ87tfmwTxn5NpG5mqCwrL9xJaXk0/oinNSF4gv8WrMxd0agW
DaHZ5UB6Yyyy0Z77o53xqsU60a0baZw7DN1fMq7hkL6T8W0v46jE/t1MOnBwRQ8K
2+8/b7gsmglpSyk8g7plO9vTCMfTZi2BZHOZYKhMmHHp1tixxP4X2+KK1M7VVASg
Vgw3rPv6rt4+tr5l2zQU6M7Ywu4UFIKEeiskc5hIrsXVfRrwaOl+uwArs4+6Nu+Q
VFbe6gLqhkFgkCAeHclLG77c7I8JMXiQ/AwX0I66DqQKn86GDt4CWPcHWYF4uRMU
gYz1sEyTkA00c2R4svLtNKDc94Jl0kwOjEAgQFu9h/aIt6bozMvo9/tfpk+xHJEW
3IdlOgq9geYEgnFuxhCbm6zaAs2EPFpljMJfZ57ZTKQAjNzF3xZfKJ5QYrl10weC
cDsIBabM9apkQfH4TADWYOAKhsFRHks5fQtq5kOJOKZcgRVfaKRn36qLNmaLT1Fa
gqRga7/RZFFYJhpZ9c2UelOxj8VXPY4fXDZvnhaziEUuwj78PyNwLTDx/p/aTgNx
+jymvqHEYOEBNdONf/+fu1/CAuVmvns+YDnTqKk6JvrOpYPz15P7GJFGGHi5lh44
4047MxMTg970UzGeF/VF6XnW2WzORwtTRDqbiAXuaxSCxxjzq3g6kqEtti5xR3TW
ZirUSqJrK95yHrAB7xqVtgZb90JJFkn6t9aFqUQxVLBx+3d+6BCh4tfRj/+/8WNm
tt6x2ql+oyugKB1NfsS2ygXPP0/UAmGgfIMkcBVrqHvIv6xBQUn71UUFoXwdh934
CC4nJV233yNuhzyHTVX4wsLXI65UwZTAc2guPBbs9RL5FaG9DSYvCDDMUpG0K3Zv
KTxDCv5JlQQesmKvXZJxXAfC0xAGL1k+T99dfJs7W9+4LTcjzTBvDqtjgxhB1sK9
Y+8HVkWcxgm4tHlVDXdRKcbBKAH7bs4BMG8aScwGeco2UVMXYseyxi2n1X0eAAhY
o1WFrcos3PJa3LVryWjL+1dtrEDl5RR1VYoXHrT8SDls/UbFueDzRlsXw4jCjA6a
ypPkxUDDQU/ld36tmcPTjoj5aWVraoskzyLhP1lQL/5gSocB3YEqfsDahXN/p4rn
UpgnNR/K4GY7beOcWw3x38Js4u1cv96zqFW32e1uC12B7kRQIKPyP6Sa7XyTsGZL
NmfWZTJaQnqxCl7KCcOWt91XWJxmDjo9AD5vOIr6hfMS/8PBLWUT/bOmJAvFfXEb
Ek+iORQNfCzc0Jd1Auuc9JimXgzl/bJUEYlPxG/kEURkXPwmt8aK6g4/xucU0lYi
B2UfxrUYuL/MPk3ukmNfydezyMtP5c8YLqiX+U1mA1Sd3HMMaxQw2dGRmt1Q3Zfk
vGGoD95vYAVTS9ol7snTfcMm1YJrRKwU/KSdvv5NNkh4P3eAWpmVaN2zXkNm1pnI
RnW3PDGq8mm1oSAasFkDgiD3wTKBgkEwMIMaQuaSZ6Slp1yY7xuYT8Feq0WYz8ki
knjygK9ohoI0YZwdN4fSz6Sk+YTG8FDWqukDl7D6qrjz6vF3RaCZmd52n6KulBFU
3lG4npydlq8x+9xMCv930eAGZxfAY2HxdCaQ8ZUkLbyvA6gXlnGmA/D32u9/FNcC
vnI5PBgG0lgJzxhR07Fo+5bXTp7H7bnirkSLYEgEnpIHXzrIU+dOVbQH3wDqeuSM
kz3HJqxJq0/nKkYUux/B7PXFGBvBbbEzblPJ30b3Jk5SRW01GhKuLKD7vJ9/AR9L
urdogzCRjj6vgvwv6cofTVwCVmk5KZgAlz+8bbXuHQU8K83EcjZ4bdSFTRDaIcY2
E08sbO3MXRKy3r1MJGmTw3DA482F3BjDQ6fHyAySX35RpFI9s0EqCEX1301rrUJh
TBwl3CJ7GCTjTLnruJtetMms4igbaBFRXHnokzBYlHsVdFsiV/gzuYK6OgMTifi0
NC1ke8qab88ijvcQDgxFufVDFo0Anr/8cgYD7VXB6g8i02nNOSa0IHRvYQ4kKlgz
XdFSpjrZNEFHiwJnk6EH/OcKX5GE4QLAxj7GOFt09BzdzYspH9WNa8rmWJYB7Ktj
ZZPck7IyoipakvwWK7i4ly8Ua4sGWYuYGagjOHoKekI0jl7K3F1moKbMkdrybPBx
e44uigwEgA4bV2oRNI9XFQJ4Gs6nCtZ4QVrmSEAFQBoiJp1xBNa4hqquopZEzaoR
9X9jKUoi3yy0vbvMlQY1SMA1JysIB8snVqiJgu2wQ56nuhxp1yN7ITpVSeevlS4A
S5po6eM+BNcFP+0ZkT6GNPtg9mxeA+MM5YBfNANj4+IaYkJIuSYnW4Yll/4wGBej
zPQeTtvQuZJlIljwNJd2ED6npynyVkvEfiwlOZ1kPKV2Ux1V0Fa9BtlYphN2nOan
eXqe9sXjNQd38Bb4DVLdXbzQaQlwLGMz3MSY/dzJRTRhByca7MepEtOOyuftiKLd
Dxxt0a/DWGXa6rgTpWQ9atvW57t+LrVpEROeQnZmH35MejwkBoSAiwFh4hj7CVMX
xUf5BsbQVq0xY5LNHJIy31C4sNQHvNneN8ztJYY286MNZqQKf4J4IJZmqFgnCAW4
A9VNG+GUU0AYYpKwKKDAOXO481qzQs+ryNLPiVKi7VTqhy9arUgJsEIsGupAR6Vg
35eoJAgBUO/nEzxTnmI1AH/OkvwKvwpYvSz1h01rN0cgaewrg/K7cw8WKbvDGeER
49ZLHA9I2CxuCeSHCWyRhTz+oyRaL42Ydj0jOGKOo8s1utblsZaOXMVwL1ZhKvMR
VRhFWUvOgkLATjtYMJXoRz1VV5hHdiv2NSlBgqZr1ao62o/qV2hYsfv/Cuy9SAIF
OtNkp2n+h4hCjKhs/90IuzU7o3WXRbTB39esphy/YaZudUhUjrfN/kAA5ri04goi
J2Aw8vpDOES0btEm4sOzXVnyNU1/8YZics3/YsT644MDSMNifOtEy/uq4okrHbse
DVQi16uzBSJ3jTcCQc/Obk8OBusUUfDsYpelJG9XtWjaxn6eFdvnW8GjnlVx9B5Q
Mupk4kKoHIViSWgEhhSQ141uOxejVS2ZlUwTZmtandzb0vwq1E+KnL1HF/fhEsnt
yzf/IGOgR7XpPRYNzUvVKQbmFyZv5r+JDvxonHVEqrwLsgtMkOCeUUDaDF7jmzv6
CaCmshgQmJOmf8/VGITl5sRmFHAYYPoFFguGWBPQF54CMPh2Z/LQOVztn0Qk0d3I
PWj1LO8wQ1o1GJ6griTE415hn9TrjqMvYpS3IJo1Vvv5dFarC2NmjtohgtXAKdHX
QrDoFqQMPV8UIk/OkWPcEEeObZGeyGlAKjUdOQxEZ6tLOj+EzcqWTUPTB+lsMOIv
RHIgnotmzw4EIOYKpZc8+kvo7/KZq+fm+LvxQjmQ0aYwXB94RfG3akJ71X1TLncM
9rFVQij4eHO1pqjj2AnlXKiQRBBZxX+f1LMZ4wcuCucUONDYslp34g253EEBHmvF
r/6Vns8/OuCca+zFaRHwgXNLGj5q79MDWOj1Sri6mHz/Be7jJbXMBt9Of/sKfrrp
jHa538C9dBSrg/J/QGuU+JTMlqdHqL/NB9czUKfDOu2cvaEiaHcGo+m+GhV30Q3v
IGbB87wKlTsyWFZBcnIsK9IsCMqK0ikNmUi1GqYyLFMIWP8XrC18kG+WgNDlQPuC
Nu8GYznhSUGR97tYVfHzcLr/fpwpdW1BBPRvAjE3EvAeIq5TPoHacbxTqM+z+7s0
FejsfBryLLZpv+HlV7Lo2Pd+x2DMSnSCIa8O+ssEYTaegTGtfgzMxcRT1a/ZjFie
cqMXQvdt5/r0x4yiGqmv6Wpl1TwUuf7C29zMRgs9b1s8cV/aoIi9JqTkFa88+VCL
KN7o3InbuSlsdA15x2wyt78TnDppROqS2b0/HQq/MmH5QUxPpLRyfNyciFwARjTm
BXI983sFOAcx6Dssr3/w07szNYCxr1/5m9ekp26bflPs61WYF5/h6QKYJjqaA2iE
yLXq7cZo9idEoN4IXEb2bjz/iPZd/z/VLfANehAmCVi5Ut6gnWgNCJsm/kvVZ96y
NRJ1ERmc7K1nY8fihQv/6ejlGKc9QsZ+jgvgUM2Hm/NaStB7+FvARBqX68O/SIkR
f7ilCfUN0701JnkeqWMaE283SJ8y+qzNNoUI48hc+4cLUXrfR6xJiyyggY79y89Y
K8Yl8ryE8oYRt0pPijgNYnB2Hy5SEA2Zhaz4HjRg0DTAID4s7Uq/hyvO/62WP532
+kVym9pJBt45+h1myYhCGWip8tk12e8p7HlbJg47sHAfjqbEZly5mYRpzxAQARx5
DWqRODJtgil4Q7IS1Mp+Q4SPjen7GJ/SQEFqwxz5MDn3m6ayOAyPYdYTWGjEAir8
4QoFN9HrTNjWq/R7iuyGHoODBoGOK3OaLxNFPEnoubeXN/a0HWhLlgQtW5kmdQNt
qV8VCVF8SXNx9TPw7w4I+oHGH48n8NiwjECymtd6Vk9lZGdWdDxPEgHfDFV/QEiw
6V9eh5M1lz3XTaY3ZUJw6c7w29v0XsVXCk+FVExVOf6JJSbgKs7avu0RjR24jx3Y
JrlqjcH/O9NpTRtFUaNuNViqYTwnLvdZB4vc/InRzl456gwTb6te3vswUOZScTqW
3Wh7Y9KJ1lTK56T09svqujSguiYQGn6iIRG32ctVpAs6mnJB/7NWPkyRBNky0Ltz
2etspfXazLv/WiFb1EdRSma8/2RLFib1nh7YnvPvbQNvwcwE9AW0XBggtZNoQLvD
PHbo9BhhYbdbxh3sARrCczSGWjLtzHJY62ZeuG562tAAqnFq7h9IvBJXZpEp/fS/
VtUfq00CFgiocEs0VIP5+7WtTuSKQHQdF6pEUEL8o4zRjNCQGGM6lNmbBqXEonvT
jOBCsKCWFgH4rG3whiIlUZL4Dx6JfoUe4f2ZuA15Km+ZWC0HR1NGnqwrVUmCwPOO
7yqECTSCGGQGgTZC/R+i2Q6VIwVr/V3DZvDyclZH81zXsMWVOuekJq73BdLSIQvw
7HT9qRJXvmL1XeNZzbOAdeHXOIIoP0+S84BqUZ5x+9D3niPIFIS7ZqW7gXrqQU6s
sUrR5iaZ6QLaJjl/81QF3e6I+19C7tDbzJ0PWSJZPGqykWMwXP8p2SxQuNrvu7Xm
Et+Ib310FMr8xPia8C1OfP9B8zkqgJivhWyWahL8aX4L8sSvvgkmnXFAOk6DRBHH
YU+N8d8V33c765h3bK4NWMZD0PEag3tcEL6dMZB5AOGI80RLiyCaH9szcc7JYU/b
D93a9uckaha3EtIIz67HjwJT/OVygql4LuoKDEshehrlo3yj9y4w2oJb52rxugOg
LVp7szwqPJ++GyqAxIO642yY6uykA/lZSJigc9S1t9EzWY2sNDM+GolaOBeDUXCl
0bFGPBuzr/hLbzB/I1O96FMkRdpc2F/XSabaNC3EIqBB8cYJ9nGHpxQBSp5KM4Vq
l7aFJZqnA+2ReAvl+XfCJGKrGTcr4rCaO0U6lLUFqUc8GzLtckr2GGeL6mPpUWS2
Pp2vutjLMlxLB6TjxtbTlw==
`pragma protect end_protected
