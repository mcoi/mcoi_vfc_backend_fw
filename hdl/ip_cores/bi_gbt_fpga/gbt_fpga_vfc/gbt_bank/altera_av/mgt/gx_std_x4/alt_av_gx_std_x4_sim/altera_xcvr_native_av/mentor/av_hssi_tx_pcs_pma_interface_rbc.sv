// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:39 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Dtxtj7wJuK67GCZVzgElLFebCSsDpayIwLAoal4kFzAsGPKbdu2adZcy+NxZ0hsv
zawDzx4fxeS/fgyoly0WsEjYcLE2yeY4PV/NUduuc0lO8UC7ObYBwA7i88Df1D5I
/+sRevaKN4lgRLZHlaMjTB49j4KpJSs9ahujL2zp69w=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 4688)
78V9mgA9dIcg6qTYSN/M3sMjX3vt7EKltmOG98CVvBsEfY7hA5GoOztqSOFZlW/5
383tz2eJRh5NSeW6Q+FFQ67lY07W3b5TWB65Dy/8mCwILTs74pfP33f9mxAuyvBD
kkLfTESjvYTZkTT5QmC4ZdXjt1XkC7+ci/FsdDcF4TuAZQ81FpQd8Slu5b6BKcQp
u40Hpy/mi1ginvKFW0bELBRp3lCKCZm7zce1yubrZ+XsaCes7znIx9xu9Ld2eKXQ
y8jFJ62kub8aJcWSupSVxCs4LRvfHK6zFT4rmvT2EEtMl0FPp5rvtvVjfxeXQ2ug
RJBdvShXSlYQIuc0Yu1G43RyI8grSQFpngl/vTC15hO8r7qyby3HYSRCuNMexbU0
FroVjE46YCvhfuSfgaC7jo4bScPRSd6oShKZX42EEFpR0IOjO/Lnh42t4+xqJ0H3
DXggwLXtdoFCpIdwPBx05iDh2Kmskl01ZRZyl33nI8eT1I9YFmbXuUphaslB/UZY
cZRfKXwnRwt6/4SUjFUwZgl/eiCNQzqdgRK8kmDMyHyxICIGru9bTMgaGc8cM55+
MJ3iSMrtO9CIVU2VskQhZMEl1i/xTV/oKO1c9XXCTbBo7Wpcb/MKP6gLnMGwKHQz
wE5Xbu8cN7AyK6iYF37vrIYPwPH3uI/cuccRRexnj6ME/PF1Q5TOTpcpWIEgBOpz
Xw3nRpI0FRoisiT11WtMfvh6G2xjQLB3KaAXgtAlIh0GCDvLICk20gZSZdOOruxR
pQOW1hPwspUnpxk2SZDNgGAMgIyTb/5lPuFhgqA07CqdMDLvYBl5uSFsIMvdU/Ue
vW7iG4G/TnHDRwxWe2M+sYvHvUveovHI6zlMGqd0rI6urgo+vlS44Ji234cRYKpu
Q+JjGSUlhs8Gr65WdlSfT5+TnZ6a94SFTplkdCDUOKX7FvnhN6BWscEZavzWgkGO
kYps+QWhhAtZrTQQc/HK876BfxwQgdYEGuzzvo6O9SCu5PjYQiUKQX1wCuszE7M9
WT1hAQ82R94N9mScgoMp4rOBoQuksJAjHQ+4HOVFyWAUuF/uEvs+Uuj1LDSpkxax
XCN0/3/pPd6wywpH5JyZbiWOMlW4jeg5oQFTkxe2XQOG3Ciey2N1/rTPi9zH9+jJ
uN1/8B3BJdNQ9N0r/KFjeMmsIFL2Iz2/pcX+c7pMZw3nh4GCALE9kZWBYelcC613
9nt8NikaV2+phLlJGVhSQwJANgRuofsgKSb7qZ3cSyy2AWGZDYA/HOqm8OknW1Dw
pB2tVgIWaFrXJT+qrMXbexKVIVU3X7KXz0+8MCuGivuZiWdpZ+yC16FzxFCOgCvb
GF+2GOCIMxRKHncWAeUw5BB8C9cZS1+9Q/YbeE54fV+EHl0pkIx6+WAgHgru7uwq
64/I4a8boozwMmewLzMoEPdwqzAWiVW25AKH5aZrQZlgP0O+j24yaeSjVT0hfl2S
kwHbzipc11jVHkSOQaeCvruRhCMPdVdnTIIYJcDMd6M+mK3EM7yAKwAkqPkJxdfV
FpzWLR9kSorHSA3057iGyPvEQ52wLw4sQt42kEbTBqM99JeZwB0zu03kFLTlP52Z
ji3rZQS8//ysqke8PAQwF2Y4ahr1OKP1XIsWGxBRD1uP5bLwr+A4ovN1VhXwDX26
KMpKRjFzMlc63YnC0S41ySgYI2cRyd9BjASfTXOlkLffMQHZA3eO3ZlXiuMiV34J
1xFh84qhDbWlWCUDLmhjMftjMkgkjdV4cinhU/E3N8Za6OR8+52oKmXJGc7Fkjxm
TcpQw3rcAHJWFIBAGh1LfLUm4NX2jdB6nEgeUBCelvjD7auf7bZYbw6OIm4Nq4Tl
y41pQSiEP4QA/8HQDUlbDR6RFQf4oLsteXE1tdtPshs3l2xUy+rf9CT4oXkTcEdd
OofDmCecSzCzvq+aHKa8/Iqq14kglmydzyqud/8pNysDKkNl8/GhpHvCo+wETxzJ
0Su9IISBeBVGjwleV7234t5diO8yb9aiuFOGM0Nf+YTbaI5gbPRduXeb7YbvKNRg
/XSAhST9lye7S4Y+iO8ClxtOkBlyC2zppg3ojN5zUD5/QnlOJYmW0j7ZHpX6S3ns
TUKtSP58uuU92pes8rEZyzefRnSdc7CIH7mmZK4oocelRDEH7RK8ee2U5X1gqXb7
B+LsUxdk16w1GYShSg4Y4/Tk0DC0siSZr7zm+ox9API5Y/vGv3eMt2ZnKEtfjD8L
e5Yv5NFRA+UyWKNgw66lRzbXvFrcVoDxXt0CMKODCoCLKoEbPG/y6KsHfsetYuIm
KcnlrMazwvVweq2W3Rnw2p0ilizn+GtS+SoCSl0pusd5G3D1JqdPyMzhRE3VUVXI
UpD6++m/exxXFPyt4Zfrm/DsBqobV5KBaxl5/tiFIVASaECFeEA3Vj4GY9vHprj1
SQIW7sCAUtoP5PB8KcjeyiQg9erFV0GVyafOaVZXnfIshGvE0EIHy7ye6XQK9gia
rPPu9HozTlK3oylG7Jav5oJ4eDrvbxkUWZ+bOneVg4xC4iNxsN1sQW35U3si/1zR
ZNUwoZX76Yxma0u3xdpWo/xt9uX9H9r1F0n2lycsVQL+MLzqFPP2w8R1QNZuKGS0
Mt+Ra9RUY3JTHOhdKZ96sdH59k5Ah+PWZlJm++4tkkRUwurGNqXs1DuhK1GNwUmu
fYyZ5gC7jUfLE+y6B63n8OpLZBkRZhegw6UBk1oVOmkSFJWsw/E7fahSOjMitRbk
sQOVxOUtiQiUH7K5XtD6W6VVFmgT1t6Rz2jp3RITBrfAV56Q7Tz1Mo9qBqWe0cJk
pyS7UBH9wQmda5z64uA5JJ5WYDL/LQj+ECgAE8GW7hyfSu0doG7reNvJYitkSKxg
lE/yHFvyt04vba2cBhaxnu2SefcPHX1z9jU/OlGq8TiXoTfk3nGktwRoZSrJu7UL
dm0/b1MDOLySALSDzOF2AAgA5g3PT6tzdDEJQ64UVdl/FS6IpwZib0iv8nOkTLLw
P9C+HAVP1BcnmMCR9SX9QEAnAwGUJZ9V3yKFL9xlvfZpXpYivJg6bf4/7decXblt
Vuv84hJTzTzd4Uq7S3VbsMfzmUx0OAqcL5Q+TGJzwWOvD69gD1s8oGJ7vDT58zkE
yQpSrn24KgY1oEZzEP9cGDcqX2u1j4K97B+Jy3lV/4hRfn3st2ZO5k0HzCSTdzq8
8tmRsy+d1FdZ0Fd4acedXNZYydNaGTT4qy4gg8RXZWaGIutW/Sc54oVu82b8Gznu
VdB7MMNCvMsh6g39HsgfHx8ty9NRCkrWXJNDQ+LKE+logjMErPLiVBJuIHCxI/lS
/d75+tktJvcG/F5AT/pQoWwBh6HFsKyIHv32n7IuRKmgkInHxY9WgCbDwF/+HWFL
BwI3cRppWxdpxz+W42627rLrQ49acBrJeUff5CHrvczlaDlN+NE+nkl1aH+goSxm
cVYZLAhkPFtFmtu62mVXL4WMSkoWgLPtJPG99AKzlEx50/JvRzVoZWFeWijuk/mV
bbzNWxA95UpEYbbffNmm7ryej7ZW3xCP59y0FPkVL2ucDuPqc+E35kTdEjf3fbRk
UpI9xdvTPm5fWCoMPxJgYKuppYNrOZw3DUcL89mIvKJ/QPWTU49c+je1YQcjxDgg
WuwkHm+Og6J7Ys6CtxHBczClIMh6D1cFfHusZHPc3FD564QoXHt8KP0bMjOx5mFN
twN07A/9jK5iRr58HVgfnjIBw/y/DvSAm26u+Qr2CoWSpj7DarkBPk0PwiGVcJUZ
xnDl8tVawC1Eek9lZjvuKCMqPTfm1WDy3jGB33Gb4rMcj8ZDHEMzcvZJ1gBQwk38
ykubDxVcLNfVmuSj02OcT+KiJ0w0w8dcHeGNX+9xf5utYqTBltCELoEzOedB7PrA
SZf7gAb5u+9DxgY5jSs2Ei2gSuZqyxotizzVAlhlTkZkpxwt9kHDrk2ZDDr5j06b
yqBQ69t5m8WQKwxh64Vq2bL1nd7A8WiJW/zZO8huT/IxonkKg6hgkifqyg8u4cqQ
ZbNhVvYKjF441SXTiEha3igOch60jE7pjMMVjGRTK3oTX+QVyJH7J1b7VofWx3Vd
CNSL2FlroLwF+wTxEjWQyuOTEf34qqOORNSHcyJQMSPo3mNOhS/UBTH7+KMV4u5U
DVz5COJcMDVHlc+3I/uIkcg4YIuEGwC+gioq/7D00nq4dBVAB2SXwrG1QW24bY+t
23A8f6WJSYkRX7mFO7NlcbUbqe0AoTVsttfRUiarFyBvViLTvpcqROg0zTU9QFp3
2VM2ezVwlhOsDhHNoO168WPlL9/ekqOvhc7v+puF0IfKQaeY2D8QwHM5jjzWiAkt
DeW/e880gnXurQgGoJyr+S5Omeu5vlq0moK5Dsh12VCM8pF1wNbJE7F18PwYQp0+
0y+2LB/v5Y+OdWmd0B1cqt7RjUcs7d3VOHzcD0wxeYVUMRJMxG7V1ax7rpudKAgf
J6bVqm+GmM70TwWpqsBVUbZjSTihFt4CB8OruzS7hdDXGgFlym9DmahxKVawtO5i
5iC8dAfKYjPCHrTYc8AO+jSVU/q3QBrz8Sg3U8iu+R4zT/WovQionCqkgoYKsG33
O8XKG6n1cdLTJogEapHfHyRSj0aYg9WRIAvwI6FQm45neXcIZtiRNqzksQvI4hX/
D5nUWaQWOSL/Fouzv6UmHIFXoFFXiQp+nzDgOVWXOqaJOaG8jhA78M9eA2KVqvc3
eyr9i9RK98QzwiRFF5+0WY2GW+boOpj11FFoC5IrJf8hVsJ48vKu5tRsTCfXiWfU
Z8Eq1FqTJxg5+2ydsHroE4VFQhfNDRtZ8YKzi4j/G4r2n0gevB54RuPj7VENnEAx
z/4erqKEqEhAPtCcupLeipriSETWajkgfrVNoEkui38GZ8KdcChJGbH0PbR0IxGa
RpOeCBAXV7DzwvrXEpqBxu2nwO9bOMfuLrXXIeGaJrH6vd1Cm+4AIaDj2WWG2GID
TpH07GkVzsn7B/tL9JpdbdcGJ6KbMH7nicLb2PN83xiXPqbzBovfGubPMjiouReS
Unpuiy0c6kHwAE5rH8NeCYUL8pGxIOBbHRiJnODLQ+3b30IRcIDaQPkFwpR8fq4S
xZPZZsO5qQK6upkZiBlqGcTdKqcrO/CtCa/owXNaSsU0Ajv83p1rgLfiS45uqK0F
0XKNIasz1RTv5fXuPtPMjY5d+1p2LuUWdB+zbq9tGo7STO93utowAwYwxrIfwDez
hv2w7in+AUvzEdWss0gE7k6ufbGKVjMQM59v5Bh67bZyI/n7OY/tK8rsmcmW49CL
CuETZ0qVC6EXxwVCTD1TGRSfQL55xp1TrnTuO41wIWPlgCZhCUchjZNKsq2o/thG
1SNTutksS8ZwlLHMYffZtnQucdMQ7gw1OmuKWQnkJ3MLcZNg6xDPUDLmywmfV2qF
LNo/zNpsSOimlFIlR5sb4flydRWJwWicgoZRlWlkjOxqJATpQhz0alBIfCXxjawu
fqSFYBkvS4/DSQCh2V/HaD3SBj78wsNNI8iAU9S5/nTCIz94Ho7o1haAaThPsKB+
1ZLrikKchuRyPCTn74Fc0yNvRo4j6OzG2/WC7zEObcoWHeL1rG9NX/kJ4At8X0MH
LkWPYqbQxHo8U6HOA9feU/RbTuYPNIiVa2tMZ7meEIad5sWivYTl1FxwEXi6ivoS
xCi5Xoy80ms5M/OZBhvQXHunsiDcSvv0S3hdaPebJZe5NX5pYh45ll9QDDDy30ub
RQQYDEzHmwCx89qMJE3inQrZDaXN3M1qKpfG2MqKA4JRATvE+uORhw+wquHKR3fH
aP8QnUFElSo3D3C1nkitEUtVnYRs6bEDgSHGBVvVikk9GWUaPDK1p8bJ8VziB09v
6/D4C4MQRgzavDcawgo23XQqOpjVH/kw3uWnypeNkKc1SbejrhQPEsO2zi+tO5xj
CIPj1AL3BKfz0tXGZ+U0Lbg9ozy9LqXdiTDryZ7k6d2niyh41eQ5xAAhGe1g9ddQ
4U6J/L9W+Xuse3MR+KWp58GH584ykK/1MCpdlKwJNV5+47JcO5qIQAwUs50psZGu
DfbnQnx3yS1CPcqtRJJayyVaZoKwAwIJtShvTTc+QHHZSzB8EB6ZYYIH74KiB2i1
Ys3x+A3CM3W7Nfbk3LAvCKW/BSNV9lnNzV063RiqQ7OuHT0akDDKUIjWO163Zafk
1PzWs54SiGTHBbXMG1UWhz8bQExQ2SY12zrga5aLQ94=
`pragma protect end_protected
