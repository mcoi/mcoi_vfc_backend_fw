// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:39 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Nyz0AIqtZ55RSy1I6eMeKda2sLg1wVbsPcquIUcl67neH4WcZ3CojNgTeDIOK2kN
rCm9+L55ipmuVrCVUNsKo4hzlGeh+4iHKQkTe83qJqow50JGotbtuzRiXh30ZO6S
rrNL3bi39JKKttCuNQv2Duat1yHBqOPY/IUof1tJNRI=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2224)
czW11dFToGyV7Ak0Sj5e3CTyZS4qgUDwOuyAJ2WRkL6lLDauXY78ztbBd5Xhevat
vuXys8vMf28IOu29JMkYLO4w7+W0i3ZxEvOw/8sXUN7tVopRUgkCX3yxmrsN3uA9
T9Nje6rAWc83h6YQyqObV186zxKrEbYj5IszHCjpHb0fzzacQ1EOOn7QriW8ejhR
ua4r3ks6RZAlWsoDIxN0NGzzwVPPZdiYI1BUmmLkQvgDSoW5BXaSY4C43bLem/aC
lipDQ0A7wgSENSvQ4Wq4U19OwEBszlbDiOqigrJIhSGik9Tn9ETRuNCVIowSEQp8
AlMSArf1teHMDhw3ElKzgeVRVegMKxFG5SsgKJicMDFP8rkctAnVomqEox0hfUEO
+d/ekC62cwEU2388ByBwFc1fsoAWsGPYee/Sv2nqYzlWgrmCurORnGyYvsnqMv85
uUXWrt0NMquUKr4nz7DTsVBwryBAjJjLK13/lbQCP+Oz1ZaowfKeYJcacGI+d7OL
EwW/40HGkPv/T3tN9V+lmHzA6pQiE6nXzMkgIh4cQM8yryp1ObFaE03sKfKmqj1C
7z2UKrOZ2aPXzLjZrfjuNWgeyIXsAAGSpVua5/SlRsVkaPD5dQNI/4EK+sz3Pkpq
+eixBcGsRu01mTG62e3bGplpmhDCb+lsmQFLTBXyFYfR5Bp2/Ks1CYfab2UAQA52
NRwdY50WFWx0G7mO3kGhh4Wf+U4X4jYZJag/kyx20wz+6rJlvwXtQPrYgqdKC8s7
OAw4BVvqLdumhkrsm756SW6/1IhJ/Qir3w68uJhQqrZOCvP9rzBdnw/WShowqrXT
OpRPQ+UIMgicXHqhDFlIjKJljK9TRnx2TgHymO9KRJpubuTJZHnJqOyoegwpuwWK
JV+yVp0T36qSY83QeA+FD8MNWynwIrgj109gpsqhDSjQeyQvbECfuLGhZ/F58jJU
UpGpmcBW5GatjTIQ2xO3NmUsw1aUCpZBJAu4HzKTeceKDx/bxl8WswIegx5JWyyf
svFc0VKHAQw2fXcNef6jt+lnj5rBNcoa0ynC6l4XRIQ/1WMY4m5iBzABAELDKxhM
mCw/JGzh9rbTan0KK9XH3SvwO99SneKzDjELtxFqXMJBEwuU+0NzcmRVaNhGfA85
JpZx0gMVnv3cOYN9WqbHm4xkCrBgBiDUd+yNLixvYVtPHu+SGs/PZT7QMTI2POhN
+Dh7iHXipvrCqamtofmHGNYsKWhaEr8U8Hak2PcgtJxE5mK5um/cZLifRzBMtEU0
Sd5qhn8qnBSn+IDUYS2ekYw36CWqcVoW/fbRTkZvdR1lUG+COsqayk+tx3xqX8Py
KhjFQhOd9bWKxbnKarysYWpdb85zhbapvRTsjXyDPZb4fvcXiz2z7KL5yIJ0CF9I
KFQrjauEgh2kpkjyaN4+7H5c0hUdDPleuT9wuL2zbUTkpiR+kKAXZ0RgGuljQSq9
A/3QNHWgOsG0nBp3dPewrWs+MSNtLT2A8xUbsuVhb8N46mlGmoWSL9Hp8uOAJ+NE
LC4McVqgfUEdMeyiaArpTvBLYs80BERJMrT0mWdMexbf8IJkh9SANSPXErw0cD3D
uDmfo4qY/S8teq9PR1kghmLacPhJZ1kE/OrL1nC9t9Qk3Y25Njh1kBtodClke6iH
MfJA1fs4BP58s8XGJjSNST/Pw2LP/hglprt5k+/dlvAlfXiRUU5rJd5u5ZaGOW/0
9Gr9fEWz2CkzsunsaAgoyhTy5rnknp8JE3Dv/0GQWekCfmG7YLgBIDxle981ur9l
CsAbgAsuix+Ez3g9K9u8uJ6WH1OGSCTUxO/0erAQbfPDHjKtCqtwAqUg5iLsbko6
TcS/90SfLCpj42AVxzyAlmqyIAUSBA3UJjKietbyjDsaAM2gwORaUdWvYsJsHiCk
WDQE2Qq2WLrgT6IUMiHcLYeThsw0d6vaQnwkjdAIWMud966TujDygEs3bDLENZQf
rCMOBKTqkPVf8r2RigtQeFH8k7JZLXaEj93eqqH+Mp+fAKmMMFqgd3e77R54xTnw
ANLWYf59MlOIha30mzm2fbuDkYNAN8YAa6RLkCkkIPYGUXSbnTxEX9JbgDsvjvDx
ql7h3TtfmIEGGtxOeiYKXxcfKGBsnndD6iVqP+NTNeeEHp4f12yKngrUTC8yF/f3
fLnBndbRLjsiDFAzsay9P5NrY+a5GDyz0MiRmqnLNVe6P8XeUIFWORWhsJCuRB/+
rTlnrzkJjQ+VwLINd+hFCCIx04HJWkyJD9M0LpjG8v+6bbCxiMAey2m/Q4Rlwqhg
lmZ74mD7+azIHnyHM4Pg8oHIvQq5Ohq8ube/bvXWWXm7t6UbjOsWIrr2bMAY6u1l
xTTaJWc2GfqOdgegcLwZeFS2Dv73+ctJfLw5biZXk7Sg6PY6quCD2uKkAgVHBEyF
wN0+Kd+6EGoN1v9+kCOifeN1mjMvKo9RTPPQeiCDN5cx2FeSkOgM0COMes9HfcE4
bEcSreQiCkmggthcGeHG/NejvLwkOnC+6B//arJKrP4fh7mcg/2EG5jt6hOhrx1e
1J2VboltKyntH422EsYTJMfSLDOqvLf4LfSqzSBey/N5BXdR9/rJsNBXJFEK52z0
rUAU/21P0DchbX7IO8UKU4QajZ4tKcSn7D+JVPmURno2X2/HiRAk3Mvo4LkwRuGD
5vPjt19dMR76gYDkG9F30pZLhrcHc0KL3YMvxPkz4G4VHPV3nI89tAdc611nsV75
g5p4hUW/GwcweXO+c+92XRRBWLTqmJjR/+ULwa2G+i/tw4pZ0KYM+LLn0ZK3o0+c
lEGxffZYkBERmNuRmwpUoamIP33w84h5ZBi5/qC0Ww9LANMchy3wjbmiWx5vOgT5
IufcJ9Ag4h13dbag4kvwzZMglhSIcNMPXFL5VYnb7vOxzmNK3DGkt16varv8FTuG
Ox2niszr21hiQ731pXllEw==
`pragma protect end_protected
