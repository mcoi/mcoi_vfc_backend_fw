// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:39 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
enkn4ThpEr7hvb0p+RNwqcYG8tQoJloNifXVIOTGC4UOUoI9gfrgcBhz/HhRjkNP
PSJ03hYcZMLCbLXWGe+unBbNrWERo0rzyE5TBbWmCKdtw18J7rNs792DQnorgz5m
8HmIUTDB9PYSKeCWOU3LScWReL4tHKXiet2WEuZ5+Dw=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2976)
fNob9aDI4uTWxmQ0p9Aqf4FmVZxGcmNm5hQ87fvS64O+P7TgS/Jx9cLQEh//JfmL
tdYlFQ4AcbsNoso5PSMkXcHWWOWgemuXq0fI+tHks39qOHVH483osGk95wvWzdCy
PHSiS/lxjA9NWDcOe2heRuzeq6rngwJDEvKUgXvD7s1NXRLFzCBRrVkPoRl6gTvD
qLsejioFbCXhwjct5p5LGZSk3M204mM2PaFYK2EbwpowJ08tyxJdj5p12huYkto9
bfPVZHLpKsg5Ju9r7dPq7QuCJumNakdMecJmRLHepZ2vuxxiRbyAFYTIKuRHC6Ug
dLxvTPIPcM6ysgvC4QO6+tNO0HHwTlYfrm0UJVbrE07h9DbzRiZKjU0DOCFCMYt5
5MEvqBluFXCzX2gg+NiYBwr/4Lj6dEdRPAx434thmot0oJQeTCQJGHg0829bVw4U
aHRTgBpZMuxsXDEJgEnevv4Gs73DAH2k2TwLlbXZlc0vzoKyTGbI+fI5wyJ/VRsU
hwOikpTB+frtcTl0f9+ZUc82ka0PmgAlKt6+wc/HE1koS5/NObxN8hjjJzfkU6xH
Hhra8NEO4CJOjgF2Nq85EZY3PHS+spbJdLXRo0GP1yyo644IfJSSbJN1kyuIE3MP
C4iFFvRDEW2Si2MY4y+p7b2H1UoavZk0gzUVf5Uw1PD5vqtN3pQMUNIfBrpife1u
zq8i6q5Q4zgkXJiWbnFWFrJw+C2GRiJsIqfjt6WCZg/p/yhBPwaQHAl5fjs5bXVe
DSzgrU7AbxelbCAe70ccV+4lIgXAUvXc6Nb9AWsR4Nae9yjDSPsSkh1/pSsQl21S
OOgdt0KATM2UHHUEfPyp9mIVqIhaGOmlWNkAcH7HDzYVocak/8lDFZrI0uYsngj2
JrUCcXfO6HD2eyXE2aH9/d4+6HzzCTOr0g8dqmsMVUFRVn4M56jM9NypmqpQhaFo
QaFM2LD6Rc9/ywQMpr8TZj5csFjsIZSIg6pvT8nLmI2eaC6DYWKZiymjBtM6fztO
cI3SsuarRxdaD1d2YpzFf86vMZ6HNH5+5T3Dgs9iVqhI82e6yMeGjVEuE4Bd+mED
wwXfXj4X9nYswos5EeBi9HqRhyjZSOqn+mpwG51eb0k0jzXOI8FgMlIXKT47XBkB
7hN/t8HEyfJ9JZw3KY/3bmfjVycptvC6vxZEj2k62zS1dbH+1Vf2MAp1x8DehwYr
OM9YIisK1km6Fx5UfqJrcuLV6u8lpCg22rS4386ig6zMZ6uwvGEa4wgVSYOyHj64
5iBhbJwngAWhJmkAo2Dgjp0piBJNvKXzFT/9oyUdMqmTss4o6V+g3iHk5wmfHnQk
c45efzH6+ZCqtWrCrJtZZIip6Gbnn+F+L+3XTGFl7VhU1vKcKri60K1yqgSBTXXg
V3O0mDXTFnG2r4zWBnDppU7EvJW6fixFrEF1Ia5ICX1oEZie2bM8pTlii7+H0Y2z
IZ2pqc5Ii3hVF9B2NbIq3mf7CdwkRdQldF9W/+IEHnpwfekBobOsiSr6k6OToaUR
DV9F6e6SeXRyQK+BbtaXPfZZEzw6pui6StlEElaW9s56ki4Er42HS3p6f/8ULySC
UiX488pE7Qelluy3611DDAREOtRXpGQHB1Abc30K2xjI4+LATE3ESkCNjt0OQoiA
3BHuKfBqersM1wjD7CKUHFmWB5I+Sum9JrO166NufUezPawVDOjPwsOeb1/N3o5H
NtwvSRdvR+fOK/WYHkA0lQmreeFbq2hCO+AYkPWmNVOCOKg+ZzmbA3MyjA1PqCgB
33d3h4KbPTF5u0apscH1dLiWMG3BHGDP3TmG/JvBv8ZxtiXQD6RCe4m4FtcYMuBG
pzeume+9UHVdUHr7cQOgdrdGicb3GQF4w4G+7SF9cYF+2llBInErLUGjcfC6bBwJ
/HoLPn3IBAcEMxzQ5n6tcEFtvvIpuwiVaA6XGcMHbJxAFIPPhH1uvXeUvc5boSE1
xosfEW+c805Kd0YpIcIl7t7Z38LhzCZO+EIZ2z33L2sDJdrW5Wz01bNE4WTegmch
UWIun1jCYHNuFsQhxe65dFCWfljzS1zD/u/h+vCG+qrLdiRNIG46esYvM1ATzMfm
VBNSQysedWz7rw/Xa5q8hssJzkiwQxAn2WpAxX8P5NqLqtkMAKethwcexzTYDcQ3
u2+qo7t7/YLfzotf2AkdDtcN7DQRDVZsOLWxaIO6Ko4253J+8CQfhLQj20xKKzyv
VJ2oq9DhIotqfDcyBN0L+8Jl9IB6C25m1SylFEOLOT6t/qv3oOAyW0NF7OX0mutk
UPVByIHdLH6qMjFiAVcy4LpHG4sTurPf3XPLcxX2l77TurqanX/MhjN52rgaQhzH
7yy8mM3cpteza0ouEZ0YMQUfiBn7qOqdjafoU/0GGamXThigXJR5zsHE2nA+ZH5R
XkOw8QdRRy2sUPNeWJfvd5XE1jAcSxtLHucbREqEGn+Ssx6VJuYPx1NiBy9Sltn8
MHVSqtEtMAHXHUmsmhNtrU55/xWdFqpKki6qMGCwdWCT4a9IGqY2YBUd64VH/cL5
Dcqge1h6sdJB4Rbks+TsU3LiaNsnHeiCgeUFljRU5ZRMNqJ0oZlFxMZ7TV/8SgKA
+A0kp3ouYcimbTq5F9ERImzF6+8x2jpYJ/M/cw8Kvgdj5V1a0n4XaG6sFx5W5cEN
L6Z2jV8EdQZbK7vxi3g8p+nyJ37zEK9biGcRuV/V4kCBfuECSatTsMhzcy3jYGn5
Bhd33gydnhlbblDZgPJ8G3bKrr8J8Bs7Iye23eaIK2TFt+pHvqN3VC93s++YarLo
vEmjg53t63XFBl6W5AoA6ezXVWgbS50lMcDQaPl5EDqMCUf+dqPQslUNdBQW+hfy
EyQExwRNrdHT23yoaMFKJPC94+6NvN68ZDzNWfV/NcheAizsMlIdHuq5/OpJushz
yvfRUXk6/rxRtAO2k17vWq7364slclK2n98Qey14LSXFF3HJEH/Tg+fYqvFBRJ1h
tvvGXZVE+OXOuHeVTYJx9KKGmYy5DOoBH8Lhqc08TjLm70lwoVx0M6RJFZXCuIyW
d+sUHj9z2jEkBktSa87y8f04S42JT48vim1ZFIhYHMupZa5+1dxoTajatSi6Qhsi
dEZ98kFo6A8RH/r991ogcVMGP87lDFuVHv4cnfK/y/vmejvqTKFFB5bIyFOJwYJy
P8+pYqf8mX8rxE9t6FIOQoas9oi/4p5QlkfNhFXBg6N13I7VwWAzT7FEWeFmdf6M
PzXpmMMykcyEhuL8VIIoIxvz+qqAypaKAHs18MuztR197i7YQOqQZLGOR7AcaxiR
ZrN52SBTyqljnAM9fBafG25JN729S/RaZMt17hGX/pU02KX4J3tUSicEjijO5VZn
ssMrrJVyUxRzO6XJET4S/8VfHJEb+YHaqZD4YTjVwpOYK+JgN3bNVFYAQYgUYKSP
q51g5PAjIw/aSy8k/mivSxj/998Jp1uZNiVpF/37RthHCWjS0pmfYiaG7hKV9I6F
kO7Ue1oZIfCzwHcarurJcXjWWY17eE9cFz0QiwKo6mql90ZBmHH01LeCjp7VHiqf
A+6hqsRsKFw5AQ+PmbYXhwVTGJcZQDIZT8N/mb6yDh6UKKqvOwtHBGPOMspL34uJ
Yfae3Rg7ZN3x/9iAKjAW/H5ELXhyoSsu8C8PicWeUCsbPhgzXXxz81+otDDbl7NX
95sSkFuBHGj67O7nLhe2eD0WgQ+Q/OonO4qmW+v0vBrBqx/OH1XHkKNUw71a12+Y
fqglKE1jxsSlCo1/+WjWMIl0sL2lebkXX1hYb60vcPoOfxVd9sEoDfeVgkKAMZqJ
dhVEyLivnyVdNgEBdUDh6UuLkLQPJCxG0ZkhS7CGNl6B7tMVmwFXFCUh+CyOHpGr
q4YsFDJt5bT7EG6IpAfI/AK7FNkqq5wqByhS4FXYe66Q+7kkMJsSz8KMQvyrZCm7
`pragma protect end_protected
