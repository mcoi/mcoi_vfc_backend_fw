// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:39 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
HSIQ6K7+KliSoh+BI8VuK/C7GYGvivT2C5DGCtyIVKXCOitmnrLXmhaEqNnMxdaX
z4dP7JiBOVEMKj+NxTMDSGF2Vl84SFCSFtfMto8OgHkMruDmkOrJtLMc0Rn8hc1Y
c7yDCnwwo9xwrMnTTRS9/MMGdWeupDVO7aUWeTNdAt4=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 1728)
6mHjBf4N8DcBEZi8ew6Vl0QofQwDzdb0A0v0iw8X8JUwdCWAJb+vy5PSF6l6f93C
gsGzTv4fH1umy8UvuxtShb2e/6KtOtxAkLovje3mR/0Z32+SU3JhUbOeunTllgDA
elsSuCUEEZEkP5e4CTTwlLI3kVVSEsscvClyPmn+gtKMVYd968c+XNP/1rhSn20W
7zG45MMbAnhJGiW1HGqr5KMcbnxHw+LGKMb/9k1SlRslQH10PrYScksrnBujvSHZ
wC9L4ZZ65Dei3B/oFZz0QRKd+PeSPB+3UlLzAC3v+UPsxw5zYKuyPuwAaznSt3Mm
O6S31Q9vChtj35g3hsFr04j0ND1SzrS3bkiMuPo2N8cIk2++ehBkcC0APTZXOq+q
6xH2AHkqvNIljvehNQzd6+9aiMW+o4NHYdrb9/TECf1PIhPpMf7NzDuauTDCLqUA
xr35Lr3QUjzRdqHAmEqv6N/kBIPTQ4mAgrijWmce/GnRph79qO1DwUdUyoWr/DkB
29v9DUcWtSlaLu7yh6Cylh7aPj0q7ixOOxAUajvfTYSRFDDlzn485CPRshbc3gxm
ibJqLJO0abPmRNlB71GJA7qnnCFeE2p8im0Ct9OpDR014juoaMdeYFu2Klz++DIs
6sOkP0qQ/BJIHYmgJiHxBlpTLhhM1jy3Fwk8Gs1ROhDgdPoupNDLcDB69Pzxac5D
a6PRTdSPlk29Ec9zGOBpbgo3uIrvg6LHVX3OFOgvoICYVN3bM9qO4v5igt/5SPAM
YZXd2DwxOoNC0YyihBZ944rwPU2LwFgrFLERF+ZX3TzxbkyzKD5sZpZuaeVDrdnm
jDEX/CBh2dpDwXuOjRinMEAzTLjZTLNdTh4XMm/CZqVxEknVIOfduoXR7DA3PKYv
qPZtUtDblJ3Bbl8XFDc6dQCL+rKDCCbTkNHPv4428/8duE0VYhQ3mMtC0znpJSs4
c3vYyWxf6Zg9e6yqX5gtEYFPwF3g2tpQjmW0On49c5xD/2knVD9+q8s7APhA7CT2
Q7Sw1gorbIJIRD5laoHP2VF0G20qORxvZNjnymJzjN3Ieb0Tp6keQr9neLbB3dWv
C7l42qrkdEeyx0+nXA2NFfBiZwnmo+Toemm7RH/BMeG2SYyK/ykivATBqxUh8+NY
SyFTse9aCugjMCjtysb0MWFhDSwjg8e3AYegiKtvICT1KelQzdslJVpZs7NH+knX
IGUj3x/JUbzb25jLWZkGYz/KV79zOVEtRoukl/vPkkpfd4qlioFroEu+66mkHnbm
8fQmm3rItJAHH1IUR9BJQ7jA0qD1fjq616tz9gtJCgu7Uv4Wv4s7ZBXwu6PeaiAx
l+8Kjs6JHfid0XC8EFwG/fgmvNd7BAjPbH6+UPgXfpZDhMuQSdbUQBvHM44E4H4U
WlqMLvf+zS8egUY4b0jfunvNMoBNEoMJmrpqwrSNNEuUZFzQgQrlUCDAe6LOzsBC
8y6pnbxgzeNpnrhkStDQvjf1jujAnSGixmwyoPVpC+ozWW3dZ121CK06WojLbbMA
sWN7C5uFgqilX1a8kl4fxlfjE59GgdriFkklBxpsfOum/medxnKUsW1cuGeC95rt
pQJyZSRV6/mWRGDtyxD4VdWU80aVTAXTAEc1VamZmXbN0U85D25CZzMeSICZxj11
v+zMTK07YHThZX2UyfT/IRB4IvA4NGOy3EuoRX9g63ltuztPJoiymyyUtO6wPOVV
1m0hqYE+eMtR8/F7GhLbBwZDuDxNbpvg+r8adSMpRLyWAXZHaNAlWfrrqrqo/own
PnFdjEBvhm6Q/eWGSggMc3DFDngDMy5RC1NCJpjbsafsaBMNgBX3pB6Co7uS0GtG
h5UPHOm6c+FwlBlvddRuxnDDK3aRRK6kEKnr6EqiowkhmZKusmsegR5dBBlLCR2y
pK1T7k1MTpFkkDfUQ9og1qG0kE2lWeIvP0Mk75Wufq5c6JQoS8HyThEwBM5VHeei
p7bWyfGQ19OSpvRStHrvoLEu8+mnmKKmQCzZ4Pn2h1U2U6WUm1T4tuWiQ+0N4oUM
ZDgRoxS+f4TiX5+8eJ5K5CSNfTz1nfAv112ctA2UUiCunphsKtc4Ze+DKDu+8lUU
lyMEUk67LBwY6vc+BweooNXRTwJUGMwJwHSa3lVfhzCl1RgUVHbBm2FmTzs2P3UY
B8svYeoOtbVXcnJuJl1aqkHNivHz4t8Ki/OSDPagrrlMuWz3H5BKqtmjZhzBkst+
eTtEZL7dAPtUqfj/nUOpcUo3AsBL9YtlL1HUUO1jNAWtBa9gCIgIINWwrJc3NWDk
`pragma protect end_protected
