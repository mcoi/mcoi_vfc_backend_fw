// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:24 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
hxcbfCpYSkoCdgY/nQhcLLKhC14t3vlSaXqvKQU94KmT1FvJkAiEz7XVExq8lV8/
h/z0Ob53bwLYLTJlKJmkOKhsuoVPtfD8iD+U/ueuEURKAtOEwDkNow/OWMhVbFrV
Zwes78rcMxv/ib2BLNVYegaHzapxzXrlO2WAVY37DAo=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 5456)
2zU4SqwUp/XaWH2E3hQSDCBoj/H90YGqmOWzgHcIBxvvYKLMB7MDrGfgtLmCHERH
AEtGlKlHv4bweFMUK5pJKoZfEMuqB/CgExGGEo0JyDY/cav3cV/uIR6CAF/iSXdS
zikHHYUx95eQt2ruSJQUzgZJObqZoqutAvm7JkP3MxGh5x04E9zMnLjARflxL9Yx
H+W/Ul77mVVT7MUTO20b7lTNLiWu5mKXqposrmUG3ZHkzpeb3sLATSYoFDfaX1Cx
8qK9Y+iPcYQPTtxOYGTOj6njWurQ5q24fp4VqXPv85lMJ9yBVqm/uRiA+tBRtE9c
AczfW5u/qferOA41nXPTWBCBFbBGeSz7f7yPGdNASDfzVle8+/Q3u6K9y7s5x57y
q3wd55FxvZeP7oF2p+RqpAT0/qATnpgW7by6v9YMBsiWHWlXoJkx20cO2EGZbEGQ
evh8hzJYKFvCPRqTSLDfWmqbPwf8T39G7WCcchPr7mVdHsi4wnFJxRXKbyQH1N1W
n948FSggx4uz758+6/0n6XI8oe5IIaV+beDX6JmBG8MRhZrniaMl3tPs7+pkzOsT
HFs171irj/tUeCYwkjdQxIjkbSqVo2IkNz7fYVYEo+wEuKpocuDr+90m5mleXoIV
FYuqg/O9I74JCDyQUdLogmwyOGeIccQHNnulr+5HGk/V8AA2xS6dIOTEx2k8KMGO
CWpJWQYx4kB9qpZMxyVZTT1EzE3YLkTS7AHmyS24mSPub0IuPqnA24/qJ+AzN1m8
K/wsbANyk609zZxQ1YWdq+nT6GI2QWdMm9GQq2Wi21NR/9mp4PnnZ49RBw6Nj/tJ
soRAjsOvhsvTuwZwDdd+09+xvXV7dRPcMgxpWsIWUVMpqgGhNcGZZ1mVBC3jF+jo
7EXmVOdUKBml/jdvmtiupJ4LFiwIg4kjvpUKnh+o9I5diNiWQN6sCVrgv6h7E7kn
FrAkIwzLk0lzXLGitVmDMWh28ngEBuT6Z12jpHqi5+cSiBbNAvSp6YsTJOFXvONt
MjhzzUuMFPvJeVz4AAjY+n1op7ni+LXEFsZ2jW+5F0xec6vYwfpQtMgMceXR/red
EQ+ZU/w7IMAb3Px77V7UuTxeE9WxY4ENfBUxPyD7q04ezaiehXpHTZTiMQCRaIx9
/JEF2GxySWLcPheAXLOkJZgoi6c0m5QzZFkaNP7i89Yhf3TPNMevthavldisjiaB
pu3gQJyGNXUIgCCBQ3wNKCgeurSEeIQ7RkbApQv7+LLnumk0u0LUXAeQvt+11VFD
mm3wrJLf6b0sP6+X9CYJhk+VTws0oiW0HgUMiEewHb7f7H+YMqOpvayZx7biEIF0
ba41aYUhn9udGYRnkOWe0O+xeMjKYvqlb6hyNZnxdT73c7LF8l+RQELGbjjShpm9
6Rr9mzDqI53e0aWCh4gp1VuPXs7LYSR+h1/iStT3abIC1HS84rjWhATVrt3DSGzo
XFyPi/YrocMdYdzWG5/l3SQIJ5sRM+banGAk3tPbJk8GkVJ12csORvhCGxgCjrSK
GV/han60Vs7RCjmP9e4ppTbFF/p0LK8yw/7lj4uskpPzpNM54yTo8bQL4DrkkOPM
cr6r/S5ML5I///LjUrbv0HguXNk6zgvUumP9FEDubBVe0LBmMct7HqjujCJkLfaI
SKoW5iGuWi927j2HKcD+XC41Xwwm/Ln57ZMUs+1sseJvIlveozzXpjZrSBqX3b2q
jPbj1qoj/3NMZ6hpn4Am3j/FY/BRnozsr4muQ0nlGK3T6qJ/gPDCK7QsYION3KhD
hKZPQ6jkuUVlepYrCr2hTjf2GH906Td1xM1hvYgOdv2wHJzBnppzXLN8P9M/uxnD
rslL51Rg+/9zqv/PeIp8W2wPEaiqSas3XHlpVmeFS6lgWpWDG1MnHLruuIQ0Yw6X
sk5pW+RpoTKIU1GVXiNI35STREPuiHJLYKAWmwMXY8S3hLCfgGQu3KPQsUD85eBq
7fRHlOrltCisllOleZdubCuSjEDaSFAjpE4YnP3+0JB1MVSlL5Caw9a9nqZS5Ip6
jKpTPwt719DlaSMXeMDU49iyhOdTdqRzNTtuYce8b1p5+JlzaFistW9uLk7480uB
quWua8nppE+mYFqSbGoL4vWXG+2vzL3D5xJZYw9MKPpGK73nfUbnMrobeBTrI6e9
pqEDzh1nN5I+FQJjvHMG+nEmPFdrzuFHljxWhTznY2AxP4qRohJU99Le0jTgUFme
v2tAvZUKGFj3Hiow/YY21duIESBeBrWzTtw1G/ecYvONGipH/3GmeQZlA5M+wrLA
2vN+4MdC2q+bsjWhz4XJDsFhEII+ZT2lXwCfr2R79dNj2EjOFM3QU4P6uY+qGtAP
E2a8LfiHVftcT2LNxIsUsfTEHjF8MoZdH2ZLQydJ4x3/RHSBltmxqwfSMt4jk00u
Kn+jLs6LJc+Mh8a/pw+n2LZBysopx2zIWDJBiQNWY/byJ5tfnKEaB8sOFqUa8kOq
BtjJG/l9+o26eP7dCv/S+DvGbVXieX4PNi+TwCyM0rFl36I1VUPuiXc0u8bGrH15
WI2UWdMLSBDm1A1zU4F2JmlESGeS9CiVtuqYo14inVXNe0EporKaMi23/jcNWW9w
moh92O8wPuwr07cI8i/edCj0oaind+JjWnR9uEub98Ec/s12LQKEDc4y8zloEQR+
z1FRb5A2MrKNgasna6Fs8d/unjJAvyjAi7WJZudKdGPMRammGwHpmIKkSl9wZHf4
PPuSa/frVhm4P3Ez6T+sSUznwF88YVVYHCG7VDmEc+LoDJiUIYfTLZPVn6DmDqcq
xJgqXBV3gyu+7UaEufxcIYB3MbP/usz3sXlLDPlIYJ7ry8PKxFMrPc32Pf8LAjW9
ltakCpBXqC1ffTQ4clrgpYswyEPVvDIS5OwpXLYCeAwhBjS6gDzqmUJmJw95JZry
TKvlqJvwrSUHityWNG0+wKXpMO2s7ZpIgaL11PlZq6ypYlZwNS3eXkHG6NcjFN/u
EUHlynfVTQLLNmEy2rF6pdWDTbE6IqwlAaSVM9OMCZHJAC9mUSpTvS4JQn7ScbFZ
LFRZyJuD5631zwyFBRTQAaVZY/IN7G4WlxrBbh2WbI/y/pnpl3hlxkUmjTcE2vuh
7qB7oDy3StgXmz18fPH4QqBdXaQQgIZXRXJeFo6UZ8fL9HJ8VlYtv88pBhDxjHOK
DbN+olmLOz6MPS2nwtLFtL0cTFIYXV7bBk7JR0M2txtceM17bkYagu3Pl4lcj6Fk
oBuaXuTNWR47dk8bGjiaGX+09fRuQrfP/AZMh3qS0B3xKkBDbeExYea8aAmyBHQA
b7qScxa9X6f8K13CI29AoSKkaK7sVE7QaTdda9H2aHMuIqhSD/g2Buiq9ugImRsO
B9BH0+blXEu2dSWAzkt2C8tfL/75UP896a8qOhrVNS5gBbIYRXwIFK5AmEUxeGKO
U14b/Te8gIy2z4bIqMjWnsN+7/zZ7SujpcHCsY6jU2gJ33q8JxYxiBqGX5v6wEvY
G7sa+/dOB8a9aau06LDFcFsLAGyxJsFpTGUsPE79MGGikKP4HuIsB0V119ALZz02
FyaRI0G2Nvpr7i2opFk3ZVooDclCj4THimOvIskt6PpO9iudU1pMzVpbWeNFpvBe
pF9UEdkLHITtIXvSSO3qM8PaMGNk4jg1Jyikp/Ee5F7y+brYCnKbbkIOdDNEqEBM
aaUwmnXKp/ZyqC2yNhfpTn75/5puhif23W45lFRI2Vv/b5Wb5eH+daXIactzGC4m
rMuUzswSVz/P9+Lal4SaPNGXCOPa02nlnGVraHbNLmVtgHaLt2y8m2vnGlYrVoHQ
yrfqRp++syC8QEbu91L6XxJ14h/TKym5r7LhqDOgh+vJIlp11cpMEmOoKuBN3XuM
VVvNeQKsSgXxOoMvtrbo9wqngC1Mg1uRkoEsKjcTgtG5PIVNdmGRxMIF+0eg4r4k
5qj0U/L6HUDrG3Eo88DV3tG8iYZ9ZhAAsb0fAHYALUOcEWwsqpE7TmyRxe+EkuId
ZwGZwajcGANo9bqPIlFabPWZ7gUjlCSbsiexPnavgPu+gm7k3kdS8LSZWEBFPdJd
+44SNCVFE6lAMvvG/HdeTOMBBvhcQId8rifyEaRWKWAK64wocrLLO90pHO+n86xU
7BQfMKXPLG3BYpJVaYZ80MppkTryIXNChdJjd6cxLWEdoaKvPicpRzEe3TcVEfj4
FzWQE/C6c0eHuj8yDOR9zKaum7ackr5T7YgItOrGrKdmgHne8MOYbTB7FTgvq+aD
8u1h5bNikRakbb6txgbx7eJkFgmvD1lSy/s+919VUzd0/VtlEw3lbYD7ceyAzhxM
mVbEgsMoevRVN+0Aoe0xn78zCOORqVZZDyMxOcsFju3JK2j8Q19CdzR+t+FwD4Is
x2KbAZMkAkzw9hCbpSeGxtILLnh8XzqfesN0o2gaivo20VgNMHgZKl07gyL+dI8y
SNtDvqTa4t8pHNNvXfDDMOBRiNlYqFVA0eEhhxrZWFfq+0ck0FoF45yU1UBr0NWd
+TvES5zo4vPAQBhmizRpO1dLcUFf/DwGPvoZBWhtmP7KJQg6zDWX6IkipnbTMdv+
qYNt/NkFcseMUdmdCKwFq/F1Xop8baPBkXfJkIcFelm7/IFZoeqfeOvipTdIfqRP
iE3hxRpKcIuKSXv6GkrbHG+HwyXWBFTFu3Pg3IemzYwQ5OgIYHCJoyP8J8mwoSEF
Aw6kkofC9O2rkn7qeTz3cNO8B4r2oHjzDJQsPIjqvLLfPIgDIn4JyP8/LLTcuGok
HtgnFDlTy6/9vGFwDBRm3bW6EKh7OMc2MqgIZ0729J5p3VgE1bbW69lwsy2ZZ8HC
IrkzpYQQlKwUqytmh/RyMf3pBDJz2vM8WbIF10oYbA0IAyZglf30iiz1gVgmMoJI
ObxmGmrIZrDnjv5nw/uyVEtcm4PSm1Fx8lWPugrrLcVr4jw1cD62kUNKNfo3GXZT
R5kUwh1Va5mF2CdpG7HBcftDIYEFoGtOapSX8A8qVJNP0BTx+txeH3sSKw0ly6IH
aAYYchD2zOpeqNTaxSsc91IiyimzXkVh/rxtTEPejv+mEsVWXL7JkCLeAFwishA4
RJJqTQhR+YMt6HAvomcKws7t1lJKr0BGXB79IGHA6pIQ4w1sZ/dHDN4ndHDlCv8m
0UEI4OaNSqJpsko7Dgm2kHbZmbsiBAvd56u2KY5ilVZiOH3moNbVlju2uWlUBtcZ
8tb+cgrCoCHOHJ1q82WATDmbstvwscVl2T+AU+9RUe/cFCwgjlbEmU9cOkE+RzD2
XPiVKKia3IXe14km5IqJJFg0mxpfeN9xjXpqYs3LLGKF3piyCZIe5EmujtSo+XO9
3hOzegQwX3Eg4VEX+hYWKJl1qsF0r//aih8nAnZINcrc1UnsdLlnM6TW+yiC0j+3
2rkmfVYZzfuGK5deymbgbptsn1XYtgs0N3zWiwFQxUwx88PlSGVYwDNqJT7Rb/He
bH4GYWc6pvLnnCm6hbxhp7tTc0X2UkOU1b9/RX9BjSUVIltTwYA1/uXAtpaJxuOP
IdQE/ENM0f5YDB1V+DQOTeVheaYyaeHo0YJEUJntrC0T04awOsgTmyXWK8Yd/2pg
j5OVW3yzaFP0HVvLEYIa0GlGRHAQTZW9njZOUp7OKH4kjwVndyTLjwDRyXMH+j23
n1zYL/SWGcLJFR4oKNfmm5qrqGsLrzcXawLLg5tUgijIdyRI3aAoqjBagQH7NMAo
gRYtUa8qzwnc1oVVhDtp30sJhNNV/vI2mHH4fdkyen11AavU7N1sQwVn4t9z1MqF
hpSOZLHfjr5+mQmk6kuGkvqxf/+mKsTZH+Nb3Ym9lIs+ZNGa4Q4qOJG0Jxgzt8PY
O3wpBEI4ihlcZHOwWvhQWxQVL0imL+QiM3UCTKc5q9UxCnLwAggS+nfOVC5vIGOF
MJhbBhsig2Aoiz79DGFFbXj0xXpSax47T4NTzEoVM+EoDCdN6dJQzRL0XwhxP9PY
wCZ8ADrKGBtzJ/+1WZ7fgTiv/1RNLgP8kaMXAXZc5ZffGlryNUAfqdJ8v1hsf+LH
ieOierkUA1fnKDzo6OqnKOq4eM9T3NNqY3eN5bt1vjZDQQzFMq+FHlMQ96Bn/KMi
PCQJTHSKwoOn3N48iFBIGnna1Wh4rncl6d232umSxUNA/4Tv+sZsDjhvahUKqQF0
k7hiDmjJFYz8yrABxFkfwuerW3+0ZBojJeQnhOUbC7zu8Mui6dpSoIOmEUleWWW+
pqYsM0XnCiZ/2MLByFoUbSd2kCwQF89C6qBtKeMYP3GGFwUZmDRXz2bdT4V2vOXV
RThckJkSUzw6pBzheFnRfaw2tiOdTtKMoB5B8w6sJcJP5iKA9dDGh8qXEG1P8PYZ
Gr7hRXvnNa+LwBxCRP7oBpwz1iehS4/SHGM/8+SOpQJg1hIOVAj1AuNVDxBU1ceD
NQSGJqxXHZRsgAnOM76r0XkqgbyPKxsq5bRGSWXtj5l4oUHspz1tV07jzlBL6nzJ
r7AlvlLUasiD9Gzw/SNT3V4Ny5qv+Pp8YwfBgO81FK92wXlzsR+732svjdfaywAT
KubhyK15S7hMI81N57UWWKroai81CSC10fY5wLN6bHMqQQo0k0zpVzWWOWAPXJR+
Bf2dk6s2FO2l56BFx48jNK/dZ4g4r3PeABFb1osXSa787Nj0ZLSzNgpn9QtAWU0W
Abt258G/g0B0Ei0Hvoj2QnZuYaCMkuaSXCXzkG17crj0/f+gCdjdlFiyabDVqCgj
xEolCupav2pHFq+QlPDPM+E8ugA+ky+7jewg8coR2Mdsv5cROeW2V65BliT27IZx
/s7GNmBtlMrlFc+qj3EgJBaXNLC8A2+6p35+D+PdWckfUzutXp7NkfOlclzuuKs7
QGop0NFN4odkQ8eUqlHTZCDJnBmN+4npuqeNHmvN1p3eQ7+MMlHOCYUtJeo4Tr62
UvuvkohlEaBqYBgVhkebgchnnZTb1pbRsdozBJ6SxrJynDAwa0JqANrhMI4PQ2N6
nl38rqn6lp4LbX5qv+hoY7t1I5HYJL04LEwn8EigFEG+gARu6UOTKKznTeudq51e
MhXoS11OLHNz/Qyfsd1bC7gYs5rQBsP+4jFjijAaY95gE4sjYqd2NVg4mbZSy+7O
OcgmnfIQBD3H8r4y2RyPDrIvNnZE6Nwg939RnOp1MCpno2BfHNfrsNLF4nHIycZk
nx946rIyoa2m63zsBjqELH12kguBenWUhiIIAOKHGQg=
`pragma protect end_protected
