// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:24 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
QDvtKRuqZLNdM3r0MMOOWVCQuhQGaZY759tMyZMZmAKASrrGCVMbVh/XuZCmRXgu
PfQAnEbB2zaPPgK97Y5oWcgHW59zcuYpqFIw5WwMLVYlgKQXM7KTnXPM9zuhVNgr
HmHDriuHMH3qj83FHqrCMQKrxPKP3ODrB5mL24vK/PA=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 11072)
zaFljWMdJLrIqYqBID4UUd+SgAwdrTRrXp52QY1OzqJskTj9usiteinhgSyGJuEO
bQ7pA7n1Lg9Qegty4Ok9U4wT1+meQBfvscttcHdRkNMciQZZpHsq+F5TrOMMtf5W
0NP5/40A7URt0gdQ/m7bonfhbyBPTcJMKkA3QftPYB6/IqJWqjTLxHgEH3wRtqJw
FdtJHWu9F+ZSspxwbHxzUT4WbfwiMQP/DPSHyd0B0rcbrl+TxnevQxe38f7purKt
GtCpp5b+ef9UbY84UKrHiToATbzVYyBEDs6tHZAnf/diqTckF22su+Kt8o+P6vNh
eAM3boe3dyVxJdNH3SWHsOA1qtjzYwG8miwbStRslK6g8gyC6cGYyGbuykatzulI
yEfqfSdrgxWF6NO28dzYxeNJmazP27uWEvHafpLnvcfyM2Nu9f+Ernpr7ypz3jYp
fIXMsrJIKhvzYJM7xdSPausx4QMCMqI+cq/YnCjB/hRKeZK20rowU5AiArVDsbe/
ybykG+kChj4Ik6H9qfp7zbd2DHRJ0ntipI5aiJnaggdDD/QeICSmr2virgx6BMY3
1BHxrlEavZbg2Il48CpJBoSxnEGJY2mGa2Rf1oOKRJip31iRZdkrYLmSEJdbue/f
biCp573WOs/JypD4H4IYC77RU35YAB9DvoJ9A1xIalaM6IjX/ao+fjyypUdXP2VY
/liVKVqia48Ih2n4klvAnp5VE7QQgvVBp7GDh1UI4jCZf76BCnX8hM0zSYz4/9AJ
ApFZNHIhi+EP1yxivTPiAtRnld9AkRLN+mrss5tejHb6YLsTNTNHuow/JkINqoDB
LpHTIiP4/Lmc86FoKJhMzgdgQBtRjZOZbPA/KArsNJN+mGjJQcTBN8Cx9eJsFlD7
wpXf7T126HqNy4AU6NmcY2vRmzJDncTMgsYZV/vdUGPQUfk5ck9uf3EvQNdRSR+s
A/g0XUuRrSQei77CAVEmatrOs++5RtWXVd0tTTKmgmXp6qldnfDFLNLp5ENsZMjP
5xq6X2Y90T9hOf/epN4ptbP6x8YdJIquLZw5kxl+pdQiIguLR5ssXIlf9zRa1pKH
tG4PvqnL8+j7l5g7puFXCRWJC+XtVdtppLSsgoqVj3T7NJ0kWDwfslSGnZmOj3/V
el5kcAFcMcaZuQnJ7NV7GjEfwMzjVMsFd41Po3UK2a1avca6kvaY3UjfBvG+IIgC
POrvfTxvhhaMkziB5FX82dQmFkvmAiRrFCq5zA7NivjwLY6swamSwPsuqrXiJC3Z
WbwxVJgGdHunLCJtffc6GU8hzFP+Ul08a6xAtTda8+3o0knbNbA3QAijUQh8jriG
El6h9T85F89vMfDWMu0HA/k1hVXV+NSCrhklYcVtp46I6KivrbjXuJ8FJDkxetw8
kr3IstuMGRQpm3pN5tD1i8vjlBPuF51tEreqBW24obUHWMRIVUw23pG2Oe95aTmj
3xzopDhXg76NNeBCdmpjPqUvWuenI2NACNINQBedzWn/sBO0SQG5KQaIPJFEAYSv
9wdN0ShJaUTgy4vBmVabx2VFJWahaZlU/mXmYqCTxfEGEU0h6G1wcgWgUtxgR962
rmHnk7gjbITq7d6e9hS3A2WEFFAs1VHmTsuoksn5KnxlTCF48BaT3zm9x/+Rjlv1
+tb3WG1LiKwvbzUd2Qrp7tvv5g1KeunTn7T+uWTqMpkUwW6JzfixsPJmi9aVlL5w
K/xiED1yzI8V0M9ndsgNjeQkG8IGBXar0dJS8vnLLHqV2jNHEhyYnjk9sb9FAz3O
sxx0DvKitZk2vSPJiYXQ9uGK74xEIRZTU3xnkh5Ds2tw/NpMsLTtSwsixfYHzJSh
UP+DtNE6F7HNVd5ByUhDh4K0HbVY997Hjb6KsNLWVgjC9+b7H/6cxUTrPxxzyYon
WYuFKXeVPAZ7k8ypqNgtb3KrDC0kHrnIP9Z1aDlbH8WsEk1VsFrxKYzM4F7vddno
sejL+FCkhZweCk6FXmoCeT4m1BfNzqalHBVKE1dULKrdVLkYXU8HJ62p/jB8c52H
UArvyOc+Ef9Mc93nphb2xy7xSkTYtcxilqWy+jHsFg+hrGi6R705Xm7vtDJ2y3QU
RUsTXeS4LyS0qDPwwx/q8TJB9cxYHwXAEYbTqyig5HVnKm0L/nYySwWH9Y9idU78
k5W2sP5mb+iGphEptAoFP01ux2Gl9glCGCgZbcBmEruR4KG0MwPck3rk42sbuumj
2BSWuR8XwiY2QwZv2LFQTq8VYwA3de9wl4uDrmvBnQzaPbbSHmLgInc5FqSG7Bi6
pobSNuwuTDZdQslb1BwtBvKSFHu/1/ftzGHhUAXeZKscLupvBx85e8WocihUyP+f
XZam57y7g6+zuXrDbOLARlNRQpnVH/mE0GzwQbVjzxqm+syyHLBkdToxCnb+WcyI
bcxQ3O6gCnm43PItGA88Vw1BXzTTJ5nsTDS+WyErCbJy5EVySanQlOzX4RvflpRz
vwraqJIHGgmaWF2bZKo3hoQ4Shq0pBlx8fAkkXV5eJ1yi+d0RZKGXXVYWZUArYIh
MD7HTTnDUGvr7Kv7qTIGcYb52cieF4uzcWWnSxh6agnpmqwmEc5C3qGzSjte8YaV
TIeWPOTtK9xmokXf8itqjmL1HUovk84+2IjCqbkZfc71Sy14EtvJaLgl6dbNZ+SN
PtuggPJPvx7ca7prJWUf1nfnQmL8lsfWJ5hEOEx68HhCAh5zwRsYWyGbNvQIdTTu
VoQD11E8mb8I6UyvFHJhyYGgESdQ83gQRNFiDLGpbrrVpSYmA75maEAL6+11f4JZ
Gzz1z1szWVI5uWed8l5FaYwfNzn6mgoHxc2KPs004eEHksCA3ZJknLqUA4ZmePG3
oadyjPdbfCjDOiWzPwnskMbEFr9jKJ/eu7iWr+hq+/w0fRS/lUe+4XIKLJ7uDLVR
n/PwPf9g3hhQRkKtmnMdbu0ZL56moIzGGpCvGKUhvB2DeJfZ5bgqvx88f9eceAKJ
wAF1lq6k6kyxewdkvq/YFFS8vIoCNOTe/vWVwwHHcLbZBdTOBgjF6AtUkcGrTNo3
00cGt5Sk2qeP9rkoEo5r+wbQ1mGvet6Glqx5zTh2rpI+ThRtDLRpcpnFFm7O3+TJ
SCv6+0CY7mVkXknAcOEofEi6YNgIR3tNjece5NX60pe+q5J4+yOTZ/Zy7fN/DMUJ
wXppwG7jK0O2Y1K9ClatUuNdzuLU0slGE4tF8+MC08jbUkWXUvSNIlUpdGITRDq1
SnVe8QFN8Q0PRaUZY9ukfJU1l1y31KmpIfDGWRTNZCcB1lpwOTW8uJ+XLwjpeekX
4zm0ndP9uuE55orAySG67SDlmy3yORY6x8lYc+i6GlqmEMYpoTPGvvGfR6/FYrxf
qzwD7WNR9UAT1WME5DD82uVVKSVvuo98HRu7EHRNeh2ZGA4JkIlum77IO/fZ8mAE
qnArl7GvSLympE+6Cl+bW17wUXzyPtSaFLRXnrTNilNDAh00YVU2yAZMsG+RMwIn
2c1OLa5sYbwrqfd2B3ZHeaukY5v6RsHdhWTKaS/6RmB3g/0UysNtQgOzq+kkD4lm
m45be5AoTdn1EDNbCKNrGO35cmd23Y7kIkT/lKPuv1PBArySe+2hgdibXTcohFuM
2N6sYZwvZVcBXnO29lW8/H5gUgJrGACOvlDbULxsNa2rKSk0/dLCCuBkiwbw1Rc9
JdImYqR7blpHd2ArhRyOnN8+o9qDFPtpRMAsGUGGrOl9gq4TN+V08eS1fi1Kss3E
Kt7fl8Ce7qP6+slvirsNgEHfVitH4aPW4NFyVqKSP/RnCom/+nwGDU6jFQ9i1V+g
hxb+9W4GRASpUti+TdnKjsu1ZgNGNsymUM4/Yp44pQYYQZ4O7lBW0h3iOTQ7lpRY
gMPwkQhvmSt23zQVA9xItTUlKmbdgvnHANzvCnvZfhimvq5UfUDaYu1VdnoRJcCh
jj2oZeA8jhX9IRa7xfOLvECgDdoGMJ9QISf2Ts3PKMtHYFB7vmB3HR+0pKN7kfsn
dlnSPU5BOMcKPoXXoBLySfmsDrejQwTua8YGvGEBFYHruUZUqI9bsDiOJZxxy9Vf
35QYaZVjXxS/grThglEJaxGBmxJwNIuQCXPAj/tTPoiTy5XHMugg2vKSKYLe9e6w
++75Vl5V4AMy6RQAVFamLe83lfNejSVpwqz9HPg2i3F76Ex/EO3MdvJ24x3F3OC2
9PsBfXbOeB0cdTVQx5l6VRW4mnSBv3IIU8YUHtIBGFQSCv64Omw+kKZxiUkymqrX
wuIkEnfKyxaKnMxhvaBp3rlyjiwi9LYJadtBhrajrUjYpbgpbZyXbMx6k6ivunty
mT+3QcAHCmrxls6fnwMYaiiUJUmZvFjPQEy4d6If+SrkVRaE1wtVxamO6724B4vp
FYgH/+l77m7GEHkpX4I8FuMNJvakcdaj1UCOU1m4XYJDAobdiVID5128By+2OB1B
D5L6eXV7hUlvpQuPX0lre7zhdVZ0hVO0GOCOWB9yKHALapOlZylRKFUdWr2V6g9r
KulmSlXEPAYCTlyy0s8kprSRLADxlSK1dbIn1sKHA7NkNywsJs6pCX4XKWqUj6Hm
CjiMz03VGu93QZDdvr2Rrejp6GRtHl4Uhh2cgPngnkETjM23PVhPu4rvDcof8z4p
l/9NWLX37rGMHTJTMxLzCyVZTs9jq+uAGe8cAuXacD3gbeKTYAHMpeydDxKwzyNJ
KSWo9viHIwoz00OYe3bvpFcH6q2MhSpk7A2uCNBbHRKpzhlGWr+Y3108SmtK11mw
YinGhxbByA8YPgi2GjVcCG0SUg9bfIM8Dp4aKp/PhbCZkDM5FGud7OniT0NwfZbZ
ZeHehEWUU6ls3PqvSN2x4IbMMVJbPCj69/WdxhBuOEt67O3HqtFVggRO5a62HAf4
UVjQDcKo2/C9ndNtB8KC1vHe4I0k2u0d532SBl0Y4pV1jESDTVwNrcNOokzJGMKg
Sg1uFSlMJFKORnqPVJUzZ6v96n9i9WiL3nAgZtuFyeN2OUH7WASq9+FMDxEg7dEG
5fw8ggm71NhJuCq9ocbnxWzj+fKpH4fkjYGhojPNQzxdnbxcqFGjBnK4+em5emQu
pHEuV3zTVRIxHCG0QQX7nPRPs2dRHf4m6Ah+Ar4aYiVp8XlDkC6KIO0msoUIbTzW
Wchg7vaWER6qRf2amnnlCjH3rWtMtWfEwJXOEs5GG0HQnvXz8UXPwC5MWJVh/Q5o
aY3GyirQRrUp9NBWex65zH15BoGU6Iz8Rsa+VDCGbkGHcxvdQtrS0oOMN3oVUEXm
Y9xfc9hJ3D8jmKwxOUqHWUNvqGnVzljyhsrAxBnNgmWzrj1WSD+DoePk3/rYYirz
VqaUEkTeVHL2Gch017VBvzlprUqMbgAzbc/bNojIQDTJGFzVHqKxzg8EolpHQFfl
ynheANCbb4VyLsWmVj5mxjuSLXYO6qE8tJj3T8RGQ9d8WtKJCzmWlepsYCsGsIpw
tQQJ7mYrNG8Ii9DK+MGbIAeyS5pafxXbAaBxjhp3fBsypj0ML72j1KfYui+AbefS
7IDJGcBvCcGstsDr0ZuxG9YbA8vwmk9P4O7TFUWpzKWPvHcXgd1AuWzvsH0NY5ri
6VFZfX/8PSIz2cRyfe8bM+e1p6JhN1QyJ0EPjTT2cjy/Z9lkjI0NYDZ557HTQ9Kc
rMTTTBw9zlk8gVsptA+K7J/favrcqM8smh1gKSqdRgfixdx4YRFQirBWy0h68KTY
BQsZDMFIT8T7ftLwHui20HK9ZQb8eZx6JpkHMDRZAgYWh85uRBM0FgqzBK9n3JTG
QOJ9I6mTHAeDLOLISeyBF/7EwFIb1umVLyEoCE+0PeodCvg8kxtkNgwpb1ONcbTP
QRv0xX3PyDzZUZmmoiPB/ikkMw/624tDQSielkhVFww9xIM9iYlviwJh725BEVWg
Z8duProk0PBr41+BCJG2LtvcouT5OP1k6ND4gkH55Er2iF6RhsBI6N/s1RF1BQQZ
wjql7ieKcTzSL/SG0sNF0ceWI8eIm0caPRu2oYIrSLnUSFG/yqt+pCwzIASFbCc2
dFn2WTBjKzoTuqIjbdrzVa7npSzt11W2SHC7DzsBxveMfF7sn3v1qqqtNzpRoMU8
+bBVfVn0F6yAIDOWZCuVfOsW1yyWCrcVCWz5MFz8xB5RHsS3WLhdAWXHnhgyAYOB
gVi5zSiJih2r/LQdRi44H8U56Devziv9yOMyEf6NDJB87QkyooIFAakUptIv6IXO
3tLt/ImAs6fvo24s0TbG9JNtpv7C5RX98J3InrJDoxCQfRZ7iCO7zv3iHLjgb48x
5cLw5XRScTQzxWx3Dtolr28vKcmtrl9qbwi/UBr+huAqRSRP10OXqKplUi11/3//
SF/J3B35ErBnsdV2eYh18nhWps20blODFnqwYDiOLm5wl/NQG7BLQ6pHtxPEhN1B
AhQrR6zaKC2w7wElyfdoqDdYM58oK0VBcD+mOiLxDy/LkaLcJ831EcbVwQ9ks1Jg
cI5sJ31fZcDO4l36a6QQI6+vz65TTPwyOqgZRBFaeQ21novJMo1nswD5oZFht5J8
amUHmv62Tug82mhJoF1RwwcLbRHvLSrqaUPKWIx8X0E6Qqx5KrPC2w7V1QlfpoAQ
vJVweFPdTIozGIRG7M31pUXUBuSBmn3ux7wraYySfOB/MOpto270ybqpR34VmPiG
lDQoK4zSJS3P/Jp87uRTVXxsqVCacX2kCfknFzLU+zc0Tedf1eCQgw462AgLzwgU
ygV6KCW45ZYa3BV1aNTb4k0fAARNFGXYxpqExEwkWuqyByfQ70mUkQu9QmlT5DDW
CgnGWLsxRVCwAJGQBIXdXOBINjfxQl2ysMUDczhU1SSjN4UF1q8KARt8j9n9S3Xk
1tpi0QXApkL0jG9fAY5HOx76hJo5deV/5LGkrtl3DEtvk9GF8WDn4kn2rDV0N+L8
qTAdejcp7/XboC8Ez4BO84FOl5ICtqObb0TE//nhw0H+sPkgwCI6wxOPV503hnje
URguf8ArHh5VHJaI1cIS/41TsGqiAXtp41+YJ/rGb90/gCnvL07HvOXfo4Ka6NAB
7q+srA0+9LJorRcyoYCv0/D4qNyd8dTjLnw+HRNtUdARFJlpLbUkGQ9RTim/D2Zo
GjQCwQGU/mYE/Qt2YrVdkqDeIwpZD0yV5DmKNdTGu6iE8rKlob1imao3xXJtcfTm
0aOIkc8MH9W2wx2AcRxVaDYJf956cu6G0oWot5eeg7cXu7lJjhARIpAv+wSNQw4d
s2Rc7gnD/IQRMiXtFXavAadtBrD00K5IMCsGaojvc7g00/247o8/Be/9Ba9ymfEg
WYNhMSSfGn5oNY7oPXp1DurKjEm+TIkreHNoo8//qV1JU487iR7qaLbGeGKiyt5a
yuiQ0X/0yn+fmTBhxH+90CJzN0xDfkJqbZWqsuycl5qnJSUGXdwyrg3LytFZzGRq
SmZDzkZWY/pE7kFRXXfIbHjfwj/EI9BfqdJfUYYXNY68Mz1dDoQmM/ay+VOuRQ0n
Q7GStM+eFJyiJP8C6eYzZEDyEZJs3xvLlAkqQyqUglVNgvN1WaNAJCa7xgsQM7d4
tTs0aWfSVkN7dct+3tQErDF8tjA4G/usutRlveK2x3cNRVi1Z+jE9yufvjGUOaf/
kfRxN9VUsC2EDxGuzzyecGsuNHFa68eA/DP8pLgHfcPOccoTt/6qboR1V2v1slNc
M6bep6QBxjiPtjkSGVDjJPiptMvI0MT/9BOHk9cwm56uku9E3igptrgu3Nsf1hcm
kjCBMwr+StZrUe0lacQoYnPe+4Ovm/w/bJrKFQTFWLsmhHkN+8VO5wH0t89OJiQB
+8gh7o3fRJtWDwODhyF979b8DiwTHfeWeknM5SctWNlmBVyCvZY05Did+ZliBzd1
GNv3x0r0F7SfTulbEf3CPXvCkemz6Xk7mbzRktVSSOgmgm7pUMGae1UlT3/Pw39Z
2hEH6t3C5ww++9ML8+flcMfiS3W8EkLMBLs7cw0JCJI85xq9oMqD6feKvPQxZjSt
pamcwZCFBJU/zxj2lk+DnjfnvZXCAgxfdfX498yp0XKEQ+1k/MyVDOANkK16S5vq
BupsplJyxKmjWzlDnq/rsVL8+CzNaqgnsfmn5nyQFC5rSovrONiQT6f7tZepsQWD
qis1jgfmCvcli5crqqUliJamIuSxnoc0/VoLvdMfffZo9GzPe/cJk5G4hA6GkuRn
v7xKv42Q/ttH6hlHeT1XmPFLjOc9B4P58fVhfXdGyqgYLpJzg5gpugCgG7WnRdhq
LxULglOcduOS6MudciZDESiToPBxjKFQfz3SQmHiDLYDR7vYu/8PrqziQa0NUBMG
Ko9pF/RmZSKh2xs2jR4oZRpFbRS84uD2BxxOjF/ysdUsvLDJ79+apKKAGDszn5Bg
tXRezhhetqnI8kaaRASfBwzR+D/Kng9Df79Bg8Bul3DLBFV5ZhjK0GUBGYttE6mt
02FOI6PZtPcBrasCZUEknJTX+YIWE3Wvy2xprHN/6Y7PN9SuaRISwuKxtIeM3MS0
gj7eZshH8pzwExwMSDDdmL6HK+FAzAdSVKxj1fC1KXpYwaqCT3FPRoHDHvnFQg9X
oGNQWx/9zZUUjSijJcd8tLNPCq57UNCd8riQ4DSWfy5C7wsP3GtByt28cCzZVBg5
A36B5s01l0YpRVq21NALCfGdUA1yv4bsPmPM9lokCF99++dh3IeMjRXxaftUhr+m
/2Cng15u0kMqZyxJKyjqFx2MjnquEvvYLZua9/q831QOxpo+09f4JpCIyiegPn1W
1uSU+Q4Ht1Ck31umjgxsVuxpGhtDwV8vOLREvB8yh1Ild3vJMmfuhD8CbotIQKTM
+IReKvSAngtsCLa/JrmTOPzQxrrQwhpK7KQ8jQGXI9XFDxv8nzS047gHRS6S/JsD
flru+kBp+CSFi7NX/qXWjRj+IP/abL2P4vcs4yFFjs9FjtiywoiWid2y0DKZgIUY
VZkIxIVUgJ2ejfdBHqpVYXymPEhAGu/ePdRlEoxwHnby8+wYgrfhTAS66XsIqpL3
UT+vlO5zcGHPaFOG5zi0fef2Ldk5yiq/+Rj1sTBfVPn4FRB8fIsFxqwK8W0ldNNN
v5dcrPrEgnKJzkP6hxNhVtCu3BVMAti+53GcT+vDmDAl3Okcbmg4BrmftcpIrkej
kCsP5RfhKM6tNHhBd5B6dNyx6m++PLKGoh84cQnfLLOM32fXPberUjoDGRTK06rs
ktNvYXnNDs7oo9+XhbYv9Ie92UiPT57tPRDX01UaLrgi2o7vMvNWkKWvclO5+YBa
HBx8l2b9feSRuD6euqPNi2/acBVBi5smu4UAnNgLpoTr8JNV8g57QXgRYqIU/dGI
0vOjqxAUS1d78WlYQ3hrSYptw7g6R7e/kLBGRDvQwF2SuaYnWT26WIIyjowq49IM
MxURd5Wg//J8zKXr/s9H7Ua3pjtyveHZrHI+y0kBSfruNJ7AU3lJuB3BC+jdsQZv
wBLJ7bnmG/7fGf+dgt5ksSnXB9la9AEAbgi+wOJhc7wsslzY+zJ+vVxAapQwJXc7
Fm0/Lb7r5EgSmh3AdDOe9EBMlbgg9/Y+4mHwMxjneSetPYx6/V2qCxm+HiHtnvQe
Ltb/3e5eI43gp7vV93FVmIkqujIl6Y3KC3NnM2651TLr38S3I/tfcpccxJSB6ASK
Yx22FtYWBJlLvSaj7V3riJMjW/4ijzmBgzcgjol+RqZ9X95MAxzpnLBmjzRQlzpM
1/njSU6VXS0i58a2/3OvC3wZb/KJOu9yzIkf5YmQxRltueQymKHzCN3QCed0qKJE
5+jRDRhRk44SPtREZGlOfpik8X7fTsF2t//YrsOSA7Vcp3m+SIKLrGh39SZXKNdF
ZJyPQIXZJKIGSzTmeadYN5fA5U1a5ESLP/bO1CQGHSGQ6D1eFOmr8W4fRore+8Zq
Alzde/Ax2YBmn01CIqrQuGcpbzmNjHDqcP4qX4TqDhqtXLNCbQn6LOT87Uqk8448
erBfwclTea7r4tVb7PZBjj/uEM+gq105CB2NdW+6Pj7hjYsS1nQVRV+X6VweFnb5
aefvKMJptAZpgW8Mg1/ErGCj6lt9PY4hdL/0kAwu2hP/+b+n+BqKelYsxKMapg3A
KIb/BKvIiPoThRWmz7sN5Bs+wU/+0fUR2hb9X63Be4bhkzII1UodKcNseTG5iVLK
5cYhxmaWg9S8WrKlQBkwNp6/jhHMcz6dZU9lZ0McN4fZEefdva4G4fHK32Cq4FNF
nPau/htPiCrElmQ1yOVS7VOUGQzEN5huz+SDM7pYh1Nyj2VTqlyOL1xoMsbuZXe1
CYi4Bz4bsQIurboV1LR4h+wZMlYzdVdxnWgIwIbBpQi73NW0DUGw7+pFunmGpk37
1vJl1bKrjyPTOKJxx4zBFBTn8kED3Fp2NKWswJiP4A+s83TuSXW8zSp/+Zau4Xwc
qKjJt3nDU9jbidhcBNO6pFpK+I5OYBegzRtMdvunUa9P2A6VVy2gdCsZln74bBJE
qFuubezxdGCujgc8u+KePHatRGrqBve4k4JOZm8LYpylAkqxc0fTiZuCH3bOXxiX
750XcOlTgXzDgJzD6MRyiUcIOO0ju8gKeogGsSB/JguRjTuhjoz770kd/1myPO6S
XIj+VQXM7X4d8aCPPfODoFXc1Gy8aoSvYwF6yBGOxM93eXRPyepa9y/GF9fGOsgJ
h+Uums9O3wi5X+SYRmcOxCnfT943wd05qocj2R/gTLc0vDBomoARhwjfWaXjRNBR
uyrl/BgJdUQYeGSXEy1Ob4iUMwJDi3iCl25ZIdbwdA/mNWM/27qSMB2kAFpWNyNG
15QLu6MC/0+GZU8vqmjmEOP/4QlydYWfVRGJRXjoDUUdXHAYfxbVmW5MuLXOnBxj
pGujB/TFLD7tlpgWzfp/oVpBnzksU9jFuUa9FT9tyU4bNhh1zTOS02DqQMgUB1b2
TDMcl7Q5AfG3Qp36j83rlV/dS+pnaCdu4BVt8gXqyHxQPRZdqCJT9sWAj+8jN7s9
CD60C1Ziw3gcOiSJmbdZpfhGsqYMOWsS6nCYmeZhiqWeXamkDbHnlwhuYqtUdHul
r+2UE4TLVZ+dzIq0rlDhEUSayIKlggYT5yYaIg8v4DmaKTN/jJpCjtGh48+2vyuO
P7Dgj2wPXOt0m/AZmkDAfrt7UJiq3d58+7Yi09XeOu1GnhKVZj75EQS6/TQ2omK6
QQ6oKW9xtKBN89WInXJr9de/JcyZnDTgKAYvr2IckM0lHa5uk+tengd6XXi7GjOd
+1IaArTtplECSWGOgCIwaYEEDIMYIo3VQzwRcxC3xHOoIDG2OY91FnylpPC8vM+0
9lmpQSNejED6qafJunvGbxZ3sEL0+TofrTp4c5Kglw4Nq0r6ITFSojAlHsTILwKg
HGUqehfL1NeIGMa2qUs+x8/laBq4xHe2KkWL/qlXtXs7s2V958IjhzTr1kaGkVFW
vU8ZwNBDs7KdOxdZPP0T/Vvv+9qX7r100qYjf4GjussVxSFeGKcRBbPkkkX/dgyl
JtPEMQy791xfkoP0qi9Hj9nSmxrQBXwFUjuh4uLQ0w5q/zMQMUb4JF7Nohfkyda+
D8tAkFxZ4Hx3AxJxzUtki+OP+vgaJk7cAp2LcUjBcx/cqpQ7krtPwZU8p5VoyG4K
dvgw8c2oCzkCrNsxs4CfD7Q0glmxMRsY1b6sjb+Rs0m2yYY/E8WYTdgnKlZ19c+t
EqRMTBA/WU4t1dk0sVb9hd5kYa07sG7Mvh4IP+YEe0LvCugOnEd/0SHKDuXj/Xdb
hhJnAzWTyFaSBmvVieDIToIfxv1Xl0vYsIlAd29lyNonxY2aVuT4XdUoPwMWtsZu
73xLfVNF03Mk7QOBDC2kk+1IDXIrmQZQ1kiZlKVmkyBx+Vu5RiANkTi39GHN78zw
WvJXWFi7u3Lid7S+vvGkz9lnw1RF+hIWH8IJ+mP3Ovv7u/iNBIGaarqeNKmUUPrf
FTotNIEuUG/umBkzHeVtW7NscPi7ugkd57Uazg/mudHwVss2bONL0u3BZihz48Pf
+Wvc/LzeOKSVBo5qFP7bW3FHi6IqIwVHDSr3ZY0Dv2+r7+f/V8GqD7B+NmO/4nqy
ANTwkUTUVQoObPMnIeVmmbW7QrOB4WY9BInaymdQIWG5wnYn7snjp5cL4jY0caIm
xWBMsFoZstOpmyGKrxPAWyz/8muhEWjopRCM6ALz8CKvByg5zCQ2lZzT+ahPEdYR
q+AJBp4UGestFkpZQBm6RZvEQfauED28i8jElnxDamXdB3jK9aP1/VLFht92qKo9
2X59fxuoQY2o6qmEqzJIVAdEfSE//c79dQZAyS1Vbz1q8EXm5JXfG7RQJT1L3+Ra
Ys8AtqJsxAv2Xv6fB8r2UhnIa+Vm73v7fCJRNJO3ozVMxdJy9W8EOVGL+Zcbg8Zl
lY34vXXZUxdtn5ujXyVWOe1+WqGEaf2VplXpI+l+swylBg0k5Hl1gnjtuyd8nkjv
NXfeOD1ApMDENVq/sLfvmmvbMvTjFy5JgB4U7DoRldo/RWaClvK20bcpXUr86M5d
42i/wQCRUGrvyUI/Ws+tI91Ps1H2fLUd+N3RALSNV1s/osdBFeLlsdyNt0oCrTAL
6PjC6WNJeQhYJjdZ/4ci54YRgP1DntGR5a8JIWeUZ3z7TyL3JPvpEorLu10BDSTQ
W2UNnvPPZFHvy3YuFtV6K7+bOK1lpv6NGsUJsTZcUwUFnDUnYCLNnKyEJ5XBciny
9mkji5GadN9IT5q1Do4qK9M6yW2SSeQVfymWJ+s3O+0nXYXk9ckQBizxgD0lp+PP
qB5XJrMsF199/B7p7taLdSU3D0w5p/wW1NiyWOdLgVgk5CsJocO0I8zfect6yBFu
DzdqSSNX6iz7dffNO+oC9Yg+aM1RNpwQLubwDJwXHPjalwW5MfqAQLy60b+StuGH
KF0f1noiRq01+0nB9uoyD/NW6H4jcUDE0YClNa1yC2LPAeLvI3SCKrq7FWeRgPvu
VemNQxSbDJxz0oJzOiOmO53JqAbI+F9GjKSZGqXNr4L0K40IKxcA1kyOunqA6wqK
75txzT6xHXbnZM/eJ0s4ACoogr3SPBVIYHKPfxqLQx9FT4kt3YAxPXjVI6kND9ho
pOUCcBnbFKOYMVDxgIFblvZi6+Q+0S1XEuR2xXeQ5cp/J4gMmICFW58K4XkIZ2Bu
R4c1nmLj1QVNT+NsHFJ6vcCDwLa6+6IK0DlM8ahW3Wmoftc50rpMOwRrb0aWYke8
H5gWbzgDtC60E+kwrlBYNXxdsl163q7U+PEr0NhR8LJAc86BZ6+511kU9cvqEF5e
V9nzQgTufAqIbpEO0+sez3+kgmLCJsG6oa/+wY3pdfD61XByb6apysh7s4K1HnDw
8+yZAGTm5+PJzsQ/3YtQn/2kxnUD6CIbulCASOzqh7y7tBL2z8NTcGrd+KkqOHJ/
26zE2t1QM4JmF2dzkJqob9AI+bEMS22R4+VjxV6to9xKMUZv4F9K106AgbNregnW
cI1ZltYfnvW1G06R/jTFSW3fY3BxFCX+Aa0qbwbpKO9uNX+Zo9n9HhBPX4mKiy3c
N2kiIRg3Zh9KIECrHg7h9FQqEianlcjdAPG0s6ZPUu5Aevw3zNHAKsZ2bVz5MScz
PjCHz6imvbtrQ+M1Z7LpER+Gq0knAc9ykuQObjVcbtxAIVBC81NPZqf3SnzA+4MI
zX3gTmFdaYPnhyyjZ4dEiArXSiw+YYx+CD6Lkvy8QY3j0pfnF7u+Ke/Gmw/72hKb
TjIQ2Djq580WINbrPdHTPem2Z5NJ+zITmpkS7fy/FjXX6lHCekYVRfkPifln30gu
mZXFd5iB93M2KjnYmnLt5ZBTlHpL/DSDCgoGjdYR9/wOsDiTZQtsN1C1cPQJ6ZUn
YlQEA7c6H1/nAsPEOWyEOigolzHDCAdSZC3wWr/Xcze1QMwcErM9o5s2Lp7BvRch
qYDpJc76Q9zIu64zPfPsvp9HIIjp/UbEta/YILNQAx0cAPsnv9yOWrlHapRCDCdO
Tg1u2+9B6CneL0PHVdNkN1QvVzNkSt2LKTbc9Egiggh+EnnO3SUEqOp+lbn0Se6r
Puyq2BGf4DoKJqUuNDQWKC8JIxlgiKb1bmO+4sCIZUko80WOsuncL8KNt/uga5gU
6sojSpyrj60KSY+A3oxkDe7f9I+WMDNcaxjyUHdgSb3i0FkD+Gd0uGi7CKiSc1tY
1MQW8EewUNzLjiNjJ5u8HwAB975KfeQoYb/mKHbMT6sxQlMK9qCpvLK2iTDvkJV0
ygMhtitmcAeMqj3bz2+gVMtPKRNxXNX8umnTcMFa9uUsMFLCs4uOkXc+RawFFi0p
4sOQ9mG9J86GNPI2XWwK4GiJPmLO3e3OLN/1tpW2ShlUuSAb7nOJ+MO2iDKQ6W9W
RNHoS72vm6VmBO8iZ26h+cFVLfu3tsC6/lI4o0oNkKs0EqBaoM6juai9WCMlayiI
2v/nUq7Ct1dKhMP9WIMfy5uC0+nslEAaJsl19IdYgycj6Ib9vNARONP/nhClbS/N
X0cLUs6KjexBHRSmuquPJqXmyDBbj21wm1U4Fe6UMyh0w5h0Dc+fjtdIqZfr4ol/
k1hODbNkmV6j6b53uD6KngufY0vfy3jIeeObj4nICQs/xT/9CdhiEeEpn2q94cfs
CEzGWr4iY5l9svZGlyTsNCuy2DsdXwmSBP3OvjdeYxQ=
`pragma protect end_protected
