// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:23 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
h5R7KPinmayMNUJD1EeuxRtpA/a8gty0TMJTWkZK4H5YFyeGoLM5bJbnPe8Mq4WV
qvaI78X6v39nNyat7axIXn7nRWbDTxrKMxfhTwwr9Gg/Xe9wSwZsxiPLF9aUPfhL
Hbo4baI5MoMsS47XomwmwBX0ElVua+T3TsCHut56yOE=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 7712)
5MjHcS0gpOaCZQ2rtDLX5sCAIxwq5XtsI/aSJlP/4uoC6X2YGc4ul/nM6QdPkgLn
S0hdch2UrTdAGZU+CMP6XRMMp06prvt+i3YxyeJlAD0wrtYPWE0sB9/I9wUjDgwl
ZlI6li7Yfb+6TsGTF5X5X8gM6o2cpGhA+zr7wfER0nlDtxwjBs4UB2mgOtbznvtw
p0CqLJL5uFp7t/zLqQfMFwacn4VLNwnl3ZFO1xz2ZqYTDNEuQ1L/ZEyNSyewTvnT
wiXSC8lHCUhv+DaMDnBQhvE5Eg2AWZmV61eRZCHYSOVPcE4nW3hnL2CpgDTze/SW
OBU5w8T+We1Cdfe+3IVrXTzjaeXMh5N+VmR7+FViFe38d6Bl5OSY5DwPC2HK2rC8
HujbFmKQMn2jp2FGuNts7kFzfSO+0JCKnZkT4QhX+RaQTkNwK+Jj5UJLNefjEeW4
Li2W0vHJOg2r/xA084WfCaAJf/AABQcHMsE0Ek2E/0zsmOLICdFUeK5Twx4UPPoW
0p7aZoyzJn+tJAln8YVMmsz35K/o0IipRqMLMb3lOy34edu2Ns0AyMOspTUY//FN
bW3I1GwXiM+6A6/O6cP3DU/97DTVNypI+gZiwN+3kgHhjrmp7nbzpyWKTZK9pOpF
srBLarztU2B41mIYzNop4d4ovnYhs//dpVB7HtdYVAi8fy2lZDktJWkkl0MKXMTJ
x7OQdcqtNnPAKLv29ymhIXTFFitW7VlJ4KQ4bACoViyGaP5MV5DjRhG+ZZDTZdZj
9BZL16BUEKV67VHUS3hu2mSYhjdBmFgJt9+iHvEbsUJsYPJPpUad/Yi4V8+2HeAz
1oQJq191DQXiTr6/1wLLk/WWWLmVO2jgpgFLfqKKw1AxnRVzWm5sxmcjhUwVXbeS
bvCg90ZwbG9ifrIv3EbzqtZpCDbm2pUXbMbTb2w/nO9ktD7uk9410TVoWN4GH6K7
YUXjyCQW6vDV5tJyucLVNqOE2Am+nZjEP0RjQNZV5uve9WKU06wlNAM0fO+utL49
bDKRRQIAIuUY+qoyeYSPdhcmVZtg+DCbyvnh770fyzLgcwSPnGkixr9XTeC613pg
Ha2GR6ZS+cAJs+hQ7+s9RuNroHvxfoz+6IRLus6nW43nEXmT1ZFTA8tL3vuKZWKl
rG7Wmar6TA1Ib0UyiEX1idYcoYW3SyHF0ZrTbi1JjtNgP5QcdUteE3tmkSoKYFOV
xzqtOeo84wNz1wna30H8r5f8QuXL3B3LgOuUPxXOEwF2Ox2snIql3vgS706z9Zpt
RYrhAeDYZU2aniB5owJMDw9ksScdw4duE4b9zLIz8TYj6hhIjJtNwM11LZN9G3/a
JKoqEO/lq9hspGOVeNFZYv3tRaceVyxmE+PxlhWBpaQHMS5QBPO7gTl+rzOj8zjf
ygZgQZlWR4a1Xy7FJbMyQVMz+aapdq57aDDe5yI0lzqeZgByXjFgGnzv0zC2lcgz
CH2AAQ6bQaltb51YxLH1299jXgJDdnNofqI56sd69bYCFk28vxVlcrTlbJunqfBh
8SPVYhGK5L+gVsBUOf36JVie53ClQbMlgeGhqJvYUL+QS/qyGELzxgl7jGgUWzth
CiVZ5EkDJrA5mmHqd3ZZdikPkxlZ0kRE1j8rfUx56/UWCW148QA0U06CJRrHkfk2
fAw4f2+e3fIDUm4krGGFyKsaBk9O0RB8ZuELi9gCLs1kXc12AI3Cdj+ZbNfTdTVk
zEPK2eYrQ36WIg6p56R0N0u5VODy+D69BX89BqExrA/abNj3PoFSLnjrSTHtX2YA
JNwNOPVaEkWIP0bOQwMydN1w8diDMEl729ulEfuU0LODMiKLTqltkEaHzdKUq8Ts
yrFk+wJaqXEfnrXhFRDw3gu8xxgy9w7yd6fRRMbBLMFdsZ1QpECG6nrDP8BLMxGz
fP51vAfUp0g2UKoOTWa80Yi4+JBuFbNfxy5OVPXA77tihjSo4HPnDJRsWhvrhcSW
QecftFNUn79yV3mzGXlmrDNhAl/jwoslC1GWUyVq5Wvo83KDqnxaqs+E3QF7Fj91
UeTssb9m8hwvzw+ZrUvs6ZuN/7oINTZ8mw6nRB33GzfhaXN+GlzdtzxZ4SOri5Ux
Siy48oREGDg1qAMGfxUdyNkGkObDpPx5c13PWZVsKkLd12X+oDa8ruGZtWNxtUbd
l9fg4brXrearuYbm8W9Dk8bqrnq8/broEzy98Saz6DlaWhMlvVjczcTCBZ+lzXHN
efTFEVe4+aN5XEYpNC/uAihHW5xq8MND3WUux7VoXaI0Yd/f77TE5MD5elF9Qjpx
Mukg/Vyjzxl97+ahEfMN0ZTqnEXb7cikf0MB3h272dpINdYJiDLBfXW90aTRyzhE
rTqgzmma53JF81CRrKgtEUFGLv4hlR2FlZBXKqkaAxzjZj9txP0JkobJFuPwM6bm
qEla9Pj60qbtd7n70be2io5tPNq5EOcISrO17uUwyAmdDIk2HAWf2fWYEztN5blh
awcJshWZIc015Zmls0zPVQoZqQ/G7IeC3xyEEzF52hQ0+4wmWgjG2TEYGuaCDdYo
Oh0z2J6MpoqGS4gYPPCVOt7PYdIlp1+iyrefV5utNsueGkzSLr+7QRvgWqXNF1PB
jzAHUFUb1SdSsqzyIPXJwabmWSfDxtXsWqg5EzZGN6Z50F0Jr4J3+6IpdPDXfIr2
wwm0yNM+tdNKnIVxJDakbLtI6equwAmdet1wvGuCJciZJuFm1K6JgT3XGK5nNJNC
7S7WKvgp98DfUtXOYv2JBqA9Ls6ctnlVUhKmboFB6QAkzjSeE6qGoE7HuDtYPahe
pP+igBQmLzVzpsCnfi2CZdzfEJQs861/N18dg+TdrnSsb6RyB8047zpaSP84JKYD
h0OHhsy+kvl3gj/OVr9wODsazawFvOW0JmaYPQJgCLNHmXPCPE4FCMVJi4IsaxdM
Wvza72+/jiJcj0KW/yDbNrebVeD5vYNl2FiJWaKqJjGONeqAvDlsuK9ix86mPQvj
B+elu2WUhKsKUOTmLlooEacLjhLDuxTvcBHYuBgtWAQboTF71KR5kOnE/DBiR/bh
e6LYPRBlVHHXyZAqEmXhZu+0lff5lSrZrzNKSi9Pf3CE2S7cE+g2/vY8BuvVB8ku
WIUk72ckmGPl4YHV6kTAohJ62Fohkx/Gncu89Ln71qUPg+j5jjGBL7WQoKGdCyvY
1U2mm7pGb15wxhBsZtNXk5ko58lWtlwBL7fXyI0i63+14eSiENzzyiF+jg5kaRgx
bPWVtWUzemmhnvaQ6jlp4ykU2a5gOREiP4assG6MLPQa6+o4XkJVvCkFHaJOQe4s
o7npDGJ1sK4jMkunkqqQLi2Ku5XSULwr165BTEW/IX6Jd03JbUMp+0nbQbp7XdzP
l8qcbOzrQFl1OL8/r6b5Fj809DVB6f/PTIs6VLEevEcU2KE7x93tVREPR6CJJKm3
jUJazxvzoYfLIODfHg9gl5vWazGr8tq2X6nY9KhcSmyfEn3rmpEDi8SWOYFhoEYG
27RnaFSkc9c+XXXdqtVa0jBsQaa/NrB7ao5St+1IOGOfFgC66yGOPiPF9goKV/NS
ga4OPAswDYIA1CKC2GKyh2p+8YLQqeO8X6Yb1hL+hy7PUjLGXm0luTRarqnepvhs
7Iyx0PBMbgRGamsO0B4QrM/cIippjck4XWp5m7Yvl4KbqG/9grDjJc2ZbnittQOm
st5aZThbxhFs59CNRWrhv0BOIZI0Xc840KwRTvsUFKb+2jtteqEGu+RiQ0X9RaPg
waF+Br6/+1EUCz8gQLYHNC2DalBDc1BJ5tHqGZnoGScAkGFnB0SpnCTYtY4u22oo
rTeQsb1yvIsFUR+K9YsxMJt48LQl4BTVF7QiHuk0+Ny9nqdUYNui4RktkKCIRtlU
CWdGXA6JhWSrILvetr+pLjqFD4aeqDL6rpZCJmar2bZqdKshSJTI33L8k4Rk63+X
p72XFThH0b7oDf/KjjejBQ/S+EFF8swDYJ2gUqrKgiyI5w7NZUXwxN493OOrWE3C
Rizo70gkTf134isc9MzJ3YsG9Qgf76nx6Ojvs5PVIUTXANAIoVB7yNhxLYTfGvsN
WN8yaopgOZeg5sb3S1DhrThdBLxAWK0Gy3eCTX1tI3bKUTTMprdViP7h4cHT7zfF
SMpGvpR1A78U+ib6XwpSphd1Z6Zbof4blDiPmhVWfCnp4xVZRvkQPA0gMwvHK2xC
urK7LdJViTnyGSjX6JVD5iUjj9qc73x+gdjobUUMuQKVigr8oz6I3+2CohFXu2sV
XQKbnqCMlFlqi4L/mKx2gmub6qU+2F9myzieLrNg5TTyjX1rC2bXFpOIgtMwh3Br
Dnwymly2bFdSFilPlGNEd/riYggc5ITCvLkg71rxCYAAhb0QOB3xapSm90anjBD4
5Zcs+O0uXhtWIwG2XshpX8/rr9e1RnqaEBHdu50WZfEs6ENVMwFkUDHhiQSIWHHV
RIrmi4ULe4jjmzgSqDJPnKaKgpa5U/QsWhD+0R1JPUINGHlCcs+yNRbwxyCkDk4D
TBds8//qgXKElzwyXjSAk7UEzFKC1ILPuZf71tEGLlVSKnUvqP0eAefiw7QFEqx+
PFj8h3ARneL3qnM/eyed5Mq33Qe/o9xXH2ek4BlqecVpVTTKalSC6BoXqAPLkslD
rZEBrhbsVQ0NMUjpT6woHoaBrMaaqm15VQ3YLSdrbgmrSJgqAonUh5fTjOu68jhb
RpPF1fHa/OGf8uR/n2EhZqr0rvooMbgHNUjdVap4mzm491aLa43z5vIERwVF6qiB
1hNi02QEQb0aoLv4+bQOfIKWtyexnEGaXgW1wrT1gtsQG8gG0DsLI4tVlrpMifPO
v5FIH4o8Ge+qPna86j1cIU3AK0ep/rt4RTfWneFr0md7FfwlhqFbqnK7gJ2+TkMF
S6bgvNs9DtJX7odoU5ubKucVl1t8anLwNutM5ea/tGgMkkWIyj4DpxV5IJAIEaiQ
nyqyJ0Cj8C19ZLqQ+gblaYKT1y/AfQ5dF6l/f7yCk89LcgxH/GkWGuZhzgfnTByO
rIfQwdYlXd5zg5skt3LshvY652thp8CE4aCMUpXZyiIZijZ4/0DL7tawUQOMwCSa
9AJCFJANjxAPq9wPai8CBq9Xar/tSbpKDYsn5zcMX7Nz8N8Fw8MW4kxn9gL68c8C
YsRDuymZ2gyz8QipEgZs5VH6qT93E6qsoeJlv36FLcnWkcaeUrYUnQWrf3ElcLRS
8AOF1SWAQt5ZiITTelZ/k3ImhyuOEw5AxC2p5d/O9Xv2kKsgUp9n7B+Z1srbelsj
7RwELJgC2uKUDiiJioltjosJDoajKq0gxITnF47TmGPc3YdVVUp9rj+YZO/CYdJx
pMHGE/9HKBjYyQDqKXR9qHcxXj2XaVDPezlEbP9Qz24uHevraEl0xCoZ/5KIirkU
1cluF1tMj9wwZFo5EJyPp12fTwhg4vQUDvwg/wtDw9g/iWuzf4zzlYKD6EZUS10U
UdgFEJfICIiDuJjEAOLuonwYc/hTcc97pK5P/yiJSGGeZPbn5i5wW/bS7lJPR8k2
T4qrhb5sDJ/QZES9LozBftL+NL2l0n7pZT2pgapqGqa30j5TXknSzuP+5ga+iXV3
b6eaXzPWQSpqxwBR4zAyvYeljHGO/uREjgiUvv9kjhJ4aoX+joMGyZ4G6bSM5XzD
KGdxYrviDuA2H/dN27pvmLc0/M4xfGlxEPdOsqJJGnNblhS2nFBKvSc9z5j7lNMM
rJeOjHDfI5ToxMK3XWmOfKJt++ip/2DdFX/1+iEjUeJEP0CNHu8ehTNrk8gFw0GU
Xs8F+W1jH+L5CmlS9o/GCzxU9ycb4CE0o2rj4JDx1gcxuXndao8MyHobq6TsePR/
FDdWH3m58S3gJpIbzyemM8Kte8N/xwbvr8P01HNUCpoRMJItbeBdfKVIdAQDK3Uq
/Y5wd0oKa7stMqtUHwZisAapUCAH12fXn/ZKdT7x7tDRdjBa3Ba9DLMABGu8Px4g
g4IF61hfczUi7g9JP06Xfvjohe5XrOdU/EO9VuaWuNoZhTo3xDX9QFoGWM7bvRu4
eWkzz0RTFsutzqFJ9MBfSxgveiBBGq9w7O1+9Ro9oYw5MKKVj7xWilYY8r+bpw2L
jqyUR/uZV49cI6hY2m/EaJ4VBpiZot8465JDtb5IoTxkYzxKlDpFdGJ9f878olHh
yey0AmicLX+Puo8oBe+p5UQVSdu4TfB1Ud4Yjbbj2MozRTUAuZGtPTA8bU29lR30
yFGBjK0OPXJ+S45YMtQEHdPs8JH/Dckhci7SwX/HaMMkw4EPW4tPQ1thgnKdnGDU
E/MoZhLv5U683CSJOgjf8Xfl2I//IQZXbr8WT13zrFVXNtMOGKW9xyU1TbraLibX
ztpUuKQZTGAZmmFmB/ipvLVI1VZh2x446bCoCjKlOKIe/7qD6kkkPu/hPDLCDkAZ
u5B+y9xf2kHnQPPhJiPgkNA4KJZM8NMKPhxrojrNwkzMOsC4dUvEDEBlDRnydU54
4cqE1cyJD+aBvrMt7HUOB9FcdnB/RatzB6RHYei0w+/W4hzGgHbsb4QSuztyPPgx
qJKnZc0pByArjA9nbCumqdfBl19tifxQnQqV2kM2M7o5/Z4Z2RyUvBz+vN2xdZev
/6odrdpFYcYHY1pvJ1el0MoO9EtwOICS9O61AY1SOF/TAksIi/6cRlr447R53tI9
BKUP2uMNSEn84JNxMmLBBw3fKNv1K2JjdNsMmgWksgZyAA2Lyxc8owmpJDozE8AU
xPrKaV5wLd2HAaBTDx72UAo6xvwQ1mSD+WsZDbhSz+dms4a7MifMBaLY2/9cALof
r50/wX1S3WyKJEIzx3TV2IXX/g/4t7WM7goriwoVDAz57RKY7nPInE7jgTRVEXXj
K86s181SUXm7J8YWGOHHHEQn0WbJRaVQ89jLi+wbMDEiBR7wKLfI//RgpARS1WoS
k/j3g62/UUD0q5rAdnTfnPFJiDmaPzoDwXyEuqY05j1Dptkmwihlw17kj18eiECK
h7VtE+hZnN10o0LLf6+TYvnPBE4OY+S66gMY0y3Wsr/wq1xHgkABLhJ7iczCbG5E
14xtiGufijSJ88h5nH3in8EJoqLwrkttyzXmjdwCo2YGgaq4QB/IzbnCrwrB1Bdj
/wnDvOn1cQEJK7qVOiDMbdqh++JbyeTjpUC4w4S5Swmw8K4dn+61e5Rn3JPGuOAv
iJO1ncUAx2QhyybzhFTFgsZS3YPnyb5XDYAux1PaTQEPzewRYyP+IzC2GRI5rEma
QLf6D0B2T1+qWPxdja52l6y9hA6MqoDoOsBieyX9Zy35GTAIr5G+h/uXPnxaXThE
dPxJQ27jPWJfYxHyfHGsTTsX6VIDQxmcGOq59jzpwQJyO+7W3nceAGOWquYOs0we
2+y4tTCmJQ6niJmUAxgR18iXCQDovSaMwgjuRhosrLekrS1vNzkzA4lVOlw80zlH
WoJABp/DtRvsp50WfDxyz5DjnPK37ftm5AQxzYZXSFLGwMFElo5ZmoVjAXzb4Z7S
MHYvL4fRuSWrR9S9T5Lon4fh8YpawpZ3RYs2YiKAxI6MPZLy0sB/RJsH09L960DF
OC6BeDIEBQCpJYbc2xEetzh1x43UXnOZ6xuVm7mwliveqcsm2HoSL4K/KyzzC/v+
48koAGeDww4y5ivd97/GAQMlbsPf8ik+4sveh1aLH7G4Da2YBwEeOQLHGsAFJhLM
lzwaWNJVfFJBEps/hOl30IkfuWX2xZCu6Tbo+CT4WGl0hr0dpAyeH2TB8KJxjDMt
BwqKSwgr68GlxStCXhb3DnCG2+68ptXTrDgSTYO+UWyYzpWwEu0iW6fL7zQn531i
A4WVEddo+sBSrzPJRmvK1jlNaMRtD9xLxSmiRryLh5P5WjP8mABXRMpwsxBs4hgx
V+pFoLOQ2Py6YcnzvMfGUd581khGEv+sr7E+ZyK5mnF84u2D8YYnr65R/6TgR+uy
1FqUGUibkRFqVoNofHJRag5Y7Z/ddyaHF24TBLNdByQ5q+V5SW4BVlAyaQBG8MF/
t90lFSubeRTMi6u9bNNky8XPnLhJrYUEE6CiJeTSiTYg3bToyFj0thfmLWqhDb0B
igu29UUkdjGF5K2e2pDF2JyCO1vlirVjSY6rpdtVtrx6XL7CcU/lPSZm3rKH2hPl
RztRxBzQAq6NcQepd+XtNYJLEOipuAF8VYu9jTMXmdHBCjjyrn19tnTakANJsQ1c
Ohv1I1uBpaHZTaGg04f30XS7/4BP2a4p9q/c2RHOXticUQRFLn0PRcfXy3KZuQ/y
n96Csen6X+5bSxQXo7BmXvwjuBl4/0sigxAwp5iwqGe7eU9bAdjVynTbH6BcgORA
YYvPY+zh1SgfNWJNlocUPkGD6lZ0YEsFIeQmPny31EI/ldEdH5ovuTW11MW9sU8A
Am+GfuzxcUSm8ZT/GYNjB5lUH86ulCNOFo9s4XsWK2ctYYQ3DtbenvmLPdRkk5qC
zsZTtPOl4J0q2spaPipEwycRIbpL6Jhir2QswNfKYh/XUSbanwIxs1d1AplSFj0T
dnwKmVC1EMqopNuwgYh1MSvmoIU6kl7R+r10fv1BtUvDXtc9zsmldYno4JoMbV3M
F5HEzlCnJh7NPGxbn5GCpRAtCY38NOoIYFuk0GKKLHHlVdONZJ2N/XxzW2mhdQcC
Gvb5XC16gXraxqX+CFTfEv02VcTR8AoFf6ZHbs/HnQviYRJYrbranThxx08SCpPG
lI9Hu0JPzYHBEpzCzOdXvP1I559hWgKlpvJNW/0AM8RMwKbqfWMlh0jftvTbf8aK
yDSvi1/j7Dt1bd208/xI/WzoYNuZ/IQ1JNSZdJsGVPL7QLDvTNDAvsx2wDFqOMTi
dei0bWU5Ziv4aQiJyTIBiBBGgxO0/vF1tKS/R4jLut16W1g8QxSQPyC5ORIagidw
l3ZQtTB400l2uSp2w1Gav8REVqJMxvGAHuz98B9RYcebqYpTZcfhhq/8tFHWOXmC
rIEefixt9fySZ/yPUxs0RelyRy/ot/+7MlfqsqwNP8R1VM6+SRUA91K5yuky5xP8
I6rYCLew1JQ0j3BY9NgAho5G5WVsG+pS+JF9d/katcvLnqExZDJdGdcCeGQE9ino
qLe4jMLjTmaYpn/nkjthICdk3hd5Gpo4qb9pnfqEBazPMC/1XN+Hr2pDXhmtdvOc
ZF0VfePG4SQHV1bd8FKa0ilShNgjBX8Wzyb4g+UvzFMHnkmSu9vLVLIoU2RYSrAT
ky5U6+ga+riRv9fwzbYrc2ziZriU0AgceODM24zcJSVJ2n05Fe/Px3IG+kOllsmg
Qf5J/m1Qf0BM6Avhp4Vvd36jcV3dsphKaNwLWzSyifesSwxO4iS7JVScBkESwoQL
IM6JuCA/PsmGTpugB2gJ4ujd5b9ZJVgNZiQ/Apai8kAIOURxqtRv2TGthHei5CBg
7OYG6Cu/y2aGq4roiR1KkyYqzr28r1Qin9XAgqnAZm/3ON41ktLjlcuAB23OCZaZ
1fCUwqBCWtT3PIOnjzwr9s9w72R/GSdE6PTGlV70GyK1ER40xsAbzsBJBw+eYFC4
saWy+du17LlXCzVLlyPUbOHSQa9DeachJvy/VeSOQqXOYK3YUhM/QF3CpPwyiQyS
uCc8jHbmQkhjcFpqpMEDfPIJehVvevRDGNFodvCQNmK+AeU2QnD2LJU/TMVaW0Hl
dekrzt5ZfJvmkSXCBnQpx7IPm2Rcj94Ru0nkW0N4vWSu8CS4ocgZr3h7yEMfEWcn
NdxYEB5TdqK/3u9p17gMxdpw+MPg9Kr+LESqVanwyuRKEH7cpHTowrsB8V7gTwoC
nSPs5kNonkBW3+3p1UT0L8luxxoNx1ceKgQKpy/+t5E7EsTCrfgwYKTHah+yOmPG
ei9F/W29Xo9KfxTJHWrxnhfLCKTJGAxakTOIk5S4oBKmFbsJx8YjIcKi8oreNDfo
kr/bhZIyZHi6950CaV5BVgUDXC+SnHNP08sjum/uRK2zRP4tlyLOyiQedQjtBHwb
+9Utc94tfcWH1Uwz2scTXKnjPx4g5ArUCN0jVDGZZ3AikJIty4Nq30magpxe/J2J
Nl3O5FtBLLh/jIhYV4g7thlxaMyKoABM8UDjGJBXG4qnJKfQc48ve47PEjPQbmlW
xYVEXcs0IaqWA0dByNFYrzHuGWDcTEa8asROivivs5+jCmDkta/jF+VBgS/ORQ59
cU+BjPMlfH7fvJDVb9ZTTqsWVUo5PIP1BFrW9hi3msQ=
`pragma protect end_protected
