// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:24 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Pdr0aqUtQLwn1SL4ZFwblnWv5E5VtQrommrTIhuynw4uJNsoi7vd8l7CAyguBJp5
OcRS6BxKdrHHZNhJKZ3YtaBz1Cp2lWXgYt2WSwZHVreW+bG3GORMyMAZZQ0gfsi3
vu0TlLPIBgizmlexUWOYibswiW4Ek/PRM5QPSbYjFdo=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2176)
WQGOmGRUEunRjwbuhc/sdpMecukoJdCYsJOcWuAvHpwZRYToM9SeVFzmLIWCWw+t
LMARx3ivd8Y8dMoez1Rli3MmpxC4eHrML8yGgsCQ4jEWXzyA9uhqZNXmclsWFBaJ
59HR1o0a/zxuQbHNOfee6dTIPPoWJOwVeJB9Cqq0TZNgEoPhv9nmV3V6OQEply6e
YVlaT/q1jtRPaU0lwDfi8X8XqQ8cEEhcpSeuIwol6qFBJ81sKiDJimY7ChAxr0BG
epWsgnsO7qS14cd/Mbpqp+gBClFJQQmBbUcYBNEEi5mNHetIYOuwSkjEwF2VIacY
q5nlKtzrI+6Kk2F/AkeUjPuD6C25xziY/+g2h94u6MXJ70a/jhTbEQeSrr/yahBI
yGgFHv38mpNO4ue1CjBjE3gnb/Gtlr5nxsNXFdxaVyLdK/nF4eupzT5snWfsE0aH
8t1ccNInNzDxWzGYQzXV2sRZW4xsBMFAuMmZ7uaVePr2pUCvG/k1Ig/xwnPLoE28
zWOQiTFupMnjjQJhec+H1Y8RDeLHo+Tryq017GTD3YQoefcm4n2XuLRx1m7y6Cdq
7gvP6ilJR4rBfmWb2OmlD40HokkYHUSfcm5ZBmj1bQqYPwT/a5yznjQwswVHHYK+
wmugZeIl7CR+HSTaxvJ3xjNGrCmBfYD3jaE/GLdd9DocxG8T3Wwgt/ePIZaY753n
Q7sT8YJNKfkKqHuFVkHSFi0j4Tfa4YsWz7nQYpXhdcYopDIbT6v/fq4DT06Lpdif
8bdoHC1qAn/nZo5pDP/812YA1k3ZQB94oXi+Rc7voneyOY2Rs0O90JcFwEJ5EVVk
4LiKmC/WIS55CgzGy0yBX0NlRcLEFVWx6xOvLqy0kJ//WjZUHxn7I0VzDbowLz4u
B1yc6S8fog9H3P5TCELyMzxOhdvAQ0zBhPoWqSH+a5Oqtmpc66UZ7MBwt1hqicP7
2Mv4A+VDhtqRo7c+TWIzEYKkFmL/UShlo9l+xg6mxsOyS7qidVL/fo6QKJGjordZ
5S9/K0mkzVOwD/V2SACrhRAXRyS3o/oZ7Ig+wcrX3Yw19c8HN6aQGDfMw8Ub6evG
poICVPr71O8LlByrOahRm1V6Q9RU6qojwLmrvjsvNyJdGyAkemPsy0gHuPpMAHjX
At8WEFpOsrF6yphWskE4TKtaaihTX1RjZV9jtYJdQKWzs/OKJb5B1AGpdcQoxJgw
sXBhQm5IETk9btUWuh3aBjt9o2aWKmxQZE/omDINfelczWpeQHKaRMPjkf0E9T84
6o4shhXeFcIxYlAMiuFaU4xpHreBkafuu5ueB6xv2kbSy/CC5hTZ4BVb658njiUB
IAARnmBfGoktGj4+kmZ2l3IOXaNOsDJtR+L9GPZv0FBnQgkTQmjqrtXWm5ZVaTA0
K8EZ7EVxPe9DEGq2hIlfFKkqOWb9w3XUnMS8O7+SCnOmDfTEWoN3kJXX16YGlIEa
H0K5cnfV8RPBKoSI3UWh7wjV7NjsjMtui/ILor4WfX1ZKXEittwUlVJsSJ4R54g/
90gIvYhqAXQGcE9y9DiBdHgtXe/Wu/Yv5U5TOzFpEbqfKa6+gcdRgFpbJSLhgBCm
QgGCUegpekwu59zhAMyNZ3Q/m1U2rt6Px0EtyXuhRsUB3OQnPQLle3anne1k1EBu
suzayKPHisjr+QOxO0kXzWEMI0Hch3mS0GiwoM1IJx+U3wBRmkoP852RV+PQNec1
LIfAFmtMB1JduK3oE3nrJ3dE8haaPs0YvhcyYnffzZNt1l2I4t5oNkzQowJlcEYP
rXpOjjkI4ItMnPnDutpwP6Ovs3fVYapn58XjGOxhkPbtpC86qnABtXU0bxMBlw2n
jQKas8jVLISfdSKGOlW8iMFWrIHtwkrUMZSomH4ZkvLjDGa8nwTWGm05BpjC0GAz
sRxdkLYZVkANy1SxiXjIG3VyxF5k0t9GN1rAF/MnQlcZPcnwowcDnv+cYpYTNnkK
IOaa3xORuZVDQ43QLW1LM67o5nqn9NdUIw051mwmgP7XSbZ1i3ZXcQQLwtpsRH3x
mzsiRjD2cpgJW1NsDJy3U+LQaX5jVMMHZDU0wfy6AWgAJxRQhNvNZAn+PUge1+b3
0KFnzsJ7EG78yzBdMUFkW9npGQspuddV8h4e4idmn1Du21eQaKwQVH9dgEnUy7vZ
5xbMNS0C+e62xHz4Fz/7bfXiFZlzsYxybzfa+qY0PXhnO44OwiDopD57AJtQmcRZ
dvP5pXy4shBwY5CMOd60astRq+z3QvzJ8tcOrNhsYZRt4+ppWZhCxoHmMJ4W8dHY
zYUbSgi+gpX1kD39RNhNzqojzUX9mT+df263q66XX3B9P5Tdb7zlupo2srTmTBXN
xVzipZk1sBxrMqR3Ij3iHLj6+QRy41IR9iH0vAwZ4xsfkp1cMR/H+1G33G63PKmn
WuKppOD91G/UH80lB2cLjA7i/b+SzgRto3BquQlSlOWcJd0ePTCefnfXMFOB5uFd
EtIr8FP30CyVS9LBa97/0xMothN2L+Bo3byZ5tk15wTZGUI67ollBKeG3z3DQm++
zvlUPzA7+Jjby4iyEexIXNibcA03lSHcBgAUqTf6dwhhTzztK6XNMtGGpzMceUJN
6UnJqvA6CihLH40eOXJ3q4dIA0Ux8xQKUgzDpsHew44UJgw8l6+u/YXXBvUkiTg4
Gseg8i1/8XU6A4mtcPZCPrujQlaCSD5sNhRt5zaSzEUTAkurPNjfbyvDq4fI6Dz0
Dp2tAwQa11QwyZFrpeyW5dLsTLfHhK7mGiq1wrHB4zNwV8jNaWp7GBg2HOa1toGK
azuscQ5Bxdv939hZdcDGKp13LybU5e2Hy7kaWlHdSDSjhOIIldUfIEok8Wz9j+7b
18GY73TvArB/HmAO5/lYpw==
`pragma protect end_protected
