// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:39 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
nLM9IGXrPl77Rs4L2bulbV/m6lmrCtTaCFYUJtWxam29G+upBYz6NMzisZMygKu4
DGNl1xmx9T79yOkS+yi313ADtpd/9gZ3iZLmhEDNiaZDVA13E6Cr4UN1QDINuNfh
JdOBJZ+2XeCIywx5Ne8RcmqhBQ1idFsgxHc9L9qLwC8=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2288)
qC9u2ZLIR5sY1ej5IfOYamOX4cjLD+I7/ioHgHdjYK+70UV+nlWeYtxbEi+GLvXs
6ZLLE0bc01BNsDBU70KuBy3GUIVhmc6m+CCk8hEHpEZtm7I/W/D452Da/J7C7jYU
FH5gDMA5O6sBgY4t3JIkXqQBipcHImeHAO338n1/Laj2tSAUqaDQj/wQbZqiIMCz
nr/izknKKwJW1ZZyQlVu7i3eY46uTBcwtt3/ha9llgDBQaUWDjRsaPHM7vw52pw9
Pwgdc9atCKf+jIzPUTTiHAfZstQ6ug4cHnURy9xHm0+bBg7BQkTOoxOsFfMVKlCV
//eexpr2x/ONEisvcys/b4B+nPwBwRUurN1rx3gXRGiA4WEmvzDp8la5vqwGN95c
SQJ20oFjfr3jjelaXIS6BWZl59hPd4HQ+/YVxhW+xvzFjcNH6CQA9ravIbBQ2SzY
6tbzESCcR+xuYr2OxOarC1bJpoPgi0SgE/C/hBwF6ipa6/bl3GXjvPnyuuHTlLas
A3H3DwkYKB+E4g9Blb9kq8KGWgoLzXsvTR27OPANLE5ub32ZUDq5dGmFOpQJ/GMk
n3L7LQpjciIsTwkr+fbjQZYI13CX588FaA+ePgResUOBNnBuCJG1NlsZWgxZcbF+
YvwgXKr4k9PRXRxYyYnZJXyCpLelzxyYCf7nhTZ/Eu8UYS9IlPvWch4j2gaEx7v9
jDUZBZskctQ4q9ilhu3gZQAagkdkFsZ2oyXTSMA7H3ET/ulUv5V8NsQ4mY2oK+MK
ZQNFsuh+iF7/J6gvXwMGWLOsg1P/to49XXuYcmhNr8UfvW393W8xiEuiYSJjlrof
zBCI6JdcW6Kn2UyrXlcsA/HHgLp2US1tJ43AAOfLKi0Cj6rkPRwVO9567S9WwkNM
n0tVqBhlOq+6jpKIz3zjJSxwsaquf/sRVKyc2nk46PcldXSMa3jdrQKnhVCe2sDW
7SblDebsM+LbaqO01rjjW4R4tETnFcuBKKW0dKFBtMXeqJBMAkP/pzFiOUlq28P4
9WOiKMo8k3vQa8zSJSV4jRXG42Zt89BAuJzfKvYulII2ixdwAYfQcmjUbUJywqc4
K6LZomHUIDHQdhZZvYL8T1EvxpBZVZohS5/bDOdYlLfa2HVn9kk6+2GFkmpHDABG
MBqF1BTsI8G2HCFrMF7jT5ZLYwQhSqEb5OtzLp9rcoqie69zBwynwheR/dOHL1lF
OpIrZEkTbnzHxkPkJtrCOPVeXpVWukQ3QGZLCwgOhf+0Vvlq5AqO/OpVZeTxnV57
tloW8LZUGjOf5aGcBu2w5qjzxH10eVERJL8kQTh/6DPJvCKO5HymA6TT6C6WO2Dn
QD7iIKWmw8nkgBg5K9wj0ESczjduIrhIKtod+dNfw4o8f22hirdbSDL/UQiZ29Ms
dDm3sSWyzX/QtktmztbzZhTwnB76VSRaa8QaVq42JqkJfxM6aVY6r3FXdvSfXhLr
dAptshxK5RbRUrOqLA4rBw1JwNlvhZ2/RMfWnI4Fk1Npk3pt3gfIpmbmjYN3AeSj
vLXICNKROeqU+v1m65ma7jIQmywIBeUdVA06qObgwiAqClaVk7G22QL5lQnqxe9Q
k62rjhNsSmUkHR7AuSas+LTVmX3f5zFvAin5/HoPmly16HixMe46FNWDPU7sVmnK
8cxFleYWZabHg7WY5c/z8G7i1sYQHZrOpnm4L13Djrfkn1sef10oNkEjGo/xDAVJ
Bm2UTr+mu/g3AmTTVd4EJ9MB21nXlQ78XInp0FfycGxQEygHTScHEHsQASMViO3z
nMfzyYWIbtSe4zxxS4ULl6XpqQ/e8fbRDvoJV/BtTNoavs+rngmAjon88LqzIqT+
/J6DxfaqbZxuJ+zmW6siBe/OQMNgwOxDWfLGcRxiaYblDL/BXWqpVlr6MPuCQnYu
afHagH2Szv6patFljjuU6XXEuaLx2yAOrQjvGyPeBzIyojmAbkPuGDchlv1Rp1Ds
z2IOPKSfbnq4VLppz7Lyw+j/wfTY1WS8h4rDRqIEpf4905pzyVmRxy0kQVbnb6RU
5vkYokk8TRfuS2LOhHemScl1JtQGeoc36p7MH3obGln3CmR8wNcrwQPzKUBbhV27
49QM37o869UGcE1cNcb2zsI+BTuYwE9EAu0vbMeW7Oqe3UgbPXwttIFQF0PNEeSD
RNEO6IGC8q24Y7H9qODCfIZr+j5CE9y2zuqL1TwjCbhp+pg0kxBOh0buUulF7r9k
ofqELvy1k9r8fRHv8Wv1mOKQE4dbzREbm+98NVkrMhejCIDQ/45Oz+D7rDR2rb0X
c2mlX/FmV7aXgVQiFMBTunhCNe6bvWr2ofRruIcr8bnhtYkJefP9RjnzS4I1pvRn
0PWiHb87jo1CjbtPEau6YFr7YZtK8g7qMYQYkWWZA7iLiMO+WMvojkCAZ9dCoAR2
FwnPflhlf/lKmgn37kkM6LZX8xWoTSyBMOFyfE615Ud9Tw8sCD0LXAv957XCPVp3
rum0teMY7Ju/TmV2n4j16h7Fo7KM1LabT5Mr7W6UiVFrob0XAGbKookPwp04TfJg
cniAId3nBe/9okCEz1+mxtSloJwe8EfiWNAVWy1gLsIQJ+fI288uVxkNjGr3Hf6Y
HqXu4fjYDsXmWERLYeSDQe4rdaYeB9+TzKhA5Z8iYhCzapfApo66aOeXPEd/hP/z
JelaaLnxpLRxyrLbaAhDmDI1CmVYeyb+cf9YGvrLd0ZPguyHUniOmn8UDQQKpvLt
NQlUwpPQBJpCrKmh+zdAXNfHKXwxCNqE89ZP7RzV9ELXiNg+VGDbXqx+19Rpvnvj
CFEN2qjyqDO967LYkQYLf534LbEj2MRayYK60pBSF5BH/wmPEe6as09pkPwSCCuw
L00IR/BLq4ofdw9kI8zS9UwfZfZaCZ5hBcchco2b8djZcJ+qwAf3Yw5YcdRdWHAd
KyK7zok/MikSbcftv2E+r4ey1J6paAW/PkZEl4YGkizXTVu3ejXQ6Cbq3s6xjY9q
s+avdsL6kgRcpYSMu32wlXvh3Ak2EdXAa6rCgYr1RRo=
`pragma protect end_protected
