// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:24 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Tl6cJJ/njkHJWNcjMTIRBPRx9jE7XelcXYObEPQQsLICzg+5X6605UsO6HRG5opx
2pRa1Cra31qKB6Crlo+5moNeoPwrLE0pj0nSCOe6qa3CoKDzpfF5qr5aTBE8ZSro
v9j/FzMlw5IZxfrP/89US0lz50HQk+piRZaLf/r35lc=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3184)
/U++Go8PWLxPljvlD8aZx3TFWQS0MOipN18c+OZJzeQw1r+vJjmISwNGmojcmNIB
/ADClXO5N5sJHHzkTYMRh9CHIj8kBdFpFejX1bSG8EFX+e7Cn+kPtA705FAlq69k
5m+mgZvXNUSRAnF0fHe2CCG6JCjhQAVvvaMuiu5CfgzWovATTWSQPYgmukPp/Pl4
Q0dv3VVWcP2hECzH7VAgDQLC3+Kc4/MMyhNY4GAIKtwGtUyp1th4NrKEq57YkROk
Ht+KXDbqgTAHtsJHeJw03UzGyTz0ZTKaym5jl2kydM4hhP0iZHuzf2PxTyDd+o4E
dCPILVfm8jBiucE+C0YIamZQCvZKlwfqN5zOTT1jTGwxCgI4/C1qiYXCzlUY+pUl
Zh48RY7OFZPrfxezZjtnP4jkIxqsIsWsppo7eri67dgnse6tqqo2YxzT1RJNIuNU
0/EJOpkd/4Ri7YWah6x2UawZooDLOX6KWtmJxY3HdtE2BYZWnhsdo49DccAIiAyr
HzVDYgjySobIn17klmT9M0Ywb8Ym7PL4yAS3RfpseVkWR+x3KNxM7sPfH7G8vJgZ
Jlc/RMvHyLK1yWV2zgZ/szJYLyj4HqUb9TOO/V9A8tSr7JDtVJEbk1KH3mjuClVT
pLFolvvGcAUOj/1QxUFWHDRD2kdoHrYENnYUAl5i1ZxEOGOGzziCf2CSFLQkrJ7z
XNj1FpD4KGYS3rLRGSNDjo5UOEWFdacoT7wx0gt4MSLoTxA7aKU4dBhC0cNuJF8j
53J61jJzAkhHvgWqV+N5wf61Hb47shB46EVKYh+MgL31nB+/Y4mQPZdQ7dmhcJfW
B/xQvynXo28BsEczrLnMUwoHH9+8mO19zIb2a2/1/C/dEcsU9kwpu6jYzIeEh6qi
1spmc9L8NmykI3YtfFS1u134QHRtuAQRNagH4lzUfOOlhkGLw8mLBQYiGyu3f0gi
6wkmEVAcUc2CRAXv6q8xMQ5fGh3kgW0zY+eC72D5bhoiNPtnV+fFAx3yOd0AnuJa
i5pA1vu9c9PuZs4DCG69zmCCYB7cOldeVVUO9Q/ZKRCEfQp6utZYxQGFtzMcEEeq
ketigq5TH6UeNCoS49B2buhk+RckVsJwnQ0T/9mwIl9lsHvNsN7uZm/EfTuL9uZv
W4MZ83G5FgrWtb6ya5lQEc3LbJvcSyPQhlsA2gfr4jVrk7iVOEkPaAgPs2vH2DT6
BKlmCicdkKYVH526bPWyzuDJyHJ2NG+EyRFggCWKp8fQxu9tgfEFS77/PGaHirMh
doKoPviIsNHaQdqhZWtWBXv+cSayz+q54mvV24PMvjUhwOPawKybvA9xL5rvXJBl
SKVkKy/HlPe0Z9+vuqdRchkavAV+khWM3+sF+N87b9zSP0JM4+hFPgrYgag5vdwf
ByzrygfbzPnjrxuqNsQ++ac3/fx2OPiD8QN0rWw926PxeuuA7IPIiimRbAFR44qI
WTTtA9/SJ8laL2Iu7v1wHtGERUd2mHWYX+PGBcB8E4+RcLLGVQ5zOZ0LJ9+xBsYU
qn1uJtR0MxfE2Tkb5MP7d0y66HK3ncYFNbmhJRWPiQTv94cw8WV6GoHo6XOqNE6m
jdYunXa9UyE9PgXvmAzhhKkiw3uBNs8kZLL5+eUSw43uLFJGOAyMB7js83MmwEsX
7T35HfcrbfJa83ZaXk9qjH9GxxpaBuhOiHYAYbdCjcrWfw+46+QOuIlqYwHbXBI8
OmuKKrlvpOfT7tGCcBhcYONpw7ve5PqI2IhLhjddhbBKut5w8rD7jpU/8S8fAl8u
3iZ1nKNRtFyUvxfaLhQXPPxX9cEcIo9HAtBGDsRSKiAyEOqCGCK+zie3PhrxtsZj
JvCjG/J61KbWGobr4/9YbOqASrYjUoXJgpvpdNClg86+W1Vey89y6g3m1w8WEhYh
Eqq5PA53lr/J0Ctm2JTraVq4zyQKo5MCG3gNenqBSDNhOPPrD2xtbcVXXA0M/Pnv
Zlq9fvkPK8VFbWU7siwFUY7EVekdif2KSzqAwfwe011IKOgXgH0Dk4nhbM7Gw51g
HJuZqDsI/n467/pSXiYqoUXwrYd7pF+aJN7uWg7pRSF2aScH+tPWsqxN8pkIvDGB
XAKnFeol/Qxqj4k6me/0hvv3ovZkR4lw+HsNjtYoQZ595VFzLcgzYRJNV7Pwb8Zj
PkxUDPcRGlOcVKU6/26KCGGSAL5/wbBgDlKWRBb6xa3F1m/pgm/mOF30ht5ge3hL
8j1xFhDjPDMTW1m77iVml8TVOvcC4zQxsYEofJmSWwPONL49n4MNV44aEuHjvMqw
lA87OlYlpVAXHisR+TkUCsRta2PyJDrDZJ4O3bp+Gp9kXMv1TMsV/Sz3zxAzxJUq
xKGUZN7D9zugykFKkObVfkqGQkjDJIEmM9U9B02A1sjkh57miSNsCfllTfvxQ+w9
Wj5n/fVbUL/ndSJHEvr++62WTu5ms40iiyleRGF2wic286ezF4ZHEpoRLlDnovkW
jL75DLGpozxSY2oA5U628S8ZlMkt/To9Bd1OXG55QW9c9bP2o0BGh/ne9XASvzHd
rJeA9cyjxjr9CbiiH+yGePX0p4aSzrawVKRt3i1mhu2EPitPNePZ5u0w0ccvO5KR
l8d4hOksjAyn72ROmO1QLd1VrEiToImdD+omUzV4uETpSdxBpBChNiHmxs28ejha
s8RmBe4f+PNBekTuQXnNrthtpmhsves4LEgLgtZCtIKFXzA8BQhB4kS6k1n3jZHn
z0Y4HCnlzvFGbiA2/1RUVIv5VKLrwtyime5G8k/+qK4icTGpmTrVFJzi45q9z7hi
jNDOZtBNjlY7DiFp4CHZPbMYMPHYZ/aYxQQzYVDsc5nVfZ8xh8T0CCfGnCeWrmjB
xcQm31wk+CnfdqMgfLPpjKCgHhrGWQXiUe4WpJWPmvLn39C3q3JReBZrCE2FIgYz
NI0fV8SzidVbZ++mKF1jWTQxA/RtZ9gfmbnSP1xIcxlS0KXh0beRRnLgDjLftJAf
29GRY8skIAsLjuqcIx49gOdJVjby3vlxwPkE86tIK6+X2HCPk/5KUe5yYnadMDWU
ssvO1SbzB17uSUHGG4hMeN6Bnw27D1q1i8UZ5WZMo8mzZk9Q+AbJucFIMv3Eezjx
s+h8UbpkS0pAIdP1OCLU5HDGn7+RFC5fbXw5uyBbWnjBc6eIv/7oLyGzGQsvmIUc
mj4Zr0YGUFk8Xw7/mAiChDvmK9KeNn+sCKZUV/A5z0QOI59a97Rf3eVq6OM8f0Nj
r3pcnfrYKRPP4vxgWW3MkLaWz2ZQSEuh/Baxyg07VrPV18tj+n4nsKqQMYS0pjr0
fQNI5aubHhRJsegovvEz8VN5s0Ibhu3pkFyBLaLNgTesr0HqoLLRUmIv5fi+OHSF
aFOUH/4MMrsY+EjXM3EvCgho+SXvvE82RpVFxiwqitZZ05dLSzCbSqep+BseVHrk
TfnWbuAr6Em9nK+g6HWOvQlQyaAm55ZxGASKXv+Qp9tO9mf+l1DvaA3bYyiYMPgN
4jGxqYEc9KXvjbJBE+HR9KUSlA63aAPHmRkz1jpFg3YnpZ9PM7NjbiITVj8kzqSj
apEhqgmu94KBAGhfSTuBUC4XUWsp0+swUbXV47Sy1fZYxHBrN38I5R63DGVrXBPJ
5SEJVJ0/fIdxs070DrOUMzPnecyOhPHps/SwR4mYwGAW0htnabsSAv/NjiF7U0bj
e65/pSQj9Dkp8mefIhJU4xbkHtoz5ZumfOhkWw4AXLxUBder7ruT8zQsf8iUJVsZ
sRTIh1g304BabJIEa246dtE8ieOxUQiHQF6kVuU0Dhwyd5zn10mVN5I4qQ+j36hW
d6D9ANLG4efwmutWNVDcPv2UzBmTwC+wr3jZwpG1ptT/NyDJZaviRBH0tC/eRFva
HKzWXEadQYkz0Wsdndfs4lki7pDnBbVK3ejGztr/HKSt0q3YqGrkizakJeU2pRm/
E/UWc0vNgOz6gi8/EiwyanZwHNQ6gDi6y+ABXWyhWvqTtgSF3eq5dG514S9yOT8p
p5IzJnG2GCEAuAkgULF1cHAtES2yls/NJOFN/rYjSSW3G/JyEF9FHXQww9muq390
k6I8gY9qPg5vraS05yS8SWx2d/Zip4iOZGxkKhtUDhp1mDZzgevWe1g8PBHzIxew
q4k57Qfeo9K2pG6o9hGD25Q9kMxia9er7l2JBdlIMnV9EIx+qa01obLPx91sM9n7
BSmviNavi2tCd64QAkfGVQ==
`pragma protect end_protected
