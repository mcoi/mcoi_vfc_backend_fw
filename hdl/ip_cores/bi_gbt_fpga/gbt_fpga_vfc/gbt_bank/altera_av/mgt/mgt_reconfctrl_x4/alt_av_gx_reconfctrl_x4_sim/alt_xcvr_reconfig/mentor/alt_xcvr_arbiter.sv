// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:39 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
j2zx35QdErZV+FNeBQbNftGMjNBnsY4p15T5cWynAMhPmGZoUtmwgeCC+vIjZuOt
16aCCJ4VqYhpdarLfLfxl9RQBX8pbaWkOg/y0l6JJ/YWt+v7AYT4HGPPkP8UIfQ1
s0IK9JaD02toLK029JV6C/bzI3QWUNS8pastwxZgP5E=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2976)
tLXuBihXswHZfmjTEaBbTrU90XPRm05TiGkx+6mUYL4EcaBcor1p1eDProtKHvPo
m/1aO5nAZJe6BfCTP28gC7tXIaYeIDSG/cmPblY9fotCwvNJL3yAVgfLOdlOWV9a
H3QyC/tfbZIJObkoveixWaPq6Utix4BtKE5+knrKpVal7KmuSmSWvALVj16BhOvE
ZTuCEWo0IGqnn/4zu/0GHgl496kinZNbzW1wze5lCX0/umEvCEOYql0FEN/NnKRZ
c/5oy7vUY4FZX5g9OM26cCG5CbALgCQT5anjAEst/+2PR2VSLOcg2hKSHz8ksjof
LtAhHOjcRW/uIm3Fbd+FPucXcm7i3y4B16hY6ukYg5d9nz2O1iHWZYUOFTaovmjg
wtW5WzBF844HT5d0DQ1X0Lf6g6PxUA9WihZPnF+AOVk7RcSnYfbEkrx7GzyTwYV6
1HPQf0xx1WYTHS5zVxn6MYV7qdVQ4IkY0IRK25r9cM8fUeKPvuKp6dpPOADmJTR3
cP47t77iF9R2/Ufio0micfF9gOfB3S0+vp8PooZjCR39ViCzV1thExA3Jk/91RzL
kTZIQpuI+4N58Cgt2gnyI571oQw9AXEI7b0QdZaTi6dMFaXVU4czxtBDtVYtiIm8
jRs49hx7BglqXtqYizy2B3Zwy26H2EQp6xL0N6u2KBesjSJ5aS2rua9eiEoo+geN
EQ4szzF6Y5HBLe3bs8gO+PiigVEksQ/YoTg4su0csUSWlJwGK/uhEiPmHj2za1Xe
BCT+90+3PNojO40GBkZJGIzGZ6Eq66oMZeWXwjHsrZlt3sO1vjsAWfZZzVG612DL
qhJqJDK5hGxcehldXWM+/F53c6pW6+r+8s6H4ENGPPjnVS1B+TiurO/ZkA7X3ONK
7zZupNm7JBMgXrcW+BboZPpmsyJfjcR99o4BbL8fxFHWAids9NIpn6G3gGF/Glbs
J4e2CABhcP0E0vB0xHoA5uJkgez/ZEtLBf37LROgIP/nQ9NlzcWTJB+sGqdNsXlG
wdIWXYPBpDVOiq3f7EQpEzvohRAi5NfoyAwIZKsLg92CMdhX6JrHbXqDnF9pfNDa
2xegm4kLCIUPWShogRJHd3WlIJyzpILEWLFUEjcCx1doxq0IFL3/g2qcE1QIj0WR
ASaOJI+i2FDPE8h4c6DaTGqBERzN3jcIQLJggkDJ1dDvx7pCJUxW0nVLTmUH4K0C
Wswk6QFG7S3viCWH/rNzpSiXy2NvXgOS2VhdW3eVvYDOCQKpHLg9TodZadyxQ+Ge
N5Dhp/KLJSaELUDGUoiwQOMX5ksAtxnbvPahjv2U/ACSfiGXW28wldiO1uTWadYE
bjgrsCo5VbEmajtyc1mp9VXIKalBHE5TzbeKIoh9MEIExLpi2+O6VJ4dWTlPqN0s
XYi51USi6SzSFgI4NqhPJaFI8qac0H2+azmu5+hRIkwaVFSzp67+948OxqVMBdVL
k0Fh04TiaZvsDSU30Cey3L4a8dEv+sXGRl/RInWzpz3hoKTDrR1a3l8fbuCgevOX
IwrZ8yDuVniRZ5xccWN9GjbexhSjbt1rfXM4O+EGBesAjYtVR4RiSK6rIWVblEYe
hHnIUbj/LTPuiJKq4FT4JMaEYn6PbGVLf7102s3QiCT/655eNxoG4fvqi51aM+c9
Ozf2OjwWEOubF4Tgcy2TXVQKy7wINAPCNdg9X0od7WNWIkVCyd2+yafVzLVTseqR
OurfcqQlsjOyr5GNarEClrHoe/HlLhvkjH4UFM8/VsLp7JJMfCnOWASYNpn5dsIK
P2M9KwTDGZOahtDqJC/2hun8X2nUEmrCK3G8KBn0Kchdg65CDdYtQtsssdx2XnQk
VyBTqveA8/iuXUTmQKWXeSlr7GIqSW6hRdJGV6XiZqiB4r4xP+lybIKyqTXimCjv
9iLBs8/ZfxnZ5Jtb7qN/2necfFrenE7HAa2h5WHOe5XUSplCX3JjkZR2QCGmJUu8
HKVZRRW2XKIGVcwiPVFWrg5pVSQKm258tMc6a5AQ8FaGmXK6PNJOULCkXbXf9uwl
YIK6braFnwc0GZ4swRUZi3AF6noz764TKbF6FOxYcZpxVNKtPPvm56PkW0HkHG2m
kWoCZraFVzoN6aSjIzL/QLj8TpIIjvJjb46AhhN4wWoviZ36fUoRREug6x0vZWBa
5auIORlPbijQMTYSE2qIEwLB555Q5WetN8VHEUYO5wwzqVpvfSpI6I7Vbu+sYuqw
ed5RlFomfohSyh9JC9HmyBBMIB65uDJzgi5Pnfoohk6X4mx+vKRKf/4PWe9uzvdS
tBeAeNOEQB4kekZaHvtlolhDw7GaeAEy3lCJ7Msg9JUqvuajUWLsbqQuEvKh7LZ6
7IOBkVwYQdmGHkQM2Fhbm8L+Rp8TOBCXEW/y8XfAkR7EMFBzFfEys14p1PRkcym5
AkvmwngViTNIKtlqmtqOJBcwDzFW/7aaXswo57u0ETV90eJsNMFZDkbiYVbsfDYx
yW9Bn8j8CbjnHa5UdM6x4dhY/njGjMIi3Er7kpDG39iNf9mY6kd4pBLWCqvfRuIk
XPA9gP+fYv2EzvVbyow47fHZDogPVwrNiLMB/5ZQW4wsGW7KWmdr1dYxK4nmG4G1
9Zm6mg9CycoHbbmqJ12/ZwGEkiMRkA7yP0dPCijmLnGKWDkBUGsRGvffc/1U908l
QVB2+wD0kEXdzik2KdP0XeKA4E6ljsQXhi3VpWx0h6kCaoscc4evIpbdxi3XWVhj
wHjKOiNYM17Dc0mJZW6XAGFiTtaBZgeZoStDPyooSVfrJiNp/VMlenuOugUvdmfh
+t8PTNnURhXXEUrNIM+odPEcUZOijg5WaBj6QU3kT1Qb8wnbIZGumkLm+tQ78Uqy
h6BNYMLi7jMAa9xXGozz+bMklH1Iwx241ahHGxf0YQC8b3kMKySSNKtOTinRiH/0
vwlqA7B4TiO+Fv+ftNkzNnZlXfz+VlhB/WBtJIVjb73FN0W9Im7MxoshOjooh+cJ
tzbMkKpQvoE87n8rFxU8LsFLFE1cpVkBQ8zdYYx9aV/rGz3KGDstfDS0kQL0aCVX
BhruIlK6LU94MBmjTX2L93X2kbLxJnay3dPG3DgzBGUHvzod9M48240N13LhbRNS
vakeb+QvUUzGQRi34tFIxIpnFeVcQM7/j+0yKkrth3zP9CA9pEqdEKRzk1B8pdat
vD2klX4GJGDjXiZEBxw6EeJ01Mhwe5e5WhrVialzvcygsD6COGktvOdb3hhpzhbK
ksmuCYW1C8Z6OCRoIhDs708VDqfSr2AcDQCAyj8XdKDgV74JlvIDWvDWT+tjRmhm
8hlzoj6+iAdjp3IWOZXWSZe6S84uUFd6OMDY1vQKodteP1puJH6/5A9U5QR2ov7x
00/SZFSE4DjVbhWguj1oRhsOHEROzeVkTyQyKwNdf2kPhiXCDSFHpvonsdCrNgLW
ruek/HwhOFvhfOC2v4JuAFATHatBqhZkP3OnrOaZtzLXdwzz3xFUqCi5W2a4d8o6
XOB0fdKmQNgybwcWjqoy0Dc+IiFqc2Xzf/XMIVtfwVBpWYOS+eq7iMkpY22S5Dox
pn5F1ARcaI0jL7YNKW6MLpsMo3dVc5yx0oEQ0IJ94Xw7UmLMHGctNHcR5hV7Sr9K
b0l1dpumH9TXxZFygu+AXpx0VodmWMiT/U4y0TB23tCWLXCswzQ3pX/jN/RILtMl
dCGryg22Ym7ZOIumTxhdvGe1c6v/TwvisPjlHTnNjocGAQKFZMq8x3Z1tEoGVmpZ
qzCMNC0SxONGDolhcFbi2K0xWak26KOsUDs+5gLeodoySxd6CL06bx9Juhv8Wc4W
nts71I+eiVxY0yOEteJx3fBzI+pZC7d1DHbmCP+t3blSHhqdTonmSqZaQUoxgoaY
35BhOxpPZoxm9kMnQ/c8p4XW+D008y3AvBSMj5esp5zGLQUIVI/NVvZ+/HQarokZ
`pragma protect end_protected
