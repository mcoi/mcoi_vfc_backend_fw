// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:23 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
hocor6gJWVTZSD8j7ulDbHvEeCpQyR8EvWRlgmIDBz0ydo9dPBVbPVA2ZIwCM3qu
s/987j+o/pPYpcYDlE4mCeWbseh8+LFhd8J42JxPZqDsbxW9X+38YH2pJWsHNeKQ
8gqjbXcGsJHUCR3onRFVu6kHeth8tumdE23QkqI0JEg=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 4304)
erWbZqRt7KRv3P1GnQPb7vntXyH3ZC6SskTOSHwR2cgbao0aVvshCs4mfldwPFtf
9v+sq/yBMk0UlFemZMdv6m5kV3Gr61oXQfDpBGrM9b+4cAsWoK7GlbZ0JD6/ukc9
wnokWBJUY7nA3eMpu+D5c7gHlzUEJF8212ZjF+RV3rWdG0DUwbzkMmSnx38nXIAM
hirE+rMux/3K/KL94qxckjxeuFaShuQqHORa2SPr1oNp3S16oLv0cGMGBxbdTPx9
pju9RmpxXSz0o8M1+pFIR+KSBc2ZYE3eE7J2LKpK4LvUECJO4yY1lOa7Xbb7hVF6
2dPqq5BR5yyaNPgCx3YyN0XYa8iuCheFFhjSt4Jf8/6+VhhZofsJpFRubcf08j4a
t1Oj3fr3vXa1goA6RVXsPlfFOuyMTguUbZCRCpdRzGVoTrhaKeDYe8EKAleBP5Mz
7RVjMmd0gTNUyeD244b5g1cCzx0PeMswLEAjV/ogEKoQyvapuCWjIJTw0cMCmCA8
bviNSBsXOQGk3Nt/hc5b6HgYvFRitcEsZ3XvR8YQ/CuZ+3FeqheVGzswmW9NtapB
1u1XgT+43XI9qC48//TR9gOx1PgmGXkw/XRvFCE8wWpXMPOpUQ+crYRfASBU8szN
Wi0oHZDRS5LtyXM7NnxWY9ZfCMq4LV54qHc1wm/jWUh3yc2ZUMELSRfLZuNONCYz
YzmFnhq8TJkN8DVFRKnqbRw8MuoI0dyKDqEGQSH93l75vG3cy5bC79fIAfi3Cid3
Wec+ux0u3qjeDo3/vNVYeyF6XY8KCTVrgZSxz2UmWKobUGhFZY3MS4aoZxMRxVHT
PAq4lWJ72Ho1blCr6W27u9PTuesJWodYYQh4PGo7U7Co45dEQ2OsBBLNEI+833LV
rmH2YluoFrJAj4Ho37jFPcD1dVKLJOMdsS6GD5sBZ+L85KplZRUkN5ePV0mXeuSO
PH3Mcp7enBmiA+6V3s6JaOaeDR/hsEB86U/N5NtUCLfrIv2k6e3Jv66xrs9SHSA8
i+++2IOC34c7zxugoadqdA1eZEUNRw6Qbo6cSrt/hvhxd9lOcCzPOrUma2DngVz/
1ldnP03uRai4Hbu0d1mwi4vL3oOWR17p8zkIo8RcUP5nIQmiO+GJEKoajGDukijA
ZXVSoTgUddewp0auWWfEUMRvGaoTTWGnvr0lDPMATZ9EDlGMuKz6p5iBpIYkJuuf
x7FiMBWuYhBxhwzDlT/oPYJ2hvTBpti7LAW7zSFxeUBvoDaM3RHO5o9jxWOnWuew
A8JzMgVZLI+9laZ6ifT3zRbd5y4waUxYvAEdDCjkQoN3jcQaGuLEfF3Wwgtoqmo2
iW0hkoWI5++rjsvqMk2vXkypSowPbcx4x4K31RClRM7TDrMwTMdbpJWjQUH/tRXP
3Ta4/nYRYhgmkssIhs70DbDwI4Gp9mbuVlNSU0Lj3A4JvpzlrDGAFvOb3KH9oRs3
kl8F8pR8UPvk8ifJfinwBLR62dRhzYJm1rmeVhM+ayB0/jwkSDRwCJWHphjasNvu
2A+zUeh05YLIhWqdmOEA1wMFYOS/IOLugAln1asuwr7up+qQDetjySKydhZMUog+
D7snS/aeUScMGFOQDswpbWHYSGzsJrnWpTWJbwBl+IGKM0LDFNEk5hXWBexrqMzO
6Dd6Tzsi8hexDFr6gtcDTkWEc1PeFYhppubfKwZivvVQwMpQkOW8kLiH3sg9K/UZ
Sioj5U1iOlAzyYGCxsbDwzMKPrl8ezvIz3wp+kIbDnq5eUNWcTvLwsergb6Wtjpk
F5kOvJJV+oWLGIDY36Rz8tTxZ/n8cba1s9eD4BY+Sr3G5msaDVPFVlzyCt0eHqw2
X0pYbD3hIcyc6jzC95a7TeIsxI7NLQE45pqjSKDlJgiNPmZdys2T8toy1KapIx5P
fqtQwR/7GJ+IrfQ8fqglnl80Rn/LRsqO8/ss/mD+zeSQQf7Ejgj1fNNLWiI9Tvci
YSjJt5mnSjAvJ53sx8I5zQZ41HzHvOqAc7hJSpT2PjyJip7eEfjN72J4qLjOnP+Z
d7CNBC7jzGrv3yikvO3N/WI726kkovMDUNvXsfDcLwajhyCjMKBTGbuSnVuZShbo
g22M4FrNzvlDnI8zlcGazZ0rg971MYY6BJtVmrqBEc66/X4TfTCmO5UNZCegiHMi
i11Mrasn9Otw+6ydRoCwNm8tldr52Tsx8vjJMBRlZzG2YjkL0UYdS6ZWQ0ZtGwsZ
zdQK9EFSNY+BNZCPRoTmOuTHlIb4dnn88tYp5C/asnG7sj0FAfB4exUjOiTtKqja
9Xqnt4DyUJa9XeQw6eT7NnVOLh5wFC+cxtbts0Wf/Kq/n87KMmQTy1vKJpajWq9Z
/dONGhOZFijiJElopOYqth5FjWbyouwQlQhLL9ERXVyD0PMSh4rvOEepds4/RUJZ
8KqXzLKrpq0y9apTkxBPFqoKntcu9GCRLsiGzTtogEquA11Exb2AKneafEK/2v3j
2oPBfciu1cewX8fkJ+MbG21R1nbbgEEPH7gPfUKQdVjjpWVJ0Hh+yMgRZB0F7KgK
KOuqHaO3dCINJ0kHX+Ea8iYpIaGoOUR6wNmDM2UzyoiSGgpKdWOTpVguRpjF1Zqz
5/dhntQZ75noEFahEIgEsWcLQZenov+lvVGzWf0I0aO1DUUh49gBeXmiwbl3idqV
MJl28mlhZn9me9Uqi0exmyliL7qF4S3LOz8PCNtoyXmyh1bELeKsfMPUv2JG+gVv
7XMTYeBT2SqIclASl+Ih3nQOSl8CyCACZdAF9rHGmel+ogVyTIDt8+dlGowML414
RYjrjoYI4UgvdBeUxAejvGhhwLv+LDHI+Ni4aP3rs61E615o9rux/XrQQ03nH4bw
bKnq+Sfu6/M0Tu4Uj0373ogImmhSMQzb9UCukK6J+uvZPJeCigVo1hzJn775ffYr
qtk1KGceIwLIwsicFXmv9BIt735hRzsacE42MLGXYaFV/Ro2aepFxwyHVZncg6DT
oOd3ud/P9INxeud7BIalyLWDEsd6+mpT5JtFZ8gFZLdEdF+s1i8EEetq1SO0mGkO
hjMsImogRs8bXm+fVzbePdgGy8nA7JAZaQXVYNYeb1mm8qumjcIJ9V8ROmUdVswO
fihnmpeWey/EWKRucVZ26Jmn4XrFpkPIpNkTRJ5iTFDx5lSwI0bvxeNVPrHrtojR
tmhtYbC0KpO+EatEJoD32ulHSTehvS6w2lSiEobX9lz9ROa20sUom3skmPKCBCfo
LeCNBGIWhq9jkC1uz3VZmlBv73WdMlsjMHfPwq1Qz+VwKtBG/uzIQCryzEUeliga
LEMuJ8kWzJKYxIzCUaGCl+lY4VrKMSCDqOaBLTGwa9yBZxt5Wl5goaj9Xf06UnCY
Mf3i719Cg7oNAtlaqTCE5IhPd2a+LXdpoIC3S/g60y6TZcxXwLEnHNoXodwsiIwe
+Zrdi3bb57XhESKl3FDdKYOei18AxdaO33K/aBJjwdp3dzeen8L0XOEyW3pIicdA
TrEQsnDJwzo278xDoFePBjlMy3Pa5aNRFy5JJyhMAiXPy6S+Xlh6EaJGOk8TZflL
64C2l75H8n0GxMOX2SbT2EJmRFvFTTW17EVxtRts4yf3JoNE5PVeajg9wB0vgTTi
Z0OWa1TOSvRrDgfTFsA38l5eijWj7Oahn1za3cJR3izeQLI6mVeVjQ6k06HLS3mw
Wty3+gTqTIUgvRwYrNlELDyY9/cViKAGZ4eKrGxjkhge34nZqgE5xxzHAACRFidc
DCcA4QcOWrRL4xjqtVzCwUnzdnuiCoPOAMw5LpSASmUk/py7gC2gRCiC3ZM+7e+B
hgkCG7m+FlyTQzGO0iaTdSUyf+OZAWoZN3yAL8ZklJfbfCdhWFMmvcbewc7e0VL2
dTXRJN11jkrjZ+/+2JQTv4/sEu7piLoHNjhVLo/G1JjQ4LpGGfaQ/I98PJKLuUCh
O9+EpSt2tJreOBPdRPKVdPC1L7mnK4FNCeoATpzPBW/WIpIQqknBEpTAGUVNCYoV
tGGKtvCHcEdyYGx/nx8HbnqxdTH1WQKglYB79SgGwSm5aJq9S2oFkPMiocmUZ6C6
nYa2hOBM7x8pXSqkLYTjhvN5QHLRv3k+KDug1oc1My3hlx+Ghk49togRJ72elrhp
Ybt+4j5LfelUbZdswkf1J5a+udho/wMPwjVcJSfs3nMGA7LSq742j68mkmj3MAaV
FG7GMBHiag4RkGTIneQL3MHDmVj2Nj52IioFO0RQc7BfAGsDBtqAenX2CvsmD5ED
ygnM9jo14LrkiizDEvHtQ5cF6N6FPGlI7CMHi1zK5KAbwmhxaIzJ7W/fguhxcDP6
Xeiad/XxQaq2cQMG6nkxYACyntASg/uYzCc3qdpuDGlRDqWhQYukIgJHs+zj3Ya2
sh1OdNkTEpHK6zH0FqjFiQVbJJvyy/QU9odoWTkKtVFNTh2H9L2CSiUuscgR9DNB
xWUibNIpd5KCSX9Wj4p2blH/uukCXiLbFnY/k5O7LCcoXpLjOKp/HO/RKypdqitc
GnCr84kp9Uqml8ssUxY328MJk/Id0HhHjpEEbyA0I8KSNlQ80GtDyrzKRB+h0hoK
6+lNC40mPhyTt+UZFJyJiLl8MzrEn/71wQ0vvMnWGNoWt3GAGDReY4mj0Dh04+eL
CqXR+Y/FlJkAZ0wbin0Lr6g1xZ6x50FXQ89OiiBmJgEDjOZpsZfuIjX0aXwNLRa9
sTjt9VrENoTckEw2MIbZCbZ7ANewYgvsExnLHa89c+00cMTram/Fe9tStKUbZMz7
MbvwjonbrSrz6hFw2Lu7ZWIN92dmomIuzUQ27HRgzu8I07DGu0oy8fCXhPTrUtqQ
llOMGZwt/iEMT2Qc+CmluUygwfx+N/I5QFd0b6bUg3Y/fpLda7eSZm5M9lRzsp2W
gkpcKIOjXOsdcP4v1XJXOAdyGAhq3GCpUgOyW6ukzUR+owM1EiMg9roI0KDlzoTf
yXPQcbc0jZIRG1lC5ojOdTMWB43exfNFvN8HJfUHq9ys48ZViwl5WnXYkPH67xbM
YsVTi01YoHqoGMgzabtrH+5ZJKncgRLjm/jjQCUgVAWJfMB/FeY1ZHnIi1dwM178
CaUCXkEsfUk+mf+4BnVEvyytoDDEQrwT5T4/n2t7niugWhavhLW6s5blst6IVjUQ
tArRbN1/BeBLind/GChD6IS2VCcrgWpC3cJgybMkLDLNcI0Fz5Ad5TN4eI9e2PGD
wg6zrHgSGSo8DjNKo2R4io7rw8Ut+mJdM7wjzZiEQvVBNdtBpLJnRcJAAkbPfl59
aUsIptMLPe1RigEAo5C0tZEcCcQTOBB+IMOXkXb0/ZVPKVRIFEeyfwYTpAEXC8Ts
lPMdSnKrMG58dHpOyZFLdKPzjiZgQu8wWAPxxiQvZ0DCf8WVQGkxnfy9zrbppXyD
MqpIdC5XBtG8BD7YkG6R1N5Jq0vTcOwKbqruzeNbmB3YABV+cxTLSz9YTi9lPYr1
DG77eEoZrl6qlO6PWnNV26x7RzFtnNTQjroj3RsAvwqu3bEVONE2SSEFI/oshm3b
sz7SPyAcxO8Cp5kvAIZXp4NkGDtJISU69BNyv5eD+oVdjhQZlBM0ltAidkrppJBv
A0l2V+rr6JvcNM23IQxX8Gdk87Ailu/w1gfFZPwNL57g/trBKVQMvgDedHw46Vua
RqhmqXQmXeCuMH5CglTjo0N6E9g5LFkpXc9eN3b8DZE=
`pragma protect end_protected
