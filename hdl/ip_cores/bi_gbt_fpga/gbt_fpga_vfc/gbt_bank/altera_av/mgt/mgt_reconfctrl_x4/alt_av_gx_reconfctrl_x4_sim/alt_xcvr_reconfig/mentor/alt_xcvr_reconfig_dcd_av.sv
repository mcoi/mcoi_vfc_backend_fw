// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:23 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
rEriYzup3CeX3eRdgIHLoJz9lDbVUnV1QXTmQkwj+XFXzKaUjMLC4AwWRFOO8J2e
rulN1CkJC8KqW6h7ET3mDJBlzUXX4eDFAwnfqa620jrxyAzYjU9GZSWP+qh9zsE+
WmSJw9EH4YubH/jByKLsHSYhAjoeljSZ/vYUX1U2qS0=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 7360)
4rtFzOhsH8dsvHRn0/OJpwARjhHal/3V5FrniPO+fct1K/E4tKisGVCiEbSLJHJe
Us3+20cHOoJ+L6r0cVJsoH/IWsxjtYIzurwnH6BRyuWXff+IECVRPoKvPA/zsSg8
LAHR3o4bxdtqXVt89bEDYktbDBMHgxy74Nzd/U0qPw7gh/e+8gJvfg15wFqd7K9D
DV3WnVXekgjtqVzV/wSawAdjxGG119D7vjLgXKjURaUlIgldjonYqbM1o2pYPxFx
SyCEe7RBWnBlqUb2wjf48EE/biaKcmma+HNrkrpJgHNjK3VbyvbVQXCcZKCifTtJ
zAVyftykgn/F/U/FAUHUAjcvTicUCsFbD5Wk/3xy2s1K2EdquHGlsy9YlQS/gp3X
ggnLwtO2xUC1ce0snNiuTdUltpJZRcoVMUmq7U5gH0n5gqlRIKflXmZPvr/sMAF0
S9W6qsOy0eQ2HHcEJ05qFIl8Pmo6x1IXucfhI/19wskQEUWwshR1Gkyz4S92VSOp
9epJ8MA9tFpGovyqjCyTO524U2iInV6zzX8bF/fN8CsMwcGFUl+mtLcnv3T9+RJq
e4QQrgZ9pDHHa1CWRlKrbQzPnrLsfQr5c4slr+JqhcYO/AottdrFBkeTjkOc2wV+
B4NrwKZ3CJOD1UMk1yA6Cakz310TX60g5oYh1/i0IbOLtYzm2FJT8ABvBWt0pkAN
xbDhQ1BAoUPVKm4wojFZ7famk3Cs9lB8EStu0hdlrG8bNqbrER2mJ+GwBXEsnaQt
M/E6q3xu4++25LkQ37vdNTJ3qc+pD/Yt0TgOP7a4vtKSoyvNQDjAkIpquNBgib3i
jueF0rCxGmVpL3NGNTZeNkP2C088+h4Hw5xTDzDeM+gjj00+bsUhbiaoAD19sWfS
RSvxHL2z0a5QKjh7DlgvLEaIpuv3xHpmuxN82DXNEWKJ455uvIv73DS6xNCCaCJc
ye6LvFtP9f/vMNScx0yBCTjXEE4qs/YDhSFW8ruY+Ybtu2CwbqojnboASuwGYixs
9Z8NGEeovEXa891kzylkNFjhwezz/mNLvXhO8moIL5d3ZWJamV5sShQsUfBBfOVX
oaAwxK/e5FbuaAzeeQFXbW16Cf9vENsJL9ilvkGjvlDt9P/h9fFZXgg9Vf+0Sdr5
RqhwQnFsewebkjlW4dUykX1KRN5FH6+wt7HoDtJVIZa11NMKOm0yexVvIqaWFtLZ
Ywn+AduZx2Vj3qFh8j8ns+wVB+51cFwyUJkBV2YQXgpDPphSkM7kz6TAKCHm2XYr
kI60q7KIy9SWnPjBGxyWPH/jjMcNR6RaUZyl9i4zGVb5j7YzOFY9RZ56iZRc/NsN
+H5RlP3nMSpsdCu+v0toQ3Cs7XewuDRjIsFyaVFWw++GFY8pPVhkzI+TpiTZaQex
TDSkKO/tFEI7L9IS0uhjNuP5aaX2JWBufzvgFoYshXE0J6l3H7SdKT16t7XX2Ju1
yMS9N3V6tzMfGVL7h9wHgapH/FRkdWEBASC+LwJxnG8tNRsrCKTPMVQlwexn+ssO
678fuKVO+BzRrj0y0YDGzU5EXPQKt8LhK6XHjTSCAw4aPOI5smpeUdLEZYvnQhQL
QD9ZITVI27fVrd9Oy9yQ5DmVAV21RNhQAZdmksqE1ag8Wc0FWEfpLMTUe4faXmzm
tPF01D6/plyKOjNMtWsgldJqdxtuJFjTtP/FdgXlMgNjYNgzVek1q3rHicbH0fWt
Meh1e+0FUB6DZQ4kQ/Zm+WYx6CgxGkIWQ0VH7FOxwprTAF/FRTlGjCEM56xDKlbg
G/mkBlRm7foPgfpH3N9abBc5BM5nUR1L9BXJJLmwkXGzbUuL/BQlL+5RsAG7PVLE
Ggj2uMFfw1AE1loT4FWqa/ZSZ7FATwoIqxSpfTFW1Jw8RXvVTc91ZV+6/qb2YgGh
cTIz+LDgqecj9iSUWGpiVX35vtLU45bVZzRxzdHloRAFXUVPdxZykSgTD3oWJ+7C
NVxZTVxVfk94NSUD9T+mEDq0BeOQ2p6tZ6qquBCA5NxzUBBM9//myAkvqLRd4XIB
3Uu82+mtRZyGpWVnb+mm5v3ImIduRqIC8BZ1/dW27+p6O+n/ony+p4tgJO/hAbGr
OQ0bBenN/F/a8Ywq5zAa2ihA8IFTZQxSe9SQRMfOoqJnUcYPs/KCq9uRifj2YyZC
ifOM3pRApU3W0bhpbpBORmo3cYHX2aZYBGXHP7OWtk3nEqc0Ou8sOxy9TMr5baPG
JGWezqBIYLBumhGnxpufl67rMze5DHV0fCGnbukRGdsbbZV0t80GQRkeEEVqnwrn
vP2GnmIb+3TbrkPFuuvG7G1474StvbpjcHEDfmBxFZGd9Jg57DF0mjJ9Ykfkndw4
OT3CkYThz5ThKxL7CsHpEYT9+/iWgFu5iIcG6VRm5kF/Jvm7/aWx1Z4e1Z0G102F
3bfVMrBH37X43E8XMkjXc+KKtsS0Id7W5K+QMJjbpNkDPmgb+1BatDxgO9Ic+i72
welHNO3bSvtY+4FbjXBL+2IH8ebI7paSy4Mc+axGi5gSqD5vN0R2mKk2d/Jvpxm6
iUrPbJJuufvUYwI96Xouf/BFew0/tVsPFx9S1bsFkgMWQCOyDsEhaMiQYwt8YJVW
NrD9d0XDYvVfp5qzDE0s68wfWMBl+wQlWxuiF9TwdvCj0M0zELQgY1KQgHS/yekx
UphlozlONFEmHaOzv4KXXKsqXpVjxAo3UvubnMz3SrnN83WQJfo/IAs3T69f6+Ul
PEsJqZafN7bN9rnl+pziMcwv083U6EcxtXbj4BjohwqV4C0+WW901Pd3p9fJvgpu
mHwJW7x5d6IeFX3XWbtrAvZCr3m7QvW+kCng3SHxPUQqgZsEBkmkU6fIHGMWHV6M
AgGVZGn+3mc+v/raODIjZ53Awq8ii2/PeQYyobBGTRfqbJ9PhUh13/sy7ApcJ4wa
vIwnnhM0pE5C/R+Dm/xzjN8N0N0TlNEYRdS7AFJQ1DgyRoM7fkZQW4XbOMGyF/Ri
Ze+VHmIBht8TAD8GW4cZL7+IibFIfTa03a9aIPfmWafpzmzxlGd18DwrdE6RKktE
wzUI5Faxkb9bNQh3WRMFtKqj0YsvV/mEWwNrbIHL0T+ymCUFkKlshq0jorXmbvU6
s5shV8N8en77hjTVbPpWIRssb55hCb+9QzdKCgF6oeTZ3O3g1GmE/h+S+gOf63wL
2pBDdT8KtvAdamdZ53N0ZKfNjUp+ARaKgAE4+ZxGj+ZjuSmUV6NuZuMueLXElGz3
pr99jBGKJXzbIR3eRi3bQZwf8ifEcjJn8C3sWZgC5qcURSAiWB6hMm4N+XKCb0Iy
PQJQ7COq/hDNNo2jRNJ+0PuP5YPrHeN6X21Q5b1Mf5qO3Xn5JD5fBf8m3SawSF/m
kofoLe6sAisKDZKCegbCaIp4tuL/2OMGtNafmrQnu4nUd85GiafEZ79U3JeR7jSK
sUnRidVvatbgeMNeIgSsxeYYiQen7MhoWzWJC1lXXLakryLRNP7/0zAXk1NxNVnV
knydfqDeTm90uqwKw5y75OTc4745vR6CEpIbNLPrBaGFANlJAW3/tlqrU9se1BCg
td6MWn5QyEvyf88RRNF37sXUXMvMr3iLk8clOGcaNxAACkOWJF9FtlWS1SBucMia
LKDRsRkJ0vnpmqNRQigUmDn6dX2NKsEkvzEf1zu6lek6rtRYNU56mdRFmj94cnsS
fSvA3manjYcfBqmH8iWy3c3glyWOTVItt2LSZIoVKwpgFA0bICd16rLhTBMc2UnJ
+DSVl/taAciUxqbYVJrQyPIz1OZc+ryrEuxnDSMgl9MmG5mC6S8NA/NcJ+Yr1n6Y
fLjJMFFtJDTntsMYvw+q7me+gqT1JfNoW537cXlCASdJfvPN6HoUtXodKX9DlvVL
JpeQTJqFNaT2M2tzNAuFaPhNE1SAHHDrxJTu/WnJOd5DfbQNHEFqxwG+AaVtTMmF
kajoMR85DAcIZZRaVW2SqOIa/wQ609nvnJpY7FX9eDTXHx+m/0IgJVSCpDzCYSHN
+XKSLDTFTizZnOhW2YoVVgUzBsnML/QkBbMNrQb6pVvynZE7Qq/lSs8A5HkGJbIE
kbasTJh7yLv9B9hK524yzb/dfbAnVyqKoO+Cmn0+j64HzDiSRuPV7ww57Kt/yMCM
od9nmZkxXUMfuHPvauwY3dagLKkVrnzol0OVEvFP9o7FICDv7/qzYApq3L7RLHDy
uDpm9LTULswld0Kg08nVQxrIbAojCn/uc6JZ8HSJbpkKoLI2+1RcRA21Tqx3OYmA
YIGdJuRrgYma4B+vyjmIWxpyX3b2eL40j5dKVmLn/BDqDvOwUjdJRXFjaI1gZ70p
Pw3lwRXk61rveI+5xrlOvJLPQPT6Feb0TF2k2Or9OVTh2j2xwYkJgHpXWyan7vz3
BB3aoOwOF+57/OQ83eezD+71bptEfH0tR9979CvHKXwNLOxOHFtvPrl598CtQmKH
52lOCca49Rx6s0fxivw/0hVTvAza6wkiayGsw5bxvHS3qd3g/7q+0fQkJwm3E6zJ
W/4marU8bU7inz/LCycqP/V3Oo5f5wSt251T9YRPFRJq+WDcrvAq6Cz+BiLut6VM
8GihbGeptRFMyQft+z1xA/UW0Bb8JEbb81gYEiOye+WDX3ianurw3r8CjuCslNBD
Yh84iVQf+/kNCSAzEPw5IZeZg46nw1RSIaOIBFblRj+xVgTJg8FnJThypOQsnJ2d
vUCdXsmdiZF8gpzQwlqMGCj0INZNe+toFyMAII2Jh5WR4GusLCJ4CQZxqnCRTf6L
R9sptsy7PHFEbDejpCJG46j7nW8d0QKJALNbfqIJFqg0cSAbOOi/NwIx0H+VAqsA
jVvY/e7yE6ZEI72dDcxfjEP7RPVKca6VoejrBEzXAqWfJ9wW1jFOIn3YmtfRWhtK
wcpKbZ77OCsG4viDqXiuVD8ryQDC4bgSM5pUaQD8bX1Ov4u/p0bs+xdc6Ifc5Knb
dybYBnmT0qg2uj02soLlDhSLrpMjVkmEB5gUw4kBCgu8kcLTF9+ulkq0koj2koUL
2AsjpRZox25mu3nua+P0dkTY6tSxbEA98JF65cbFtHZvAf4ug2BeMBRtF0e0xTKL
+Fh41zACOoQmxKwpuvQAACgIKWWv6tAtcJ5gH98TXns40BOuXmsRychyN7BEAUH7
TWuKeFgHqHBLb4Hxfjdd+r9BFTNFHcLRkFEzFIMP3+i1UQczmn5Iy2iDxbvGneQe
kSE+9WTK1x0ruWjKYVGXNBaollxmuYAF0W6JE0nbXX72bMZ0LdcGy+AcAp3cYeL5
hBVfSvFnSry4NsnpDcwKX5dr7rmMMRJTT2R60ihpEjSetMiAjGCXeCQNs+Wn5foI
As+THGvRcG6N62ac0SsRDJmQV4lLeMdpnz3NBJSPyAGONzsWS/XvalleCRkyj5Qs
yJIxExebcN05rWFljq3PZm7oSDIRaa2w1r41m68UdfmLbdmpg9gcNGdskv62/MBY
mRt7+p6SrEhoLXf4FSbORplqBDzPA9rIgQ0qIZDqjsIJV6sOwUz1FEsuEHcToWwx
Telx8CiamjCK0P7dmjDlTUpz4SyWJ8me+f5X4ExexYDLFawB1PdxYXjgDDt3BmnM
Rx1Gf0h44NWT2f5c6EzcdUKOOABUSBO5X1rzNKSXS2c/Im+X6cR9nnYmC2GwTDr3
Njs81Hxi1wZk6TRrDEakWkHwTHfw8pmY0CtI3nR7HYPOJJUhQxFbT2psdibR2Ism
NFMhVM2NJb6CCyLmN9ljMiJOkzVsFygjABuFh4i3YwjVGFxgnmyqVbjSjxLzbVA7
9zX+EAtk9TmCezgIOte/QgMsVnEngWTrq5XMvgek2uSpkVM9DNzshtxyQ3cr3oBo
rejnMwhBORAHiPj73UXWFII9be8iIpovKNWUZK2V0ru8iMFUTiX2RS+rKMX6z6Y7
T13eOM6iQKtxuc3AlQDruocBcTUn36+DoO5MwVMWphvV7qjr0+WOaC910PBDdA/q
qJNMZQlx272WHY26Xy56YxZiQznSIhW2FSvTONUbqWWHZDgRDZ6bL/eD/r6NEEaJ
4DAtPgx0jNP6RrcAODJ9TJGlfGUvXbNaVfO8jPjrOUfZoyagxFLNnsGI9wWidvxL
YvqQAR0q4zIeauraMWR56u4lB9eTdch+5uyP1o59b4kvxjUavmDhYZBO87LGUWU+
p2hmh3wq5n0Plwx65F5PlRy8IGeSmEdnydzhAF5jj1yf5Ng3LT+JcZ7g8hovff+3
0jfN8G0Ik/MIpZEUKhxcFCQKRMt7aWRzNBUU54nB7e+pmOxKdVUmlH7e4qklhFBB
spOcf+R2SLeLut6NuykeAdLY/cY6Bxfmo1ZWiFhFHJdJEG7Owll0WTt8pCw+01th
XTbpc343NBC8EqDQDw6GiU9NlpssNEC38KexfM22RIi+5vpJiIOaFoB/NvWHi+Hk
/PsyjD3bG9q/si5ciCcR2Y5m3PtSL2+QXcjjd9jvuuKgpMzXd1SWkJrJ5tG+l8Cc
9/oB8LAK5aVAsahe0dEphXT0QIZjzuY1BGB9KZV5Yvak7LpVenexpLuOzKdULIeC
vRAHtDjtyLJ1CT/4jV/LfU+jv4cv9v1cAb/b4U6yp/jFteYXT6bGpZcoObOaYkkN
U2STn1oN8uytVcSHsyDUhLQULaLipnY5qoyaFd9gprBaNO+OjcdPbEiSjlecq1UX
rhfoVU7q7gU4Hnoy7w8NqG+7ssrS5oeII1mUlVfQz2BIvcF8TYkhT1nhLKGHOEO4
fg47nhmGQbFFc33jldCfFSswWhv64fk4ynQJgNzu1iZWzdr6LyImoGXox5g96KRE
JL/BbG6qoyVCeAXaHMRgaaJTZeeuQdeEdqN9Uulg0tzleZPknUcNXY2zYnufDWHc
Hc4wasZdPZDwQh8Pdke+rc5nURLEd1xW9SLmnspVfuWZLglKRnN6xCxab1CxBc2Q
IjgdfUIndawVGcWes56ctyR+QhRbMB2aAxwyw+DzTosgRrXerYjC2zShpIKoocHc
1+8RMYYe7mgKrrGjpbopo6PzPJZIvdMwkeWx+3C3kglzmk+tfi/aTLnvaKqVv+fG
vMET58nmqJmQMU4+Etrw/gO07Nr229UjFptXJKN2txJ+mLOY19pZYKa21CVTW3Ue
bWV151Q1sz4l4MEu4bAsNMJahKm1WVQ4Rju081AU2P5yb+DXc3xYv7XtC174gkGH
bRkvoubxWo2QEy1lUgxSonJKAD9WbToPPSpLdE8nBd5qwUBWWgoyKGJjzKzCjdPu
f484Tc6qs70oG4t4Ntp3f1YNWuaUl7kEBCmtT4SSegMLPKpaKFjIUcRBGrs9qCAQ
RT1D0JJ91qFpxoRQ3dDAF+Yg4ccHCWlX6TfA/q4lmOOs2/GSY3XysgWM2kQWiX4/
3tL30Tpi45yN9Tkmw8VN/uAftvYraNXM6KKEi1Sv65W6GCSvU8zO7Gxh6RE7WNrd
Von4468mg264Rxpjcs7s8UGZMum6kFNTfV7PXJKTSbNy3Byy5xPpUNDNs2j0w9Ru
8JTpkXjCa9l0wyBwCQsyxpG17E8laV2IXvBzd0EDd3tvC+whVu3bHrz3awMvDnkF
M7syKkJF9WOrLGdiVuO3ERZPnXG/oxhUpsV1ypk7ban/E20KUCXw8y0tJk9y9nn7
ahU2n2PuEiv7Uz5TB9NAPMTRh7EwvnYLiAAQBAdZK6yB4d8WAZkSbJU/A5R8/shM
LLuHIK+b033qjoLioeY+orfbhMz7SrChG3N8DGjncQAFG3r62L7bEBdo8ueYSm/j
pB9YuiQS/intwWOUXlMW+mHmGSldmyONPzW1j6/CfSq5WVKKF5+EDX9/xEuduObV
lEqssf4H6ks/FIg4Uf7kAWcJAUzrxDXFOZPW7OXQ0eyQ0VGEQpi5QNzzlm0IFJB3
lMpqTUCa3kkFRZyyQYEZzEv3uDx2b886qI7ieAzXRoEIhTtjnO9yNnEmBBrpkssO
y1unH6IpSo5sMjfqjqcadHl5HDTExS40kfpoJt8EwzbZf8jgqO5yDOr7flgJdhYP
fJK+PEYl/79FicD59WmxL0tQ4qZ/X5QwgIlfnMYZhWG51SYw0/TkT0PFOUC+DV4C
n6p09NPYcK6pMw/Rn6JG/Giy0WWSVEJOydTPzpyY46DwJB0xB979YBefYSsoNs7s
mlOCM59BhtNvG4AsyaEQeEED+8OojS7B7zTaeD7hpZjFc12EODG2FByjX/WHdbwV
nooqb3JnPN8LCyDE0paidoooOwzOaFIaeO5eMDP0lLl0lgayWxmCXmDtsjyPgYxw
KQL4zvMuTJRlkmkDttxzVohlH1mmHtuTW2mUNnaN9MR7E1k34GxijdRM8iiXNX07
q9ODvCEpPLKaSksQ/VP6jpOmMaTj0C50GcnLHt59uryyeQaOdNAj03H/JHdBeK4c
hJ4nEcZG9P5vTW+jsODNJIFsB+q9N9laCcZtKOZhKr6zDwP7TdVeB2TErjn6UnY+
48FsdT/ZjcEQF6X4ljlZ9LsVsVXwdyNvSUHaNgLDxInMj7DbZ4+Mi+FXybFCcWys
THXS7N1Cq8aKPy034dFof/YiNv83LIuW8qioQ2mCwFK6/LAcrkvJlhwPrDFFkgKC
bWV0I/rpVFOx2n0+PTjAjxfBj5FH1q+Ap8+g1wv5S646BrbLGeXvAGP9dvq93TsG
+2XwZSZfxu4XbTCtVaoYCwVzO5Wk6WmLJgRTlFKaPncYCoPw+zp7NQN0GWqRQLLd
JNdc+C41YDZEisMoWtfJ0XRwpJMMT3NQ0hH9UYH3/VPlbG7rhXkal0zo/yl0A1bJ
71ACuU2Q+uLpgCZ4fSlP8AEX72AahLs0cK7fY7Y1W+lWW1TIXP7dkZc/ZkLulH26
2ZKywXl5A+q6XPSvKr1XQkpEPQsYcUiQcyxK0OgjD3O1y82zQaq8olYqdih7Hk2G
p9+KCeJ2ibSY6N18MOHA/pc445AngeQwQqrvO/voZAbHDmeDWjIsE3rJWot1rrP6
I1F4tUXzYAxUV3sTH8l0t1mxdsxfPRdzZgbGfoeQQ+56Ofh2YLii8Hb+iG+QE3pj
arLoogvsJjLmjF45eHXaVLkqGYy1xrjn6/j0hvrr8ukXeL6ak3NE8f055K8IGjOM
5fkWEp6viCnJ8ooU5HK2SH24DGfIBCJOgEnNnbDN6MnCxHBPs77U5ei54OcdcjvH
/7sQwac7za9FcsVy/oU/E9XLIXefCiAWZ5EFeDK48uQC3+YOEpi68q1s8ZdHoK99
772k8pFjC+HG4WF1DWdvUD7qMvJHC6XbCQgEgBqx96WzJPC2Y8F4kjTXdS8W5T7l
1erVlioLa0rzI9Sx/d2need+ifoS8VJeWNMkw55LMBlJ4/o0yuUsRKV84Kaq6cKd
KaYiQxhDPxnLGZdlEiCbyKmjarwcZOGI+3qJLqBjvJzt+so/CdR/fOchMexFxDXU
/xAYIMreiQeSmlRjJsDjk7eO+0bOV4kPRJM71l9FfKRKlaB3YTnevyoRTuZgTsDA
2pit3JBALHHPaQdteD/IYziTNnG/hj83AVlKtlwULFLNOwCPC4lwAxRTd+rV32cm
5aqiiVwttr2OSixUGETmWnEmrPgwVJtY77Wr8kj0h5ORnJ0ZsIwv+5Ss8E6tk3PC
pKTfk0vgD++QmAawsommSh5kOBGj1U4SDNOeO8xNbB/zoWS6fPFj2k3e3Z6kL2Vn
7KdJEjx+0vG1h6kTYmfOeoRDVZ9nn7+dXz922Cr8pc8oduSaimEx2Ivb2+Or3k9s
AG7SqCOeGmemHAK12629CQ==
`pragma protect end_protected
