// Copyright (C) 2017 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the
// Intel FPGA IP License Agreement, or other applicable license
// agreement, including, without limitation, that your use is 
// for the sole purpose of simulating designs for use exclusively
// in logic devices manufactured by Intel and sold by Intel or 
// its authorized distributors. Please refer to the applicable
// agreement for further details. Intel products and services
// are protected under numerous U.S. and foreign patents, 
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 17.1std
// ALTERA_TIMESTAMP:Thu Oct 26 07:22:23 PDT 2017
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
CwJ03TBxD+T2b+QuLBjI47vOG/JD5U5B0RDSUN7HIwaRLJAiJ0nM8zoMJpD6/l4B
o3bi/lM38icAMb+W6GkZ0hLZKhP0tYC6yC9gmsteelAvGQ/eZV9TW5iXadsfcBvU
R/8F0U7bMhRm1DWkvZoiJ+vVT755a0iYvXcKINguM0g=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3472)
/1JHauzEOQnMhi65JI1dc7fx629o5XNM4FaSzlEimBpvY+7p/kdSYhtc7ZedTxhw
AMKNNq+vz1iVlb7xImlROAk5a8vQnQv6vGyH6kD8I9U+X9vP0KXttWQr8S4aNGjB
Tz+5/q9SqATLcpHvI3xzOxLppwjAepvFbYhe7B3Ry22LXDKB6/6TKgPxiofcvrxz
0XwxGRkTtcahROMa9r2NhArwCoKk4Zbg26ksMnoEAX3P/ILgna+81OOSVfTaf8YJ
8eDbRsTskMq3MTs7rwG7NSnT8zWn5Vbm4q9TbCbDmay8PPXjtkRI6/22YscaAKFE
/S32P9WeDCT621qPG1rh1Y4vl9MxRthRqVMRvJZyOVwJXkq3QHthwUmA7muywHVf
/WNdYW2m5KM2pxjWP5lVljlHmvHM0AJl+4n9bQ6VjGXAu6WP8b4ORCWVaFaN3l/2
w2a/csqtGwwQjXG+6VvTIScMbpumaMRGhlNwO7cslszJ/5hAsMst5BmcNLn9vBmM
urvkn+X2jMoxAXk4OWxIYEgbQvbzKIt9csyPbThH4j+IF9n2i71U13/hLRUK8R+h
IJv5jCuD2cLIPrZg3t5WdkiarcGgKf3DLfbTLkOoceipmOp3bkJmltBIdJ8yR1+r
ZK+fGKFVU0Jee+lB0x0qXITCgP+6PjsWf9JS7EnpprceVfmInI+E0mF1RQ6FbRvq
cwvZwWCjfAKcudVvZKkU4DyvWG+NpnzR3zAgricINOlL6wd01Agwm1/Afi6c8Md7
aoMN4QgHPTEguZT0VOB1AeI1qmv/rH+a+t/et9rS854VhLEte018e7L6vQ/hIrFF
uUPiX82oVO7cLwJUfMUy4etVw8tAeIBtNxy8HB4TRqXFeMQB+eBV12Mhr8GfsnWi
oWFa5RUAkctGyqTBUJE/K4zg78pZE1GMIPHVOYS+t+ZnOnzVMfvMRW3o4y8/BrqG
TyaNTLcTo2McHMFQo8FmFPyTd23ZplZ3hMrAAQ9NRHvRjs/Kob7tCRMPWq2IqDou
aunqSrv6qfncSVZ8T57gNz71evrSl0P/Z1XNG44cU2wIyXOmCkPF/srEpEmn+bNB
A4fTIUTUDIUEnhZ2GYYc+NAzkI9edxVQp8yaFFu2NDkKYeSjLypdEwT6E9Gpgd1x
g7ZC1hE7yc6mV6EOi7ICo1A74cM5nOE9BhghsY51Isa8oFcOqHSWL5gWDe1Iuelp
iWgghQZtUvVgjQMSTs4aNd7Q6gMl3+oplDxsAcZHwX4eA/tRlxmCVuKXHn69fOTV
k0jfBxr7gqc4gaNZNNcQOwtfeiHHULZ4cFypR4M/x5gHxDYMHLrmpqSbKqdQvwjj
EN57qZ4JhyU53MLJSLoP2P8V3pZv/DRwY9f2zHfQ3hK0A/c/i1EU4lI/iLyOG3Q8
rYyf57yXLUoNytHUUfsYj4DbdIxhAE0f2pLwM23UlDqUkPnT+DNS8EpdV6B/E9sk
SVRim3+X/Hlo2/BiDivCp9hCi1lGiJTbo+dLimKVDPpfEjyn7Et+GmaSXVPx9aie
IqqxL1+i5TeDGdjeZiJEscDycnmg2P/kx/gxQyEX4I0zI/BKqygci1xDRQC3au2f
kfSBL6Ux4bNnVhDXI2/1wv7beYXJdDwHzucuLjFqisK2D/SD6LrLS6MMGsNin/3b
w7uQFm/rws2MIUIEFbJ/6NN70kKq/v/K3PQIrzZEkw9HdLZlQajB1DTCylpsGBTO
NtfYwbrIfVkQHcLKsL+OdSLAndrTnmN038rDTJUfWS/382zFPXpggx4vwXHX3A5P
LrCLbx+QVlG9LrZbgIXnae1r900nkdB/XHNLp0x7deASpD7YxUTscrnzoyRwi/9d
P0d0MQSZwTpbGbHSaAexirAgiGfgLdjFXidWh4YSRL1Wug21wWFK2fbGdzTs45Pg
9Y/mWGQ2oF89dRdFeUAreT7KKfebp2m1ZSueuGnkCRuauMa1wi98ihdu2ByK2SZT
i1d2PIZo6WxrjlDzR8TWBf0nSd4NwRvzoUXmnb1GsQmmySd/O+ZgCJ1cYXPbn6mg
qT/Va8yih4kjmBxyBVND8Mj7vIpU7G5IXbgEXiFo1bTsLaK2Bpx4emayycnyag2f
m0eY+Om7ujiozQWxv4HrhoPhrtYOGsgXQLNAsIGuTMUFLfXlavMseVsCd8c5KoOL
kije6jTSPZPLb4Gn/gxi3Mo2e05P5XtSSiQmUUakTndswcHPw5PTTs7457xB06a/
fs+IZzCpPgMymfQYsI7G/OX5HwPkgvEq/g7MEXWhZvUFui0ie85Tz1+3Zi5gDrht
YtGHMjTR2mkWKAzGPGQHbAm8/fnenxg//+Taz0W5mbdN53AtFwMPYlV5E6fCXeM5
q03HCBQpnEbn/P4RKQmc8xp66G3zp4VWrQQ/Astq3WytcoCfCxCbJpX7iBzZ/9yt
FpAMB9YtqqwNKnFVGHvWExpO6LPtvr6VJ7x4HL7UGLok1kPatAo4Eyv/tjwLhFja
HK3Al10fiOAO6LS6U9JHOfwQeNNqw7H815shS8/1KnEGFlRguOnA62W7Aqw3T9av
y5Z+c9uv5I787lO1wxQB5HvzKO1beKktAlILRxBFbYItmwNelXfIQA/XgXMi1xoI
sWORF3U111zXrcoj6FCGgPcdYjbB11xhrNgom/Hi/lyIgloBvE6jEqHeJyxB2i+o
iddzh/LGnHxEHAhu1h0KYB5lWkOaKIxGOcxrwfypp3pYLgV7vv7DkqvfJu3tPkL/
uiu5p3ZY0I06eJs0Gaj2NDgl0L/KAqHR0Mu3PQ2wyHAteLtcwpekAmaUG5JxhPYn
DGS2SnWWqRAOqmpmpImDb4zS1Bhnvpg/NxMXpaSCoPbI1zWcW5Nhc9IHNwbxKGBw
f+26o2dywm9rLhV0GCa+lF3lykTlQCFiZfqT97dSIsc3UOwFZHEb6MjtMxuThmvI
mXckLHRNRzbn+bWSnmqKsv+Lqt9NG3BVe/erCjMhnwlsmsKE5dRUfxu9nq5QlfF8
ffpBSjCC5XXMH6VezqU5mNVk5HC2BAkyvYybFeSyI6MoX5FmWER3+Be8KvYGkje7
JkHJB/otZh4qBBoBc7O5XwEb9JqZbO07R38dXcdBdl2yUtNATJe5rT+T9FgZwquq
/4KVlvDkPeIrU9EVJa3wGkiTS4pHi4YOirzMeY9T1eTFrmS+iVTwKeuaTp9IB8/c
G7XUT64UJl9RvcnOqbui6WZwC4WwShM3AGlI9qx1CLf4Kxs9HUJqJ95BLPSu+Ggv
zJVuoUpqEbUgBqGpL9RZg9P6Jl5lzCiZquiHTVTW/OVXBVfo3zg/+fL1j4uXhbhi
JyaUeu0s0bOOJfQDo61aZhvC+mEH8nlccjFA3px/u9g5e4/yrUtd/T5L/j35jtuU
GN2zvopcmVSacroFHg/Wy/4Rw7y7uB6pu/lSk3B5PvOpN1RoCR/9Dmq2Q3NMHQRH
IxYaRCXsYEYUUnyAlJro/40RE5x9llIsR79bBE3a37B7kkI9yYscaZF8265mjJFa
PbYUBceYOQS+fmaxABj2jj1Yjd6vUUcXrmGXq76O/ADXmqs08SuiS7bgOoGVpcws
gz/woU2TudHW5wAwWsxqjtqab/XVBpYW3hHSFW3jkPKB06kWkFcbPlFgGJwia5Q2
epLFHWLQpu/vkkW0BEZInCTUM3ApQATcuo9P20dAKohQnYCdh5N5OMk0U5Od2X8p
ym43P0rpJ7NCnJE0cUEbpLOg8aMo+F+ldG3QeiqYCIkImr8DuF3jx+rwhsL8+sqd
nnwe4kFZek528W6MvdHeG2G2BSmvrK+wz1COf3Xplmh8TUedpI2G+/N0aUmdjjVc
tNxwdJaYxrm5Vf6CQP7gWdonL9zxx5nhFlpQgBGfh/E1waSWpj+pLoCviRpDt7Pf
XiHcRFqYLsgK8De7yVEqpNs9378L1rjhJz3RplPSF+8CtUYTPzZH3od3clK+LpMh
n32HS5qp5+r5jA3PUF/YIaQpVrxFrrDnSO/38jDvkblcE6HmnROiJaVBdO0G2MQj
vu/d0yKnEECYJrrlVVxSDdUr1LltDfbNJMaaQEFxkTVFkfHLpdyOMPinE6YLzeeY
4CKvgFa0AISCh1wMuu6F75QtqRdPUJiu6k869ZkjxXWDhPzH7BoYeIolBRKS+dqa
jjCAehJHPsTb6llliqci8HGGpCLjh52hunJQRmBnj9Tl3+E6Ijm/xznwtN8nXGmT
gJfOSrCisFGc+waecTu1Abwdj/KxeD2s1BfaMMZBRZfKk+aCBppW9zUMHYyKf4LK
+lluExbD9yHHypJ9svB/TxsULRwZwCISF4ZMTBWPWmGGnVhxIS8/tzmiKOmUg6+O
RDk3ywScMs3W+26/fQN7rfdRxht4/PCRk+RjvgFPHLZYelbmaWIQrEV2ElW9rgZt
zsG93qWAH9gr7rtM5GO6LEIV0CYEzlxhLXPCDPthdNggC1/cQzaf74XciaUxAxIJ
MLn4v221hBmSoLHy/n2XtRwdbr5L3M9hZ0p5tzvry7Ckhyx0MJd11DrXWPBvGeRw
P3lmkoLxuGHGpa544dCWvWJDASszf3hy2LCO6rfdjkYhLVfsTedNNZGPAj5dX6wA
oqLl1q7JMc6sDPyXrnrI9g==
`pragma protect end_protected
