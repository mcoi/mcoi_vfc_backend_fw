
module remote_update (
	asmi_addr,
	asmi_busy,
	asmi_data_valid,
	asmi_dataout,
	asmi_rden,
	asmi_read,
	busy,
	clock,
	data_in,
	data_out,
	param,
	pof_error,
	read_param,
	reconfig,
	reset,
	reset_timer,
	write_param);	

	output	[31:0]	asmi_addr;
	input		asmi_busy;
	input		asmi_data_valid;
	input	[7:0]	asmi_dataout;
	output		asmi_rden;
	output		asmi_read;
	output		busy;
	input		clock;
	input	[31:0]	data_in;
	output	[31:0]	data_out;
	input	[2:0]	param;
	output		pof_error;
	input		read_param;
	input		reconfig;
	input		reset;
	input		reset_timer;
	input		write_param;
endmodule
