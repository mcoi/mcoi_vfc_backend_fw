	remote_update u0 (
		.asmi_addr       (<connected-to-asmi_addr>),       //       asmi_addr.asmi_addr
		.asmi_busy       (<connected-to-asmi_busy>),       //       asmi_busy.asmi_busy
		.asmi_data_valid (<connected-to-asmi_data_valid>), // asmi_data_valid.asmi_data_valid
		.asmi_dataout    (<connected-to-asmi_dataout>),    //    asmi_dataout.asmi_dataout
		.asmi_rden       (<connected-to-asmi_rden>),       //       asmi_rden.asmi_rden
		.asmi_read       (<connected-to-asmi_read>),       //       asmi_read.asmi_read
		.busy            (<connected-to-busy>),            //            busy.busy
		.clock           (<connected-to-clock>),           //           clock.clk
		.data_in         (<connected-to-data_in>),         //         data_in.data_in
		.data_out        (<connected-to-data_out>),        //        data_out.data_out
		.param           (<connected-to-param>),           //           param.param
		.pof_error       (<connected-to-pof_error>),       //       pof_error.pof_error
		.read_param      (<connected-to-read_param>),      //      read_param.read_param
		.reconfig        (<connected-to-reconfig>),        //        reconfig.reconfig
		.reset           (<connected-to-reset>),           //           reset.reset
		.reset_timer     (<connected-to-reset_timer>),     //     reset_timer.reset_timer
		.write_param     (<connected-to-write_param>)      //     write_param.write_param
	);

