/*

access to VFC EEPROM and remote reconfiguration
Jiri Kral
jiri.kral@cern.ch
*/

module vfc_configuration #(
    parameter g_Synthesis=1,
    // SETUP_LOAD_ONBOOT allows automatic load of boot settings from EEPROM
    // This option should be off for any application FW
    // The settings contain Watchdog configuration and application image address
    // See BOOT_SETUP_ADDRESS_SECTOR for details
    parameter SETUP_LOAD_ONBOOT = 1'b0,
    // AUTO_APPLICATION_LOAD allows auto load of application image after a delay after boot
    // This option should be off for any application FW
    // No matter of this parameter settings, the auto load will not be executed if application image was loaded already.
    // This is achieved by checking the AnF flag that is saved into the reconfiguration logic on the first load of any image
    // executed by the factory firmware
    parameter AUTO_APPLICATION_LOAD = 1'b0
)
(
    input   ClkEeprom_ik,                // clock 20 MHz
    input   RstEeprom_irn,               // syncronous reset

    // wishbone interface
    input Clk_ik,                        // wishbone clock, must be slower than ClkEeprom_ik
    input    Cyc_i,                         // wb
    input    Stb_i,                         // wb
    input    We_i,                          // wb
    input    [14:0] Adr_ib15,               // wb
    input    [31:0] Dat_ib32,               // wb

    output reg [31:0] Dat_oab32,         // wb
    output reg Ack_oa                    // wb
);

generate 
if (g_Synthesis==0) begin: Simulation_gen

    always @(posedge Clk_ik) begin
        Dat_oab32 = 32'hdeadbeef;
        Ack_oa = Cyc_i && Stb_i;
    end
    
end else begin: Synthesis_gen 
    
// timeouts for application image load
// by default, 10s when watchdog is enabled, 2 minutes when it is not
parameter AUTO_APPLICATION_TIMEOUT_WD = 32'd200000000; // 200M @ 20M clk = 10 seconds
parameter AUTO_APPLICATION_TIMEOUT_NOWD = 32'd3600000000; // 3600M @ 20M clk = 2 minutes

// configuration of EEPROM status register to write protect a selected memory region
// the register is 8 bit |NA|BP3|TB|BP2|BP1|BP0|NA|NA|
// TB selects orientation from top(0) or bottom(1) of the memory
// BP? give the size of the protected area
// Qiad-Serial Configuration EPCQ Device Datasheet page 25
// one sector is 512*512 bytes

parameter FACTORY_PROTECT_AREA = 8'h60; // 60 = protect 128 first sectors from bottom (0,127), 3c = (0,63), 38 = (0:31), 34 = (0:15), 30 = (0:8)
parameter ALL_PROTECT_AREA = 8'h7c; // protect all sectors
parameter UNPROTECT_AREA = 8'h20; // protect nothing (keep the TB bit up)

// address of the boot setup register
// The factory (golden) FW will read 16 bytes starting at this address to acquire its boot configuration
// byte 0 - ID word 0xeeee1234, byte 1 - watchdog timeout, byte 2 - watchdog enable, byte 3 - page select (start address) for application image
// If save reconfig command arrives, the current values are written to the EEPROM
// Place ideally right after the factory image. Whole sector is required, since the sector must be erased in between rewrites.
parameter BOOT_SETUP_ADDRESS_SECTOR = 9'h080; // sector 128
parameter BOOT_SETUP_ADDRESS_BYTE = 16'h0000; // byte in sector

// default application image address
// this parameter is overloaded by data from boot setup saved on flash or by user
parameter APPLICATION_ADDRESS_SECTOR = 9'h081; // sector 129
parameter APPLICATION_ADDRESS_BYTE = 16'h0000; // byte in sector


// EEPROM communication registers
localparam c_SectorRegAddr = 3'h0;
localparam c_CommandRegAddr = 3'h1;
localparam c_StatusRegAddr = 3'h2;
localparam c_ReconfigTriggerRegAddr = 3'h3;
localparam c_ReconfigTimeoutRegAddr = 3'h4;
localparam c_ReconfigWatchdogRegAddr = 3'h5;
localparam c_ReconfigAddressRegAddr = 3'h6;
localparam c_ReconfigAnFRegAddr = 3'h7;

// command register codes
localparam c_CommandBulkErase = 4'h1;
localparam c_CommandSectorErase = 4'h2;
localparam c_CommandFactoryProtect = 4'h3;
localparam c_CommandAllProtect = 4'h4;
localparam c_CommandUnprotect = 4'h5;
localparam c_CommandEn4BAddr = 4'h6;
localparam c_CommandReconfigSaveSettings = 4'h7;
localparam c_CommandReconfigLoadSettings = 4'h8;
localparam c_CommandReconfigReconfig = 4'h9;
localparam c_CommandReconfigCancel = 4'ha;
localparam c_CommandEx4BAddr = 4'hb;

// header for setup data
localparam c_SetupHeader = 32'heeee1234;


// input FF layer
reg CycI_q;
reg StbI_q;
reg WeI_q;
reg [14:0] AdrI_qb15;
reg [31:0] DatI_qb32;

// clock transfers
reg CycP_q;
reg StbP_q;
reg Cyc_q;
reg Stb_q;
reg We_q;
reg [14:0] Adr_qb15;
reg [31:0] Dat_qb32;
reg AckP_q;

// vme bus
reg Ack_q;
reg [31:0] DatOut_qb32;


// initial reconfig
reg ReconfigTimerAllow_q = AUTO_APPLICATION_LOAD[0];
reg ReconfigTimer_q;
reg ReconfigTimerReceived_q;
reg [31:0] AutoReconfigCounter_qb32 = 32'h0;

// EEPROM and remote reconfig state machine
localparam s_EepromIdle = 4'h0;
localparam s_EepromRead = 4'h1;
localparam s_EepromWrite = 4'h2;
localparam s_EepromBulkErase = 4'h3;
localparam s_EepromSectorErase = 4'h4;
localparam s_EepromProtect = 4'h5;
localparam s_EepromEn4BAddr = 4'h6;
localparam s_EepromEx4BAddr = 4'h7;
localparam s_EepromAck = 4'h8;
localparam s_ReconfigRead = 4'h9;
localparam s_ReconfigWrite = 4'ha;
localparam s_EepromReconfigSaveSettings = 4'hb;
localparam s_EepromReconfigLoadSettings = 4'hc;
localparam s_ReconfigReconfig = 4'hd;

reg [3:0] EepromState;

// EEPROM access and remote reconfig
reg [2:0] EepromCounter_qb3;

reg FlagMask_q;
reg EepromRdEn_q;
reg EepromFastRead_q;
reg EepromShiftBytes_q;
reg EepromWrite_q;
reg EepromWrEn_q;
reg EepromBulkErase_q;
reg EepromSectorErase_q;
reg EepromSectorProtect_q;
reg EepromEn4BAddr_q;
reg EepromEx4BAddr_q;

reg [31:0] EepromAddress_qb32;
reg [31:0] EepromData_qb32;
reg [7:0] EepromDataIn_qb8;
reg BootRegisterAccess_q;
reg BootFlag_q;

reg [8:0] SectorReg_qb9;

reg [23:0] StatusReg_qb24;
reg OperationNotExecuted_q;
reg [15:0] StatusClear_qb16;

wire [7:0] EepromDataOut_b8;
wire EepromBusy;
wire EepromDataValid;
wire [7:0] StatusReg_b8;
wire EepromIllegalWrite;
wire EepromIllegalErase;

reg [2:0] ReconfigParam_qb3;
reg ReconfigReadParam_q;
reg ReconfigWriteParam_q;
reg [31:0] ReconfigDataIn_qb32;

reg [31:0] ReconfigWdTimeout_qb32;
reg [31:0] ReconfigWdEnable_qb32;
reg [31:0] ReconfigPageSelect_qb32;
reg [4:0] ReconfigStatusReg_qb5;

reg [2:0] SettingsProgress_qb3;
reg ReconfigReconfig_q;
reg ReconfigAccess_q;

wire [31:0] EepromAddress_b32;
wire EepromRdEn;
wire EepromFastRead;

wire [31:0] ReconfigEepromAddress_qb32;
wire ReconfigEepromRdEn_q;
wire ReconfigEepromFastRead_q;
wire ReconfigPoFError;

reg ReconfigPoFCheck_q;


// ASMI interface and Loader
wire AsmiDclk;
wire AsmiSceIn;
wire [3:0] AsmiDataOe_b4;
wire [3:0] AsmiDataOut_b4;
wire [3:0] AsmiSdoIn_b4;
wire LoaderAccessRequest;
reg LoaderAccessGranted_q;

// Remote Reconfig
reg ReconfigResetTimer_q;
wire ReconfigBusy;
wire [31:0] ReconfigDataOut_b32;

// input FF layer
always @(posedge Clk_ik )
begin
    CycI_q <= Cyc_i;
    StbI_q <= Stb_i;
    WeI_q <= We_i;
    AdrI_qb15 <= Adr_ib15;
    DatI_qb32 <= Dat_ib32;
end

// transfer the wishbone to the EEPROM clock domain and back

// to EEPROM clock
// delay the Stb and Cyc by one clock to ensure that We and data are well stable before
always @(posedge ClkEeprom_ik )
begin
    CycP_q <= CycI_q;
    StbP_q <= StbI_q;
    Cyc_q <= CycP_q;
    Stb_q <= StbP_q;
    We_q <= WeI_q;
    Adr_qb15 <= AdrI_qb15;
    Dat_qb32 <= DatI_qb32;
end

// to Wishbone
// delay the ack by one clock to ensude that data is well stable before
always @(posedge Clk_ik)
begin
    Ack_oa <= AckP_q;
    AckP_q <= Ack_q;
    Dat_oab32 <= DatOut_qb32;
end

// use the following timing constraints
/*
set_max_delay -from [get_keepers {vfc_configuration:i_vfc_configuration|CycI_q}] -to [get_keepers {vfc_configuration:i_vfc_configuration|CycP_q}] 8
set_max_delay -from [get_keepers {vfc_configuration:i_vfc_configuration|StbI_q}] -to [get_keepers {vfc_configuration:i_vfc_configuration|StbP_q}] 8
set_max_delay -from [get_keepers {vfc_configuration:i_vfc_configuration|WeI_q}] -to [get_keepers {vfc_configuration:i_vfc_configuration|We_q}] 8
set_max_delay -from [get_keepers {vfc_configuration:i_vfc_configuration|AdrI_qb15[*]}] -to [get_keepers {vfc_configuration:i_vfc_configuration|Adr_qb15[*]}] 8
set_max_delay -from [get_keepers {vfc_configuration:i_vfc_configuration|DatI_qb32[*]}] -to [get_keepers {vfc_configuration:i_vfc_configuration|Dat_qb32[*]}] 8
set_max_delay -from [get_keepers {vfc_configuration:i_vfc_configuration|Ack_q}] -to [get_keepers {vfc_configuration:i_vfc_configuration|AckP_q}] 8
set_max_delay -from [get_keepers {vfc_configuration:i_vfc_configuration|DatOut_qb32[*]}] -to [get_keepers {vfc_configuration:i_vfc_configuration|Dat_oab32[*]}] 8
*/


// initial automatic application image load
// issue reconfig signal given timout after the boot
// the tiemout depends on the watchdog being enabled or not
// ReconfigTimerReceived_q turns and stays 1 after the first try
always @(posedge ClkEeprom_ik or negedge RstEeprom_irn)
begin
    if( RstEeprom_irn == 1'b0 )
    begin
        AutoReconfigCounter_qb32 <= 28'h0;
    end
    else begin
        if(( AutoReconfigCounter_qb32 == AUTO_APPLICATION_TIMEOUT_WD && ReconfigWdEnable_qb32[0] == 1'b1 )||
             ( AutoReconfigCounter_qb32 == AUTO_APPLICATION_TIMEOUT_NOWD && ReconfigWdEnable_qb32[0] == 1'b0 ) || ReconfigTimerReceived_q == 1'b1 )
        begin
            if( ReconfigTimerReceived_q == 1'b1 )
                ReconfigTimer_q <= 1'b0;
            else
                ReconfigTimer_q <= ReconfigTimerAllow_q;

        end
        else begin
            ReconfigTimer_q <= 1'b0;
            AutoReconfigCounter_qb32 <= AutoReconfigCounter_qb32 + 28'h1;
        end
    end
end



// status register construction
always @(posedge ClkEeprom_ik or negedge RstEeprom_irn)
begin
    if( RstEeprom_irn == 1'b0 )
    begin
        StatusReg_qb24 <= 24'h0;
    end
    else begin
        // publish status flags, bits 16-23
        StatusReg_qb24[23:16] <= {ReconfigStatusReg_qb5,ReconfigPoFError,ReconfigBusy,EepromBusy};

        // persistent status flags (user erasable), bits 15-
        // illegal flags are up for 2 cycles only
        StatusReg_qb24[15:0] <= (StatusReg_qb24[15:0] | {13'h0,EepromIllegalErase,EepromIllegalWrite,OperationNotExecuted_q}) & StatusClear_qb16;
    end

end

// state machine that processes the interaction with the EEPROM
always @(posedge ClkEeprom_ik or negedge RstEeprom_irn)
begin
    if( RstEeprom_irn == 1'b0 )
    begin
        EepromState <= s_EepromReconfigLoadSettings;
        BootFlag_q <= 1'b1;

        EepromCounter_qb3 <= 3'h0;

        FlagMask_q <= 1'b0;
        EepromRdEn_q <= 1'b0;
        EepromFastRead_q <= 1'b0;
        EepromShiftBytes_q <= 1'b0;
        EepromWrite_q <= 1'b0;
        EepromWrEn_q <= 1'b0;
        EepromBulkErase_q <= 1'b0;
        EepromSectorErase_q <= 1'b0;
        EepromSectorProtect_q <= 1'b0;
        EepromEn4BAddr_q <= 1'b0;
        EepromEx4BAddr_q <= 1'b0;

        EepromAddress_qb32 <= 32'h0;
        EepromData_qb32 <= 32'h0;
        BootRegisterAccess_q <= 1'b0;

        StatusClear_qb16 <= 16'h0;
        OperationNotExecuted_q <= 1'b0;

        SectorReg_qb9 <= 9'h0;

        DatOut_qb32 <= 32'h0;
        Ack_q <= 1'b0;

        ReconfigParam_qb3 <= 3'h0;
        ReconfigReadParam_q <= 1'b0;
        ReconfigWriteParam_q <= 1'b0;
        ReconfigDataIn_qb32 <= 32'h0;

        ReconfigWdTimeout_qb32 <= 32'h0;
        ReconfigWdEnable_qb32 <= 32'h0;
        // default page select is one sector after setup data
        ReconfigPageSelect_qb32 <= {7'h0,APPLICATION_ADDRESS_SECTOR, APPLICATION_ADDRESS_BYTE};
        ReconfigStatusReg_qb5 <= 5'h0;

        SettingsProgress_qb3 <= 3'h0;
        ReconfigReconfig_q <= 1'b0;
        ReconfigAccess_q  <= 1'b0;
        ReconfigTimerAllow_q <= AUTO_APPLICATION_LOAD[0];
        ReconfigTimerReceived_q <= 1'b0;

    end
    else begin

        case( EepromState )
        // wait for any activity
        s_EepromIdle: begin
            // counter erase
            EepromCounter_qb3 <= 3'h0;
            BootFlag_q <= 1'b0;

            // flags erase
            BootRegisterAccess_q <= 1'b0;
            ReconfigAccess_q <= 1'b0;
            FlagMask_q <= 1'b0;
            OperationNotExecuted_q <= 1'b0;
            StatusClear_qb16 <= 16'hFFFF;

            EepromRdEn_q <= 1'b0;
            EepromFastRead_q <= 1'b0;
            EepromShiftBytes_q <= 1'b0;
            EepromWrite_q <= 1'b0;
            EepromWrEn_q <= 1'b0;
            EepromBulkErase_q <= 1'b0;
            EepromSectorErase_q <= 1'b0;
            EepromSectorProtect_q <= 1'b0;
            EepromEn4BAddr_q <= 1'b0;
            EepromEx4BAddr_q <= 1'b0;

            SettingsProgress_qb3 <= 3'h0;
            ReconfigReconfig_q <= 1'b0;
            ReconfigPoFCheck_q <= 1'b0;


            // transactions or commands
            if( Cyc_q == 1'b1 && Stb_q == 1'b1 )
            begin
                // write to sector register
                if( We_q == 1'b1 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_SectorRegAddr )
                begin
                    SectorReg_qb9 <= Dat_qb32[8:0];
                    EepromState <= s_EepromAck;
                end
                // read from sector register
                if( We_q == 1'b0 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_SectorRegAddr )
                begin
                    DatOut_qb32 <= {23'h0,SectorReg_qb9};
                    EepromState <= s_EepromAck;
                end
                // write to command register
                else if( We_q == 1'b1 && Adr_qb15[14] == 1'b0 && Adr_qb15[3:0] == c_CommandRegAddr )
                begin
                    // set address
                    EepromAddress_qb32 <= {7'h0,SectorReg_qb9,Adr_qb15[13:0],2'h0};

                    // do not execute if EEPROM busy or JTAG in progress, raise error flag
                    if( EepromBusy == 1'b1 || LoaderAccessGranted_q == 1'b1 )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        if( Dat_qb32[3:0] == c_CommandBulkErase )
                            EepromState <= s_EepromBulkErase;
                        else if( Dat_qb32[3:0] == c_CommandSectorErase )
                            EepromState <= s_EepromSectorErase;
                        else if( Dat_qb32[3:0] == c_CommandFactoryProtect || Dat_qb32[3:0] == c_CommandAllProtect ||  Dat_qb32[3:0] == c_CommandUnprotect )
                            EepromState <= s_EepromProtect;
                        else if( Dat_qb32[3:0] == c_CommandEn4BAddr )
                            EepromState <= s_EepromEn4BAddr;
                        else if( Dat_qb32[3:0] == c_CommandEx4BAddr )
                            EepromState <= s_EepromEx4BAddr;
                        else if( Dat_qb32[3:0] == c_CommandReconfigSaveSettings )
                            EepromState <= s_EepromReconfigSaveSettings;
                        else if( Dat_qb32[3:0] == c_CommandReconfigLoadSettings )
                            EepromState <= s_EepromReconfigLoadSettings;
                        else if( Dat_qb32[3:0] == c_CommandReconfigReconfig )
                            EepromState <= s_ReconfigReconfig;
                        else if( Dat_qb32[3:0] == c_CommandReconfigCancel )
                            ReconfigTimerAllow_q <= 1'b0;
                    end
                end
                // read from status register
                else if( We_q == 1'b0 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_StatusRegAddr )
                begin
                    DatOut_qb32 <= {StatusReg_b8,StatusReg_qb24};
                    EepromState <= s_EepromAck;
                end
                // write to status register
                else if( We_q == 1'b1 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_StatusRegAddr )
                begin
                    StatusClear_qb16 <= Dat_qb32[15:0];
                    EepromState <= s_EepromAck;
                end
                // EEPROM read
                else if( We_q == 1'b0 && Adr_qb15[14] == 1'b1 )
                begin
                    // set address
                    EepromAddress_qb32 <= {7'h0,SectorReg_qb9,Adr_qb15[13:0],2'h0};

                // do not execute if EEPROM busy or JTAG in progress, raise error flag
                    if( EepromBusy == 1'b1 || LoaderAccessGranted_q == 1'b1 )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        EepromState <= s_EepromRead;
                    end
                end
                // EEPROM write
                else if( We_q == 1'b1 && Adr_qb15[14] == 1'b1 )
                begin
                    // set address and data
                    EepromAddress_qb32 <= {7'h0,SectorReg_qb9,Adr_qb15[13:0],2'h0};
                    EepromData_qb32 <= Dat_qb32;

                    // do not execute if EEPROM busy or JTAG in progress, raise error flag
                    if( EepromBusy == 1'b1 || LoaderAccessGranted_q == 1'b1 )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        EepromState <= s_EepromWrite;
                    end
                end
                // Reconfig trigger source read
                else if( We_q == 1'b0 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_ReconfigTriggerRegAddr )
                begin
                    ReconfigParam_qb3 <= 3'h0;

                    // do not execute if reconfig busy, raise error flag
                    if( ReconfigBusy == 1'b1  )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        EepromState <= s_ReconfigRead;
                    end
                end
                // Reconfig watchdog timeout read
                else if( We_q == 1'b0 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_ReconfigTimeoutRegAddr )
                begin
                    ReconfigParam_qb3 <= 3'h2;

                    // do not execute if reconfig busy, raise error flag
                    if( ReconfigBusy == 1'b1  )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        EepromState <= s_ReconfigRead;
                    end
                end
                // Reconfig watchdog timeout write
                else if( We_q == 1'b1 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_ReconfigTimeoutRegAddr )
                begin
                    ReconfigParam_qb3 <= 3'h2;
                    EepromData_qb32 <= Dat_qb32;

                    // do not execute if reconfig busy, raise error flag
                    if( ReconfigBusy == 1'b1  )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        EepromState <= s_ReconfigWrite;
                        ReconfigWdTimeout_qb32 <= Dat_qb32;
                    end
                end
                // Reconfig watchdog enable read
                else if( We_q == 1'b0 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_ReconfigWatchdogRegAddr )
                begin
                    ReconfigParam_qb3 <= 3'h3;

                    // do not execute if reconfig busy, raise error flag
                    if( ReconfigBusy == 1'b1  )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        EepromState <= s_ReconfigRead;
                    end
                end
                // Reconfig watchdog enable write
                else if( We_q == 1'b1 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_ReconfigWatchdogRegAddr )
                begin
                    ReconfigParam_qb3 <= 3'h3;
                    EepromData_qb32 <= Dat_qb32;

                    // do not execute if reconfig busy, raise error flag
                    if( ReconfigBusy == 1'b1  )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        EepromState <= s_ReconfigWrite;
                        ReconfigParam_qb3 <= 3'h3;
                        ReconfigWdEnable_qb32 <= Dat_qb32;
                    end
                end
                // Reconfig address read
                else if( We_q == 1'b0 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_ReconfigAddressRegAddr )
                begin
                    ReconfigParam_qb3 <= 3'h4;

                    // do not execute if reconfig busy, raise error flag
                    if( ReconfigBusy == 1'b1  )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        EepromState <= s_ReconfigRead;
                    end
                end
                // Reconfig address write
                else if( We_q == 1'b1 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_ReconfigAddressRegAddr )
                begin
                    ReconfigParam_qb3 <= 3'h4;
                    EepromData_qb32 <= Dat_qb32;

                    // do not execute if reconfig busy, raise error flag
                    if( ReconfigBusy == 1'b1  )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        EepromState <= s_ReconfigWrite;
                        ReconfigPageSelect_qb32 <= Dat_qb32;
                    end
                end
                // Reconfig AfN read
                else if( We_q == 1'b0 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_ReconfigAnFRegAddr )
                begin
                    ReconfigParam_qb3 <= 3'h5;

                    // do not execute if reconfig busy, raise error flag
                    if( ReconfigBusy == 1'b1  )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        EepromState <= s_ReconfigRead;
                    end
                end
                // Reconfig AnF write
                else if( We_q == 1'b1 && Adr_qb15[14] == 1'b0 && Adr_qb15[2:0] == c_ReconfigAnFRegAddr )
                begin
                    ReconfigParam_qb3 <= 3'h5;
                    EepromData_qb32 <= Dat_qb32;

                    // do not execute if reconfig busy, raise error flag
                    if( ReconfigBusy == 1'b1  )
                    begin
                        OperationNotExecuted_q <= 1'b1;
                        EepromState <= s_EepromAck;
                    end
                    else begin
                        EepromState <= s_ReconfigWrite;
                        ReconfigPageSelect_qb32 <= Dat_qb32;
                    end
                end
                // unknown write, just ack
                else begin
                    EepromState <= s_EepromAck;
                    DatOut_qb32 <= 32'hdeadbeef;
                end
            end // wishbone cycle
            else if( ReconfigTimer_q == 1'b1 && ReconfigTimerReceived_q == 1'b0 )
                EepromState <= s_ReconfigReconfig;

        end // idle

        // read from the Eeprom, 4 bytes
        s_EepromRead: begin
            // strobe the fast read, keep it up for 1 cycle only
            EepromFastRead_q <= 1'b1 & ! FlagMask_q;
            FlagMask_q <= 1'b1;

            // hold read enable up for 3 data valid flags
            if( EepromCounter_qb3 < 3'h3 )
                EepromRdEn_q <= 1'b1;
            else
                EepromRdEn_q <= 1'b0;

            // shift the data in
            if( EepromDataValid == 1'b1 )
            begin
                // swap bits in byte
                DatOut_qb32 <= {EepromDataOut_b8[0],EepromDataOut_b8[1],EepromDataOut_b8[2],EepromDataOut_b8[3],
                                     EepromDataOut_b8[4],EepromDataOut_b8[5],EepromDataOut_b8[6],EepromDataOut_b8[7],DatOut_qb32[31:8]};
                EepromCounter_qb3 <= EepromCounter_qb3 + 3'h1;
            end

            // exit when busy released
            if( EepromBusy == 1'b0 && EepromCounter_qb3 == 3'h4 )
            begin
                EepromCounter_qb3 <= 3'h0;
                FlagMask_q <= 1'b0;

                if( BootRegisterAccess_q == 1'b1 )
                    EepromState <= s_EepromReconfigLoadSettings;
                else
                    EepromState <= s_EepromAck;
            end
        end

        // write to the Eeprom, 4 bytes
        s_EepromWrite: begin
            // operate the flags and shift in the data
            if( EepromCounter_qb3 == 3'h0 )
            begin
                // raise wren, shift_bytes and provide the first byte
                EepromShiftBytes_q <= 1'b1;
                // swap bits in byte
                EepromDataIn_qb8 <= {EepromData_qb32[0],EepromData_qb32[1],EepromData_qb32[2],EepromData_qb32[3],
                                            EepromData_qb32[4],EepromData_qb32[5],EepromData_qb32[6],EepromData_qb32[7]};
                EepromWrite_q <= 1'b0;
                EepromWrEn_q <= 1'b1;
            end
            else if( EepromCounter_qb3 == 3'h1 )
            begin
                // the second byte
                EepromShiftBytes_q <= 1'b1;
                // swap bits in byte
                EepromDataIn_qb8 <= {EepromData_qb32[8],EepromData_qb32[9],EepromData_qb32[10],EepromData_qb32[11],
                                            EepromData_qb32[12],EepromData_qb32[13],EepromData_qb32[14],EepromData_qb32[15]};
                EepromWrite_q <= 1'b0;
                EepromWrEn_q <= 1'b1;
            end
            else if( EepromCounter_qb3 == 3'h2 )
            begin
                // the third byte
                EepromShiftBytes_q <= 1'b1;
                // swap bits in byte
                EepromDataIn_qb8 <= {EepromData_qb32[16],EepromData_qb32[17],EepromData_qb32[18],EepromData_qb32[19],
                                            EepromData_qb32[20],EepromData_qb32[21],EepromData_qb32[22],EepromData_qb32[23]};
                EepromWrite_q <= 1'b0;
                EepromWrEn_q <= 1'b1;
            end
            else if( EepromCounter_qb3 == 3'h3 )
            begin
                // the fourth byte
                EepromShiftBytes_q <= 1'b1;
                // swap bits in byte
                EepromDataIn_qb8 <= {EepromData_qb32[24],EepromData_qb32[25],EepromData_qb32[26],EepromData_qb32[27],
                                            EepromData_qb32[28],EepromData_qb32[29],EepromData_qb32[30],EepromData_qb32[31]};
                EepromWrite_q <= 1'b0;
                EepromWrEn_q <= 1'b1;
            end
            else if( EepromCounter_qb3 == 3'h4 )
            begin
                // remove the shift_bytes signal
                EepromShiftBytes_q <= 1'b0;
                EepromWrite_q <= 1'b0;
                EepromWrEn_q <= 1'b1;
            end
            else if( EepromCounter_qb3 == 3'h5 )
            begin
                // raise the write signal
                EepromShiftBytes_q <= 1'b0;
                EepromWrite_q <= 1'b1;
                EepromWrEn_q <= 1'b1;
            end
            else begin
                // remove wren and write
                EepromShiftBytes_q <= 1'b0;
                EepromWrite_q <= 1'b0;
                EepromWrEn_q <= 1'b0;
            end

            // increment the counter 7 times
            if( EepromCounter_qb3 < 3'h7 )
                EepromCounter_qb3 <= EepromCounter_qb3 + 3'h1;
            // exit when busy released
            else if( EepromCounter_qb3 == 3'h7 && EepromBusy == 1'b0 )
            begin
                EepromCounter_qb3 <= 3'h0;

                if( BootRegisterAccess_q == 1'b1 )
                    EepromState <= s_EepromReconfigSaveSettings;
                else
                    EepromState <= s_EepromAck;
            end
        end

        // erase the whole Eeprom
        s_EepromBulkErase: begin
            // strobe the erase, keep it up for 1 cycle only
            EepromBulkErase_q <= 1'b1 & ! FlagMask_q;
            EepromWrEn_q <= 1'b1 & ! FlagMask_q;
            FlagMask_q <= 1'b1;

            // do not wait for the operation to complete before ack, since it takes too long and would block the bus
            if( FlagMask_q == 1'b1 )
            begin
                FlagMask_q <= 1'b0;
                EepromState <= s_EepromAck;
            end
        end

        // erase sector
        s_EepromSectorErase: begin
            // strobe the erase, keep it up for 1 cycle only
            EepromSectorErase_q <= 1'b1 & ! FlagMask_q;
            EepromWrEn_q <= 1'b1 & ! FlagMask_q;
            FlagMask_q <= 1'b1;

            // do not wait for the operation to complete before ack, since it takes too long and would block the bus
            if( FlagMask_q == 1'b1 )
            begin
                FlagMask_q <= 1'b0;

                if( BootRegisterAccess_q == 1'b1 )
                    EepromState <= s_EepromReconfigSaveSettings;
                else
                    EepromState <= s_EepromAck;
            end
        end


        // write proctect area in memory
        s_EepromProtect: begin
            // set the area
            if( Dat_qb32[3:0] == c_CommandFactoryProtect )
                EepromDataIn_qb8 <= FACTORY_PROTECT_AREA;
            else if( Dat_qb32[3:0] == c_CommandAllProtect  )
                EepromDataIn_qb8 <= ALL_PROTECT_AREA;
            else if( Dat_qb32[3:0] == c_CommandUnprotect )
                EepromDataIn_qb8 <= UNPROTECT_AREA;

            // strobe the erase, keep it up for 1 cycle only
            EepromSectorProtect_q <= 1'b1 & ! FlagMask_q;
            EepromWrEn_q <= 1'b1 & ! FlagMask_q;
            FlagMask_q <= 1'b1;

            // wait for busy removal
            if( EepromCounter_qb3 < 3'h2 )
                EepromCounter_qb3 <= EepromCounter_qb3 + 3'h1;
            else if( EepromBusy == 1'b0 )
            begin
                FlagMask_q <= 1'b0;
                EepromCounter_qb3 <= 3'h0;
                EepromState <= s_EepromAck;
            end
        end

        // enable 4 byte addressing
        s_EepromEn4BAddr: begin
            EepromWrEn_q <= 1'b1 & ! FlagMask_q;
            EepromEn4BAddr_q <= 1'b1 & ! FlagMask_q;
            FlagMask_q <= 1'b1;

            // wait for busy removal
            if( EepromCounter_qb3 < 3'h2 )
                EepromCounter_qb3 <= EepromCounter_qb3 + 3'h1;
            else if( EepromBusy == 1'b0 )
            begin
                FlagMask_q <= 1'b0;
                EepromCounter_qb3 <= 3'h0;
                EepromState <= s_EepromAck;
            end
        end

        // exit 4 byte addressing
        s_EepromEx4BAddr: begin
            EepromWrEn_q <= 1'b1 & ! FlagMask_q;
            EepromEx4BAddr_q <= 1'b1 & ! FlagMask_q;
            FlagMask_q <= 1'b1;

            // wait for busy removal
            if( EepromCounter_qb3 < 3'h2 )
                EepromCounter_qb3 <= EepromCounter_qb3 + 3'h1;
            else if( EepromBusy == 1'b0 )
            begin
                FlagMask_q <= 1'b0;
                EepromCounter_qb3 <= 3'h0;
                EepromState <= s_EepromAck;
            end
        end

        // ACK
        s_EepromAck: begin
            // stay in ACK until strobe released
            if( Stb_q == 1'b1 )
            begin
                Ack_q <= 1'b1;
            end
            else begin
                Ack_q <= 1'b0;
                EepromState <= s_EepromIdle;
            end
        end

        // Reconfig read
        s_ReconfigRead: begin
            ReconfigReadParam_q <= 1'b1 & !FlagMask_q;
            FlagMask_q <= 1'b1;

            // wait for busy removal
            if( EepromCounter_qb3 < 3'h2 )
                EepromCounter_qb3 <= EepromCounter_qb3 + 3'h1;
            else if( ReconfigBusy == 1'b0 )
            begin
                EepromCounter_qb3 <= 3'h0;
                FlagMask_q <= 1'b0;

                if( ReconfigAccess_q == 1'b1 )
                    EepromState <= s_ReconfigReconfig;
                else
                    EepromState <= s_EepromAck;

                // save status to register
                if( ReconfigParam_qb3 == 3'h0 )
                    ReconfigStatusReg_qb5 <= ReconfigDataOut_b32[4:0];

                DatOut_qb32 <= ReconfigDataOut_b32;
            end
        end

        // Reconfig write
        s_ReconfigWrite: begin
            ReconfigDataIn_qb32 <= EepromData_qb32;
            ReconfigWriteParam_q <= 1'b1 & !FlagMask_q;
            FlagMask_q <= 1'b1;

            // wait for busy removal
            if( EepromCounter_qb3 < 3'h2 )
                EepromCounter_qb3 <= EepromCounter_qb3 + 3'h1;
            else if( ReconfigBusy == 1'b0 )
            begin
                EepromCounter_qb3 <= 3'h0;
                FlagMask_q <= 1'b0;

                if( BootRegisterAccess_q == 1'b1 )
                    EepromState <= s_EepromReconfigLoadSettings;
                else if( ReconfigAccess_q == 1'b1 )
                    EepromState <= s_ReconfigReconfig;
                else
                    EepromState <= s_EepromAck;
            end
        end

        // save default reconfig settings to the EEPROM
        s_EepromReconfigSaveSettings: begin
            BootRegisterAccess_q <= 1'b1;

            // erase the sector
            if( SettingsProgress_qb3 == 3'h0 )
            begin
                EepromState <= s_EepromSectorErase;
                EepromAddress_qb32 <= {7'h0, BOOT_SETUP_ADDRESS_SECTOR, BOOT_SETUP_ADDRESS_BYTE };
                SettingsProgress_qb3 <= SettingsProgress_qb3 + 3'h1;

                // ack, since erase will take long
                Ack_q <= 1'b1;
            end
            // write setup header
            else if( SettingsProgress_qb3 == 3'h1 )
            begin
                Ack_q <= 1'b0;

                // wait for the erase
                if( EepromBusy == 1'b0 )
                begin
                    SettingsProgress_qb3 <= SettingsProgress_qb3 + 3'h1;
                    EepromState <= s_EepromWrite;
                    EepromData_qb32 <= c_SetupHeader;
                    EepromAddress_qb32 <= {7'h0, BOOT_SETUP_ADDRESS_SECTOR, BOOT_SETUP_ADDRESS_BYTE };
                end
            end
            // write wd timeout
            else if( SettingsProgress_qb3 == 3'h2 )
            begin
                SettingsProgress_qb3 <= SettingsProgress_qb3 + 3'h1;
                EepromState <= s_EepromWrite;
                EepromData_qb32 <= ReconfigWdTimeout_qb32;
                EepromAddress_qb32 <= {7'h0, BOOT_SETUP_ADDRESS_SECTOR, BOOT_SETUP_ADDRESS_BYTE + 16'h4 };
            end
            // write wd enable
            else if( SettingsProgress_qb3 == 3'h3 )
            begin
                SettingsProgress_qb3 <= SettingsProgress_qb3 + 3'h1;
                EepromState <= s_EepromWrite;
                EepromData_qb32 <= ReconfigWdEnable_qb32;
                EepromAddress_qb32 <= {7'h0, BOOT_SETUP_ADDRESS_SECTOR, BOOT_SETUP_ADDRESS_BYTE + 16'h8 };
            end
            // write page select
            else if( SettingsProgress_qb3 == 3'h4 )
            begin
                SettingsProgress_qb3 <= SettingsProgress_qb3 + 3'h1;
                EepromState <= s_EepromWrite;
                EepromData_qb32 <= ReconfigPageSelect_qb32;
                EepromAddress_qb32 <= {7'h0, BOOT_SETUP_ADDRESS_SECTOR, BOOT_SETUP_ADDRESS_BYTE + 16'hC };
            end
            else
                EepromState <= s_EepromIdle;

        end

        // load default reconfig settings from the EEPROM
        s_EepromReconfigLoadSettings: begin
            BootRegisterAccess_q <= 1'b1;

            SettingsProgress_qb3 <= SettingsProgress_qb3 + 3'h1;

            // skip reads if not allowed on boot
            if( SettingsProgress_qb3 == 3'h0 && BootFlag_q == 1'b1 && SETUP_LOAD_ONBOOT == 1'b0 )
                // move to default page select
                SettingsProgress_qb3 <= 3'h6;
            // read setup header
            else if( SettingsProgress_qb3 == 3'h0 )
            begin
                EepromState <= s_EepromRead;
                EepromAddress_qb32 <= {7'h0, BOOT_SETUP_ADDRESS_SECTOR, BOOT_SETUP_ADDRESS_BYTE };
            end
            // read wd timeout
            else if( SettingsProgress_qb3 == 3'h1 )
            begin
                // check setup header and move to set default page select otherwise
                if( DatOut_qb32 == c_SetupHeader )
                begin
                    EepromState <= s_EepromRead;
                    EepromAddress_qb32 <= {7'h0, BOOT_SETUP_ADDRESS_SECTOR, BOOT_SETUP_ADDRESS_BYTE + 16'h4 };
                end
                else begin
                    SettingsProgress_qb3 <= 3'h6;
                end
            end
            // read wd enable
            else if( SettingsProgress_qb3 == 3'h2 )
            begin
                ReconfigWdTimeout_qb32 <= DatOut_qb32;
                EepromState <= s_EepromRead;
                EepromAddress_qb32 <= {7'h0, BOOT_SETUP_ADDRESS_SECTOR, BOOT_SETUP_ADDRESS_BYTE + 16'h8 };
            end
            // read page select
            else if( SettingsProgress_qb3 == 3'h3 )
            begin
                ReconfigWdEnable_qb32 <= DatOut_qb32;
                EepromState <= s_EepromRead;
                EepromAddress_qb32 <= {7'h0, BOOT_SETUP_ADDRESS_SECTOR, BOOT_SETUP_ADDRESS_BYTE + 16'hC };
            end
            // set wd timeout
            else if( SettingsProgress_qb3 == 3'h4 )
            begin
                ReconfigPageSelect_qb32 <= DatOut_qb32;
                EepromState <= s_ReconfigWrite;
                ReconfigParam_qb3 <= 3'h2;
                EepromData_qb32 <= ReconfigWdTimeout_qb32;
            end
            // set wd enable
            else if( SettingsProgress_qb3 == 3'h5 )
            begin
                EepromState <= s_ReconfigWrite;
                ReconfigParam_qb3 <= 3'h3;
                EepromData_qb32 <= ReconfigWdEnable_qb32;
            end
            // set page select
            else if( SettingsProgress_qb3 == 3'h6 )
            begin
                EepromState <= s_ReconfigWrite;
                ReconfigParam_qb3 <= 3'h4;
                EepromData_qb32 <= ReconfigPageSelect_qb32;
            end
            else begin
                // read the status register from the reconfiguration IP on boot
                if( BootFlag_q == 1'b1 )
                begin
                    EepromState <= s_ReconfigRead;
                    ReconfigParam_qb3 <= 3'h0;
                end
                else
                    EepromState <= s_EepromAck;
            end

        end

        // reconfigure command
        s_ReconfigReconfig: begin
            ReconfigAccess_q <= 1'b1;
            SettingsProgress_qb3 <= SettingsProgress_qb3 + 3'h1;

            // exit if timer and application load is not allowed or address is 0x0000xxxx or 0xffffxxxx
            // also exit if timer and boot status indicates watchdog or faild CRC as boot source
            if( ReconfigTimer_q == 1'b1 && ( AUTO_APPLICATION_LOAD == 1'b0 || ReconfigPageSelect_qb32[31:16] == 16'h0 || ReconfigPageSelect_qb32[31:16] == 16'hffff
                                                        || ReconfigStatusReg_qb5[4] == 1'b1 || ReconfigStatusReg_qb5[0] == 1'b1 ))
            begin
                ReconfigTimerReceived_q <= 1'b1;
                EepromState <= s_EepromIdle;
            end
            else begin
                // read the AnF parameter
                if( SettingsProgress_qb3 == 3'h0 )
                begin
                    EepromState <= s_ReconfigRead;
                    ReconfigParam_qb3 <= 3'h5;
                end
                // write 1 to the AnF parameter to set for application image
                else if( SettingsProgress_qb3 == 3'h1 )
                begin
                    // exit if acting on timer and application image already loaded
                    if( ReconfigTimer_q == 1'b1 && ReconfigDataOut_b32 == 32'h1 )
                    begin
                        ReconfigTimerReceived_q <= 1'b1;
                        EepromState <= s_EepromIdle;
                    end
                    else begin
                        EepromState <= s_ReconfigWrite;
                        ReconfigParam_qb3 <= 3'h5;
                        EepromData_qb32 <= 32'h1;
                    end
                end
                // reconfigure
                else if( SettingsProgress_qb3 == 3'h2 )
                begin
                    ReconfigPoFCheck_q <= 1'b1;
                    ReconfigReconfig_q <= 1'b1;
                end
                // finish, wait for busy removal
                else if( SettingsProgress_qb3 == 3'h7 )
                begin
                    SettingsProgress_qb3 <= 3'h7;
                    ReconfigReconfig_q <= 1'b0; // remove reconfig after at least 250ns

                    if( ReconfigBusy == 1'b0 )
                    begin
                        ReconfigTimerReceived_q <= 1'b1;
                        if( ReconfigTimer_q == 1'b1 )
                            EepromState <= s_EepromIdle;
                        else
                            EepromState <= s_EepromAck;
                    end
                end
            end
        end


        endcase
    end
end



//// access reservation to the ASMI by JTAG
// state machine that processes the interaction with the EEPROM
always @(posedge ClkEeprom_ik or negedge RstEeprom_irn)
begin
    if( RstEeprom_irn == 1'b0 )
    begin
        LoaderAccessGranted_q <= 1'b0;
    end
    else begin

        // request incoming
        // grant if no operation on EEPROM
        // keep up until request removed
        if( LoaderAccessRequest == 1'b1 && EepromState == s_EepromIdle )
            LoaderAccessGranted_q <= 1'b1;
        // remove grant when no more reqested
        else if( LoaderAccessRequest == 1'b0 )
            LoaderAccessGranted_q <= 1'b0;
    end
end


//// simple watchdog reset timer
always @(posedge ClkEeprom_ik or negedge RstEeprom_irn)
begin
    if( RstEeprom_irn == 1'b0 )
        ReconfigResetTimer_q <= 1'b1;
    else
        ReconfigResetTimer_q <= ~ReconfigResetTimer_q;
end

//// EEPROM IP core (ASMI parallel)
asmi_parallel i_asmi_parallel (
    .clkin          (ClkEeprom_ik),          // clock in up to 20 MHz, generated from the VME clock
    .fast_read      (EepromFastRead),      // fast read strobe
    .rden           (EepromRdEn),           // read enable
    .addr           (EepromAddress_b32),           // eeprom write/read address 32bit
    .read_status    (1'b0),    // read status trobe
    .write          (EepromWrite_q),          // write strobe
    .datain         (EepromDataIn_qb8),         // data to write 8 bit
    .shift_bytes    (EepromShiftBytes_q),    // data shift in strobe
    .sector_protect (EepromSectorProtect_q), // sector protect strobe
    .sector_erase   (EepromSectorErase_q),   // sector erase strobe
    .bulk_erase     (EepromBulkErase_q),     // whole eeprom erase strobe
    .wren           (EepromWrEn_q),           // write enable
    .en4b_addr      (EepromEn4BAddr_q),         // 4 byte addressig enable
    .ex4b_addr      (EepromEx4BAddr_q),         // 4 byte addressig exit
    .reset          (!RstEeprom_irn),          // reset
    .dataout        (EepromDataOut_b8),        // data out 8 bit
    .busy           (EepromBusy),           // busy flag
    .data_valid     (EepromDataValid),     // data valid strobe
    .status_out     (StatusReg_b8),     // status register value
    .illegal_write  (EepromIllegalWrite),  // illegal write flag
    .illegal_erase  (EepromIllegalErase),   // illegal erase flag

    .asmi_dataout   ( AsmiDataOut_b4 ),   //   data from ASMI
    .asmi_dclk      ( AsmiDclk ),      //      data clock to ASMI
    .asmi_scein     ( AsmiSceIn ),     //     chip select to ASMI
    .asmi_sdoin     ( AsmiSdoIn_b4 ),     //    data to ASMI
    .asmi_dataoe    ( AsmiDataOe_b4 )     //    data enable to ASMI
);

// eeprom access multiplexor
assign EepromAddress_b32 = ( ReconfigPoFCheck_q == 1'b1 ) ? ReconfigEepromAddress_qb32 : EepromAddress_qb32;
assign EepromRdEn = ( ReconfigPoFCheck_q == 1'b1 ) ? ReconfigEepromRdEn_q : EepromRdEn_q;
assign EepromFastRead = ( ReconfigPoFCheck_q == 1'b1 ) ? ReconfigEepromFastRead_q : EepromFastRead_q;

//// serial flash loader for JTAG config
// the loader operates the ASMI pins and receives the bus from user for other than JTAG operation
// JTAG can requeste the accesss to pins, which must be granted
// grant must be kept low unless the request is high and granted
serial_flash_loader i_serial_flash_loader (
    .dclk_in             ( AsmiDclk ),             //            data clock to ASMI
    .ncso_in             ( AsmiSceIn ),             //             chip select to ASMI
    .data_in             ( AsmiSdoIn_b4 ),             //             data to ASMI
    .data_oe             ( AsmiDataOe_b4 ),             //            data enable to ASMI
    .noe_in              ( 1'b0 ),              //             core enable, ASMI tristate
    .asmi_access_granted ( LoaderAccessGranted_q ), // access to asmi granted to the core (JTAG)
    .data_out            ( AsmiDataOut_b4 ),            //            data from ASMI
    .asmi_access_request ( LoaderAccessRequest )  // access requested by the core (JTAG)
);

//// remote update IP core
remote_update i_remote_update (
    .read_param  ( ReconfigReadParam_q ),  //  read_param.read_param
    .param       ( ReconfigParam_qb3 ),       //       param.param 3
    .reconfig    ( ReconfigReconfig_q ),    //    reconfig.reconfig
    .reset_timer ( ReconfigResetTimer_q ), // reset_timer.reset_timer
    .clock       ( ClkEeprom_ik ),       //       clock.clk
    .reset       ( !RstEeprom_irn ),       //       reset.reset
    .busy        ( ReconfigBusy ),        //        busy.busy     o
    .data_out    ( ReconfigDataOut_b32 ),    //    data_out.data_out 32o
    .write_param ( ReconfigWriteParam_q ), // write_param.write_param
    .data_in     ( ReconfigDataIn_qb32 ),     //     data_in.data_in 32

    .pof_error       ( ReconfigPoFError ),       // programming file error
    .asmi_addr       ( ReconfigEepromAddress_qb32 ),       // asmi interface address
    .asmi_busy       ( EepromBusy ),       // asmi interface busy in
    .asmi_data_valid ( EepromDataValid ),       // asmi interface data valid in
    .asmi_dataout    ( EepromDataOut_b8 ),       // asmi interface data in
    .asmi_rden       ( ReconfigEepromRdEn_q ),       // asmi interface rden
    .asmi_read       ( ReconfigEepromFastRead_q )          // asmi interface read

);

end
endgenerate

endmodule
