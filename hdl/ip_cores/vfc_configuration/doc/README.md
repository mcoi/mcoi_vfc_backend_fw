# vfc_configuration

Module to program the VFC's onboard flash and reconfigure the card.

## Documentation

Introductory presentation:

https://gitlab.cern.ch/bi/BI_HDL_Cores/raw/master/cores_for_synthesis/vfc_configuration/documentation/vfcfactory.pdf

Full documentation:

https://gitlab.cern.ch/bi/BI_HDL_Cores/raw/master/cores_for_synthesis/vfc_configuration/documentation/vfc_factory_intro.pdf

## Instantiation Template


```verilog
vfc_configuration
    i_vfc_configuration(
        .ClkEeprom_ik(),
        .RstEeprom_irn(),

        // Wishbone interface
        .Clk_ik(),
        .Cyc_i(),
        .Stb_i(),
        .We_i(),
        .Adr_ib15(),
        .Dat_ib32(),
        .Dat_oab32(),
        .Ack_oa()
    );
```
