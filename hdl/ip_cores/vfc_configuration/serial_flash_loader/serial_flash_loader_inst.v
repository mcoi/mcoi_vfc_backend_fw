	serial_flash_loader u0 (
		.asmi_access_granted (<connected-to-asmi_access_granted>), // asmi_access_granted.asmi_access_granted
		.asmi_access_request (<connected-to-asmi_access_request>), // asmi_access_request.asmi_access_request
		.data_in             (<connected-to-data_in>),             //             data_in.data_in
		.data_oe             (<connected-to-data_oe>),             //             data_oe.data_oe
		.data_out            (<connected-to-data_out>),            //            data_out.data_out
		.dclk_in             (<connected-to-dclk_in>),             //             dclk_in.dclkin
		.ncso_in             (<connected-to-ncso_in>),             //             ncso_in.scein
		.noe_in              (<connected-to-noe_in>)               //              noe_in.noe
	);

