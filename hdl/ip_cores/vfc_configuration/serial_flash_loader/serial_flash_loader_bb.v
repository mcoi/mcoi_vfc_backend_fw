
module serial_flash_loader (
	asmi_access_granted,
	asmi_access_request,
	data_in,
	data_oe,
	data_out,
	dclk_in,
	ncso_in,
	noe_in);	

	input		asmi_access_granted;
	output		asmi_access_request;
	input	[3:0]	data_in;
	input	[3:0]	data_oe;
	output	[3:0]	data_out;
	input		dclk_in;
	input		ncso_in;
	input		noe_in;
endmodule
