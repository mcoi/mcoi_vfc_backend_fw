//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) May 2018 CERN

//-----------------------------------------------------------------------------
// @file TB_UVM.SV
// @brief simple testbench to test a feature of UVM called field
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 16 May 2018
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;
import uvm_style_test::*;


// simple testbench to test a feature of UVM called field
module tb_uvm;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};

   /*AUTOWIRE*/
   /*AUTOREGINPUT*/
   /*AUTOINOUTPARAM("uvm")*/

   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   logic [31:0]       localmap_31b128 [127:0];


   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 ClkRs_ix.reset = 1;
	 ##10;
	 ClkRs_ix.reset = 0;

      end

      `TEST_CASE("field") begin
	 localmap_31b128 = '{128{'0}};
	 `CHECK_EQUAL(localmap_31b128[AHOST], '0);
	 localmap_31b128[AHOST] = 32'haabbccdd;
	 `CHECK_EQUAL(localmap_31b128[AHOST], 32'haabbccdd);
	 `CHECK_EQUAL(AHOST_MSB(localmap_31b128[AHOST]), 16'haabb);
	 `CHECK_EQUAL(AHOST_LSB(localmap_31b128[AHOST]), 16'hccdd);
	 `CHECK_EQUAL(LSB(localmap_31b128[AHOST]), 1);
	 `CHECK_EQUAL(MSB4(localmap_31b128[AHOST]), 4'ha);

      end

   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10ms);


endmodule
