//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) April 2018 CERN

//-----------------------------------------------------------------------------
// @file TB_GLITCH_CATCH.SV
// @brief
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 12 April 2018
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

//
module tb_glitch_catch;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};
   localparam		g_CounterBits = 8;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic		q_o;			// From DUT of glitch_catch.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic		blankToLogicLow_i;	// To DUT of glitch_catch.v
   logic		q_i;			// To DUT of glitch_catch.v
   logic [g_CounterBits-1:0] width_ib;		// To DUT of glitch_catch.v
   // End of automatics

   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 ClkRs_ix.reset = 1;

	 blankToLogicLow_i = 1;
	 width_ib = (g_CounterBits)'(10);
	 q_i = 0;
	 ##10;
	 ClkRs_ix.reset = 0;
	 ##1;

      end

      `TEST_CASE("debug_test") begin
	 // width of 0 should disable the glitcher and signal should
	 // just bypass
	 blankToLogicLow_i = 0;
	 assert(std::randomize(width_ib));
	 $display("Random pulse of length %d testing", width_ib);

	 ##20;
	 // now trigger single failure:
	 ##1 q_i = '1;
	 ##1 q_i = '0;
	 @(posedge q_o);
	 repeat(width_ib) begin
	    ##1;
	    `CHECK_EQUAL(q_o, 1);
	 end
	 // plus one more:
	 ##1;
	 `CHECK_EQUAL(q_o, 1);
	 // anod nothing else:
	 ##1;
	 `CHECK_EQUAL(q_o, 0);
      end

      `TEST_CASE("blank_to_high_zero_width") begin
	 // width of 0 should disable the glitcher and signal should
	 // just bypass
	 blankToLogicLow_i = 0;
	 width_ib = 0;
	 ##20;
	 // now trigger single failure:
	 ##1 q_i = '1;
	 ##1 q_i = '0;
	 @(posedge q_o);
	 // AT LEAST 10 pulses
	 ##1;
	 `CHECK_EQUAL(q_o, 1);
	 ##1;
	 `CHECK_EQUAL(q_o, 0);
      end

      `TEST_CASE("zero_width_still_works") begin
	 // width of 0 should disable the glitcher and signal should
	 // just bypass
	 q_i = '1;
	 width_ib = 0;
	 ##20;
	 `CHECK_EQUAL(q_o, 1);
	 // now trigger single failure:
	 ##1 q_i = '0;
	 ##1 q_i = '1;
	 @(negedge q_o);
	 // AT LEAST 10 pulses
	 ##1;
	 `CHECK_EQUAL(q_o, 0);
	 ##1;
	 `CHECK_EQUAL(q_o, 1);
      end

      `TEST_CASE("width_one") begin
	 // width of 1 should generate 2cc long pattern (always one plus)
	 q_i = '1;
	 width_ib = 1;
	 ##20;
	 `CHECK_EQUAL(q_o, 1);
	 // now trigger single failure:
	 ##1 q_i = '0;
	 ##1 q_i = '1;
	 @(negedge q_o);

	 // AT LEAST 10 pulses
	 ##1;
	 `CHECK_EQUAL(q_o, 0);
	 ##1;
	 `CHECK_EQUAL(q_o, 0);
	 ##1;
	 `CHECK_EQUAL(q_o, 1);
      end // # #20;

      `TEST_CASE("multiple_spurious") begin
	 q_i = '1;
	 ##20;
	 `CHECK_EQUAL(q_o, 1);
	 // now trigger single failure:
	 ##1 q_i = '0;
	 ##1 q_i = '1;
	 // should generate zero-blanking
	 @(negedge q_o);
	 // generate few ccs and another glitch, it has to extend the
	 // blanking by the same amount as original one
	 ##4;
	 ##1 q_i = '0;
	 ##1 q_i = '1;

	 // and yet one more
	 ##2;
	 ##1 q_i = '0;
	 ##1 q_i = '1;

	 // AT LEAST 10 pulses
	 repeat(10) begin
	    ##1;
	    `CHECK_EQUAL(q_o, 0);
	 end
	 // and give 3 more ccs to turn off:
	 ##3;
	 `CHECK_EQUAL(q_o, 1);
      end

      `TEST_CASE("single_spurious") begin
	 q_i = '1;
	 ##20;
	 `CHECK_EQUAL(q_o, 1);
	 // now trigger single failure:
	 ##1 q_i = '0;
	 ##1 q_i = '1;
	 // should generate zero-blanking
	 @(negedge q_o);
	 // AT LEAST 10 pulses
	 repeat(10) begin
	    ##1;
	    `CHECK_EQUAL(q_o, 0);
	 end
	 // and give 3 more ccs to turn off:
	 ##3;
	 `CHECK_EQUAL(q_o, 1);
      end

      `TEST_CASE("static_output") begin
	 q_i = '1;
	 `CHECK_EQUAL(q_o, 0);
	 ##20;
	 // it should be already definitely at 1
	 `CHECK_EQUAL(q_o, 1);
      end

      `TEST_CASE("por_state") begin
	 ##100;
	 `CHECK_EQUAL(q_o, 0, "bad por state");
      end
   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10ms);

   glitch_catch #(/*AUTOINSTPARAM*/
		  // Parameters
		  .g_CounterBits	(g_CounterBits)) DUT
     (.*);

endmodule
