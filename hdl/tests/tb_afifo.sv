//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_AFIFO.SV
// @brief Tests fifo implemented with memory cells
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 30 October 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

// Tests fifo implemented with memory cells
module tb_afifo;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   localparam g_DataWidth = 8;
   localparam g_FifoWidth = 4;

   ckrs_t clkfast_ix = '{clk:'0, reset:'0};
   ckrs_t clkslow_ix = '{clk:'0, reset:'0};
   ckrs_t clkr_ix = '{clk:'0, reset:'0};
   ckrs_t clkw_ix = '{clk:'0, reset:'0};

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic		rFifoEmpty_o;		// From DUT of afifo.v
   logic		wFifoFull_o;		// From DUT of afifo.v
   // End of automatics
   /*AUTOREGINPUT*/

   // number of write-read cycles to verify fifo integrity
   localparam NUMTESTS = 512;

   int 			   test;
   // this is worst-case scenario
   logic unsigned [g_DataWidth-1:0] storage[$];

   // generate fast and slow clock
   always forever #(clk_period/2 * 1ns) clkslow_ix.clk <= ~clkslow_ix.clk;
   always forever #(16.4 * 1ns) clkfast_ix.clk <= ~clkfast_ix.clk;

   // define clock selector
   logic 			    clockselector = '0;
   // multiplex clocks to be able to select slow and fast clocks for each gate
   always_comb begin
      if (!clockselector) begin
	 clkw_ix = clkslow_ix;
	 clkr_ix = clkfast_ix;
      end
      else begin
	 clkr_ix = clkslow_ix;
	 clkw_ix = clkfast_ix;
      end
   end

   // write clock is default
   default clocking cb @(posedge clkw_ix.clk); endclocking
   // generate interfaces for data
   data_x #(.g_DataWidth(g_DataWidth)) wdata_ix(.ClkRs_ix(clkw_ix));
   data_x #(.g_DataWidth(g_DataWidth)) rdata_ox(.ClkRs_ix(clkr_ix));


   // write task - writes all on negative edge
   task write;
      input [g_DataWidth-1:0] Data_ib8;
      begin
	 @(negedge wdata_ix.ClkRs_ix.clk);
	 wdata_ix.data = Data_ib8;
	 wdata_ix.enable = 1;
      end
   endtask // WbWrite


   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 wdata_ix.data = '0;
	 wdata_ix.enable = 0;
	 rdata_ox.enable = 0;
	 // this is reset-less design, wait for some cycles before beginning
	 // such, that all the things stabilize
	 ##20;

      end

      `TEST_CASE("debug_test") begin
         `CHECK_EQUAL(rFifoEmpty_o, 1, "FIFO NOT EMPTY");

	 wdata_ix.data = 10;
	 wdata_ix.enable = 1;
	 ##2;
	 wdata_ix.enable = 0;
	 // immediatelly after we detect fifo not empty, we read
	 wait ($fell(rFifoEmpty_o));
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 rdata_ox.enable = 1;
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 rdata_ox.enable = 0;
	 // and check that immediately next clock cycle the fifoempty goes
	 // back. At this this stage the read request should be already
	 // identified
	 @(negedge rdata_ox.ClkRs_ix.clk);
	 `CHECK_EQUAL(rFifoEmpty_o, 1,
		      "FIFO EMPTY DOES NOT CLEAR OUT IMMEDIATELLY");
      end

      `TEST_CASE("fifoempty_immediate_clearout_consecutive") begin
         `CHECK_EQUAL(rFifoEmpty_o, 1, "FIFO NOT EMPTY");

	 wdata_ix.data = 10;
	 wdata_ix.enable = 1;
	 ##2;
	 wdata_ix.enable = 0;
	 // immediatelly after we detect fifo not empty, we read
	 wait ($fell(rFifoEmpty_o));
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 rdata_ox.enable = 1;
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 rdata_ox.enable = 0;
	 // and check that immediately next clock cycle the fifoempty goes
	 // back. At this this stage the read request should be already
	 // identified
	 @(negedge rdata_ox.ClkRs_ix.clk);
	 `CHECK_EQUAL(rFifoEmpty_o, 1,
		      "FIFO EMPTY DOES NOT CLEAR OUT IMMEDIATELLY");
      end

      `TEST_CASE("fifoempty_immediate_clearout") begin
         `CHECK_EQUAL(rFifoEmpty_o, 1, "FIFO NOT EMPTY");

	 wdata_ix.data = 10;
	 wdata_ix.enable = 1;
	 ##1;
	 wdata_ix.enable = 0;
	 // immediatelly after we detect fifo not empty, we read
	 wait ($fell(rFifoEmpty_o));
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 rdata_ox.enable = 1;
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 rdata_ox.enable = 0;
	 // and check that immediately next clock cycle the fifoempty goes
	 // back. At this this stage the read request should be already
	 // identified
	 @(negedge rdata_ox.ClkRs_ix.clk);
	 `CHECK_EQUAL(rFifoEmpty_o, 1,
		      "FIFO EMPTY DOES NOT CLEAR OUT IMMEDIATELLY");
      end

      `TEST_CASE("fifo_empty_signal_test") begin
         `CHECK_EQUAL(rFifoEmpty_o, 1, "FIFO NOT EMPTY");

	 wdata_ix.data = 10;
	 wdata_ix.enable = 1;
	 ##1;
	 wdata_ix.enable = 0;
	 // this takes some read cycles to propagate into read domain
	 @(posedge clkr_ix.clk);
	 @(posedge clkr_ix.clk);
	 @(posedge clkr_ix.clk);
	 @(posedge clkr_ix.clk);
	 @(posedge clkr_ix.clk);
         `CHECK_EQUAL(rFifoEmpty_o, 0, "FIFO EMPTY?");
         `CHECK_EQUAL(rdata_ox.data, 10, "FIFO direct output invalid");
      end

      `TEST_CASE("fifo_async_random_test") begin
	 // we use for loop to sta
	 for (int clk=0; clk < 2; clk++) begin : clockswitcher
	    // switch the clocks and wait for stabilization
	    clockselector = clk;
	    ##100;

	    fork
	       //Process-1
	       begin
		  repeat(NUMTESTS) begin
		     // write does all the job on negative edge to distinguish when
		     // the operation really happens (rising edge sensitive)
		     // have to do manually here
		     @(negedge clkw_ix.clk);
		     if (!wFifoFull_o)
		       begin
			  wdata_ix.enable = 1;
			  wdata_ix.data = $urandom_range(255, 0);
			  $display($time, "\tWriting ", wdata_ix.data);
			  // store written value into queue
			  storage.push_back(wdata_ix.data);
			  @(posedge wdata_ix.ClkRs_ix.clk);
			  wdata_ix.enable = 0;
		       end
		     else begin
			wdata_ix.enable = 0;
			// ignore data
			@(negedge wdata_ix.ClkRs_ix.clk);
			$display($time, "\tFIFO full");
		     end // else: !if(!wFifoFull_o)
		  end
	       end
	       //Process-2
	       begin
		  forever begin
		     @(posedge rdata_ox.ClkRs_ix.clk);
		     if (rFifoEmpty_o) begin
			$display($time,"\t\tFIFO empty - nothing to read");
			if ($size(storage) == 0) begin
			   $display("Queue empty, ending process");

			   break;
			end

		     end

		     else begin
			// NO NEED TO CLOCK IN WITH READ NOW, if fifo is not empty,
			// that means that we have already data on the port, hence
			// we can display them and _then_ fetch another data. This
			// is kind of prefetch fifo, which reduces latency by
			// exporting the first sample already at the output. Hence
			// rdata_ox.enable should be rather considered as 'fetchnext',
			// which increases counter.
			$display($time,"\t\tRead ", rdata_ox.data);
			// and check it against what is in queue
	    		`CHECK_EQUAL(rdata_ox.data, storage.pop_front(),
				     "DATA READINGFROM FIFO FAILS");

	    		// fetch next
	    		@(negedge rdata_ox.ClkRs_ix.clk);
	    		rdata_ox.enable = 1;
	    		@(negedge rdata_ox.ClkRs_ix.clk);
	    		rdata_ox.enable = 0;
		     end

		  end
	       end
	    join
	 end // block: clockswitcher

      end

      `TEST_CASE("fifo_full_handling") begin
         `CHECK_EQUAL(wFifoFull_o, 0, "FIFO FULL IS NONZERO");
	 ##1;
	 test = 0;
	 for (int i = 0; i < 2**g_FifoWidth+10; i++) begin
	    // 16th is the last same which fits, hence if we're on 17th, fifo
	    // should be full
	    if (i==17) test = 1;
            `CHECK_EQUAL(wFifoFull_o, test, "FIFO FULL MISBEHAVING");
	    write(i+10);
	 end
	 wdata_ix.enable = 0;

	 // let's read the data back through the interface, those exceeding the
	 // fifo should not be counted
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 rdata_ox.enable = 1;
	 test = 0;

	 for (int i = 0; i < 2**g_FifoWidth+10; i++) begin
	    @(posedge rdata_ox.ClkRs_ix.clk);
	    if (i < 16) begin
	       // for the first 16 readings check the data out, all the others
	       // are invalid as we read empty fifo
               `CHECK_EQUAL(rdata_ox.data, i + 10, "DATA NOT CORRECT");
	    end else
	      test = 1;

	    `CHECK_EQUAL(rFifoEmpty_o, test, "FIFO EMPTY SIGNAL MISBEHAVING");
	 end
	 rdata_ox.enable = 0;
      end

      `TEST_CASE("fifo_empty_flag_cycling") begin
         `CHECK_EQUAL(rFifoEmpty_o, 1, "FIFO NOT EMPTY");

	 wdata_ix.data = 10;
	 wdata_ix.enable = 1;
	 ##1;
	 wdata_ix.enable = 0;
	 // this takes some read cycles to propagate into read domain
	 @(posedge clkr_ix.clk);
	 @(posedge clkr_ix.clk);
	 @(posedge clkr_ix.clk);
	 @(posedge clkr_ix.clk);
	 @(posedge clkr_ix.clk);
         `CHECK_EQUAL(rFifoEmpty_o, 0, "FIFO EMPTY?");
         `CHECK_EQUAL(rdata_ox.data, 10, "FIFO direct output invalid");
      end
   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(100ms);

   afifo #(/*AUTOINSTPARAM*/
	   // Parameters
	   .g_FifoWidth			(g_FifoWidth)) DUT
     (.*);

endmodule
