//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) January 2018 CERN

//-----------------------------------------------------------------------------
// @file RNDTEST.SV
// @brief
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 30 January 2018
// @details Checks SV random number generator. Point here is to
// generate an array of maximum 4 numbers, where each number is
// between 0 to 3 and they are unique, distributing length vector
// probability such, that roughly 80% of generated vectors will be
// single length
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

module rndtest;

   int 			   howManyIfaces;
   int 			   ifaces[];
   localparam NUM_OF_INPUTS = 4;


   initial begin
      ifaces = new[NUM_OF_INPUTS];
      repeat(500) begin
	 assert(std::randomize(ifaces) with {
	    ifaces.size inside {[1:NUM_OF_INPUTS]};
	    ifaces.size dist {1 := 80, 2 := 20, 3 := 10, 4 != 5};
	    foreach (ifaces[i]) ifaces[i] inside {[0:NUM_OF_INPUTS-1]};
	    foreach (ifaces[i]) foreach (ifaces[j])
	       if (i != j) ifaces[i] != ifaces[j];

	 });
	 $display("Val: ", ifaces);

      end // repeat (500)
   end // initial begin


endmodule // rndtest
