//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) May 2018 CERN

//-----------------------------------------------------------------------------
// @file TB_GEFE_RELATION.SV
// @brief gefe and mcoi together
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 11 May 2018
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

// gefe and mcoi together
module tb_empty_mcoi_placeholder;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};
   localparam c_VmeAccessVerbose = 1'b1;
   localparam g_NSlots = 20;
   localparam g_VerboseDefault = 1;
   localparam g_Timeout = 132000;
   localparam c_VfcSlot4BaseAddress = 32'h0400_0000;  // Comment: The address is for now fixed

   wire [5:0] 	      AM_ib6;
   wire 	      As_in;
   wire [31:1]       A_iob31;		// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire 	      LWord_io;		// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire [1:0] 	      Ds_inb2;		// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire [20:1] 	  Gap_nbm;
   wire		Wr_in;			// To/From i_VmeBusModule of VmeBusModule.v
   wire [31:0]		D_iob32;		// To/From i_VmeBusModule of VmeBusModule.v, ...
   wire		DtAck_on;		// From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire		FmcClk2Bidir_iok;	// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire		FmcClk3Bidir_iok;	// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire [23:0]		FmcHaN_iob24;		// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire [23:0]		FmcHaP_iob24;		// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire [21:0]		FmcHbN_iob22;		// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire [21:0]		FmcHbP_iob22;		// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire [33:0]		FmcLaN_iob34;		// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire [33:0]		FmcLaP_iob34;		// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire		FmcScl_iok;		// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire		FmcSda_io;		// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire [4:1]		GpIoLemo_iob4;		// To/From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire		IackIn_in;		// To/From i_VmeBusModule of VmeBusModule.v
   wire		IackOut_on;		// From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire		Iack_in;		// To/From i_VmeBusModule of VmeBusModule.v
   wire [7:1]		Irq_onb7;		// From i_VfcHd_v3_0 of VfcHd_v3_0.v
   wire		SysClk_ik;		// To/From i_VmeBusModule of VmeBusModule.v
   wire		SysResetN_irn;		// To/From i_VmeBusModule of VmeBusModule.v

   logic [31:0]   LastVmeReadData_b32;
   logic [7:0] 	  LastVmeAccessExitCode_b8;

   localparam c_SystemInfo1 = 32'h0000_0004;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic [9:0]		FmcDpC2m_ob10;		// From i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcGa0_o;		// From i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcGa1_o;		// From i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcPgC2m_o;		// From i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcTck_ok;		// From i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcTdi_o;		// From i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcTms_o;		// From i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcTrstL_orn;		// From i_VfcHd_v3_0 of VfcHd_v3_0.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic [7:0]		DipSw_ib8;		// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcClk0M2cCmos_ik;	// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcClk1M2cCmos_ik;	// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcClkDir_i;		// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic [9:0]		FmcDpM2c_ib10;		// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcGbtClk0M2c_ik;	// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcGbtClk1M2c_ik;	// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcPgM2c_i;		// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcPrsntM2c_in;		// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		FmcTdo_i;		// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   logic		PushButton_i;		// To i_VfcHd_v3_0 of VfcHd_v3_0.v
   // End of automatics
   /*AUTOINOUTPARAM("VfcHd_v3_0")*/
   wire [4:0] 	      Ga_ionb5 [g_NSlots:1];		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v

   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   task Read (input [31:0] Address_b32);
      i_VmeBus.ReadA32D32(c_VfcSlot4BaseAddress + Address_b32, LastVmeReadData_b32, LastVmeAccessExitCode_b8, c_VmeAccessVerbose);
   endtask:Read

   task Write (input [31:0] Address_b32, input [31:0] Data_b32);
      i_VmeBus.WriteA32D32(c_VfcSlot4BaseAddress + Address_b32, Data_b32, LastVmeAccessExitCode_b8, c_VmeAccessVerbose);
   endtask:Write

   task Reset();
      i_VmeBus.VmeReset(500, c_VmeAccessVerbose);
   endtask:Reset

   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 Reset();
	 #100;
      end

      `TEST_CASE("debug_test") begin
	 Read(c_SystemInfo1);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'h5646432d, "systeminfo read failed");

         $display("This test case is expected to pass");
         `CHECK_EQUAL(1, 1);
      end

   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10ms);

   VmeBusModule
     #(/*AUTOINSTPARAM*/
       // Parameters
       .g_NSlots			(g_NSlots),
       .g_VerboseDefault		(g_VerboseDefault),
       .g_Timeout			(g_Timeout))
   i_VmeBus
     (
      // Inouts
      .As_n				(As_in),
      .AM_b6				(AM_ib6),
      .A_b31				(A_iob31),
      .LWord				(LWord_io),
      .Ds_nb2				(Ds_inb2),
      .Wr_n				(Wr_in),
      .D_b32				(D_iob32),
      .DtAck_n				(DtAck_on),
      .Irq_nb7				(Irq_onb7),
      .Iack_n				(Iack_in),
      .IackIn_nb			(IackIn_in),
      .IackOut_nb			(IackOut_on),
      .SysResetN_irn			(SysResetN_irn),
      .SysClk_k				(SysClk_ik),
      .Ga_nmb5				(Ga_ionb5),
      .Gap_nbm				(Gap_nbm)
      /*AUTOINST*/);


   VfcHd_v3_0
     #(/*AUTOINSTPARAM*/)
   i_VfcHd_v3_0
     (
      .Ga_ionb5				(Ga_ionb5[4]),
      .Gap_ion				(Gap_nbm[4]),

      /*AUTOINST*/
      // Outputs
      .DtAck_on				(DtAck_on),
      .Irq_onb7				(Irq_onb7[7:1]),
      .IackOut_on			(IackOut_on),
      .FmcTck_ok			(FmcTck_ok),
      .FmcTms_o				(FmcTms_o),
      .FmcTdi_o				(FmcTdi_o),
      .FmcTrstL_orn			(FmcTrstL_orn),
      .FmcPgC2m_o			(FmcPgC2m_o),
      .FmcDpC2m_ob10			(FmcDpC2m_ob10[9:0]),
      .FmcGa0_o				(FmcGa0_o),
      .FmcGa1_o				(FmcGa1_o),
      // Inouts
      .A_iob31				(A_iob31[31:1]),
      .LWord_io				(LWord_io),
      .D_iob32				(D_iob32[31:0]),
      .FmcLaP_iob34			(FmcLaP_iob34[33:0]),
      .FmcLaN_iob34			(FmcLaN_iob34[33:0]),
      .FmcHaP_iob24			(FmcHaP_iob24[23:0]),
      .FmcHaN_iob24			(FmcHaN_iob24[23:0]),
      .FmcHbP_iob22			(FmcHbP_iob22[21:0]),
      .FmcHbN_iob22			(FmcHbN_iob22[21:0]),
      .FmcScl_iok			(FmcScl_iok),
      .FmcSda_io			(FmcSda_io),
      .FmcClk2Bidir_iok			(FmcClk2Bidir_iok),
      .FmcClk3Bidir_iok			(FmcClk3Bidir_iok),
      .GpIoLemo_iob4			(GpIoLemo_iob4[4:1]),
      // Inputs
      .As_in				(As_in),
      .AM_ib6				(AM_ib6[5:0]),
      .Ds_inb2				(Ds_inb2[1:0]),
      .Wr_in				(Wr_in),
      .Iack_in				(Iack_in),
      .IackIn_in			(IackIn_in),
      .SysResetN_irn			(SysResetN_irn),
      .SysClk_ik			(SysClk_ik),
      .FmcPrsntM2c_in			(FmcPrsntM2c_in),
      .FmcTdo_i				(FmcTdo_i),
      .FmcPgM2c_i			(FmcPgM2c_i),
      .FmcClk0M2cCmos_ik		(FmcClk0M2cCmos_ik),
      .FmcClk1M2cCmos_ik		(FmcClk1M2cCmos_ik),
      .FmcClkDir_i			(FmcClkDir_i),
      .FmcDpM2c_ib10			(FmcDpM2c_ib10[9:0]),
      .FmcGbtClk0M2c_ik			(FmcGbtClk0M2c_ik),
      .FmcGbtClk1M2c_ik			(FmcGbtClk1M2c_ik),
      .DipSw_ib8			(DipSw_ib8[7:0]),
      .PushButton_i			(PushButton_i));

endmodule
