//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_STEPPINGCONTROLLER.SV
// @brief Verifies functionality of the stepping controller
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 17 October 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;
import MCPkg::*;
`include "../modules/t_MCIface.sv"

// Verifies functionality of the stepping controller
module tb_steppingcontroller;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic [1:0]		ConfiguredSwitches_b2;	// From DUT of steppingcontroller.v
   stepperstatus_t	StepperStatus_x;	// From DUT of steppingcontroller.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   cntparam_t		CntParam_ix;		// To DUT of steppingcontroller.v
   logic		DeviceInterlocked;	// To DUT of steppingcontroller.v
   command_t		ICmdParams_x;		// To DUT of steppingcontroller.v
   motorcommand_t	ITriggerParams_x;	// To DUT of steppingcontroller.v
   logic [31:0]		reload_ib32;		// To DUT of steppingcontroller.v
   triggers_t		triggers;		// To DUT of steppingcontroller.v
   // End of automatics
   int 			smstate = 0;
   int 			unitDistance = 0;
   int 			slowDistance = 0;
   real 		ratio, expected, diff;
   // instance of interface
   mc_x mc();
   int 			count = 0;
   int 			testval;

   // values to be tested for division
   int 			testedvals[$] = {0, 1, 2, 4, 5, 7, 13, 16};
   logic 		zeroone[$] = {'0, '1};




   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   // e? wtf! systemverilog does not have $abs?
   function real absolute(input real a);
      if (a >= 0.0)
	return a;
      return -a;
   endfunction // absolute

`define checkSwitches(switchvalue, interiorsignal, switchsignal) \
   fork\
      driveMotorToExtremity(switchvalue);\
      forever begin\
	 @(posedge mc.invec.RawSwitches_b2[switchvalue]);\
	   ##100;\
	     `CHECK_EQUAL(interiorsignal, 1);\
	       end\
   join_any;\
   `CHECK_EQUAL(switchsignal,\
		1);

`define start\
   ##1 ITriggerParams_x.StartMove = 1;\
   ##1 ITriggerParams_x.StartMove = 0;

`define stop\
   ##1 ITriggerParams_x.StopMove = 1;\
   ##1 ITriggerParams_x.StopMove = 0;

`define waitIRQ\
   fork\
      #10ms;\
      @(posedge StepperStatus_x.Interrupt);\
   join_any\
   `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);

   // following task runs motor and turns on particular extremity
   // switch. Then it counts how many pulses of stepout were generated
   // after the switch was launched, and compares that to the value,
   // which was setup in configuration
   task automatic driveMotorToExtremity(input logic dir);
      int cntr;

      testval = 0;

      $display("Checking correct Switch[%d] functionality", dir);
      mc.invec.RawSwitches_b2 = '0;
      ##1000;

      ICmdParams_x.powerconfig.enable_move = 1;
      ICmdParams_x.StepNumber_b32 = 5000;
      ICmdParams_x.GenInterrupt = 0;

      // DIRECTION=1, SW_OUT switch hits at certain point
      ICmdParams_x.Direction = dir;
      `start
      // enforce switch triggering, should trigger switch1
      $display("Enforcing switch %d", dir);
      ##1000;
      mc.invec.RawSwitches_b2[dir] = 1;
      $display("Counting pulses of stepout:");
      testval = 1;

      // and now we calculate how many steps were generated after
      // rising this signal. They should correspond to
      // stepsafterlimitswitch
      cntr = 0;
      fork
	 @(posedge StepperStatus_x.Done);
	 forever begin
	    @(negedge mc.outvec.StepOutP_o);
	    cntr++;
	    $display("Count: %d", cntr);
	    ##1;
	 end
      join_any;
      testval = 2;

      // there's difference between positive and negative direction
      // and number of steps to be done
      if (dir) begin
	// positive:
	`CHECK_EQUAL(CntParam_ix.StepsAfterLimitSwitchN_b6, cntr);
      end else begin
	// negative:
	`CHECK_EQUAL(CntParam_ix.StepsAfterLimitSwitchP_b6, cntr);
      end


      `CHECK_EQUAL(StepperStatus_x.AbortStatus.StopLimit, 1);
      `CHECK_EQUAL(StepperStatus_x.AbortStatus.Direction, dir);
      $display("Done checking Switch[%d]", dir);

   endtask

   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 // this TB does not check interlocking, as this functionality
	 // must be tested from wbmc
	 DeviceInterlocked = '0;
	 // setup all the signals by default to 'not do anything'
	 reload_ib32 <= 32'hf00ff00f;
	 triggers.TrigMoveInt_i = 0;
	 triggers.TrigMoveExt_i = 0;
	 triggers.TriggerEnable_i = 1;

	 mc.invec.OH_i = 0;
	 mc.invec.RawSwitches_b2[0] = 0;
	 mc.invec.RawSwitches_b2[1] = 0;
	 mc.invec.StepPFail_i = 0;
	 // no triggers generating
	 ITriggerParams_x = '0;
	 // configure switches - not that INPUT SWITCH SIGNAL does not
	 // have any notion of what is 'in switch', and what is 'out
	 // switch'. This is done by switch configuration
	 // first. Purpose of switch configuration is to setup the
	 // switches such, that they correspond to the implementation
	 // in motors. We setup the switches to 'normal' polarity and
	 // non-swapping here. DIRECTION=0, switch[0] is
	 // hit. DIRECTION=1, switch[1] is hit.
	 CntParam_ix.ExtremitySwitches_b2[0].Polarity = NORMAL;
	 CntParam_ix.ExtremitySwitches_b2[0].SelectedInputSwitches_b2 = 2'b01;
	 CntParam_ix.ExtremitySwitches_b2[1].Polarity = NORMAL;
	 CntParam_ix.ExtremitySwitches_b2[1].SelectedInputSwitches_b2 = 2'b10;

	 CntParam_ix.StepsAfterLimitSwitchP_b6 = 10;
	 CntParam_ix.StepsAfterLimitSwitchN_b6 = 15;
	 CntParam_ix.SlowDown_b32 = 0;

	 // construct default command, it disables the motor motion
	 // FOLLOWING PARAMETERS ASSURE, THAT ALL STATE MACHINE STATES
	 // ARE TRAVERSED
	 ICmdParams_x.StepNumber_b32 = 123;
	 ICmdParams_x.Direction = 1;
	 ICmdParams_x.LowSpeed_b36 = 50;
	 ICmdParams_x.HighSpeed_b36 = 2000;
	 ICmdParams_x.AccDeccRate_b19 = 2000;
	 ICmdParams_x.Trail_b32 = 10;
	 ICmdParams_x.globaltrigger = '0;
	 ICmdParams_x.GenInterrupt = 0;
	 ICmdParams_x.powerconfig = '0;
	 ClkRs_ix.reset = 1;
	 ##10;
	 ClkRs_ix.reset = 0;
	 ##1;

      end

      `TEST_CASE("debug_test") begin
	 ##1;

      end // UNMATCHED !!

      `TEST_CASE("zero_steps_after_negative_switch") begin
	 // check behaviour when zero steps must be fgenerated when
	 // hitting switch:
	 ICmdParams_x.StepNumber_b32 = 50;
	 // set both SALS to zero
	 CntParam_ix.StepsAfterLimitSwitchP_b6 = 0;
	 CntParam_ix.StepsAfterLimitSwitchN_b6 = 0;
	 // and drive motor to both extremities:
	 // direction = 1 reacts to StepsAfterLimitSwitchN_b6
	 // direction = 0 reacts to StepsAfterLimitSwitchP_b6
	 `checkSwitches(1, DUT.IsNExtremity,
			StepperStatus_x.AbortStatus.SW_N_triggered);
      end // UNMATCHED !!

      `TEST_CASE("zero_steps_after_positive_switch") begin
	 // check behaviour when zero steps must be fgenerated when
	 // hitting switch:
	 ICmdParams_x.StepNumber_b32 = 50;
	 // set both SALS to zero
	 CntParam_ix.StepsAfterLimitSwitchP_b6 = 0;
	 CntParam_ix.StepsAfterLimitSwitchN_b6 = 0;
	 // and drive motor to both extremities:
	 // direction = 1 reacts to StepsAfterLimitSwitchN_b6
	 // direction = 0 reacts to StepsAfterLimitSwitchP_b6
	 `checkSwitches(0, DUT.IsPExtremity,
			StepperStatus_x.AbortStatus.SW_P_triggered);
      end // UNMATCHED !!

      `TEST_CASE("different_positive_and_negative_sals") begin
	 // we test here that depending of direction pin the number of
	 // steps after hitting the switch is corresponding to desired
	 // value. There are two registers to handle this
	 ICmdParams_x.StepNumber_b32 = 50;
	 // setup different sals for directions
	 assert(std::randomize(CntParam_ix.StepsAfterLimitSwitchP_b6));
	 assert(std::randomize(CntParam_ix.StepsAfterLimitSwitchN_b6));

	 $display("Checking SALS for (%d, %d)",
		  CntParam_ix.StepsAfterLimitSwitchP_b6,
		  CntParam_ix.StepsAfterLimitSwitchN_b6);

	 ##100;

	 // drive to direction '1' (positive), hence SwitchP should apply:
	 `checkSwitches(1, DUT.IsNExtremity,
			StepperStatus_x.AbortStatus.SW_N_triggered);

	 ##100;

	 // drive to negative extremity, SwitchN should be considered:
	 `checkSwitches(0, DUT.IsPExtremity,
			StepperStatus_x.AbortStatus.SW_P_triggered);

	 // check are performed inside checkSwitches
      end // UNMATCHED !!



      `TEST_CASE("no_IRQ_at_the_beginning_of_move_after_movestop")
      begin
	 mc.invec.RawSwitches_b2 = '0;
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 50;
	 ICmdParams_x.GenInterrupt = 1;
	 ICmdParams_x.Direction = 1;
	 ##1000;

	 // this has to generate single IRQ event:
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus.StopMove, 0);
	 $display("Starting motor");
	 `start;
	 // wait for at least 1st stepout
	 repeat(10) begin
	    @(negedge mc.outvec.StepOutP_o);
	    // this should generate IRQ
	    $display("Stopping motor and waiting for IRQ due to\
 stop");
	    `stop;
	    `waitIRQ;
	    ##100;
	    `CHECK_EQUAL(StepperStatus_x.AbortStatus.StopMove, 1);
	    // this should generate IRQ, but only at the end of the
	    // cycle, not at the beginning:
	    $display("Restarting motor again");
	    `start;
	    $display("Waiting if by chance a spurious IRQ is\
 generated");

	    fork
	       @(negedge mc.outvec.StepOutP_o);
	       @(posedge StepperStatus_x.Interrupt);
	    join_any
	    // catch if by chance IRQ was risen between start of the
	    // motor and first pulse going out. If so, that's a fail!
	    `CHECK_EQUAL(StepperStatus_x.Interrupt, 0);
	    `CHECK_EQUAL(mc.outvec.StepOutP_o, 0);
	    // on-fly abortstatus:
	    `CHECK_EQUAL(StepperStatus_x.AbortStatus.StopMove, 0);
	    // and if not, wait for the IRQ at the end of cycle
	    $display("No spurious IRQ, waiting for the final one");
	    `waitIRQ;
	    $display("Got final IRQ, loop is OK");
	    ##500;
	    $display("Starting another cycle");
	    `start;
	 end
      end // UNMATCHED !!

      `TEST_CASE("movestop_and_switch_IRQ_behaviour")
      begin
	 // after motor start we issue stop command, and while
	 // performed we trugger one of the switches. then when
	 // movement is triggered again into switch IRQ should be
	 // generated. reverse direction and check whether IRQ is
	 // generated at the end of the cycle
	 mc.invec.RawSwitches_b2 = '0;
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 5000;
	 ICmdParams_x.GenInterrupt = 1;
	 ##1000;
	 // first we generate movement and we stop the motor:
	 ICmdParams_x.Direction = 1;

	 // this has to generate single IRQ event:
	 `start;
	 // wait for at least 1st stepout
	 @(negedge mc.outvec.StepOutP_o);
	 #10us;
	 `stop;
	 `waitIRQ;
	 // hit the limit switch and restart the movement, wait until
	 // switch propagates through debouncers:
	 mc.invec.RawSwitches_b2[1] = '1;
	 ##1000;
	 `start;
	 `waitIRQ;
	 ##100;
	 // reverse the direction and fire again:
	 ICmdParams_x.Direction = 0;
	 ICmdParams_x.StepNumber_b32 = 50;
	 `start;
	 @(negedge mc.outvec.StepOutP_o);
	 `waitIRQ;

      end // UNMATCHED !!

      `TEST_CASE("IRQ_changed_direction_after_movestop")
      begin
	 // turn motor to given direction, reach switch, during
	 // stepsafterlimitswitch hit move stop and change direction
	 // of motor and start it again. Totally 3 IRQs have to be
	 // generated....
	 mc.invec.RawSwitches_b2 = '0;
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 5000;
	 ICmdParams_x.GenInterrupt = 1;
	 ##1000;
	 // first we generate movement and we stop the motor:
	 ICmdParams_x.Direction = 1;

	 // this has to generate single IRQ event:
	 `start;
	 // wait for at least 1st stepout
	 @(negedge mc.outvec.StepOutP_o);
	 #10us;
	 `stop;
	 `waitIRQ;
	 `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);
	 // hit the limit switch and restart the movement, wait until
	 // switch propagates through debouncers:
	 mc.invec.RawSwitches_b2[1] = '1;
	 ##1000;
	 `start;
	 `waitIRQ;
	 ##100;
	 // reverse the direction and fire again:
	 ICmdParams_x.Direction = 0;
	 ICmdParams_x.StepNumber_b32 = 50;
	 `start;
	 @(negedge mc.outvec.StepOutP_o);
	 `waitIRQ;

      end // UNMATCHED !!

      `TEST_CASE("IRQ_into_switch_as_first_move") begin
	 // this test case is observed in hardware: when system is in
	 // complete initial state (no movement so far) and we trigger
	 // startmove into switch, the IRQ must be generated. Let's
	 // turn both switches on:
	 ICmdParams_x.GenInterrupt = 1;
	 mc.invec.RawSwitches_b2 = '1;
	 // wait for bouncing to get them on logical:
	 ##100;
	 `CHECK_EQUAL(DUT.ConfiguredSwitches_b2, 2'b11);
	 // switches in position, the other things are setup in
	 // test_suite_setup, we can start the motor
	 fork
	    begin
	       `start;
	       #100ms;
	    end
	    @(posedge StepperStatus_x.Interrupt);
	    #1ms;
	 join_any;
	 `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);
      end // UNMATCHED !!


      `TEST_CASE("IRQ_behaviour_after_stopmove_and_revese_dir")
      begin
	 // drive the motor, then we issue limit switch after first
	 // pulse and when in stepsafterlimitswitch, we make stopmove,
	 // this should generate immediate IRQ with stopmove. Then we
	 // go against switch, we should get IRQ, and then we turn the
	 // direciton and we should go the other way, receiving the
	 // IRQ at the end of movement.
	 mc.invec.RawSwitches_b2 = '0;
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 5000;
	 ICmdParams_x.GenInterrupt = 1;
	 ##1000;
	 // first we generate movement and we stop the motor:
	 ICmdParams_x.Direction = 1;

	 // this has to generate single IRQ event:
	 fork
	    begin
	       // start move
	       `start;
	       // wait for at least 1st stepout
	       @(negedge mc.outvec.StepOutP_o);
	       #10us;
	       // hit the limit switch
	       mc.invec.RawSwitches_b2[1] = '1;
	       @(negedge mc.outvec.StepOutP_o);
	       // and after second pulse issue stop move (this might
	       // happen)
	       // so we check here what happens if we're in
	       // stepsafterlimitswitch and issue stop motor
	       #10us;
	       `stop;
	       ##100;
	    end // fork begin
	    @(posedge StepperStatus_x.Interrupt);
	 join_any
	 `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus.StopMove, 1);
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus.SW_N_triggered, 1);
	 ##100;
	 ICmdParams_x.StepNumber_b32 = 50;

	 // let's check start again, we should intercept single IRQ
	 `start;
	 `waitIRQ;
	 ##100;
	 // now we change direction, hence motor should freely start
	 // and we expect 1IRQ, AT THE END OF MOVEMENT
	 ICmdParams_x.Direction = '0;
	 `start;
	 `waitIRQ;
	 ##100;
      end // UNMATCHED !!

      `TEST_CASE("stopmove_with_disabled_move_must_generate_IRQ") begin
	 // testing here what happens if we stop the move, but due to
	 // the motors inertial we jump into switch. This should in
	 // theory keep abortstatus unchanged, i.e. - stop due to
	 // stopmove command, but on next consecutive start into the
	 // direction of switches it should not do anything except
	 // generation of IRQ. By default stepsafterlimitswitch is set
	 // to 10, hence 10 additional pulses should be generated to
	 // the motor
	 mc.invec.RawSwitches_b2 = '0;
	 ##1000;

	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 5000;
	 ICmdParams_x.GenInterrupt = 1;


	 // first we generate movement and we stop the motor:
	 ICmdParams_x.Direction = 1;
	 `start
	 // wait for at least 1st stepout
	 @(negedge mc.outvec.StepOutP_o);
	 #10us;
	 // let's stop and observe if IRQ received
	 fork
	    #10ms;
	    @(posedge StepperStatus_x.Interrupt);
	    begin
	       `stop
	       #100ms;
	    end
	 join_any
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus.StopMove, 1);
	 `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);
	 #100us;
	 // now we setup switch, which does not permit any further
	 // movement
	 ##1 mc.invec.RawSwitches_b2[1] = 1;
	 #100us;

	 // and we start the motion again, IRQ should come
	 // immediatelly and abort status should change to hitting
	 // limit switch.
	 fork
	    #10ms;
	    @(posedge StepperStatus_x.Interrupt);
	    begin
	       `start
	       #100ms;
	    end
	 join_any
	 // check whether the system after stopmove sampled correctly
	 // the abort status - stopmove is bit special as we require
	 // that further startmove into invalid condition
	 // (limitswitch, fail) will resample the abort because IRQ is
	 // generated, motor did not move - so software wants to know why
	 `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus.StopMove, 0);
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus.SW_N_triggered, 1);
	 // note about stoplimit: stoplimit is generated ONLY when
	 // operation of motor movement was completed _and_ additional
	 // pulses of the motor from stepsafterlimitswitch were
	 // generated. This is not a case of stopmove as stopmove
	 // stops the motor without any further pulses generated after
	 // stop limit....
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus.StopLimit, 0);
	 #500us;

      end

      `TEST_CASE("repetitive_start_stop_generates_IRQ") begin
	 // testing here what happens if we stop the move, restart the
	 // movement
	 mc.invec.RawSwitches_b2 = '0;
	 ##1000;

	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 5000;
	 ICmdParams_x.GenInterrupt = 1;

	 // DIRECTION=1, SW_OUT switch hits at certain point
	 ICmdParams_x.Direction = 1;

	 repeat(4) begin
	    `start
	    // wait for at least 1st stepout
	    @(negedge mc.outvec.StepOutP_o);
	    #10us;
	    // let's stop and observe if IRQ received
	    fork
	       #10ms;
	       @(posedge StepperStatus_x.Interrupt);
	       begin
		  `stop
		  #100ms;
	       end
	    join_any
	    `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);
	    #100us;
	 end // repeat (4)
      end // UNMATCHED !!

      `TEST_CASE("interlock_stop_movement_and_irqgen") begin
	 // test if we interlock the movement, so the motor stops on
	 // place and generates IRQ
	 mc.invec.RawSwitches_b2 = '0;
	 ##1000;

	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 5000;
	 ICmdParams_x.GenInterrupt = 1;

	 // DIRECTION=1, SW_OUT switch hits at certain point
	 ICmdParams_x.Direction = 1;
	 `start
	 // wait for at least 1st stepout
	 @(negedge mc.outvec.StepOutP_o);
	 #10us;
	 // let's stop and observe if IRQ received
	 fork
	    #10ms;
	    @(posedge StepperStatus_x.Interrupt);
	    // we are not allowed to produce any more pulses when
	    // interlock happens, what means that when interlocked, NO
	    // MORE PULSES out of motor. If so, that's an error
	    @(negedge mc.outvec.StepOutP_o);
	    begin
	       // this is not 'glitching action, but once device gets
	       // interlocked, it is not allowed to move for certain
	       // time as 'the other motors' caused it, and it wont'
	       // disappear next clock edge.
	       ##1 DeviceInterlocked = '1;
	       #100ms;
	    end
	 join_any
	 `CHECK_EQUAL(mc.outvec.StepOutP_o, 1);
	 `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);

	 // now, let's release device interlock, motor should not move
	 // any more as this operation caused motor to stop
	 fork
	    begin
	       ##1 DeviceInterlocked = '0;
	       #10ms;
	    end
	    @(posedge StepperStatus_x.Busy);
	    #200us;
	 join_any
	 `CHECK_EQUAL(StepperStatus_x.Busy, 0)

      end // UNMATCHED !!


      `TEST_CASE("stopmove_generates_IRQ") begin
	 // testing here what happens if we stop the move - if the IRQ
	 // is generated as well
	 mc.invec.RawSwitches_b2 = '0;
	 ##1000;

	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 5000;
	 ICmdParams_x.GenInterrupt = 1;

	 // DIRECTION=1, SW_OUT switch hits at certain point
	 ICmdParams_x.Direction = 1;
	 `start
	 // wait for at least 1st stepout
	 @(negedge mc.outvec.StepOutP_o);
	 #10us;
	 // let's stop and observe if IRQ received
	 fork
	    #10ms;
	    @(posedge StepperStatus_x.Interrupt);
	    begin
	       `stop
	       #100ms;
	    end
	 join_any
	 `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);
      end

      `TEST_CASE("large_random_fail_trigger_should_stop_motor") begin
	 // repeat 100 times with different timing - motor should
	 // _always trip_ as we generate large enough latching
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 20;
	 repeat(100) begin
	    // checkout if debounced fail correctly stops the motor!
	    fork
	       // run motor
	       begin
		  `start
		  // wait for motor to run
		  wait ($fell(mc.outvec.StepOutP_o));
		  ##10;
		  // rise stepfail by random amount _not causing to trip fail_
		  assert(std::randomize(count) with {count > 20;
		     count < 50;});
		  $display("Generating StepFail width of %d clocks",
			   count);

		  mc.invec.StepPFail_i = '1;
		  ##(count) mc.invec.StepPFail_i = '0;
	       end // fork branch
	       // check for stepper done
	       @(posedge StepperStatus_x.Done);
	    join
	    // and check the reason, stepper HAS TO CONTINUE as we did
	    // not produce fail signal long enough to trip
	    `CHECK_EQUAL(StepperStatus_x.AbortStatus.StepPFail, 1);
	    ##100;
	 end // repeat (100)
	 // and make one more trigger (correct one) to see that
	 // stepfail moves
	 `start
	 // wait for motor to run
	 wait ($fell(mc.outvec.StepOutP_o));
	 @(posedge StepperStatus_x.Done);
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus.StepPFail, 0);
	 ##100;
      end

      `TEST_CASE("small_random_fail_trigger_should_not_stop_motor") begin
	 // repeat 100 times with different timing - motor should
	 // _never trip_
	 repeat(100) begin
	    // checkout if debounced fail correctly stops the motor!
	    fork
	       // run motor
	       begin
		  ICmdParams_x.powerconfig.enable_move = 1;
		  ICmdParams_x.StepNumber_b32 = 20;
		  `start
		  ##2;
		  // wait for motor to run
		  wait ($fell(mc.outvec.StepOutP_o));
		  ##10;
		  // rise stepfail by random amount _not causing to trip fail_
		  assert(std::randomize(count) with {count > 0;
		     count < 19;});
		  $display("Generating StepFail width of %d clocks",
			   count);

		  mc.invec.StepPFail_i = '1;
		  ##(count) mc.invec.StepPFail_i = '0;
	       end // fork branch
	       // check for stepper done
	       @(posedge StepperStatus_x.Done);
	    join
	    // and check the reason, stepper HAS TO CONTINUE as we did
	    // not produce fail signal long enough to trip
	    `CHECK_EQUAL(StepperStatus_x.AbortStatus.StepPFail, 0);
	    ##100;
	 end // repeat (100)
      end

      `TEST_CASE("driver_fail_debounce") begin
	 // apply random short pulse and verify that the thing does
	 // not trip
	 foreach (zeroone[i]) begin
	    // setup initial value of the signal and let propagate
	    mc.invec.StepPFail_i = zeroone[i];
	    ##100;
	    `CHECK_EQUAL(DUT.StepFailDebounced, zeroone[i]);
	    // once propagate we will apply 500 times serie of random
	    // pulse widths at the input, but widths such that they do
	    // not trigger the output signal, and then verify if that
	    // was really triggered or not
	    repeat(500) begin
	       fork
		  // generate random value:
		  begin
		     assert(std::randomize(count) with {count > 0;
			count < 19;});
		     ##1;
		  `CHECK_EQUAL(DUT.StepFailDebounced, zeroone[i]);
		     mc.invec.StepPFail_i = ~zeroone[i];
		     ##(count) mc.invec.StepPFail_i = zeroone[i];
		     // wait for random time, but minimum 20cc as not
		     // to mix it with current debouncing
		     assert(std::randomize(testval) with {testval > 19;
			testval < 500;});
		     ##(testval);
		  end // repeat (500)
		  // check for presence of failing edge:
		  if (zeroone[i])
		    @(posedge DUT.StepFailDebounced);
		  else
		    @(negedge DUT.StepFailDebounced);
	       join_any
	       // check if failed or finished:
	       `CHECK_EQUAL(DUT.StepFailDebounced, zeroone[i]);
	    end // repeat (500)
	 end // foreach (zeroone[i])
      end // UNMATCHED !!


      `TEST_CASE("driver_fail_wiggle one") begin
	 // generate continuous wiggle in signal to see if output
	 // holds
	 mc.invec.StepPFail_i = 1;
	 ##100;
	 `CHECK_EQUAL(DUT.StepFailDebounced, 1)
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(DUT.StepFailDebounced, 1);
	    mc.invec.StepPFail_i = 0;
	    ##1;
	    mc.invec.StepPFail_i = 1;
	    `CHECK_EQUAL(DUT.StepFailDebounced, 1);
	 end

      end // UNMATCHED !!

      `TEST_CASE("driver_fail_wiggle_zero") begin
	 // generate continuous wiggle in signal to see if output holds
	 `CHECK_EQUAL(DUT.StepFailDebounced, 0)
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(DUT.StepFailDebounced, 0);
	    mc.invec.StepPFail_i = 1;
	    ##1;
	    mc.invec.StepPFail_i = 0;
	    `CHECK_EQUAL(DUT.StepFailDebounced, 0);
	 end
      end // UNMATCHED !!

      `TEST_CASE("driver_fail_debounce_width") begin
	 // check that shorter than 20 cycles signal should not
	 // generate output fail trigger
	 fork
	    for(count=1; count<100; count++) begin
	       ##1;
	       mc.invec.StepPFail_i = 1;
	       ##(count);
	       mc.invec.StepPFail_i = 0;
	       ##200;
	    end
	    @(posedge DUT.StepFailDebounced);
	 join_any

	 // count indicates how many cycles is required to intercept
	 // fail:
	 $display("Number of 40MHz clock cycles to intercept fail:\
 %d", count);
	 if (count < 18 || count > 30)
	   $error("Number of clock cycles to trigger fail outside of\
 range");
      end // UNMATCHED !!

      `TEST_CASE("functionality_of_slowdown") begin

	 // this one is long. What happens is that we measure first
	 // discance between two stepout pulses, and then up to 2**5
	 // slowdown. The ratio should not be more than 5% different
	 // from original value
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 50;
	 // no acceleration this time as it would be a hell to find on
	 // ramps whether
	 ICmdParams_x.HighSpeed_b36 = ICmdParams_x.LowSpeed_b36;
	 ICmdParams_x.GenInterrupt = 0;
	 ICmdParams_x.Direction = 0;
	 CntParam_ix.SlowDown_b32 = 0;
	 `start
	 wait ($fell(mc.outvec.StepOutP_o));
	 count = $time;
	 wait ($rose(mc.outvec.StepOutP_o));
	 wait ($fell(mc.outvec.StepOutP_o));
	 // this is distance between steps:
	 unitDistance = $time - count;
	 // wait end of cycle
	 $display("Unit step distance: ", unitDistance);
	 wait ($rose(StepperStatus_x.Done));

	 // slowdown values:
	 // 0, 1 = feature disabled, full 125MHz speed (not good idea,
	 // but currently supported feature)
	 // 2 =
	 // 3 = clk divided by 3
	 // ...
	 // 17 = clk divided by 17
	 foreach(testedvals[i]) begin
	    // now we fire again, but with slowdown
	    CntParam_ix.SlowDown_b32 = testedvals[i];
	    `start
	    wait ($fell(mc.outvec.StepOutP_o));
	    count = $time;
	    wait ($rose(mc.outvec.StepOutP_o));
	    wait ($fell(mc.outvec.StepOutP_o));
	    // this is distance between steps:
	    slowDistance = $time - count;
	    // definition of slow down is as follows:
	    // if 0 or 1, then the signal is the same,
	    // any other number is that amount of distance * unit
	    // distance:
	    if (testedvals[i] == 0 ||
		testedvals[i] == 1)
	      expected = $itor(unitDistance);
	    else
	      expected = $itor(unitDistance) * testedvals[i];
	    ratio = $itor(slowDistance) / $itor(unitDistance);
	    // handle special case:
	    if (testedvals[i] == 0)
	      diff = (ratio-1.0)/ratio*100.0;
	    else
	      diff = (ratio-testedvals[i])/ratio*100.0;

	    $display("Slow step ", testedvals[i],
		     " distance: ", slowDistance,
		     " ratio ", ratio,
		     " expected ", expected,
		     " diff: ", diff);
	    wait ($rose(StepperStatus_x.Done));
	    // allow 0.2% dispersion:
	    assert(absolute(diff) < 0.2) else
	      $error("Error too high");

	 end; // foreach (testedvals[i])


      end

      `TEST_CASE("motor_direction_signal") begin
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 50;
	 ICmdParams_x.GenInterrupt = 0;

	 // DIRECTION=0, SW_IN switch hits at certain point
	 ICmdParams_x.Direction = 0;
	 `start
	 // enforce switch triggering
	 ##1;
         `CHECK_EQUAL(mc.outvec.StepDIR_o, 0);
	 wait ($rose(StepperStatus_x.Done));
	 // add 1cc because this is 'last' pulse out of stepper and
	 // within this we cannot change the direction
	 ##1;
	 // now change the direction and run the movement again
	 ICmdParams_x.Direction = 1;
	 `start
	 // enforce switch triggering
	 @(negedge mc.outvec.StepOutP_o);
	 ##1;
         `CHECK_EQUAL(mc.outvec.StepDIR_o, 1);
      end

      `TEST_CASE("rising_IRQ_when_in_switch") begin
	 // we drive the motor into extremity with IRQ disabled, and
	 // then we run the motor again. although the motor should not
	 // move, we want to generate IRQ as this is a way how to
	 // indicate that the 'operation was performed', but with fail
	 // status (it is up to sw to find if the operation was
	 // performed OK)
	 `checkSwitches(1, DUT.IsNExtremity,
			StepperStatus_x.AbortStatus.SW_N_triggered);
	 // generation of IRQ turned on, and we try to move again
	 ICmdParams_x.GenInterrupt = 1;
	 for(int zz=0; zz < 10; zz++) begin
	    // run move and wait for irq
	    #1us;
	    fork
	       begin
		  `start
		  forever @(posedge ClkRs_ix.clk);
	       end
	       @(posedge StepperStatus_x.Interrupt);
	    join_any;
	    `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);
	 end // for (int zz=0; zz < 10; zz++)
	 #10us;

	 // and when IRQ is disabled, it should not generate anything
	 ICmdParams_x.GenInterrupt = 0;
	 #1us;
	 fork
	    begin
	       `start
	       forever @(posedge ClkRs_ix.clk);
	    end
	    @(posedge StepperStatus_x.Interrupt);
	    #100us;
	 join_any;
	 `CHECK_EQUAL(StepperStatus_x.Interrupt, 0);

	 // and one more with IRQ enabled to see how it handles:
	 ICmdParams_x.GenInterrupt = 1;
	 // run move and wait for irq
	 #1us;
	 fork
	    begin
	       `start
	       forever @(posedge ClkRs_ix.clk);
	    end
	    @(posedge StepperStatus_x.Interrupt);
	 join_any;
	 `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);
      end // UNMATCHED !!


      `TEST_CASE("OUT_switch_functionality") begin
	 `checkSwitches(1, DUT.IsNExtremity,
			StepperStatus_x.AbortStatus.SW_N_triggered);
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus, 200);
	 `checkSwitches(0, DUT.IsPExtremity,
			StepperStatus_x.AbortStatus.SW_P_triggered);
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus, 68);
      end

      `TEST_CASE("IN_switch_functionality") begin
	 `checkSwitches(0, DUT.IsPExtremity,
			StepperStatus_x.AbortStatus.SW_P_triggered);
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus, 68);
	 `checkSwitches(1, DUT.IsNExtremity,
			StepperStatus_x.AbortStatus.SW_N_triggered);
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus, 200);
      end

      `TEST_CASE("rising_IRQ") begin
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 50;
	 ICmdParams_x.GenInterrupt = 1;
	 `start
	 wait (StepperStatus_x.Done);
	 // only direction allowed, no forced interrupt
         `CHECK_EQUAL(StepperStatus_x.Interrupt, 1);

	 // no interrupt now
	 ##10;
	 ICmdParams_x.GenInterrupt = 0;
	 `start
	 wait (StepperStatus_x.Done);
         `CHECK_EQUAL(StepperStatus_x.Interrupt, 0);
      end

      `TEST_CASE("traverse_all_states") begin
	 // this is fancy: we start a cycle and verify by assertion
	 // that machine passes through correct states. The cycle we
	 // fire is starting with acceleration, cruising, decelation
	 // and idle

	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 5000;
	 `start
	 count = 0;
	 // setup first state IDLE, so that idle state does not count
	 // into total number of states. The IDLE at the end of cycle
	 // will be counted
	 smstate = StepperStatus_x.State;

	 while (!$rose(StepperStatus_x.Done)) begin
	    ##1;
	    if (smstate != StepperStatus_x.State) begin
	       smstate = StepperStatus_x.State;
	       count++;
	    end;
	 end;
	 // only direction allowed, no forced interrupt
         `CHECK_EQUAL(StepperStatus_x.AbortStatus, 8'h80);
	 // this checks, whether we have traversed all the state
	 // machine states
         `CHECK_EQUAL(count, 7);

      end

      `TEST_CASE("overheat_aborts") begin
	 // modify default params to get only 10 steps, this thing is
	 // tested by SV PSL assert in the code
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 10;
	 `start
	 ##2;
	 wait ($fell(mc.outvec.StepOutP_o));
	 ##10;
	 // rise overheat
	 ##1 mc.invec.OH_i = 1;
	 ##1 mc.invec.OH_i = 0;
	 ##100;
	 `CHECK_EQUAL(StepperStatus_x.AbortStatus.OH, 1);
      end

      `TEST_CASE("position_counter_reset") begin
	 // modify default params to get only 10 steps
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.globaltrigger.external = 0;
	 ICmdParams_x.globaltrigger.internal = 1;
	 ICmdParams_x.StepNumber_b32 = 10;
	 ##1 triggers.TrigMoveExt_i = 1;
	 ##1 triggers.TrigMoveExt_i = 0;
	 ##2;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 end

	 ##1 triggers.TrigMoveInt_i = 1;
	 ##1 triggers.TrigMoveInt_i = 0;
	 ##5;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 0);
         `CHECK_EQUAL(StepperStatus_x.Busy, 1);
	 wait ($fell(StepperStatus_x.Busy));
	 ##10;
	 `CHECK_EQUAL(StepperStatus_x.PosCounter_b32, -10);
	 // issue soft reset, but this one is requivalent of 'reload',
	 // which means that PosCounter_b32 gets filled with reload
	 ##1 ITriggerParams_x.ResetCounterPos = 1;
	 ##1 ITriggerParams_x.ResetCounterPos = 0;
	 ##5;
	 `CHECK_EQUAL(StepperStatus_x.PosCounter_b32, reload_ib32);
      end

      `TEST_CASE("position_counter_functionality") begin
	 // modify default params to get only 10 steps
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.globaltrigger.external = 0;
	 ICmdParams_x.globaltrigger.internal = 1;
	 ICmdParams_x.StepNumber_b32 = 10;
	 ##1 triggers.TrigMoveExt_i = 1;
	 ##1 triggers.TrigMoveExt_i = 0;
	 ##2;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 end

	 ##1 triggers.TrigMoveInt_i = 1;
	 ##1 triggers.TrigMoveInt_i = 0;
	 ##5;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 0);
         `CHECK_EQUAL(StepperStatus_x.Busy, 1);
	 wait ($fell(StepperStatus_x.Busy));
	 ##10;
	 `CHECK_EQUAL(StepperStatus_x.PosCounter_b32, -10);
	 ICmdParams_x.Direction = 0;
	 ##1 triggers.TrigMoveInt_i = 1;
	 ##1 triggers.TrigMoveInt_i = 0;
	 wait ($fell(StepperStatus_x.Busy));
	 ##10;
	 `CHECK_EQUAL(StepperStatus_x.PosCounter_b32, 0);
	 ##1 triggers.TrigMoveInt_i = 1;
	 ##1 triggers.TrigMoveInt_i = 0;
	 wait ($fell(StepperStatus_x.Busy));
	 ##10;
	 `CHECK_EQUAL(StepperStatus_x.PosCounter_b32, 10);
	 ICmdParams_x.Direction = 1;
	 ##1 triggers.TrigMoveInt_i = 1;
	 ##1 triggers.TrigMoveInt_i = 0;
	 wait ($fell(StepperStatus_x.Busy));
	 ##10;
	 `CHECK_EQUAL(StepperStatus_x.PosCounter_b32, 0);
      end

      `TEST_CASE("internal_triggering") begin
	 // modify default params to get only 10 steps
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.globaltrigger.external = 0;
	 ICmdParams_x.globaltrigger.internal = 1;
	 ICmdParams_x.StepNumber_b32 = 10;
	 ##1 triggers.TrigMoveInt_i = 1;
	 ##1 triggers.TrigMoveInt_i = 0;
	 ##5;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 0);
         `CHECK_EQUAL(StepperStatus_x.Busy, 1);


      end

      `TEST_CASE("external_triggering") begin
	 // modify default params to get only 10 steps
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.globaltrigger.external = 1;
	 ICmdParams_x.globaltrigger.internal = 0;
	 ICmdParams_x.StepNumber_b32 = 10;
	 ##1 triggers.TrigMoveInt_i = 1;
	 ##1 triggers.TrigMoveInt_i = 0;
	 ##2;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 end

	 ##1 triggers.TrigMoveExt_i = 1;
	 ##1 triggers.TrigMoveExt_i = 0;
	 ##5;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 0);
         `CHECK_EQUAL(StepperStatus_x.Busy, 1);
      end

      `TEST_CASE("no_external_internal_trigger_when_disabled") begin
	 // modify default params to get only 10 steps
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.globaltrigger.external = 0;
	 ICmdParams_x.globaltrigger.internal = 0;
	 ICmdParams_x.StepNumber_b32 = 10;
	 ##1 triggers.TrigMoveInt_i = 1;
	 ##1 triggers.TrigMoveInt_i = 0;
	 ##2;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 end

	 ##1 triggers.TrigMoveExt_i = 1;
	 ##1 triggers.TrigMoveExt_i = 0;
	 ##2;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 end
      end

      `TEST_CASE("nothing_can_trigger_when_move_not_enabled") begin
	 // modify default params to get only 10 steps
	 ICmdParams_x.powerconfig.enable_move = 0;
	 ICmdParams_x.globaltrigger.external = 1;
	 ICmdParams_x.globaltrigger.internal = 1;
	 ICmdParams_x.StepNumber_b32 = 10;
	 ##10;
	 `start
	 ##2;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 end
	 ##1 triggers.TrigMoveInt_i = 1;
	 ##1 triggers.TrigMoveInt_i = 0;
	 ##2;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 end

	 ##1 triggers.TrigMoveExt_i = 1;
	 ##1 triggers.TrigMoveExt_i = 0;
	 ##2;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 end
      end

      `TEST_CASE("disabled_move_cannot_trigger_pulses") begin
	 // modify default params to get only 10 steps
	 ICmdParams_x.powerconfig.enable_move = 0;
	 ICmdParams_x.StepNumber_b32 = 10;
	 ##10;
	 `start
	 ##2;
	 // this should not trigger
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 end

      end

      `TEST_CASE("move_stop_causes_abort") begin
	 $display("enabling move");

	 // modify default params to get only 10 steps
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 10;
	 ##10;
	 `start
	 ##3;
	 $display("Motor started, waiting for pulseout");

	 // 4 cycles later at last the enab has to go low - this
	 // depends on details of implementation. when added delays
	 // because of timing, this test will probably break.
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 0);
	 wait ($fell(mc.outvec.StepOutP_o));
	 // inside pulse generation we force stop and we expect this
	 // one to be finished and that's it
	 ##10 ITriggerParams_x.StopMove = 1;
	 ##1 ITriggerParams_x.StopMove = 0;
	 wait ($rose(mc.outvec.StepOutP_o));
	 `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 ##100;

	 //`CHECK_EQUAL(StepperStatus_x.AbortStatus.StopMove, 1);
      end

      `TEST_CASE("pulses_match") begin
	 // this is to verify, whether exactly stepnumber pulses go off
	 // modify default params to get only 10 steps
	 ICmdParams_x.powerconfig.enable_move = 1;
	 ICmdParams_x.StepNumber_b32 = 134;
	 ##10;
	 `start
	 ##1;
	 ##1;
	 ##1;
	 // 4 cycles later at last the enab has to go low
         `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 0);
	 // let's count amount of steps generated

	 while (!mc.outvec.StepDeactivate_o) begin
	    ##1;
	    if ($fell(mc.outvec.StepOutP_o))
	      count++;
	 end
	 //
         `CHECK_EQUAL(count, 134);
      end

   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10000s);


   steppingcontroller #(/*AUTOINSTPARAM*/) DUT
     (/*AUTOINST*/
      // Interfaces
      .mc				(mc),
      // Outputs
      .StepperStatus_x			(StepperStatus_x),
      .ConfiguredSwitches_b2		(ConfiguredSwitches_b2[1:0]),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .ICmdParams_x			(ICmdParams_x),
      .ITriggerParams_x			(ITriggerParams_x),
      .DeviceInterlocked		(DeviceInterlocked),
      .CntParam_ix			(CntParam_ix),
      .triggers				(triggers),
      .reload_ib32			(reload_ib32[31:0]));

endmodule
