//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) May 2018 CERN

//-----------------------------------------------------------------------------
// @file TB_STRANSPORT.SV
// @brief
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 11 May 2018
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

module tb_stransport;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};
   localparam g_numBits = 32;


   logic [g_numBits-1:0] data_i, data_o;
   logic [83:0] 	 longdatain, longdataout;
   logic sdata_o;

   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking
   logic [g_numBits-1:0] sample;


      `TEST_SUITE begin

	 `TEST_SUITE_SETUP begin
	    data_i = 32'haabbccdd;

	    ClkRs_ix.reset = 1;
	    ##10;
	    ClkRs_ix.reset = 0;
	    $display("Reset done");
	 end

	 `TEST_CASE("debug_test") begin
	    `CHECK_EQUAL(data_o, 32'haabb_ccdd);
	    $display("Check 5555_5555");
	    data_i = 32'h55555556;
	    @(posedge ClkRs_ix.clk);
	    `CHECK_EQUAL(data_o, 32'h5555_5556);
	    $display("Check aaaa_aaaa");
	    data_i = 32'haaaa_aaaa;
	    @(posedge ClkRs_ix.clk);
	    `CHECK_EQUAL(data_o, 32'haaaa_aaaa);
	    $display("Random numbers check");
	    @(posedge ClkRs_ix.clk);
	    repeat(1000) begin
	       // every clock cycle one large sample has to be
	       // transported
	       assert(std::randomize(sample));
	       assert(std::randomize(longdatain));
	       data_i = sample;
	       @(posedge ClkRs_ix.clk);
	       `CHECK_EQUAL(data_o, data_i);
	       `CHECK_EQUAL(longdatain, longdataout);
	    end
	 end

      end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(100ms);

   stransport_tx
     #(.g_numBits(g_numBits))
   i_stransport_tx (.clk_ik(ClkRs_ix.clk),
		    .*);

   stransport_rx
		  #(.g_numBits(g_numBits))
   i_stransport_rx (.clk_ik(ClkRs_ix.clk),
		    .sdata_i(sdata_o),
		    .*);

   // make 84bits TX to find if with given clocks it is still OK
   logic serout;

   stransport_tx
     #(.g_numBits(84))
   i_stransportlong_tx (.clk_ik(ClkRs_ix.clk),
			.sdata_o(serout),
			.data_i(longdatain),
			.*);
   stransport_rx
     #(.g_numBits(84))
   i_stransportlong_rx (.clk_ik(ClkRs_ix.clk),
		    .sdata_i(serout),
		    .data_o(longdataout),
		    .*);


endmodule
