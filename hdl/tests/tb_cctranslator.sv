//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_CCTRANSLATOR.SV
// @brief tests how the translator works with data enable
// @author Dr. David Belohrad  <david.belohrad@cern.ch>, CERN
// @date 31 October 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

// tests how the translator works with data enable
module tb_cctranslator;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   localparam g_DataWidth  = 32;
   localparam g_FifoWidth = 4;

   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};
   ckrs_t ClkRsSlow_ix = '{clk:'0, reset:'0};

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic		wfifoOverrun_o;		// From DUT of cctranslator.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic		woverrunClear_i;	// To DUT of cctranslator.v
   // End of automatics

   int 			i;

   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   always forever #((3.3*clk_period)/2 * 1ns) ClkRsSlow_ix.clk <= ~ClkRsSlow_ix.clk;
   default clocking cb @(negedge ClkRs_ix.clk); endclocking

   // two interfaces for data
   data_x #(.g_DataWidth(g_DataWidth)) wdata_ix(.ClkRs_ix(ClkRs_ix));
   data_x #(.g_DataWidth(g_DataWidth)) rdata_ox(.ClkRs_ix(ClkRsSlow_ix));


   `TEST_SUITE begin
      `TEST_SUITE_SETUP begin
	 woverrunClear_i = 0;

	 wdata_ix.data = '0;
	 wdata_ix.enable = 0;
	 ##10;
	 ClkRs_ix.reset = 1;
	 ##1;
	 ClkRs_ix.reset = 0;
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 ClkRsSlow_ix.reset = 1;
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 ClkRsSlow_ix.reset = 0;
	 ##10;
      end

      `TEST_CASE("debug_test") begin
	 // flooding fifo without readout
	 wdata_ix.data = 10;
	 wdata_ix.enable = 1;
	 // we run at full blast until fifo gets full. Which it should as write
	 // clock is faster, this is by checking internally if fifo overran
	 do
	   ##1;
	 while(!DUT.wFifoFull_o);
	 // stop writing
	 wdata_ix.enable = 0;
	 ##1;

	 // fifo overrup should go '1' and never come back
	 `CHECK_EQUAL(wfifoOverrun_o, 1, "FIFO does not overrun");
	 ##100;
	 `CHECK_EQUAL(wfifoOverrun_o, 1, "FIFO does not overrun");
	 // reset
	 woverrunClear_i = 1;
	 ##1;
	 woverrunClear_i = 0;
	 `CHECK_EQUAL(wfifoOverrun_o, 0, "FIFO overrun does not clear");
      end

      `TEST_CASE("overrun_flag_clearout") begin
	 // flooding fifo without readout
	 wdata_ix.data = 10;
	 wdata_ix.enable = 1;
	 // we run at full blast until fifo gets full. Which it should as write
	 // clock is faster, this is by checking internally if fifo overran
	 do
	   ##1;
	 while(!DUT.wFifoFull_o);
	 // stop writing
	 wdata_ix.enable = 0;
	 ##1;

	 // fifo overrup should go '1' and never come back
	 `CHECK_EQUAL(wfifoOverrun_o, 1, "FIFO does not overrun");
	 ##100;
	 `CHECK_EQUAL(wfifoOverrun_o, 1, "FIFO does not overrun");
	 // reset
	 woverrunClear_i = 1;
	 ##1;
	 woverrunClear_i = 0;
	 `CHECK_EQUAL(wfifoOverrun_o, 0, "FIFO overrun does not clear");
      end

      `TEST_CASE("two_data") begin
	 // two pieces
	 wdata_ix.data = 10;
	 wdata_ix.enable = 1;
	 ##1;
	 wdata_ix.data = 11;
	 ##1;
	 wdata_ix.enable = 0;
	 for(i=0; i < 100; i++) begin
	    if(rdata_ox.enable) break;
	    @(posedge rdata_ox.ClkRs_ix.clk);
	 end
	 assert(i < 99) else $error("FAILED TO SEARCH FOR DATA ENABLE IN SLOW DOMAIN");
	 `CHECK_EQUAL(rdata_ox.data, 10, "FAILED TO TRANSPORT DATA");
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 `CHECK_EQUAL(rdata_ox.enable, 1,
		      "ENABLE LAST TWO CYCLES");
	 `CHECK_EQUAL(rdata_ox.data, 11, "FAILED TO TRANSPORT DATA");
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 `CHECK_EQUAL(rdata_ox.enable, 0,
		      "ENABLE LAST ONLY TWO CYCLES");
      end

      `TEST_CASE("single_data_in") begin
	 // write single data in, check direct propagation. This should not take
	 // more than 10 cycles in fast domain (slow domain is 2.3 times
	 // slower), we do not really care about internals,
	 wdata_ix.data = 10;
	 wdata_ix.enable = 1;
	 ##1;
	 wdata_ix.enable = 0;
	 for(i=0; i < 100; i++) begin
	    if(rdata_ox.enable) break;
	    @(posedge rdata_ox.ClkRs_ix.clk);
	 end
	 assert(i < 99) else $error("FAILED TO SEARCH FOR DATA ENABLE IN SLOW DOMAIN");
	 `CHECK_EQUAL(rdata_ox.data, 10, "FAILED TO TRANSPORT DATA");
	 @(posedge rdata_ox.ClkRs_ix.clk);
	 `CHECK_EQUAL(rdata_ox.enable, 0,
		      "ENABLE SHOULD LAST ONLY SINGLE CYCLE");
      end

   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10ms);

   cctranslator #(/*AUTOINSTPARAM*/
		  // Parameters
		  .g_DataWidth		(g_DataWidth),
		  .g_FifoWidth		(g_FifoWidth)) DUT
     (.*);

endmodule
