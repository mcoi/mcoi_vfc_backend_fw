//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) December 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_MOTOR_EMULATOR.SV
// @brief Motor emulator functionality checks
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 07 December 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

// Motor emulator functionality checks
module tb_dummy_wishbone_module_with_registers;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   localparam g_DataWidth = 32;
   localparam g_AddressWidth = 32;
   localparam		g_NumberOfRegisters = 8;

   ckrs_t ClkRs_ix;
   t_WbInterface #(.g_DataWidth(g_DataWidth),
		   .g_AddressWidth(g_AddressWidth)) Wb_iot(ClkRs_ix.clk, ClkRs_ix.reset);
   logic [g_DataWidth-1:0] ReadData;

   /*AUTOWIRE*/
   /*AUTOREGINPUT*/



   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
         i_WbMasterSim.Reset();
      end

      `TEST_CASE("debug_test") begin

         i_WbMasterSim.WbWrite(0, 32'haabbccdd);
	 i_WbMasterSim.WbRead(0, ReadData);
         `CHECK_EQUAL(ReadData, 32'haabbccdd, "REGISTER RW FAILED");
      end

   end;

   // instantiate wishbone interface
   WbMasterSim
     i_WbMasterSim (// Outputs
		    .Rst_orq		(ClkRs_ix.reset),
		    .Adr_obq32		(Wb_iot.Adr_b),
		    .Dat_obq32		(Wb_iot.DatMoSi_b),
		    .We_oq		(Wb_iot.We),
		    .Cyc_oq		(Wb_iot.Cyc),
		    .Stb_oq		(Wb_iot.Stb),
		    // Inputs
		    .Clk_ik		(ClkRs_ix.clk),
		    .Dat_ib32		(Wb_iot.DatMiSo_b),
		    .Ack_i		(Wb_iot.Ack));

   assign Wb_iot.Sel_b = 4'hf;
   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10ms);

   dummy_wishbone_module_with_registers #(/*AUTOINSTPARAM*/
					  // Parameters
					  .g_NumberOfRegisters	(g_NumberOfRegisters),
					  .g_DataWidth		(g_DataWidth)) DUT
     (.*);

endmodule
