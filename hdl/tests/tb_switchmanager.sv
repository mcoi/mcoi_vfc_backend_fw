//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) June 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_SWITCHMANAGER.SV
// @brief tests switchmanager functionality
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 27 June 2017
// @details
// switch manager can case one-of-N input switches and can revert their polarity
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

// tests switchmanager functionality
module tb_switchmanager;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   reg 		      Clk_ik = 1'b0;
   localparam integer g_NumSwitches = 5;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic		SWSelect;		// From DUT of switchmanager.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic		Polarity;		// To DUT of switchmanager.v
   logic [1:0]		SelectedInputSwitches_b2;// To DUT of switchmanager.v
   logic [1:0]		Switch_b2;		// To DUT of switchmanager.v
   // End of automatics

   always forever #(clk_period/2 * 1ns) Clk_ik <= !Clk_ik;
   default clocking cb @(posedge Clk_ik); endclocking


   `TEST_SUITE begin
      `TEST_CASE("switch_selector_sw1_connection") begin
	 SelectedInputSwitches_b2 <= 2'b10;
	 Polarity <= 0;
	 Switch_b2 <= 2'b00;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b01;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b10;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 1);
	 Switch_b2 <= 2'b11;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 1);
      end

      `TEST_CASE("switch_selector_both_switches_connection") begin
	 SelectedInputSwitches_b2 <= 2'b11;
	 Polarity <= 0;
	 Switch_b2 <= 2'b00;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b01;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 1);
	 Switch_b2 <= 2'b10;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 1);
	 Switch_b2 <= 2'b11;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 1);
      end

      `TEST_CASE("switch_selector_sw0_connection") begin
	 SelectedInputSwitches_b2 <= 2'b01;
	 Polarity <= 0;
	 Switch_b2 <= 2'b00;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b01;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 1);
	 Switch_b2 <= 2'b10;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b11;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 1);
      end

      `TEST_CASE("switch_selector_no_connection") begin
	 // let's start with 'no connection at all'
	 SelectedInputSwitches_b2 <= '0;
	 // and no matter what we select as inputs, output has to be zero
	 Polarity <= 0;
	 Switch_b2 <= 2'b00;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b01;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b10;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b11;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
      end

      `TEST_CASE("inverse_polarity_input_signals") begin
	 // select switch0 as target:
	 SelectedInputSwitches_b2 <= 2'b01;
	 // original 1:1 signal polarity:
	 Polarity <= 1;
	 Switch_b2 <= '0;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 1);
	 Switch_b2 <= 2'b01;
	 ##10;
	 // as SelectedInputSwitches_b2 is set to zero, switch '1' should propagate to '1'
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b10;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 1);

	 // select switch 1 as target and make the same exercise:
	 SelectedInputSwitches_b2 <= 2'b10;
	 // original 1:1 signal polarity:
	 Polarity <= 1;
	 Switch_b2 <= '0;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 1);
	 Switch_b2 <= 2'b01;
	 ##10;
	 // as SelectedInputSwitches_b2 is set to zero, switch '1' should propagate to '1'
	 `CHECK_EQUAL(SWSelect, 1);
	 Switch_b2 <= 2'b10;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
      end

      `TEST_CASE("regular_polarity_input_signals") begin
	 // select switch0 as target:
	 SelectedInputSwitches_b2 <= 2'b01;
	 // original 1:1 signal polarity:
	 Polarity <= 0;
	 Switch_b2 <= '0;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b01;
	 ##10;
	 // as SelectedInputSwitches_b2 is set to zero, switch '1' should propagate to '1'
	 `CHECK_EQUAL(SWSelect, 1);
	 Switch_b2 <= 2'b10;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);

	 // select switch 1 as target and make the same exercise:
	 SelectedInputSwitches_b2 <= 2'b10;
	 // original 1:1 signal polarity:
	 Polarity <= 0;
	 Switch_b2 <= '0;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b01;
	 ##10;
	 // as SelectedInputSwitches_b2 is set to zero, switch '1' should propagate to '1'
	 `CHECK_EQUAL(SWSelect, 0);
	 Switch_b2 <= 2'b10;
	 ##10;
	 `CHECK_EQUAL(SWSelect, 1);
      end

   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10ms);

   switchmanager  DUT
     (/*AUTOINST*/
      // Outputs
      .SWSelect				(SWSelect),
      // Inputs
      .Switch_b2			(Switch_b2[1:0]),
      .Polarity				(Polarity),
      .SelectedInputSwitches_b2		(SelectedInputSwitches_b2[1:0]));


endmodule
