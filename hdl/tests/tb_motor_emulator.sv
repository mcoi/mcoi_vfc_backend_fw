//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) December 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_MOTOR_EMULATOR.SV
// @brief Motor emulator functionality checks
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 07 December 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;
import MCPkg::*;


// Motor emulator functionality checks
module tb_motor_emulator;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   localparam g_AddressWidth = 32;
   localparam g_NumberOfRegisters = 8;
   localparam g_DataWidth = 32;
   // this is to limit amount of time spent in simulation when trying
   // to get fails
   localparam g_OrMask = 'h80;
   int 		      mult;



   ckrs_t ClkRs_ix;
   t_WbInterface #(.g_DataWidth(g_DataWidth),
		   .g_AddressWidth(g_AddressWidth)) Wb_iot(ClkRs_ix.clk, ClkRs_ix.reset);
   data_x #(.g_DataWidth(g_DataWidth)) Random_iot(ClkRs_ix);

   logic [g_DataWidth-1:0] ReadData;
   mcinput_t mc_ox;
   mcoutput_t mc_ix;


   /*AUTOWIRE*/
   /*AUTOREGINPUT*/

   task TripFail;
      input int whichbit;
      begin
	 $display("Checking bit", whichbit);

	 for (int i = 0; i < 10; i++) begin
	    // writing into zero into the first mask, and tripping bit 30
	    // should cause loading counter and setting up bit in status
	    // register that failure mode started
	    `CHECK_EQUAL(muxedfail, 0, "Fail bit is not zero by default");
	    // set both masks to small (below 24bits) number, hence we
	    // open window for some fancy random behaviour
            i_WbMasterSim.WbWrite(4, 'h1ff);
            i_WbMasterSim.WbWrite(5, 'h1ff);
	    i_WbMasterSim.WbWrite(3, 1 << whichbit);
	    // check if OH in progress
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[whichbit], 1, "BIT FAIL BIT NOT TURNED ON");
	    do begin
	       i_WbMasterSim.WbRead(6, ReadData);
	       // check if STATUS contains bit2/3 turned OFF during
	       // this period as it should not yet be turned on.
	       // OH is bit 2, Stepfail is bit 3. When test in
	       // progress, the signals should be inactive, but when
	       // finished waiting, the signal should become active
	       if (ReadData[whichbit])
		 `CHECK_EQUAL(ReadData[whichbit-30+2], 0,
			      "Checked signal should not be active yet")
	    end while (ReadData[whichbit]);
	    `CHECK_EQUAL(muxedfail, 1, "FAIL should be injected");
	    // now check motor iface, OH should be tripped
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[whichbit-30+2], 1,
			 "FAIL signal status bit reading should be 1")
	    ##100;
	    `CHECK_EQUAL(ReadData[whichbit-30+2], 1,
			 "FAIL signal status bit reading should be 1")
	    // check again, should be still tripped
	    `CHECK_EQUAL(muxedfail, 1, "FAIL should be still injected");
	    // read through the VME register, the proper location of
	    // fail status is
	    // reset
	    i_WbMasterSim.WbWrite(3, 1<<2);
	    `CHECK_EQUAL(muxedfail, 0, "FAIL should be zero again");
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[whichbit-30+2], 0,
			 "FAIL signal status bit reading should be cleared")
	 end // for (int i = 0; i < 10; i++)
      end
   endtask // TripFail


   task SetupMotor(input int MaxPosition, INSwitch, OUTSwitch);
      // motor total count
      i_WbMasterSim.WbWrite(0, MaxPosition);
      // inswitch position
      i_WbMasterSim.WbWrite(1, INSwitch);
      // outswitch position
      i_WbMasterSim.WbWrite(2, OUTSwitch);
      // masks set to lower 16 bits
      i_WbMasterSim.WbWrite(4, 65535);
      i_WbMasterSim.WbWrite(5, 65535);
   endtask; // SetupMotor



   // here we cast bit for failure scenario
   logic muxedfail;
   int 	 bix;

   always_comb begin
      if (bix == 30)
	muxedfail = mc_ox.OH_i;
      else if (bix == 31)
	muxedfail = mc_ox.StepPFail_i;
      else
	muxedfail = '0;
   end



   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   logic [g_DataWidth-1:0] rnd;

   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 // motor inputs default to zero
	 // enab is actually negative (at certain point I have to
	 // rename it)
	 mc_ix.StepDeactivate_o = 1;
	 mc_ix.StepBOOST_o = 0;
	 mc_ix.StepDIR_o = 0;
	 mc_ix.StepOutP_o = 0;

         i_WbMasterSim.Reset();
      end

      `TEST_CASE("debug_test") begin
	 // motor movement. Let's move motor positive and negative
	 // setup motor max and masks
	 SetupMotor(100, -80, 80);
	 // what happens if we trigger random failure count, and then
	 // trig it again? we should trigger that thing
	 // immediatelly. Hence: we start triggering, the countdown
	 // for failure event starts, we trigger again -> fail occurs
	 // immediatelly. This allows quick fail injection. Both OH
	 // and StepPFail_i must provide this functionality
	 // set mux
	 for(int bix = 30; bix < 32; bix++) begin
	 // set both masks to small (below 24bits) number, hence we
	 // open window for some fancy 'random' behaviour
         i_WbMasterSim.WbWrite(4, 'h100);
         i_WbMasterSim.WbWrite(5, 'h100);
	 // triggering
	 i_WbMasterSim.WbWrite(3, 1 << bix);
	 // and verify fail functionality
	 // check if the countdown is in progress
	 i_WbMasterSim.WbRead(6, ReadData);
	 `CHECK_EQUAL(ReadData[bix], 1, "BIT FAIL BIT NOT TURNED ON");
	 `CHECK_EQUAL(ReadData[bix-30+2], 0,
		      "FAIL SHOULD NOT BE ANNOUNCED");
	 // and now we trigger once more
	 i_WbMasterSim.WbWrite(3, 1 << bix);
	 // and here the counting should be stopped, and fail signal
	 // turned on
	 i_WbMasterSim.WbRead(6, ReadData);
	 `CHECK_EQUAL(ReadData[bix], 0, "BIT FAIL BIT IS TURNED ON");
	 `CHECK_EQUAL(ReadData[bix-30+2], 1,
		      "FAIL SHOULD BE ANNOUNCED");
	 end
      end

      `TEST_CASE("inject_failure_immediatelly") begin
	 // motor movement. Let's move motor positive and negative
	 // setup motor max and masks
	 SetupMotor(100, -80, 80);
	 // what happens if we trigger random failure count, and then
	 // trig it again? we should trigger that thing
	 // immediatelly. Hence: we start triggering, the countdown
	 // for failure event starts, we trigger again -> fail occurs
	 // immediatelly. This allows quick fail injection. Both OH
	 // and StepPFail_i must provide this functionality
	 // set mux
	 for(int bix = 30; bix < 32; bix++) begin
	 // set both masks to small (below 24bits) number, hence we
	 // open window for some fancy 'random' behaviour
         i_WbMasterSim.WbWrite(4, 'h100);
         i_WbMasterSim.WbWrite(5, 'h100);
	 // triggering
	 i_WbMasterSim.WbWrite(3, 1 << bix);
	 // and verify fail functionality
	 // check if the countdown is in progress
	 i_WbMasterSim.WbRead(6, ReadData);
	 `CHECK_EQUAL(ReadData[bix], 1, "BIT FAIL BIT NOT TURNED ON");
	 `CHECK_EQUAL(ReadData[bix-30+2], 0,
		      "FAIL SHOULD NOT BE ANNOUNCED");
	 // and now we trigger once more
	 i_WbMasterSim.WbWrite(3, 1 << bix);
	 // and here the counting should be stopped, and fail signal
	 // turned on
	 i_WbMasterSim.WbRead(6, ReadData);
	 `CHECK_EQUAL(ReadData[bix], 0, "BIT FAIL BIT IS TURNED ON");
	 `CHECK_EQUAL(ReadData[bix-30+2], 1,
		      "FAIL SHOULD BE ANNOUNCED");
	 end
      end

      `TEST_CASE("switches_inversion") begin
	 // motor movement. Let's move motor positive and negative
	 // setup motor max and masks
	 SetupMotor(100, -80, 80);
	 for(int direction=0; direction < 2; direction++) begin
	    // read status for switch info
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[direction], 0, "Switch should be 0 by default");
	    `CHECK_EQUAL(mc_ox.RawSwitches_b2[direction], 0,
			 "HW Switch iface issue");
	    // reverse switch direction
	    i_WbMasterSim.WbWrite(3, direction+1);
	    // and assert the state
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[direction], 1, "Switch should be 1 by default");
	    `CHECK_EQUAL(mc_ox.RawSwitches_b2[direction], 1,
			 "HW Switch iface issue");

	 end
      end

      `TEST_CASE("hw_switches_test") begin
	 // motor movement. Let's move motor positive and negative
	 // setup motor max and masks
	 SetupMotor(100, -80, 80);

	 for (int direction = 0; direction < 2; direction++) begin
	    if (direction == 0)
	      mult = 1;
	    else
	      mult = -1;

	    // enable motor stepping
	    mc_ix.StepDeactivate_o = 0;
	    // direction = 0 -> increase counter until hitting SWOUT
	    mc_ix.StepDIR_o = direction;

	    for (int i = 0; i < 79; i++) begin
	       // let's pulse motor
	       mc_ix.StepOutP_o = 1;
	       #10us;
	       mc_ix.StepOutP_o = 0;
	       #12us;
	    end
	    // reaching SWin or out depending of direction
	    i_WbMasterSim.WbRead(7, ReadData);
	    `CHECK_EQUAL(ReadData, mult*79, "Actual motor position fail");
	    // read status for switch info
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[direction], 0, "Switch should be still off");
	    `CHECK_EQUAL(mc_ox.RawSwitches_b2[direction], 0, "HW Switch iface issue");
	    // next cycle we should get detection
	    // let's pulse motor
	    mc_ix.StepOutP_o = 1;
	    #10us;
	    mc_ix.StepOutP_o = 0;
	    #12us;
	    i_WbMasterSim.WbRead(7, ReadData);
	    `CHECK_EQUAL(ReadData, mult*80, "Actual motor position fail");
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[direction], 1, "Switch did not react");
	    `CHECK_EQUAL(mc_ox.RawSwitches_b2[direction], 1, "HW Switch iface issue");

	    // switch should stay
	    mc_ix.StepOutP_o = 1;
	    #10us;
	    mc_ix.StepOutP_o = 0;
	    #12us;
	    i_WbMasterSim.WbRead(7, ReadData);
	    `CHECK_EQUAL(ReadData, mult*81, "Actual motor position fail");
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[direction], 1, "Switch should stay on");
	    `CHECK_EQUAL(mc_ox.RawSwitches_b2[direction], 1, "HW Switch iface issue");
	    // reset
	    i_WbMasterSim.WbWrite(3, 4);
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[direction], 0, "Switch should stay off");
	    `CHECK_EQUAL(mc_ox.RawSwitches_b2[direction], 0, "HW Switch iface issue");

	 end // for (int direction = 0; direction < 2; direction++)
      end

      `TEST_CASE("rail_to_rail_behaviour") begin
	 // we exert the motor to both rails and observe functionality
	 // of stepfail. Once the motor reaches fail value, it should
	 // stick there and not move until one does reset
	 for (int direction = 0; direction < 2; direction++) begin
	    if (direction == 0)
	      mult = 1;
	    else
	      mult = -1;

	    // motor movement. Let's move motor positive and negative
	    // setup motor max and masks
	    SetupMotor(100, -80, 80);
	    // enable motor stepping
	    mc_ix.StepDeactivate_o = 0;
	    // direction = 0 -> increase counter until hitting SWOUT
	    mc_ix.StepDIR_o = direction;

	    for (int i = 0; i < 99; i++) begin
	       // let's pulse motor
	       mc_ix.StepOutP_o = 1;
	       #10us;
	       mc_ix.StepOutP_o = 0;
	       #12us;
	    end
	    // reaching failure
	    i_WbMasterSim.WbRead(7, ReadData);
	    `CHECK_EQUAL(ReadData, mult*99, "Actual motor position fail");
	    // read status fail bit
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[3], 0, "Stepper should be still OK");
	    // next cycle, and it should fail!
	    // let's pulse motor
	    mc_ix.StepOutP_o = 1;
	    #10us;
	    mc_ix.StepOutP_o = 0;
	    #12us;
	    i_WbMasterSim.WbRead(7, ReadData);
	    `CHECK_EQUAL(ReadData, mult*100, "Actual motor position fail");
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[3], 1, "Stepper should indicate failure");

	    // and we make another step -> counter should not change and
	    // fail should stay
	    mc_ix.StepOutP_o = 1;
	    #10us;
	    mc_ix.StepOutP_o = 0;
	    #12us;
	    i_WbMasterSim.WbRead(7, ReadData);
	    `CHECK_EQUAL(ReadData, mult*100, "Actual motor position fail");
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[3], 1, "Stepper should indicate failure");

	    // and with reset we should come to the state. Note that if
	    // once fail happens, the motor will fail until one resets
	    // it. Reset:
	    i_WbMasterSim.WbWrite(3, 4);

	    i_WbMasterSim.WbRead(7, ReadData);
	    `CHECK_EQUAL(ReadData, 0, "Actual motor position fail");
	    i_WbMasterSim.WbRead(6, ReadData);
	    `CHECK_EQUAL(ReadData[3], 0, "Stepper should indicate OK");
	    ##100;
	 end // for (int direction = 0; direction < 2; direction++)
      end

      `TEST_CASE("simple_stepping") begin
	 // motor movement. Let's move motor positive and negative
	 // setup motor max and masks
	 SetupMotor(100, 20, 80);
	 // enable motor stepping
	 mc_ix.StepDeactivate_o = 0;
	 // direction = 0 -> increase counter until hitting SWOUT
	 mc_ix.StepDIR_o = 0;
	 ##100;
	 i_WbMasterSim.WbRead(7, ReadData);
	 `CHECK_EQUAL(ReadData, 0, "Actual motor position fail");
	 // let's pulse motor
	 mc_ix.StepOutP_o = 1;
	 #100us;
	 mc_ix.StepOutP_o = 0;
	 ##10;
	 // pointer should not move as enable is not enabled
	 i_WbMasterSim.WbRead(7, ReadData);
	 `CHECK_EQUAL(ReadData, 1, "Actual motor position fail");

	 // and yet another pulse to negative
	 // let's pulse motor
	 mc_ix.StepDIR_o =  1;
	 mc_ix.StepOutP_o = 1;
	 #100us;
	 mc_ix.StepOutP_o = 0;
	 ##10;
	 // pointer should not move as enable is not enabled
	 i_WbMasterSim.WbRead(7, ReadData);
	 `CHECK_EQUAL(ReadData, 0, "Actual motor position fail");
      end

      `TEST_CASE("disabled_move_check") begin
	 // motor movement. Let's move motor positive and negative
	 // setup motor max and masks
	 SetupMotor(100, 20, 80);
	 ##100;
	 i_WbMasterSim.WbRead(7, ReadData);
	 `CHECK_EQUAL(ReadData, 0, "Actual motor position fail");

	 // let's pulse motor
	 mc_ix.StepOutP_o = 1;
	 #100us;
	 mc_ix.StepOutP_o = 0;
	 ##10;
	 // pointer should not move as enable is not enabled
	 i_WbMasterSim.WbRead(7, ReadData);
	 `CHECK_EQUAL(ReadData, 0, "Actual motor position fail");
      end

      `TEST_CASE("motor_fail_due_to_injection") begin
	 // set mux
	 bix = 31;
	 // and verify fail functionality
	 TripFail(31);
      end // UNMATCHED !!

      `TEST_CASE("overheat_triggering") begin
	 bix = 30;
	 TripFail(30);
      end

      `TEST_CASE("center_motor_position") begin
	 // write random data into steps of motor and watch how center is setup
	 for(int i = 0; i < 100; i++) begin
	    rnd = $urandom_range(32'hffff_ffff, 0);

            i_WbMasterSim.WbWrite(0, rnd);
	    i_WbMasterSim.WbRead(0, ReadData);
            `CHECK_EQUAL(ReadData, rnd, "REGISTER RW FAILED");
	    i_WbMasterSim.WbRead(7, ReadData);
	    `CHECK_EQUAL(ReadData, 0,
			 "CENTRAL MOTOR POSITION FAIL");

	 end
      end

   end;

   // instantiate wishbone interface
   WbMasterSim
     i_WbMasterSim (// Outputs
		    .Rst_orq		(ClkRs_ix.reset),
		    .Adr_obq32		(Wb_iot.Adr_b),
		    .Dat_obq32		(Wb_iot.DatMoSi_b),
		    .We_oq		(Wb_iot.We),
		    .Cyc_oq		(Wb_iot.Cyc),
		    .Stb_oq		(Wb_iot.Stb),
		    // Inputs
		    .Clk_ik		(ClkRs_ix.clk),
		    .Dat_ib32		(Wb_iot.DatMiSo_b),
		    .Ack_i		(Wb_iot.Ack));

   assign Wb_iot.Sel_b = 4'hf;
   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(1000000ms);

   motor_emulator #(/*AUTOINSTPARAM*/
		    // Parameters
		    .g_NumberOfRegisters(g_NumberOfRegisters),
		    .g_DataWidth	(g_DataWidth),
		    .g_OrMask		(g_OrMask)) DUT
     (.*);

   // instantiate generator of random numbers
   logic [g_DataWidth-1:0] RndNumber_b;
   fibonacci_lfsr_nbit #(.BITS(g_DataWidth))
   i_fibonacci_lfsr_Fail (
			  // Outputs
			  .data_ob		(RndNumber_b),
			  // Inputs
			  .ClkRs_ix		(ClkRs_ix));
   // cast signals to the data stream:
   assign Random_iot.data = RndNumber_b,
     Random_iot.enable = 1;


endmodule
