//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_SWITCHCONTROLLER.SV
// @brief
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 13 October 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;
import MCPkg::*;

//
module tb_switchcontroller;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic [1:0]		ConfiguredSwitches_b2;		// From DUT of switchcontroller.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   ckrs_t		ClkRs_ix;		// To DUT of switchcontroller.v
   cntparam_t		CntParam_ix;	// To DUT of switchcontroller.v
   logic [1:0]		RawSwitches_b2;		// To DUT of switchcontroller.v
   // End of automatics

   int 					      tim, pulses, cx;
   int 					      test;


   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   task burst(input int switchnumber);
      assert(std::randomize(pulses) with {pulses > 1; pulses < 100;});
      for(cx=0; cx < pulses; cx++) begin
	 RawSwitches_b2[switchnumber] = 1;
	 assert(std::randomize(tim) with {tim > 1; tim < 16;});
	 ##(tim);
	 RawSwitches_b2[switchnumber] = 0;
	 assert(std::randomize(tim) with {tim > 1; tim < 16;});
	 ##(tim);
      end
   endtask // burst



   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 test = 0;
	 // no inversion and do not connect any switch to output matrix
	 for (int i = 0; i < 2; i++) begin
	   CntParam_ix.ExtremitySwitches_b2[i].Polarity = NORMAL;
	   CntParam_ix.ExtremitySwitches_b2[i].SelectedInputSwitches_b2 = '0;
	 end;
	 // we setup both signals from the beginning to zero, that's
	 // because there are asserts in debouncer, which would trip
	 // when signals change in the reset. State machine basically
	 // does not work so counting does not work either
	 RawSwitches_b2 = '0;
	 ##1;
	 ClkRs_ix.reset = 1;
	 ##10;
	 ClkRs_ix.reset = 0;
	 ##20;
	 test = 1;
      end

      `TEST_CASE("debouncing") begin
	 // start with stable state, and set both raw switches to
	 // 'normally open', hence '1' at both inputs
	 RawSwitches_b2 = '1;

	 // set the inputs and polarity such, that logical switches
	 // will result in zero (inversion). Invert the switches - so
	 // raw0 will appear on logic1 and vice versa!
	 CntParam_ix.ExtremitySwitches_b2[0].Polarity = INVERT;
	 CntParam_ix.ExtremitySwitches_b2[0].SelectedInputSwitches_b2 = 2'b10;
	 CntParam_ix.ExtremitySwitches_b2[1].Polarity = INVERT;
	 CntParam_ix.ExtremitySwitches_b2[1].SelectedInputSwitches_b2 = 2'b01;
	 test = 2;

	 #10us;
	 `CHECK_EQUAL(ConfiguredSwitches_b2, '0);
	 test = 3;
	 for(int c = 0; c < 50; c++) begin
	    // let's generate glitch on switch 0, result should appear on
	    // switch1
	    burst(0);
	    RawSwitches_b2[0] = 0;
	    #10us;
	    `CHECK_EQUAL(ConfiguredSwitches_b2[1], 1);
	    `CHECK_EQUAL(ConfiguredSwitches_b2[0], 0);
	    test = 4;
	    burst(0);
	    RawSwitches_b2[0] = 1;
	    #10us;
	    `CHECK_EQUAL(ConfiguredSwitches_b2[1], 0);
	    `CHECK_EQUAL(ConfiguredSwitches_b2[0], 0);
	 end // for (int c = 0; c < 50; c++)
      end // UNMATCHED !!

      `TEST_CASE("switching_and_inversion_of_signals") begin
	 // setup no inversion and switches mapping 1:1 ([0] = [0] and
	 // [1] goes to [1])
	 CntParam_ix.ExtremitySwitches_b2[0].Polarity = NORMAL;
	 CntParam_ix.ExtremitySwitches_b2[0].SelectedInputSwitches_b2 = 2'b01;
	 CntParam_ix.ExtremitySwitches_b2[1].Polarity = NORMAL;
	 CntParam_ix.ExtremitySwitches_b2[1].SelectedInputSwitches_b2 = 2'b10;

	 // check that with this polarity the switches are turned off
	 // as switches signals are zero by reset:
	 ##30;
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b00);
	 ##10;
	 // turn on first switch, wait for debouncing:
	 RawSwitches_b2 = 2'b01;
	 ##10;
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b00);
	 ##30;
	 // should be debounced by now, so signal propagates to the output
	 `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b01);

	 // let's setup one by one switches to another switch,
	 CntParam_ix.ExtremitySwitches_b2[0].SelectedInputSwitches_b2 = 2'b10;
	 ##1;
	 // both switches are now configured to look on rawswitch1,
	 // which is at zero, hence:
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b00);
	 ##1;
	 // now select logical switch1 to look on raw switch0:
	 CntParam_ix.ExtremitySwitches_b2[1].SelectedInputSwitches_b2 = 2'b01;
	 ##1;
	 // and now logical switches have to be 2'b10 because log1
	 // looks on raw0 and log0 looks on raw1, but only raw0 is
	 // turned on:
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b10);
	 // invert polarity of one of them:
	 ##1;
	 CntParam_ix.ExtremitySwitches_b2[0].Polarity = INVERT;
	 ##1;
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b11);

      end

      `TEST_CASE("react_on_any_change") begin
	 // both switches do not invert and point to both raw
	 // switches. This means that both logical switches should
	 // react on both raw switches
	 RawSwitches_b2 = '0;
	 CntParam_ix.ExtremitySwitches_b2[0].Polarity = NORMAL;
	 CntParam_ix.ExtremitySwitches_b2[0].SelectedInputSwitches_b2 = 2'b11;
	 CntParam_ix.ExtremitySwitches_b2[1].Polarity = NORMAL;
	 CntParam_ix.ExtremitySwitches_b2[1].SelectedInputSwitches_b2 = 2'b11;

	 ##40;
	 `CHECK_EQUAL(ConfiguredSwitches_b2, '0);
	 RawSwitches_b2 = 2'b01;
	 ##40;
	 `CHECK_EQUAL(ConfiguredSwitches_b2, '1);
	 RawSwitches_b2 = '0;
	 ##40;
	 `CHECK_EQUAL(ConfiguredSwitches_b2, '0);
	 RawSwitches_b2 = 2'b10;
	 ##40;
	 `CHECK_EQUAL(ConfiguredSwitches_b2, '1);
	 RawSwitches_b2 = 2'b11;
	 ##40;
	 `CHECK_EQUAL(ConfiguredSwitches_b2, '1);

      end // UNMATCHED !!


      `TEST_CASE("from_reset_all_switches_in_zero_position") begin
	 // we're testing whether if reset happens, NO SWITCHES SHOULD
	 // react, and all of them have to return permanently
	 // zero. This is bit dangerous because it means that motor
	 // will not stop at all, but there is no other way how to do
	 // it because in theory switches might be of NO an NC types,
	 // so difficult to set it up such that switches will
	 // interlock until they are configured.... (unless I do some
	 // specific action maybe later?)
	 ##30;
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b00);
	 ##10;
	 RawSwitches_b2 = 2'b01;
	 ##10;
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b00);
	 ##30;
	 // both output switches are routed to 'no switch', hence
	 // still display zeros...
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b00);
      end
   end;


   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(20s);

   switchcontroller DUT
     (/*AUTOINST*/
      // Outputs
      .ConfiguredSwitches_b2			(ConfiguredSwitches_b2[1:0]),
      // Inputs
      .CntParam_ix		(CntParam_ix),
      .ClkRs_ix				(ClkRs_ix),
      .RawSwitches_b2			(RawSwitches_b2[1:0]));

endmodule
