//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) January 2018 CERN

//-----------------------------------------------------------------------------
// @file TB_IRQ_EMULATOR.SV
// @brief Verifies functionality of IRQ emulator
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 10 January 2018
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

// Verifies functionality of IRQ emulator
module tb_irq_emulator;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic [23:0]		CurrentIRQ_ob24;	// From DUT of irq_emulator.v
   logic [23:0]		IRQEmulator_ob24;	// From DUT of irq_emulator.v
   // End of automatics
   /*AUTOREGINPUT*/
   localparam		BITSWIDTH = 5;


   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 ClkRs_ix.reset = 1;
	 ##10;
	 ClkRs_ix.reset = 0;
      end

      `TEST_CASE("debug_test") begin

	 for(int i = 0; i < 23; i++) begin
	    while (IRQEmulator_ob24 == 0)
	      ##1;
	    $display("Emulation jump: ", IRQEmulator_ob24);
	    `CHECK_EQUAL(IRQEmulator_ob24, 2**i, "BARREL SHIFTING ERROR");
	    `CHECK_EQUAL(CurrentIRQ_ob24[i], 1, "CURRENT IRQ does not hold");
	    ##1;

	 end
	 // wait for two more:
	 while(IRQEmulator_ob24 == 0)
	   ##1;
	 `CHECK_EQUAL(IRQEmulator_ob24, 2**23, "BARREL SHIFTING ERROR");
	 ##1;

	 while(IRQEmulator_ob24 == 0)
	   ##1;
	 `CHECK_EQUAL(IRQEmulator_ob24, 2**0, "BARREL SHIFTING ERROR");
	 #100;

      end

   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(100ms);

   irq_emulator #(/*AUTOINSTPARAM*/
		  // Parameters
		  .BITSWIDTH		(BITSWIDTH)) DUT
     (.*);

endmodule
