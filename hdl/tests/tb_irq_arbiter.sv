//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) January 2018 CERN

//-----------------------------------------------------------------------------
// @file TB_IRQ_ARBITER.SV
// @brief Functionality of IRQ arbiter
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 19 January 2018
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

// Functionality of IRQ arbiter
module tb_irq_arbiter;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   localparam NUM_OF_INPUTS = 4;
   localparam FIFO_DEPTH = 4;

   localparam VECTOR_SIZE = 16+4;

   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};

   logic [NUM_OF_INPUTS-1:0] Overflow_ob;	// From DUT of irq_arbiter.v
   logic [VECTOR_SIZE-1:0]   data_ob;		// From DUT of irq_arbiter.v
   logic [$clog2(NUM_OF_INPUTS)-1:0] link_ob;	// From DUT of irq_arbiter.v
   logic 			     ResetOverflow_i;	// To DUT of irq_arbiter.v
   logic [VECTOR_SIZE-1:0] 	     data_ib [NUM_OF_INPUTS-1:0];// To
				// DUT of irq_arbiter.v
   logic [NUM_OF_INPUTS-1:0] 	     FifoFull_ob;


   /*AUTOWIRE*/
   /*AUTOREGINPUT*/

   // transaction holder
class QueuedData;
   // this is set upstream
   logic [7:0] 			     series;

   // we get random value for the tranmitted data
   rand logic [7:0] value;
   // and we want to randomize the link value as well
   rand int link;
   constraint link_value {link >= 0; link < NUM_OF_INPUTS;};

   function new(int series);
      this.series = series;
   endfunction // new

   function logic compare(input logic [VECTOR_SIZE-1:0] datain,
			  logic [$clog2(NUM_OF_INPUTS)-1:0] linkin );
      if (datain == {series,  value} &&
	  linkin == link) return 1;
      return 0;
   endfunction // compare

   function string sprint();
      return $sformatf("QueuedData: Series: %.2x: link: %d, value %.4X",
		       series, link, value);
   endfunction // sprint

endclass // QueuedData

   function void displayQueue(input QueuedData queue[]);
      $display("QUEUE CONTENT:");
      foreach(queue[i])
	$display("\t%s", queue[i].sprint());

   endfunction // displayQueue

   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   int 							    c[$] = {5,  6, 8, 10};
   QueuedData 			   veriQueue[$];
   semaphore 						    lock;
   logic [15:0] 					    testVal;

   QueuedData data;
   int 							    retrieved[$], isiface[$];
   int 							    ifaces[];



   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 ResetOverflow_i = 0;
	 data_ib = '{NUM_OF_INPUTS{(VECTOR_SIZE)'(0)}};
	 ClkRs_ix.reset = 1;
	 ##10;
	 ClkRs_ix.reset = 0;
      end

      `TEST_CASE("debug_test") begin
	 fork
	    // fancy randomization here: we generate random
	    begin
	       lock = new(1);
	       ##10;
	       for(int i=0; i <256; i++) begin
		  // any of our attemps to push samples here results
		  // in overflow -> something fishy
		  `CHECK_EQUAL(Overflow_ob, 0, "OVERFLOW");

		  // first generate how many ifaces we want IRQ to
		  // occur
		  assert(std::randomize(ifaces) with {
		     ifaces.size inside {[1:NUM_OF_INPUTS]};
		     ifaces.size dist {1 := 80, 2 := 20, 3 := 10, 4 != 5};
		     foreach (ifaces[i]) ifaces[i] inside {[0:NUM_OF_INPUTS-1]};
		     foreach (ifaces[i]) foreach (ifaces[j])
			if (i != j) ifaces[i] != ifaces[j];

		  });
		  $display("_________________%p_______________________", ifaces);

		  $display("Generating IRQs on following links: ", ifaces);

		  foreach (ifaces[j]) begin
		     // now we have to iterate through interfaces and
		     // setup the input vectors _and_ put all generated
		     // signals into queue, those are the _driven_ signals
		     // drive data bus if this is the case
		     lock.get();
		     // each time we pull out new reference (otherwise we
		     // would push back into queue always the same ref)
		     data = new(i);
		     // randomize data
		     if (!data.randomize())
		       $error("Randomizator failed");
		     if (data.value != 0 && !FifoFull_ob[ifaces[j]]) begin
			// we have to replace link value (because that
			// one is used as random in different test
			data.link = ifaces[j];
			testVal = {8'(i), data.value};
			// throttling here: we only issue another fifo
			// command when FIFO is not full!
			$display("ITER %d scheduling irq on link %d \
with IRQ status %x", i, data.link, testVal);
			// setup one link and all the others will be
			// zeroed. Valid IRQs are pushed into QUEUE,
			// invalids or zeros are discarded. This allows read
			// task to do the magic with testing
			data_ib[ifaces[j]] = testVal;
			veriQueue.push_back(data);
		     end else begin
			  $display("No operation");
			data_ib[ifaces[j]] = 0;
		     end // else: !if(data.value != 0 &&
		     // !FifoFull_ob[ifaces[j]])
		     //displayQueue(veriQueue);
		     lock.put();
		  end // foreach (ifaces[j])

		  // and now we have to clear out all the other busses
		  // otherwise they will generate IRQ due to previous
		  // items
		  for(int lk=0; lk < NUM_OF_INPUTS; lk++) begin
		     isiface = ifaces.find_first_index(x)
		       with (x==lk);
		     if (isiface.size)
		       continue;
		     data_ib[lk] = 0;
		  end
		  ##1;
	       end // for (int i=0; i <256; i++)
	       // let irqs be processed
	       // we setup all IRQ sources to 0 so after certain time
	       // all should clear out (and if not, means FIFO gets crazy)
	       data_ib = '{NUM_OF_INPUTS{0}};
	       // wait for fifo clear out
	       while(DUT.FifoEmpty_b != '1)
		 ##1;

	       `CHECK_EQUAL(data_ob, 0, "Immediatelly when IRQs run away, we should get zero");
	    end
	    begin
	       // wait for initial synchronization
	       // we don't know in which order data arrive
	       forever begin
		  //$display("----------------------------------------");
		  if (data_ob) begin
		     lock.get();
		     //$display("Looking for link %x IRQ %x in the \
//queue:", link_ob, data_ob);
		     //displayQueue(veriQueue);

		    // nonzero data at the output, they _must be_ in
		    // the queue, we can delete it
		     // class comparison does not work, we need to
		     // compare items
		     foreach (veriQueue[i]) begin
			if (veriQueue[i].compare(data_ob, link_ob))
			  begin
			     //$display("Found:");
			     //$display("\t%s", veriQueue[i].sprint());

			     // this item has to be deleted from the
			     // queue as match was found with one of
			     // the data in.
			     retrieved = veriQueue.find_first_index(x)
			       with ( x == veriQueue[i] );
			     if (retrieved.size)
			       veriQueue.delete(int'(retrieved));
			     else
			       // THIS IS ERROR - we did not find the item in
			       // the queue!
			       `CHECK_EQUAL(0, 1, "ITEM NOT FOUND IN \
QUEUE!");
			     //displayQueue(veriQueue);
			  end
		     end
		     lock.put();
		  end // if (data_ob)
		  ##1;
	       end // forever begin
	    end
	 join_any
	 while(!(&DUT.FifoEmpty_b))
	   ##100;
	 lock.get();
	 displayQueue(veriQueue);
	 `CHECK_EQUAL(veriQueue.size, 0, "QUEUE NOT CLEARED WITHIN A\
 TIMEOUT");
	 lock.put();
      end

      `TEST_CASE("randomized_single_link") begin
	 fork
	    // fancy randomization here: we generate random
	    begin
	       lock = new(1);
	       ##10;
	       for(int i=0; i <256; i++) begin
		  // each time we pull out new reference (otherwise we
		  // would push back into queue always the same ref)
		  data = new(i);
		  if (!data.randomize())
		    $error("Randomizator failed");

		  testVal = {8'(i), data.value};

		  if (data.value)
		    $display("Scheduling irq: link: %d, irq %x", data.link, testVal);
		  else
		    $display("No operation");

		  // setup one link and all the others will be
		  // zeroed. Valid IRQs are pushed into QUEUE,
		  // invalids or zeros are discarded. This allows read
		  // task to do the magic with testing. We need to
		  // throttle the data here as FIFO becomes pretty
		  // quickly full with this data pace, hence if the
		  // FIFO overrun is setup, we just do not fire the
		  // data channel any more. We do not however check
		  // here for overrun flag (as this one once set it
		  // does not unset), but we look for fifo full
		  // signals in the DUT
		  for(int lk=0; lk<NUM_OF_INPUTS;lk++)
		    if ((lk==data.link) && (data.value != 0)) begin
		       data_ib[lk] = testVal;
		       lock.get();
		       veriQueue.push_back(data);
		       lock.put();
		    end
		    else
		      data_ib[lk] = 0;
		  ##1;
	       end // for (int i=0; i <256; i++)
	       // let irqs be processed
	       // we setup all IRQ sources to 0 so after certain time
	       // all should clear out (and if not, means FIFO gets crazy)
	       data_ib = '{NUM_OF_INPUTS{0}};
	       ##50;

	       `CHECK_EQUAL(data_ob, 0, "Immediatelly when IRQs run away, we should get zero");
	    end
	    begin
	       // wait for initial synchronization
	       // we don't know in which order data arrive
	       forever begin
		  if (data_ob) begin
		     $display("Looking for %x in %p", data_ob, testVal);
		    // nonzero data at the output, they _must be_ in
		    // the queue, we can delete it
		     lock.get();
		     // class comparison does not work, we need to
		     // compare items
		     foreach (veriQueue[i]) begin
			$display(veriQueue[i].sprint());
			if (veriQueue[i].compare(data_ob, link_ob))
			  begin
			     // this item has to be deleted from the
			     // queue as match was found with one of
			     // the data in.
			     retrieved = veriQueue.find_first_index(x)
			       with ( x == veriQueue[i] );
			     if (retrieved.size)
			       veriQueue.delete(int'(retrieved));
			     else
			       // THIS IS ERROR - we did not find the item in
			       // the queue!
			       `CHECK_EQUAL(0, 1, "ITEM NOT FOUND IN QUEUE!");
			  end
		     end
		     lock.put();
		  end // if (data_ob)
		  ##1;
	       end // forever begin
	    end
	 join_any
	 ##100;
	 lock.get();
	 `CHECK_EQUAL(veriQueue.size, 0, "QUEUE NOT CLEARED WITHIN A\
 TIMEOUT");
	 lock.put();
      end

      `TEST_CASE("single_multichannel") begin
	 fork
	    begin : stimulusx
	       ##10;
	       data_ib[2] <= 5;
	       ##1;
	       data_ib[2] <= 0;
	       data_ib[1] <= 6;
	       ##1;
	       data_ib[1] <= 0;
	       data_ib[3] <= 8;
	       ##1;
	       data_ib[3] <= 0;
	       data_ib[0] <= 10;
	       ##1;
	       data_ib[0] <= 0;
	       ##100;

	    end // block: stimulus
	    begin : watcherx
	       automatic int result[$];

	       // wait for initial synchronization
	       // we don't know in which order data arrive

	       for (int i=0; i < 1000; i++) begin
		  result = c.find_first_index(x) with (x==data_ob);
	    	  if (result.size)
	    	    break;
	    	  ##1;
	       end
	       $display("Queue size: %d", c.size);

	       for (int i=0; i < 4; i++) begin
		  $display("Dequeueing %d", data_ob);
		  c.delete(int'(c.find_first_index( x ) with ( x == data_ob )));
		  ##1;
	       end
	       `CHECK_EQUAL(data_ob, 0, "Immediatelly when IRQs run away, we should get zero");
	       `CHECK_EQUAL(c.size, 0, "IRQ sources not completely cleared");

	    end // block: watcher
	 join

      end

      `TEST_CASE("multiple_stimulus_single_channel") begin
	 fork
	    begin : stimulus
	       ##10;
	       data_ib[3] <= 5;
	       ##1;
	       data_ib[3] <= 6;
	       ##1;
	       data_ib[3] <= 8;
	       ##1;
	       data_ib[3] <= 10;
	       ##1;
	       data_ib[3] <= 0;
	    end // block: stimulus
	    begin : watcher
	       // wait for initial synchronization
	       for (int i=0; i < 1000; i++) begin
		  if (data_ob == 5)
		    break;
		  ##1;
	       end
               `CHECK_EQUAL(data_ob, 5);
	       ##1;
               `CHECK_EQUAL(data_ob, 6);
	       ##1;
               `CHECK_EQUAL(data_ob, 8);
	       ##1;
               `CHECK_EQUAL(data_ob, 10);
	    end // block: watcher
	 join

      end

      `TEST_CASE("single_irq") begin
	 ##10;
	 data_ib[3] <= 10;
	 ##1;
	 data_ib[3] <= 0;
	 for (int i=0; i < 1000; i++) begin
	    if (data_ob == 10)
	      break;
	    ##1;
	 end
         `CHECK_EQUAL(data_ob, 10);
      end

   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(1000ms);

   irq_arbiter #(/*AUTOINSTPARAM*/
		 // Parameters
		 .NUM_OF_INPUTS		(NUM_OF_INPUTS),
		 .FIFO_DEPTH		(FIFO_DEPTH),
		 .VECTOR_SIZE		(VECTOR_SIZE)) DUT
     (.*);

endmodule
