package uvm_style_test;
   parameter AHOST = 32'h00000044;
   let AHOST_MSB(field) = field[31:16];
   let AHOST_LSB(field) = field[15:0];
   let LSB(field) = field[0];
   let MSB4(field) = field[31:28];
endpackage // VmeAddressMap
