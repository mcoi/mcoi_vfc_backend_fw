//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_MCLINK.SV
// @brief testbench for motor controller optical link
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 25 October 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;
import MCPkg::*;
import VmeAddressMap::*;

`define waitCheck(clk, sig, chk, timeout, errcond)\
for(x=0; x < timeout; x++) begin\
   @(posedge clk);\
     if(sig == chk) break;\
       end\
assert(x<timeout-1) else $error(errcond)


  // testbench for motor controller optical link
  module tb_mclink;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 8; // clock period in ns
   localparam g_DataWidth = 32;
   localparam g_FrameSize = 80;
   // length of the command queue
   localparam   QUEUELENGTH  = 32;

   // debouncing counter with for the input switches
   localparam g_DebouncingCounterWidth = 4;

   // how many flip-flops will be used in the input signal
   // synchronization chain to avoid metastability
   localparam g_SynchronizationFlips = 3;

   // random mclink
   localparam g_mclinkInterfaceNumber = 32'haabbeeff;

   // we intentionally put address offset somewhere into higher spheres of Wb
   // addressing to check if all works as supposed
   localparam g_AddressSpaceOffset = $pow(2, 18);

   logic [7:0] 	      testing_b8;
   logic [31:0]       retval_b32, maskp, maskn;

   integer 	      i;


   initial begin
      $display("MCLINK_WB_ADDRESS_WIDTH: %d, hence one motor controller occupies\
      address space of 0x%h (%d) elements",
	       MCLINK_WB_ADDRESS_WIDTH,
	       $pow(2, MCLINK_WB_ADDRESS_WIDTH),
	       $pow(2, MCLINK_WB_ADDRESS_WIDTH));
      $display("Using g_AddressSpaceOffset: %h", g_AddressSpaceOffset);

   end

   int loop;


   localparam g_AddressWidth = 25;


   ckrs_t ClkRs_ix, rClkRs_ix, wClkRs_ix;

   t_WbInterface #(.g_DataWidth(g_DataWidth),
		   .g_AddressWidth(g_AddressWidth)) Wb_iot(ClkRs_ix.clk, ClkRs_ix.reset);
   logic [g_DataWidth-1:0] ReadData;

   triggers_t	triggers;		// From DUT of mclink.v
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic		Busy_o;			// From DUT of mclink.v
   logic [NUMBER_OF_MOTORS_PER_FIBER-1:0] IRQ_ob;// From DUT of mclink.v
   // End of automatics
   /*AUTOREGINPUT*/

   // NOTE: THIS ENTITY REQUIRES, THAT ALL THE INPUT PACKET SIGNALS ARE
   // SYNCHRONIZED TO CLKRS DOMAIN! OUTPUT PACKET SIGNALS ARE DRIVEN
   // BY GBTX CLOCK (that's why wClkRs_ix is set to 25ns)
   // wisbone clock
   always forever #(25ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   // TX GBT clocks
   always forever #(25ns) wClkRs_ix.clk <= ~wClkRs_ix.clk;
   // RX GBT clock -
   always forever #(25ns) rClkRs_ix.clk <= ~rClkRs_ix.clk;


   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   // this is to mimic the behaviour of packet
   mcinput_t [NUMBER_OF_MOTORS_PER_FIBER-1:0] mcpacket_i;
   logic [g_FrameSize-1:0] packet_i;
   logic [7:0] 		   cntr;
   int 			   motorpos, motorneg;



   // hand assigned mcpacket_i into 80bit input packet so we can easily
   // manipulate it using mcpacket_i in the testbenches.
   assign packet_ix.data = {mcpacket_i, ($size(packet_i)-
				   ($size(mcpacket_i)*$size(mcinput_t)))'(0)};


   // assign mcoutput from optical interface such, that we can easily test
   mcoutput_t [NUMBER_OF_MOTORS_PER_FIBER-1:0] mcpacket_o;
   // magic decomposition of optical stream (to be sent through GEFE to motor
   // controller) to mcpacket_o structure, which we can test in easy way using
   // unit testbenching
   assign mcpacket_o = packet_ox.data[$left(packet_ox.data):
   				$left(packet_ox.data) -
   				$size(mcpacket_o)*$size(mcinput_t)
				+ 1];

   int 			   basereg;
   int 			   plength;
   int 			   x;
   logic [0:NUMBER_OF_MOTORS_PER_FIBER-1] MotorBusy_b;



   // function recalculating motor control unit number into physical address
   function [g_AddressWidth-1:0] getBaseAddr (input int motor);
      getBaseAddr = g_AddressSpaceOffset + (motor * 2 * NUMBER_OF_MOTORS_PER_FIBER);
   endfunction // getBaseAddr


   // make counter which counts pulses going out of stream. This one is
   // non-resettable, hence expected that each unit test fires only single
   // command per channel.
   logic [NUMBER_OF_MOTORS_PER_FIBER-1:0][31:0] 	   countPulses = '0;
   genvar 		   cc;
   generate
      for (cc=0; cc<NUMBER_OF_MOTORS_PER_FIBER; cc++) begin: potato
	 always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset)
	   begin
	      if (ClkRs_ix.reset)
		countPulses[cc] = 0;
	      else if ($fell(mcpacket_o[cc].StepOutP_o))
		countPulses[cc]++;
	   end
	 end
   endgenerate

   // simple counter of total IRQ pulses
   logic [31:0] 	   irqPulses = '0;
   always_ff @(posedge ClkRs_ix.clk) begin
      if ($rose(|IRQ_ob))
	irqPulses++;
   end

   task Write( input int motor, register,
	      input logic [31:0] Data_ib32,
	      input logic 	 display = '0);
      basereg = getBaseAddr(motor) + register;
      i_WbMasterSim.WbWrite(basereg, Data_ib32);
      if (display)
	$display("Writing data: %h -> %h", basereg, Data_ib32);
   endtask // Write

   task Read(input int motor, register,
	     output logic [31:0] data,
	     input logic 	 display='1);
      begin
	 basereg = getBaseAddr(motor) + register;
	 i_WbMasterSim.WbRead(basereg, data);
	 if (display)
	   $display("Reading data: %h -> %h", basereg, data);
      end
   endtask // Read

   task CheckBusy(input logic [31:0] motor);
      // now, those interlock switches are _not asserted_ hence any
      // attempt to run the motor should fail:
      fork
	 // this one starts motor defined by motor
	 begin
	    Write(motor, MTRG, 1);
	    #100ms;
	 end
	 // this one is timeout
	 #500us;
	 // this one is catching busy flag which should kill the
	 // stuff with error, as motor should not run
	 @(posedge Busy_o);
      join_any
      `CHECK_EQUAL(Busy_o, 0);
   endtask; // CheckBusy


   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 packet_ix.enable = 1;
	 triggers.TrigMoveInt_i = 0;
	 triggers.TrigMoveExt_i = 0;
	 triggers.TriggerEnable_i = 1;
	 testing_b8 = 0;

	 // clearout packet from GEFE
	 mcpacket_i = '0;

         i_WbMasterSim.Reset();
	 // and reset as well the other two domains (otherwise FIFOs will bail
	 // out)
	 wClkRs_ix.reset = '1;
	 rClkRs_ix.reset = '1;

	 #(200ns);
	 @(posedge wClkRs_ix.clk);
	 wClkRs_ix.reset = '0;
	 rClkRs_ix.reset = '0;
	 `CHECK_EQUAL(Busy_o, 0, "AFTER RESET, BUSY MUST BE DEASSERTED");

      end

      `TEST_CASE("debug_test") begin
	 ##10;

      end

      `TEST_CASE("switches_configuration_to_mclink") begin
	 // checks whether if we setup a configuration of a switch for
	 // a given motor, if it propagates to switches config inside
	 // MClink. this is important as this information then goes
	 // through fiber
	 for (int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	    assert(std::randomize(ReadData));
	    Write(i, MSWITCHESCONFIG, ReadData);
	    ##100;
	    `CHECK_EQUAL(DUT.SwitchesConfiguration_2o16[i],
			 ReadData[2*$bits(switchstate_t)-1:0]);
	 end
      end // UNMATCHED !!


      `TEST_CASE("interlock_data_propagation_from_other_motors") begin
	 // setup all the motors switches such, that none of them is
	 // engaged, then setup one motor to interlock on another
	 // motors' positive and negative switches and try to run
	 // it...
	 for (int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	    Write(i, MSWITCHESCONFIG, 32'b0_10_0_01);
	    ##100;
	    // with this setting all the logical switches have to be
	    // turned off
	    Read(i, MMSTATUS, ReadData);
	    `CHECK_EQUAL(MRSW(ReadData), '0);
	 end
	 // select random motor and program it to do the stuff
	 assert(std::randomize(retval_b32) with {retval_b32 >= 0;
	    retval_b32 < NUMBER_OF_MOTORS_PER_FIBER;});
	 // program the motor
	 Write(retval_b32, MSTEPS, 20);
	 // direction is only bit[0], we cast there the number and check
	 Write(retval_b32, MDIRECTION, 1);
	 Write(retval_b32, MLOWSPEED, 50);
	 Write(retval_b32, MHIGHSPEED, 2000);
	 Write(retval_b32, MACCRATE, 2000);
	 Write(retval_b32, MTRAIL, 10);
	 // powerconfig - enable move
	 Write(retval_b32, MPOWER, 1);
	 // get two other numbers, mutually exclusive and exclusive to
	 // the chosen motor as well, we will use them to program P
	 // and N interlock masks
	 assert(std::randomize(maskp) with
		{maskp >= 0;
	    maskp < NUMBER_OF_MOTORS_PER_FIBER;
	    maskp != retval_b32;});
	 assert(std::randomize(maskn) with
		{maskn >= 0;
	    maskn < NUMBER_OF_MOTORS_PER_FIBER;
	    maskn != retval_b32;
	    maskn != maskp;});
	 $display("Configuring motor %d with maskp %.8x and maskn\
  0x%8x", retval_b32, 2**maskp, 2**maskn);
	 Write(retval_b32, MILCKP, 32'(2**maskp));
	 Write(retval_b32, MILCKN, 32'(2**maskn));

	 CheckBusy(retval_b32);
	 // once we have this setup, we do other tests: first invert
	 // positive switchconfig of positive motor such that this
	 // ilock gets released. Change that config and check if it is engaged:
	 Write(maskp, MSWITCHESCONFIG, 32'b0_10_1_01);
	 Read(maskp, MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRSW(ReadData), 2'b01);

	 // now run the motor again, this should not yet work as
	 // positive switch only, but no negative is engaged at the
	 // other side:
	 CheckBusy(retval_b32);
	 // revert positive back to original setting, and do the same
	 // exercise with negative, still motor should not trigger
	 // because one of the options is not there:
	 Write(maskp, MSWITCHESCONFIG, 32'b0_10_0_01);
	 Read(maskp, MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRSW(ReadData), 2'b00);
	 Write(maskn, MSWITCHESCONFIG, 32'b1_10_0_01);
	 Read(maskn, MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRSW(ReadData), 2'b10);
	 CheckBusy(retval_b32);
	 // and now we finally reprogram both p and n switches of two
	 // different motors to be engaged and run motor again. This
	 // time is MUST run. Negative is already configured, do
	 // positive again:
	 Write(maskp, MSWITCHESCONFIG, 32'b0_10_1_01);
	 Read(maskp, MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRSW(ReadData), 2'b01);
	 // and start motor:
	 fork
	    // this one starts motor defined by motor
	    begin
	       Write(retval_b32, MTRG, 1);
	       #100ms;
	    end
	    // this one is timeout
	    #500us;
	    // this one is catching busy flag which should kill the
	    // stuff with error, as motor should not run
	    @(posedge Busy_o);
	 join_any
	 `CHECK_EQUAL(Busy_o, 1);
      end

      `TEST_CASE("busy_catch") begin
	 // turn on all the motors at different times, observe busy
	 // behaviour
	 `CHECK_EQUAL(Busy_o, 0, "BUSY ERROR");

	 for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin

	    // setup a single move, number of steps for each motor we keep
	    // constant as this will result in worst case fifo filling (as all
	    // the motors do the operation 'almost' at the same time)
	    Write(i, MSTEPS, 20);
	    // direction is only bit[0], we cast there the number and check
	    Write(i, MDIRECTION, 1);
	    Write(i, MLOWSPEED, 50);
	    Write(i, MHIGHSPEED, 2000);
	    Write(i, MACCRATE, 2000);
	    Write(i, MTRAIL, 10);
	    // powerconfig - enable move
	    Write(i, MPOWER, 1);
	    // trigger startmove
	    Write(i, MTRG, 1);
	    ##10;
	    `CHECK_EQUAL(Busy_o, 1, "At least one motor is already\
 triggered");
	 end // for (int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++)
	 MotorBusy_b = '0;

	 for(x=0; x<2500; x++) begin
	    for(int motor=0;
		motor < NUMBER_OF_MOTORS_PER_FIBER;
		motor++) begin
	       // read busy flag for each motor
	       Read(motor, MMSTATUS, ReadData);
	       // check if this particular one is still busy
	       MotorBusy_b[motor] = MRBUSY(ReadData);
	       $display("BUSY: ", MotorBusy_b);
	    end;
	    ##10000;

	    // we've read out all the motors:
	    if (~MotorBusy_b) break;
	 end; // for (x=0; x<2500; x++)
	 // either timeout, or busy is off
	 assert(x < 2500) else
	   $error("TIMEOUT while waiting for motors", x);
	 `CHECK_EQUAL(Busy_o, 0, "BUSY FAILS TO RESET");
      end


      `TEST_CASE("disable_data") begin
	 // test disable data = no transmission, we check simply on
	 // StepPFail_i signal
	 for (int i = 0; i < NUMBER_OF_GBT_LINKS-1; i++) begin
	    mcpacket_i[i].StepPFail_i = 1;
	    // the same fifo issue as with OH
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].StepPFail_i,
		       '1,
		       10,
		       "StepPFail_i does not propagate behind syncfifo");
	 end
	 // now TURN OFF enable on data channel:
	 packet_ix.enable = 0;
	 // check again without toggling stepfail, shold be still high
	 for (int i = 0; i < NUMBER_OF_GBT_LINKS-1; i++) begin
	    // the same fifo issue as with OH
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].StepPFail_i,
		       '1,
		       10,
		       "StepPFail_i should stay when packet data disabled");
	 end
	 // now we turn off the stepfail, but as the enable signal is
	 // not '1', it should do 'nothing'!
	 for (int i = 0; i < NUMBER_OF_GBT_LINKS-1; i++) begin
	    mcpacket_i[i].StepPFail_i = 0;
	    // the same fifo issue as with OH
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].StepPFail_i,
		       '1,
		       10,
		       "StepPFail_i changing while enabled=0 should\
 not change data");
	 end
	 // and as soon as we enable again, the situation should come
	 // back to normal
	 packet_ix.enable = 1;
	 for (int i = 0; i < NUMBER_OF_GBT_LINKS-1; i++) begin
	    // the same fifo issue as with OH
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].StepPFail_i,
		       '0,
		       10,
		       "StepPFail_i does not recover");
	 end
      end

      `TEST_CASE("mclink_status_wb_registers") begin
	 // read link number, this is base address of mclink register set
	 Read(16, LLINKIDENT, ReadData);
	 `CHECK_EQUAL(ReadData, g_mclinkInterfaceNumber, "MODULE INSTANCE does not fit");
	 // read default setting of command queue length
	 Read(16, LQLEN, ReadData);
	 `CHECK_EQUAL(ReadData, 32, "Queue length does not fit");
      end

      `TEST_CASE("POR_value") begin
	 // as resetting the motors into correct stage requires at least
	 // stepenab to go 'l', we will assume, that this propagates to the fifo
	 // output for the transceiver, and hence anytime within say 1000 clocks
	 // we should receive updated of the data in the FIFO queue. This is
	 // represented by rising enable and changing data to 'POR' defaults
	 `waitCheck(packet_ox.ClkRs_ix.clk,
		       packet_ox.enable,
		       '1,
		       1000,
		       "POR value failed");
      end

      `TEST_CASE("all_channels_irq") begin
	 // we run all the motors almost at the same time. Note that it is not
	 // necessary to test that two motors come at the same time. Although
	 // this situation can happen, it is very obscure. Firstly the motors
	 // can _never_ start at the same time, as they occupy different VME
	 // spaces, hence multiple VME write commands are issued. Secondly it is
	 // very improbable that two motors would have such setting, that they
	 // would compensate for start-time-differences and finish exactly at
	 // the same moment. If that would be however considered as correct
	 // situation, then we would have to refurbish the way how IRQs are
	 // generated and we would have to use FIFO to pile up generated IRQs
	 // and for each event trigger independently IRQ to VME.
	 for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	    // setup a single move
	    Write(i, MSTEPS, 20);
	    // direction is only bit[0], we cast there the number and check
	    Write(i, MDIRECTION, 1);
	    Write(i, MLOWSPEED, 50);
	    Write(i, MHIGHSPEED, 2000);
	    Write(i, MACCRATE, 2000);
	    Write(i, MTRAIL, 10);
	    // enable IRQ for each of them
	    Write(i, MTRIGCFG, 8);
	    // powerconfig - enable move
	    Write(i, MPOWER, 1);
	    // trigger startmove
	    Write(i, MTRG, 1);
	 end // for (int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++)

	 // wait for some time and check if busy asserted for the last motor
	 // 100ccs _from the busy flag going down should be enough (process is
	 // quite slow), maximum 100 readings
	 do
	   begin
	      Read(NUMBER_OF_MOTORS_PER_FIBER-1, MMSTATUS, ReadData);
	   end
	 while (MRBUSY(ReadData) == 1);
	 // wait for few more ccs and verify the IRQ counter
	 ##100;
	 `CHECK_EQUAL(irqPulses, NUMBER_OF_MOTORS_PER_FIBER, "DID NOT CATCH ALL\
	 THE IRQs");
	 `CHECK_EQUAL(DUT.IRQCounter_b32, irqPulses, "IRQ COUNTING WRONG");
	 Read(16,LIRQCNT, ReadData);
	 `CHECK_EQUAL(ReadData, irqPulses, "WB IRQ COUNTING WRONG");

      end

      `TEST_CASE("single_irq_generation") begin
	 // setup a single move
	 Write(0, MSTEPS, 20);
	 // direction is only bit[0], we cast there the number and check
	 Write(0, MDIRECTION, 1);
	 Write(0, MLOWSPEED, 50);
	 Write(0, MHIGHSPEED, 2000);
	 Write(0, MACCRATE, 2000);
	 Write(0, MTRAIL, 10);
	    // enablemovtri/genirq
	 Write(0, MTRIGCFG, 8);
	 // powerconfig - enable move
	 Write(0, MPOWER, 1);
	 // trigger startmove
	 Write(0, MTRG, 1);
	 // wait for some time and check if busy asserted
	 // 100ccs _from the busy flag going down should be enough (process is
	 // quite slow), maximum 100 readings
	 for(x=0; x<100; x++) begin
	    Read(0, MMSTATUS, ReadData);
	    if (MRBUSY(ReadData))
	      break;
	 end;
	 `CHECK_EQUAL(DUT.IRQCounter_b32, 0, "IRQ SHOULD BE ZERO");
	 Read(16, LIRQCNT, ReadData);
	 `CHECK_EQUAL(ReadData, 0, "WISHBONE IRQ COUNT NONZERO");


	 // this is how we check
         assert(x < 99) $display("\tBusy asserted in %d readings", x);
	 else
           $display("BUSY NOT ASSERTED WITHIN 100 wisbone readings");
	 // and wait for some time to assert IRQ
	 for(x=0; x<100; x++) begin
	    ##1;
	    // each cycle when there's enable of the motor we disable the
	    // counter, and we only enable it when at the end of cycle. This
	    // should mimic 'at maximum 100 cycles after the end of
	    // operation). We have setup motor 0 to do all the
	    // bussiness, hence IRQ0 should come
	    if (DUT.mcpacket_o[0].StepDeactivate_o == 0)
	      x = 0;
	    if ($rose(IRQ_ob[0]))
	      break;
	 end;
	 assert(x < 99) $display("Caught IRQ in %d readings- all OK", x);
	 else $error("IRQ not detected even after 100cc after the end of operation");
	 // checking for counter:
	 `CHECK_EQUAL(DUT.IRQCounter_b32, 1, "IRQ COUNTER DID NOTMOVE");
	 Read(16, LIRQCNT, ReadData);
	 `CHECK_EQUAL(ReadData, 1, "WISHBONE IRQ COUNT NOT ONE");

      end // assert (x < 99)

      `TEST_CASE("per_motor_reset") begin
	 // tests assertion of reset for each particular motor. We
	 // program the motor registers with random numbers, and then
	 // perform reset, all should go clean.
	 for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++)
	    // into each motor entity write random data. this can
	    // create quite a mess, INCLUDING STARTING THE MOTOR as we
	    // write into reg8 as well. Due to this we exclude trigger
	    // register 8, 17 and 18
	    for(int register = 0; register < 32; register++) begin
	       assert(std::randomize(ReadData));
	       // we have to exclude register8 because this one does
	       // command queue as well, which might break fifo when
	       // nonsense info gets in...
	       if (register != 8)
		 Write(i, register, ReadData);
	    end

	 // once motors are programmed with bullshit, we reset them
	 // and test if the data read correspond to what we want....
	 for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	    // issue reset:
	    Write(16, LMRESET, 2**i);
	    // and now check all the registers
	    for(int register = 0; register < 32; register++) begin
	       Read(i, register, ReadData);
	       // by default we expected cleared register to contain zero:
	       retval_b32 = '0;
	       // but there are _exceptions_:
	       if (register == 6)
		 // IRQ enabled by default:
		 retval_b32 = 32'h8;
	       else if (register == 9)
		 // motor identification
		 retval_b32 = (i << 28);
	       else if (register == 10)
		 // slow down set to 40:
		 retval_b32 = 40;
	       else if (register == 11)
		 // SALSP
		 retval_b32 = 10;
	       else if (register == 15)
		 // state machine in idle mode:
		 retval_b32 = 32'h01000000;
	       else if (register == 19)
		 // SALSN
		 retval_b32 = 15;
	       else if (register > 22)
		 // unused registers return this:
		 retval_b32 = 32'hdeadbeef;

	       $display("%d %d read %.8x, expected %.8x", i, register, ReadData,retval_b32);
	       `CHECK_EQUAL(ReadData, retval_b32);

	    end

	 end





      end // UNMATCHED !!


      `TEST_CASE("random_pulsing_complex_test") begin
	 // take random motor and count number of pulses at the output
	 // when fired. Note that this test can be _very long_
	 // depending of chosen plength
	 assert(std::randomize(i) with {i >= 0; i < NUMBER_OF_MOTORS_PER_FIBER;});

	 $display("Configuring motor ", i+1);

	 // we have to limit ourselves here, because of test length....
	 plength = $urandom_range(30, 10);
	 basereg = getBaseAddr(i);

	 cntr = i;

	 $display("Checking motor %d dir %d base 0x%x with random \
pulsing of %d pulses", i+1, cntr[0], basereg, plength);

	 // setup a single move
	 Write(i, MSTEPS, plength);
	 // direction is only bit[0], we cast there the number and check
	 Write(i, MDIRECTION, cntr[0]);
	 Write(i, MLOWSPEED, 50);
	 // we have to select very high values here in order to force
	 // the pulse generation to be quick, otherwise this test will
	 // take ages...
	 Write(i, MHIGHSPEED, 1500000);
	 Write(i, MACCRATE, 1510000);
	 Write(i, MTRAIL, 150000);
	 // enablemovtri/genirq
	 Write(i, MTRIGCFG, 0);
	 // powerconfig - enable move
	 Write(i, MPOWER, 1);

	 // trigger startmove
	 Write(i, MTRG, 1);
	 // wait for some time and check if busy asserted
	 // 100ccs should be enough (process is quite slow), maximum 100
	 // readings
	 $display("Waiting busy flag");
	 // suppress all 'rd/wr/ messages
	 fork
	    #100us;
	    forever begin
	       Read(i, MMSTATUS, ReadData, '0);
	       if (MRBUSY(ReadData))
		 break;
	    end
	 join_any;
	 `CHECK_EQUAL (MRBUSY(ReadData), 1, "TIMEOUT GETTING BUSY");

	 $display("Checking packet_ox output");

	 // poll busy flag, at the same time check if direction signal,
	 // enable signal and others from optical frame are OK
	 for(x=0; x<100;x++) begin
	    // wait a cycle in TX domain
	    @(posedge(packet_ox.ClkRs_ix.clk));
	    if(!mcpacket_o[i].StepDeactivate_o)
	      break;
	 end
	 assert(x<99) else
	   $error("STEPENAB failed - did not receive in 100 cycles");
	 // now read always wishbone and verify the state of bus. wb is
	 // faster hence busy flag will most likely go and then stepenab will
	 // go high. We verify that throughout the cycle stepenab does not
	 // fluctuate (the way how we do is not 100% bulletproof, but should
	 // give an idea)
	 $display("Reading wishbone busy");
	 do
	   begin
	      // and as well direction should be appropriate
	      `CHECK_EQUAL(mcpacket_o[i].StepDIR_o, cntr[0], "DIR FAILED");
	      if (mcpacket_o[i].StepDeactivate_o == 1 && countPulses[i] != plength)
		`CHECK_EQUAL(1, 0, "STEPENAB FAILED");
	      // read busy flag again
	      Read(i, MMSTATUS, ReadData, '0);
	   end
	 while (MRBUSY(ReadData) == 1);
	 ##5;

	 // we check out counting position register
	 Read(i, 13, ReadData);
	 if (cntr[0]) begin
            `CHECK_EQUAL(ReadData, unsigned'((32)'(-plength)));
	 end else begin
            `CHECK_EQUAL(ReadData, unsigned'((32)'(plength)));
	 end

	 // and stepsleft should be restarted
	 Read(i, 14, ReadData);
         `CHECK_EQUAL(ReadData, plength);
	 // and check independent counter counting real number of pulses
	 // going out
	 `CHECK_EQUAL(countPulses[i], plength, "WRONG NUMBER OF PULSES OUT");
      end // UNMATCHED !!

      `TEST_CASE("reading_switchesstatus_register") begin
	 // POR:
	 Read(16, LSWNSTATUS, ReadData);
	 `CHECK_EQUAL(LBSWNSTATUS(ReadData), '0);
	 `CHECK_EQUAL(LBSWPSTATUS(ReadData), '0);
	 // let's invert randomly one positive and one negative switch
	 // of 'any motor' such, that they will make a response in
	 // this particular register
	 assert(std::randomize(motorpos) with {motorpos >= 0; motorpos
	    < NUMBER_OF_MOTORS_PER_FIBER;});
	 assert(std::randomize(motorneg) with {motorneg >= 0; motorneg
	    < NUMBER_OF_MOTORS_PER_FIBER;});
	 $display("Inverting positive switch of motor %d and negative\
 switch of motor %d", motorpos, motorneg);
	 // write into configuration registers, in case of motorpos
	 // and motorneg being the same motors we need to write
	 // different number
	 if (motorpos == motorneg)
	   Write(motorpos, MSWITCHESCONFIG, 32'b1_10_1_01);
	 else begin
	    Write (motorpos, MSWITCHESCONFIG, 32'b0_10_1_01);
	    Write (motorneg, MSWITCHESCONFIG, 32'b1_10_0_01);
	 end
	 // configuration is written, we can read the values of the
	 // switches from the common status register:
	 Read(16, LSWNSTATUS, ReadData);
	 // and now we should check the values:
	 `CHECK_EQUAL(LBSWPSTATUS(ReadData), 2**motorpos);
	 `CHECK_EQUAL(LBSWNSTATUS(ReadData), 2**motorneg);

      end // UNMATCHED !!


      `TEST_CASE("optics_to_mc_data_propagation") begin
	 // here we test if data from packed optical interface (80bits) are
	 // correctly casted into 16 motors and if each of them can be read-back
	 // through the wishbone iface
	 for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	    // read all block numbers and check them. The reason why there is
	    // 2*NUMBER_OF_MOTORS_PER_FIBER is that we have intentionally
	    // increased the address bit of the motor interface by '1', which
	    // allows to extend the amount of MC registers up to 32 without
	    // later on changing the VME address map (and consecutively driver,
	    // fesa interface etcetc. It is much more easy to do it here and
	    // later on have some margin.) This does eat twice more memory, but
	    // no big deal, in total we're talking about 16*32 = 512 registers,
	    // so in 32bit VME address map this corresponds to an address block
	    // of 0 to 0x800.
	    // motion counter should be zeroed
	    Read(i, 20, ReadData);
	    `CHECK_EQUAL(ReadData, 0, "NONZERO MOTION COUNTER");
	    Read(i, MMSTATUS, ReadData);
	    // state machine should be in idle mode:
	    `CHECK_EQUAL(MRMSMAS(ReadData), 1, "SM IS NOT IDLE");
	    // identifier:
	    `CHECK_EQUAL(MRRAW_SW(ReadData), 0, "Raw switches config fail");
	    // let's propagate OH into VME
	    `CHECK_EQUAL(MROH(ReadData), 0, "NONZERO OH");
	    mcpacket_i[i].OH_i = 1;
	    // we have to _wait_ in the slow GBTXRX domain to propagate through
	    // FIFO, this might take time, let's check
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].OH_i,
		       '1,
		       10,
		       "OH does not propagate behind syncfifo");
	    // and now wait additional 100 clocks in fast domain to propagate
	    // through wishbone things into VME space
	    ##10;
	    Read(i, MMSTATUS, ReadData);
	    `CHECK_EQUAL(MROH(ReadData), 1, "OH is not setup");
	    // same with stepper fail
	    `CHECK_EQUAL(MRFAIL(ReadData), 0, "NONZERO STEPFAIL");
	    mcpacket_i[i].StepPFail_i = 1;
	    // the same fifo issue as with OH
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].StepPFail_i,
		       '1,
		       10,
		       "StepPFail_i does not propagate behind syncfifo");
	    ##10;
	    Read(i, MMSTATUS, ReadData);
	    `CHECK_EQUAL(MRFAIL(ReadData), 1, "Stepfail is not setup");
	    // setup switches each into its corresponding hardware input
	    // (otherwise by default all switch outputs mimic input0)
	    Write(i, MSWITCHESCONFIG, 32'b0_10_0_01);
	    // switches are more complicated as they require debouncing, let's
	    // cycle them
	    for (int switch=0; switch < 4; switch++) begin
	       mcpacket_i[i].RawSwitches_b2 = switch;
	       // wait for debouncing
	       ##1000;
	       // read wishbone
	       Read(i, MMSTATUS, ReadData);
	       $display("%h, %h", switch, ReadData);
	       `CHECK_EQUAL(MRSW(ReadData), switch, "Improper switch value");
	    end
	 end
      end

      `TEST_CASE("switches_cycling") begin
	 // OH propagation
	 for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	    `CHECK_EQUAL(DUT.mcpacket_i[i].RawSwitches_b2, 0, "RawSwitches_b2 should be cleared");
	    mcpacket_i[i].RawSwitches_b2[0] = 1;
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].RawSwitches_b2[0],
		       '1,
		       10,
		       "Switch 0 does not propagate");
	    mcpacket_i[i].RawSwitches_b2[1] = 1;
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].RawSwitches_b2,
		       3,
		       10,
		       "Switch 1 does not propagate");
	    mcpacket_i[i].RawSwitches_b2[0] = 0;
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].RawSwitches_b2,
		       2,
		       10,
		       "Switch 0 does not turn off");
	    mcpacket_i[i].RawSwitches_b2[1] = 0;
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].RawSwitches_b2,
		       0,
		       10,
		       "Switch 1 does not turn off");
	 end
      end

      `TEST_CASE("StepPFail_propagation") begin
	 // stepfail

	 for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	    `CHECK_EQUAL(DUT.mcpacket_i[i].StepPFail_i, 0, "StepPFail_i should be cleared");
	    mcpacket_i[i].StepPFail_i = 1;
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].StepPFail_i,
		       '1,
		       10,
		       "StepPFail_i does not propagate behind syncfifo");
	 end
      end

      `TEST_CASE("OH_propagation") begin
	 // OH propagation
	 for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	    `CHECK_EQUAL(DUT.mcpacket_i[i].OH_i, 0, "OH should be cleared");
	    mcpacket_i[i].OH_i = 1;
	    // wait maximum 10 'fast clock cycle domain clocks' to propagate
	    // into internals. This time should be somehow proportional to slow
	    // spped GBX clocks as FIFO inside mclink pulls the data on the slow
	    // edge
	    //
	    `waitCheck(DUT.mstatus_x.ClkRs_ix.clk,
		       DUT.mcpacket_i[i].OH_i,
		       '1,
		       10,
		       "OH does not propagate behind syncfifo");
	 end
      end

      `TEST_CASE("block_identifiers") begin
	 for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	    // read all block numbers and check them. The reason why there is
	    // 2*NUMBER_OF_MOTORS_PER_FIBER is that we have intentionally
	    // increased the address bit of the motor interface by '1', which
	    // allows to extend the amount of MC registers up to 32 without
	    // later on changing the VME address map (and consecutively driver,
	    // fesa interface etcetc. It is much more easy to do it here and
	    // later on have some margin.) This does eat twice more memory, but
	    // no big deal, in total we're talking about 16*32 = 512 registers,
	    // so in 32bit VME address map this corresponds to an address block
	    // of 0 to 0x800.
	    Read(i, MIDENTIFIER, ReadData);
	    `CHECK_EQUAL(MRIDENTIFIER(ReadData), i, "MODULE IDENTIFIER DOES NOT\
	 MATCH");
	    end
      end
   end;

   logic [31:0] Adr_obq32;
   assign Wb_iot.Adr_b = Adr_obq32[$size(Wb_iot.Adr_b)-1:0];

   // instantiate wishbone interface
   WbMasterSim
     i_WbMasterSim (// Outputs
		    .Rst_orq		(ClkRs_ix.reset),
		    .Adr_obq32		(Adr_obq32),
		    .Dat_obq32		(Wb_iot.DatMoSi_b),
		    .We_oq		(Wb_iot.We),
		    .Cyc_oq		(Wb_iot.Cyc),
		    .Stb_oq		(Wb_iot.Stb),
		    // Inputs
		    .Clk_ik		(ClkRs_ix.clk),
		    .Dat_ib32		(Wb_iot.DatMiSo_b),
		    .Ack_i		(Wb_iot.Ack));

   assign Wb_iot.Sel_b = 4'hf;
   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(2000000ms);

   // interfaces - !! NOTE THAT THIS INTERFACE USES CLOCK FROM writer (GBTX tx clock)
   data_x #(.g_DataWidth(g_FrameSize)) packet_ox(.ClkRs_ix(wClkRs_ix));
   // switches interface - direction from GEFE to VFC
   data_x #(.g_DataWidth(g_FrameSize)) packet_ix(.ClkRs_ix(rClkRs_ix));


   mclink #(/*AUTOINSTPARAM*/
	    // Parameters
	    .QUEUELENGTH		(QUEUELENGTH),
	    .g_FrameSize		(g_FrameSize),
	    .g_DataWidth		(g_DataWidth),
	    .g_mclinkInterfaceNumber	(g_mclinkInterfaceNumber),
	    .g_AddressWidth		(g_AddressWidth),
	    .g_AddressSpaceOffset	(g_AddressSpaceOffset))
   DUT
     (/*AUTOINST*/
      // Interfaces
      .Wb_iot				(Wb_iot),
      .packet_ox			(packet_ox),
      .packet_ix			(packet_ix),
      // Outputs
      .IRQ_ob				(IRQ_ob[NUMBER_OF_MOTORS_PER_FIBER-1:0]),
      .Busy_o				(Busy_o),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .triggers				(triggers));

endmodule
