//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_WBMC.SV
// @brief Wishbone motor controller testbench
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 23 October 2017
// @details Note that this testbench does not verify functionality of the
// ensemble, as these are already covered by unit tests of the specific modules,
// this one verifies if the data propagate to correct registers from the
// wishbone, and whether the feedback over modules works ok.
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;
import MCPkg::*;
import VmeAddressMap::*;

// Wishbone motor controller testbench
module tb_wbmc;
   //@TODO: changing timeunit change as well division ratio in
   //static_speed_calculus test bench as this one recalculates
   //measured $time to real microseconds
   timeunit 100ps;
   timeprecision 100ps;

   localparam integer QUEUELENGTH = 4;
   localparam g_DebouncingCounterWidth = 4;
   localparam g_SynchronizationFlips = 2;
   localparam g_ModuleIdentifier = 4'ha;


   logic [31:0]       ReadData, WriteData;
   int 		      start;


   triggers_t	triggers;		// To DUT of wbmc.v
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   ckrs_t		ClkRs_ix;		// To DUT of wbmc.v
   logic [1:0] [NUMBER_OF_MOTORS_PER_FIBER-1:0] OtherMotorSwitches_2b16;// To DUT of wbmc.v
   logic		reset_i;		// To DUT of wbmc.v
   // End of automatics

   logic 	      IRQ_o;
   logic 	      Busy_o;
   logic [31:0]       regdata[$] = {100, 101, 102, 103, 104, 105,
				    106, 107, 0, 32'ha0000001, 110, 47,
				    48, 0, 100, 32'h01000080};
   logic [1:0] 	      ConfiguredSwitches_b2;

   logic [31:0]       retval;

   time 	      prevstep;
   int 		      speed;
   real 	      distance;

   switchstate_t [1:0] SwitchesConfiguration_o;


   always forever #(12500ps) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking


   t_WbInterface #(.g_DataWidth(32),
		   .g_AddressWidth(32)) Wb_iot(ClkRs_ix.clk, ClkRs_ix.reset);
   mc_x mc();

   // this task will program a motor into a specific fixed state. The
   // motor is started but no further check whether it was really
   // performed is done (that depends on interlock checks)
   task startMotor ();
      i_WbMasterSim.WbWrite(MSTEPS, 192);
      i_WbMasterSim.WbWrite(MDIRECTION, 0);
      i_WbMasterSim.WbWrite(MLOWSPEED, 50);
      i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
      i_WbMasterSim.WbWrite(MACCRATE, 2000);
      i_WbMasterSim.WbWrite(MTRAIL, 10);
      // enablemovtri/genirq - enable interrupt (bit3, hence integer=8)
      i_WbMasterSim.WbWrite(MTRIGCFG, 8);
      // powerconfig - enable move
      i_WbMasterSim.WbWrite(MPOWER, 1);

      // trigger startmove, but not by writing into startmove, but
      // by writing 'any number' into
      assert(std::randomize(ReadData));
      i_WbMasterSim.WbWrite(MMSTART, ReadData);
   endtask // startMotor

   task checkMotorBusy(input int valueExpected);
      i_WbMasterSim.WbRead(MMSTATUS, ReadData);
      `CHECK_EQUAL(MRBUSY(ReadData), valueExpected, "BUSY SIGNAL NOT\
 IN EXPECTED STATE");
      if (valueExpected == 0) begin
	 fork
	    @(posedge Busy_o);
	    #1ms;
	 join_any
	 `CHECK_EQUAL(Busy_o, 0);
      end
   endtask // checkMotorBusy



   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 reset_i = '0;
	 // other motor switches 'off' by default...
	 OtherMotorSwitches_2b16 = '0;
	 // and now own motor config:
	 mc.invec.OH_i = 0;
	 mc.invec.RawSwitches_b2[0] = 0;
	 mc.invec.RawSwitches_b2[1] = 0;
	 mc.invec.StepPFail_i = 0;
	 mc.invec.RawSwitches_b2 = '0;
	 triggers.TrigMoveExt_i = '0;		// To DUT of wbmc.v
	 triggers.TrigMoveInt_i = '0;		// To DUT of wbmc.v
	 // enable triggers otherwise no trigger will be launched
	 triggers.TriggerEnable_i = 1;

         i_WbMasterSim.Reset();
	 // check default values:
	 // IRQ enabled:
	 i_WbMasterSim.WbRead(MTRIGCFG, ReadData);
	 `CHECK_EQUAL(ReadData, 8);
	 i_WbMasterSim.WbWrite(MTRIGCFG, 0);


	 // clearout registers
	 for (int i = 0; i < 8; i++)
           i_WbMasterSim.WbWrite(i, '0);

	 // and to the others write some reasonable values so we can 'trigger'
	 // if it comes
	 // stepnumber
         i_WbMasterSim.WbWrite(MSTEPS, 123);
	 // direction = 1
         i_WbMasterSim.WbWrite(MDIRECTION, 32'h00000001);
	 // low speed and high speed
         i_WbMasterSim.WbWrite(MLOWSPEED, 50);
         i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
	 // acceleration
         i_WbMasterSim.WbWrite(MACCRATE, 2000);
	 // trail
         i_WbMasterSim.WbWrite(MTRAIL, 10);

	 // slowdown default set to 40:
	 i_WbMasterSim.WbRead(MSLOWDOWN, ReadData);
	 `CHECK_EQUAL(ReadData, 40);
	 i_WbMasterSim.WbWrite(MSLOWDOWN, 0);

	 // steps after limit switch set to 10:
	 i_WbMasterSim.WbRead(MSALSP, ReadData);
	 `CHECK_EQUAL(ReadData, 10);
	 i_WbMasterSim.WbWrite(MSALSP, 0);

	 // move is disabled as well interrupt and power, to be setup in the
	 // unit test individually
	 `CHECK_EQUAL(Busy_o, 0);

      end // UNMATCHED !!

      `TEST_CASE("debug_test") begin
	 `CHECK_EQUAL(1, 1);
      end // UNMATCHED !!

      `TEST_CASE("trigger_registers_return_zero_val") begin
	 // all registers which trigger by writing have to return zero
	 // value:
	 i_WbMasterSim.WbWrite(MORIDECNT, 20);
	 i_WbMasterSim.WbRead(MORIDECNT, ReadData);
	 `CHECK_EQUAL(ReadData, '0);
	 i_WbMasterSim.WbWrite(MMSTART, 20);
	 i_WbMasterSim.WbRead(MMSTART, ReadData);
	 `CHECK_EQUAL(ReadData, '0);
	 i_WbMasterSim.WbWrite(MMSTOP, 20);
	 i_WbMasterSim.WbRead(MMSTOP, ReadData);
	 `CHECK_EQUAL(ReadData, '0);
      end // UNMATCHED !!


      `TEST_CASE("external_reset") begin
	 // test if by pulsing a reset signal to '1' it clears out all
	 // the registers and stops the operation
         i_WbMasterSim.WbWrite(MSTEPS, 192);
         i_WbMasterSim.WbWrite(MDIRECTION, 1);
         i_WbMasterSim.WbWrite(MLOWSPEED, 50);
         i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
         i_WbMasterSim.WbWrite(MACCRATE, 2000);
         i_WbMasterSim.WbWrite(MTRAIL, 10);
	 // enablemovtri/genirq - enable interrupt (bit3, hence integer=8)
         i_WbMasterSim.WbWrite(MTRIGCFG, 8);
	 // powerconfig - enable move
         i_WbMasterSim.WbWrite(MPOWER, 1);
	 i_WbMasterSim.WbWrite(MSLOWDOWN, 2);
	 i_WbMasterSim.WbWrite(MSALSP, 999);
	 // switchesconfig to 'whatever' which runs the motor
	 i_WbMasterSim.WbWrite(MSWITCHESCONFIG, 32'b0_10_0_01);

	 // fire motor by write:
	 i_WbMasterSim.WbWrite(MMSTART, 1);
	 // wait for busy:
	 fork
	    @(posedge Busy_o);
	    #1ms;
	 join_any
	 `CHECK_EQUAL(Busy_o, 1);

	 #1ms;
	 // kill the process:
	 ##1 reset_i = '1;
	 ##1 reset_i = '0;

	 ##5;
	 `CHECK_EQUAL(Busy_o, 0);
	 // and now let's read out all the registers and check against
	 // default values they should have:
	 for(int register = 0; register < NUMBER_OF_MC_REGISTERS; register++) begin
	    i_WbMasterSim.WbRead(register, ReadData);
	    $display("Register: %d -> 0x%.8x", register, ReadData);
	    // individual checks:
	    retval = 0;
	    // specific registers:
	    if (register == 6)
	      // IRQ enabled by default:
	      retval = 32'h8;
	    else if (register == 9)
	      // module identifier is 10:
	      retval = 32'ha0000000;
	    else if (register == 10)
	      // slowdown = 40 to get 1MHz clock in WBMC
	      retval = 32'h28;
	    else if (register == 11)
	      // 10 positive steps after limit switch by default
	      retval = 10;
	    else if (register == 15)
	      // state machine in idle mode - properly reset device
	      retval = 32'h01000000;
	    else if (register == 19)
	      // 15 negative SALS
	      retval = 15;
	    else if (register > 22)
	      // unused registers should return this:
	      retval = 32'hdeadbeef;

	    `CHECK_EQUAL(ReadData, retval);
	 end

      end // UNMATCHED !!

      `TEST_CASE("export_of_switches_configuration") begin
	 // we check whether if setup switches, the information
	 // propagates to the output signal
	 `CHECK_EQUAL(SwitchesConfiguration_o, '0);
	 ##10;
	 assert(std::randomize(ReadData));
	 i_WbMasterSim.WbWrite(MSWITCHESCONFIG, ReadData);
	 ##100;
	 `CHECK_EQUAL(SwitchesConfiguration_o,
		      ReadData[$bits(SwitchesConfiguration_o)-1:0]);
      end // UNMATCHED !!


      `TEST_CASE("writing_lockmasks") begin
	 // only 16 bits allowed in lock masks
	 i_WbMasterSim.WbWrite(MILCKP, 32'hffff_ffff);
	 i_WbMasterSim.WbWrite(MILCKN, 32'hffff_ffff);
	 i_WbMasterSim.WbRead(MILCKP, ReadData);
	 `CHECK_EQUAL(ReadData, 32'h0000_ffff - 32'(2**g_ModuleIdentifier));
	 i_WbMasterSim.WbRead(MILCKN, ReadData);
	 `CHECK_EQUAL(ReadData, 32'h0000_ffff - 32'(2**g_ModuleIdentifier));

      end // UNMATCHED !!


      `TEST_CASE("reading_unused_register") begin
	 i_WbMasterSim.WbRead(31, ReadData);
	 `CHECK_EQUAL(ReadData, 32'hdeadbeef);
      end // UNMATCHED !!

      `TEST_CASE("setting_up_arbitrary_position_counter") begin
	 // check if writing register 16 sets up the position counter
	 // to a predefined (two's complement) value

	 // by default, POR register should be at zero
	 i_WbMasterSim.WbRead(MPOSITION, ReadData);
	 `CHECK_EQUAL(ReadData, '0);
	 // write into register 16 to 'overwrite' the value
	 assert(std::randomize(WriteData) with {WriteData > 0;});
	 i_WbMasterSim.WbWrite(MORIDECNT, WriteData);
	 // this register is toggle, which means that any write into
	 // this register should resolve, and next clock cycles it
	 // should show 'zero' when reading
	 i_WbMasterSim.WbRead(MORIDECNT, ReadData);
	 `CHECK_EQUAL(ReadData, '0);

	 // reading back we should get the position counter to this
	 // value
	 i_WbMasterSim.WbRead(MPOSITION, ReadData);
	 `CHECK_EQUAL(ReadData, WriteData);

	 // one more read of the same, and checkup if value is
	 // persistent
	 i_WbMasterSim.WbRead(MPOSITION, ReadData);
	 `CHECK_EQUAL(ReadData, WriteData);
	 // trigger reset counter pos to see if all OK:
	 i_WbMasterSim.WbWrite(MTRG, 4);
	 i_WbMasterSim.WbRead(MPOSITION, ReadData);
	 `CHECK_EQUAL(ReadData, '0);
	 // check if writing 0 into register 16 works as well, for
	 // this we need arbitrary value first
	 i_WbMasterSim.WbWrite(MORIDECNT, WriteData);
	 i_WbMasterSim.WbRead(MPOSITION, ReadData);
	 `CHECK_EQUAL(ReadData, WriteData);
	 // write zero into counterpos and check:
	 i_WbMasterSim.WbWrite(MORIDECNT, '0);
	 i_WbMasterSim.WbRead(MPOSITION, ReadData);
	 `CHECK_EQUAL(ReadData, '0);


      end // UNMATCHED !!

      `TEST_CASE("readback_set_values") begin
	 // write into all 15 registers and read back the values
	 for (int i = 0; i < 16; i++)
            i_WbMasterSim.WbWrite(i, 100+i);

	 start = 0;
	 foreach (regdata[i]) begin
	    i_WbMasterSim.WbRead(start, ReadData);
	    $display("Register %d read %.8x, should be %.8x",
		     start,
		     ReadData,
		     regdata[i]);

	    `CHECK_EQUAL(ReadData, regdata[i]);
	    start++;
	 end

      end // UNMATCHED !!

      `TEST_CASE("inverted_switches_fire_as_well") begin
	 // we test here, whether stepping controller reacts on
	 // MODIFIED switches settings. So setup first - write
	 // INVERTED switches - these signals we get over the optical
	 // stream
         i_WbMasterSim.WbWrite(MSWITCHESCONFIG, 6'b0_10_0_01);

	 mc.invec.RawSwitches_b2 = '1;
	 // accounts for signals deboucing
	 #10us;
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b11);
	 // now invert their polarity and setup correctly the switches
	 // config as in real motor
         i_WbMasterSim.WbWrite(MSWITCHESCONFIG, 6'b1_10_1_01);
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b00);
	 // now the system is setup such, that 'external switches'
	 // show both logic '1', but correctly configured switches
	 // show both '0' which is the state we need to enable the
	 // motor movements. Write motor enable so the
	 // stepenab should go high when the cycle is launched
         i_WbMasterSim.WbWrite(MPOWER, 32'(1));
	 // trigger enable:
         i_WbMasterSim.WbWrite(MTRIGCFG, 32'(2));
	 // and now we can fire the trigger:
	 `CHECK_EQUAL(DUT.ICmdParams_x.globaltrigger.internal, 1);
         i_WbMasterSim.WbWrite(MTRIGCFG, 32'(2));
	 // startmove:
         i_WbMasterSim.WbWrite(MTRG, 32'(1));
	 // verify that motor moves:
	 fork
	    #10ms;
	    @(negedge mc.outvec.StepOutP_o);
	 join_any;
	 #1 `CHECK_EQUAL(mc.outvec.StepOutP_o, 0);
	 // inverted switches should be seen in register as well:
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRRAW_SW(ReadData), 2'b11);
	 mc.invec.RawSwitches_b2 = '0;
	 #10us;
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRRAW_SW(ReadData), 2'b00);
      end

      `TEST_CASE("power_config_separate_settings") begin
	 // check if power settings propagate correctly to cmdparam
	 // power config: move enable and standby = stepenab goes
	 // enabled, and hence driver should be powered:
         i_WbMasterSim.WbWrite(MPOWER, 32'(1));
	 i_WbMasterSim.WbRead(MPOWER, ReadData);
	 `CHECK_EQUAL(ReadData, 1, "Failed setting power");
	 `CHECK_EQUAL(DUT.ICmdParams_x.powerconfig.enable_move, 1);
	 `CHECK_EQUAL(DUT.ICmdParams_x.powerconfig.boost, 0);
	 `CHECK_EQUAL(DUT.ICmdParams_x.powerconfig.standby, 0);
         i_WbMasterSim.WbWrite(MPOWER, 32'(2));
	 i_WbMasterSim.WbRead(MPOWER, ReadData);
	 `CHECK_EQUAL(ReadData, 2, "Failed setting power");
	 `CHECK_EQUAL(DUT.ICmdParams_x.powerconfig.enable_move, 0);
	 `CHECK_EQUAL(DUT.ICmdParams_x.powerconfig.boost, 1);
	 `CHECK_EQUAL(DUT.ICmdParams_x.powerconfig.standby, 0);
         i_WbMasterSim.WbWrite(MPOWER, 32'(4));
	 i_WbMasterSim.WbRead(MPOWER, ReadData);
	 `CHECK_EQUAL(ReadData, 4, "Failed setting power");
	 `CHECK_EQUAL(DUT.ICmdParams_x.powerconfig.enable_move, 0);
	 `CHECK_EQUAL(DUT.ICmdParams_x.powerconfig.boost, 0);
	 `CHECK_EQUAL(DUT.ICmdParams_x.powerconfig.standby, 1);
      end

      `TEST_CASE("max_power_number_write") begin
	 // check if power settings propagate correctly to cmdparam
	 // power config: move enable and standby = stepenab goes
	 // enabled, and hence driver should be powered:
         i_WbMasterSim.WbWrite(MPOWER, 32'(5));
	 i_WbMasterSim.WbRead(MPOWER, ReadData);
	 `CHECK_EQUAL(ReadData, 5, "Failed setting power");
	 `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 0);
      end

      `TEST_CASE("stepenab_turns_with_standby") begin
	 // checks how stepenab behaves with power config
	 `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 1);
	 // power config: move enable and standby = stepenab goes
	 // enabled, and hence driver should be powered:
         i_WbMasterSim.WbWrite(MPOWER, 32'(5));
	 i_WbMasterSim.WbRead(MPOWER, ReadData);
	 `CHECK_EQUAL(ReadData, 5, "Failed setting power");
	 `CHECK_EQUAL(mc.outvec.StepDeactivate_o, 0);
      end

      `TEST_CASE("correct_value_steps_after_limit") begin
	 // maximum value for steps after switch limit
         i_WbMasterSim.WbWrite(MSALSP, 32'hffff_ffff);
	 ##10;
	 i_WbMasterSim.WbRead(MSALSP, ReadData);
	 // input selected switch (more checks so it is clear which
	 // bit is what)
	 `CHECK_EQUAL(ReadData, 2**6-1, "Steps after limit switch problem");
	 `CHECK_EQUAL(DUT.CntParam_ix.StepsAfterLimitSwitchP_b6,
		      2**6-1,
		      "Steps after limit goes into VME but not target");
	 // same for negative target:
         i_WbMasterSim.WbWrite(MSALSN, 32'hffff_ffff);
	 ##10;
	 i_WbMasterSim.WbRead(MSALSN, ReadData);
	 // input selected switch (more checks so it is clear which
	 // bit is what)
	 `CHECK_EQUAL(ReadData, 2**6-1, "Steps after limit switch problem");
	 `CHECK_EQUAL(DUT.CntParam_ix.StepsAfterLimitSwitchN_b6,
		      2**6-1,
		      "Steps after limit goes into VME but not target");
      end

      `TEST_CASE("power_config_reg") begin
	 // checking writes to power configuration
         i_WbMasterSim.WbWrite(MPOWER, 32'h7);
	 ##10;
	 i_WbMasterSim.WbRead(MPOWER, ReadData);
	 // input selected switch (more checks so it is clear which
	 // bit is what)
	 `CHECK_EQUAL(MRENABLE_MOVE(ReadData), 1, "Enablemove not writable");
	 `CHECK_EQUAL(MRBOOST(ReadData), 1, "Boost bit1 not writable");
	 `CHECK_EQUAL(MRSTANDBY(ReadData), 1, "Standby bit2 not writable");
      end

      `TEST_CASE("switches_config_write") begin
	 // checking polarity invesion write (this was seen in real
	 // system not working!)
         i_WbMasterSim.WbWrite(MSWITCHESCONFIG, 32'hffff_ffff);
	 ##10;
	 i_WbMasterSim.WbRead(MSWITCHESCONFIG, ReadData);
	 // input selected switch (more checks so it is clear which
	 // bit is what)
	 `CHECK_EQUAL(ReadData, 63);

      end

      `TEST_CASE("motion_counter") begin
	 // let's queue 3 same commands - this will fill the queue
         i_WbMasterSim.WbWrite(MSTEPS, 192);
         i_WbMasterSim.WbWrite(MDIRECTION, 1);
         i_WbMasterSim.WbWrite(MLOWSPEED, 50);
         i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
         i_WbMasterSim.WbWrite(MACCRATE, 2000);
         i_WbMasterSim.WbWrite(MTRAIL, 10);
	 // enablemovtri/genirq - enable interrupt (bit3, hence integer=8)
	 // enablemovetrig=00 means all external triggers disabled and we run
	 // queued commands automatically
         i_WbMasterSim.WbWrite(MTRIGCFG, 8);
	 // powerconfig - enable move
         i_WbMasterSim.WbWrite(MPOWER, 1);
	 for(int i=0; i < 10; i++) begin
	    i_WbMasterSim.WbRead(MMOTION, ReadData);
            `CHECK_EQUAL(ReadData, i, "motion counter is flimsy");
	    // start motion and wait until it finishes
            i_WbMasterSim.WbWrite(MTRG, 1);
	    wait ($rose(IRQ_o));
	    ##100;
	 end
      end // UNMATCHED !!

      `TEST_CASE("static_speed_calculus") begin
	 // this test does not perform any verification, but runs
	 // motor with various speeds and displays the time between
	 // pulses out. Used to see if the real system behaves exactly
	 // the same
         i_WbMasterSim.WbWrite(MSTEPS, 3);
         i_WbMasterSim.WbWrite(MDIRECTION, 1);
         i_WbMasterSim.WbWrite(MLOWSPEED, 2000);
         i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
         i_WbMasterSim.WbWrite(MACCRATE, 0);
         i_WbMasterSim.WbWrite(MTRAIL, 10);
	 // enablemovtri/genirq - enable interrupt (bit3, hence integer=8)
	 // enablemovetrig=00 means all external triggers disabled and we run
	 // queued commands automatically
         i_WbMasterSim.WbWrite(MTRIGCFG, 8);
	 // powerconfig - enable move
         i_WbMasterSim.WbWrite(MPOWER, 1);

	 // set slow-down to 40 as in real system. This should make
	 // division of clock by 40 hence with clock being 40MHz we
	 // should go to 1MHz pusing
         i_WbMasterSim.WbWrite(MSLOWDOWN, 40);

	 // start motion and wait until it finishes
	 $display("Table of SPEED versus pulses distance versus\
 pulses per second");
	 for(speed=200; speed <= 3000; speed+=100)
	   fork
	      begin
		 i_WbMasterSim.WbWrite(MLOWSPEED, speed);
		 i_WbMasterSim.WbWrite(MHIGHSPEED, speed);
		 i_WbMasterSim.WbWrite(MTRG, 1);
		 wait ($rose(IRQ_o));
	      end
	      begin
		 @(negedge mc.outvec.StepOutP_o);
		 prevstep = $time;
		 @(negedge mc.outvec.StepOutP_o);
		 // we divide here by 10000 to convert to
		 // microseconds. NOTE THAT IF YOU CHANGE
		 // TIMEUNIT/TIMEPRECISION, THIS WILL CHANGE!
		 distance = ($time-prevstep) / 10000.0;

		 $display("%d, %f, %f",
			  speed,
			  distance,
			  1 / (distance * 1e-6));
	      end
	   join

	 // having table of speeds we check for a particular case how
	 // it is for the acceleration. Set low=200, high=2000, and
	 // print out each step
/* -----\/----- EXCLUDED -----\/-----
	 i_WbMasterSim.WbWrite(MLOWSPEED, 200);
	 i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
         i_WbMasterSim.WbWrite(MSTEPS, 100);
	 for(speed=0; speed <= 1000; speed+=100) begin
	    $display("ACC rate = %d", speed);
	    fork
	       begin
		  i_WbMasterSim.WbWrite(MACCRATE, speed);
		  i_WbMasterSim.WbWrite(MTRG, 1);
		  wait ($rose(IRQ_o));
	       end
	       begin
		  @(posedge mc.outvec.StepOutP_o);
		  prevstep = $time;
		  repeat(20) begin
		     @(posedge mc.outvec.StepOutP_o);
		     // we divide here by 10000 to convert to
		     // microseconds. NOTE THAT IF YOU CHANGE
		     // TIMEUNIT/TIMEPRECISION, THIS WILL CHANGE!
		     distance = ($time-prevstep) / 10000.0;
		     $display("%f", distance);
		     prevstep = $time;
		  end
	       end
	    join
	 end // for (speed=0; speed <= 1000; speed+=100)
 -----/\----- EXCLUDED -----/\----- */
      end // UNMATCHED !!

      `TEST_CASE("interlock_mask_itself_forbidden") begin
	 // we know our g_ModuleIdentifier, hence we can write into
	 // both interlocks and read back value:
	 i_WbMasterSim.WbWrite(MILCKP, '1);
	 i_WbMasterSim.WbWrite(MILCKN, '1);
	 // read back:
	 i_WbMasterSim.WbRead(MILCKP, ReadData);
	 // check that motor itself is cleared to zero, meaning it can
	 // never mask itself, for both positive and negative
	 `CHECK_EQUAL(ReadData, 32'h0000_ffff - 32'(2**g_ModuleIdentifier));
	 i_WbMasterSim.WbRead(MILCKN, ReadData);
	 `CHECK_EQUAL(ReadData, 32'h0000_ffff - 32'(2**g_ModuleIdentifier));
      end // assert (std::randomize(OtherMotorSwitches_2b16))

      `TEST_CASE("interlock_seen_in_status_register") begin
	 // status register should contain the interlock value
	 OtherMotorSwitches_2b16 = '1;
	 i_WbMasterSim.WbWrite(MILCKP, 32'hffff);
	 i_WbMasterSim.WbWrite(MILCKN, 32'h0);
	 // all interlocks cleared out, we should read zero:
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRAILC(ReadData), 0);
	 // engage negative interlocks, nothing should be seen still:
	 OtherMotorSwitches_2b16[1] = '0;
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRAILC(ReadData), 0);
	 // disable one positive switch bit, this time interlock
	 // should appear:
	 OtherMotorSwitches_2b16[0][0] = '0;
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRAILC(ReadData), 1);
	 // and disable masks:
	 i_WbMasterSim.WbWrite(MILCKP, '0);
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRAILC(ReadData), 0);
      end // UNMATCHED !!


      `TEST_CASE("interlock_logic_checks") begin
	 // interlocking: into each motor block enters the information
	 // about other motors switches (logical) settings. Now, part
	 // of the motor block are two interlock registers enabling
	 // masks on positive and negative switches of other
	 // motors. Functionality is such, that if a particular bit is
	 // enabled in the positive/negative mask, it means that our
	 // motor can only be enabled when positive/negative switch
	 // from the other motor(s) masked by pos/neg mask are in the
	 // switch positions. Here we test if we can start our motor
	 // when the masks are all set to zero. In principle that
	 // means 'dont care about other motors in the link'

	 // generate random switches config
	 assert(std::randomize(OtherMotorSwitches_2b16));

	 // and generate random mask, which is _different_ from
	 // switches config:
	 assert(std::randomize(retval) with {retval > 0;});
	 $display("Motors switches: 0x%.8x, mask: 0x%.8x",
		  OtherMotorSwitches_2b16,
		  retval);

	 // all other motors have to have both switches engaged in
	 // order to make any movement in our case (and this is the
	 // case due to the command above):
	 i_WbMasterSim.WbWrite(MILCKP, 32'(retval[15:0]));
	 i_WbMasterSim.WbWrite(MILCKN, 32'(retval[31:16]));
	 // this configuration should _never_ trigger motor as mask is
	 // nonzero and switches configuration does not match the mask!
	 startMotor();
	 ##100;
	 checkMotorBusy(0);

	 // now we however set the mask to be equal to motors switches
	 // configuration, and this _must trigger_
	 i_WbMasterSim.WbWrite(MILCKP, 32'(OtherMotorSwitches_2b16[0]));
	 i_WbMasterSim.WbWrite(MILCKN, 32'(OtherMotorSwitches_2b16[1]));
	 startMotor();
	 ##100;
	 checkMotorBusy(1);
      end // UNMATCHED !!

      `TEST_CASE("interlock_irq_when_interlocked") begin
	 OtherMotorSwitches_2b16 = '1;
	 OtherMotorSwitches_2b16[0] = '0;
	 i_WbMasterSim.WbWrite(MILCKP, 32'hffff);
	 i_WbMasterSim.WbWrite(MILCKN, 32'h0);
	 fork
	    startMotor();
	    // irq should be generated immediatelly as there's no movement
	    @(posedge IRQ_o);
	 join_any
	 `CHECK_EQUAL(IRQ_o, 1);
	 // and yet check that busy is never risen
	 fork
	    @(posedge Busy_o);
	    #1ms;
	 join_any
	 `CHECK_EQUAL(Busy_o, 0);
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRBUSY(ReadData), 0, "BUSY SIGNAL NOT\
 IN EXPECTED STATE");
      end // UNMATCHED !!

      `TEST_CASE("interlock_motor_stops_immediately") begin
	 // when motor in movement, and interlock in engaged, all the
	 // activity has to stop on that motor. After disengaging
	 // interlock the motor has to stay stopped as its task just
	 // finished...

	 // setup OK interlocks
	 OtherMotorSwitches_2b16 = '1;
	 i_WbMasterSim.WbWrite(MILCKP, 32'hffff);
	 i_WbMasterSim.WbWrite(MILCKN, 32'h0);
	 // and run the motor
	 startMotor();
	 checkMotorBusy(1);
	 // now we stop the movement, check if IRQ was generated:
	 fork
	    begin
	       OtherMotorSwitches_2b16[0][0] = '0;
	       #100ms;
	    end
	    @(posedge IRQ_o);
	    #1ms;
	 join_any
	 // irq generated
	 `CHECK_EQUAL(IRQ_o, 1);
	 ##10;
	 // no more motor busy
	 `CHECK_EQUAL(Busy_o, 0);
	 // VME register announces no busy as well
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
	 `CHECK_EQUAL(MRBUSY(ReadData), 0, "BUSY SIGNAL NOT\
 IN EXPECTED STATE");

	 // interlock release, check that motor does not work:
	 checkMotorBusy(0);
	 OtherMotorSwitches_2b16[0][0] = '1;
	 checkMotorBusy(0);

      end // UNMATCHED !!



      `TEST_CASE("interlock_positive_none_engaged") begin
	 // take random positive switch, check matrix if that works:
	 OtherMotorSwitches_2b16 = '1;
	 // disengage one, this should disqualify motor startup
	 OtherMotorSwitches_2b16[0] = '0;

	 // all other motors have to have both switches engaged in
	 // order to make any movement in our case (and this is the
	 // case due to the command above):
	 i_WbMasterSim.WbWrite(MILCKP, 32'hffff);
	 i_WbMasterSim.WbWrite(MILCKN, 32'h0);
	 // and start, and it should go:
	 startMotor();
	 ##100;
	 checkMotorBusy(0);
      end // UNMATCHED !!

      `TEST_CASE("interlock_positive_all_engaged") begin
	 // take random positive switch, check matrix if that works:
	 OtherMotorSwitches_2b16 = '1;
	 // disengage one, this should disqualify motor startup
	 OtherMotorSwitches_2b16[1] = '0;

	 // all other motors have to have both switches engaged in
	 // order to make any movement in our case (and this is the
	 // case due to the command above):
	 i_WbMasterSim.WbWrite(MILCKP, 32'hffff);
	 i_WbMasterSim.WbWrite(MILCKN, 32'h0);
	 // and start, and it should go:
	 startMotor();
	 ##100;
	 checkMotorBusy(1);
      end // UNMATCHED !!

      `TEST_CASE("interlock_all_switches_but_one_engaged") begin
	 // take random positive switch, check matrix if that works:
	 OtherMotorSwitches_2b16 = '1;
	 // disengage one, this should disqualify motor startup
	 OtherMotorSwitches_2b16[0][0] = '0;

	 // all other motors have to have both switches engaged in
	 // order to make any movement in our case (and this is the
	 // case due to the command above):
	 i_WbMasterSim.WbWrite(MILCKP, 32'hffff);
	 i_WbMasterSim.WbWrite(MILCKN, 32'hffff);
	 // and start, and it should go:
	 startMotor();
	 ##100;
	 checkMotorBusy(0);
      end // UNMATCHED !!


      `TEST_CASE("interlock_all_switches_engaged_with_mask") begin
	 // take random positive switch, check matrix if that works:
	 OtherMotorSwitches_2b16 = '1;
	 // all other motors have to have both switches engaged in
	 // order to make any movement in our case (and this is the
	 // case due to the command above):
	 i_WbMasterSim.WbWrite(MILCKP, 32'hffff);
	 i_WbMasterSim.WbWrite(MILCKN, 32'hffff);
	 // and start, and it should go:
	 startMotor();
	 ##100;
	 checkMotorBusy(1);
      end // UNMATCHED !!

      `TEST_CASE("interlock_no_switches_with_masking") begin
	 // take random positive switch, check matrix if that works:
	 OtherMotorSwitches_2b16 = '0;
	 // all other motors have to have both switches engaged in
	 // order to make any movement in our case (and this is the
	 // case due to the command above):
	 i_WbMasterSim.WbWrite(MILCKP, 32'hffff);
	 i_WbMasterSim.WbWrite(MILCKN, 32'hffff);
	 // and start, and it should go:
	 startMotor();
	 ##100;
	 checkMotorBusy(0);
      end // UNMATCHED !!

      `TEST_CASE("interlock_all_other_motors_in_both_positions") begin
	 OtherMotorSwitches_2b16 = '1;
	 // all other motors have to have both switches engaged in
	 // order to make any movement in our case (and this is the
	 // case due to the command above):
	 i_WbMasterSim.WbWrite(MILCKP, 32'hffff);
	 i_WbMasterSim.WbWrite(MILCKN, 32'hffff);
	 // and start, and it should go:
	 startMotor();
	 ##100;
	 checkMotorBusy(1);
      end // UNMATCHED !!


      `TEST_CASE("interlock_no_masking_all_other_in_switches") begin
	 // see interlock_logic_checks for more explanation
	 OtherMotorSwitches_2b16 = '1;
	 // and start, and it should go:
	 startMotor();
	 ##100;
	 checkMotorBusy(1);
      end // UNMATCHED !!

      `TEST_CASE("interlock_no_masking_no_other_in_switches") begin
	 // see interlock_logic_checks for more explanation
	 OtherMotorSwitches_2b16 = '0;
	 // and start, and it should go:
	 startMotor();
	 ##100;
	 checkMotorBusy(1);
      end // UNMATCHED !!


      `TEST_CASE("startstop_by_write") begin
	 // tests if the start and stop movement can be triggered as
	 // well by writing 'any number' into particular registers.
	 // setup motor first:
         i_WbMasterSim.WbWrite(MSTEPS, 192);
         i_WbMasterSim.WbWrite(MDIRECTION, 0);
         i_WbMasterSim.WbWrite(MLOWSPEED, 50);
         i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
         i_WbMasterSim.WbWrite(MACCRATE, 2000);
         i_WbMasterSim.WbWrite(MTRAIL, 10);
	 // enablemovtri/genirq - enable interrupt (bit3, hence integer=8)
         i_WbMasterSim.WbWrite(MTRIGCFG, 8);
	 // powerconfig - enable move
         i_WbMasterSim.WbWrite(MPOWER, 1);

	 // trigger startmove, but not by writing into startmove, but
	 // by writing 'any number' into
	 assert(std::randomize(ReadData));
	 i_WbMasterSim.WbWrite(MMSTART, ReadData);
	 // wait for some time and check if busy asserted
	 // 100ccs should be enough (process is quite slow)
	 ##100;
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
         `CHECK_EQUAL(MRBUSY(ReadData), 1, "BUSY SIGNAL NOT ASSERTED");
	 // issue stop by write:
	 assert(std::randomize(ReadData));
	 i_WbMasterSim.WbWrite(MMSTOP, ReadData);
	 ##100;
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
         `CHECK_EQUAL(MRBUSY(ReadData), 0, "BUSY SIGNAL NOT ASSERTED");
      end // UNMATCHED !!

      `TEST_CASE("startstop_by_writing_zero") begin
	 // tests if the start and stop movement can be triggered as
	 // well by writing zero into particular registers.
	 // setup motor first:
         i_WbMasterSim.WbWrite(MSTEPS, 192);
         i_WbMasterSim.WbWrite(MDIRECTION, 0);
         i_WbMasterSim.WbWrite(MLOWSPEED, 50);
         i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
         i_WbMasterSim.WbWrite(MACCRATE, 2000);
         i_WbMasterSim.WbWrite(MTRAIL, 10);
	 // enablemovtri/genirq - enable interrupt (bit3, hence integer=8)
         i_WbMasterSim.WbWrite(MTRIGCFG, 8);
	 // powerconfig - enable move
         i_WbMasterSim.WbWrite(MPOWER, 1);

	 // trigger startmove, but not by writing into startmove, but
	 // by writing 'any number' into
	 i_WbMasterSim.WbWrite(MMSTART, '0);
	 // wait for some time and check if busy asserted
	 // 100ccs should be enough (process is quite slow)
	 ##100;
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
         `CHECK_EQUAL(MRBUSY(ReadData), 1, "BUSY SIGNAL NOT ASSERTED");
	 // issue stop by write:
	 i_WbMasterSim.WbWrite(MMSTOP, '0);
	 ##100;
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
         `CHECK_EQUAL(MRBUSY(ReadData), 0, "BUSY SIGNAL NOT ASSERTED");
      end // UNMATCHED !!


      `TEST_CASE("startstop_by_write_masked") begin
         i_WbMasterSim.WbWrite(MSTEPS, 192);
         i_WbMasterSim.WbWrite(MDIRECTION, 0);
         i_WbMasterSim.WbWrite(MLOWSPEED, 50);
         i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
         i_WbMasterSim.WbWrite(MACCRATE, 2000);
         i_WbMasterSim.WbWrite(MTRAIL, 10);
	 // enablemovtri/genirq - enable interrupt (bit3, hence integer=8)
         i_WbMasterSim.WbWrite(MTRIGCFG, 8);
	 // powerconfig - enable move
         i_WbMasterSim.WbWrite(MPOWER, 1);
	 // startstop registers must be masked - no matter what we
	 // write, reading always results in zero
	 assert(std::randomize(ReadData));
	 i_WbMasterSim.WbWrite(MMSTART, ReadData);
	 i_WbMasterSim.WbRead(MMSTART, ReadData);
	 `CHECK_EQUAL(ReadData, '0);
	 ##100;
	 assert(std::randomize(ReadData));
	 i_WbMasterSim.WbWrite(MMSTOP, ReadData);
	 i_WbMasterSim.WbRead(MMSTOP, ReadData);
	 `CHECK_EQUAL(ReadData, '0);
      end // UNMATCHED !!


      `TEST_CASE("consecutive_starts") begin
	 // let's queue 3 same commands - this will fill the queue
         i_WbMasterSim.WbWrite(MSTEPS, 192);
         i_WbMasterSim.WbWrite(MDIRECTION, 0);
         i_WbMasterSim.WbWrite(MLOWSPEED, 50);
         i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
         i_WbMasterSim.WbWrite(MACCRATE, 2000);
         i_WbMasterSim.WbWrite(MTRAIL, 10);
	 // enablemovtri/genirq - enable interrupt (bit3, hence integer=8)
	 // enablemovetrig=00 means all external triggers disabled and we run
	 // queued commands automatically
         i_WbMasterSim.WbWrite(MTRIGCFG, 8);
	 // powerconfig - enable move
         i_WbMasterSim.WbWrite(MPOWER, 1);

	 // queue 3 times into the commandqueue
         i_WbMasterSim.WbWrite(MTRG, 8);
         i_WbMasterSim.WbWrite(MTRG, 8);
         i_WbMasterSim.WbWrite(MTRG, 8);


	 // check if 3 commands are in the queue
	 i_WbMasterSim.WbRead(MIDENTIFIER, ReadData);
	 // queue should not be yet full, we have instantiated 4 records queue,
	 // but currently entered in only 3 records
         `CHECK_EQUAL(MRITEMS(ReadData), 3, "NOT QUEUED 3 records");
	 `CHECK_EQUAL(MRFULL(ReadData), 0);

	 // trigger startmove - first one
         i_WbMasterSim.WbWrite(MTRG, 1);

	 for(int i = 0; i < 3; i++) begin
	    // wait for some time and check if busy asserted
	    // 100ccs should be enough (process is quite slow)
	    ##100;
	    i_WbMasterSim.WbRead(MMSTATUS, ReadData);
            `CHECK_EQUAL(MRBUSY(ReadData), 1, "BUSY SIGNAL NOT ASSERTED");
	    // wait for IRQ to come, if not coming within a specific interval,
	    // we check out // commentunting position register
	    start = $time;

	    do begin
	       if ($rose(IRQ_o))
		 break;
	       ##1;
	    end
	    while ($time - start < 100ms);

	    if ($time-start >= 100ms)
              `CHECK_EQUAL(1, 0, "TIMEOUT - IRQ NOT RECEIVED");

	    // queue descending:
	    i_WbMasterSim.WbRead(MIDENTIFIER, ReadData);
            `CHECK_EQUAL(MRITEMS(ReadData), 2-i);
	    `CHECK_EQUAL(MRFULL(ReadData), 0);

	    i_WbMasterSim.WbRead(MPOSITION, ReadData);
            `CHECK_EQUAL(ReadData, unsigned'((32)'(192*(i+1))));
	    // and stepsleft should be restarted
	    i_WbMasterSim.WbRead(MSTEPSLEFT, ReadData);
            `CHECK_EQUAL(ReadData, 192);
	 end // for (int i = 0; i < 3; i++)
      end

      `TEST_CASE("IRQ_propagation") begin
         i_WbMasterSim.WbWrite(MSTEPS, 192);
         i_WbMasterSim.WbWrite(MDIRECTION, 0);
         i_WbMasterSim.WbWrite(MLOWSPEED, 50);
         i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
         i_WbMasterSim.WbWrite(MACCRATE, 2000);
         i_WbMasterSim.WbWrite(MTRAIL, 10);
	 // enablemovtri/genirq - enable interrupt (bit3, hence integer=8)
         i_WbMasterSim.WbWrite(MTRIGCFG, 8);
	 // powerconfig - enable move
         i_WbMasterSim.WbWrite(MPOWER, 1);

	 // trigger startmove
         i_WbMasterSim.WbWrite(MTRG, 1);
	 // wait for some time and check if busy asserted
	 // 100ccs should be enough (process is quite slow)
	 ##100;
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
         `CHECK_EQUAL(MRBUSY(ReadData), 1, "BUSY SIGNAL NOT ASSERTED");
	 // wait for IRQ to come, if not coming within a specific interval,
	 // we check out counting position register
	 start = $time;

	 do begin
	    if ($rose(IRQ_o))
	      break;
	    ##1;
	 end
	 while ($time - start < 100ms);

	 if ($time-start >= 100ms)
           `CHECK_EQUAL(1, 0, "TIMEOUT - IRQ NOT RECEIVED");


	 i_WbMasterSim.WbRead(MPOSITION, ReadData);
         `CHECK_EQUAL(ReadData, unsigned'((32)'(192)));
	 // and stepsleft should be restarted
	 i_WbMasterSim.WbRead(MSTEPSLEFT, ReadData);
         `CHECK_EQUAL(ReadData, 192);

      end

      `TEST_CASE("polling_busy_flag") begin
         i_WbMasterSim.WbWrite(MSTEPS, 123);
         i_WbMasterSim.WbWrite(MDIRECTION, 0);
         i_WbMasterSim.WbWrite(MLOWSPEED, 50);
         i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
         i_WbMasterSim.WbWrite(MACCRATE, 2000);
         i_WbMasterSim.WbWrite(MTRAIL, 10);
	 // enablemovtri/genirq
         i_WbMasterSim.WbWrite(MTRIGCFG, 0);
	 // powerconfig - enable move
         i_WbMasterSim.WbWrite(MPOWER, 1);

	 // trigger startmove
         i_WbMasterSim.WbWrite(MTRG, 1);
	 // wait for some time and check if busy asserted
	 // 100ccs should be enough (process is quite slow)
	 ##100;
	 i_WbMasterSim.WbRead(MMSTATUS, ReadData);
         `CHECK_EQUAL(MRBUSY(ReadData), 1, "BUSY SIGNAL NOT ASSERTED");
	 // poll busy flag
	 do
	    i_WbMasterSim.WbRead(MMSTATUS, ReadData);
	 while (MRBUSY(ReadData) == 1);
	 // we check out counting position register
	 i_WbMasterSim.WbRead(MPOSITION, ReadData);
         `CHECK_EQUAL(ReadData, unsigned'((32)'(123)));
	 // and stepsleft should be restarted
	 i_WbMasterSim.WbRead(MSTEPSLEFT, ReadData);
         `CHECK_EQUAL(ReadData, 123);
      end

      `TEST_CASE("catching_busy_flag") begin
	 // start motor and observe busy flag
         i_WbMasterSim.WbWrite(MSTEPS, 123);
         i_WbMasterSim.WbWrite(MDIRECTION, 0);
         i_WbMasterSim.WbWrite(MLOWSPEED, 50);
         i_WbMasterSim.WbWrite(MHIGHSPEED, 2000);
         i_WbMasterSim.WbWrite(MACCRATE, 2000);
         i_WbMasterSim.WbWrite(MTRAIL, 10);
	 // enablemovtri/genirq
         i_WbMasterSim.WbWrite(MTRIGCFG, 0);
	 // powerconfig - enable move
         i_WbMasterSim.WbWrite(MPOWER, 1);

	 // trigger startmove
         i_WbMasterSim.WbWrite(MTRG, 1);
	 // wait for some time and check if busy asserted
	 // 100ccs should be enough (process is quite slow)
	 ##100;
         `CHECK_EQUAL(Busy_o, 1, "BUSY SIGNAL NOT ASSERTED");
	 // poll busy flag by Wb:
	 do
	    i_WbMasterSim.WbRead(MMSTATUS, ReadData);
	 while (MRBUSY(ReadData) == 1);
	 // we check out counting position register
	 i_WbMasterSim.WbRead(MPOSITION, ReadData);
         `CHECK_EQUAL(ReadData, unsigned'((32)'(123)));
	 // and stepsleft should be restarted
	 i_WbMasterSim.WbRead(MSTEPSLEFT, ReadData);
         `CHECK_EQUAL(ReadData, 123);
	 // and busy out:
	 `CHECK_EQUAL(Busy_o, 0, "BUSY SIGNAL SHOULD NOT BE\
 ASSERTED");
      end

      `TEST_CASE("switching_matrix") begin
	 // set both switches to raw0, outputswitches should give both zero
	 // readings (and they are setup to zero switch). Bits are allocated as
	 // follows:
	 // <polarity1><rawswitchmatrix1><polarity0><rawswitchmatrix0>
	 mc.invec.RawSwitches_b2 = '0;
	 // POR: no switch connected, result should be zero:
	 ##1;
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b00);
	 // invert polarity on switch 0, hence switch 0 should be '1'. Note that
	 // switch 1 should stick still to zero as polarity switching happens
	 // _after_ switch selection. Switch1 is disabled:
         i_WbMasterSim.WbWrite(MSWITCHESCONFIG, 6'b0_01_1_01);
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b01);
	 // polarity reverse of second switch, disable switch0
         i_WbMasterSim.WbWrite(MSWITCHESCONFIG, 6'b1_01_1_00);
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b10);
	 // all polarities to normal, and flip switch setting, set input switch0
	 // to '1' and as it is casted to input0 of switch matrix, we should see it
	 mc.invec.RawSwitches_b2 = 2'b01;
	 // this takes some time due to debouncings
	 ##100;
         i_WbMasterSim.WbWrite(MSWITCHESCONFIG, 6'b0_01_0_01);
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b11);

	 // switch 1 goes to input 1 of the switch matrix:
         i_WbMasterSim.WbWrite(MSWITCHESCONFIG, 6'b0_10_0_01);
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b01);

	 // switch 0 goes to intpu 1 of the switch matrix:
         i_WbMasterSim.WbWrite(MSWITCHESCONFIG, 6'b0_10_0_10);
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b00);

	 // turn switch the signals
	 mc.invec.RawSwitches_b2 = 2'b10;
	 // this takes some time due to debouncings
	 ##100;
         `CHECK_EQUAL(ConfiguredSwitches_b2, 2'b11);
      end

      `TEST_CASE("registers_clearout") begin
	 // slowdown -
         i_WbMasterSim.WbWrite(MSLOWDOWN, 12);
	 i_WbMasterSim.WbRead(MSLOWDOWN, ReadData);
         `CHECK_EQUAL(ReadData, 12);
	 // stepsafterlimitswich
         i_WbMasterSim.WbWrite(MSALSP, 22);
	 i_WbMasterSim.WbRead(MSALSP, ReadData);
         `CHECK_EQUAL(ReadData, 22);
	 // stepsafterlimitswitch have some vector length, we verify that no
	 // larger than that is written into the register

         i_WbMasterSim.WbWrite(MSALSP, '1);
	 i_WbMasterSim.WbRead(MSALSP, ReadData);
	 // length of the vector is specified in MCPkg
         `CHECK_EQUAL(ReadData, 32'h3f);

	 // and now testing switches configuration and whether they really
	 // switch
         i_WbMasterSim.WbWrite(MSWITCHESCONFIG, '1);
	 i_WbMasterSim.WbRead(MSWITCHESCONFIG, ReadData);
	 // length of the vector is specified in MCPkg. Thing is, that we have 2
	 // switches, hence switching matrix requires 2 bits. Additional
	 // one bit is for inverted polarity. That makes 3 bits for single
	 // switch * 2 = 6 bits in total have to be useable, others are cleared away
         `CHECK_EQUAL(ReadData, 63);
      end

      `TEST_CASE("queue_full") begin
	 // fill queue
	 for(int i = 0; i < 5; i++)
           i_WbMasterSim.WbWrite(MTRG, 8);
	 i_WbMasterSim.WbRead(MIDENTIFIER, ReadData);
	 `CHECK_EQUAL(MRFULL(ReadData), 1);
	 `CHECK_EQUAL(MRITEMS(ReadData), 4);
      end

      `TEST_CASE("queueing_edge_sensitive") begin
	 // command to store data into queue:
         i_WbMasterSim.WbWrite(MTRG, 8);
	 // and the point here is, that this register should just edge trigger -
	 // so every write into that register should generate single trigger
	 // pulse. If not, it would overflow the cache immediatelly. Hence we
	 // leave this register filled and wait for 'some time' and if queue
	 // reports full, that means that it does not implement correctly level
	 // triggering
	 ##200;

	 i_WbMasterSim.WbRead(MIDENTIFIER, ReadData);
	 `CHECK_EQUAL(MRFULL(ReadData), 0);
	 `CHECK_EQUAL(MRITEMS(ReadData), 1);
      end

      `TEST_CASE("empty_queue_after_reset") begin
	 i_WbMasterSim.WbRead(MIDENTIFIER, ReadData);
	 `CHECK_EQUAL(MRITEMS(ReadData), 0);
	 `CHECK_EQUAL(MRFULL(ReadData), 0);
      end

      `TEST_CASE("registers_setting_and_readback") begin
	 // check readings against what was setup in common block

	 i_WbMasterSim.WbRead(MSTEPS, ReadData);
         `CHECK_EQUAL(ReadData, 123);
	 i_WbMasterSim.WbRead(MDIRECTION, ReadData);
         `CHECK_EQUAL(ReadData, 1);
	 i_WbMasterSim.WbRead(MLOWSPEED, ReadData);
         `CHECK_EQUAL(ReadData, 50);
	 i_WbMasterSim.WbRead(MHIGHSPEED, ReadData);
         `CHECK_EQUAL(ReadData, 2000);
	 i_WbMasterSim.WbRead(MACCRATE, ReadData);
         `CHECK_EQUAL(ReadData, 2000);
	 i_WbMasterSim.WbRead(MTRAIL, ReadData);
         `CHECK_EQUAL(ReadData, 10);
      end
   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(200s);

   wbmc #(
	  /*AUTOINSTPARAM*/
	  // Parameters
	  .QUEUELENGTH			(QUEUELENGTH),
	  .g_ModuleIdentifier		(g_ModuleIdentifier[3:0])) DUT
     (/*AUTOINST*/
      // Interfaces
      .mc				(mc),
      .Wb_iot				(Wb_iot),
      // Outputs
      .IRQ_o				(IRQ_o),
      .ConfiguredSwitches_b2		(ConfiguredSwitches_b2[1:0]),
      .SwitchesConfiguration_o		(SwitchesConfiguration_o[1:0]),
      .Busy_o				(Busy_o),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .reset_i				(reset_i),
      .OtherMotorSwitches_2b16		(OtherMotorSwitches_2b16/*[1:0][NUMBER_OF_MOTORS_PER_FIBER-1:0]*/),
      .triggers				(triggers));

   WbMasterSim
     #(/*AUTOINSTPARAM*/)
   i_WbMasterSim (
		  // Outputs
		  .Rst_orq		(ClkRs_ix.reset),
		  .Adr_obq32		(Wb_iot.Adr_b),
		  .Dat_obq32		(Wb_iot.DatMoSi_b),
		  .We_oq		(Wb_iot.We),
		  .Cyc_oq		(Wb_iot.Cyc),
		  .Stb_oq		(Wb_iot.Stb),
		  // Inputs
		  .Clk_ik		(ClkRs_ix.clk),
		  .Dat_ib32		(Wb_iot.DatMiSo_b),
		  .Ack_i		(Wb_iot.Ack));

   assign Wb_iot.Sel_b = 4'hf;

endmodule
