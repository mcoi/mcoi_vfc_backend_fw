//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) February 2018 CERN

//-----------------------------------------------------------------------------
// @file TB_PULSESYNC.SV
// @brief Verifies functionality of PulseSync from CORES library
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 21 February 2018
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

// Verifies functionality of PulseSync from CORES library
module tb_pulsesync;
   timeunit 1ns;
   timeprecision 100ps;

   localparam c_NumberOfPulses = 50;


   int 		      counter;
   int 		      fastclock, slowclock, waittime;


   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic		PulseOut_o;		// From DUT of PulseSync.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic		ClkIn_ik;		// To DUT of PulseSync.v
   logic		ClkOut_ik;		// To DUT of PulseSync.v
   logic		PulseIn_i;		// To DUT of PulseSync.v
   // End of automatics
   /*AUTOINOUTPARAM("PulseSync")*/

   task clockgen(input int fastclock, slowclock);
      $display("Using following clocks for simulation:");
      $display("Fast clock = %d ns", fastclock);
      $display("Slow clock = %d ns", slowclock);
      fork
	 forever begin: fastclockstuff
	    ClkIn_ik = 0;
	    #(fastclock * 1ns);
	    ClkIn_ik = 1;
	    #(fastclock * 1ns);
	 end
	 forever begin: slowclockstuff
	    ClkOut_ik = 0;
	    #(slowclock * 1ns);
	    ClkOut_ik = 1;
	    #(slowclock * 1ns);
	 end
      join
   endtask // clockgen

   task countpulses ();
      counter = 0;
      forever begin
	 @(posedge ClkOut_ik);
	 if (PulseOut_o)
	   counter++;
      end
   endtask // countpulses

   // generates single pulse in input domain
   task generatePulse(input int numpulses, input int clock_cycles=5);
      // reset
      PulseIn_i = 0;
      // wait for at least 10 clock cycles of both domains to be sure
      // that internal registers get into working state
      #100us;

      repeat(numpulses) begin
	 // sync in clock
	 @(posedge ClkIn_ik);
	 // generate single clock cycle pulse in fast domain
	 PulseIn_i = 1;
	 @(posedge ClkIn_ik);
	 PulseIn_i = 0;
	 // and now we have to wait before generating another
	 // pulse, that's because we're testing here single
	 // pulse propagation (and not length of the signal).
	 // point is, that it takes. Basically at the _end_
	 // of fast domain pulse generation we have to wait 3
	 // clock cycles to get the pulse into slow
	 // one. That's minimum amount of time between two
	 // pulses in fast domain, we allow uncertainly of
	 // 1cc, hence 4ccs
	 repeat(clock_cycles)
	   @(posedge ClkOut_ik);
      end // repeat (numpulses)
      // delay the end of simulation so signals can propagate to slow
      // domain if that's the case
      repeat(10) @(posedge ClkIn_ik);
   endtask // generatePulse

   task longpulse();
      begin
	 PulseIn_i = 0;
	 #100us;
	 @(posedge ClkIn_ik);
	 PulseIn_i = 1;
	 #100us;
	 @(posedge ClkIn_ik);
	 PulseIn_i = 0;
	 #100us;
      end
   endtask // longpulse



   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 // at the beginning generate two random numbers, which will
	 // be used to generate clocks
	 assert(std::randomize(fastclock, slowclock)
		with { fastclock inside {[10:600]};
	    slowclock inside {[601:5000]};});
      end

      `TEST_CASE("fast_to_slow_domain") begin
	 fork
	    // generate clock signals using random but constrained periods
	    clockgen(fastclock, slowclock);
	    // generate stimuli
	    generatePulse(c_NumberOfPulses);
	    // count the amount of pulses caught in slow domain
	    countpulses();
	 join_any
	 // check how many pulses we have done
	 $display("Counted %d pulses at the output", counter);
         `CHECK_EQUAL(counter, c_NumberOfPulses, "Wrong amount of\
 pulses counted");
      end

      `TEST_CASE("long_pulse_fast_to_slow") begin
	 // this should generate single clock cycle pulse in the other domain
	 fork
	    clockgen(fastclock, slowclock);
	    countpulses();

	    longpulse();
	 join_any
	 `CHECK_EQUAL(counter, 1, "ONLY SINGLE PULSE EXPECTED");
      end // UNMATCHED !!

      `TEST_CASE("long_pulse_slow_to_fast") begin
	 // this should generate single clock cycle pulse in the other domain
	 fork
	    clockgen(slowclock, slowclock);
	    countpulses();
	    longpulse();
	 join_any
	 `CHECK_EQUAL(counter, 1, "ONLY SINGLE PULSE EXPECTED");
      end // UNMATCHED !!

      `TEST_CASE("slow_to_fast_domain") begin
	 // this is somewhat fishy - if going that direction, the
	 // pulse syncer will require at least 4 clock cycles in slow
	 // domain with respect to fast one to synchronize. This is
	 // because 1cc for setting flip flop, 2ccs in fast to
	 // propagate to fast domain, from fast another 2 ccs in slow
	 // domain to generate reset of flip + additional one to
	 // clearout the rsff and generate output pulse. If slowclock
	 // is faster than this, it will not work. Note that we need
	 // at least 4 in slow + 2 in fast domain. So we have to
	 // generate proper clocking
	 waittime = int'(real'(slowclock) /
			 real'(fastclock) * 5.0) + 10;

	 $display("Wait time of %d fast cycles between two pulses",
		  waittime);

	 fork
	    // generate clock signals using random but constrained periods
	    clockgen(slowclock, fastclock);
	    // generate stimuli, we WAIT MUCH LONGER HERE. Generally 5
	    // slow + 2 fast cycles, counter here is in fast cycles,
	    // so we take 10 fast cycles + 5 slow and recalculate
	    generatePulse(c_NumberOfPulses,
			  waittime);
	    // count the amount of pulses caught in slow domain
	    countpulses();
	 join_any
	 // check how many pulses we have done
	 $display("Counted %d pulses at the output", counter);
         `CHECK_EQUAL(counter, c_NumberOfPulses, "Wrong amount of\
 pulses counted");
      end
   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(100000ms);

   PulseSync #(/*AUTOINSTPARAM*/) DUT
     (.*);

endmodule
