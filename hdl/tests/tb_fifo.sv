//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) November 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_FIFO.SV
// @brief Fifo functionality
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 14 November 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

// Fifo functionality
module tb_fifo;
   timeunit 1ns;
   timeprecision 100ps;

   // bus width of mem address inside fifo
   localparam g_FifoWidth = 4;
   // data bus
   localparam g_DataWidth = 32;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic		FifoEmpty_o;		// From DUT of fifo.v
   logic		FifoFull_o;		// From DUT of fifo.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic		FifoReset_i;		// To DUT of fifo.v
   // End of automatics
   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 FifoReset_i = 0;

	 ClkRs_ix.reset = 1;
	 ##10;
	 ClkRs_ix.reset = 0;

      end

      `TEST_CASE("debug_test") begin
	 // this tests the assertions on read and write counter in
	 // fifo. These count if write and read commands happen in
	 // some structured manner. We need to check if overflowing is
	 // handled correctly. THESE OPERATIONS SHOULD NOT TRIGGER THE
	 // ASSERTION IN FIFO.SV
	 wdata_ix.data = 32'heeffccaa;

	 wdata_ix.enable = 1;
	 ##10;
	 wdata_ix.enable = 0;
	 rdata_ox.enable = 1;
	 #5;
	 // we have shifted pointers such, that there won't be any
	 // overflow and we will reach end of counting
	 wdata_ix.enable = 1;
	 ##10;
	 // now the write should trigger if something goes
	 // wrong. Let's do the same thing with read:
	 rdata_ox.enable = 1;
	 ##20;
	 rdata_ox.enable = 0;
	 wdata_ix.enable = 1;
	 ##10;
	 wdata_ix.enable = 0;
	 rdata_ox.enable = 1;
	 ##10;
      end // UNMATCHED !!

      `TEST_CASE("ABVs_validity") begin
	 // this tests the assertions on read and write counter in
	 // fifo. These count if write and read commands happen in
	 // some structured manner. We need to check if overflowing is
	 // handled correctly. THESE OPERATIONS SHOULD NOT TRIGGER THE
	 // ASSERTION IN FIFO.SV
	 wdata_ix.data = 32'heeffccaa;

	 wdata_ix.enable = 1;
	 ##10;
	 wdata_ix.enable = 0;
	 rdata_ox.enable = 1;
	 #5;
	 // we have shifted pointers such, that there won't be any
	 // overflow and we will reach end of counting
	 wdata_ix.enable = 1;
	 ##10;
	 // now the write should trigger if something goes
	 // wrong. Let's do the same thing with read:
	 rdata_ox.enable = 1;
	 ##20;
	 rdata_ox.enable = 0;
	 wdata_ix.enable = 1;
	 ##10;
	 wdata_ix.enable = 0;
	 rdata_ox.enable = 1;
	 ##10;
      end // UNMATCHED !!

      `TEST_CASE("reset") begin
	 wdata_ix.data = 32'heeffccaa;

	 wdata_ix.enable = 1;
	 ##10;
	 wdata_ix.enable = 0;
	 `CHECK_EQUAL(FifoEmpty_o,0, "FIFO IS NOT EMPTY, BUT FLAG RAISED");
	 `CHECK_EQUAL(FifoFull_o, 0, "FIFO IS NOT YET FULL");
	 ##10;
	 FifoReset_i = 1;
	 ##1;
	 FifoReset_i = 0;
	 `CHECK_EQUAL(FifoEmpty_o, 1, "RESET DID NOT WORK");
	 `CHECK_EQUAL(FifoFull_o, 0, "RESET DIDT NOT WORK");
	 ##10;
	 `CHECK_EQUAL(FifoEmpty_o, 1, "RESET DID NOT WORK");
	 `CHECK_EQUAL(FifoFull_o, 0, "RESET DIDT NOT WORK");

      end // UNMATCHED !!

      `TEST_CASE("steady_output") begin
	 wdata_ix.data = 32'heeffccaa;
	 wdata_ix.enable = 1;
	 ##1;
	 wdata_ix.enable = 0;
	 `CHECK_EQUAL(rdata_ox.data, 32'heeffccaa, "OUTPUT DOES NOT MATCH");
	 // add one more sample and READ OUTPUT should not change
	 wdata_ix.data = 32'h55aa55aa;
	 wdata_ix.enable = 1;
	 ##1;
	 wdata_ix.enable = 0;
	 `CHECK_EQUAL(rdata_ox.data, 32'heeffccaa, "OUTPUT DOES NOT MATCH");
	 // pulse one read out and output should change
	 rdata_ox.enable = 1;
	 ##1;
	 `CHECK_EQUAL(rdata_ox.data, 32'h55aa55aa, "OUTPUT DOES NOT MATCH");
	 ##1;
	 // and output goes 'xx' when we try to read past fifo
	 `CHECK_EQUAL(rdata_ox.data, 32'('x), "OUTPUT DOES NOT MATCH");
      end // UNMATCHED !!

      `TEST_CASE("underreading") begin
	 `CHECK_EQUAL(FifoEmpty_o, 1, "EMPTY SHOULD BE 1 AT POR");
	 rdata_ox.enable = 1;
	 `CHECK_EQUAL(FifoEmpty_o, 1, "EMPTY SHOULD STAY");
	 ##10;
	 `CHECK_EQUAL(FifoEmpty_o, 1, "EMPTY SHOULD STAY");
      end

      `TEST_CASE("pulling_out_data") begin
	 // do write pulse
	 wdata_ix.enable = 1;
	 for(int i=0;i< 27;i++) begin
	    wdata_ix.data = i+10;
	    ##1;
	 end;
	 wdata_ix.enable = 0;
	 `CHECK_EQUAL(rdata_ox.data, 10, "FIFO READOUT WRONG");
	 // and now we read out the values one by one
	 ##1;
	 `CHECK_EQUAL(rdata_ox.data, 10, "FIFO READOUT WRONG");
	 // here we express interest to get new data
	 rdata_ox.enable = 1;
	 // at this cycle the intent of the data readout gets acknowledged and
	 // should be pulled out consecutively
	 for(int i=0; i < 17; i++) begin
	    `CHECK_EQUAL(rdata_ox.data, 10+i, "FIFO READOUT WRONG");
	    ##1;
	    if (FifoEmpty_o) break;
	 end
      end

      `TEST_CASE("fifo_single_pulse_readout") begin
	 // do write pulse
	 wdata_ix.enable = 1;
	 for(int i=0;i< 27;i++) begin
	    wdata_ix.data = i+10;
	    ##1;
	 end;
	 wdata_ix.enable = 0;
	 `CHECK_EQUAL(rdata_ox.data, 10, "FIFO READOUT WRONG");
	 // and now we read out the values one by one
	 ##1;
	 `CHECK_EQUAL(rdata_ox.data, 10, "FIFO READOUT WRONG");
	 // here we express interest to get new data
	 rdata_ox.enable = 1;
	 $display("%d" , rdata_ox.data);
	 `CHECK_EQUAL(rdata_ox.data, 10, "FIFO READOUT WRONG");
	 ##1;
	 rdata_ox.enable = 0;
	 // and at this point the new data should be available
	 `CHECK_EQUAL(rdata_ox.data, 11, "FIFO READOUT WRONG");
      end

      `TEST_CASE("fifo_full_drops_data") begin
	 // do write pulse
	 wdata_ix.enable = 1;
	 for(int i=0;i< 27;i++) begin
	    wdata_ix.data = i+10;
	    ##1;
	 end;
	 `CHECK_EQUAL(FifoEmpty_o, 0, "FIFO NOT EMPTY");
	 `CHECK_EQUAL(FifoFull_o, 1, "FIFO IS FULL");
	 // when feeding full fifo, samples should be dropped
	 `CHECK_EQUAL(rdata_ox.data, 10, "FIFO IS OVERWRITTEN");
      end

      `TEST_CASE("fifo_full_flag") begin
	 wdata_ix.data = 32'haabbccdd;
	 // do write pulse
	 wdata_ix.enable = 1;
	 ##15;
	 wdata_ix.enable = 0;
	 // wait 1cc more
	 ##10;
	 `CHECK_EQUAL(FifoFull_o, 0, "FIFO SHOULD NOT BE FULL YET");
	 wdata_ix.enable = 1;
	 ##1;
	 wdata_ix.enable = 0;
	 `CHECK_EQUAL(FifoFull_o, 1, "FIFO SHOULD BE FULL");
	 `CHECK_EQUAL(FifoEmpty_o, 0, "FIFO IS NOT EMPTY");
      end

      `TEST_CASE("first_data_write") begin
	 wdata_ix.data = 32'haabbccdd;
	 // do write pulse
	 wdata_ix.enable = 1;
	 ##1;
	 wdata_ix.enable = 0;
	 // wait 1cc more
	 ##1;
	 `CHECK_EQUAL(FifoEmpty_o, 0, "FIFO SHOULD NOT BE EMPTY");

	 `CHECK_EQUAL(rdata_ox.data, 32'haabbccdd, "FIFO SHOULD SHOW FIRST VALUE");
      end

      `TEST_CASE("fifoempty_clearout_when_data") begin
	 wdata_ix.data = '0;
	 wdata_ix.enable = 1;
	 ##1;
	 wdata_ix.enable = 0;
	 ##10;
	 `CHECK_EQUAL(FifoEmpty_o, 0, "FIFO SHOULD NOT BE EMPTY");
      end

      `TEST_CASE("fifo_empty_after_reset") begin
	 `CHECK_EQUAL(FifoEmpty_o, 1, "FIFO NOT EMPTY AFTER RESET");
      end
   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10ms);

   data_x #(.g_DataWidth(g_DataWidth)) wdata_ix(.ClkRs_ix(ClkRs_ix));
   data_x #(.g_DataWidth(g_DataWidth)) rdata_ox(.ClkRs_ix(ClkRs_ix));


   fifo #(/*AUTOINSTPARAM*/
	  // Parameters
	  .g_FifoWidth			(g_FifoWidth)) DUT
     (.*);

endmodule
