//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) November 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_FOURMCLINKS.SV
// @brief Tests connectivity of 4 links to a single wb iface
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 07 November 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;
import MCPkg::*;


// Tests connectivity of 4 links to a single wb iface
module tb_fourmclinks;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   localparam QUEUELENGTH = 64;
   localparam g_DebouncingCounterWidth = 4;
   localparam g_SynchronizationFlips = 2;
   localparam g_FrameSize = 80;
   localparam g_AddressWidth = 32;
   localparam g_fourlinksAddress = 25'hc00000;
   localparam g_DataWidth = 32;


   ckrs_t ClkRs_ix, rClkRs_ix, wClkRs_ix;
   triggers_t triggers;

   t_WbInterface #(.g_DataWidth(g_DataWidth),
		   .g_AddressWidth(g_AddressWidth)) Wb_iot(ClkRs_ix.clk, ClkRs_ix.reset);
   logic [g_DataWidth-1:0] ReadData;
   int 			   x;


   // function recalculating motor control unit number into physical address
   function [g_AddressWidth-1:0] getBaseAddr (input int motor, gbtlink);
      getBaseAddr = g_fourlinksAddress +
		    (motor * 2 * NUMBER_OF_MOTORS_PER_FIBER) +
		    gbtlink * $pow(2, MCLINK_WB_ADDRESS_WIDTH);
   endfunction // getBaseAddr

   logic [31:0] 	   basereg;

   task Write;
      input int gbtlink, motor, register;
      input [31:0] Data_ib32;
      begin
	 // base address is combination of which motor, register of motor, total
	 // base address and number of GBT link (all zero indexed)
	 basereg = getBaseAddr(motor, gbtlink) +
		   register;

	 i_WbMasterSim.WbWrite(basereg, Data_ib32);
	 $display("Writing data: %h -> %h", basereg, Data_ib32);
      end
   endtask // Write

   task Read;
      input int gbtlink, motor, register;
      output logic [31:0] data;

      begin
	 basereg = getBaseAddr(motor, gbtlink) +
		   register;

	 i_WbMasterSim.WbRead(basereg, data);
	 $display("Reading data: %h -> %h", basereg, data);
      end
   endtask // Read


   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic		Busy_o;			// From DUT of fourmclinks.v
   logic [$clog2(NUMBER_OF_GBT_LINKS)+NUMBER_OF_MOTORS_PER_FIBER-1:0] IRQ_ob;// From DUT of fourmclinks.v
   logic [NUMBER_OF_GBT_LINKS-1:0] Overflow_ob;	// From DUT of fourmclinks.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic		ResetOverflow_i;	// To DUT of fourmclinks.v
   // End of automatics

   logic [NUMBER_OF_GBT_LINKS-1:0][NUMBER_OF_MOTORS_PER_FIBER-1:0]
				  Busy_b;

   always forever #(25ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking
   // TX GBT clocks
   always forever #(25ns) wClkRs_ix.clk <= ~wClkRs_ix.clk;
   // RX GBT clock
   always forever #(25ns) rClkRs_ix.clk <= ~rClkRs_ix.clk;

   // interfaces - !! NOTE THAT THIS INTERFACE USES CLOCK FROM writer (GBTX tx clock)
   data_x #(.g_DataWidth(g_FrameSize)) packet_ox[NUMBER_OF_GBT_LINKS-1:0] (.ClkRs_ix(wClkRs_ix));
   // switches interface - direction from GEFE to VFC
   data_x #(.g_DataWidth(g_FrameSize)) packet_ix[NUMBER_OF_GBT_LINKS-1:0] (.ClkRs_ix(rClkRs_ix));

   // mask to detect missing IRQs
   logic [NUMBER_OF_MOTORS_PER_FIBER-1:0] IRQmask_b
					  [NUMBER_OF_GBT_LINKS-1:0];
   logic 				  Alldone;
   logic [NUMBER_OF_MOTORS_PER_FIBER*NUMBER_OF_GBT_LINKS-1:0] cast;

   // dirty hack to generate that all IRQs came
   always_comb begin
      cast = {>>{IRQmask_b}};
      Alldone = &cast;
   end


   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 `CHECK_EQUAL(NUMBER_OF_GBT_LINKS, 4, "REPAIR CODE BELOW AS IT\
 IS NOT GENERIC")
	 packet_ix[0].data = 0;
	 packet_ix[1].data = 0;
	 packet_ix[2].data = 0;
	 packet_ix[3].data = 0;

	 IRQmask_b = '{NUMBER_OF_GBT_LINKS{'0}};

         i_WbMasterSim.Reset();
	 // and reset as well the other two domains (otherwise FIFOs will bail
	 // out)
	 wClkRs_ix.reset = '1;
	 rClkRs_ix.reset = '1;
	 ResetOverflow_i = 0;
	 triggers.TriggerEnable_i = 1;
	 #(200ns);
	 @(posedge wClkRs_ix.clk);
	 wClkRs_ix.reset = '0;
	 rClkRs_ix.reset = '0;
	 // beware - if putting more delay, some tests will fail because after
	 // reset FIFO is setting up its 'new value' and we want to catch
	 // it. Too long delay, pulse is gone and then POR_value test will fail.
	 #(200ns);
	 `CHECK_EQUAL(Busy_o, 0, "BUSY NOT RESET");

      end

      `TEST_CASE("irq_arbiter_catch") begin
	 // we fire all motors of all links almost at the same time,
	 // WITH IRQ enabled,

	 fork
	    for(int link = 0; link < NUMBER_OF_GBT_LINKS; link++)
	      for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
		 // setup a single move
		 Write(link, i, 0, 2);
		 // direction is only bit[0], we cast there the number and check
		 Write(link, i, 1, 1);
		 Write(link, i, 2, 50);
		 Write(link, i, 3, 150000);
		 Write(link, i, 4, 150000);
		 Write(link, i, 5, 50000);
		 // enable IRQ for each of them
		 Write(link, i, 6, 8);
		 // powerconfig - enable move
		 Write(link, i, 7, 1);
		 // trigger startmove
		 Write(link, i, 8, 1);
		 #100ns;

		 `CHECK_EQUAL(Busy_o, 1, "BUSY NOT RISEN");

	      end // for (int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++)

	    begin
	       forever begin
		  // we're in position of catching the IRQs, now let's do
		  // following: all the motors were fired, we should get
		  // totally 64 IRQs in 4 links, each IRQ received can be used
		  // as a MASK to mask empty array of vectors, if all
		  // vectors are caught, its the end
		  if (IRQ_ob)
		    // got IRQ in this cycle, use it to mask the links
		    IRQmask_b[IRQ_ob[17:16]] |= IRQ_ob;
		  if ( Alldone )
		    break;
		  ##1;
	       end // forever begin
	    end
	 join;

	 Busy_b = '0;

	 for(x = 0; x < 5000; x++) begin
	    // follows testing of BUSY assertion - read all the links
	    // through wishbone
	    for(int link=0; link < NUMBER_OF_GBT_LINKS; link++)
	      for (int i=0; i < NUMBER_OF_GBT_LINKS; i++) begin
		 Read(link, i, 15, ReadData);
		 Busy_b[link][i] = ReadData[0];
	      end
	    // collapse global busy to see if the exported one gives
	    // sense
	    `CHECK_EQUAL(Busy_o, |Busy_b, "BUSY FUNCTIONING");
	    if (~|Busy_b) break;
	    ##100;

	 end // for (x = 0; x < 5000; x++)
	 assert (x < 5000) else
	   $error("TIMEOUT");

      end

      `TEST_CASE("reading_links_identifiers") begin
	 // we try to read identifiers of all 4 interfaces
	 // gbt=0, motor=16 is access to mclink iface, reg0=identifier
	 for(int link = 0; link < 4; link++) begin
	    Read(link, 16, 0, ReadData);
            `CHECK_EQUAL(ReadData, link+10);
	    `CHECK_EQUAL(Busy_o, 0, "BUSY NOT DEASSERTED");
	 end
      end

      `TEST_CASE("debug_test") begin
	 // we try to read identifiers of all 4 interfaces
	 // gbt=0, motor=16 is access to mclink iface, reg0=identifier
	 for(int link = 0; link < 4; link++) begin
	    Read(link, 16, 0, ReadData);
            `CHECK_EQUAL(ReadData, link+10);
	 end
      end
   end;

   // instantiate wishbone interface
   WbMasterSim
     i_WbMasterSim (// Outputs
		    .Rst_orq		(ClkRs_ix.reset),
		    .Adr_obq32		(Wb_iot.Adr_b),
		    .Dat_obq32		(Wb_iot.DatMoSi_b),
		    .We_oq		(Wb_iot.We),
		    .Cyc_oq		(Wb_iot.Cyc),
		    .Stb_oq		(Wb_iot.Stb),
		    // Inputs
		    .Clk_ik		(ClkRs_ix.clk),
		    .Dat_ib32		(Wb_iot.DatMiSo_b),
		    .Ack_i		(Wb_iot.Ack));

   assign Wb_iot.Sel_b = 4'hf;
   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(100000ms);

   fourmclinks #(/*AUTOINSTPARAM*/
		 // Parameters
		 .QUEUELENGTH		(QUEUELENGTH),
		 .g_FrameSize		(g_FrameSize),
		 .g_DataWidth		(g_DataWidth),
		 .g_AddressWidth	(g_AddressWidth),
		 .g_fourlinksAddress	(g_fourlinksAddress)) DUT
     (.*);

endmodule
