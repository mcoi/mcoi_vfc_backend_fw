//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) November 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_MCOI.SV
// @brief Top-level testbench,
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 03 November 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"


import CKRSPkg::*;
import MCPkg::*;
import constants::*;
import VmeAddressMap::*;

// Top-level testbench,
module tb_mcoi;
   timeunit 1ns;
   timeprecision 100ps;
   //=======================================  Declarations  =====================================\\

   //==== Local parameters ====\\

   localparam c_VmeAccessVerbose = 1'b1;
   localparam g_NSlots = 20;
   localparam g_VerboseDefault = 1;
   localparam g_Timeout = 132000;

   // Base Address:
   localparam c_VfcSlot4BaseAddress = 32'h0400_0000;  // Comment: The address is for now fixed

   // application info vector check
   localparam int checkVector[0:10] = '{32'h5646432d, 32'h4d434f49, 32'h2d445242, 32'h00000000, 32'h00000000,
					32'h00000000, 32'h00000000, 32'h00000000, 32'h00000000, 32'h00000000,
					32'h00000000};


   // Application Memory Addresses:
   // Comment: Address offset of the Aplication
   localparam c_AppAddrOffset     = 32'h0080_0000;
   // Address offset of link interfaces
   localparam c_AppMCLinkInterfaceOffset = 32'h0000_8000;
   // address offset of the global block of registers in app space
   localparam c_AppGlobalRegisters = 32'h0001_0000;

   logic 	  Osc25MhzCg_ik = '0;

   logic [3:0] 	  PCBrevision_i4;

   wire 	  FmcClkBidir2Cq_iokn;
   wire 	  FmcClkBidir2Cq_iokp;
   wire 	  FmcClkBidir3Qg_iokn;
   wire 	  FmcClkBidir3Qg_iokp;
   wire 	  GbtxI2cScl_io;
   wire 	  GbtxI2cSda_io;
   wire [23:0] 	  FmcHa_iob24n;
   wire [23:0] 	  FmcHa_iob24p;
   wire [21:0] 	  FmcHb_iob22n;
   wire [21:0] 	  FmcHb_iob22p;
   wire [33:0] 	  FmcLa_iob34n;
   wire [33:0] 	  FmcLa_iob34p;
   wire [0:9] 	  FmcDpC2m_iob10n;
   wire [0:9] 	  FmcDpC2m_iob10p;
   wire [9:0] 	  FmcDpC2m_ob10;
   wire [0:9] 	  FmcDpM2c_iob10n;
   wire [0:9] 	  FmcDpM2c_iob10p;
   wire 	  FmcScl_io;
   wire 	  FmcTdi_i;
   wire 	  MmcxClkIoCg_iokn;
   wire 	  MmcxClkIoCg_iokp;
   wire [0:3] 	  MmcxGpIoQg_iokb4;
   wire 	  LemoGpioDir_o;
   wire 	  LemoGpioQg_iok;
   wire [12:0] 	  BoardIdConn_iob13;
   wire [12:0]		GpioConnA_iob13;
   wire [23:0]		GpioConnB_iob24;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic		AppSfpTx1_ob;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		AppSfpTx2_ob;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		AppSfpTx3_ob;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		AppSfpTx4_ob;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcGa0_o;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcGa1_o;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcPgC2m_o;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcTck_ok;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcTdi_o;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcTms_o;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcTrstL_orn;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		GbtxReset_or;		// From i_GefeApplication of GefeApplication.v
   logic		GbtxTxDataValid_o;	// From i_GefeApplication of GefeApplication.v
   logic [0:5]		Leds_onb6;		// From i_GefeApplication of GefeApplication.v
   logic [31:0]		build_ob32;		// From i_build_number of build_number.v
   mcoutput_t [NUMBER_OF_MOTORS_PER_FIBER:1] motorControl_ob;// From i_reverse_pin_mapping of reverse_pin_mapping.v
   logic		mreset_o;		// From i_reverse_pin_mapping of reverse_pin_mapping.v
   logic [7:0]		test_ob8;		// From i_reverse_pin_mapping of reverse_pin_mapping.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic		AppSfpRx1_ib;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		AppSfpRx2_ib;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		AppSfpRx3_ib;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		AppSfpRx4_ib;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic [8:1]		DipSw_ib8;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcClk0M2cCmos_ik;	// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcClk1M2cCmos_ik;	// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcClkDir_i;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic [9:0]		FmcDpM2c_ib10;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcGbtClk0M2c_ik;	// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcGbtClk1M2c_ik;	// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcPgM2c_i;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcPrsntM2c_in;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		FmcTdo_i;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   logic		GbtxRxRdy_i;		// To i_GefeApplication of GefeApplication.v
   logic		GbtxTxRdy_i;		// To i_GefeApplication of GefeApplication.v
   logic		GeneralReset_iran;	// To i_GefeApplication of GefeApplication.v
   logic		PushButton_i;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   mcinput_t [NUMBER_OF_MOTORS_PER_FIBER:1] motorStatus_ib;// To i_reverse_pin_mapping of reverse_pin_mapping.v
   // End of automatics

   wire 	  FmcClk2Bidir_iok;	// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  FmcClk3Bidir_iok;	// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  FmcScl_iok;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  FmcSda_io;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [23:0] 	  FmcHaN_iob24;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [23:0] 	  FmcHaP_iob24;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [21:0] 	  FmcHbN_iob22;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [21:0] 	  FmcHbP_iob22;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [33:0] 	  FmcLaN_iob34;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [33:0] 	  FmcLaP_iob34;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [4:0] 	  Ga_ionb5 [g_NSlots:1];		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  Gap_ion;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [4:1] 	  GpIoLemo_iob4;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  SysClk_ik;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  SysResetN_irn;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  IackIn_in;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  Iack_in;		// To i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  IackOut_on;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [7:1] 	  Irq_onb7;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  DtAck_on;		// From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [31:0] 	  D_iob32;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  Wr_in;			// To i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  As_in;			// To i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [5:0] 	  AM_ib6;			// To i_VfcHdSlot1 of VfcHd_v3_0.v
   wire 	  LWord_io;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [31:1] 	  A_iob31;		// To/From i_VfcHdSlot1 of VfcHd_v3_0.v
   wire [1:0] 	  Ds_inb2;		// To i_VfcHdSlot1 of VfcHd_v3_0.v

   wire [20:1] 	  Gap_nbm;
   logic 	  fook;
   int 		  counter, swa;
   logic [31:0]   LastVmeReadData_b32, Q_b32[$];
   logic [7:0] 	  LastVmeAccessExitCode_b8;

   assign GpIoLemo_iob4[1] = fook;

   logic [31:0]   data [NUMBER_OF_GBT_LINKS:1];
   logic [31:0]   tmp;
   logic [31:0]   queues[NUMBER_OF_GBT_LINKS:1][$];
   logic 	  datapush;
   semaphore 	  queuelock = new(1);
   int 		  diode;
   logic [17:0]   irq_sequence[$] = {18'h0_0001};

   int 		  countpos, totalcount, countneg;




   // forces signals from scec channel1 such, that it looks like GEFE
   // loopback was correctly done:
   task ForceScEcLoopback();
      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.linkgenerator[1].scecgenerator[1].i_serial_scec0.data_ob32
	  = GEFE_INTERLOCK;
      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.linkgenerator[2].scecgenerator[1].i_serial_scec0.data_ob32
	  = GEFE_INTERLOCK;
      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.linkgenerator[3].scecgenerator[1].i_serial_scec0.data_ob32
	  = GEFE_INTERLOCK;
      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.linkgenerator[4].scecgenerator[1].i_serial_scec0.data_ob32
	  = GEFE_INTERLOCK;
   endtask // ForceScEcLoopback

   // following task initializes SCEC and waits for it to LOCK. This
   // only works if tentire puzzle fits together: VFC + GBT link +
   // GEFE application part has to respond on link1:
   task ScEcLock(input integer channel = 0);
      // INIT SCEC:
      $display("Initialization of ScEc link %d", channel);
      // WAIT FOR SCEC TO LOCK, THIS TAKES ROUGHLY 600us OF
      // SIMULATION TIME:
      $display("ADDRESS: %x", GEFESERIAL0STAT + 4*channel);

      Read(GEFESERIAL0STAT + 4*channel);
      `CHECK_EQUAL(RXSLOL(LastVmeReadData_b32), 15,
		   "RXLOL invalid");
      $display("Waiting ScEc to lock");
      // here we check only channel 0 to lock. channel1 should happen
      // exactly at the same time:
      fork: myblock
	 @(posedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.
	   linkgenerator[1].scecgenerator[0].i_serial_scec0.SerialLinkUp);
	 #1ms;
      join_any
      `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.
		   linkgenerator[1].scecgenerator[0].
		   i_serial_scec0.SerialLinkUp,
		   1);

      $display("Link is UP, waiting for initial data");
      // and verify if passed through VME into particular channel:
      Read(GEFESERIAL0STAT + 4*channel);
      `CHECK_EQUAL(RXSERIALUP(LastVmeReadData_b32), 15,
		   "Timeout during locking serial_register");
      `CHECK_EQUAL(RXSLOCKED(LastVmeReadData_b32), 15,
		   "Timeout during locking serial_register");
      // now we have to wait for first sample to come - this happens
      // after reset and it shows first valid value
      waitScEcData(channel);
      $display("ScEc channel %d initialized", channel);
   endtask // ScEcLock

   // task waiting for internal variable, which says when new data
   // have arrived to ScEc channel. This information is NOT available
   // through the VFC vme, but that's not a big deal as reading
   // through VME is usually quite slow, hence there's enough waiting
   // time between two consecutive VME operations to do the job of
   // updating new sample. If that's found incorrect, we might in the
   // future make for example a small counter which counts up any time
   // there's scec update and export this one to VME to indicate that
   // data has changed.
   task automatic waitScEcData(input integer channel = 0);
      // hell long path:
      $display("Waiting new ScEc data to come on channel %d",
   channel);
      fork
	 begin
	    if (channel == 0)
	      @(posedge
		i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.
		linkgenerator[1].scecgenerator[0].i_serial_scec0.newdata_o);
	    else
	      @(posedge
		i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.
		linkgenerator[1].scecgenerator[1].i_serial_scec0.newdata_o);
	 end // fork begin
	 #5ms;
      join_any;

      if (channel == 0) begin
	`CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.
		linkgenerator[1].scecgenerator[0].i_serial_scec0.newdata_o,
		     1, "NEW DATA DID NOT COME");
      end else begin
	`CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.
		linkgenerator[1].scecgenerator[1].i_serial_scec0.newdata_o,
		     1, "NEW DATA DID NOT COME");
      end

   endtask // waitScEcData



   // this task test the behaviour of leds for link channel when an
   // IRQ is issued. IRQ serves as blanking signal and hence if it
   // appears, there should be 18 consecutive '1' values (at least)
   // when this happens. This is controller here, and must work even
   // if this signal overlaps with correct basic period of led blinker
   task automatic checkLED(input int led);
     $display("Checking led output %d", led);
      // first we have to detect posiedge within an interval:
      fork
	 #1us;
	 @(posedge
   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[led]);
      join_any
      #1;
      `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[led],
		   1,
		   "LED SHOULD TURN ON");
      // if on, then we have to check that at least LED_IRQ_MKO_VALUE
      // consecutive is on. That's because this signal is superimposed
      // on standard LED blinking and if present/los is present, led
      // pulses. If we hit the pulsing, then those signals mix
      // together. It is assured by LED test to make all SFPs such,
      // that no LED signal is generated
      repeat(LED_IRQ_MKO_VALUE) begin
	 clkix();
	`CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[led],
		     1,
		     "LED SHOULD BE CONTINUOUSLY ON");
      end
      // we must not test falling edge because it might not come if
      // the signal is mixed with error signal
   endtask // checkLED

   task automatic setupMotor(input integer link,
			     input integer motor,
			     input integer steps = 2);
      $display("Configuring motor L%dM%d", link, motor);
      Write(getAddr(link, motor, MSTEPS), steps);
      Write(getAddr(link, motor, MDIRECTION), 1);
      Write(getAddr(link, motor, MLOWSPEED), 1500000);
      Write(getAddr(link, motor, MHIGHSPEED),2000000);
      Write(getAddr(link, motor, MACCRATE),   500000);
      Write(getAddr(link, motor, MTRAIL), 10);
      // enable IRQ for each of them, in addition enable both int and ext
      // starts
      Write(getAddr(link, motor, MTRIGCFG), 8 + 1 + 2);
      // powerconfig - enable move
      Write(getAddr(link, motor, MPOWER), 1);
      // verify switches configuration, if they are not at zero, we
      // will configure them to be, otherwise motor does not start
      Read(getAddr(link, motor, MRAW_SW));
      // BTW: they should NOT be at zero, as all optical links return
      // correct 'normally open' data, link1 is connected to gefe,
      // other links are returning dummy switches data
      $display("Status register where switches report: 0x%.8x",
	       LastVmeReadData_b32);

      $display("Switches currently configured as: 0x%x",
	       MRSW(LastVmeReadData_b32));
      // MRSW - 2:1 range l1m1 motor switch. Range is same for all
      // motors, switches MUST be at zero as if they are not
      // configured, they are not connected
      `CHECK_EQUAL(MRSW(LastVmeReadData_b32), 0,
		   "Something is wrong with switches");
      // write switches configuration such, that one to one mapping is
      // achieved (raw0 = logic0, raw1=logic1). No inversion.
      $display("Configuring 1:1 mapping of switches with no inversion");
      // following sets motors switches:
      Write(getAddr(link, motor, MSWITCHESCONFIG), 32'b0_10_0_01);
      // after reloading switches config read the status again
      Read(getAddr(link, motor, MRAW_SW));

      // if both switches are on '1', let's invert them. This happens
      // when gefe connected. Other motors have direct loopback, which
      // means that switches are set to '0'. This is mandatory to
      // setup otherwise motor will not move, thinking that we've
      // ended in extremity
      if (MRSW(LastVmeReadData_b32) == 3) begin
	 $display("Inverting switches");
	 // and that means that motor will not move until we switch
	 // the polarity: invert both switches and point each switch
	 // into different raw switch:
	 Write(getAddr(link, motor, MSWITCHESCONFIG), 32'b1_10_1_01);
	 // read back config, now switches should be zero, so motors can
	 // run
	 Read(getAddr(link, motor, MRAW_SW));
	 $display("Switches currently configured as: 0x%x",
		  LastVmeReadData_b32);
      end // if (LastVmeReadData_b32[2:1] == 3)

      `CHECK_EQUAL(MRSW(LastVmeReadData_b32), 0,
		   "Could not configure switches");
   endtask // setupMotor

   task automatic setupAllMotors(input integer steps = 2);
      for(int link = 0; link < NUMBER_OF_GBT_LINKS; link++)
	for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	   setupMotor(link, i, steps);
	end // for (int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++)
   endtask // setupAllMotors

   // this task verifies, that on given interval between busy risen
   // there is no activity on any stepper motor signals. WE CHECK FOR LOGIC 1
   // at the end of cycle, because this verification is used before the ScEc
   // loop is closed. In such case GEFE shall send logic '1' to all pins. One of the pins is 'driver disable' signal,
   // which automatically disables all the steps activity.
   task automatic VerifyNoMotorActivity();
      $display("Waiting for busy of motor");

      // wait until start of the cycle
      if (~i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o)
	fork
	   @(posedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o);
	   #500us;
	join_any
      `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o, 1,
		   "Timeout motor starting. Switches config?");

      $display("Verifying activity....");

      fork
	 @(motorControl_ob[1].StepOutP_o);
	 @(motorControl_ob[1].StepDeactivate_o);
	 @(motorControl_ob[1].StepBOOST_o);
	 @(motorControl_ob[1].StepDIR_o);
	 @(negedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o);
      join_any
      `CHECK_EQUAL(motorControl_ob[1].StepOutP_o, 1);
      `CHECK_EQUAL(motorControl_ob[1].StepDeactivate_o, 1);
      `CHECK_EQUAL(motorControl_ob[1].StepBOOST_o, 1);
      `CHECK_EQUAL(motorControl_ob[1].StepDIR_o, 1);

   endtask // VerifyNoMotorActivity



   //=====================================  Status & Control
   //====================================\\

   task automatic clkix();
     @(posedge
       i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRs_ix.clk);
   endtask // clkix

   task automatic clkgbt();
     @(posedge
       i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.clk);
   endtask // clkgbt


   task triggerIRQ(input int val);
      // as we force the signal, we do it on negative edge to avoid racing condition when detecting the edge event
     @(negedge
       i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.clk);

      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.IRQ_ob = val;

     @(negedge
       i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.clk);

      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.IRQ_ob = '0;
   endtask


   // task checks if DIODE1, linked to L1 blinks given amount of
   // time. This is used to check behaviour of LED diode during
   // linking process
   task automatic CheckNumberOfDiodeBlinks(input int numblinks);
      int counting = 0;

      // wait until blinking period starts
      $display("Led period syncing");
      @(posedge i_VfcHdSlot1.i_Fpga.
	i_VfcHdApplication.linkgenerator[1].
	i_led_blinker.period_o);
      // and now detect amount of rising edges
      $display("Counting LED blinks, should be %d", numblinks);
      fork
	 @(posedge i_VfcHdSlot1.i_Fpga.
	   i_VfcHdApplication.linkgenerator[1].
	   i_led_blinker.period_o);
	 forever begin
	    // wait until led posedge is detected
	    @(posedge i_VfcHdSlot1.i_Fpga.
	      i_VfcHdApplication.Led_ob8[1]);
	    counting++;
	    $display("Led blink");
	 end
      join_any
      $display("Counted %d blinks on the interval", counting);
      `CHECK_EQUAL(counting, numblinks,
		   "NUMBER OF LED BLINKS DOES NOT MATCH");
   endtask // CheckNumberOfDiodeBlinks


   task automatic CheckIRQline(input int irqline, mustTimeout);
      counter = 0;
      $display("Cheking IRQ arrival for %d", irqline);

      // make reasonable timeout of 5000ccs
      repeat(5000) begin
	 counter++;
	 clkgbt();
	 if (i_VfcHdSlot1.i_Fpga.
	     i_VfcHdApplication.
	     InterruptRequest_b24[irqline]) begin
	    $display("IRQ detected: %d, checking for deassert", irqline);

	    // one more clock cycle and should be deasserted!
	    clkgbt();
	    `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.InterruptRequest_b24[irqline], 0,
			 "IRQ NOT DEASSERTED");
	    break;
	 end // if (i_VfcHdSlot1.i_Fpga....
      end // repeat (5000)
      if (counter == 5000 & ~mustTimeout)
	`CHECK_EQUAL(1, 0, "FAILED TO GET IRQ");
      if (counter != 5000 & mustTimeout)
	`CHECK_EQUAL(1, 0, "CAUGHT IRQ WHILE IT SHOULD NOT COME");

   endtask // CheckIRQline

   task Read (input [31:0] Address_b32);
      i_VmeBus.ReadA32D32(c_VfcSlot4BaseAddress + Address_b32, LastVmeReadData_b32, LastVmeAccessExitCode_b8, c_VmeAccessVerbose);
   endtask:Read

   task Write (input [31:0] Address_b32, input [31:0] Data_b32);
      i_VmeBus.WriteA32D32(c_VfcSlot4BaseAddress + Address_b32, Data_b32, LastVmeAccessExitCode_b8, c_VmeAccessVerbose);
   endtask:Write

   task Reset();
      i_VmeBus.VmeReset(500, c_VmeAccessVerbose);
      // wait until reset clears out through the clock domains
      @(negedge i_VfcHdSlot1.i_Fpga.ClkRs20MHz_ix.reset);
      #100ns;

   endtask:Reset

   task ResetFlags(input int value, howmanytimes);

      repeat(howmanytimes) begin
	 Write(APPSFPGBTUSRDATCTRLREG,
	       value);
	 #124.4ns;
      end
   endtask // ResetFlags

   function [31:0] getAddr(input integer link,
			   input integer motor,
			   input integer register);
      return L1M1 +
	4* (link*$pow(2, MCLINK_WB_ADDRESS_WIDTH) +
	    NUMBER_OF_MC_REGISTERS*motor + register);
   endfunction // getAddr

   task checkReset(input logic val);
      $display("Checking reset lines state");
      `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdSystem.ClkRs20MHz_ix.reset,
		   val);
      `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRs_ix.reset,
		   val);
      `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.reset,
		   val);
      // LED7 indicates reset state
      `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.LedMux_b8[7],
		   val)
   endtask // checkReset


   task forceLOS();
     // task forces LOS and presence signals to predefined state:
     // link1 = missing sfp, link2 = sfp but no signal, links 3/4 OK
     force
     i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.AppSfp1Present_iq =
							       0;
      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.AppSfp1Los_iq =
							      1;
      // SFP2 = sfp present but LOS
      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.AppSfp2Present_iq =
								  1;
      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.AppSfp2Los_iq =
							      1;
      // SFP3,4 = sfp present and lo loss - working line
      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.AppSfp3Present_iq =
								  1;
      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.AppSfp3Los_iq =
							      0;

      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.AppSfp4Present_iq =
								  1;
      force
	i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.AppSfp4Los_iq =
							      0;
      #10us;

   endtask // forceLOS


   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 fook = 0;
	 // push button is a reset button as well. Note however that
	 // POLARITY OF THE SIGNAL IS INVERTED. While in this
	 // testbench '1' indicates button pressed, in simulation top
	 // the signal is INVERTED. In real hardware the signal is
	 // inverted at the hardware level (not sure why BEQP
	 // colleagues did it this way....)
	 // so PushButton_i = 0 means button not pressed!
	 PushButton_i = '0;
	 Reset();
	 #100;
	 $display("Some constants: ");
	 $display("NUMBER_OF_GBT_LINKS: ", NUMBER_OF_GBT_LINKS);
	 $display("NUMBER_OF_LINK_REGISTERS: ", NUMBER_OF_LINK_REGISTERS);
	 $display("NUMBER_OF_MOTORS_PER_FIBER: ", NUMBER_OF_MOTORS_PER_FIBER);
	 $display("NUMBER_OF_MC_REGISTERS: ", NUMBER_OF_MC_REGISTERS);
	 $display("NUMBER OF REGISTERS IN ONE MCLINK: %d, hence address is %d bits",
		  NUMBER_OF_MC_REGISTERS + NUMBER_OF_LINK_REGISTERS,
		  $clog2(NUMBER_OF_MC_REGISTERS + NUMBER_OF_LINK_REGISTERS));

	 $display("MCLINK_WB_ADDRESS_WIDTH: ", MCLINK_WB_ADDRESS_WIDTH);
	 $display("VME-ADDRESSING APPLICATION ADDRESS SPACE STARTS AT 0x%h",
		  c_AppAddrOffset);
	 $display("VME-ADDRESSING FIRST OPTICAL LINK BLOCK (l1/m0) STARTS AT: 0x%h",
		  c_AppAddrOffset+c_AppMCLinkInterfaceOffset);
	 for(int link=0; link < NUMBER_OF_GBT_LINKS; link++) begin
	    $display("LINK %d STARTS AT VME ADDRESS 0x%h", link,
		     (4*link*$pow(2, MCLINK_WB_ADDRESS_WIDTH)) +
		     c_AppAddrOffset + c_AppMCLinkInterfaceOffset);

	    // the equation below requires detailing: first, both expressions are
	    // multiplied by 4 to get the VME address out of local
	    // address. second, NUMBER_OF_MC_REGISTERS is multiplied by 2
	    // because the amount of MC registers in increased twice to get
	    // enough room for extension of MC registers if that's found OK
	    // HENCE:

	    // 2#1000_0000_10FF_LCCC_CMMM_MMVV = 0x00808000 = LINK1, motor 0
	    //        |      |  |  |   |    |
	    //        |      |  |  |   |    |
	    //        |      |  |  |   |    +----------------- VME address space as multiple of 4
	    //        |      |  |  |   |
	    //        |      |  |  |   +---------------------- 2*16 motor control registers (WBMC)
	    //        |      |  |  |
	    //        |      |  |  +-------------------------- motor identifier (0-15) within link
	    //        |      |  |
	    //        |      |  +----------------------------- motor link register page (8 registers)
	    //        |      |
	    //        |      +-------------------------------- identifies fiber link (0-3)
	    //        |
	    //        |
	    //        +--------------------------------------- prefix (0x00808000) for MC page
	    //
	    // And all this is described by following equation:
	    for(int motor=0; motor < NUMBER_OF_MOTORS_PER_FIBER; motor++) begin
	       $display("\tMOTOR %d REGISTERS BASE ADDRESS: 0x%h", motor,
			(4*link*$pow(2, MCLINK_WB_ADDRESS_WIDTH)) +
			4*(NUMBER_OF_MC_REGISTERS)*motor +
			c_AppAddrOffset + c_AppMCLinkInterfaceOffset);
	    end
	 end
	 $display("Length of IRQ_ob: %d",
	    $size(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.IRQ_ob));
	 $display("Distribution of IRQ lines:");
	 $display("\tbits 15 to 0 indicate IRQ from given motor");
	 $display("\tbits 17, 16 indicate link which caused IRQ\
 trigger");
	 $display("\tbits 21 to 18 indicate IRQ link to fail");
	 $display("GEFE INTERLOCK VALUE: 0x%x", GEFE_INTERLOCK);

      end

      `TEST_CASE("read_product_id") begin
	 Read(PRODUCTID);
	 $display("Product ID: 0x%x", LastVmeReadData_b32);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'hab010100,
		      "Product ID does not match");
      end

      `TEST_CASE("gbtrx_propagation_to_wbmc") begin
	 // this forces L1M1 switches to be 'connected'. (value of 3
	 // means that switches are connected, but not actuating as
	 // the motor does not reach any extremity)
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.
	     i_AppSfpGbtBank.RxData_o4b80[1] = 80'h30000;
	 // others are at zero
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.
	     i_AppSfpGbtBank.RxData_o4b80[2] = 80'h0000;
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.
	     i_AppSfpGbtBank.RxData_o4b80[3] = 80'h0000;
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.
	     i_AppSfpGbtBank.RxData_o4b80[4] = 80'h0000;

	 #1us;
	 // check reading VME if the switch info propagated
	 Read(getAddr(0, 0, MMSTAT));
	 // the value checked: 01 = state machine in idle, and two
	 // switches disabled - all that in link1, motor1 (that's where
	 // getAddr comes handy). And it propagates into raw and
	 // converted switching because setting is 1:1. Switches are
	 // not configured, that's why they return always zeros...
	 `CHECK_EQUAL(MRMSMAS(LastVmeReadData_b32), 1);
	 `CHECK_EQUAL(MRABRT(LastVmeReadData_b32), 0);
	 `CHECK_EQUAL(MROH(LastVmeReadData_b32), 0);
	 `CHECK_EQUAL(MRFAIL(LastVmeReadData_b32), 0);
	 `CHECK_EQUAL(MRRAW_SW(LastVmeReadData_b32), 3);
	 `CHECK_EQUAL(MRSW(LastVmeReadData_b32), 0);
	 `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 0);

      end // UNMATCHED !!

      `TEST_CASE("irq_causes_led_blink") begin
	 // serial register tests, force GBT clocks reset, and FORCE
	 // gefe link to be closed (without initing scec channel as it
	 // is quite time consuming, and we want to verify different thing)
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.reset
	     = 1;

	 #1us;
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.reset
	     = 0;
	 force i_VfcHdSlot1.i_Fpga.AppSfpPresent_b4 = '1;
	 ForceScEcLoopback();
	 #1us;

	 // let's force one link by one, this is done by following
	 // pattern
	 // resync to GBT clock used to generate IRQs
	 clkgbt();
	 triggerIRQ(18'h0_0001);
	 checkLED(1);
	 triggerIRQ(18'h1_0002);
	 checkLED(2);
	 triggerIRQ(18'h2_0003);
	 checkLED(3);
	 triggerIRQ(18'h3_0004);
	 checkLED(4);
      end // UNMATCHED !!

      `TEST_CASE("led_mux") begin
	 forceLOS();

	 // serial register tests, force GBT clocks reset
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.reset
	     = 1;
	 #1us;
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.reset
	     = 0;

	 #1us;
	 // THIS SHOULD BE PRESENT ON INTERNAL MUX FOR LED FLASHER due
	 // to how the presence and los signals are setup
	 // NO SFP:
	 `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_8b4[1],
		      1);
	 // SFP BUT LOS:
	 `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_8b4[2],
		      2);
	 // SFP, LOS OK but not closed loop:
	 `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_8b4[3],
		      3);
	 // and now we force closed loop:
	 ForceScEcLoopback();
	 #1us;
	 `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_8b4[4],
		      0);
      end // # 1us;


      `TEST_CASE("led_one_blink_out") begin
	 // lets chech how the diodes flash. While led 3 and 4 should
	 // be silent, leds 1 and 2 should blink once and twice
	 // respectively because one is without sfp, the other one is
	 // with sfp but without optics
	 @(posedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[1]);
	 #1;
	 repeat(5) begin
	    clkgbt();

	    `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[1],
			 1);

	 end
	 clkgbt();
	 `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[1],
		      0);
      end // # 1us;

      `TEST_CASE("led_two_blinks") begin
	 forceLOS();

	 // LED two should blink twice, with 8 spaces in between. As
	 // the blinks between '1' and '2' are synchronized, we can
	 // detect start of the period from led1
	 @(posedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[1]);
	 #1;
	 for (int x=0; x < 2; x++) begin
	    repeat(5) begin
	       clkgbt();
	       `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[2],
			    1);
	    end
	    repeat(8) begin
	       clkgbt();
	       `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[2],
			    0);
	    end
	 end // for (int x=0; x < 2; x++)
      end // UNMATCHED !!

      `TEST_CASE("debug_test") begin
	 // check whether reset can be induced by both button and VME
	 // reset
	 #5us;

	 checkReset(0);
	 // issue reset command by VME - single flip write should be
	 // enough to trigger
	 Write(SYS_CTRLREG, 1);
	 fork
	    #10us;
	    @(posedge
	      i_VfcHdSlot1.i_Fpga.i_VfcHdSystem.ClkRs20MHz_ix.reset);
	 join_any
	 #50ns;
	 checkReset(1);
	 // and should clearout itself as the register should be of
	 // type autoclear (we can do it as it is single clock driven
	 // design) - note that ORIGINAL SYSTEM SUBMODULE DOES NOT
	 // IMPLEMENT THIS AND USER IS REQUIRED TO MAKE 2 VME ACCESSES
	 // TO RESET - ONE TO SET THE BIT, THE OTHER TO CLEAROUT
	 fork
	    #10us;
	    @(negedge
	      i_VfcHdSlot1.i_Fpga.i_VfcHdSystem.ClkRs20MHz_ix.reset);
	 join_any
	 #50ns;
	 checkReset(0);

      end // UNMATCHED !!

      `TEST_CASE("no_need_to_clear_reset_vme_command") begin
	 // check whether reset can be induced by VME
	 // reset and that only single write of '1' is enough to reset
	 // the design (hence not needed to flip the reset value back
	 // to zero)
	 #5us;

	 checkReset(0);
	 // issue reset command by VME - single flip write should be
	 // enough to trigger
	 Write(SYS_CTRLREG, 1);
	 fork
	    #10us;
	    @(posedge
	      i_VfcHdSlot1.i_Fpga.i_VfcHdSystem.ClkRs20MHz_ix.reset);
	 join_any
	 #50ns;
	 checkReset(1);
	 // and should clearout itself as the register should be of
	 // type autoclear (we can do it as it is single clock driven
	 // design) - note that ORIGINAL SYSTEM SUBMODULE DOES NOT
	 // IMPLEMENT THIS AND USER IS REQUIRED TO MAKE 2 VME ACCESSES
	 // TO RESET - ONE TO SET THE BIT, THE OTHER TO CLEAROUT
	 fork
	    #10us;
	    @(negedge
	      i_VfcHdSlot1.i_Fpga.i_VfcHdSystem.ClkRs20MHz_ix.reset);
	 join_any
	 #50ns;
	 checkReset(0);

      end // UNMATCHED !!

      `TEST_CASE("push_button_reset") begin
	 // check whether reset can be induced by both button and VME
	 // reset
	 #5us;

	 checkReset(0);
	 PushButton_i = 1;
	 fork
	    #10us;
	    @(posedge
	      i_VfcHdSlot1.i_Fpga.i_VfcHdSystem.ClkRs20MHz_ix.reset);
	 join_any
	 #50ns;
	 checkReset(1);
	 // deassert button
	 PushButton_i = 0;
	 fork
	    #10us;
	    @(negedge
	      i_VfcHdSlot1.i_Fpga.i_VfcHdSystem.ClkRs20MHz_ix.reset);
	 join_any
	 #50ns;
	 checkReset(0);
      end // UNMATCHED !!

      `TEST_CASE("number_of_executed_queued_commands") begin
	 // this test verifies how many commands are executed when
	 // queuing 2 commands into queue and the third one fires
	 setupMotor(0, 0, 5);
	 // we have to turn off the possibility to trigger by external
	 // sources as this would prevent 'automatic'
	 // movement. Disable IRQ as well. Because when enabled, it
	 // runs first command, and then waits for external/internal
	 // trigger to perform another two. If disabled, this is ran consecutively
	 Write(L1M1 + (MTRIGCFG * 4), 0);
	 // the previous function set up the direction '1', so
	 // mcpacket will show it (but no others as the loop is not
	 // closed, so for inspection we need to check directly within
	 // firmware and not at the output of GEFE as this will show
	 // nothing
	 // SCHEDULE COMMAND - write one movement into queue
	 Write(L1M1 + (MQUEUE * 4), 8);
	 // change direction:
	 Write(L1M1 + (MDIRECTION * 4), 0);
	 // queue this one as well:
	 Write(L1M1 + (MQUEUE * 4), 8);
	 // and write again direction into the register:
	 Write(L1M1 + (MDIRECTION * 4), 1);
	 // but this time do not queue, but fire the command. This
	 // should execute in total 3 movements: 2 from queue (dir
	 // up/down) and third from active register (up). Fire single
	 // motor:
	 Write(L1M1 + (MSTARTMOVE * 4), 1);
	 #1us;
	 fork
	    forever begin
	       // READ BUSY
	       #1us;
	       Read(L1M1 + (MBUSY * 4));
	       if (~MRBUSY(LastVmeReadData_b32)) break;
	    end
	    #10ms;
	    begin
	       // this task verifies if MOTOR gives correct amount of
	       // pulses at its output, including direction, which
	       // must be exactly setup. Each iteration gives 5 pulses
	       // with given direction. This test is not 'nice' as we
	       // check the signals burried inside stepping controller
	       `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.i_fourmclinks.
			    linkextraction[0].i_mclink.mcontrols.singlemc[0].
			    i_wbmc.mc.outvec.StepDeactivate_o, 0);
	       countpos = 0;
	       countneg = 0;
	       totalcount = 0;

	       forever begin
		  @(negedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.i_fourmclinks.
			    linkextraction[0].i_mclink.mcontrols.singlemc[0].
		    i_wbmc.mc.outvec.StepOutP_o);
		  totalcount ++;
		  if (i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.i_fourmclinks.
				 linkextraction[0].i_mclink.mcontrols.singlemc[0].
				 i_wbmc.mc.outvec.StepDIR_o == 1)
		    countneg++;
		  else
		    countpos++;

		  if (totalcount > 5 && totalcount < 11) begin
		    `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.i_fourmclinks.
				 linkextraction[0].i_mclink.mcontrols.singlemc[0].
				 i_wbmc.mc.outvec.StepDIR_o, 0);
		  end else begin
		    `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.i_fourmclinks.
				 linkextraction[0].i_mclink.mcontrols.singlemc[0].
				 i_wbmc.mc.outvec.StepDIR_o, 1);
		  end
		  `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.i_fourmclinks.
			    linkextraction[0].i_mclink.mcontrols.singlemc[0].
			       i_wbmc.mc.outvec.StepDeactivate_o, 0);
	       end
	    end

	 join_any
	 `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 0);
	 $display("Counts: %d, %d, %d", countpos, countneg,
		  totalcount);

	 `CHECK_EQUAL(countneg, 10);
	 `CHECK_EQUAL(countpos, 5);
	 `CHECK_EQUAL(totalcount, 15);

      end // UNMATCHED !!

     `TEST_CASE("pattern_generator_on_opens_loop") begin
	// the test verifies, that if the loop is closed, and the
	// pattern generator turns on, it shall automatically break
	// the loop. This is a security feature, which allows motors
	// NOT TO MOVE while pattern generator is on. Once the pattern
	// generator is disabled, the loop comes back on. When pattern
	// generator interferes with the loop, the scec1 channel
	// should return 32'hdeadbeef.
	 ScEcLock();
	 $display("System locked, messing up with LOS and present");
	 // now we write linking word and diode on L1 should go off
	 // because the channel is ready to work!
	 Write(L1GEFECHANNEL1CTRL, GEFE_INTERLOCK);
	 waitScEcData(1);
	 #10us;
	 Read(L1GEFECHANNEL1STAT);
	 $display("Magic number caught: 0x%x", LastVmeReadData_b32);
	 `CHECK_EQUAL(LastVmeReadData_b32, GEFE_INTERLOCK,
		      "Magic number is not setup");
	// we have a pattern generator still turned off, now we turn
	// it on and wait for new scec data, which should contain
	// deadbeef
	 Write(APPSFPGBTUSRDATCTRLREG, 32'hf000_0000);
	 waitScEcData(1);
	 #10us;
	 Read(L1GEFECHANNEL1STAT);
	 $display("Broken link number caught: 0x%x", LastVmeReadData_b32);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'hdeadbeef,
		      "Magic number is wrong");
	// and we put all into previous state
	 Write(APPSFPGBTUSRDATCTRLREG, 0);
	 waitScEcData(1);
	 #10us;
	 Read(L1GEFECHANNEL1STAT);
	 $display("Link new magic number caught: 0x%x", LastVmeReadData_b32);
	 `CHECK_EQUAL(LastVmeReadData_b32, GEFE_INTERLOCK,
		      "Magic number is wrong");
     end // UNMATCHED !!


      `TEST_CASE("closed_loop_concept") begin
	 // this test verifies the 'closed loop' concept - we wait for
	 // channel1 to setup, then we write there a 'specific' number
	 // saying 'we did eveything to setup the link, GEFE can cast
	 // the signals'. And this will be verified by checking step
	 // enable signal AT THE OUTPUT OF GEFE! (not at the output of
	 // VFC - this does not say anything yet). So let's go:
	 // establish ScEc connection. Before we setup SFP values by
	 // forcing them (to test behaviour of LED diodes), we run
	 // ScEc lock. As SFPpresent signal is not asserted, all 4
	 // diodes should blink once within a period
	 $display("Before locking");
	 CheckNumberOfDiodeBlinks(1);
	 ScEcLock();
	 $display("System locked, messing up with LOS and present");

	 // here we force SFP present and since this time diodes
	 // should blink twice per period
	 force i_VfcHdSlot1.i_Fpga.AppSfpPresent_b4 = '1;
	 force i_VfcHdSlot1.i_Fpga.AppSfpLos_b4 = '1;

	 // once established we try to run a stepper and catch the
	 // signal at the output of stepper. L1M1 is connected to
	 // GEFE, we setup only 2 steps as we don't care about
	 // stepping profile
	 setupMotor(0, 0, 2);
	 // launch motor and verify if VFC does the job (VFC will work
	 // independently of GEFE, it will still work, just GEFE will
	 // not listen to those commands)
	 Write(APPGLOBALTRIGGER, 1);
	 $display("Waiting motor busy");
	 // fancy stuff: we need to check if ANY of the stepper
	 // signals is risen during that time.
	 VerifyNoMotorActivity();
	 // we need to push a bit time, IRQs are enabled and distort
	 // LED diodes blinking
	 #200ns;
	 // now check that LED diodes blink twice:
	 CheckNumberOfDiodeBlinks(2);
	 // before we inject the gefe interlock word, we force as well
	 // LOS signal - this will make diodes blink three times as
	 // the linking word is still missing
	 force i_VfcHdSlot1.i_Fpga.AppSfpLos_b4 = '0;
	 #200ns;
	 // three blinks required when word does not match GEFE interlock
	 CheckNumberOfDiodeBlinks(3);
	 // check that gefe returns no data valid. thich we check
	 // through VME
	 Read(APPSFPGBTUSRDATSTATREG);
	 `CHECK_EQUAL(RXISDATAFLAG(LastVmeReadData_b32),
		      0,
		      "RXISDATA is not correctly propagating");

	 // now we write linking word and diode on L1 should go off
	 // because the channel is ready to work!
	 Write(L1GEFECHANNEL1CTRL, GEFE_INTERLOCK);
	 waitScEcData(1);
	 #10us;
	 Read(L1GEFECHANNEL1STAT);
	 $display("Magic number caught: 0x%x", LastVmeReadData_b32);
	 `CHECK_EQUAL(LastVmeReadData_b32, GEFE_INTERLOCK,
		      "Magic number is not setup");
	 // that means, that no LED diode blinking should be detected
	 // as line is ready to work
	 CheckNumberOfDiodeBlinks(0);
	 // if magic number is setup, we fire yet one more motor
	 // running, but this time we _expect_ data being present at
	 // the output!
	 Write(APPGLOBALTRIGGER, 1);
	 $display("Waiting motor busy");
	 fork
	    @(posedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o);
	    #500us;
	 join_any
	 `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o,
		      1,
		      "Motor did not start. Switches config?");
	 // wait until negative edge of Busy, now we check if signals
	 // move as supposed to (in particular stepenab)
	 @(negedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o);
	 // check that gefe returns data valid for first channel (the
	 // others are not propagated as gefe is not connected there)
	 Read(APPSFPGBTUSRDATSTATREG);
	 `CHECK_EQUAL(RXISDATAFLAG(LastVmeReadData_b32),
		      1,
		      "RXISDATA is not correctly propagating");


      end // UNMATCHED !!

      `TEST_CASE("propagation_of_switchesconfig") begin
	 // led diodes on the front-panel of GEFE show the extremity
	 // switches. WE WANT that whenever the device reaches _OUT OF
	 // VACCUM CHAMBER_ so that left diode is turned on NO MATTER
	 // what kind of switch it is (NO/NC). For this gefe needs to
	 // know what is the switches configuration. Hence we need to
	 // propagate this information through the fiber using
	 // tx_memory from mclink.sv. This information should
	 // propagate to gefe through the 16-bit LSBs of optical link
	 // and has to be restored on the gefe side. This test sets
	 // random numbers on switches configuration registers and
	 // verifies their presence on GEFE side (by directly picking
	 // the gefe signals)
	 for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	    // generate random switches configuration and write into HW
	    assert(std::randomize(LastVmeReadData_b32))
	    Write(getAddr(0, i, MSWITCHESCONFIG),
		  LastVmeReadData_b32);
	    // store in the queue:
	    Q_b32.push_back(LastVmeReadData_b32);
	 end
	 // we don't need to wait too long here as the data are
	 // transported using FASTLINK (no scec locking is needed). We
	 // still have to wait for 2 frames:
	 #100us;
	 for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	    LastVmeReadData_b32 = Q_b32.pop_front();
	    // we compare only to the length of switchesconfig as only
	    // those data are caught from VME iface:
	    `CHECK_EQUAL(i_GefeApplication.SwitchesConfiguration_2b16[i],
			 LastVmeReadData_b32[$bits(i_GefeApplication.SwitchesConfiguration_2b16[i])-1:0]);
	 end

      end // UNMATCHED !!


      `TEST_CASE("match_gefe_build_number") begin
	 // this test waits for setup of sc/ec link, and reads through
	 // VFC and GEFE pages of data in scec link. This basically
	 // simulates _entire chain_ including GBT paging and requires
	 // quite some time to do the job
	 $display("ScEc link preparation");
	 ScEcLock();
	 // once locked we can read through GEFE serial 'some number',
	 // which should return '0x01' saying GEFE present.
	 // read link 0 register block. This is reading
	 // GefeStatus_4x2b32[1][0], i.e. link1 channel0
	 // status. Should return '1' saying GEFE present.
	 $display("Writing 0x01 to the page register to see build number");
	 Read(L1GEFECHANNEL0STAT);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'(1));
	 // write into the page to see the build number -
	 // L1ScEcControlChannel0:
	 // write that we want to see page 1
	 Write(L1GEFECHANNEL0CTRL, 1);
	 // we're hardware fast here, so wait until new data come. In
	 // real world VME access is quite slow so the system updates
	 // info until next VME read request come.
	 waitScEcData();
	 waitScEcData();
	 // and read back:
	 #10us;
	 Read(L1GEFECHANNEL0STAT);
	 $display("GEFE build number: 0x%x", LastVmeReadData_b32);
	 `CHECK_EQUAL(LastVmeReadData_b32, build_ob32,
		      "Build number does not fit");

	 // check as well PCB revision:
	 Write(L1GEFECHANNEL0CTRL, 2);
	 waitScEcData();
	 // and read back:
	 #10us;
	 Read(L1GEFECHANNEL0STAT);
	 `CHECK_EQUAL(LastVmeReadData_b32, 'ha);

      end // UNMATCHED !!

      `TEST_CASE("los_blinking") begin
	 forceLOS();
	 // serial register tests, force GBT clocks reset
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.reset
	     = 1;
	 #1us;
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.reset
	     = 0;

	 #1us;

	 // LED two should blink twice, with 8 spaces in between. As
	 // the blinks between '1' and '2' are synchronized, we can
	 // detect start of the period from led1
	 @(posedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[1]);
	 #1;
	 for (int x=0; x < 2; x++) begin
	    repeat(5) begin
	       clkgbt();
	       `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[2],
			    1);
	    end
	    repeat(8) begin
	       clkgbt();
	       `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Led_ob8[2],
			    0);
	    end
	 end // for (int x=0; x < 2; x++)

      end // UNMATCHED !!


      `TEST_CASE("irq_fail_link_generates_diode_blink") begin
	 // serial register tests, force GBT clocks reset
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.reset
	     = 1;
	 #1us;
	 force
	   i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.reset
	     = 0;

	 // force SFP present and wait for 1us to clean all IRQs
	 force i_VfcHdSlot1.i_Fpga.AppSfpPresent_b4 = '1;
	 ForceScEcLoopback();
	 #1us;

	 for(diode = 0; diode < 4; diode++) begin
	    $display("Checking diode %d", diode+1);

	    // this should trigger IRQ of link loss (one of further tests)
	    force i_VfcHdSlot1.i_Fpga.AppSfpLos_b4 = (2**(diode+1)-1);
	    checkLED(diode+1);
	 end
      end

      `TEST_CASE("scec_fifo_empty") begin
	 // checking behaviour of SCEC fifo. Unfortunately there's no
	 // way how to find if data were actually propagated because
	 // the loopback mechanism timing is 'arbitrary'. Hence we
	 // need to put into this tests some pauses here and there to
	 // make it running
	 ScEcLock();
	 Read(GEFESERIAL0STAT);
	 `CHECK_EQUAL(RXSERIALUP(LastVmeReadData_b32), 15,
		      "Timeout during locking serial_register");
	 `CHECK_EQUAL(RXSLOCKED(LastVmeReadData_b32), 15,
		      "Timeout during locking serial_register");
	 // tx FIFO should be empty for all channels:
	 `CHECK_EQUAL(TXSEMPTY(LastVmeReadData_b32), 15,
		      "TxFifoEmpty flags should be risen");
	 #1us;
	 // write into each channel
	 Write(L1GEFECHANNEL0CTRL, 1);
	 Write(L2GEFECHANNEL0CTRL, 1);
	 Write(L3GEFECHANNEL0CTRL, 1);
	 Write(L4GEFECHANNEL0CTRL, 1);
	 // now we wait until the fifos start to claim that they are
	 // not empty:
	 fork
	    forever begin
	       Read(GEFESERIAL0STAT);
	       if (TXSEMPTY(LastVmeReadData_b32) == 0)
		 break;
	       #200ns;
	    end
	    // timeout:
	    #500us;
	 join_any
	 `CHECK_EQUAL(TXSEMPTY(LastVmeReadData_b32), 0,
		      "All Tx fifos should not be empty");
      end

      `TEST_CASE("serial_slow_control_link") begin
	 // serial register tests, force GBT clocks reset
	 ////////////////////////////////////////////////////////////////////////////////
	 // FIRST INITIALIZATION OF SCEC CONTROL:
	 ////////////////////////////////////////////////////////////////////////////////
	 // here we have to use for this kind of test channel1,
	 // because channel0 is connected to GEFE and does paging and
	 // setting up loopback mode for 80bit data. Channel1 is in
	 // loopback mode by default as it serves to identify, that SW
	 // properly setup targets
	 ScEcLock(1);
	 // clear out the rxlol and txerror:
	 Write(APPSFPGBTUSRDATCTRLREG,
	       32'h0f00_0000);
	 // read channel1 status
	 Read(GEFESERIAL1STAT);
	 `CHECK_EQUAL(TXSERROR(LastVmeReadData_b32), 0,
		      "Error check registers not cleared out");
	 `CHECK_EQUAL(RXSLOL(LastVmeReadData_b32), 0,
		      "Error check registers not cleared out");
	 // write into TXGefe registers to send the data in RANDOM
	 datapush = 0;

	 ////////////////////////////////////////////////////////////////////////////////
	 // PUSHING 20 RANDOM DATA TO THE QUEUE AND OBSERVING AT RX
	 ////////////////////////////////////////////////////////////////////////////////
	 fork
	    begin
	       // wait until end of data transmission
	       @(negedge datapush);
	       // here all the data were pushed to queue, we give it
	       // additional 500us to clear out the queue, if not, timeout:
	       #500us;
	       // and this is the end of transmission, we'll check the queue
	    end

	    begin
	       repeat(20) begin
		  // exclude zero as reader is not able to identify
		  // it, and as well we have to assure that no two
		  // same numbers we get, otherwise the algorithm of
		  // getting and reading will not work
		  assert(std::randomize(data) with {data[1] > 0;
		     data[2] > 0;
		     data[3] > 0;
		     data[4] > 0;});
		  // push all data into queues, and write them into the
		  // channels
		  for(int xlink=1; xlink < NUMBER_OF_GBT_LINKS+1; xlink++)
		    begin
		       queuelock.get(1);
		       queues[xlink].push_back(data[xlink]);
		       Write(c_AppAddrOffset + c_AppGlobalRegisters +
			     (40 + xlink-1)*4,
			     data[xlink]);
		       queuelock.put(1);
		    end
		  $display("Pushed data: 0x%.8x, 0x%.8x, 0x%.8x, 0x%.8x, ",
			   data[1], data[2], data[3], data[4]);
		  #20us;
		  // announce to reader that first sample was sent and
		  // he can start the reading thread
		  datapush = 1;
	       end // repeat (10)
	       datapush = 0;
	       // wait forever, the threads should come out after 500us;
	       forever #1us;

	    end // fork begin

	    // each queue now acts separately, we check whether we see
	    // _all_ values sent
	    begin
	       $display("ReadThread: Waiting for first data to queue");
	       @(posedge datapush);
	       $display("ReadThread: checking queue");
	       forever begin
		  for(int ylink=1; ylink < NUMBER_OF_GBT_LINKS+1; ylink++)
		    begin
		       #1us;
		       // and if the data match, remove them from the
		       // queue, get first item in the queue:
		       queuelock.get(1);
		       // read link data:
		       Read(c_AppAddrOffset + c_AppGlobalRegisters +
			    (44 + ylink-1)*4);
		       // note: tmp is xxxxxxxx if the queue is empty,
		       // but that does not harm.
		       tmp = queues[ylink][0];
		       if(LastVmeReadData_b32 == tmp)
			 $display("Removed 0x%.8x", queues[ylink].pop_front());
		       queuelock.put(1);
		       $display("Queue ylink%d 0x%.8x, read 0x%.8x",
				ylink, tmp, LastVmeReadData_b32);
		    end
	       end // forever begin
	    end
	 join_any
	 // and now you're doomed if at least one queue is not empty
	 // and as well if rx and tx does not contain the same thing:
	 for(int xlink=1; xlink < NUMBER_OF_GBT_LINKS+1; xlink++)
	   begin
	      `CHECK_EQUAL(queues[xlink].size(), 0, "Queue not empty");

	      Read(c_AppAddrOffset + c_AppGlobalRegisters +
		   (44 + xlink-1)*4);
	      tmp  = LastVmeReadData_b32;
	      Read(c_AppAddrOffset + c_AppGlobalRegisters +
		   (40 + xlink-1)*4);
	      `CHECK_EQUAL(tmp, LastVmeReadData_b32,
			   "Inouts do not match in serial loopback");
	   end

      end

      `TEST_CASE("fail_irq_disabled_when_txdisable") begin
	 // DISABLE ALL TXs, and the thing should fail for all
	 // registers to generate IRQ even if TxFault is
	 // reported. That's because we do not want to clutter IRQ
	 // subsystem when TX is not in use!
	 force i_VfcHdSlot1.i_Fpga.StatusAppSfpTxDisable_b4 = '1;

	 for(swa = 0; swa < NUMBER_OF_GBT_LINKS; swa++) begin
	    fork
	       force i_VfcHdSlot1.i_Fpga.AppSfpTxFault_b4 = 2**swa;
	       CheckIRQline(18 + swa, 1);
	    join
	 end
	 release i_VfcHdSlot1.i_Fpga.AppSfpTxFault_b4;
      end


      `TEST_CASE("link_irq_due_txfault") begin
	 for(swa = 0; swa < NUMBER_OF_GBT_LINKS; swa++) begin
	    // we force the line to a given state
	    fork
	       force i_VfcHdSlot1.i_Fpga.AppSfpTxFault_b4 = 2**swa;
	       CheckIRQline(18 + swa, 0);
	    join
	    // and force back to zero, should generate yet another IRQ
	    fork
	       force i_VfcHdSlot1.i_Fpga.AppSfpTxFault_b4 = 0;
	       CheckIRQline(18 + swa, 0);
	    join
	 end

      end

      `TEST_CASE("link_irq_due_los") begin
	 // checking IRQ propagation when links are not ready
	 // force LOS irq on one line. Thing here: IRQ lines 0-15
	 // are for motor, lines 16/17 are link number causing
	 // IRQ. but links 18-21 are dedicated to LINK fail IRQs,
	 // LOS1 should be triggered by line 18, los2 by 19
	 // etc. each SFP has dedicated link AND THOSE CAN BE MASKED!
	 for(swa = 0; swa < NUMBER_OF_GBT_LINKS; swa++) begin
	    fork
	       force i_VfcHdSlot1.i_Fpga.AppSfpLos_b4 = 2**swa;
	       CheckIRQline(18 + swa, 0);
	    join
	    // and force back to zero, should generate yet another IRQ
	    fork
	       force i_VfcHdSlot1.i_Fpga.AppSfpLos_b4 = 0;
	       CheckIRQline(18 + swa, 0);
	    join
	 end
	 release i_VfcHdSlot1.i_Fpga.AppSfpLos_b4;
      end

      `TEST_CASE("reset_rxerr_singlepulse_propagation") begin
	 // checking propagation of signals into multiple clock
	 // domains
	 fork
	    ResetFlags(32'h0f0_00000, 500);
	    begin
	       counter = 0;
	       forever begin
		  if (i_VfcHdSlot1.
		      i_Fpga.
		      i_VfcHdApplication.
		      ResetGbtRxDataErrorsAppSfpGbtUserData_b4)
		    counter++;
		  @(posedge i_VfcHdSlot1.
		    i_Fpga.
		    i_VfcHdApplication.ClkRsGBT40MHz_ix.clk);
	       end
	    end
	 join_any
	 $display("Triggers counted: %d", counter);

	 `CHECK_EQUAL(counter, 500, "MISSING TRIGGERS ");
      end

      `TEST_CASE("reset_rdylost_singlepulse_propagation") begin
	 // checking propagation of signals into multiple clock
	 // domains
	 fork
	    ResetFlags(32'h00f00000, 500);
	    begin
	       counter = 0;
	       forever begin
		  if (i_VfcHdSlot1.
		      i_Fpga.
		      i_VfcHdApplication.
		      ResetGbtRxRdyLostFlagAppSfpGbtUserData_b4)
		    counter++;
		  @(posedge i_VfcHdSlot1.
		    i_Fpga.
		    i_VfcHdApplication.ClkRsGBT40MHz_ix.clk);
	       end
	    end
	 join_any
	 $display("Triggers counted: %d", counter);

	 `CHECK_EQUAL(counter, 500, "MISSING TRIGGERS ");
      end

      `TEST_CASE("flags_resets_edge_sensitive") begin
	 // trigger reset flags for pattern, they should generate
	 // single clock answer. This fires ResetGbtRxRdyLostFlagAppSfpGbtUserData_b4
	 Write(APPSFPGBTUSRDATCTRLREG,
	       32'h00F0_0000);
	 // and this fires ResetGbtRxDataErrorsAppSfpGbtUserData_b4:
	 Write(APPSFPGBTUSRDATCTRLREG,
	       32'h0F00_0000);
	 // read back values, they should be zero:
	 Read(APPSFPGBTUSRDATCTRLREG);

	 `CHECK_EQUAL(LastVmeReadData_b32[27:20], 0, "FAILED READOUT ");
      end

      `TEST_CASE("pattern_generator_on") begin
	 $display("Checking pattern generator state");
	 // check if pattern generator is disabled:
	 Read(APPSFPGBTUSRDATSTATREG);
	 `CHECK_EQUAL(ISPATTERNON(LastVmeReadData_b32), 0, "PTG FAIL");
	 // we turn on pattern generator and observe how it propagates
	 // through the system while motors do not work
	 Write(APPSFPGBTUSRDATCTRLREG, 32'hf000_0000);
	 Read(APPSFPGBTUSRDATSTATREG);
	 `CHECK_EQUAL(ISPATTERNON(LastVmeReadData_b32), 4'hf, "PTG DOES NOT PROPAGATE");
	 // turn off GBT pattern:
	 Write(APPSFPGBTUSRDATCTRLREG, 32'h0000_0000);
	 Read(APPSFPGBTUSRDATSTATREG);
	 `CHECK_EQUAL(ISPATTERNON(LastVmeReadData_b32), 4'h0, "PTG DOES NOT COME BACK");
	 setupMotor(0, 0);
	 $display("Checking pattern generator being off");
	 // we check if pattern generator is turned off as if it is
	 // still on, we never manage to fire the motor
	 Read(APPSFPGBTUSRDATSTATREG);
	 `CHECK_EQUAL(ISPATTERNON(LastVmeReadData_b32), 4'h0, "PTG DOES NOT COME BACK");
	 // this command triggers internal trigger for all motors - as
	 // we've configured motors above, the motor should start
	 // movement _and_ busy within mcoi.sv should go high
	 Write(APPGLOBALTRIGGER, 1);
	 $display("Waiting motor busy");
	 fork
	    @(posedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o);
	    #500us;
	 join_any
	 `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o,
		      1,
		      "Motor did not start. Switches config?");

	 $display("Got busy flag, firing request for pattern");
	 Read(APPSFPGBTUSRDATSTATREG);
	 `CHECK_EQUAL(ISPATTERNON(LastVmeReadData_b32), 4'h0, "PTG SHOULD BE ZERO");
	 Write(APPSFPGBTUSRDATCTRLREG, 32'hf000_0000);
	 // and now first the assertion in mcoi should trigger if we
	 // do not get any pattern up,
	 #1us;
	 $display("Waiting for busy");


	 forever begin
	    if (~i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o) break;
	    // this should be still zero even if we requested '1' as
	    // motors are busy, and until they are busy, this flag is
	    // not asserted
	    Read(APPSFPGBTUSRDATSTATREG);
	    // check for racing condition: busy can go up anywhere in
	    // the read cycle, and if it is up, this condition should
	    // no more be valid:
	    if (i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o)
	      `CHECK_EQUAL(ISPATTERNON(LastVmeReadData_b32), 4'h0, "PTG SHOULD BE ZERO");
	    #100ns;
	 end
	 $display("End of cycle detected, should trigger");
	 Read(APPSFPGBTUSRDATSTATREG);
	 `CHECK_EQUAL(ISPATTERNON(LastVmeReadData_b32), 4'hf, "PTG SHOULD BE ONE");
	 #1us;
      end


      `TEST_CASE("single_cc_errinclr") begin
	 // getting into AppSfpGbtUsrDatCtrlReg in global register
	 // space writing
	 // into errinj bit, should generate single cycle. If not,
	 // that's wrong, write into error injection, and this thing
	 // should make single trigger
	 Write(APPSFPGBTUSRDATCTRLREG, 256);
	 #10us;
      end

      `TEST_CASE("all_irqs_with_motor_in_switch") begin
	 // setup all motors into positive switch, setup internal
	 // triggering and then fire them at the same time. We should
	 // see all 16 irqs
	 // enable IRQ:
	 Write(SYS_INTMASK, 0);
	 // IRQ level:
	 Write(SYS_INTLEVEL, 3);
	 // IRQ vector:
	 Write(SYS_INTVECTOR, 32'h84);
	 // IRQ enable:
	 Write(SYS_INTCONFIG, 1);

	 // configure switches of the L1M1 such, that motor is IN the
	 // switch position, hence any start results in 'no
	 // move'. Setup motor L1M1 to perform 500 steps.
	 for (int motor=0; motor < NUMBER_OF_MOTORS_PER_FIBER; motor++) begin
	    setupMotor(0, motor, 5);
	    //enable internal triggering of all motors at once _and_
	    //keep IRQ enabled
	    Write(getAddr(0, motor, MTRIGCFG), 32'ha);
	    // setup switches. By default rawswitches are coming from mc
	    // stream. both of them at logical 1. We want to setup
	    // positive direction of the movement (dir=0), and raw
	    // switches such, that in positive direction the switch is actuated
	    Write(getAddr(0, motor, MDIRECTION), 0);
	    // switch actuated means that in positive direction there is
	    // no inversion (so logical positive switch is engaged = 1),
	    // while negative direction we show disengaged switch by
	    // enforcing inversion:
	    Write(getAddr(0, motor, MSWITCHESCONFIG), 32'b1_10_0_01);
	    // switches raw values are 'no care', but logical values
	    // after some time should say that both of them are engaged
	    Read(getAddr(0, motor, MMSTATUS));
	    // check for positive dir switch being engaged, and irqs not
	    // in action:
	    `CHECK_EQUAL(MRSW(LastVmeReadData_b32), 2'b01);
	    `CHECK_EQUAL(i_VfcHdSlot1.Irq_onb7, '1);
	 end // for (int motor=0; motor < NUMBER_OF_MOTORS_PER_FIBER)

	 #10us;
	 // global trigger - all motors should trigger at the same
	 // time, and we should observe that all the motors irqs come
	 // at the same time
	 fork
	    begin
	       Write(APPGLOBALTRIGGER, 32'h1);
	       #10ms;
	    end
	    #1ms;
	    @(negedge i_VfcHdSlot1.Irq_onb7[3]);
	 join_any
	 `CHECK_EQUAL(i_VfcHdSlot1.Irq_onb7[3], 0);
	 Read(SYS_INTSTATUS);
	 // all IRQs should be triggered:
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'hffff);
      end // UNMATCHED !!

      `TEST_CASE("irq_when_motor_in_switch") begin
	 // we test here whether if IRQ is enable, switch into
	 // positive direction is engaged and we try to move the motor
	 // positive direction, whether we get the IRQ
	 // (we should)

	 // enable IRQ:
	 Write(SYS_INTMASK, 0);
	 // IRQ level:
	 Write(SYS_INTLEVEL, 3);
	 // IRQ vector:
	 Write(SYS_INTVECTOR, 32'h84);
	 // IRQ enable:
	 Write(SYS_INTCONFIG, 1);

	 // configure switches of the L1M1 such, that motor is IN the
	 // switch position, hence any start results in 'no
	 // move'. Setup motor L1M1 to perform 500 steps.
	 setupMotor(0, 0, 5);
	 // setup switches. By default rawswitches are coming from mc
	 // stream. both of them at logical 1. We want to setup
	 // positive direction of the movement (dir=0), and raw
	 // switches such, that in positive direction the switch is actuated
	 Write(L1M1 + (MDIRECTION * 4), 0);
	 // switch actuated means that in positive direction there is
	 // no inversion (so logical positive switch is engaged = 1),
	 // while negative direction we show disengaged switch by
	 // enforcing inversion:
	 Write(L1M1 + (MSWITCHESCONFIG * 4), 32'b1_10_0_01);
	 // switches raw values are 'no care', but logical values
	 // after some time should say that both of them are engaged
	 Read(L1M1 + (MMSTATUS * 4));
	 // check for positive dir switch being engaged, and irqs not
	 // in action:
	 `CHECK_EQUAL(MRSW(LastVmeReadData_b32), 2'b01);
	 `CHECK_EQUAL(i_VfcHdSlot1.Irq_onb7, '1);

	 // we can trigger the motor now, and wait for IRQ to come
	 // (for some time)
	 fork
	    begin
	       Write(L1M1 + (MMSTART * 4), 1);
	       // after this writing busy should never go high, we do
	       // lazy test here:
	       forever begin
		  #1us;
		  Read(L1M1 + (MMSTATUS * 4));
		  `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 0);
	       end
	    end
	    #1ms;
	    @(negedge i_VfcHdSlot1.Irq_onb7[3]);
	 join_any
	 `CHECK_EQUAL(i_VfcHdSlot1.Irq_onb7[3], 0);
      end // UNMATCHED !!


      `TEST_CASE("irq_when_motor_in_opposite_switch") begin
	 // we test here whether if IRQ is enable, only positive
	 // switch is engaged and we try to move negative
	 // direction. Motor should start and we should get IRQ at the
	 // end.

	 // enable IRQ:
	 Write(SYS_INTMASK, 0);
	 // IRQ level:
	 Write(SYS_INTLEVEL, 3);
	 // IRQ vector:
	 Write(SYS_INTVECTOR, 32'h84);
	 // IRQ enable:
	 Write(SYS_INTCONFIG, 1);

	 // configure switches of the L1M1 such, that motor is IN the
	 // switch position, hence any start results in 'no
	 // move'. Setup motor L1M1 to perform 500 steps.
	 setupMotor(0, 0, 5);
	 // setup switches. By default rawswitches are coming from mc
	 // stream. both of them at logical 1. We want to setup
	 // positive direction of the movement (dir=0), and raw
	 // switches such, that in positive direction the switch is actuated
	 Write(L1M1 + (MDIRECTION * 4), 1);
	 // switch actuated means that in positive direction there is
	 // no inversion (so logical positive switch is engaged = 1),
	 // while negative direction we show disengaged switch by
	 // enforcing inversion:
	 Write(L1M1 + (MSWITCHESCONFIG * 4), 32'b1_10_0_01);
	 // switches raw values are 'no care', but logical values
	 // after some time should say that both of them are engaged
	 Read(L1M1 + (MMSTATUS * 4));
	 // check for positive dir switch being engaged, and irqs not
	 // in action:
	 `CHECK_EQUAL(MRSW(LastVmeReadData_b32), 2'b01);
	 `CHECK_EQUAL(i_VfcHdSlot1.Irq_onb7, '1);

	 // we can trigger the motor now, and wait for busy to come
	 // (for some time)
	 fork
	    begin
	       Write(L1M1 + (MMSTART * 4), 1);
	       #10ms;
	    end
	    #1ms;
	    @(posedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o);
	 join_any
	 `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o,
		      1);
	 // having busy we wait for IRQ:
	 fork
	    @(negedge i_VfcHdSlot1.Irq_onb7[3]);
	    @(negedge i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o);
	 join_any
	 // IRQ has to be received still when BUSY turned on, as it is
	 // part of the cycle (note: we should use here only vme-style
	 // commands to make this proper top-level TB, but then it
	 // would be difficult to catch changes on edges within application)
	 `CHECK_EQUAL(i_VfcHdSlot1.Irq_onb7[3], 0);
	 `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o,
		      1);

      end // UNMATCHED !!

      `TEST_CASE("irq_emulator_test") begin
	 // read magic of appropriate register
	 // this is the 8th register of global register set, which is
	 // fixed to 'beefcafe
	 Read(APPGLOBALTRIGGER + (7*4));
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'hbeefcafe, "WRONG MAGIC");
	 // enable IRQs:
	 // mask:
	 Write(SYS_INTMASK, 0);
	 // IRQ level:
	 Write(SYS_INTLEVEL, 3);
	 // IRQ vector:
	 Write(SYS_INTVECTOR, 32'h84);
	 // IRQ enable:
	 Write(SYS_INTCONFIG, 1);
	 // check irq state:
	 `CHECK_EQUAL(i_VfcHdSlot1.Irq_onb7, 7'b1111111, "IRQ lines are not off by default");
	 // write into emulator enable
	 Write(APPIRQEMULATOR, '1);
	 Read(APPIRQEMULATOR);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'h00000001, "ENABLE EMULATOR FAIL");
	 // wait for some cycles to assert IRQ
	 #1us;
	 force i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.IRQEmulator_ob24 = 8;
	 #10us;
	 `CHECK_EQUAL(i_VfcHdSlot1.Irq_onb7, 7'b1111011, "IRQ did not fire");
      end

      `TEST_CASE("all_motors_start") begin
	 // let's schedule all registers of each motor and each link to start,
	 // and then fire them at the same time
	 setupAllMotors(64);

	 for(int link = 0; link < NUMBER_OF_GBT_LINKS; link++)
	   for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	      Read(getAddr(link, i, MMSTAT));
	      `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 0,
			   "ALL MOTORS SHOULD BE TURNED OFF");
	   end

	 // DO NOT FIRE
	 Write(APPGLOBALTRIGGER, 32'h11111110);
	 for(int link = 0; link < NUMBER_OF_GBT_LINKS; link++)
	   for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	      Read(getAddr(link, i, MMSTAT));
	      `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 0,
			   "ALL MOTORS SHOULD BE TURNED OFF");
	   end

	 // and FIREEEE!, but this time using 'arbitrarily long' external
	 // trigger.
	 fook = 1;
	 #300ns;
	 fook = 0;

	 for(int link = 0; link < NUMBER_OF_GBT_LINKS; link++)
	   for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	      Read(getAddr(link, i, MMSTAT));
	      `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 1,
			   "ALL MOTORS SHOULD BE TURNED ON");
	   end

      end


      `TEST_CASE("inhibit_external_trigger_when_pattern_mode") begin
	 setupMotor(0, 0, 64);
	 // now TURN ON PATTERN MODE - if turned on, we're not able to
	 // trigger
	 Write(APPSFPGBTUSRDATCTRLREG, 32'hf000_0000);
	 // and now first the assertion in mcoi should trigger if we
	 // do not get any pattern up,
	 #1us;
	 forever begin
	    Read(APPSFPGBTUSRDATSTATREG);
	    if (ISPATTERNON(LastVmeReadData_b32) == '1) break;
	 end
	 #1us;
	 // triggering here - it should not happen as we're in pattern mode
	 fook = 1;
	 #300ns;
	 fook = 0;
	 // and point here: we wait for like 10 us and check for busy,
	 // which SHOULD NOT BE ASSERTED!
	 #10us;
	 `CHECK_EQUAL(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.Busy_o,
		      0,
		      "TRIGGER WORKED AND SHOULD NOT IN PATTERN MODE");

	 $display("Triggering internal move");

	 // and while being in pattern mode, we try to trigger as well
	 // using software internal trigger, which should not work
	 // either
	 Write(APPGLOBALTRIGGER, 1);
	 // we wait for some time - if motor works, then something
	 // fishy there
	 #1us;

	 Read(getAddr(0, 0, MMSTAT));
	 `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 0,
		      "Motor should not work using global trigger in pattern");

	 // and now the most difficult one: trigger the motor by motor
	 // command movement. This gets difficult as 'top level' does
	 // not know that there was such event, and the inhibit signal
	 // has to be injected somehow into the command
	 // queuer. Register 8, bit 0 = startmove
	 Write(getAddr(0, 0, MTRG), 1);
	 Read(getAddr(0, 0, MMSTAT));
	 `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 0,
		      "Motor should not work using single trigger");


      end // UNMATCHED !!


      `TEST_CASE("global_external_trigger_through_lemo1") begin
	 // let's schedule all registers of each motor and each link to start,
	 // and then fire them at the same time
	 setupAllMotors(64);

	 for(int link = 0; link < NUMBER_OF_GBT_LINKS; link++)
	   for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	      Read(getAddr(link, i, MMSTAT));
	      `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 0,
			   "ALL MOTORS SHOULD BE TURNED OFF");
	   end

	 // DO NOT FIRE
	 Write(APPGLOBALTRIGGER, 32'h11111110);
	 for(int link = 0; link < NUMBER_OF_GBT_LINKS; link++)
	   for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	      Read(getAddr(link, i, MMSTAT));
	      `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 0,
			   "ALL MOTORS SHOULD BE TURNED OFF");
	   end

	 // and FIREEEE!, but this time using 'arbitrarily long' external
	 // trigger.
	 fook = 1;
	 #300ns;
	 fook = 0;

	 for(int link = 0; link < NUMBER_OF_GBT_LINKS; link++)
	   for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	      Read(getAddr(link, i, MMSTAT));
	      `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 1,
			   "ALL MOTORS SHOULD BE TURNED ON");
	   end

      end

      `TEST_CASE("global_internal_trigger") begin
 	 // let's schedule all registers of each motor and each link to start,
	 // and then fire them at the same time
	 setupAllMotors(64);

	 for(int link = 0; link < NUMBER_OF_GBT_LINKS; link++)
	   for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	      Read(getAddr(link, i, MMSTAT));
	      `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 0,
			   "ALL MOTORS SHOULD BE TURNED OFF");
	   end

	 // DO NOT FIRE
	 Write(APPGLOBALTRIGGER, 32'h11111110);
	 for(int link = 0; link < NUMBER_OF_GBT_LINKS; link++)
	   for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	      Read(getAddr(link, i, MMSTAT));
	      `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 0,
			   "ALL MOTORS SHOULD BE TURNED OFF");
	   end

	 // and FIREEEE!
	 Write(APPGLOBALTRIGGER, 1);
	 for(int link = 0; link < NUMBER_OF_GBT_LINKS; link++)
	   for(int i = 0; i < NUMBER_OF_MOTORS_PER_FIBER; i++) begin
	      Read(getAddr(link, i, MMSTAT));
	      `CHECK_EQUAL(MRBUSY(LastVmeReadData_b32), 1,
			   "ALL MOTORS SHOULD BE TURNED ON");
	   end

      end

      `TEST_CASE("global_register_set") begin
	 // global registers position - got from default
	 // values. There's magic at this position. All other global
	 // registers are part of emulation layer
	 $display("%x", c_AppAddrOffset + c_AppGlobalRegisters + (6*4));

	 Read(c_AppAddrOffset + c_AppGlobalRegisters + (7*4));
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'hbeefcafe, "WRONG MAGIC");
	 Read(c_AppAddrOffset + c_AppGlobalRegisters + (6*4));
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'hbeefcafe, "WRONG MAGIC");
      end

      `TEST_CASE("read_registers_declarations") begin

	 for(int link=0; link < 4; link++) begin
	    $display("Link ", link);
	    for(int motor=0; motor < 17; motor++) begin
	       $display("Checking motor %d", motor);
	       // so this number is:
	       // 1) address offset is offset to the app space of
	       // the vme address space
	       // 2) 2000h is where the 4 links controllers start their wishbone
	       // interfaces
	       // 3) each motor has exported 32 register space
	       // 4) +15 is the local address of the status/identifier register
	       // and the point is, that motor control number is hidden in the
	       // specific bits
	       if (motor < 16) begin
		  Read(getAddr(link, motor, MIDENTIFIER));
		  `CHECK_EQUAL(MRIDENTIFIER(LastVmeReadData_b32), motor,
			       "MOTOR MODULE IDENTIFIER does not fit");
	       end
	       else if (motor == 16) begin
		  // this is link register layer, we have to read register0, which
		  // identifies interface number and other registers length of the
		  // queue
		  Read(getAddr(link, motor, MSTEPS));
		  `CHECK_EQUAL(LastVmeReadData_b32, link+10,
			       "LINK LAYER IDENTIFIER DOES NOT FIT");
		  // check if - how the queuelength and fifo was instantiated -
		  // whether it is correctly exported to a register
		  Read(getAddr(link, motor, MDIRECTION));
		  `CHECK_EQUAL(LastVmeReadData_b32, 32'h20,
			       "QUEUE DEPTH DOES NOT FIT");
	       end

	    end // for (int motor=0; motor < 16; motor++)
	 end
      end

      `TEST_CASE("application_example_registers_write") begin
	 Read(TAG);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'haaaaaaaa,
		      "EXAMPLE REGISTERS FAILED TO READ");
	 // then there is 1 register used for slow control,
	 // which we cannot check, and
	 // one register here, which is currently not used:
	 Read(24'h81_003C);
	 `CHECK_EQUAL(LastVmeReadData_b32, 0,
		      "EXAMPLE REGISTERS FAILED TO READ");
	 for(int i=0; i < 4; i++)
	   Write(TAG + 4*i, i*4);

	 Read(TAG);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'haaaaaaaa,
		      "EXAMPLE REGISTERS FAILED TO READ");
	 Read(24'h81_003C);
	 `CHECK_EQUAL(LastVmeReadData_b32, 12,
		      "EXAMPLE REGISTERS FAILED TO READ");
      end // UNMATCHED !!

      `TEST_CASE("application_example_registers_read") begin
	 // accessing 'example registers' is a good way how to check validity of
	 // memory map for default base application registers (spreading from
	 // 0x0000-> 0x1fff in local addressing)
	 // these are read-only:
	 Read(TAG);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'haaaaaaaa,
		      "EXAMPLE REGISTERS FAILED TO READ");
      end

      `TEST_CASE("appinfo_constant_readback") begin
	 for(int i = 0; i < 11; i++) begin
	    Read(SYS_APPINFO + 4*i);
	    `CHECK_EQUAL(LastVmeReadData_b32, checkVector[i],
	    		 "appinfo read failed");
	 end
      end

      `TEST_CASE("app_release_check") begin
	 Read(SYS_APPREVID);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'h04030201, "appinfo read failed");
      end

      `TEST_CASE("system_info_read") begin
	 // should read in ascii VFC-HD
	 Read(SYS_SYSINFO);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'h5646432d, "systeminfo read failed");
	 Read(SYS_SYSINFO + 4);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'h48440000, "systeminfo read failed");
	 Read(SYS_SYSINFO + 8);
	 `CHECK_EQUAL(LastVmeReadData_b32, 0, "systeminfo read failed");
      end

      `TEST_CASE("system_release_read") begin
	 Read(SYS_SYSREVID);
	 `CHECK_EQUAL(LastVmeReadData_b32, 32'h17042604, "SYS ID read failed");
      end

   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(100000ms);



   //=== VME bus and transaction models ====\\
   VmeBusModule
     #(
       // Parameters
       .g_NSlots			(g_NSlots),
       .g_VerboseDefault		(g_VerboseDefault),
       .g_Timeout			(g_Timeout))
   i_VmeBus
     (
      // Inouts
      .As_n				(As_in),
      .AM_b6				(AM_ib6),
      .A_b31				(A_iob31[31:1]),
      .LWord				(LWord_io),
      .Ds_nb2				(Ds_inb2),
      .Wr_n				(Wr_in),
      .D_b32				(D_iob32),
      .DtAck_n				(DtAck_on),
      .Irq_nb7				(Irq_onb7),
      .Iack_n				(Iack_in),
      .IackIn_nb			(IackIn_in),
      .IackOut_nb			(IackOut_on),
      .SysResetN_irn			(SysResetN_irn),
      .SysClk_k				(SysClk_ik),
      .Ga_nmb5				(Ga_ionb5),
      .Gap_nbm				(Gap_nbm));

   VfcHd_v3_0
     i_VfcHdSlot1
       (
	.Ga_ionb5				(Ga_ionb5[4]),
	.Gap_ion				(Gap_nbm[4]),

	/*AUTOINST*/
	// Outputs
	.DtAck_on			(DtAck_on),
	.Irq_onb7			(Irq_onb7[7:1]),
	.IackOut_on			(IackOut_on),
	.FmcTck_ok			(FmcTck_ok),
	.FmcTms_o			(FmcTms_o),
	.FmcTdi_o			(FmcTdi_o),
	.FmcTrstL_orn			(FmcTrstL_orn),
	.FmcPgC2m_o			(FmcPgC2m_o),
	.FmcDpC2m_ob10			(FmcDpC2m_ob10[9:0]),
	.FmcGa0_o			(FmcGa0_o),
	.FmcGa1_o			(FmcGa1_o),
	.AppSfpTx1_ob			(AppSfpTx1_ob),
	.AppSfpTx2_ob			(AppSfpTx2_ob),
	.AppSfpTx3_ob			(AppSfpTx3_ob),
	.AppSfpTx4_ob			(AppSfpTx4_ob),
	// Inouts
	.A_iob31			(A_iob31[31:1]),
	.LWord_io			(LWord_io),
	.D_iob32			(D_iob32[31:0]),
	.FmcLaP_iob34			(FmcLaP_iob34[33:0]),
	.FmcLaN_iob34			(FmcLaN_iob34[33:0]),
	.FmcHaP_iob24			(FmcHaP_iob24[23:0]),
	.FmcHaN_iob24			(FmcHaN_iob24[23:0]),
	.FmcHbP_iob22			(FmcHbP_iob22[21:0]),
	.FmcHbN_iob22			(FmcHbN_iob22[21:0]),
	.FmcScl_iok			(FmcScl_iok),
	.FmcSda_io			(FmcSda_io),
	.FmcClk2Bidir_iok		(FmcClk2Bidir_iok),
	.FmcClk3Bidir_iok		(FmcClk3Bidir_iok),
	.GpIoLemo_iob4			(GpIoLemo_iob4[4:1]),
	// Inputs
	.As_in				(As_in),
	.AM_ib6				(AM_ib6[5:0]),
	.Ds_inb2			(Ds_inb2[1:0]),
	.Wr_in				(Wr_in),
	.Iack_in			(Iack_in),
	.IackIn_in			(IackIn_in),
	.SysResetN_irn			(SysResetN_irn),
	.SysClk_ik			(SysClk_ik),
	.FmcPrsntM2c_in			(FmcPrsntM2c_in),
	.FmcTdo_i			(FmcTdo_i),
	.FmcPgM2c_i			(FmcPgM2c_i),
	.FmcClk0M2cCmos_ik		(FmcClk0M2cCmos_ik),
	.FmcClk1M2cCmos_ik		(FmcClk1M2cCmos_ik),
	.FmcClkDir_i			(FmcClkDir_i),
	.FmcDpM2c_ib10			(FmcDpM2c_ib10[9:0]),
	.FmcGbtClk0M2c_ik		(FmcGbtClk0M2c_ik),
	.FmcGbtClk1M2c_ik		(FmcGbtClk1M2c_ik),
	.AppSfpRx1_ib			(AppSfpRx1_ib),
	.AppSfpRx2_ib			(AppSfpRx2_ib),
	.AppSfpRx3_ib			(AppSfpRx3_ib),
	.AppSfpRx4_ib			(AppSfpRx4_ib),
	.DipSw_ib8			(DipSw_ib8[8:1]),
	.PushButton_i			(PushButton_i));


   logic [1:0] ScEc_ib2, ScEc_ob2;
   logic [79:0] GBT_ib80, GBT_ob80;

   // this guy hooks on L1 optical stream and decomposes it into ScEc
   // and GBT data. NOTE THAT CLOCK TO USE WITH THIS IS THE SAME AS
   // THE ONE USED FOR DATA GENERATION BY CONVERTERS IN DUMMY GBT
   logic 	DataFromVFCValid;

   serial_converter
   i_serial_converter
     (
      // Outputs
      .AppSfpRx1_ob			(AppSfpRx1_ib),
      .ScEc_ob2				(ScEc_ob2[1:0]),
      .GBT_ob80				(GBT_ob80[79:0]),
      .RxIsData_i (GbtxTxDataValid_o),
      .TxIsData_o (DataFromVFCValid),
      // Inputs
      .ClkRs_ix				(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix),
      .AppSfpTx1_ib			(AppSfpTx1_ob),
      .ScEc_ib2				(ScEc_ib2[1:0]),
      .GBT_ib80				(GBT_ib80[79:0]));

   // create gefe 25MHz clock:
   always forever #(20ns) Osc25MhzCg_ik <= ~Osc25MhzCg_ik;
   initial begin
      GeneralReset_iran = '0;
      #123ns;
      GeneralReset_iran = '1;
   end

   localparam g_clock_divider = 25000;


   GefeApplication
     #(/*AUTOINSTPARAM*/
       // Parameters
       .g_clock_divider			(g_clock_divider))
   i_GefeApplication
     (
      // cast serial data from/to VFC
      .DataToGbtx_ob80			(GBT_ib80),
      .DataToGbtxSc_ob2			(ScEc_ib2),
      .DataFromGbtx_ib80		(GBT_ob80),
      .DataFromGbtxSc_ib2		(ScEc_ob2),
      // we say 'link is ready'
      .OptoLosReset_iran		(GeneralReset_iran),
      // and provide two clocks:
      .Osc25MhzCg_ik			(Osc25MhzCg_ik),
      .GbtxElinksDclkCg_ik (i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix.clk),
      // other signals:
      .V1p5PowerGood_i			('1),
      .V2p5PowerGood_i			('1),
      .V3p3PowerGood_i			('1),
      .GbtxRxDataValid_i		(DataFromVFCValid),

      /*AUTOINST*/
      // Outputs
      .GbtxReset_or			(GbtxReset_or),
      .GbtxTxDataValid_o		(GbtxTxDataValid_o),
      .LemoGpioDir_o			(LemoGpioDir_o),
      .Leds_onb6			(Leds_onb6[0:5]),
      // Inouts
      .FmcLa_iob34p			(FmcLa_iob34p[33:0]),
      .FmcLa_iob34n			(FmcLa_iob34n[33:0]),
      .FmcHa_iob24p			(FmcHa_iob24p[23:0]),
      .FmcHa_iob24n			(FmcHa_iob24n[23:0]),
      .FmcHb_iob22p			(FmcHb_iob22p[21:0]),
      .FmcHb_iob22n			(FmcHb_iob22n[21:0]),
      .LemoGpioQg_iok			(LemoGpioQg_iok),
      // Inputs
      .GbtxRxRdy_i			(GbtxRxRdy_i),
      .GbtxTxRdy_i			(GbtxTxRdy_i),
      .GeneralReset_iran		(GeneralReset_iran));

   logic supplyOK_i;

   assign supplyOK_i = '1;
   // display interface - dummy. we do not emulate this.
   t_display display();


   // connection of FMC mezzanine signals from GEFE to some
   // 'reasonable' structure, so we can format the motor control as we
   // want
   reverse_pin_mapping
     #(/*AUTOINSTPARAM*/)
   i_reverse_pin_mapping
     (/*AUTOINST*/
      // Interfaces
      .display				(display),
      // Outputs
      .motorControl_ob			(motorControl_ob[NUMBER_OF_MOTORS_PER_FIBER:1]),
      .mreset_o				(mreset_o),
      .test_ob8				(test_ob8[7:0]),
      // Inouts
      .FmcLa_iob34p			(FmcLa_iob34p[33:0]),
      .FmcLa_iob34n			(FmcLa_iob34n[33:0]),
      .FmcHb_iob22p			(FmcHb_iob22p[21:0]),
      .FmcHb_iob22n			(FmcHb_iob22n[21:0]),
      .FmcHa_iob24p			(FmcHa_iob24p[23:0]),
      .FmcHa_iob24n			(FmcHa_iob24n[23:0]),
      // Inputs
      .motorStatus_ib			(motorStatus_ib[NUMBER_OF_MOTORS_PER_FIBER:1]),
      .supplyOK_i			(supplyOK_i),
      .PCBrevision_i4			(PCBrevision_i4[3:0]));

   assign PCBrevision_i4 = 4'ha;

   // initialization of motor control structures. This thing will
   // initialize all FMC pins for direction from motor to VFC.
   initial for(int i = 1; i <= NUMBER_OF_MOTORS_PER_FIBER; i++) begin
      motorStatus_ib[i].OH_i = '0;
      motorStatus_ib[i].StepPFail_i = '0;
      motorStatus_ib[i].RawSwitches_b2 = '1;
   end


   logic [2:1] ScEc_dummy_3b2[2:NUMBER_OF_GBT_LINKS];

   // ScEc links in loopback mode, 80bit data busses in 'motor
   // constant emulation' mode
   serial_converter
   i_serial_converter_d2
     (
      // Outputs
      .AppSfpRx1_ob			(AppSfpRx2_ib),
      .ScEc_ob2				(ScEc_dummy_3b2[2]),
      // on these links we don't care about data we get out
      .GBT_ob80				(),
      .RxIsData_i ('0),
      .TxIsData_o (),
      // Inputs
      .ClkRs_ix				(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix),
      .AppSfpTx1_ib			(AppSfpTx2_ob),
      .ScEc_ib2				(ScEc_dummy_3b2[2]),
      // but we care about what we're sending in. These are in
      // particular switches in 'normally open' mode
      .GBT_ib80				(80'h33333333333333330000));

   serial_converter
   i_serial_converter_d3
     (
      // Outputs
      .AppSfpRx1_ob			(AppSfpRx3_ib),
      .ScEc_ob2				(ScEc_dummy_3b2[3]),
      // on these links we don't care about data we get out
      .GBT_ob80				(),
      .RxIsData_i ('0),
      .TxIsData_o (),
      // Inputs
      .ClkRs_ix				(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix),
      .AppSfpTx1_ib			(AppSfpTx3_ob),
      .ScEc_ib2				(ScEc_dummy_3b2[3]),
      // but we care about what we're sending in. These are in
      // particular switches in 'normally open' mode
      .GBT_ib80				(80'h33333333333333330000));

   serial_converter
   i_serial_converter_d4
     (
      // Outputs
      .AppSfpRx1_ob			(AppSfpRx4_ib),
      .ScEc_ob2				(ScEc_dummy_3b2[4]),
      // on these links we don't care about data we get out
      .GBT_ob80				(),
      .RxIsData_i ('0),
      .TxIsData_o (),
      // Inputs
      .ClkRs_ix				(i_VfcHdSlot1.i_Fpga.i_VfcHdApplication.ClkRsGBT40MHz_ix),
      .AppSfpTx1_ib			(AppSfpTx4_ob),
      .ScEc_ib2				(ScEc_dummy_3b2[4]),
      // but we care about what we're sending in. These are in
      // particular switches in 'normally open' mode
      .GBT_ib80				(80'h33333333333333330000));

   // import gefe build number so we can chech scec
   build_number
     #(/*AUTOINSTPARAM*/)
   i_build_number
     (/*AUTOINST*/
      // Outputs
      .build_ob32			(build_ob32[31:0]));

endmodule
