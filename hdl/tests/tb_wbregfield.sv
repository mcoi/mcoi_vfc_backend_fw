//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_WBTRIGREGFIELD.SV
// @brief Unit testing for triggerable/settable wishbone registers
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 23 October 2017
// @details This is a test for a standard BI component library. Functionality of
// the register block is essential for our project, hence we do additional unit
// testing to check for desired behaviour. Should sometime in the future this
// entity change, these tests will validate the module integrity.
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

// Unit testing for triggerable/settable wishbone registers
module tb_wbregfield;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   localparam g_NumberOfRegisters = 4;
   localparam g_DataWidth = 32;
   localparam logic [g_NumberOfRegisters-1:0] g_DirectionMiso_b = 'b1100;

   ckrs_t ClkRs_ix;

   t_WbInterface #(.g_DataWidth(g_DataWidth),
		   .g_AddressWidth(32)) Wb_iot(ClkRs_ix.clk, ClkRs_ix.reset);
   logic [g_DataWidth-1:0] 		      ReadData;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic [g_DataWidth-1:0] Register_omb [0:g_NumberOfRegisters-1];// From DUT of WbRegField.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic [g_DataWidth-1:0] Register_imb [0:g_NumberOfRegisters-1];// To DUT of WbRegField.v
   // End of automatics

   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking


   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
        i_WbMasterSim.Reset();
      end

      `TEST_CASE("debug_test") begin
	 // register1, hiword is clearable (trigger). Usage of WbWrite does not
	 // allow checking for single pulse transacation on register,but we can
	 // verify clearout
	 for(int i = 0; i < 4; i++)
	   i_WbMasterSim.WbWrite(i, i);

         i_WbMasterSim.WbRead(0, ReadData);
	 // only some bits work as trigger switches
         `CHECK_EQUAL(ReadData, 0);
         i_WbMasterSim.WbRead(1, ReadData);
	 // only some bits work as trigger switches
         `CHECK_EQUAL(ReadData, 1);
	 // other two are undefined as we do not have write to write into them!
         i_WbMasterSim.WbRead(2, ReadData);
	 // only some bits work as trigger switches
         `CHECK_EQUAL(ReadData, 32'hx);
         i_WbMasterSim.WbRead(3, ReadData);
	 // only some bits work as trigger switches
         `CHECK_EQUAL(ReadData, 32'hx);
      end

      `TEST_CASE("writing_into_read_only_is_undefined") begin
	 // register1, hiword is clearable (trigger). Usage of WbWrite does not
	 // allow checking for single pulse transacation on register,but we can
	 // verify clearout
	 for(int i = 0; i < 4; i++)
	   i_WbMasterSim.WbWrite(i, i);

         i_WbMasterSim.WbRead(0, ReadData);
	 // only some bits work as trigger switches
         `CHECK_EQUAL(ReadData, 0);
         i_WbMasterSim.WbRead(1, ReadData);
	 // only some bits work as trigger switches
         `CHECK_EQUAL(ReadData, 1);
	 // other two are undefined as we do not have write to write into them!
         i_WbMasterSim.WbRead(2, ReadData);
	 // only some bits work as trigger switches
         `CHECK_EQUAL(ReadData, 32'hx);
         i_WbMasterSim.WbRead(3, ReadData);
	 // only some bits work as trigger switches
         `CHECK_EQUAL(ReadData, 32'hx);
	 // define them
	 Register_imb[2] = 32'h12192837;
	 Register_imb[3] = 32'h82701983;
         i_WbMasterSim.WbRead(2, ReadData);
	 // only some bits work as trigger switches
         `CHECK_EQUAL(ReadData, 32'h12192837);
         i_WbMasterSim.WbRead(3, ReadData);
	 // only some bits work as trigger switches
         `CHECK_EQUAL(ReadData, 32'h82701983);
      end

      `TEST_CASE("triggering_and_clearout") begin
	 // register1, hiword is clearable (trigger). Usage of WbWrite does not
	 // allow checking for single pulse transacation on register,but we can
	 // verify clearout
	 i_WbMasterSim.WbWrite(1, 32'hffffffff);
         i_WbMasterSim.WbRead(1, ReadData);
	 // only some bits work as trigger switches
         `CHECK_EQUAL(Register_omb[1], 32'heeeeffff);
      end

      `TEST_CASE("read_from") begin
	 // write into the registers ordinary values
	 Register_imb[0] = 123;
	 Register_imb[1] = 456;
	 Register_imb[2] = 32'h12192837;
	 Register_imb[3] = 32'h82701983;


	 // and read back, those which are writable should contain old content
         i_WbMasterSim.WbRead(0, ReadData);
         `CHECK_EQUAL(Register_omb[0], 10);
         i_WbMasterSim.WbRead(0, ReadData);
         `CHECK_EQUAL(Register_omb[1], 20);
         i_WbMasterSim.WbRead(0, ReadData);
         `CHECK_EQUAL(Register_omb[2], 32'h12192837);
         i_WbMasterSim.WbRead(0, ReadData);
         `CHECK_EQUAL(Register_omb[3], 32'h82701983);
      end

      `TEST_CASE("write_into") begin
	 // write into the registers ordinary values
	 Register_imb[2] = 32'h55aaaa55;
	 Register_imb[3] = 32'hdeadbeef;

         i_WbMasterSim.WbWrite(0, 1);
         i_WbMasterSim.WbWrite(1, 8);
	 // only two registers are writable by us, following are NOT
         i_WbMasterSim.WbWrite(2, 32);
         i_WbMasterSim.WbWrite(3, 128);

	 // and read back
         i_WbMasterSim.WbRead(0, ReadData);
         `CHECK_EQUAL(Register_omb[0], 1);
         i_WbMasterSim.WbRead(0, ReadData);
         `CHECK_EQUAL(Register_omb[1], 8);
         i_WbMasterSim.WbRead(0, ReadData);
         `CHECK_EQUAL(Register_omb[2], 32'h55aaaa55);
         i_WbMasterSim.WbRead(0, ReadData);
         `CHECK_EQUAL(Register_omb[3], 32'hdeadbeef);
      end

      `TEST_CASE("POR_initialization") begin
         `CHECK_EQUAL(Register_omb[0], 10);
	 // this should clear to zero?
         `CHECK_EQUAL(Register_omb[1], 20);
         `CHECK_EQUAL(Register_omb[2], 30);
         `CHECK_EQUAL(Register_omb[3], 40);
      end
   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10ms);

   WbMasterSim
   i_WbMasterSim (// Outputs
		  .Rst_orq		(ClkRs_ix.reset),
		  .Adr_obq32		(Wb_iot.Adr_b),
		  .Dat_obq32		(Wb_iot.DatMoSi_b),
		  .We_oq		(Wb_iot.We),
		  .Cyc_oq		(Wb_iot.Cyc),
		  .Stb_oq		(Wb_iot.Stb),
		  // Inputs
		  .Clk_ik		(ClkRs_ix.clk),
		  .Dat_ib32		(Wb_iot.DatMiSo_b),
		  .Ack_i		(Wb_iot.Ack));

   assign Wb_iot.Sel_b = 4'hf;

   WbRegField #(// Parameters
		    .g_NumberOfRegisters(g_NumberOfRegisters),
		    .g_DataWidth	(g_DataWidth/*.[g_NumberOfRegisters-1:0]*/),
		    .g_DirectionMiso_b	(g_DirectionMiso_b/*.[g_DataWidth-1:0]*/),
		    .g_DefaultValue_mb	('{(g_DataWidth)'(10),
					   (g_DataWidth)'(20),
					   (g_DataWidth)'(30),
					   (g_DataWidth)'(40)}),
		    .g_AutoClrMask_mb('{(g_DataWidth)'(0),
					(g_DataWidth)'(32'h11110000),
					(g_DataWidth)'(0),
					(g_DataWidth)'(0)})) DUT
     (/*AUTOINST*/
      // Interfaces
      .Wb_iot				(Wb_iot),
      // Outputs
      .Register_omb			(Register_omb/*[g_DataWidth-1:0].[0:g_NumberOfRegisters-1]*/),
      // Inputs
      .Register_imb			(Register_imb/*[g_DataWidth-1:0].[0:g_NumberOfRegisters-1]*/));

endmodule
