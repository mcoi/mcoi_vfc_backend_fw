//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_COMMANDQUEUER.SV
// @brief Verifies functionality of command queue
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 16 October 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;
import MCPkg::*;


// Verifies functionality of command queue
module tb_commandqueuer;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};
   localparam QUEUELENGTH = 8;


   function command_t DataBlock(input int step = 0,
				 input logic [1:0] movingtrig = '1);
      begin
	    DataBlock.StepNumber_b32 = step;
	    DataBlock.Direction = '1;
	    DataBlock.LowSpeed_b36 = 36'b111;
	    DataBlock.HighSpeed_b36 = 36'b111001;
	    DataBlock.AccDeccRate_b19 = 19'b1010101001010101;
	    DataBlock.Trail_b32 = 32'hdeadbeef;
	    DataBlock.globaltrigger = movingtrig;
	    DataBlock.GenInterrupt = '0;
	    DataBlock.powerconfig = 3'b101;
      end
   endfunction // DataBlock


   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   command_t		OCmdParams_x;		// From DUT of commandqueuer.v
   queuestatus_t	QueueStatus_x;		// From DUT of commandqueuer.v
   motorcommand_t		OTriggerParams_x;			// From DUT of commandqueuer.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   command_t		ICmdParams_x;		// To DUT of commandqueuer.v
   logic		Done_i;			// To DUT of commandqueuer.v
   motorcommand_t		ITriggerParams_x;			// To DUT of commandqueuer.v
   // End of automatics

   int 			x;

   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 ##1;
	 Done_i <= '0;
	 ITriggerParams_x <= '0;
	 ICmdParams_x <= DataBlock();

	 ClkRs_ix.reset = 1;
	 ##1;
	 ClkRs_ix.reset = 0;
	 ##1;

      end

      `TEST_CASE("debug_test") begin
	 // testing if queue counter works OK
	 ITriggerParams_x <= '0;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 0);
	 ITriggerParams_x.DoQueue <= 1;
	 // fill the queue
	 for (int stepnumber=0; stepnumber < 5; stepnumber++) begin
	    ICmdParams_x <= DataBlock(stepnumber, '0);
	    ##1;
	    // -1 because here we catch the first 'doqueue', hence at
	    // this point counter should be still at zero, and only
	    // next clock cycle should be at one
	    `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, stepnumber + 1);
	 end
	 ITriggerParams_x.DoQueue <= 0;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 5);
	 ##10;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 5);
	 ITriggerParams_x.ResetQueue <= 1;
	 ##1;
	 ITriggerParams_x.ResetQueue <= 0;
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 0);
	 ##1;

      end

      `TEST_CASE("queue_reset") begin
	 // testing if queue counter works OK
	 ITriggerParams_x <= '0;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 0);
	 ITriggerParams_x.DoQueue <= 1;
	 // fill the queue
	 for (int stepnumber=0; stepnumber < 5; stepnumber++) begin
	    ICmdParams_x <= DataBlock(stepnumber, '0);
	    ##1;
	    // -1 because here we catch the first 'doqueue', hence at
	    // this point counter should be still at zero, and only
	    // next clock cycle should be at one
	    `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, stepnumber + 1);
	 end
	 ITriggerParams_x.DoQueue <= 0;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 5);
	 ##10;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 5);
	 ITriggerParams_x.ResetQueue <= 1;
	 ##1;
	 ITriggerParams_x.ResetQueue <= 0;
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 0);
	 ##1;
      end

      `TEST_CASE("queue_counting") begin
	 // testing if queue counter works OK
	 ITriggerParams_x <= '0;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 0);
	 ITriggerParams_x.DoQueue <= 1;
	 // fill the queue
	 for (int stepnumber=0; stepnumber < 8; stepnumber++) begin
	    ICmdParams_x <= DataBlock(stepnumber, '0);
	    $display("Step %d", stepnumber);

	    $display("Before : %d", QueueStatus_x.QueueItems_u7);

	    ##1;
	    $display("After : %d", QueueStatus_x.QueueItems_u7);
	    x = stepnumber + 1;

	    `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, x);
	 end
	 `CHECK_EQUAL(QueueStatus_x.QueueFull, 1);
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 8);
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueFull, 1);
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 8);
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueFull, 1);
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 8);
	 ##1;
	 ICmdParams_x <= DataBlock(127, '0);
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 8);
	 ##1;
	 ICmdParams_x <= DataBlock(128, '0);
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 8);
	 ##1;
	 // and now we fill manually few more to see that full flag
	 // should go in
	 ITriggerParams_x.DoQueue <= 0;
	 ICmdParams_x <= DataBlock(32'hff);
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 8);
	 `CHECK_EQUAL(QueueStatus_x.QueueFull, 1);
	 // now remove one sample from the data, this should clear out
	 // fifo full
	 Done_i <= 1;
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueFull, 0);
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 7);
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueFull, 0);
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 6);
	 ##4;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 2);
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 1);
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 0);
	 Done_i <= 0;
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueItems_u7, 0);
      end

      `TEST_CASE("queue_full") begin
	 ITriggerParams_x <= '0;
	 ITriggerParams_x.DoQueue <= 1;
	 // fill the queue
	 for (int stepnumber=0; stepnumber < 8; stepnumber++) begin
	    ICmdParams_x <= DataBlock(stepnumber, '0);
	    ##1;
	 end
	 `CHECK_EQUAL(QueueStatus_x.QueueFull, 1);
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueFull, 1);
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueFull, 1);
	 ##1;
	 ICmdParams_x <= DataBlock(127, '0);
	 ##1;
	 ICmdParams_x <= DataBlock(128, '0);
	 ##1;
	 // and now we fill manually few more to see that full flag
	 // should go in
	 ITriggerParams_x.DoQueue <= 0;
	 ICmdParams_x <= DataBlock(32'hff);
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueFull, 1);
	 // now remove one sample from the data, this should clear out
	 // fifo full
	 Done_i <= 1;
	 ##1;
	 `CHECK_EQUAL(QueueStatus_x.QueueFull, 0);
	 Done_i <= 0;
	 ##1;
      end

      `TEST_CASE("automatic_startmove") begin
	 // we queue X records with autotrigger on execution, so they
	 // should generate startmove automatically
	 ITriggerParams_x <= '0;
	 ITriggerParams_x.DoQueue <= 1;
	 for (int stepnumber=5; stepnumber < 10; stepnumber++) begin
	    ICmdParams_x <= DataBlock(stepnumber, '0);
	    ##1;
	 end
	 ITriggerParams_x.DoQueue <= 0;
	 ICmdParams_x <= DataBlock(32'hff);
	 ##10;
	 `CHECK_EQUAL(OTriggerParams_x.StartMove, 0);
	 // readout queue by issuing 'done'
	 Done_i <= 1;
         `CHECK_EQUAL(OCmdParams_x.StepNumber_b32, 5);
	 ##1;
	 `CHECK_EQUAL(OTriggerParams_x.StartMove, 1);
         `CHECK_EQUAL(OCmdParams_x.StepNumber_b32, 6);
	 ##1;
	 `CHECK_EQUAL(OTriggerParams_x.StartMove, 1);
         `CHECK_EQUAL(OCmdParams_x.StepNumber_b32, 7);
	 ##1;
	 `CHECK_EQUAL(OTriggerParams_x.StartMove, 1);
         `CHECK_EQUAL(OCmdParams_x.StepNumber_b32, 8);
	 ##1;
	 `CHECK_EQUAL(OTriggerParams_x.StartMove, 1);
	 // this is last record to be pulled out of the queue,
	 // once done the startmove should disappear as there are no
	 // more records.
         `CHECK_EQUAL(OCmdParams_x.StepNumber_b32, 9);
	 ##1;
	 `CHECK_EQUAL(OTriggerParams_x.StartMove, 0);
	 ##1;
	 `CHECK_EQUAL(OTriggerParams_x.StartMove, 0);
	 ##10;
      end

      `TEST_CASE("forced_startmove") begin
	 // if we force startmove in trig_i, it should immediatelly
	 // propagate to trig_o
	 ITriggerParams_x <= '0;
	 ITriggerParams_x.DoQueue <= 1;
	 for (int stepnumber=5; stepnumber < 10; stepnumber++) begin
	    ICmdParams_x <= DataBlock(stepnumber, '1);
	    ##1;
	 end
	 ITriggerParams_x.DoQueue <= 0;
	 ICmdParams_x <= DataBlock(32'hff);
	 ##5;
	 ITriggerParams_x.StartMove <= 1;
	 ##5;
	 `CHECK_EQUAL(OTriggerParams_x.StartMove, 1);

      end

      `TEST_CASE("no_start_move") begin
	 // we queue X records, but these have external trigger
	 // enabled. Hence it should not generate startmove signal in
	 // trig_o when it is played, because automatic movement does
	 // not get into the play.
	 ITriggerParams_x <= '0;
	 ITriggerParams_x.DoQueue <= 1;
	 for (int stepnumber=5; stepnumber < 10; stepnumber++) begin
	    ICmdParams_x <= DataBlock(stepnumber, '1);
	    ##1;
	 end
	 ITriggerParams_x.DoQueue <= 0;
	 ICmdParams_x <= DataBlock(32'hff);
	 ##10;
	 // so the point here is, that we have queued 5 records into
	 // the queue, and only first one should be for the moment
	 // going out to the output. We flush the queue. This is done
	 // by pulsing done (or just keeping high)
	 Done_i <= 1;
	 for(int i=5; i < 10; i++) begin
            `CHECK_EQUAL(OCmdParams_x.StepNumber_b32, i);
            `CHECK_EQUAL(OTriggerParams_x.StartMove, 0);
	    ##1;
	 end
	 `CHECK_EQUAL(OCmdParams_x.StepNumber_b32, 32'hff);
	 Done_i <= 0;

	 ##1;

      end

      `TEST_CASE("queue_flushing") begin
	 // this fills queue with some commands and reads them out of
	 // the queue. verifies if stepnumber increases when being
	 // pulled out
	 ITriggerParams_x <= '0;
	 ITriggerParams_x.DoQueue <= 1;
	 for (int stepnumber=5; stepnumber < 10; stepnumber++) begin
	    ICmdParams_x <= DataBlock(stepnumber);
	    ##1;

	 end
	 ITriggerParams_x.DoQueue <= 0;
	 ICmdParams_x <= DataBlock(32'hff);
	 ##10;
	 // so the point here is, that we have queued 5 records into
	 // the queue, and only first one should be for the moment
	 // going out to the output. We flush the queue. This is done
	 // by pulsing done (or just keeping high)
	 Done_i <= 1;

	 for(int i=5; i < 10; i++) begin
            `CHECK_EQUAL(OCmdParams_x.StepNumber_b32, i);
	    ##1;
	 end
	 `CHECK_EQUAL(OCmdParams_x.StepNumber_b32, 32'hff);
	 ##1;

      end

      `TEST_CASE("no_queue_direct_propagation") begin
	 // in case we do not queue the parameters into the command
	 // queue, the input parameters have to appear directly on the
	 // output
	 ICmdParams_x <= DataBlock(32'haabbccdd);
	 ##1;
	 // providing that nothing is written into the queue, out
	 // param should be equal
         `CHECK_EQUAL(ICmdParams_x, OCmdParams_x);
      end
   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10ms);

   commandqueuer #(/*AUTOINSTPARAM*/
		   // Parameters
		   .QUEUELENGTH		(QUEUELENGTH)) DUT
     (/*AUTOINST*/
      // Outputs
      .OTriggerParams_x				(OTriggerParams_x),
      .OCmdParams_x			(OCmdParams_x),
      .QueueStatus_x				(QueueStatus_x),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .Done_i				(Done_i),
      .ITriggerParams_x				(ITriggerParams_x),
      .ICmdParams_x			(ICmdParams_x));

endmodule

// Local Variables:
// verilog-library-directories:("." "../modules" )
// verilog-library-extensions:(".v" ".h" ".sv")
// verilog-typedef-regexp:"_t$"
// End:
