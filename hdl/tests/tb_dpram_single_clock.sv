//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) November 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_DPRAM_SINGLE_CLOCK.SV
// @brief Dual port single clock RAM instance test
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 14 November 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

// Dual port single clock RAM instance test
module tb_dpram_single_clock;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t ClkRs_ix;


   /*AUTOWIRE*/
   /*AUTOREGINPUT*/
   /*AUTOINOUTPARAM("dpram_single_clock")*/
   // data bus
   localparam g_DataWidth = 8;
   // address bus
   localparam g_AddrWidth = 8;
   // counter
   logic [31:0]       ReadData;

   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 i_memmastersima.waitfor(100);
	 i_memmastersima.reset();
	 i_memmastersimb.reset();
	 i_memmastersima.waitfor(100);

      end

      `TEST_CASE("debug_test") begin
	 for(int i = 0; i < 255; i++) begin
	    // writes into all positions of the memory from port A
	    i_memmastersima.writeConsecutive(i, i);
	    i_memmastersima.waitfor(1);

	    // and immediatelly reads by port B
	    i_memmastersimb.read(i, ReadData);
	    `CHECK_EQUAL(ReadData, i, "MEM READ FAIL FROM PORT B");
	 end

	 // and last write should be with write going down after operation
	 i_memmastersima.write(255, 255);
	 i_memmastersimb.read(255, ReadData);
	 `CHECK_EQUAL(ReadData, 255, "MEM READ FAIL FROM PORT B");
      end // UNMATCHED !!

      `TEST_CASE("read_part_unchanged_during_write") begin
	 for(int i = 0; i < 255; i++) begin
	    // writes into all positions of the memory from port A
	    i_memmastersima.writeConsecutive(i, 10);
	    i_memmastersima.waitfor(1);
	 end

	 fork
	    begin
	       // with this state of matters, this has to always read 10
	       forever
		 begin
		    i_memmastersimb.read(100, ReadData);
		    `CHECK_EQUAL( ReadData, 10);
		 end
	    end
	    begin
	       ##10;
	       i_memmastersima.write(20, 20);
	       ##10;
	    end
	 join_any;
      end // fork begin

   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(100ms);

   mem_x #(.g_AddrWidth(g_AddrWidth),
	   .g_DataWidth(g_DataWidth)) wmembus_ix(.ClkRs_ix(ClkRs_ix));
   mem_x #(.g_AddrWidth(g_AddrWidth),
	   .g_DataWidth(g_DataWidth)) rmembus_ox(.ClkRs_ix(ClkRs_ix));

   dpram_single_clock DUT
     (/*AUTOINST*/
      // Interfaces
      .wmembus_ix			(wmembus_ix),
      .rmembus_ox			(rmembus_ox));

   memmastersim
     #(
       // Parameters
       .g_clockspeed			(25))
   i_memmastersima (
		   // Interfaces
		   .memBus		(wmembus_ix),
		   // Outputs
		   .ClkRs_ox		(ClkRs_ix));

   memmastersim
     #(
       // Parameters
       .g_clockspeed			(25))
   i_memmastersimb (
		   // Interfaces
		   .memBus		(rmembus_ox),
		   // Outputs
		   .ClkRs_ox		());
endmodule
