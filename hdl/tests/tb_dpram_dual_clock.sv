//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file TB_DPRAM.SV
// @brief dual port ram
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 30 October 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

// dual port ram
module tb_dpram_dual_clock;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t clka_ix, clkb_ix;
   // data bus
   localparam g_DataWidth = 8;
   // address bus
   localparam g_AddrWidth = 8;
   // counter
   logic [31:0]       ReadData;


   /*AUTOWIRE*/
   /*AUTOREGINPUT*/

   // let's generate two interfaces of the same kind
   mem_x #(.g_AddrWidth(g_AddrWidth),
	   .g_DataWidth(g_DataWidth)) ckaref(.ClkRs_ix(clka_ix));
   mem_x #(.g_AddrWidth(g_AddrWidth),
	   .g_DataWidth(g_DataWidth)) ckbref(.ClkRs_ix(clkb_ix));

   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 // interfaces reset and in between waits for cycles
	 i_memmastersima.waitfor(100);
	 i_memmastersima.reset();
	 i_memmastersimb.reset();
	 i_memmastersima.waitfor(100);
      end

      `TEST_CASE("debug_test") begin
	 for(int i = 0; i < 255; i++) begin
	    // writes into all positions of the memory from port A
	    i_memmastersima.writeConsecutive(i, i);
	    i_memmastersima.waitfor(1);

	    // and immediatelly reads by port B
	    i_memmastersimb.read(i, ReadData);
	    `CHECK_EQUAL(ReadData, i, "MEM READ FAIL FROM PORT B");
	 end

	 // and last write should be with write going down after operation
	 i_memmastersima.write(255, 255);
	 i_memmastersimb.read(255, ReadData);
	 `CHECK_EQUAL(ReadData, 255, "MEM READ FAIL FROM PORT B");

      end

      `TEST_CASE("consecutive_writeread") begin
	 for(int i = 0; i < 255; i++) begin
	    // writes into all positions of the memory from port A
	    i_memmastersima.writeConsecutive(i, i);
	    i_memmastersima.waitfor(1);

	    // and immediatelly reads by port B
	    i_memmastersimb.read(i, ReadData);
	    `CHECK_EQUAL(ReadData, i, "MEM READ FAIL FROM PORT B");
	 end

	 // and last write should be with write going down after operation
	 i_memmastersima.write(255, 255);
	 i_memmastersimb.read(255, ReadData);
	 `CHECK_EQUAL(ReadData, 255, "MEM READ FAIL FROM PORT B");
      end

      `TEST_CASE("portB_write_portA_read") begin
	 for(int i = 0; i < 255; i++)
	   // writes into all positions of the memory
	   i_memmastersimb.writeConsecutive(i, i);
	 // and last write should be with write going down after operation
	 i_memmastersimb.write(255, 255);

	 // read back and compare (using the different clock)
	 for(int i = 0; i < 256; i++) begin
	    i_memmastersima.read(i, ReadData);
	    `CHECK_EQUAL(ReadData, i, "MEM READ FAIL FROM PORT A");
	 end
      end

      `TEST_CASE("portA_write_portB_read") begin
	 for(int i = 0; i < 255; i++)
	   // writes into all positions of the memory
	   i_memmastersima.writeConsecutive(i, i);
	 // and last write should be with write going down after operation
	 i_memmastersima.write(255, 255);

	 // read back and compare (using the different clock)
	 for(int i = 0; i < 256; i++) begin
	    i_memmastersimb.read(i, ReadData);
	    `CHECK_EQUAL(ReadData, i, "MEM READ FAIL FROM PORT B");
	 end
      end


      `TEST_CASE("simple_portA_write_read") begin
	 for(int i = 0; i < 255; i++)
	   // writes into all positions of the memory
	   i_memmastersima.writeConsecutive(i, i);
	 // and last write should be with write going down after operation
	 i_memmastersima.write(255, 255);

	 // read back and compare (using the same clock)
	 for(int i = 0; i < 256; i++) begin
	    i_memmastersima.read(i, ReadData);
	    `CHECK_EQUAL(ReadData, i, "MEM READ FAIL");
	 end
      end
   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(100ms);

   dpram_dual_clock #(/*AUTOINSTPARAM*/) DUT
     (/*AUTOINST*/
      // Interfaces
      .ckaref				(ckaref),
      .ckbref				(ckbref));

   memmastersim
     #(
       // Parameters
       .g_clockspeed			(25))
   i_memmastersima (
		   // Interfaces
		   .memBus		(ckaref),
		   // Outputs
		   .ClkRs_ox		(clka_ix));

   memmastersim
     #(
       // Parameters
       .g_clockspeed			(8.2))
   i_memmastersimb (
		    // Interfaces
		    .memBus		(ckbref),
		    // Outputs
		    .ClkRs_ox		(clkb_ix));


endmodule
