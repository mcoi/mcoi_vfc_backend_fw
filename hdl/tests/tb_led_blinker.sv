//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) April 2018 CERN

//-----------------------------------------------------------------------------
// @file TB_LED_BLINKER.SV
// @brief
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 12 April 2018
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

//
module tb_led_blinker;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};
   // End of automatics
   localparam		g_totalPeriod = 256;
   localparam		g_blinkOn = 3;
   localparam		g_blinkOff = 8;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic		led_o;			// From DUT of led_blinker.v, ...
   logic		period_o;		// From DUT of led_blinker.v, ...
   // End of automatics

   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic [7:0] amount_ib;		// To DUT of led_blinker.v

   int 							    howmuch;
   logic forceOne_i;

   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   localparam a = $bits(i_led_blinker.counthi_b);
   localparam b = $bits(i_led_blinker.countlo_b);
   localparam c = $bits(i_led_blinker.one_amount_b);

   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 ClkRs_ix.reset = 1;
	 amount_ib <= 0;
	 forceOne_i <= 0;

	 ##10;
	 ClkRs_ix.reset = 0;
	 ##10;
      end

      `TEST_CASE("debug_test") begin
	 // should not generate _any_ signal
	 amount_ib = 5;
	 ##100;
	 forceOne_i <= 0;
	 #1ms;
      end

      `TEST_CASE("vectors_scaling") begin
	 // should not generate _any_ signal
	 amount_ib = 5;
	 ##100;
	 forceOne_i <= 0;
	 #1ms;
	 // check the big-guy counters for overflow and proper vectors scaling
	 `CHECK_EQUAL(a, 31, "SIZE!");
	 `CHECK_EQUAL(b, 31, "SIZE!");
	 `CHECK_EQUAL(c, 31, "SIZE!");
	 `CHECK_EQUAL(i_led_blinker.counthi_b, 31'hbebc1f, "VALUE!");
	 `CHECK_EQUAL(i_led_blinker.countlo_b, 31'hbebc1e, "VALUE!");
	 `CHECK_EQUAL(i_led_blinker.one_amount_b, 31'h1, "VALUE!");
	 `CHECK_EQUAL(i_led_blinker.logic_on, 31'hbebc20, "VALUE!");
	 `CHECK_EQUAL(i_led_blinker.logic_off, 31'hbebc20, "VALUE!");
      end

      `TEST_CASE("count_pulses") begin
	 // should not generate _any_ signal
	 amount_ib = 23;
	 howmuch = 0;
	 @(posedge period_o);
	 #1;
	 fork
	    forever begin
	       @(posedge led_o);
	       howmuch += 1;
	    end
	    @(posedge period_o);
	 join_any;
	 `CHECK_EQUAL(howmuch, 23);
      end

      `TEST_CASE("low_length") begin
	 // should not generate _any_ signal
	 amount_ib <= 5;
	 // wait for start of the cycle
	 @(posedge period_o);
	 // check for length of negative between first two pulses, it
	 // has to match
	 @(negedge led_o);
	 repeat(g_blinkOff) begin
	    ##1;
	    `CHECK_EQUAL(led_o, 0);
	 end
	 ##1;
	 `CHECK_EQUAL(led_o, 1);
      end

      `TEST_CASE("single_blink") begin
	 // should not generate _any_ signal
	 amount_ib <= 1;
	 // check positive length, length should be 3
	 repeat(50) begin
	    @(posedge led_o);
	    ##1;
	    `CHECK_EQUAL(led_o, 1);
	    ##1;
	    `CHECK_EQUAL(led_o, 1);
	    ##1;
	    `CHECK_EQUAL(led_o, 1);
	    ##1;
	    `CHECK_EQUAL(led_o, 0);
	 end // repeat (50)
      end

      `TEST_CASE("no_blinks_when_zero") begin
	 // should not generate _any_ signal
	 amount_ib <= 0;
	 repeat(500) begin
	    ##1;
	    `CHECK_EQUAL(led_o, 0);
	 end
      end

      `TEST_CASE("por_state") begin
	 `CHECK_EQUAL(led_o, 0);
      end
   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10000ms);

   led_blinker
     #(/*AUTOINSTPARAM*/
       // Parameters
       .g_totalPeriod			(g_totalPeriod),
       .g_blinkOn			(g_blinkOn),
       .g_blinkOff			(g_blinkOff))
   DUT
     (.amount_ib			(amount_ib),
      /*AUTOINST*/
      // Outputs
      .led_o				(led_o),
      .period_o				(period_o),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .forceOne_i			(forceOne_i));

   logic ledlong_o, periodlong_o;

   // artificially high parameters used in MCOI to find if overflows
   parameter ILED_BLINKER_PERIOD = 625e6;
   parameter ILED_BLINKER_ON_TIME = 12.5e6;
   parameter ILED_BLINKER_OFF_TIME = 12.5e6;

   // this instance enforces further checking if there's somewhere overflow
   led_blinker
     #(
       // Parameters
       .g_totalPeriod			(ILED_BLINKER_PERIOD),
       .g_blinkOn			(ILED_BLINKER_ON_TIME),
       .g_blinkOff			(ILED_BLINKER_OFF_TIME))
   i_led_blinker
     (
      // Outputs
      .led_o				(ledlong_o),
      .period_o				(periodlong_o),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .forceOne_i			(forceOne_i),
      .amount_ib			(amount_ib));


endmodule
