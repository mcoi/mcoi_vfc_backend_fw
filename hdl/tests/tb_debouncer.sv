`include "vunit_defines.svh"

import CKRSPkg::*;


module tb_debouncer;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic		Signal_oq;		// From DUT of debouncer.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   ckrs_t		ClkRs_ix;		// To DUT of debouncer.v
   logic		Signal_ia;		// To DUT of debouncer.v
   // End of automatics

   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;

   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   `TEST_SUITE begin
      `TEST_SUITE_SETUP begin
	 Signal_ia = 0;

	 ClkRs_ix.reset = 1;
	 ##10;
	 ClkRs_ix.reset = 0;

      end

      `TEST_CASE("zero_output_after_reset") begin
	 ##100;
	 // in reset we set input to '1' and we expect the debouncer
	 // to switch its output after debouncing period to '1'
	 `CHECK_EQUAL(Signal_oq, 0);
	 ##10;
	 Signal_ia = 1;
	 ##30;
         `CHECK_EQUAL(Signal_oq, 1);
      end

      `TEST_CASE("one_output_when_changed") begin
	 ##10;
	 Signal_ia = 0;
	 ##20;
	 Signal_ia = 1;
	 ##30
         `CHECK_EQUAL(Signal_oq, 1);
	 end

      `TEST_CASE("ignore_bounce") begin
	 Signal_ia = '1;
	 ##100;

	 fork
	    forever begin
	       Signal_ia = '0;
	       ##1;
	       forever ##1 Signal_ia = '1;
	    end
	    #5us;
	    @(negedge Signal_oq);
	 join_any
	 `CHECK_EQUAL(Signal_oq, 1);

      end // UNMATCHED !!

      `TEST_CASE("single_negative_pulse") begin
	 Signal_ia = '1;
	 ##100;

	 fork
	    forever begin
	       Signal_ia = '0;
	       ##1;
	       forever ##1 Signal_ia = '1;
	    end
	    #5us;
	    @(negedge Signal_oq);
	 join_any
	 `CHECK_EQUAL(Signal_oq, 1);

      end // UNMATCHED !!

      `TEST_CASE("square_bouncing") begin
	 ##10;
	 repeat(100) begin
	    ##1 Signal_ia = 0;
	    `CHECK_EQUAL(Signal_oq, 0);
	    ##1 Signal_ia = 1;
	    `CHECK_EQUAL(Signal_oq, 0);
	 end
	 ##100;
	 `CHECK_EQUAL(Signal_oq, 1);

      end // UNMATCHED !!

      `TEST_CASE("semilong_pulse") begin
	 ##10;
	 Signal_ia = 1;
	 ##25;
	 `CHECK_EQUAL(Signal_oq, 1);
	 ##25;
	 Signal_ia = 0;
	 ##50;
	 `CHECK_EQUAL(Signal_oq, 0);
      end // UNMATCHED !!

      `TEST_CASE("short_pulse_to_ignore") begin
	 fork
	    begin
	       ##10;
	       Signal_ia = 1;
	       ##10;
	       Signal_ia = 0;
	       ##50;
	       `CHECK_EQUAL(Signal_oq, 0);
	    end
	    forever begin
	       `CHECK_EQUAL(Signal_oq, 0);
	       ##1;
	    end
	 join_any
      end // UNMATCHED !!

      `TEST_CASE("steady_change") begin
	 ##100;
	 Signal_ia = 1;
	 ##30;
	 `CHECK_EQUAL(Signal_oq, 1);
      end // UNMATCHED !!

      `TEST_CASE("steady_after_por") begin
	 Signal_ia = 1;
	 ##30;
	 `CHECK_EQUAL(Signal_oq, 1);
      end // UNMATCHED !!
   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10ms);


   debouncer DUT
     (/*AUTOINST*/
      // Outputs
      .Signal_oq			(Signal_oq),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .Signal_ia			(Signal_ia));

endmodule
