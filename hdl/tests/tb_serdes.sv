//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) March 2018 CERN

//-----------------------------------------------------------------------------
// @file TB_SERDES.SV
// @brief
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 01 March 2018
// @details
// Serdes in in MCOI project used to communicate over SC/EC line in
// some slow control link. This allows to set the data to gefe through
// SC/EC link _and_ receive from GEFE link data as well. Bidirectional
// is needed as we need to setup the GEFE into loopback mode, but as
// well into local pattern generator mode, as the link has to be setup
// appropriately
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

`include "vunit_defines.svh"

import CKRSPkg::*;

//
module tb_serdes;
   timeunit 1ns;
   timeprecision 100ps;

   localparam integer clk_period = 20; // clock period in ns
   ckrs_t ClkRs_ix = '{clk:'0, reset:'0};

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic [63:0]		RxBertByteErrors_oc64;	// From DUT of SerDes.v
   logic [63:0]		RxBertBytesReceived_oc64;// From DUT of SerDes.v
   logic [15:0]		RxBertLosCntr_oc16;	// From DUT of SerDes.v
   logic		RxBertLosFlag_o;	// From DUT of SerDes.v
   logic		RxBusy_o;		// From DUT of SerDes.v
   logic		RxCrcChkError_o;	// From DUT of SerDes.v
   logic [31:0]		RxDataFifo_ob32;	// From DUT of SerDes.v
   logic		RxEmptyFifo_o;		// From DUT of SerDes.v
   logic		RxFullFifo_o;		// From DUT of SerDes.v
   logic		RxLocked_o;		// From DUT of SerDes.v
   logic		SerialLinkUp_o;		// From DUT of SerDes.v
   logic		TxBusy_o;		// From DUT of SerDes.v
   logic		TxEmptyFifo_o;		// From DUT of SerDes.v
   logic		TxFullFifo_o;		// From DUT of SerDes.v
   logic		Tx_o;			// From DUT of SerDes.v
   // End of automatics
   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic		RxEnBert_i;		// To DUT of SerDes.v
   logic		RxRdEnFifo_i;		// To DUT of SerDes.v
   logic [31:0]		TxDataFifo_ib32;	// To DUT of SerDes.v
   logic		TxErrInjBert_i;		// To DUT of SerDes.v
   logic		TxWrEnFifo_i;		// To DUT of SerDes.v
   // End of automatics
   /*AUTOINOUTPARAM("SerDes")*/
   // Beginning of automatic parameters (from specific module)
   logic		g_IdleNotLocked_b8;
   logic		g_IdleLocked_b8;
   logic		g_Header_b4;
   logic		g_TxFifoAddWith;
   logic		g_RxEdges2Lock_b8;
   logic		g_RxNoEdges2LossOfLock_b8;
   logic		g_CorrectBytes2Lock_b8;
   logic		g_NoCorrectBytes2LossOfLock_b8;
   logic		g_RxFifoAddWith;
   // End of automatics

   logic [31:0] 	data, rndata, queue[$];

   always forever #(clk_period/2 * 1ns) ClkRs_ix.clk <= ~ClkRs_ix.clk;
   default clocking cb @(posedge ClkRs_ix.clk); endclocking

   task getData(output logic [31:0] dataout);
      if (!RxEmptyFifo_o) begin
	 RxRdEnFifo_i = 1;
	 // sample the data at the moment of asking for new ones
	 dataout = RxDataFifo_ob32;
	 ##1;
	 RxRdEnFifo_i = 0;
	 // WAIT FOR 3ccs due to FIFO output latency, but sample
	 // current data now (it takes 3ccs to output the other fifo
	 // data)
	 ##3;
      end else
	`CHECK_EQUAL(1, 0, "getData when FIFO is empty!");
   endtask



   `TEST_SUITE begin

      `TEST_SUITE_SETUP begin
	 ClkRs_ix.reset = 1;
	 ##10;
	 ClkRs_ix.reset = 0;

	 // setup transmitter to idle mode - no data
	 TxWrEnFifo_i = 0;
	 TxErrInjBert_i = 0;
	 TxDataFifo_ib32 = '0;
	 // setup reception part to idle, and automatic readout of
	 // FIFO whenever not empty, so this will work as automatic
	 // data chain
	 RxRdEnFifo_i = 0;
	 RxEnBert_i = 0;
      end

      `TEST_CASE("debug_test") begin
	 // check two consecutive samples, 4 cycles out
	 @(posedge SerialLinkUp_o);
	 ##10;
	 fork
	    // writer:
	    repeat(500) begin: writer
	       while (TxFullFifo_o) ##1;
	       assert(std::randomize(rndata));
	       queue.push_back(rndata);
	       TxWrEnFifo_i = 1;
	       TxDataFifo_ib32 = rndata;
	       ##1;
	       TxWrEnFifo_i = 0;
	    end

	    // reader
	    forever begin
	       while (RxEmptyFifo_o) ##1;
	       ##1;
	       getData(data);
	       `CHECK_EQUAL(data, queue.pop_front(),
			    "DATA DO NOT MATCH");
	    end
	 join_any;
      end

      `TEST_CASE("random_stream") begin
	 // check two consecutive samples, 4 cycles out
	 @(posedge SerialLinkUp_o);
	 ##10;
	 fork
	    // writer:
	    repeat(500) begin
	       while (TxFullFifo_o) ##1;
	       assert(std::randomize(rndata));
	       queue.push_back(rndata);
	       TxWrEnFifo_i = 1;
	       TxDataFifo_ib32 = rndata;
	       ##1;
	       TxWrEnFifo_i = 0;
	    end

	    // reader
	    forever begin
	       while (RxEmptyFifo_o) ##1;
	       ##1;
	       getData(data);
	       `CHECK_EQUAL(data, queue.pop_front(),
			    "DATA DO NOT MATCH");
	    end
	 join_any;
      end

      `TEST_CASE("three_samples") begin
	 // check two consecutive samples, 4 cycles out
	 @(posedge SerialLinkUp_o);
	 ##10;
	 TxWrEnFifo_i = 1;
	 TxDataFifo_ib32 = 32'haabbccdd;
	 ##1;
	 TxDataFifo_ib32 = 32'hdeadbeef;
	 ##1;
	 TxDataFifo_ib32 = 32'h55eeffaa;
	 ##1;
	 TxWrEnFifo_i = 0;

	 // readout ( we can do because tx is hell fast when feeding FIFO)
	 while(RxEmptyFifo_o) ##1;
	 ##1;
	 // and we move by '1' as this is equivalent of 'detection' of
	 // fifonotempty in real design. IT TAKES HOWEVER 4 CYCLES TO
	 // PROPAGATE DATA FROM GRAY FIFO INTO OUTPUT,
	 getData(data);
	 `CHECK_EQUAL(RxEmptyFifo_o, 1,
		      "Fifo should be again empty");
	 `CHECK_EQUAL(data, 32'haabbccdd,
		      "First sample fail");

	 while(RxEmptyFifo_o) ##1;
	 ##1;
	 getData(data);
	 `CHECK_EQUAL(data, 32'hdeadbeef,
		      "Second sample fail");
	 `CHECK_EQUAL(RxEmptyFifo_o, 1)

	 while(RxEmptyFifo_o) ##1;
	 ##1;
	 getData(data);
	 `CHECK_EQUAL(data, 32'h55eeffaa,
		      "Empty sample fail");
	 `CHECK_EQUAL(RxEmptyFifo_o, 1)
	 ##100;
      end

      `TEST_CASE("rx_fifo_not_empty") begin
	 // check two consecutive samples, 4 cycles out
	 @(posedge SerialLinkUp_o);
	 ##10;
	 TxWrEnFifo_i = 1;
	 TxDataFifo_ib32 = 32'haabbccdd;
	 ##1;
	 TxDataFifo_ib32 = 32'hdeadbeef;
	 ##1;
	 TxWrEnFifo_i = 0;
	 // wait until _second one_ comes. First one is pulled out
	 // automatically, hence appears always on the output. If next
	 // one is sent by TX, this one stays in FIFO until we fetch
	 // it!
	 // following puts us straight to negative edge in time
	 @(negedge RxBusy_o);
	 `CHECK_EQUAL(RxEmptyFifo_o, 0,
		      "FIFO should not be empty");
      end

      `TEST_CASE("tx_fifo_full") begin
	 // check FIFO behaviour
	 @(posedge SerialLinkUp_o);
	 ##10;
	 // consecutive writings, default fifo is 16 samples deep,
	 // this will check it
	 TxDataFifo_ib32 = 32'haabbccdd;
	 TxWrEnFifo_i = 1;
	 ##15;
	 `CHECK_EQUAL(TxFullFifo_o, 0, "FIFO should not be full");
	 ##1;
	 `CHECK_EQUAL(TxFullFifo_o, 1, "FIFO shouldbe full");
      end

      `TEST_CASE("single_cc_data_hold") begin
	 // single clock data propagation:
	 @(posedge SerialLinkUp_o);
	 ##10;
	 TxDataFifo_ib32 = 32'haabbccdd;
	 TxWrEnFifo_i = 1;
	 ##1;
	 TxDataFifo_ib32 = '0;
	 TxWrEnFifo_i = 0;
	 ##10000;
	 // whatever time late the data should get in:
	 `CHECK_EQUAL(RxDataFifo_ob32, 32'haabbccdd,
		      "DATA NOT PROPAGATED TO RX");
      end

      `TEST_CASE("link_up") begin
	 // within 1ms we should get lock for sure!
	 #2ms;
         `CHECK_EQUAL(RxLocked_o, 1, "Link is not locked");
         `CHECK_EQUAL(SerialLinkUp_o, 1, "Link is up");
      end
   end;

   // The watchdog macro is optional, but recommended. If present, it
   // must not be placed inside any initial or always-block.
   `WATCHDOG(10000ms);

   // LEAVE EVERYTHING ON DEFAULT
   SerDes DUT
     (.ClkUser_ik			(ClkRs_ix.clk),
      .ClkSerDes_ik			(ClkRs_ix.clk),
      .Reset_ira			(ClkRs_ix.reset),
      // loopback connection
      .Rx_i				(Tx_o),
      .*);

endmodule
