//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) January 2018 CERN

//-----------------------------------------------------------------------------
// @file MANYFFI.SV
// @brief interfaced version of data delay line
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 29 January 2018
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module manyffi
  #(
    parameter   g_Latency  = 4
    )
  (input ckrs_t ClkRs_ix,
    // input data interface. Use data enable to write into the FIFO
    data_x wdata_ix,
    // output data interface. Use its data enable to fetch next sample
    data_x rdata_ox
   );


   // all this bloated variable generations because of quartus
   genvar x;
   generate for (x=0; x < $bits(wdata_ix.data); x++) begin: iffs
     manyff
		 #(/*AUTOINSTPARAM*/
		   // Parameters
		   .g_Latency		(g_Latency))
   i_manyff (
	     // Outputs
	     .d_o			(rdata_ox.data[x]),
	     // Inputs
	     .ClkRs_ix			(ClkRs_ix),
	     .d_i			(wdata_ix.data[x]));
   end // block: iffs
   endgenerate

   manyff
     #(/*AUTOINSTPARAM*/
       // Parameters
       .g_Latency			(g_Latency))
   i_manyff (
	     // Outputs
	     .d_o			(rdata_ox.enable),
	     // Inputs
	     .ClkRs_ix			(ClkRs_ix),
	     .d_i			(wdata_ix.enable));




endmodule // manyffi
