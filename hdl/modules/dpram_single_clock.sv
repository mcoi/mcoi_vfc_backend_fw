//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) November 2017 CERN

//-----------------------------------------------------------------------------
// @file DPRAM_SINGLE_CLOCK.SV
// @brief Single clock dual port memory instance
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 14 November 2017
// @details This module uses single interface to operate the RAM. The interface
// has its own clock associated so all the clocking is realised using that
// clock. The memory width and data length is taken from the interface parameters
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module dpram_single_clock
  #(
    parameter g_AddrWidth = 16,
    parameter g_DataWidth = 32)
  (
   // and this one is read only
   mem_x rmembus_ox,
   // this memory interface is writable
   mem_x wmembus_ix
   );

   initial begin
      assert ($bits(wmembus_ix.datain) == $bits(rmembus_ox.datain)) else begin

	 $display("Instantiated memory:");
	 $display("\twmembus_ix bus address: %d, data: %d",
		  $bits(wmembus_ix.addrbus),
		  $bits(wmembus_ix.datain));
	 $display("\trmembus_ox bus address: %d, data: %d",
		  $bits(rmembus_ox.addrbus),
		  $bits(rmembus_ox.datain));
	 $display("Data bus sizes of both interfaces have to be the same");
      end // else: !assert ($bits(wmembus_ix.datain) ==
      // $bits(rmembus_ox.datain))

      assert ($bits(wmembus_ix.addrbus) == $bits(rmembus_ox.addrbus)) else
	$error("Address bus sizes of both interfaces have to be the same");
   end

   always_comb begin
      assert (wmembus_ix.ClkRs_ix.clk == rmembus_ox.ClkRs_ix.clk) else
	$error("Clocks have to be the same");
   end

   // Declare the RAM variable
   logic [g_DataWidth-1:0] ram[2**g_AddrWidth-1:0];

   always_ff @ (posedge wmembus_ix.ClkRs_ix.clk)
     begin
	// Write
	if (wmembus_ix.write) begin
	   ram[wmembus_ix.addrbus] <= wmembus_ix.datain;
	end
     end // always_ff @ (posedge wmembus_ix.ClkRs_ix.clk)

   // out signal is unregistered directly from matrix
   always_comb begin
      rmembus_ox.dataout <= ram[rmembus_ox.addrbus];
   end

endmodule // dpram_single_clock
