import MCPkg::*;


// interface describing all the signals for the motor control. Two
// switches, overheat and stepping controller failure as inputs and
// all the motor driving controls as the output
interface mc_x;
   // we wrap the signals into two groups as during optical stream assembly this
   // will be used to construct easily the packed data stream
   mcinput_t invec;
   mcoutput_t outvec;

   modport slave(input invec,
		 output outvec);

endinterface // mc_x
