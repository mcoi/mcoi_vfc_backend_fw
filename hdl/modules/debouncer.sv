//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) June 2017 CERN

//-----------------------------------------------------------------------------
// @file DEBOUNCER.SV
// @brief debounces signals from slow switches
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 28 June 2017
// @details
// Takes input asynchronous signal Signal_ia, and debounces
// it. I.e. removes all quick glitches, which typically appear e.g. on
// signals from push buttons. g_SynchDepth determines how many
// flipflops is generated to sync the analogue signal to the intrisic
// clock domain ClkRs_ix. g_CounterWidth gives bit width of a wait
// counter.
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module debouncer
  #(
    // Bouncing time < 2**g_CounterWidth*Period-of-Clk_ik
    parameter   g_CounterWidth  = 4,
    // Number of FF in the synchronization chain (min is 2)
    parameter   g_SynchDepth    = 3
    )
   (   // output debounced signal
       output logic Signal_oq,
       // resetclock struct
       input 	    ckrs_t ClkRs_ix,
       // input signal to debounce, asynchronous
       input logic  Signal_ia);

   // g_SynchDepth passed-through signal - metastability treatment
   logic 	    StableSignal;

   manyff
     #(
       // Parameters
       .g_Latency			(g_SynchDepth))
   i_manyff
     (
      // Outputs
      .d_o				(StableSignal),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .d_i				(Signal_ia));

   // now, any CHANGE of the input signal with respect to the output
   // signal will generate MKO trigger:
   bit 	    SignalChanged, risen, fallen;
   logic blanking;
   get_edge
   i_get_edge_samp
     (
      // Outputs
      .rising_o				(risen),
      .falling_o			(fallen),
      .data_o				(),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .data_i				(StableSignal ^ Signal_oq));

   assign SignalChanged = risen | fallen;

   mko
     #(
       // Parameters
       .g_CounterBits			(g_CounterWidth))
   i_mko
     (
      // Outputs
      .q_o				(blanking),
      .q_on				(),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .width_ib				((g_CounterWidth)'(2**g_CounterWidth-1)),
      .start_i				(SignalChanged),
      .enable_i				('1));

   // end of blanking signal is an indication, that we should change
   // the value to the one, which is currently present at the input
   logic 	    Sample;

   get_edge
   i_get_edge
     (
      .rising_o				(),
      .falling_o			(Sample),
      .data_o				(),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .data_i				(blanking));



   // output mux: either we pull 'old value' or new value depending of
   // blanking signal. NOTE: THIS IS MEANT FOR SEMI-STATIC SIGNALS. In
   // case of fast transitions which will always trigger another
   // blanking the value might NEVER CHANGE. But this is not a case of
   // buttons and very slow events
   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset) begin
      if (ClkRs_ix.reset) begin
	 Signal_oq <= '0;
      end else begin
	 if (Sample)
	   Signal_oq <= StableSignal;
      end
   end



endmodule // debouncer
