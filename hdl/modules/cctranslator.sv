//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file CCTRANSLATOR.SV
// @brief translates the bus signal from one domain to another one
// @author Dr. David Belohrad  <david.belohrad@cern.ch>, CERN
// @date 31 October 2017
// @details
// Using async fifo this module implements bus translation from one clock domain
// to another one. This is done by continuously feeding input through the input
// clock into FIFO and reading the values out using output clock. Beware
// however, that if read clock is slower than writing clock, fifo will enter
// full and the samples will just not be written into the FIFO. This behaviour
// is normal and cannot be avoided for fully streamed outputs with this clock
// configuration. The module uses data interfaces, input data are bound do
// 'valid', which is treated as write enable into the FIFO, output data generate
// valid data signal whenever they change. This introduces 1cc latency to the
// output, but no big deal (for this particular case)
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;

module cctranslator
  #(
    // bus width
    parameter g_DataWidth  = 32,
    // length of the FIFO to sync the data - in number of bits
    parameter g_FifoWidth = 4
    )
   (
    // data to be transported on write clock from domain
    data_x wdata_ix,
    // data to be transported on read clock into domain
    data_x rdata_ox,
    // if data feeding is quick enough, the fifo comes full for writing and then
    // data loss occurs, we need to announce it upstream. When this happens
    // ONCE, we need to keep this flag running until it is cleared as the
    // overrun usually means loss of data, hence improper data handling (for
    // example optical link is not setup, read clock missing etc)
    output logic wfifoOverrun_o,
    // 1cc on this will clear fifooverrun flag
    input logic woverrunClear_i
    );

   logic rFifoEmpty_o, wFifoFull_o;
   data_x #(.g_DataWidth(g_DataWidth)) rcopy_x(.ClkRs_ix(rdata_ox.ClkRs_ix));

   // whenever there's something in FIFO, just read it out
   always_comb rcopy_x.enable = !rFifoEmpty_o;

   // and at the same time, copy the exact replica of the data, _including_
   // enable (which should be in this case always '1' whenever a read takes
   // place into the output stream
   always_ff @(posedge rdata_ox.ClkRs_ix.clk or posedge rdata_ox.ClkRs_ix.reset) begin
      if (rdata_ox.ClkRs_ix.reset) begin
	 rdata_ox.data <= '0;
	 rdata_ox.enable <= '0;
      end
      else begin
	 // if fifo is not full, we can pull out the next data, current fifo
	 // data are OK
	 if (!rFifoEmpty_o) begin
	    rdata_ox.data <= rcopy_x.data;
	    rdata_ox.enable <= 1;
	 end
	 else begin
	    rdata_ox.enable <= 0;
	 end
      end // else: !if(rdata_ox.ClkRs_ix.reset)
   end // always_ff @ (posedge rdata_ox.ClkRs_ix.clk or posedge rdata_ox.ClkRs_ix.reset)

   // using enable to fetch data, we clamp them to the output stream and resync
   // enable of the output data stream such, that it will

   assert property (@(posedge wdata_ix.ClkRs_ix.clk)
		    disable iff (wdata_ix.ClkRs_ix.reset)
		    wFifoFull_o |=> wfifoOverrun_o) else
     $error("wfifoOverrun_o not risen but fifo is full");
   assert property (@(posedge wdata_ix.ClkRs_ix.clk)
		    disable iff (wdata_ix.ClkRs_ix.reset)
		    woverrunClear_i |=> !wfifoOverrun_o) else
     $error("wfifoOverrun_o clearout does not work");

   always_ff @(posedge wdata_ix.ClkRs_ix.clk or posedge wdata_ix.ClkRs_ix.reset) begin
      if (wdata_ix.ClkRs_ix.reset)
	wfifoOverrun_o = 0;
      else if (woverrunClear_i)
	wfifoOverrun_o = 0;
      else if (wFifoFull_o)
	wfifoOverrun_o = 1;
   end

   // usage of asynchronous fifo to transport the data, we use directly the
   // input stream to get the enable as 'write down' into the FIFO, on the other
   // hand, the output stream we have to 'compose' as enable has to be generated
   afifo
     #(/*AUTOINSTPARAM*/
       // Parameters
       .g_FifoWidth			(g_FifoWidth))
   i_afifo (
	    .rdata_ox			(rcopy_x),
	    .*);




endmodule // cctranslator
