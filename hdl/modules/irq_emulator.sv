//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) January 2018 CERN

//-----------------------------------------------------------------------------
// @file IRQ_EMULATOR.SV
// @brief Implements simple shift register emulating rise of the IRQ
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 10 January 2018
// @details Consecutively generates each roughly 1 second (with 160MHz
// clock) one IRQ request which lasts for a single clock tick and
// informs upstream that IRQ of that specific source happened. Now,
// MCOI uses IRQs DIFFERENTLY. In fact, the vector codes in what link
// and what motors caused the given IRQ, but that is not emulated in
// this version of the module.
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module irq_emulator
  #(parameter BITSWIDTH = 25)
  (input ckrs_t ClkRs_ix,
   output logic [23:0] IRQEmulator_ob24,
   output logic [23:0] CurrentIRQ_ob24
   );

   // IRQ emulator - when turned on, it will periodically issue
   // interrupt on given IRQ level and we cycle here the 24bit IRQ
   // vector such, that each irq vector bit gets a chance to be cycled
   logic [23:0] IRQEmulator_b24, IRQEmulatorOld_b24;
   logic [BITSWIDTH-1:0] IRQCounter;

   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset) begin
      if (ClkRs_ix.reset) begin
	 IRQCounter <= '0;
	 IRQEmulator_b24 <= 1;
	 IRQEmulatorOld_b24 <= {1'b1, 23'b0};
	 IRQEmulator_ob24 <= '0;
      end else begin
	 IRQCounter <= IRQCounter + 1'b1;
	 IRQEmulatorOld_b24 <= IRQEmulator_b24;

	 if (&IRQCounter)
	   IRQEmulator_b24 <= {IRQEmulator_b24[22:0],
				IRQEmulator_b24[23]};

	 if (IRQEmulator_b24 != IRQEmulatorOld_b24)
	   IRQEmulator_ob24 <= IRQEmulator_b24;
	 else
	   IRQEmulator_ob24 <= 0;
      end
   end // always_ff @ (posedge ClkRs_ix.clk or posedge ClkRs_ix.reset)

   assign CurrentIRQ_ob24 = IRQEmulator_b24;

   assert property (@(posedge ClkRs_ix.clk)
   		    disable iff (ClkRs_ix.reset)
   		    IRQEmulator_ob24 |=> (IRQEmulator_ob24 == 0)) else
     $error("Single clock cycle rise allowed");


endmodule // irq_emulator
