//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) November 2017 CERN

//-----------------------------------------------------------------------------
// @file FOURMCLINKS.SV
// @brief instantiates 4 motor control links and connects appropriately the
// wishbone interfaces
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 07 November 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;
import MCPkg::*;

module fourmclinks
  #(
    // length of the command queue
    parameter   QUEUELENGTH  = 32,
    // optical link frame size is defined in number of bytes
    parameter g_FrameSize = 80,
    // data bus width - NEVER! change this
    parameter g_DataWidth = 32,
    // determine how many bits the wishbone address bus is using
    parameter g_AddressWidth = 25,
    // constant declaring starting address of the first motor control link. 4
    // links are instantiated as of this address.
    parameter g_fourlinksAddress
    )
   (input ckrs_t ClkRs_ix,
    // wishbone interface
    t_WbInterface Wb_iot,
    // input and output GBT data
    data_x packet_ox[NUMBER_OF_GBT_LINKS-1:0],
    data_x packet_ix[NUMBER_OF_GBT_LINKS-1:0],
    // triggers
    input 								      triggers_t triggers,
    // busy flag, '1' when operation on any link in progress.
    output logic 							      Busy_o,
    // IRQ output passing through IRQ arbiter, which decides
    // about the priorities. Output is is packed array where
    // (currently) 16 motors are occupying 16 least significant bits,
    // and above that there are bits indicating which link fired the
    // IRQ.
    output logic [$clog2(NUMBER_OF_GBT_LINKS)+NUMBER_OF_MOTORS_PER_FIBER-1:0] IRQ_ob,
    // FIFO overflow and reset signals
    output logic [NUMBER_OF_GBT_LINKS-1:0] 				      Overflow_ob,
    input logic 							      ResetOverflow_i
    );


   // triggers have to be only single cycle
   assert property (@(posedge ClkRs_ix.clk) triggers.TrigMoveInt_i |=> !triggers.TrigMoveInt_i)
     else $error("Int trigger has to be only a single pulse");

   // wishbone interface has across the design _always the same address bus width_
   t_WbInterface #(.g_DataWidth(g_DataWidth),
		   .g_AddressWidth(g_AddressWidth))
		   Slaves_iot[0:NUMBER_OF_GBT_LINKS-1] (ClkRs_ix.clk,
							ClkRs_ix.reset);

   // addresspace is set to zero because we do not want to have any internal
   // displacement of the wishbone addresses. This is handled by crossbar
   localparam g_AddressSpaceOffset = 0;

   // DECLARE IRQ signal. This is array of IRQs from each link, later
   // on we select using IRQ arbiter which IRQ gets the IRQ handling
   logic [NUMBER_OF_MOTORS_PER_FIBER-1:0] IRQ_b [NUMBER_OF_GBT_LINKS-1:0];
   logic [$clog2(NUMBER_OF_GBT_LINKS)-1:0] irqlink_b;
   logic [NUMBER_OF_MOTORS_PER_FIBER-1:0]  irqdata_b;
   logic [NUMBER_OF_GBT_LINKS-1:0] 	   Busy_b;


   assign IRQ_ob = {irqlink_b, irqdata_b};


   // IRQ arbiter handles parallel requests to reset and casts them to
   // the IRQ output, identifying LINK which caused them! Note that we
   // do not care here about fifo full, but we care about fifo
   // overflow, which goes upstream and has to be connected to
   // appropriate IRQ status register
   irq_arbiter
     #(
       // Parameters
       .NUM_OF_INPUTS			(NUMBER_OF_GBT_LINKS),
       .FIFO_DEPTH			(IRQ_GBT_FIFO_BITWIDTH),
       .VECTOR_SIZE			(NUMBER_OF_MOTORS_PER_FIBER))
   i_irq_arbiter (
		  // Outputs
		  .data_ob		(irqdata_b),
		  .link_ob		(irqlink_b),
		  .Overflow_ob		(Overflow_ob),
		  .FifoFull_ob		(),
		  // Inputs
		  .ClkRs_ix		(ClkRs_ix),
		  .ResetOverflow_i	(ResetOverflow_i),
		  .data_ib		(IRQ_b));


   assert property (@(posedge ClkRs_ix.clk)
		    $rose(ResetOverflow_i) |=> ~ResetOverflow_i) else
     $display("Reset IRQ overflow is allowed for single clock cycle");

   genvar link;
   generate
      for (link = 0; link < NUMBER_OF_GBT_LINKS; link++) begin : linkextraction
      mclink
	#(
	  // identifies link number (number is exported into VME space for verification)
	  .g_mclinkInterfaceNumber	(link+10),
	  /*AUTOINSTPARAM*/
	  // Parameters
	  .QUEUELENGTH			(QUEUELENGTH),
	  .g_FrameSize			(g_FrameSize),
	  .g_DataWidth			(g_DataWidth),
	  .g_AddressWidth		(g_AddressWidth),
	  .g_AddressSpaceOffset		(g_AddressSpaceOffset))
      i_mclink (
		.packet_ox		(packet_ox[link]),
		.packet_ix		(packet_ix[link]),
		.IRQ_ob			(IRQ_b[link]),
		.Busy_o			(Busy_b[link]),
		.Wb_iot			(Slaves_iot[link]),
		/*AUTOINST*/
		// Inputs
		.ClkRs_ix		(ClkRs_ix),
		.triggers		(triggers));
      end
   endgenerate

   // generate wishbone crossbar for all 4 mclinks,
   localparam logic [g_AddressWidth-1:0]  c_SlaveAddresses_mb
					   [0:NUMBER_OF_GBT_LINKS-1] =
					  '{
					    // 16 motors
					    (g_AddressWidth)'(0 * (2**MCLINK_WB_ADDRESS_WIDTH) + g_fourlinksAddress),
					    (g_AddressWidth)'(1 * (2**MCLINK_WB_ADDRESS_WIDTH) + g_fourlinksAddress),
					    (g_AddressWidth)'(2 * (2**MCLINK_WB_ADDRESS_WIDTH) + g_fourlinksAddress),
					    (g_AddressWidth)'(3 * (2**MCLINK_WB_ADDRESS_WIDTH) + g_fourlinksAddress)
					    };

   // all 4 slaves have exactly the same mask, given by length of each mclink
   // register space
   localparam c_LinkMask = (2**MCLINK_WB_ADDRESS_WIDTH)- 1;

   localparam logic [g_AddressWidth-1:0] c_SlaveMasks_mb
					 [0:NUMBER_OF_GBT_LINKS-1] =
   '{NUMBER_OF_GBT_LINKS{(g_DataWidth)'(c_LinkMask)}};

   // generate 'total' busy flag
   assign Busy_o = |Busy_b;


   WbCrossbar
     #(
       .g_Slaves(NUMBER_OF_GBT_LINKS),
       .g_AddressWidth(g_AddressWidth),
       .g_SlaveMasks_mb(c_SlaveMasks_mb),
       .g_SlaveAddresses_mb(c_SlaveAddresses_mb)
       )
   i_WbCrossbar (
		 .Master_iot		(Wb_iot),
		 .Slaves_iot		(Slaves_iot/*.[0:g_Slaves-1]*/));




endmodule // fourmclinks
