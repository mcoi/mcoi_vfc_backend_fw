//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) December 2017 CERN

//-----------------------------------------------------------------------------
// @file FIBONACCI_LFSR_NBIT.SV
// @brief Implements simple fibonacci random number generator
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 08 December 2017
// @details stolen from here:
// https://stackoverflow.com/questions/14497877/how-to-implement-a-pseudo-
// hardware-random-number-generator
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module fibonacci_lfsr_nbit
  #(
    // define number of bits to produce
    parameter BITS = 16
    )
   (input ckrs_t ClkRs_ix,

    output logic [BITS-1:0] data_ob
   );

   logic feedback;
   // any arbitrary feedback will do what I want.
   assign feedback = data_ob[BITS-1] ^ data_ob[1] ^ data_ob[8] ^ data_ob[14];
   initial begin
      assert(BITS >= 16) else $error("RND cannot work for less than 16 bits");
   end

   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset) begin
      if(ClkRs_ix.reset)
         data_ob <= (BITS)'(1);
      else
        data_ob <= {data_ob[BITS-2:0], feedback};
   end

   // simple check: no two consecutive numbers should be the same (this comes from the algorithm chosen)
   property nosamenumbers;
      logic [BITS-1:0] verifign;
      @(posedge ClkRs_ix.clk)
	disable iff (ClkRs_ix.reset)
	  (1'b1,verifign=data_ob) |=> (verifign != data_ob);
   endproperty // nosamenumbers
   assert property (nosamenumbers) else
     $error("TWO CONSECUTIVE RANDOM NUMBERS ARE THE SAME");
endmodule // fibonacci_lfsr_nbit
