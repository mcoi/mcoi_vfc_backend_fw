//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) June 2017 CERN

//-----------------------------------------------------------------------------
// @file SWITCHCONTROLLER.SV
// @brief Controls signals from two switches
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 28 June 2017
// @details
// Each motor can have 2 switches attached. This entity does
// debouncing of both switches and
// passes the debounced outputs throught the switch manager, which can
// for each group of switches do the signal inversion and
// selection. Selection is not limited to 'use one or the other raw
// switch', but allows as well usage of both or none. This requires
// for each implemented switch 3 bits of control: one inversion and
// one twobit array controlling which switches to take into account to
// produce logical switch. ClkRs_ix is clock/reset pair,
// RawSwitches_b2 is the _analogue_
// input signal from all connected switches. This entity supports
// only 2 switches (extremity). CntParam_ix contains switches
// configuration, this is upstream casted into VME register
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;
import MCPkg::*;
import constants::*;


module switchcontroller
  (   input cntparam_t CntParam_ix,
      input 		 ckrs_t ClkRs_ix,
      input logic [1:0]  RawSwitches_b2,
      output logic [1:0] ConfiguredSwitches_b2);


   logic [1:0] 	       Debounced_b2;
   // for each input switch we generate appropriate debouncing circuit and
   // switch manager. Combination of these allows us to change
   // polarity of any switch in the array of switches, and cast any of
   // a single array to a single output
   genvar 					       i;
   generate
      for (i = 0; i < 2; i++)
	begin: SYNCERS
	   debouncer #(.g_CounterWidth	(g_DebouncingCounterWidth),
		       .g_SynchDepth	(g_SynchronizationFlips)) i_debouncer (// Outputs
									       .Signal_oq(Debounced_b2[i]),
									       // Inputs
									       .Signal_ia(RawSwitches_b2[i]),
									       /*AUTOINST*/
									       // Inputs
									       .ClkRs_ix	(ClkRs_ix));
	   switchmanager
	   i_switchmanager (
			    // Outputs
			    .SWSelect		(ConfiguredSwitches_b2[i]),
			    // Inputs
			    .Switch_b2		(Debounced_b2),
			    .Polarity		(CntParam_ix.ExtremitySwitches_b2[i].Polarity),
			    .SelectedInputSwitches_b2(CntParam_ix.ExtremitySwitches_b2[i].SelectedInputSwitches_b2)
			    /*AUTOINST*/);

	end
   endgenerate

endmodule // switchcontroller
