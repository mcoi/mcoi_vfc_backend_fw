//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) January 2018 CERN

//-----------------------------------------------------------------------------
// @file IRQ_ARBITER.SV
// @brief IRQ arbitration
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 19 January 2018
// @details
// Having X interrupt vectors running in parallel, this entity will
// push each clock cycle single irq vector depending on round-robin
// priority encoding. All this is required by the fact, that we are
// not able to process in parallel IRQs coming from different GBT
// links. Each GBT link handles 16 motors, those CAN generate IRQ in
// parallel as there are bits reserved for this purpose. GBT links
// however cannot be made into one gigantic IRQ status word as 64bits
// totally would be needed to describe motors which caused
// IRQ. Currently only 24bits are available
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module irq_arbiter
  #(
    // number of input vectors
    parameter   NUM_OF_INPUTS  = 4,
    // depth of fifo for each input, this is in bus width units
    parameter FIFO_DEPTH = 5,
    // bitdepth of each irq vector
    parameter VECTOR_SIZE = 19
    )
   (input ckrs_t ClkRs_ix,
    // '1' to reset the FIFO overflow flags
    input logic 			     ResetOverflow_i,
    // array of input IRQ vectors, array length depends of number of
    // inputs
    input logic [VECTOR_SIZE-1:0] 	     data_ib
					     [NUM_OF_INPUTS-1:0],
    // single vector output chosen by IRQ arbiter
    output logic [VECTOR_SIZE-1:0] 	     data_ob,
    // associated link number corresponding to currently issued vector
    // in data_ob
    output logic [$clog2(NUM_OF_INPUTS)-1:0] link_ob,
    // '1' in particular bit indicates that FIFO overflowed. This
    // happens when write into already full fifo is issued. Reset
    // this flag using ResetOverflow_i
    output logic [NUM_OF_INPUTS-1:0] 	     Overflow_ob,
    // this vector indicates that input FIFO is full. When risen, it
    // means, that NO MORE DATA have to be written. If they are indeed
    // written, that means that FIFO drops them, and the overflow_ob
    // flag is risen
    output logic [NUM_OF_INPUTS-1:0] FifoFull_ob
    );

   initial begin
      // this comes from limitation in VFCsystem part:
      assert(VECTOR_SIZE < 25) else begin
	 $error("IRQ VECTOR LENGTH OF MORE THAN 24BITS IS NOT SUPPORTED");
      end
   end

   logic [NUM_OF_INPUTS-1:0] FifoFull_b,
			     FifoEmpty_b;


   assign FifoFull_ob = FifoFull_b;

   // interfaces:
   data_x #(.g_DataWidth(VECTOR_SIZE)) wdata_ix[NUM_OF_INPUTS-1:0](.ClkRs_ix(ClkRs_ix)),
     rdata_ox[NUM_OF_INPUTS-1:0](.ClkRs_ix(ClkRs_ix)),
     FifoOut_x[NUM_OF_INPUTS-1:0](.ClkRs_ix(ClkRs_ix));

   // we cannot use automatic genvar in forloop as quartus 17.1 does
   // not yet support that X years old feature of SV
   genvar 		     irqsrc;

   generate
      begin: inpfifo

	 for(irqsrc = 0; irqsrc < NUM_OF_INPUTS; irqsrc++) begin: irqfifos
	    // each time there's non-zero sample, we feed it into
	    // FIFO:
	    assign wdata_ix[irqsrc].data = data_ib[irqsrc];
	    always_comb begin
	       if (|data_ib[irqsrc])
		 wdata_ix[irqsrc].enable = 1;
	       else
		 wdata_ix[irqsrc].enable = 0;
	    end

	    fifo
	      #(
		// Parameters
		.g_FifoWidth	(FIFO_DEPTH))
	    i_fifo (
		    // Interfaces
		    .wdata_ix		(wdata_ix[irqsrc]),
		    .rdata_ox		(rdata_ox[irqsrc]),
		    // Outputs
		    .FifoFull_o		(FifoFull_b[irqsrc]),
		    .FifoEmpty_o	(FifoEmpty_b[irqsrc]),
		    // Inputs
		    .FifoReset_i	(ClkRs_ix.reset));

	    manyffi
	      #(
		// Parameters
		.g_Latency		(1))
	    i_manyffi (
		       // Interfaces
		       .wdata_ix	(rdata_ox[irqsrc]),
		       .rdata_ox	(FifoOut_x[irqsrc]),
		       // Inputs
		       .ClkRs_ix	(ClkRs_ix));

	    assert property (@(posedge ClkRs_ix.clk)
			     disable iff (ClkRs_ix.reset)
			     FifoFull_b[irqsrc] && wdata_ix[irqsrc].enable |=> Overflow_ob[irqsrc]) else
	      $error("Fifo overflows but flag not risen");

	    // now generate overflow flag
	    always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset)
	      begin: ovf
		 if (ClkRs_ix.reset)
		   Overflow_ob[irqsrc] <= 0;
		 else begin
		    if (ResetOverflow_i)
		      Overflow_ob[irqsrc] <= 0;
		    else if (FifoFull_b[irqsrc] && wdata_ix[irqsrc].enable)
		      Overflow_ob[irqsrc] <= 1;
		 end
	      end
	 end // for (genvar irqsrc = 0; irqsrc < NUM_OF_INPUTS;
	 // irqsrc++)
      end // block: inpfifo
   endgenerate


   // PRIORITY ARBITRATION: so how does that work? We have a
   // counter, which runs through all the input vectors and chooses
   // the FIRST NON-EMPTY FIFO OUTPUT AS NEXT CLOCK CYCLE
   // OUTPUT. The counter is each clock cycle increased by one
   // hence next clock cycle any non-empty has a chance to get in,
   // in the order of current counter. Such system allows to
   // minimise the latency and provides fair possibility to get IRQ
   // handled
   logic [$clog2(NUM_OF_INPUTS)-1:0] currentIRQ;
   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset)
     if (ClkRs_ix.reset)
       currentIRQ <= '0;
     else
       currentIRQ <= currentIRQ + ($size(currentIRQ))'(1);

   logic [NUM_OF_INPUTS-1:0] 	     NextIRQ;

   irq_rom
   i_irq_rom
     (// Outputs
      .data				(NextIRQ),
      // Inputs
      .address				({currentIRQ, ~FifoEmpty_b}));

   // for sure, once non-zero any of the links, they have to complete
   // the IRQ within next 100 cycles otherwise it is lost
   property a_passthrough(index);
      logic [VECTOR_SIZE-1:0] 	     store;

      @(posedge ClkRs_ix.clk)
	disable iff (ClkRs_ix.reset)
	  (|(data_ib[index]),
	   store=data_ib[index]) |->
		 ##[0:64]
		 data_ob === store;
   endproperty // a_passthrough

   property a_eventuality(index);
      logic [VECTOR_SIZE-1:0] 	     store;
      @(posedge ClkRs_ix.clk)
	disable iff (ClkRs_ix.reset)
	  (data_ib[index] != 0,
	   store = data_ib[index]) |-> ##[0:64] data_ob == store;
   endproperty // a_eventuality

   genvar 			     iprop;
   generate
      for(iprop=0; iprop < NUM_OF_INPUTS; iprop++) begin: xasse
   	 a_foo: assert property (a_passthrough(iprop)) else
   	   $display("Did not catch output on link %d", iprop);

 	 a_moo: assert property (a_eventuality(iprop)) else
 	   $error("The other guy did not find the proper output\
 either");
      end

   endgenerate

   // this one's fancy: zero can be present at the output only when
   // all fifos are empty (logic, right?)
   assert property (@(posedge ClkRs_ix.clk)
		    disable iff (ClkRs_ix.reset)
		    (data_ob == 0) |-> (FifoEmpty_b == '1)) else
     $error("Output might be zero only when all FIFOs are cleared");


   // @TODO: MAKE THIS DYNAMIC!!! But no idea how for the moment as
   // rdata_ox is an interface and compiler does not allow dynamic
   // casts to interface arrays. This sucks a bit.
   initial begin
      assert(NUM_OF_INPUTS == 4) else
	$error("YOU NEED TO REIMPLEMENT THIS, AS IT IS NOT GENERIC!");
   end

   always_comb begin
      rdata_ox[0].enable = (NextIRQ == ($clog2(NUM_OF_INPUTS))'(0) && !FifoEmpty_b[0]);
      rdata_ox[1].enable = (NextIRQ == ($clog2(NUM_OF_INPUTS))'(1) && !FifoEmpty_b[1]);
      rdata_ox[2].enable = (NextIRQ == ($clog2(NUM_OF_INPUTS))'(2) && !FifoEmpty_b[2]);
      rdata_ox[3].enable = (NextIRQ == ($clog2(NUM_OF_INPUTS))'(3) && !FifoEmpty_b[3]);

      if (rdata_ox[0].enable) begin
	 data_ob = rdata_ox[0].data;
	 link_ob = 0;
      end else if (rdata_ox[1].enable) begin
	 data_ob = rdata_ox[1].data;
	 link_ob = 1;
      end else if (rdata_ox[2].enable) begin
	 data_ob = rdata_ox[2].data;
	 link_ob = 2;
      end else if (rdata_ox[3].enable) begin
	 data_ob = rdata_ox[3].data;
	 link_ob = 3;
      end else begin
	 data_ob = 0;
	 link_ob = 0;
      end
   end


endmodule // irq_arbiter
