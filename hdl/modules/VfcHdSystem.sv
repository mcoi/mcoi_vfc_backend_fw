//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: VfcHdSystem.sv
//
// Language: SystemVerilog 2013
//
// Targeted device:
//
//     - Vendor:  Intel FPGA (Altera)
//     - Model:   Arria V GX
//
// Description:
//
//     System specific gateware targeted to the VFC-HD v3
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps
import CKRSPkg::*;


module VfcHdSystem
  //====================================  Global Parameters  ===================================//
   #(  parameter g_Synthesis             = 1'b1,
       parameter g_PowerUpAppResetLevel  = 1'b0,
       g_IsGoldenImage         = 1'b0,
       parameter g_SystemVersion_b8      = 8'h04,
       g_SystemReleaseDay_b8   = 8'h26,
       g_SystemReleaseMonth_b8 = 8'h04,
       g_SystemReleaseYear_b8  = 8'h17)
   //========================================  I/O ports  =======================================//
   (
    //==== External Ports ====//

    // VME Interface:
    input 	   VmeAs_in,
    input [ 5:0]   VmeAm_ib6,
    inout [ 31:1]  VmeA_iob31,
    inout 	   VmeLWord_ion,
    output 	   VmeAOe_oen,
    output 	   VmeADir_o,
    input [ 1:0]   VmeDs_inb2,
    input 	   VmeWrite_in,
    inout [ 31:0]  VmeD_iob32,
    output 	   VmeDOe_oen,
    output 	   VmeDDir_o,
    output 	   VmeDtAckOe_o,
    output [ 7:1]  VmeIrq_ob7,
    input 	   VmeIack_in,
    input 	   VmeIackIn_in,
    output 	   VmeIackOut_on,
    input 	   VmeSysReset_irn,
    // I2C Mux & IO Expanders:
    inout 	   I2cMuxSda_ioz,
    inout 	   I2cMuxScl_iokz,
    input 	   I2CIoExpIntApp12_ian,
    input 	   I2CIoExpIntApp34_ian,
    input 	   I2CIoExpIntBstEth_ian,
    input 	   I2CIoExpIntBlmIn_ian,
    input 	   I2CMuxIntLos_ian,
    // ADC Voltage Monitoring:
    input 	   VAdcDout_i,
    output 	   VAdcDin_o,
    output 	   VAdcCs_o,
    output 	   VAdcSclk_ok,
    // FMC Vadj control:
    output 	   VadjCs_o,
    output 	   VadjSclk_ok,
    output 	   VadjDin_o,
    // PCB Revision Resistor Network:
    input [ 7:0]   PcbRev_ib7,
    // Specials:
    inout 	   TempIdDq_ioz,
    output 	   ResetFpgaConfigN_orn,

    //==== System-Application interface ====//
    // Reference Clock for the interface
    input 	   ckrs_t ClkRs_ix,
    input 	   ckrs_t ClkRs20MHz_ix,
    // Application & Release IDs Registers:
    input [ 7:0]   AppVersion_ib8, AppReleaseDay_ib8, AppReleaseMonth_ib8, AppReleaseYear_ib8,
    input [351:0]  AppInfo_ib352,
    // Resets Scheme:
    output reg 	   AppReset_oqr,
    // WishBone Bus Interface:
    output 	   WbMasterCyc_oq,
    output 	   WbMasterStb_oq,
    output [ 24:0] WbMasterAdr_oqb25, // Comment: The actual width of the address bus is set to 21 for the moment: top bits stuck to 0
    output 	   WbMasterWr_oq,
    output [ 31:0] WbMasterDat_oqb32,
    input [ 31:0]  WbMasterDat_ib32,
    input 	   WbMasterAck_i,
    input 	   WbSlaveCyc_i,
    input 	   WbSlaveStb_i,
    input [ 24:0]  WbSlaveAdr_ib25,
    input 	   WbSlaveWr_i,
    input [ 31:0]  WbSlaveDat_ib32,
    output [ 31:0] WbSlaveDat_ob32,
    output 	   WbSlaveAck_o,
    // LED Control:
    input [ 8:1]   Led_ib8,
    // Interrupt Request:
    input [ 23:0]  InterruptRequest_ib24,
    // GPIO Direction Control:
    input 	   GpIo1DirOut_i,
    output 	   GpIo1DirOutStatus_o,
    input 	   GpIo2DirOut_i,
    output 	   GpIo2DirOutStatus_o,
    input 	   GpIo34DirOut_i,
    output 	   GpIo34DirOutStatus_o,
    input 	   GpIo1EnTerm_i,
    output 	   GpIo1EnTermStatus_o,
    input 	   GpIo2EnTerm_i,
    output 	   GpIo2EnTermStatus_o,
    input 	   GpIo3EnTerm_i,
    output 	   GpIo3EnTermStatus_o,
    input 	   GpIo4EnTerm_i,
    output 	   GpIo4EnTermStatus_o,
    // AppSfp1:
    output 	   AppSfp1Present_oq,
    output 	   AppSfp1TxFault_oq,
    output 	   AppSfp1Los_oq,
    input 	   AppSfp1TxDisable_i,
    input 	   AppSfp1RateSelect_i,
    output 	   StatusAppSfp1TxDisable_oq,
    output 	   StatusAppSfp1RateSelect_oq,
    // AppSfp2:
    output 	   AppSfp2Present_oq,
    output 	   AppSfp2TxFault_oq,
    output 	   AppSfp2Los_oq,
    input 	   AppSfp2TxDisable_i,
    input 	   AppSfp2RateSelect_i,
    output 	   StatusAppSfp2TxDisable_oq,
    output 	   StatusAppSfp2RateSelect_oq,
    // AppSfp3:
    output 	   AppSfp3Present_oq,
    output 	   AppSfp3TxFault_oq,
    output 	   AppSfp3Los_oq,
    input 	   AppSfp3TxDisable_i,
    input 	   AppSfp3RateSelect_i,
    output 	   StatusAppSfp3TxDisable_oq,
    output 	   StatusAppSfp3RateSelect_oq,
    // AppSfp4:
    output 	   AppSfp4Present_oq,
    output 	   AppSfp4TxFault_oq,
    output 	   AppSfp4Los_oq,
    input 	   AppSfp4TxDisable_i,
    input 	   AppSfp4RateSelect_i,
    output 	   StatusAppSfp4TxDisable_oq,
    output 	   StatusAppSfp4RateSelect_oq,
    //==== WishBone interface for the I2C slaves on the Muxes ====//
    input 	   I2cWbCyc_i,
    input 	   I2cWbStb_i,
    input 	   I2cWbWe_i,
    input [ 11:0]  I2cWbAdr_ib12,
    input [ 7:0]   I2cWbDat_ib8,
    output [ 7:0]  I2cWbDat_ob8,
    output 	   I2cWbAck_o,
    // geographic address pulled out:
    output [4:0]   VmeGa_onb5,
    output [4:0]   VmeGaP_on
    );

   //=======================================  Declarations  =====================================//

   localparam  g_SysInfo_b96 = "VFC-HD"; // Up to 96 bits -> 12 ASCII Characters (12 Bytes = 3 words)

   //--
   reg [ 1:0] 	   VmeSysReset_rx2    = 2'b11; // Comment: Initialised for Power-On Reset
   reg 		   Reset_rq           = 1'b1;  // Comment: Initialised for Power-On Reset
   wire 	   a_PowerUpAppReset_r;
   //--
   wire [ 4:0] 	   VmeGa_nb5; // Comment: Need to be accessed from the I2C exp
   wire 	   VmeGap_n;  // Comment: Need to be accessed from the I2C exp
   wire [ 7:1] 	   VmeIrq_nb7;
   wire 	   VmeAccess, VmeDtAck_n;
   wire 	   WbCyc, WbStb, WbWe, WbAck;
   wire [21:0] 	   WbAdr_b22;
   wire [31:0] 	   WbDatMiSo_b32, WbDatMoSi_b32;
   //--
   wire 	   WbAckSysAppIdReg, WbStbSysAppIdReg;
   wire [31:0] 	   WbDatSysAppIdReg_b32;
   wire 	   WbAckSysCtrlReg, WbStbSysCtrlReg;
   wire [31:0] 	   WbDatSysCtrlReg_b32;
   wire 	   WbAckIntManager, WbStbIntManager;
   wire [31:0] 	   WbDatIntManager_b32;
   wire 	   WbAckSpiMaster, WbStbSpiMaster;
   wire [31:0] 	   WbDatSpiMaster_b32;
   wire 	   WbAckUniqueIdReader, WbStbUniqueIdReader;
   wire [31:0] 	   WbDatUniqueIdReader_b32;
   wire 	   WbStbI2cIoExpAndMux, WbAckI2cIoExpAndMux;
   wire [31:0] 	   WbDatI2cIoExpAndMux_b32;
   wire 	   WbStbSfpStatus, WbAckSfpStatus;
   wire [31:0] 	   WbDatSfpStatus_b32;
   wire 	   WbStbVfcConf, WbAckVfcConf;
   wire [31:0] 	   WbDatVfcConf_b32;
   //--
   wire [95:0] 	   SysInfo_b96;
   //--
   wire [31:0] 	   SysCtrlReg0_b32;
   //--
   wire 	   IntEnable, IntModeRoRa, IntSourceToRead, NewIntRequest;
   wire [ 2:0] 	   IntLevel_b3;
   wire [ 7:0] 	   IntVector_b8;
   wire [31:0] 	   IntRequestBus_ab32;
   //--
   wire 	   SpiClk_k, SpiMoSi;
   wire [31:0] 	   SpiMiSo_b32, SpiSs_nb32;
   //--
   wire 	   IoExpWrReq, IoExpWrOn, IoExpRdReq, IoExpRdOn;
   wire [ 2:0] 	   IoExpAddr_b3;
   wire [ 1:0] 	   IoExpRegAddr_b2;
   wire [ 7:0] 	   IoExpData_b8;
   wire 	   I2cSlaveWrReq, I2cSlaveWrOn, I2cSlaveRdReq, I2cSlaveRdOn, I2cMuxAddress;
   wire [ 1:0] 	   I2cMuxChannel_b2;
   wire [ 6:0] 	   I2cSlaveAddr_b7;
   wire [ 7:0] 	   I2cSlaveRegAddr_b8, I2cSlaveByte_b8;
   wire 	   I2cExpMuxMstrBusy, I2cExpMuxMstrDav, I2cExpMuxMstrAckError;
   wire [ 7:0] 	   I2cExpMuxMstrData_b8;
   //--
   wire 	   I2CMuxAndExpInitDone;

   //======================================  System Logic  ======================================//

   //==== Unused pins ====//

   assign WbSlaveDat_ob32      = 32'b0;
   assign WbSlaveAck_o         = 1'b0;
   assign ResetFpgaConfigN_orn = 1'b1;

   //==== Clocks Scheme ====//

   //===== Resets Scheme ====//

   // System Reset:

   always @(posedge ClkRs_ix.clk) VmeSysReset_rx2 <= #1 {VmeSysReset_rx2[0], ~VmeSysReset_irn};
   always @(posedge ClkRs_ix.clk) Reset_rq        <= #1  VmeSysReset_rx2[1];

   // Application Reset:
   always @(posedge ClkRs_ix.clk) AppReset_oqr <= #1 Reset_rq || a_PowerUpAppReset_r;

   //==== VME Interface & Wishbone Address Decoder ====//

   assign VmeAOe_oen = 1'b0;
   assign VmeADir_o  = 1'b0;
   assign VmeGa_onb5 = VmeGa_nb5;
   assign VmeGaP_on = VmeGap_n;


   VmeInterfaceWb #(
		    .g_LowestGaAddressBit  (24),
		    .g_ClocksIn2us         (250_000))
   i_VmeInterfaceWb (
		     .Clk_ik                (ClkRs_ix.clk),
		     .Rst_irq               (Reset_rq),
		     .VmeGa_inb5            (VmeGa_nb5),
		     .VmeGap_in             (VmeGap_n),
		     .VmeAs_in              (VmeAs_in),
		     .VmeDs_inb2            (VmeDs_inb2),
		     .VmeAm_ib6             (VmeAm_ib6),
		     .VmeWr_in              (VmeWrite_in),
		     .VmeDtAck_on           (VmeDtAck_n),
		     .VmeLWord_in           (VmeLWord_ion),
		     .VmeA_ib31             (VmeA_iob31),
		     .VmeD_iob32            (VmeD_iob32),
		     .VmeIack_in            (VmeIack_in),
		     .VmeIackIn_in          (VmeIackIn_in),
		     .VmeIackOut_on         (VmeIackOut_on),
		     .VmeIrq_onb7           (VmeIrq_nb7),
		     .VmeDataBuffsOutMode_o (VmeDDir_o),
		     .VmeAccess_o           (VmeAccess),
		     .IntEnable_i           (IntEnable),
		     .IntModeRora_i         (IntModeRoRa),
		     .IntLevel_ib3          (IntLevel_b3),
		     .IntVector_ib8         (IntVector_b8),
		     .IntSourceToRead_i     (IntSourceToRead),
		     .NewIntRequest_iqp     (NewIntRequest),
		     .WbCyc_o               (WbCyc),
		     .WbStb_o               (WbStb),
		     .WbWe_o                (WbWe),
		     .WbAdr_ob              (WbAdr_b22),
		     .WbDat_ib32            (WbDatMiSo_b32),
		     .WbDat_ob32            (WbDatMoSi_b32),
		     .WbAck_i               (WbAck));

   assign VmeIrq_ob7   = ~VmeIrq_nb7;
   assign VmeDOe_oen   = ~VmeAccess;
   assign VmeDtAckOe_o = ~VmeDtAck_n;
   assign VmeA_iob31   = {31{1'bz}};
   assign VmeLWord_ion = 1'bz;

   AddrDecoderWbSys i_AddrDecoderWbSys (
					.Clk_ik                 (ClkRs_ix.clk),
					.Adr_ib22               (WbAdr_b22),
					.Stb_i                  (WbStb),
					.Dat_oqb32              (WbDatMiSo_b32),
					.Ack_oq                 (WbAck),
					//--
					.DatSysAppIdReg_ib32    (WbDatSysAppIdReg_b32),
					.AckSysAppIdReg_i       (WbAckSysAppIdReg),
					.StbSysAppIdReg_oq      (WbStbSysAppIdReg),
					//--
					.DatSysCtrlReg_ib32     (WbDatSysCtrlReg_b32),
					.AckSysCtrlReg_i        (WbAckSysCtrlReg),
					.StbSysCtrlReg_oq       (WbStbSysCtrlReg),
					//--
					.DatIntManager_ib32     (WbDatIntManager_b32),
					.AckIntManager_i        (WbAckIntManager),
					.StbIntManager_oq       (WbStbIntManager),
					//--
					.DatSpiMaster_ib32      (WbDatSpiMaster_b32),
					.AckSpiMaster_i         (WbAckSpiMaster),
					.StbSpiMaster_oq        (WbStbSpiMaster),
					//--
					.DatUniqueIdReader_ib32 (WbDatUniqueIdReader_b32),
					.AckUniqueIdReader_i    (WbAckUniqueIdReader),
					.StbUniqueIdReader_oq   (WbStbUniqueIdReader),
					//--
					.DatI2cIoExpAndMux_ib32 (WbDatI2cIoExpAndMux_b32),
					.AckI2cIoExpAndMux_i    (WbAckI2cIoExpAndMux),
					.StbI2cIoExpAndMux_oq   (WbStbI2cIoExpAndMux),
					//--
					.DatSfpStatus_ib32      (WbDatSfpStatus_b32),
					.AckSfpStatus_i         (WbAckSfpStatus),
					.StbSfpStatus_oq        (WbStbSfpStatus),
					//--
					.DatVfcConf_ib32        (WbDatVfcConf_b32),
					.AckVfcConf_i           (WbAckVfcConf),
					.StbVfcConf_oq          (WbStbVfcConf),
					//--
					.DatAppSlaveBus_ib32    (WbMasterDat_ib32),
					.AckAppSlaveBus_i       (WbMasterAck_i),
					.StbAppSlaveBus_oq      (WbMasterStb_oq));

   //==== System/Application ID Registers ====//

   Generic16InputRegs i_SysAppIdReg(
				    .Clk_ik          (ClkRs_ix.clk),
				    .Rst_irq         (Reset_rq),
				    .Cyc_i           (WbCyc),
				    .Stb_i           (WbStbSysAppIdReg),
				    .Adr_ib4         (WbAdr_b22[3:0]),
				    .Dat_oab32       (WbDatSysAppIdReg_b32),
				    .Ack_oa          (WbAckSysAppIdReg),
				    .Reg0Value_ib32  ({g_SystemReleaseYear_b8, g_SystemReleaseMonth_b8, g_SystemReleaseDay_b8, g_SystemVersion_b8}),
				    .Reg1Value_ib32  (SysInfo_b96[ 95: 64]),
				    .Reg2Value_ib32  (SysInfo_b96[ 63: 32]),
				    .Reg3Value_ib32  (SysInfo_b96[ 31:  0]),
				    .Reg4Value_ib32  ({AppReleaseYear_ib8, AppReleaseMonth_ib8, AppReleaseDay_ib8, AppVersion_ib8}),
				    .Reg5Value_ib32  (AppInfo_ib352[351:320]),
				    .Reg6Value_ib32  (AppInfo_ib352[319:288]),
				    .Reg7Value_ib32  (AppInfo_ib352[287:256]),
				    .Reg8Value_ib32  (AppInfo_ib352[255:224]),
				    .Reg9Value_ib32  (AppInfo_ib352[223:192]),
				    .Reg10Value_ib32 (AppInfo_ib352[191:160]),
				    .Reg11Value_ib32 (AppInfo_ib352[159:128]),
				    .Reg12Value_ib32 (AppInfo_ib352[127: 96]),
				    .Reg13Value_ib32 (AppInfo_ib352[ 95: 64]),
				    .Reg14Value_ib32 (AppInfo_ib352[ 63: 32]),
				    .Reg15Value_ib32 (AppInfo_ib352[ 31:  0]));

   assign SysInfo_b96 = {g_SysInfo_b96, {96-$size(g_SysInfo_b96){1'b0}}};

   //==== System Control Registers ====//
     // NOTE CHANGE FUNCTIONALITY WRT BEBI SYSTEM: RESET FLAG IS
     // AUTOCLEARED, NO NEED TO SET IT BACK TO ZERO
   Generic4OutputRegs #(
			.Reg0Default     (g_PowerUpAppResetLevel),
			.Reg0AutoClrMask (32'hFFFFFFE),
			.Reg1Default     (32'h0000000),
			.Reg1AutoClrMask (32'hFFFFFFF),
			.Reg2Default     (32'h0000000),
			.Reg2AutoClrMask (32'hFFFFFFF),
			.Reg3Default     (32'h0000000),
			.Reg3AutoClrMask (32'hFFFFFFF))
   i_SysCtrlRegs (
		  .Clk_ik          (ClkRs_ix.clk),
		  .Rst_irq         (Reset_rq),
		  .Cyc_i           (WbCyc),
		  .Stb_i           (WbStbSysCtrlReg),
		  .We_i            (WbWe),
		  .Adr_ib2         (WbAdr_b22[1:0]),
		  .Dat_ib32        (WbDatMoSi_b32),
		  .Dat_oab32       (WbDatSysCtrlReg_b32),
		  .Ack_oa          (WbAckSysCtrlReg),
		  .Reg0Value_ob32  (SysCtrlReg0_b32),
		  .Reg1Value_ob32  (),
		  .Reg2Value_ob32  (),
		  .Reg3Value_ob32  ());

   assign a_PowerUpAppReset_r = SysCtrlReg0_b32[0];

   //==== Interrupt Manager ====//

   assign IntRequestBus_ab32[31:24] = {8'b0};
   assign IntRequestBus_ab32[23: 0] = InterruptRequest_ib24;

   // NOTE THAT WE ARE CONSIDERABLY INCREASING THE FIFO DEPTH. it
   // turns out, that even if the IRQs from the motors are coming
   // separately, they actually can come in such short-interval in
   // between, that they cause FIFO or interruptmanager to flood. This
   // is indeed an issue, as it was expected that this situation
   // should never happen. The remedy for this is to increase the fifo
   // depth. Now, if 16 motors on a link can cause 16
   // 'almost-consecutive' irqs, they have to be stored in the fifo. 4
   // links at the time this makes FIFO of at least 64 words wide. And
   // this situation is very easy to check: issue general stopmove to
   // all the motors at the same time. And you have instantly 16
   // IRQs. So being on safe side we make fifo of 127 irq records.
   InterruptManagerWb #(.g_FifoAddressWidth(7))
   i_InterruptManagerWb (
			 .Rst_irq              (Reset_rq),
			 .Clk_ik               (ClkRs_ix.clk),
			 .Cyc_i                (WbCyc),
			 .Stb_i                (WbStbIntManager),
			 .We_i                 (WbWe),
			 .Adr_ib3              (WbAdr_b22[2:0]),
			 .Dat_ib32             (WbDatMoSi_b32),
			 .Dat_oab32            (WbDatIntManager_b32),
			 .Ack_oa               (WbAckIntManager),
			 .IntRequestLines_ib32 (IntRequestBus_ab32),
			 .IntEnable_o          (IntEnable),
			 .IntModeRora_o        (IntModeRoRa),
			 .IntLevel_ob3         (IntLevel_b3),
			 .IntVector_ob8        (IntVector_b8),
			 .IntSourceToRead_o    (IntSourceToRead),
			 .NewIntRequest_oq     (NewIntRequest));

   //==== VFC Reconfiguration ====//

   vfc_configuration #(
		       .g_Synthesis           (g_Synthesis),
		       .AUTO_APPLICATION_LOAD (g_IsGoldenImage),
		       .SETUP_LOAD_ONBOOT     (g_IsGoldenImage))
   i_VfcConfiguration (
		       .ClkEeprom_ik          (ClkRs20MHz_ix.clk),
		       .RstEeprom_irn         (~ClkRs20MHz_ix.reset),
		       .Clk_ik                (ClkRs_ix.clk),
		       .Cyc_i                 (WbCyc),
		       .Stb_i                 (WbStbVfcConf),
		       .We_i                  (WbWe),
		       .Adr_ib15              (WbAdr_b22[14:0]),
		       .Dat_ib32              (WbDatMoSi_b32),
		       .Dat_oab32             (WbDatVfcConf_b32),
		       .Ack_oa                (WbAckVfcConf));

   //==== SPI INTERFACES: Vadj pot, ADC ====//

   SpiMasterWb i_SpiMaster (
			    .Clk_ik           (ClkRs_ix.clk),
			    .Rst_irq          (Reset_rq),
			    .Cyc_i            (WbCyc),
			    .Stb_i            (WbStbSpiMaster),
			    .We_i             (WbWe),
			    .Adr_ib3          (WbAdr_b22[2:0]),
			    .Dat_ib32         (WbDatMoSi_b32),
			    .Dat_oab32        (WbDatSpiMaster_b32),
			    .Ack_oa           (WbAckSpiMaster),
			    .WaitingNewData_o (),
			    .ModuleIdle_o     (),
			    .SClk_o           (SpiClk_k),
			    .MoSi_o           (SpiMoSi),
			    .MiSo_ib32        (SpiMiSo_b32),
			    .SS_onb32         (SpiSs_nb32));

   assign VadjCs_o    = SpiSs_nb32[2];
   assign VadjSclk_ok = SpiClk_k;
   assign VadjDin_o   = SpiMoSi;

   assign SpiMiSo_b32[31:4] = 28'b0;
   assign SpiMiSo_b32[3]    = VAdcDout_i;
   assign SpiMiSo_b32[2:0]  = 3'b0;
   assign VAdcDin_o         = SpiMoSi;
   assign VAdcCs_o          = SpiSs_nb32[3];
   assign VAdcSclk_ok       = SpiClk_k;

   //==== Unique-ID/Temp Controller ====//

   UniqueIdReader #(
		    .g_OneUsClkCycles(40))
   i_UniqueIdReader (
		     .Rst_irq       (Reset_rq),
		     .Clk_ik        (ClkRs_ix.clk),
		     .Cyc_i         (WbCyc),
		     .Stb_i         (WbStbUniqueIdReader),
		     .We_i          (WbWe),
		     .Adr_ib2       (WbAdr_b22[1:0]),
		     .Dat_ib32      (WbDatMoSi_b32),
		     .Dat_oab32     (WbDatUniqueIdReader_b32),
		     .Ack_oa        (WbAckUniqueIdReader),
		     .OneWireBus_io (TempIdDq_ioz));

   //==== IO Expander & Multiplexor I2C Master ====//

   I2cExpAndMuxReqArbiter i_I2cExpAndMuxReqArbiter (
						    .Clk_ik                     (ClkRs_ix.clk),
						    .Rst_irq                    (Reset_rq),
						    .IoExpWrReq_oq              (IoExpWrReq),
						    .IoExpWrOn_i                (IoExpWrOn),
						    .IoExpRdReq_oq              (IoExpRdReq),
						    .IoExpRdOn_i                (IoExpRdOn),
						    .IoExpAddr_oqb3             (IoExpAddr_b3),
						    .IoExpRegAddr_oqb2          (IoExpRegAddr_b2),
						    .IoExpData_oqb8             (IoExpData_b8),
						    .I2cSlaveWrReq_oq           (I2cSlaveWrReq),
						    .I2cSlaveWrOn_i             (I2cSlaveWrOn),
						    .I2cSlaveRdReq_oq           (I2cSlaveRdReq),
						    .I2cSlaveRdOn_i             (I2cSlaveRdOn),
						    .I2cMuxAddress_oq           (I2cMuxAddress),
						    .I2cMuxChannel_oqb2         (I2cMuxChannel_b2),
						    .I2cSlaveAddr_oqb7          (I2cSlaveAddr_b7),
						    .I2cSlaveRegAddr_oqb8       (I2cSlaveRegAddr_b8),
						    .I2cSlaveByte_oqb8          (I2cSlaveByte_b8),
						    .MasterBusy_i               (I2cExpMuxMstrBusy),
						    .MasterNewByteRead_ip       (I2cExpMuxMstrDav),
						    .MasterByteOut_ib8          (I2cExpMuxMstrData_b8),
						    .MasterAckError_i           (I2cExpMuxMstrAckError),
						    .IoExpApp12Int_ian          (I2CIoExpIntApp12_ian),
						    .IoExpApp34Int_ian          (I2CIoExpIntApp34_ian),
						    .IoExpBstEthInt_ian         (I2CIoExpIntBstEth_ian),
						    .IoExpLosInt_ian            (I2CMuxIntLos_ian),
						    .IoExpBlmInInt_ian          (I2CIoExpIntBlmIn_ian),
						    .InitDone_oq                (I2CMuxAndExpInitDone),
						    .VmeGa_onqb5                (VmeGa_nb5),
						    .VmeGaP_onq                 (VmeGap_n),
						    .Led_iab8                   (Led_ib8),
						    .StatusLed_ob8              (),
						    .GpIo1A2B_ia                (GpIo1DirOut_i),
						    .EnGpIo1Term_ia             (GpIo1EnTerm_i),
						    .GpIo2A2B_ia                (GpIo2DirOut_i),
						    .EnGpIo2Term_ia             (GpIo2EnTerm_i),
						    .GpIo34A2B_ia               (GpIo34DirOut_i),
						    .EnGpIo3Term_ia             (GpIo3EnTerm_i),
						    .EnGpIo4Term_ia             (GpIo4EnTerm_i),
						    .StatusGpIo1A2B_oq          (GpIo1DirOutStatus_o),
						    .StatusEnGpIo1Term_oq       (GpIo1EnTermStatus_o),
						    .StatusGpIo2A2B_oq          (GpIo2DirOutStatus_o),
						    .StatusEnGpIo2Term_oq       (GpIo2EnTermStatus_o),
						    .StatusGpIo34A2B_oq         (GpIo34DirOutStatus_o),
						    .StatusEnGpIo3Term_oq       (GpIo3EnTermStatus_o),
						    .StatusEnGpIo4Term_oq       (GpIo4EnTermStatus_o),
						    // this is P0
						    // connector
						    // connection, not needed!
						    .BlmIn_oqb8                 (),
						    .AppSfp1Present_oq          (AppSfp1Present_oq),
						    .AppSfp1TxFault_oq          (AppSfp1TxFault_oq),
						    .AppSfp1Los_oq              (AppSfp1Los_oq),
						    .AppSfp1TxDisable_ia        (AppSfp1TxDisable_i),
						    .AppSfp1RateSelect_ia       (AppSfp1RateSelect_i),
						    .StatusAppSfp1TxDisable_oq  (StatusAppSfp1TxDisable_oq),
						    .StatusAppSfp1RateSelect_oq (StatusAppSfp1RateSelect_oq),
						    .AppSfp2Present_oq          (AppSfp2Present_oq),
						    .AppSfp2TxFault_oq          (AppSfp2TxFault_oq),
						    .AppSfp2Los_oq              (AppSfp2Los_oq),
						    .AppSfp2TxDisable_ia        (AppSfp2TxDisable_i),
						    .AppSfp2RateSelect_ia       (AppSfp2RateSelect_i),
						    .StatusAppSfp2TxDisable_oq  (StatusAppSfp2TxDisable_oq),
						    .StatusAppSfp2RateSelect_oq (StatusAppSfp2RateSelect_oq),
						    .AppSfp3Present_oq          (AppSfp3Present_oq),
						    .AppSfp3TxFault_oq          (AppSfp3TxFault_oq),
						    .AppSfp3Los_oq              (AppSfp3Los_oq),
						    .AppSfp3TxDisable_ia        (AppSfp3TxDisable_i),
						    .AppSfp3RateSelect_ia       (AppSfp3RateSelect_i),
						    .StatusAppSfp3TxDisable_oq  (StatusAppSfp3TxDisable_oq),
						    .StatusAppSfp3RateSelect_oq (StatusAppSfp3RateSelect_oq),
						    .AppSfp4Present_oq          (AppSfp4Present_oq),
						    .AppSfp4TxFault_oq          (AppSfp4TxFault_oq),
						    .AppSfp4Los_oq              (AppSfp4Los_oq),
						    .AppSfp4TxDisable_ia        (AppSfp4TxDisable_i),
						    .AppSfp4RateSelect_ia       (AppSfp4RateSelect_i),
						    .StatusAppSfp4TxDisable_oq  (StatusAppSfp4TxDisable_oq),
						    .StatusAppSfp4RateSelect_oq (StatusAppSfp4RateSelect_oq),
						    .BstSfpPresent_oq           (),
						    .BstSfpTxFault_oq           (),
						    .BstSfpLos_oq               (),
						    .BstSfpTxDisable_ia         (1'b1),
						    .BstSfpRateSelect_ia        (1'b1),
						    .StatusBstSfpTxDisable_oq   (),
						    .StatusBstSfpRateSelect_oq  (),
						    .EthSfpPresent_oq           (),
						    .EthSfpTxFault_oq           (),
						    .EthSfpLos_oq               (),
						    .EthSfpTxDisable_ia         (1'b1),
						    .EthSfpRateSelect_ia        (1'b0),
						    .StatusEthSfpTxDisable_oq   (),
						    .StatusEthSfpRateSelect_oq  (),
						    .CdrLos_oq                  (),
						    .CdrLol_oq                  (),
						    .I2cWbCyc_i                 (I2cWbCyc_i),
						    .I2cWbStb_i                 (I2cWbStb_i),
						    .I2cWbWe_i                  (I2cWbWe_i),
						    .I2cWbAdr_ib12              (I2cWbAdr_ib12),
						    .I2cWbDat_ib8               (I2cWbDat_ib8),
						    .I2cWbDat_ob8               (I2cWbDat_ob8),
						    .I2cWbAck_o                 (I2cWbAck_o),
						    .WbCyc_i                    (WbCyc),
						    .WbStb_i                    (WbStbI2cIoExpAndMux),
						    .WbWe_i                     (WbWe),
						    .WbDat_ib32                 (WbDatMoSi_b32),
						    .WbDat_oqb32                (WbDatI2cIoExpAndMux_b32),
						    .WbAck_oa                   (WbAckI2cIoExpAndMux));

   I2cExpAndMuxMaster #(
			.g_SclHalfPeriod     (10'd160))
   i_I2cExpAndMuxMaster (
			 .Clk_ik              (ClkRs_ix.clk),
			 .Rst_irq             (Reset_rq),
			 .IoExpWrReq_i        (IoExpWrReq),
			 .IoExpWrOn_oq        (IoExpWrOn),
			 .IoExpRdReq_i        (IoExpRdReq),
			 .IoExpRdOn_oq        (IoExpRdOn),
			 .IoExpAddr_ib3       (IoExpAddr_b3),
			 .IoExpRegAddr_ib2    (IoExpRegAddr_b2),
			 .IoExpData_ib8       (IoExpData_b8),
			 .I2cSlaveWrReq_i     (I2cSlaveWrReq),
			 .I2cSlaveWrOn_o      (I2cSlaveWrOn),
			 .I2cSlaveRdReq_i     (I2cSlaveRdReq),
			 .I2cSlaveRdOn_o      (I2cSlaveRdOn),
			 .I2cMuxAddress_i     (I2cMuxAddress),
			 .I2cMuxChannel_ib2   (I2cMuxChannel_b2),
			 .I2cSlaveAddr_ib7    (I2cSlaveAddr_b7),
			 .I2cSlaveRegAddr_ib8 (I2cSlaveRegAddr_b8),
			 .I2cSlaveByte_ib8    (I2cSlaveByte_b8),
			 .Busy_o              (I2cExpMuxMstrBusy),
			 .NewByteRead_op      (I2cExpMuxMstrDav),
			 .ByteOut_ob8         (I2cExpMuxMstrData_b8),
			 .AckError_op         (I2cExpMuxMstrAckError),
			 .Scl_ioz             (I2cMuxScl_iokz),
			 .Sda_ioz             (I2cMuxSda_ioz));

   //==== Statuses of the SFPs ====//

   Generic8InputRegs i_SfpStatuses(
				   .Clk_ik         (ClkRs_ix.clk),
				   .Rst_irq        (Reset_rq),
				   .Cyc_i          (WbCyc),
				   .Stb_i          (WbStbSfpStatus),
				   .Adr_ib3        (WbAdr_b22[2:0]),
				   .Dat_oab32      (WbDatSfpStatus_b32),
				   .Ack_oa         (WbAckSfpStatus),
				   .Reg0Value_ib32 ({16'b0,
						     {AppSfp1Present_oq, 3'b0}, {2'b0,
										 AppSfp1TxFault_oq, AppSfp1Los_oq},
						     {2'b0, AppSfp1TxDisable_i,
						      StatusAppSfp1TxDisable_oq}, {2'b0,
										   AppSfp1RateSelect_i,
										   StatusAppSfp1RateSelect_oq}}),

				   .Reg1Value_ib32 ({16'b0,
						     {AppSfp2Present_oq, 3'b0}, {2'b0,
										 AppSfp2TxFault_oq, AppSfp2Los_oq},
						     {2'b0, AppSfp2TxDisable_i,
						      StatusAppSfp2TxDisable_oq}, {2'b0,
										   AppSfp2RateSelect_i,
										   StatusAppSfp2RateSelect_oq}}),

				   .Reg2Value_ib32 ({16'b0,
						     {AppSfp3Present_oq, 3'b0}, {2'b0,
										 AppSfp3TxFault_oq, AppSfp3Los_oq},
						     {2'b0, AppSfp3TxDisable_i,
						      StatusAppSfp3TxDisable_oq}, {2'b0,
										   AppSfp3RateSelect_i,
										   StatusAppSfp3RateSelect_oq}}),

				   .Reg3Value_ib32 ({16'b0,
						     {AppSfp4Present_oq, 3'b0}, {2'b0,
										 AppSfp4TxFault_oq, AppSfp4Los_oq},
						     {2'b0, AppSfp4TxDisable_i,
						      StatusAppSfp4TxDisable_oq}, {2'b0,
										   AppSfp4RateSelect_i,
										   StatusAppSfp4RateSelect_oq}}),

				   // emulates ETH fail SFP socket -
				   // this one is not used in MCOI
				   // project, hence the signals are
				   // not propagated to application
				   .Reg4Value_ib32 ({16'b0,
						     {1'b0,  3'b0}, {2'b0,
								     1'b1,  1'b1},
						     {2'b0, 1'b1,
						      1'b1}, {2'b0,
							      1'b0,
							      1'b0}}),
				   // following register EMULATES
				   // missing BST decoder by
				   // indicating to software BST fail
				   .Reg5Value_ib32 ({16'b0, {1'b0, 3'b0}, {2'b0,  1'b1,  1'b1}, {2'b0,
												 1'b1,   1'b1}, {2'b0,
														 1'b1,   1'b0}}),
				   .Reg6Value_ib32 (),
				   .Reg7Value_ib32 ());


   //==== System <=> Application Wishbone Interface ====//

   assign WbMasterCyc_oq    = WbCyc;
   assign WbMasterDat_oqb32 = WbDatMoSi_b32;
   assign WbMasterAdr_oqb25 = {4'b0, WbAdr_b22[20:0]}; //Comment: NB - For the moment only 21 bits are available
   assign WbMasterWr_oq     =  WbWe;

endmodule
