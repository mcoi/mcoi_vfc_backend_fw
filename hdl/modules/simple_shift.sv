//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) May 2018 CERN

//-----------------------------------------------------------------------------
// @file SIMPLE_SHIFT.SV
// @brief simple shift register
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 24 May 2018
// @details
// Set number of cells to propagate, it will shift. Used to pass a bit
// through FF stages, typical use in CDC
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module simple_shift
  #(parameter   g_shiftLength  = 4)
  (input logic clk_ik,
   input logic data_i,
   output logic data_o
   );

   bit [g_shiftLength-1:0] datapass_b;

   always_ff @(posedge clk_ik)
     datapass_b <= {datapass_b[g_shiftLength-2:0], data_i};

   assign data_o = datapass_b[g_shiftLength-1];

endmodule // simple_shift
