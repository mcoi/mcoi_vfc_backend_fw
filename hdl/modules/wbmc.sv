//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file WBMC.SV
// @brief
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 23 October 2017
// @details Module implements wishbone interface to a single motor. This
// interface has currently 16 registers (c_NumberOfRegisters) implemented, all
// of them are writable from the wishbone _except_ register 10
// (Register_imb[9]), which is status, and hence is writable only by this
// module. The module accepts internal and external triggers (TrigMoveExt_i,
// TrigMoveInt_i) to trigger the motion. It can be as well triggered by writing
// into register 9 (Register_omb[8]) into 'startmove' bit. This register is edge
// triggered - writing into it causes triggering for a single time. This
// register is notbacked up by fifo, so multiple consecutive triggering events
// written into this register will be ignored until current move is finished.
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;
import MCPkg::*;

// wishbone interface comes from common BI libraries
//`include "../../libs/BI_HDL_Cores/cores_for_synthesis/wishbone/t_WbInterface.sv"


module wbmc
  #(
    // length of the command queue
    parameter   QUEUELENGTH  = 32,
    // interface identifier. Up to 16 identifiers allowed. The identifier just
    // makes a part of the mcstatus word and identifies block, which we're
    // in. Can be used by SW to test if talks to correct backend
    parameter logic[3:0] g_ModuleIdentifier = 0
    )
   (
    // clock/reset typedef
    input 					      ckrs_t ClkRs_ix,
    // force default state by injecting '1'. this clears out
    // registers and stops current operation
    input logic 				      reset_i,
    // input data for interlock mechanism - this is a vector of 16
    // bits x 2 announcing the status of the switches of 'the other
    // motors' in the link. These are coupled with positive and
    // negative mask. Here the first array index is 0=positive
    // direction switches, 1 = negative direction switches. Each
    // direction contains 16 values saying whether 'logically' the
    // given motor is in switch bound or not....
    input logic [1:0][NUMBER_OF_MOTORS_PER_FIBER-1:0] OtherMotorSwitches_2b16,
    // external and internal triggers for the command queue
    input 					      triggers_t triggers, // To i_steppingcontroller of steppingcontroller.v
						      // motor controller interface
						      mc_x mc,
    // IRQ when done:
    output logic 				      IRQ_o,
    // configured switches pulled out for interlock
    output logic [1:0] 				      ConfiguredSwitches_b2,
    output 					      switchstate_t [1:0] SwitchesConfiguration_o,
    // busy flag direct output (one is as well in wishbone regs, but
    // this requires wb cycle, we want info about whether
    // 'works-in-progress' immediatelly)
    output logic 				      Busy_o,
						      // wishbone interface - comes from BI cores
						      t_WbInterface Wb_iot
    );

   // and the same with wishbone - we need to clone the interface and
   // assign the signals

   t_WbInterface #(.g_DataWidth($bits(Wb_iot.DatMiSo_b)),
		   .g_AddressWidth($bits(Wb_iot.Adr_b))) WbInternal_iot
     (Wb_iot.Clk_k,
      Wb_iot.Reset_r | reset_i);
   assign WbInternal_iot.Cyc = Wb_iot.Cyc;
   assign WbInternal_iot.Stb = Wb_iot.Stb;
   assign WbInternal_iot.Adr_b = Wb_iot.Adr_b;
   assign WbInternal_iot.DatMoSi_b = Wb_iot.DatMoSi_b;
   assign WbInternal_iot.Sel_b = Wb_iot.Sel_b;
   assign WbInternal_iot.We = Wb_iot.We;

   assign Wb_iot.DatMiSo_b = WbInternal_iot.DatMiSo_b;
   assign Wb_iot.Ack = WbInternal_iot.Ack;
   assign Wb_iot.Err = WbInternal_iot.Err;
   assign Wb_iot.Rty = WbInternal_iot.Rty;
   assign Wb_iot.Stall = 1'b0;


   // WARNING WARNING!!! Until commit 7057841 the data width was
   // defined as follows:
   // localparam c_DataWidth = $bits(Wb_iot.DatMiSo_b);
   // localparam g_DataWidth = c_DataWidth
   // then clone of wishbone iface (WbInternal_iot) was introduced as
   // it is seen now, but QUARTUS refused to define correctly
   // c_DataWidth and g_DataWidth when following declaration was made:
   // localparam c_DataWidth = $bits(WbInternal_iot.DatMiSo_b);
   // localparam g_DataWidth = c_DataWidth
   // Although it should be exactly as before, Quartus sets the
   // c_DataWidth to '32', but it is evidently 'different' 32 than
   // before, because Register_omb is declared only as 1-bit times 32
   // registers, resulting in compilation failue.
   // Even more mystic is, that if we make direct declaration (see
   // line below), all works as supposed, and $bits of Register_omb
   // returns correct value of 1024 bits (32 registers of 32bits).
   // MODELSIM does not have any problem with the above, and simulates
   // everything correctly. Just quartus fails. Not sure why, so I'm
   // leaving the direct declaration, but KEEP IN MIND, that is breaks
   // the 'chain' of assignments of g_DataWidth (it is declared at
   // top-level and then propagates to all entities, now except of
   // this one, where we re-declare that again!)
   localparam g_DataWidth = 32;
   // number of registers in the register block. These always start from address
   // index zero
   localparam int c_NumberOfRegisters = NUMBER_OF_MC_REGISTERS;
   localparam g_NumberOfRegisters = c_NumberOfRegisters;

   // direction of the register. Register is FROM THE WISHBONE MASTER
   // (i.e. upper entity, as this is slave) writable only if logic '0' at
   // specific bit is asserted. '1' here means direction FROM WBMC to
   // upstream (reading from VME), '0' means direction FROM upstream
   // to WBMC (writing data into a register, which can be as well read
   // back)
   localparam logic [c_NumberOfRegisters-1:0] c_DirectionMiso_b =
   'b11111111_10010000_11100010_00000000;

   // interlock masks:
   logic [NUMBER_OF_MOTORS_PER_FIBER-1:0] ILockMaskP_b16,
					  ILockMaskN_b16;


   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   cntparam_t		CntParam_ix;		// To i_steppingcontroller of steppingcontroller.v
   command_t		ICmdParams_x;		// To i_commandqueuer of commandqueuer.v
   motorcommand_t	ITriggerParams_x;	// To i_commandqueuer of commandqueuer.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   motorcommand_t	OTriggerParams_x;	// From i_commandqueuer of commandqueuer.v
   queuestatus_t	QueueStatus_x;		// From i_commandqueuer of commandqueuer.v
   logic [g_DataWidth-1:0] Register_omb [0:g_NumberOfRegisters-1];// From i_WbRegField of WbRegField.v
   stepperstatus_t	StepperStatus_x;	// From i_steppingcontroller of steppingcontroller.v
   // End of automatics

   // input registers (i.e. those where this entity writes into, and WBMaster
   // reads the content)
   logic [g_DataWidth-1:0] Register_imb [0:g_NumberOfRegisters-1];
   // output command from the command queue
   command_t		OCmdParams_x;		// From i_commandqueuer of
   // each time one motion is done the counter increases by one. Can be used
   // just to diagnose if in time there were some movements or not
   logic unsigned [31:0]    MotionCounter_b32;

   // declaration of the registers. The default value of all of them is set to
   // zero. Masking is setup on the register 9, as this one is triggering
   // register. These registers are setup such, that any bits not used from the
   // 32bit word due to shorter target register are cleared out. This permits in
   // the TB to verify correct vector sizes by writing '1 into that register and
   // read back the trimmed value
   initial begin
      assert (c_NumberOfRegisters == 32) else
	$error("NUMBER OF REGISTERS CHANGED, UPDATE WbRegField declaration part to\
   clear out and setup new registers");

      assert (c_NumberOfRegisters == 32) else
	$error("NUMBER OF REGISTERS CHANGED, UPDATE c_DirectionMiso_b");
      $display("Size of switches block: %d",
	       $bits(CntParam_ix.ExtremitySwitches_b2));

      $display("Number of registers allocated in Wb space: %d",
	       g_NumberOfRegisters);
      $display("Register_omb length: %d", $bits(Register_omb));
      $display("Data width: %d", g_DataWidth);
      $display("WbInternal_iot.DatMiSo_b length: %d",
	       $bits(WbInternal_iot.DatMiSo_b));
      $display("Wb_iot.DatMiSo_b length: %d",
	       $bits(Wb_iot.DatMiSo_b));

   end


   // inject new clock, where reset is combination of original ckrs
   // reset and resetting register:
   ckrs_t ClkRs_x;

   assign ClkRs_x.clk = ClkRs_ix.clk;
   assign ClkRs_x.reset = ClkRs_ix.reset | reset_i;



   // register 16 we have to decode ourselves when actually writting
   // into this register happens, as we want to use it as 'strobe' to
   // clearout the counter position.
   logic WbTransaction, WbTransactionStrobe, ReloadPos, StartByWrite, StopByWrite;
   assign WbTransaction = WbInternal_iot.Cyc & WbInternal_iot.Stb;
   EdgeDetector i_WbTransStrobe (
				 .Clk_ik(WbInternal_iot.Clk_k),
				 .Signal_i(WbTransaction),
				 .Edge_o(),
				 .RisingEdge_o(WbTransactionStrobe),
				 .FallingEdge_o()
				 );
   // decode writing into overridepos register. Registered to gain 1cc
   // and align data to output of WbRegField register 16. It resolves
   // as well start and stop by write commands. These are 'write any
   // value to this register to trigger start or stop'
   always_ff @(posedge ClkRs_x.clk or posedge ClkRs_x.reset) begin
      if (ClkRs_x.reset) begin
	 ReloadPos <= '0;
      end else begin
	 // reloading by write
	 if (WbTransactionStrobe & WbInternal_iot.We &
	     (WbInternal_iot.Adr_b == 16))
	   ReloadPos <= '1;
	 else
	   ReloadPos <= '0;
	 // start by write:
	 if (WbTransactionStrobe & WbInternal_iot.We &
	     (WbInternal_iot.Adr_b == 17))
	   StartByWrite <= '1;
	 else
	   StartByWrite <= '0;
	 // stop by write:
	 if (WbTransactionStrobe & WbInternal_iot.We &
	     (WbInternal_iot.Adr_b == 18))
	   StopByWrite <= '1;
	 else
	   StopByWrite <= '0;
      end
   end

   // all these have to be autocleared
   assert property (@(posedge ClkRs_x.clk)
		    disable iff (ClkRs_x.reset)
		    StopByWrite |=> ~StopByWrite);
   assert property (@(posedge ClkRs_x.clk)
		    disable iff (ClkRs_x.reset)
		    StartByWrite |=> ~StartByWrite);
   assert property (@(posedge ClkRs_x.clk)
		    disable iff (ClkRs_x.reset)
		    ReloadPos |=> ~ReloadPos);


   // this localparameter calculates the masking for the interlock
   // registers to mask away the motor 'itself' from the
   // mask. Example: we're motor 5, and we want to set interlock
   // masks. It is forbidden to mask itself, hence the mask for the
   // registers ilckp/ilckn has to be 0xffff0010, which assures that
   // any time someone tries to interlock the motor itself, it won't
   // be possible. We can reuse g_ModuleIdentifier to calculate the
   // correct mask, as this one tells the number of actual motor.
   localparam ilckmask = 32'hffff_0000 | 32'(2**g_ModuleIdentifier);

   WbRegField
     #(        .g_NumberOfRegisters(c_NumberOfRegisters),
               .g_DataWidth(g_DataWidth),
               .g_DirectionMiso_b(c_DirectionMiso_b),
	       // set default register values:
	       // IRQ enabled, slowdown to 40 and
	       // stepsafterlimitswitch to 10
	       .g_DefaultValue_mb('{(g_DataWidth)'(0), // 0 stepnumber
				    (g_DataWidth)'(0), // 1 direction
				    (g_DataWidth)'(0), // 2 low speed
				    (g_DataWidth)'(0), // 3 high speed
				    (g_DataWidth)'(0), // 4 accrate
				    (g_DataWidth)'(0), // 5 trail
				    (g_DataWidth)'(8), // 6 config
				    (g_DataWidth)'(0), // 7 powerconfig
				    (g_DataWidth)'(0), // 8 triggering
				    (g_DataWidth)'(0), // 9 status (rx)
				    (g_DataWidth)'(40), // 10 slowdown,
				    (g_DataWidth)'(10), // 11 SALSP
				    (g_DataWidth)'(0), // 12 switchesconfig
				    (g_DataWidth)'(0), // 13 poscounter (rx)
				    (g_DataWidth)'(0), // 14 stepsleft (rx)
				    (g_DataWidth)'(0), // 15 mcstatus (rx)
				    (g_DataWidth)'(0), // 16 overridepos
				    (g_DataWidth)'(0), // 17 startmove by writing
				    (g_DataWidth)'(0), // 18 stopmove by writing
				    (g_DataWidth)'(15), // 19 SALSN
				    (g_DataWidth)'(0), // 20 MotionCounter_b32
				    (g_DataWidth)'(0), // 21 ILockMaskP
				    (g_DataWidth)'(0), // 22 ILockMaskN
				    (g_DataWidth)'(0), //
				    (g_DataWidth)'(0), //
				    (g_DataWidth)'(0), //
				    (g_DataWidth)'(0), //
				    (g_DataWidth)'(0), //
				    (g_DataWidth)'(0), //
				    (g_DataWidth)'(0), //
				    (g_DataWidth)'(0), //
				    (g_DataWidth)'(0) //
				    }),
	       .g_AutoClrMask_mb('{(g_DataWidth)'(0),// 0 stepnumber
				   (g_DataWidth)'(0),// 1 direction
				   (g_DataWidth)'(0),// 2 low speed
				   (g_DataWidth)'(0),// 3 high speed
				   (g_DataWidth)'(0),// 4 acc/dec rate
				   (g_DataWidth)'(0),// 5 trail
				   (g_DataWidth)'(0),// 6 config
				   (g_DataWidth)'(0),// 7 power config
				   (g_DataWidth)'('1),// 8 triggering
				   (g_DataWidth)'(0),// 9 status (rx only)
				   (g_DataWidth)'(0),// 10 slowdown
				   (g_DataWidth)'({(g_DataWidth-$bits(CntParam_ix.StepsAfterLimitSwitchP_b6))'('1),
   ($bits(CntParam_ix.StepsAfterLimitSwitchP_b6))'('0)}),// 11 stepsafterlimitswitch positive
				   (g_DataWidth)'({(g_DataWidth-$bits(CntParam_ix.ExtremitySwitches_b2))'('1),
   ($bits(CntParam_ix.ExtremitySwitches_b2))'('0)}),// 12 switchesconfig
				   (g_DataWidth)'(0),// 13 Poscounter (rx)
				   (g_DataWidth)'(0),// 14 stepsleft (rx)
				   (g_DataWidth)'(0),// 15 mcstatus
				   (g_DataWidth)'('1),// 16 overridepos(wr only, flip)
				   (g_DataWidth)'('1),// 17 startmove by writing
				   (g_DataWidth)'('1),// 18 stopmove by writing
				   (g_DataWidth)'({(g_DataWidth-$bits(CntParam_ix.StepsAfterLimitSwitchP_b6))'('1),
						   ($bits(CntParam_ix.StepsAfterLimitSwitchP_b6))'('0)}), // 19 = SALSN
				   (g_DataWidth)'(0),// 20 MotionCounter_b32
				   ilckmask,// 21 ILockMaskP
				   ilckmask,// 22 ILockMaskN
				   (g_DataWidth)'(0),//
				   (g_DataWidth)'(0),//
				   (g_DataWidth)'(0),//
				   (g_DataWidth)'(0),//
				   (g_DataWidth)'(0),//
				   (g_DataWidth)'(0),//
				   (g_DataWidth)'(0),//
				   (g_DataWidth)'(0),//
				   (g_DataWidth)'(0)//
				   })
	       )
   i_WbRegField (
		 // Interfaces
		 .Wb_iot		(WbInternal_iot),
		 /*AUTOINST*/
		 // Outputs
		 .Register_omb		(Register_omb/*[g_DataWidth-1:0].[0:g_NumberOfRegisters-1]*/),
		 // Inputs
		 .Register_imb		(Register_imb/*[g_DataWidth-1:0].[0:g_NumberOfRegisters-1]*/));


   /* --------------------------------------------------------------------------------
    IMPLEMENTATION OF FUNCTIONALITY
    --------------------------------------------------------------------------------*/
   initial begin
      assert (c_NumberOfRegisters == 32) else
	$error("NUMBER OF REGISTERS CHANGED, UPDATE ASSIGNMENTS INTO THOSE REGISTERS");
   end



   logic DeviceInterlocked, ILockP, ILockN;

   // if mask is cleared, we dont' care, in all other cases we do
   // care as switches have to be engaged for those bits in given masks
   assign ILockP = (ILockMaskP_b16 & OtherMotorSwitches_2b16[0]) !=
		   ILockMaskP_b16;
   assign ILockN = (ILockMaskN_b16 & OtherMotorSwitches_2b16[1]) !=
		   ILockMaskN_b16;

   // this value has to be '1' if we're not able to move the motor due to
   // interlocks. This is due to masking of this particular motor
   assign DeviceInterlocked = ILockP || ILockN;


   // registers casting to the command type, these are INPUTs to the command
   // queuer, as this one later on decide, which command is going to serve to
   // stepping controller. OCmdParams_x is the one, which is actually performed
   // by the steppingcontroller
   assign ICmdParams_x.StepNumber_b32 = Register_omb[0];
   assign ICmdParams_x.Direction = Register_omb[1][0];
   assign ICmdParams_x.LowSpeed_b36 = {4'b0, Register_omb[2]};
   assign ICmdParams_x.HighSpeed_b36 = {4'b0, Register_omb[3]};


   assign ICmdParams_x.AccDeccRate_b19 = Register_omb[4][$bits(ICmdParams_x.AccDeccRate_b19)-1:0];
   assign ICmdParams_x.Trail_b32 = Register_omb[5];
   assign ICmdParams_x.globaltrigger.external = Register_omb[6][0];
   assign ICmdParams_x.globaltrigger.internal = Register_omb[6][1];
   assign ICmdParams_x.GenInterrupt = Register_omb[6][3];
   assign ICmdParams_x.powerconfig.enable_move =	Register_omb[7][0];
   assign ICmdParams_x.powerconfig.boost =	Register_omb[7][1];
   assign ICmdParams_x.powerconfig.standby =	Register_omb[7][2];


   // trigger inputs to the command queuer
   assign ITriggerParams_x.StartMove = (StartByWrite | Register_omb[8][0]) & triggers.TriggerEnable_i;
   assign ITriggerParams_x.StopMove = (StopByWrite | Register_omb[8][1]) & triggers.TriggerEnable_i;
   assign ITriggerParams_x.DoQueue = Register_omb[8][3];
   assign ITriggerParams_x.ResetQueue = Register_omb[8][4];
   // resetting counter position comes from two sources: single bit in
   // register[8][2], which resets counter to zero. Or writing a value
   // into register[16]. In both cases this boils down to 'reloading'
   // the counterpos from reload_ib32, but as this register is
   // autocleared, issuing flip bit of [8][2] will cause clearout
   assign ITriggerParams_x.ResetCounterPos = Register_omb[8][2] | ReloadPos;

   assign Register_imb[0] = Register_omb[0];
   assign Register_imb[1] = Register_omb[1];
   assign Register_imb[2] = Register_omb[2];
   assign Register_imb[3] = Register_omb[3];
   assign Register_imb[4] = Register_omb[4];
   assign Register_imb[5] = Register_omb[5];
   assign Register_imb[6] = Register_omb[6];
   assign Register_imb[7] = Register_omb[7];
   assign Register_imb[8] = Register_omb[8];

   // queue status goes outside, from queue items we strip one bit as it is way
   // too long anyways - assign queue status on lobits
   assign Register_imb[9][7:0] = QueueStatus_x;
   assign Register_imb[9][27:8] = '0;
   assign Register_imb[9][31:28] = g_ModuleIdentifier;

   assign Register_imb[10] = Register_omb[10];
   assign Register_imb[11] = Register_omb[11];
   assign Register_imb[12] = Register_omb[12];

   // switches setting register - we can assign them directly as unused portions
   // are cleared out by registry clearing.
   assign CntParam_ix.SlowDown_b32 = Register_omb[10];
   assign CntParam_ix.StepsAfterLimitSwitchP_b6 = Register_omb[11][$size(CntParam_ix.StepsAfterLimitSwitchP_b6)-1:0];
   assign CntParam_ix.ExtremitySwitches_b2 = Register_omb[12][$bits(CntParam_ix.ExtremitySwitches_b2)-1:0];

   // stepping controller register block
   assign Register_imb[13] = StepperStatus_x.PosCounter_b32;
   assign Register_imb[14] = StepperStatus_x.StepsLeft_b32;
   // status register composed of many bits, including state machine
   // state. Note that we store _original_ switches settings as well
   // with inverted ones. This is because it will simplify the
   // calibration process of which motor is what
   assign Register_imb[15][31:24] = StepperStatus_x.State;
   assign Register_imb[15][23:16] = StepperStatus_x.AbortStatus;
   assign Register_imb[15][15:12] = '0; // unused for the moment
   assign Register_imb[15][11] = DeviceInterlocked;
   assign Register_imb[15][10] = '0; // unused for the moment
   assign Register_imb[15][9] = mc.invec.OH_i;
   assign Register_imb[15][8] = mc.invec.StepPFail_i;
   assign Register_imb[15][7:6] = ConfiguredSwitches_b2;
   assign Register_imb[15][5:4] = mc.invec.RawSwitches_b2;
   assign Register_imb[15][3:2] = '0; // unused
   assign Register_imb[15][1] = StepperStatus_x.Busy;
   assign Register_imb[15][0] = '0; // unused

   assign Busy_o = StepperStatus_x.Busy;

   // other registers go to unused state, OMB is 'dont'care' as this
   // gets ignored.
   assign Register_imb[16] = Register_omb[16];
   assign Register_imb[17] = Register_omb[17];
   assign Register_imb[18] = Register_omb[18];
   // SALSN is quite far from SALSP because it was added quite later
   // into the design. This one is for negative direction (DIR=1)
   assign CntParam_ix.StepsAfterLimitSwitchN_b6 = Register_omb[19][$size(CntParam_ix.StepsAfterLimitSwitchN_b6)-1:0];
   assign Register_imb[19] = Register_omb[19];
   // MotionCounter_b32
   assign Register_imb[20] = MotionCounter_b32;
   // interlock masks:
   assign ILockMaskP_b16 = Register_omb[21][15:0];
   assign ILockMaskN_b16 = Register_omb[22][15:0];
   assign Register_imb[21] = Register_omb[21];
   assign Register_imb[22] = Register_omb[22];


   assign Register_imb[23:31] = '{9{32'hdeadbeef}};

   initial begin
      assert ($size(StepperStatus_x.AbortStatus) == 8) else $error("AbortStatus\
   register has to have a lenght of 8 otherwise it does not fit into VME\
   register space, actual vector length is ",
      $size(StepperStatus_x.AbortStatus));

      $display("Size: switches settings.outputswitches: %d",
	       $bits(CntParam_ix.ExtremitySwitches_b2));
   end


   // motion counter:
   always_ff @(posedge ClkRs_x.clk or posedge ClkRs_x.reset) begin
      if (ClkRs_x.reset)
	MotionCounter_b32 <= '0;
      else
	if (StepperStatus_x.Done)
	  MotionCounter_b32++;
   end

   logic Done_i;

   assign Done_i = StepperStatus_x.Done;
   assign IRQ_o = StepperStatus_x.Interrupt;

   // pull out of this entity current configuration of the switches -
   // will be used to transport info over optics slow link
   assign SwitchesConfiguration_o =
				   Register_omb[12][$bits(CntParam_ix.ExtremitySwitches_b2)-1:0];


   // stepping controller reloads the actual position counter
   // according to the register in th e
   steppingcontroller
   i_steppingcontroller (// Inputs
			 .ICmdParams_x		(OCmdParams_x),
			 .ITriggerParams_x		(OTriggerParams_x),
			 .CntParam_ix		(CntParam_ix),
			 .reload_ib32		(Register_omb[16]),
			 .ClkRs_ix		(ClkRs_x),
			 /*AUTOINST*/
			 // Interfaces
			 .mc			(mc),
			 // Outputs
			 .StepperStatus_x	(StepperStatus_x),
			 .ConfiguredSwitches_b2	(ConfiguredSwitches_b2[1:0]),
			 // Inputs
			 .DeviceInterlocked	(DeviceInterlocked),
			 .triggers		(triggers));


   // command queue realizes FIFO-like interface for consecutive execution of
   // movements. It takes input VME commands, and either stores them into the
   // queue, or casts them to the output (OTriggerParams_x and OCmdParams_x) for the
   // execution.
   commandqueuer
     #(/*AUTOINSTPARAM*/
       // Parameters
       .QUEUELENGTH			(QUEUELENGTH))
   i_commandqueuer (
		    .ClkRs_ix		(ClkRs_x),
		    /*AUTOINST*/
		    // Outputs
		    .OTriggerParams_x	(OTriggerParams_x),
		    .OCmdParams_x	(OCmdParams_x),
		    .QueueStatus_x	(QueueStatus_x),
		    // Inputs
		    .Done_i		(Done_i),
		    .ITriggerParams_x	(ITriggerParams_x),
		    .ICmdParams_x	(ICmdParams_x));
endmodule // wbmc
