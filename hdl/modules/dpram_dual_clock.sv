//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file DPRAM_DUAL_CLOCK.SV
// @brief dual port, dual clock memory as from Quartus templates
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 30 October 2017
// @details
// !! LOOK IN THE COMPILATION LOG - THIS SHOULD GENERATE RAM INSTANCES!! If not,
// then there is something wrong with instantiation of this memory. Always check
// this condition. If it gets instantiated into the registers, then it is not
// correct and will not result in true dual-clock operation.
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;

module dpram_dual_clock
   (
    mem_x ckaref,
    mem_x ckbref
    );

   initial begin
      assert ($bits(ckaref.datain) == $bits(ckbref.datain)) else
	$error("Data bus sizes of both interfaces have to be the same");
      assert ($bits(ckaref.addrbus) == $bits(ckbref.addrbus)) else
	$error("Address bus sizes of both interfaces have to be the same");
   end

   // Declare the RAM variable
   logic [$bits(ckaref.datain)-1:0] 	    ram[2**$bits(ckaref.addrbus)-1:0];

   always_ff @ (posedge ckaref.ClkRs_ix.clk)
     begin
	// Port A
	if (ckaref.write)
	  begin
	     ram[ckaref.addrbus] <= ckaref.datain;
	     ckaref.dataout <= ckaref.datain;
	  end
	else
	  begin
	     ckaref.dataout <= ram[ckaref.addrbus];
	  end
     end

   always_ff @ (posedge ckbref.ClkRs_ix.clk)
     begin
	// Port B
	if (ckbref.write)
	  begin
	     ram[ckbref.addrbus] <= ckbref.datain;
	     ckbref.dataout <= ckbref.datain;
	  end
	else
	  begin
	     ckbref.dataout <= ram[ckbref.addrbus];
	  end
     end


endmodule // dpram_dual_clock
