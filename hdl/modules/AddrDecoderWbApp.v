//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: AddrDecoderWbApp.v
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 15/10/17      3.1          M. Barros Marin    Added GBT BERT WB slave
//     - 06/12/16      3.0          M. Barros Marin    - Added more WB slaves
//                                                     - Cosmetic modifications
//     - 05/11/14      1.0          A. Boccardi        First module definition
//
// Language: Verilog 2005
//
// Targeted device:
//
//     - Vendor:  Agnostic
//     - Model:   Agnostic
//
// Description:
//
//     WishBone Address Decoder & CrossBar of the VFC-HD Application Gateware
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module AddrDecoderWbApp
//========================================  I/O ports  =======================================//
(
    //==== WishBone Interface ====//

    input             Clk_ik,
    input      [20:0] Adr_ib21,
    input             Stb_i,
    output reg [31:0] Dat_oqb32,
    output reg        Ack_oq,

    //==== WishBone Slaves Interface ====//

    input      [31:0] DatCtrlReg_ib32,
    input             AckCtrlReg_i,
    output reg        StbCtrlReg_oq,
    input      [31:0] DatStatReg_ib32,
    input             AckStatReg_i,
    output reg        StbStatReg_oq,
    input      [31:0] DatEthSfpStatReg_ib32,
    input             AckEthSfpStatReg_i,
    output reg        StbEthSfpStatReg_oq,
    input      [31:0] DatGbtRefClkSch_ib32,
    input             AckGbtRefClkSch_i,
    output reg        StbGbtRefClkSch_oq,
    input      [31:0] DatAppSfpGbtUserDataCtrlReg_ib32,
    input             AckAppSfpGbtUserDataCtrlReg_i,
    output reg        StbAppSfpGbtUserDataCtrlReg_oq,
    input      [31:0] DatAppSfpGbtUserDataStatReg_ib32,
    input             AckAppSfpGbtUserDataStatReg_i,
    output reg        StbAppSfpGbtUserDataStatReg_oq,
    input      [31:0] DatAppSfpGbtUserDataBertReg_ib32,
    input             AckAppSfpGbtUserDataBertReg_i,
    output reg        StbAppSfpGbtUserDataBertReg_oq,
    input      [31:0] DatGbtCtrl_ib32,
    input             AckGbtCtrl_i,
    output reg        StbGbtCtrl_oq,
    input      [31:0] DatI2cToWb_ib32,
    input             AckI2cToWb_i,
    output reg        StbI2cToWb_oq

);

//=======================================  Declarations  =====================================//

reg [7:0]  SelectedModule_b8;

localparam c_SelNothing             = 8'd0,
           c_SelCtrlReg             = 8'd1,
           c_SelStatReg             = 8'd2,
           c_SelEthSfpStatReg       = 8'd3,
           c_SelGbtRefClkSch        = 8'd4,
           c_SelAppSfpGbtUsrDatCtrl = 8'd5,
           c_SelAppSfpGbtUsrDatStat = 8'd6,
           c_SelAppSfpGbtUsrDatBert = 8'd7,
           c_SelGbtCtrl             = 8'd8,
           c_SelI2cToWb             = 8'd9;

//=======================================  User Logic  =======================================//

always @*
    casez(Adr_ib21)
        21'b0_0000_0000_0000_0000_00??: SelectedModule_b8 = c_SelCtrlReg;             // FROM 00_0000 TO 00_0003 (WB) == FROM 00_0000 TO 00_000C (VME) <-   4 regs ( 16B)
        21'b0_0000_0000_0000_0000_01??: SelectedModule_b8 = c_SelStatReg;             // FROM 00_0004 TO 00_0007 (WB) == FROM 00_0010 TO 00_001C (VME) <-   4 regs ( 16B)
        21'b0_0000_0000_0000_0000_10??: SelectedModule_b8 = c_SelEthSfpStatReg;       // FROM 00_0008 TO 00_000B (WB) == FROM 00_0020 TO 00_002C (VME) <-   4 regs ( 16B)
        21'b0_0000_0000_0000_0000_11??: SelectedModule_b8 = c_SelGbtRefClkSch;        // FROM 00_000C TO 00_000F (WB) == FROM 00_0030 TO 00_003C (VME) <-   4 regs ( 16B)
        21'b0_0000_0000_0000_0001_00??: SelectedModule_b8 = c_SelAppSfpGbtUsrDatCtrl; // FROM 00_0010 TO 00_0013 (WB) == FROM 00_0040 TO 00_004C (VME) <-   4 regs ( 16B)
        21'b0_0000_0000_0000_0001_01??: SelectedModule_b8 = c_SelAppSfpGbtUsrDatStat; // FROM 00_0014 TO 00_0017 (WB) == FROM 00_0050 TO 00_005C (VME) <-   4 regs ( 16B)
        21'b0_0000_0000_0000_0010_????: SelectedModule_b8 = c_SelAppSfpGbtUsrDatBert; // FROM 00_0020 TO 00_002F (WB) == FROM 00_0080 TO 00_00BC (VME) <-  16 regs ( 64B)
        21'b0_0000_0000_0000_1???_????: SelectedModule_b8 = c_SelGbtCtrl;             // FROM 00_0080 TO 00_00FF (WB) == FROM 00_0200 TO 00_03FC (VME) <- 128 regs (512B)
        21'b0_0000_0001_????_????_????: SelectedModule_b8 = c_SelI2cToWb;             // FROM 00_1000 TO 00_1FFF (WB) == FROM 00_4000 TO 00_7FFC (VME) <-  4k regs (16kB)
        default:                        SelectedModule_b8 = c_SelNothing;
    endcase

always @(posedge Clk_ik) begin
    Ack_oq                                 <= #1  1'b0;
    Dat_oqb32                              <= #1 32'h0;
    StbCtrlReg_oq                          <= #1  1'b0;
    StbStatReg_oq                          <= #1  1'b0;
    StbEthSfpStatReg_oq                    <= #1  1'b0;
    StbGbtRefClkSch_oq                     <= #1  1'b0;
    StbAppSfpGbtUserDataCtrlReg_oq         <= #1  1'b0;
    StbAppSfpGbtUserDataStatReg_oq         <= #1  1'b0;
    StbAppSfpGbtUserDataBertReg_oq         <= #1  1'b0;
    StbGbtCtrl_oq                          <= #1  1'b0;
    StbI2cToWb_oq                          <= #1  1'b0;
    case(SelectedModule_b8)
        c_SelCtrlReg: begin
            StbCtrlReg_oq                  <= #1 Stb_i;
            Dat_oqb32                      <= #1 DatCtrlReg_ib32;
            Ack_oq                         <= #1 AckCtrlReg_i;
        end
        c_SelStatReg: begin
            StbStatReg_oq                  <= #1 Stb_i;
            Dat_oqb32                      <= #1 DatStatReg_ib32;
            Ack_oq                         <= #1 AckStatReg_i;
        end
        c_SelEthSfpStatReg: begin
            StbEthSfpStatReg_oq            <= #1 Stb_i;
            Dat_oqb32                      <= #1 DatEthSfpStatReg_ib32;
            Ack_oq                         <= #1 AckEthSfpStatReg_i;
        end
        c_SelGbtRefClkSch: begin
            StbGbtRefClkSch_oq             <= #1 Stb_i;
            Dat_oqb32                      <= #1 DatGbtRefClkSch_ib32;
            Ack_oq                         <= #1 AckGbtRefClkSch_i;
        end
        c_SelAppSfpGbtUsrDatCtrl: begin
            StbAppSfpGbtUserDataCtrlReg_oq <= #1 Stb_i;
            Dat_oqb32                      <= #1 DatAppSfpGbtUserDataCtrlReg_ib32;
            Ack_oq                         <= #1 AckAppSfpGbtUserDataCtrlReg_i;
        end
        c_SelAppSfpGbtUsrDatStat: begin
            StbAppSfpGbtUserDataStatReg_oq <= #1 Stb_i;
            Dat_oqb32                      <= #1 DatAppSfpGbtUserDataStatReg_ib32;
            Ack_oq                         <= #1 AckAppSfpGbtUserDataStatReg_i;
        end
        c_SelAppSfpGbtUsrDatBert: begin
            StbAppSfpGbtUserDataBertReg_oq <= #1 Stb_i;
            Dat_oqb32                      <= #1 DatAppSfpGbtUserDataBertReg_ib32;
            Ack_oq                         <= #1 AckAppSfpGbtUserDataBertReg_i;
        end
        c_SelGbtCtrl: begin
            StbGbtCtrl_oq                  <= #1 Stb_i;
            Dat_oqb32                      <= #1 DatGbtCtrl_ib32;
            Ack_oq                         <= #1 AckGbtCtrl_i;
        end
        c_SelI2cToWb: begin
            StbI2cToWb_oq                  <= #1 Stb_i;
            Dat_oqb32                      <= #1 DatI2cToWb_ib32;
            Ack_oq                         <= #1 AckI2cToWb_i;
        end
    endcase
end

endmodule
