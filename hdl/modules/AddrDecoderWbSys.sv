//----------------------------------------------------------------------
// Title      : Application Address decoder for the BSRT gate generator
// Project    : Generic
//----------------------------------------------------------------------
// File       : AddrDecoderWbApp.sv
// Author     : A. Boccardi, M. Barros Marin
// Company    : CERN BE-BI-QP
// Created    : 2014-11-05
// Last update: 2017-04-07
// Platform   : FPGA-generic
// Standard   : SystemVerilog
//----------------------------------------------------------------------
// Description:
// WishBone Address Decoder & CrossBar of the VFC-HD System Gateware
//
//----------------------------------------------------------------------

`timescale 1ns/100ps

module AddrDecoderWbSys
(
    //-------- Master Port's Connections --------
    input              Clk_ik,
    input       [21:0] Adr_ib22,
    input              Stb_i,
    output  reg [31:0] Dat_oqb32,
    output  reg        Ack_oq,
    //-------- Slave  Ports' Connections --------
    input       [31:0] DatSysAppIdReg_ib32,
    input              AckSysAppIdReg_i,
    output  reg        StbSysAppIdReg_oq,
    //-----
    input       [31:0] DatSysCtrlReg_ib32,
    input              AckSysCtrlReg_i,
    output  reg        StbSysCtrlReg_oq,
    //-----
    input       [31:0] DatIntManager_ib32,
    input              AckIntManager_i,
    output  reg        StbIntManager_oq,
    //-----
    input       [31:0] DatSpiMaster_ib32,
    input              AckSpiMaster_i,
    output  reg        StbSpiMaster_oq,
    //-----
    input       [31:0] DatUniqueIdReader_ib32,
    input              AckUniqueIdReader_i,
    output  reg        StbUniqueIdReader_oq,
    //-----
    input       [31:0] DatI2cIoExpAndMux_ib32,
    input              AckI2cIoExpAndMux_i,
    output  reg        StbI2cIoExpAndMux_oq,
    //-----
    input       [31:0] DatSfpStatus_ib32,
    input              AckSfpStatus_i,
    output  reg        StbSfpStatus_oq,
    //-----
    input       [31:0] DatVfcConf_ib32,
    input              AckVfcConf_i,
    output  reg        StbVfcConf_oq,
    //-----
    input       [31:0] DatAppSlaveBus_ib32,
    input              AckAppSlaveBus_i,
    output  reg        StbAppSlaveBus_oq
);


enum {
        c_SelNothing,
        c_SelSysAppIdReg,
        c_SelSysCtrlReg,
        c_SelIntManager,
        c_SelSpiMaster,
        c_SelUniqueIdReader,
        c_SelIoExpAndMux,
        c_SelBstDecoder,
        c_SelAppSlaveBus,
        c_SelSfpStatus,
        c_SelVfcConf
     }
    SelectedModule;

//----------------------------------------------------------------------
//----------- Base Addresses' definition
//----------------------------------------------------------------------

always @*
    casez(Adr_ib22)
        22'b000000000000000000????: SelectedModule = c_SelSysAppIdReg;    // FROM 00_0000 TO 00_000F (WB) == FROM 000_0000 TO 000_003C (VME) <-  16 regs ( 64 B)
        22'b0000000000000000010???: SelectedModule = c_SelIntManager;     // FROM 00_0010 TO 00_0017 (WB) == FROM 000_0040 TO 000_005C (VME) <-   8 regs ( 32 B)
        22'b00000000000000000110??: SelectedModule = c_SelUniqueIdReader; // FROM 00_0018 TO 00_001B (WB) == FROM 000_0060 TO 000_006C (VME) <-   4 regs ( 16 B)
        22'b00000000000000000111??: SelectedModule = c_SelSysCtrlReg;     // FROM 00_001C TO 00_001F (WB) == FROM 000_0070 TO 000_007C (VME) <-   4 regs ( 16 B)
        22'b00000000000000001000??: SelectedModule = c_SelIoExpAndMux;    // FROM 00_0020 TO 00_0023 (WB) == FROM 000_0080 TO 000_008C (VME) <-   4 regs ( 16 B)
                                                                          // FROM 00_0024 TO 00_0027 (WB) == FROM 000_0090 TO 000_009C (VME) <-   4 regs ( 16 B)
        22'b0000000000000000101???: SelectedModule = c_SelSfpStatus;      // FROM 00_0028 TO 00_002F (WB) == FROM 000_00A0 TO 000_00BC (VME) <-   8 regs ( 32 B)
        22'b0000000000000000110???: SelectedModule = c_SelSpiMaster;      // FROM 00_0030 TO 00_0037 (WB) == FROM 000_00C0 TO 000_00DC (VME) <-   8 regs ( 32 B)
                                                                          // FROM 00_0038 TO 00_003F (WB) == FROM 000_00E0 TO 000_00FC (VME) <-   8 regs ( 32 B)
                                                                          // FROM 00_0040 TO 00_007F (WB) == FROM 000_0100 TO 000_01FC (VME) <-  64 regs (256 B)
                                                                          // FROM 00_0080 TO 00_00FF (WB) == FROM 000_0200 TO 000_03FC (VME) <- 128 regs (512 B)
                                                                          // FROM 00_0100 TO 00_01FF (WB) == FROM 000_0400 TO 000_07FC (VME) <- 256 regs (  1 kB)
        22'b0000000000001?????????: SelectedModule = c_SelBstDecoder;     // FROM 00_0200 TO 00_03FF (WB) == FROM 000_0800 TO 000_0FFC (VME) <- 512 regs (  2 kB)
                                                                          // FROM 00_0400 TO 00_07FF (WB) == FROM 000_1000 TO 000_1FFC (VME) <-  1k regs (  4 kB)
                                                                          // FROM 00_0800 TO 00_0FFF (WB) == FROM 000_2000 TO 000_3FFC (VME) <-  2k regs (  8 kB)
                                                                          // FROM 00_1000 TO 00_1FFF (WB) == FROM 000_4000 TO 000_7FFC (VME) <-  4k regs ( 16 kB)
                                                                          // FROM 00_2000 TO 00_3FFF (WB) == FROM 000_8000 TO 000_FFFC (VME) <-  8k regs ( 32 kB)
                                                                          // FROM 00_4000 TO 00_7FFF (WB) == FROM 001_0000 TO 001_FFFC (VME) <- 16k regs ( 64 kB)
        22'b0000001???????????????: SelectedModule = c_SelVfcConf;        // FROM 00_8000 TO 00_FFFF (WB) == FROM 002_0000 TO 003_FFFC (VME) <- 32k regs (128 kB)
                                                                          // FROM 01_0000 TO 1F_FFFF (WB) == FROM 004_0000 TO 07F_FFFC (VME) <-   ? regs (  ? B)
        22'b1?????????????????????: SelectedModule = c_SelAppSlaveBus;    // FROM 20_0000 TO 3F_FFFF (WB) == FROM 080_0000 TO 0FF_FFFC (VME) <-  2M regs (  8 MB)
        default:                    SelectedModule = c_SelNothing;
    endcase

//----------------------------------------------------------------------
//----------- Signals' Mux
//----------------------------------------------------------------------

always @(posedge Clk_ik) begin
    Ack_oq                       <= #1  1'b0;
    Dat_oqb32                    <= #1 32'h0;
    StbSysAppIdReg_oq            <= #1  1'b0;
    StbSysCtrlReg_oq             <= #1  1'b0;
    StbIntManager_oq             <= #1  1'b0;
    StbSpiMaster_oq              <= #1  1'b0;
    StbUniqueIdReader_oq         <= #1  1'b0;
    StbI2cIoExpAndMux_oq         <= #1  1'b0;
    StbSfpStatus_oq              <= #1  1'b0;
    StbVfcConf_oq                <= #1  1'b0;
    StbAppSlaveBus_oq            <= #1  1'b0;
    case (SelectedModule)
        c_SelSysAppIdReg: begin
            StbSysAppIdReg_oq    <= #1 Stb_i;
            Dat_oqb32            <= #1 DatSysAppIdReg_ib32;
            Ack_oq               <= #1 AckSysAppIdReg_i;
        end
        c_SelSysCtrlReg: begin
            StbSysCtrlReg_oq     <= #1 Stb_i;
            Dat_oqb32            <= #1 DatSysCtrlReg_ib32;
            Ack_oq               <= #1 AckSysCtrlReg_i;
        end
        c_SelIntManager: begin
            StbIntManager_oq     <= #1 Stb_i;
            Dat_oqb32            <= #1 DatIntManager_ib32;
            Ack_oq               <= #1 AckIntManager_i;
        end
         c_SelSpiMaster: begin
            StbSpiMaster_oq      <= #1 Stb_i;
            Dat_oqb32            <= #1 DatSpiMaster_ib32;
            Ack_oq               <= #1 AckSpiMaster_i;
        end
        c_SelUniqueIdReader: begin
            StbUniqueIdReader_oq <= #1 Stb_i;
            Dat_oqb32            <= #1 DatUniqueIdReader_ib32;
            Ack_oq               <= #1 AckUniqueIdReader_i;
        end
        c_SelIoExpAndMux: begin
            StbI2cIoExpAndMux_oq <= #1 Stb_i;
            Dat_oqb32            <= #1 DatI2cIoExpAndMux_ib32;
            Ack_oq               <= #1 AckI2cIoExpAndMux_i;
        end
        c_SelSfpStatus: begin
            StbSfpStatus_oq      <= #1 Stb_i;
            Dat_oqb32            <= #1 DatSfpStatus_ib32;
            Ack_oq               <= #1 AckSfpStatus_i;
        end
        c_SelVfcConf: begin
            StbVfcConf_oq        <= #1 Stb_i;
            Dat_oqb32            <= #1 DatVfcConf_ib32;
            Ack_oq               <= #1 AckVfcConf_i;
        end
        c_SelAppSlaveBus: begin
            StbAppSlaveBus_oq    <= #1 Stb_i;
            Dat_oqb32            <= #1 DatAppSlaveBus_ib32;
            Ack_oq               <= #1 AckAppSlaveBus_i;
        end
    endcase
end

endmodule
