//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) December 2017 CERN

//-----------------------------------------------------------------------------
// @file DUMMY_WISHBONE_MODULE_WITH_REGISTERS.SV
// @brief Emulates motor behavior
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 07 December 2017
// @details Is not used in the compilation, but serves as a template
// to generate wishbone iface with few registers.
//
//
// NOTE THAT ALTHOUGH THIS EMULATES A REAL MOTOR, IT IS PART OF THE DESIGN AS IT
// IS USED DURING THE FESA/SOFTWARE DEBUGGING. HAVING THIS LAYER WE AVOID
// NECESSITY OF HAVING FULLY FUNCTIONAL SYSTEM FOR THE SOFTWARE DEVELOPMENT
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module dummy_wishbone_module_with_registers
  #(
    // sets up how many registers the wishbone interface is supposed to
    // implement
    parameter g_NumberOfRegisters = 8,
    // length of the data bus
    parameter g_DataWidth = 32
    )
   (
    // clock/reset combo
    input ckrs_t ClkRs_ix,
    // wishbone interface - comes from BI cores
    t_WbInterface Wb_iot
    );

   // initialize here the registers direction. Set '1' at position of register,
   // which is supposed to send the data from this entity to the master. Set it
   // to zero when master writes down something into this entity
   localparam logic [g_NumberOfRegisters-1:0] c_DirectionMiso_b = '0;

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic [g_DataWidth-1:0] Register_imb [0:g_NumberOfRegisters-1];// To i_WbRegField of WbRegField.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic [g_DataWidth-1:0] Register_omb [0:g_NumberOfRegisters-1];// From i_WbRegField of WbRegField.v
   // End of automatics

   // instantiates register field
   WbRegField
     #(        .g_NumberOfRegisters(g_NumberOfRegisters),
               .g_DataWidth(g_DataWidth),
               .g_DirectionMiso_b(c_DirectionMiso_b),
	       .g_DefaultValue_mb('{g_NumberOfRegisters{(g_DataWidth)'(0)}}),
	       .g_AutoClrMask_mb('{(g_DataWidth)'(0),// 0
				   (g_DataWidth)'(0),// 1
				   (g_DataWidth)'(0),// 2
				   (g_DataWidth)'(0),// 3
				   (g_DataWidth)'(0),// 4
				   (g_DataWidth)'(0),// 5
				   (g_DataWidth)'(0),// 6
				   (g_DataWidth)'(0) // 7
				   })
	       )
   i_WbRegField (/*AUTOINST*/
		 // Interfaces
		 .Wb_iot		(Wb_iot),
		 // Outputs
		 .Register_omb		(Register_omb/*[g_DataWidth-1:0].[0:g_NumberOfRegisters-1]*/),
		 // Inputs
		 .Register_imb		(Register_imb/*[g_DataWidth-1:0].[0:g_NumberOfRegisters-1]*/));




endmodule // dummy_wishbone_module_with_registers
