
// generated ROM irq resolver, see irq_arbiter.py
// connect address[5:4] to free running counter
// connect address[3:0] to next irq source
module irq_rom (
input  wire [5:0] address , // Address input
output reg  [1:0] data      // Data output
);

always_comb
begin
  unique case (address)
	0 : data = 2'h0;
	1 : data = 2'h0;
	2 : data = 2'h1;
	3 : data = 2'h0;
	4 : data = 2'h2;
	5 : data = 2'h0;
	6 : data = 2'h1;
	7 : data = 2'h0;
	8 : data = 2'h3;
	9 : data = 2'h0;
	10 : data = 2'h1;
	11 : data = 2'h0;
	12 : data = 2'h2;
	13 : data = 2'h0;
	14 : data = 2'h1;
	15 : data = 2'h0;
	16 : data = 2'h1;
	17 : data = 2'h0;
	18 : data = 2'h1;
	19 : data = 2'h1;
	20 : data = 2'h2;
	21 : data = 2'h2;
	22 : data = 2'h1;
	23 : data = 2'h1;
	24 : data = 2'h3;
	25 : data = 2'h3;
	26 : data = 2'h1;
	27 : data = 2'h1;
	28 : data = 2'h2;
	29 : data = 2'h2;
	30 : data = 2'h1;
	31 : data = 2'h1;
	32 : data = 2'h2;
	33 : data = 2'h0;
	34 : data = 2'h1;
	35 : data = 2'h0;
	36 : data = 2'h2;
	37 : data = 2'h2;
	38 : data = 2'h2;
	39 : data = 2'h2;
	40 : data = 2'h3;
	41 : data = 2'h3;
	42 : data = 2'h3;
	43 : data = 2'h3;
	44 : data = 2'h2;
	45 : data = 2'h2;
	46 : data = 2'h2;
	47 : data = 2'h2;
	48 : data = 2'h3;
	49 : data = 2'h0;
	50 : data = 2'h1;
	51 : data = 2'h0;
	52 : data = 2'h2;
	53 : data = 2'h0;
	54 : data = 2'h1;
	55 : data = 2'h0;
	56 : data = 2'h3;
	57 : data = 2'h3;
	58 : data = 2'h3;
	59 : data = 2'h3;
	60 : data = 2'h3;
	61 : data = 2'h3;
	62 : data = 2'h3;
	63 : data = 2'h3;

  endcase
end
endmodule
