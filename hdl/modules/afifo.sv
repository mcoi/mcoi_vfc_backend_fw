/*
 Dual-clock asynchronous FIFO design and testbench in SystemVerilog
 Based on Cliff Cumming's Simulation and Synthesis Techniques for Asynchronous FIFO Design
 http://www.sunburst-design.com/papers/CummingsSNUG2002SJ_FIFO1.pdf

 Copyright (C) 2015 Jason Yu (http://www.verilogpro.com)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 10/31/17 09:24:24 - Personal comment:
 this source was downloaded from

 http://www.verilogpro.com/asynchronous-fifo-design/ and updated just to
 conform BI naming conventions for the top module. Its associated vunit
 testbench is in ../tests. I have to say, I'm not really in agreement of using
 automatic port expansions of systemverilog. It is indeed a very nice feature
 for testbenches, as EMACS can automatically declare all the signals. It is
 however a real pain when used in the main module to instantiate. If those
 modules would be in separate files (sync_r2w ...), there would not be any
 simple possibility to find for example into which of all these entities waddr
 signal goes - the only one which remains is to grep for those signal names in
 the project (TAGS might do). And this is for me the major reason to not to use
 implicit port connections (at least in this stage) in the synthesizable code.

 OK - second thought - EMACS CAN EXPAND THOSE INTO CORRECT named
 instantiations as if they were autos
 */

import CKRSPkg::*;

module afifo
  #(
    parameter g_FifoWidth = 4
    )
   (
    // interface with data/enable/clock for write domain
    data_x wdata_ix,
    // interface with data/enable/clock for read domain
    // ENABLE SIGNAL of rxdata_ox IS USED TO FETCH NEXT DATA FROM THE FIFO. THIS
    // IS UNUSUAL! HENCE THIS INTERFACE OUTPUTS DATA BUT INPUTS ENABLE!
    data_x rdata_ox,
    // fifo full signal synced into WRITE domain
    output logic wFifoFull_o,
    // fifo empty signal synced into READ domain
    output logic rFifoEmpty_o
    );

   logic [g_FifoWidth-1:0] waddr, raddr;
   logic [g_FifoWidth:0]   wptr, rptr, wq2_rptr, rq2_wptr;

   initial begin
      assert($bits(wdata_ix.data) == $bits(rdata_ox.data)) else
	$error("Bus width of input and output interfaces must be equal");
   end


   sync_r2w #(g_FifoWidth) sync_r2w (.*);
   sync_w2r #(g_FifoWidth) sync_w2r (.*);
   fifomem #($bits(wdata_ix.data), g_FifoWidth) fifomem (.*);
   rptr_empty #(g_FifoWidth) rptr_empty (.*);
   wptr_full #(g_FifoWidth) wptr_full (.*);

endmodule


//
// FIFO memory
//
module fifomem
  #(
    parameter g_DataWidth = 8, // Memory data word width
    parameter g_FifoWidth = 4  // Number of mem address bits
    )
   (
    data_x wdata_ix,
    data_x rdata_ox,
    input logic 		   wFifoFull_o,
    input logic [g_FifoWidth-1:0] 	   waddr, raddr
    );

   // RTL Verilog memory model
   localparam DEPTH = 1<<g_FifoWidth;

   logic [g_DataWidth-1:0] 		   mem [0:DEPTH-1];

   assign rdata_ox.data = mem[raddr];

   always_ff @(posedge wdata_ix.ClkRs_ix.clk)
     if (wdata_ix.enable && !wFifoFull_o)
       mem[waddr] <= wdata_ix.data;

endmodule


//
// Read pointer to write clock synchronizer
//
module sync_r2w
  #(
    parameter g_FifoWidth = 4
    )
   (
    data_x wdata_ix,
    input logic [g_FifoWidth:0]  rptr,
    output logic [g_FifoWidth:0] wq2_rptr
    );

   logic [g_FifoWidth:0] 	 wq1_rptr = '0;

   always_ff @(posedge wdata_ix.ClkRs_ix.clk or posedge wdata_ix.ClkRs_ix.reset)
     if (wdata_ix.ClkRs_ix.reset) {wq2_rptr,wq1_rptr} <= 0;
     else {wq2_rptr,wq1_rptr} <= {wq1_rptr,rptr};

endmodule


//
// Write pointer to read clock synchronizer
//
module sync_w2r
  #(
    parameter g_FifoWidth = 4
    )
   (
    data_x rdata_ox,
    input logic [g_FifoWidth:0]  wptr,
    output logic [g_FifoWidth:0] rq2_wptr
    );

   logic [g_FifoWidth:0] 	 rq1_wptr;

   always_ff @(posedge rdata_ox.ClkRs_ix.clk or posedge rdata_ox.ClkRs_ix.reset)
     if (rdata_ox.ClkRs_ix.reset)
       {rq2_wptr,rq1_wptr} <= 0;
     else
       {rq2_wptr,rq1_wptr} <= {rq1_wptr,wptr};

endmodule


//
// Read pointer and empty generation
//
module rptr_empty
  #(
    parameter g_FifoWidth = 4
    )
   (
    data_x rdata_ox,
    input logic [g_FifoWidth :0]   rq2_wptr,
    output logic 		   rFifoEmpty_o,
    output logic [g_FifoWidth-1:0] raddr,
    output logic [g_FifoWidth :0]  rptr
    );

   logic [g_FifoWidth:0] 	   rbin = '0;
   logic [g_FifoWidth:0] 	   rgraynext, rbinnext;

   //-------------------
   // GRAYSTYLE2 pointer
   //-------------------
   always_ff @(posedge rdata_ox.ClkRs_ix.clk or posedge rdata_ox.ClkRs_ix.reset)
     if (rdata_ox.ClkRs_ix.reset)
       {rbin, rptr} <= '0;
     else
       {rbin, rptr} <= {rbinnext, rgraynext};

   // Memory read-address pointer (okay to use binary to address memory)
   assign raddr = rbin[g_FifoWidth-1:0];
   assign rbinnext = rbin + (rdata_ox.enable & ~rFifoEmpty_o);
   assign rgraynext = (rbinnext>>1) ^ rbinnext;

   //---------------------------------------------------------------
   // FIFO empty when the next rptr == synchronized wptr or on reset
   //---------------------------------------------------------------
   logic 			   rFifoEmpty_o_val;

   assign rFifoEmpty_o_val = (rgraynext == rq2_wptr);

   always_ff @(posedge rdata_ox.ClkRs_ix.clk or posedge rdata_ox.ClkRs_ix.reset)
     if (rdata_ox.ClkRs_ix.reset)
       rFifoEmpty_o <= 1'b1;
     else
       rFifoEmpty_o <= rFifoEmpty_o_val;

endmodule


//
// Write pointer and full generation
//
module wptr_full
  #(
    parameter g_FifoWidth = 4
    )
   (
    data_x wdata_ix,
    input logic [g_FifoWidth :0]   wq2_rptr,
    output logic 		   wFifoFull_o,
    output logic [g_FifoWidth-1:0] waddr,
    output logic [g_FifoWidth :0]  wptr
    );

   logic [g_FifoWidth:0] 	   wbin = '0;
   logic [g_FifoWidth:0] 	   wgraynext, wbinnext;

   // GRAYSTYLE2 pointer
   always_ff @(posedge wdata_ix.ClkRs_ix.clk or posedge wdata_ix.ClkRs_ix.reset)
     if (wdata_ix.ClkRs_ix.reset)
       {wbin, wptr} <= '0;
     else
       {wbin, wptr} <= {wbinnext, wgraynext};

   // Memory write-address pointer (okay to use binary to address memory)
   assign waddr = wbin[g_FifoWidth-1:0];
   assign wbinnext = wbin + (wdata_ix.enable & ~wFifoFull_o);
   assign wgraynext = (wbinnext>>1) ^ wbinnext;

   //------------------------------------------------------------------
   // Simplified version of the three necessary full-tests:
   // assign wFifoFull_o_val=((wgnext[g_FifoWidth] !=wq2_rptr[g_FifoWidth] ) &&
   // (wgnext[g_FifoWidth-1] !=wq2_rptr[g_FifoWidth-1]) &&
   // (wgnext[g_FifoWidth-2:0]==wq2_rptr[g_FifoWidth-2:0]));
   //------------------------------------------------------------------
   logic 			   wFifoFull_o_val;

   assign wFifoFull_o_val = (wgraynext=={~wq2_rptr[g_FifoWidth:g_FifoWidth-1], wq2_rptr[g_FifoWidth-2:0]});

   always_ff @(posedge wdata_ix.ClkRs_ix.clk or posedge wdata_ix.ClkRs_ix.reset)
     if (wdata_ix.ClkRs_ix.reset)
       wFifoFull_o <= 1'b0;
     else
       wFifoFull_o <= wFifoFull_o_val;

endmodule
