//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) June 2017 CERN

//-----------------------------------------------------------------------------
// @file SWITCHMANAGER.SV
// @brief implements debouncing, negation and selection of switches
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 28 June 2017
// @details
// entity implements behaviour of simple switching matrix. Each motor
// has 2 switches attached physically, which can be configured
// 'however' to produce 'logical' switches. Logical switches then
// control behaviour of the motor motion. The raw switches signals
// (those coming directly from hardware without any modification
// except of debouncing) are coming to Switch_b2 vector. Those
// switches signals _can be inverted_ by setting Polarity to
// '1'. Those signals are then logically merged using SWSelect_b2
// vector such, that none, either, or one of the input raw switches is
// considered to be output logical switch. Due to this selections the
// SWSelect_ib2 has to be a vector of 2 bits such, that '1' written
// into a particular bit announces 'use this switch _as well_ to
// produce logical switch. The SWSelect_o then represents logical
// switch used further in the design to control motors' motion.
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

module switchmanager
   (
   // signal which contains the selected switch output value
    output logic      SWSelect,
    // input switches array
    input logic [1:0] Switch_b2,
    // set '1' to revert the polarity of the selected switch
    input logic       Polarity,
    // give number corresponding to the selected switch. I.e. when set
    // to zero the SWSelect will output signal from Switch_b2[0]
    input logic [1:0]       SelectedInputSwitches_b2);

   logic [1:0] 	     SwitchPolarity_rx;

   // eventually invert the polarity of the input signals to fit the
   // 'H' logic ('H' is when switch triggers)
   assign SwitchPolarity_rx = {{Polarity, Polarity}} ^ Switch_b2;
   // output selected switch combination depends not only on polarity,
   // but as well on vector masking: both of the switches (or none)
   // can contribute to the logical switch selection
   assign SWSelect = (SwitchPolarity_rx[1] &
		      SelectedInputSwitches_b2[1]) |
		     (SelectedInputSwitches_b2[0] &
		      SwitchPolarity_rx[0]);


endmodule // switchmanager
