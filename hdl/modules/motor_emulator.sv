//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) December 2017 CERN

//-----------------------------------------------------------------------------
// @file MOTOR_EMULATOR.SV
// @brief Emulates motor behavior
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 07 December 2017
// @details
// Each motor is driven using simple vector of input and output signals (through
// the transmitter). This entity emulates behaviour of single motor. Intercepts
// signals sent to it (as motor direction, pulse etcetc), and reacts
// appropriately to it, exporting fail signals and steps. The module emulates
// just a single motor, which is described by maximum counts it can
// support into two directions , and
// two extremity switches, which are turned on/off depending of in which
// position the motor counter just operates. It does not emulate switches
// bouncing, but is able to emulate overheat and motor failure. This is done by
// setting randomly a counter when the error should happen. Random number is
// generated using barrel register with feedback, and user can specify
// bits slice, which is used to generate the OH and fail signal.
//
// The module has to be setup with few parameters, which are setup/readback
// through wishbone interface. Following registes are implemented:
//
// 0 (rw) number of ticks the motor implements. When this number is set, the
// motor 'actual' position is set to ZERO, and motor can exert into
// positive and negative direction maximum this amount of steps
// without triggering StepPFail_i. Hence for example if value in this
// register is 100, at the moment the value is written into this
// register, the automatic counter in register (7) is cleared, and one
// is allowed to do 100 pulses with direction 0 to hit fail. The same
// is for other direction, hence -100 to reach other direction fail.
//
// 1 (rw) position of the actual motor counter which identifies point at which
// the IN-switch should act, value anywhere between register -(0) to
// (0) is valid, but this value has to be LOWER THAN VALUE in the
// register (2). SW_IN switch.
//
// 2 (rw) position of the actual motor counter which identifies point at which
// the OUT-switch should act. Value in this register must be larger than the
// value in register (1). When motor exceed this value, the information about
// switch tripping will be generated. SW_OUT switch
//
// 3 (rw) configuration:
// --> bit 0: when set to '1', the IN-switch in inactive state is set to '1',
// when set to '0', the IN-switch in inactive state is set to zero (hence when
// bound is reached, it sets to '1')
// --> bit 1: same as bit (0), but applies for OUT-switch
// --> bit 2: reset overheat and motor fail errors
// --> bit 3: when set to '1' the fail counters are automatically reloaded and
// next overheat or motorfailure happens automatically
// --> bit 30: when set to '1', it emulates single error caused by overheat
// --> bit 31: when set to '1', it emulates single motor fail error
//
// 4/5 (rw) random generator masks. This entity implements random generator, which
// each cycle generates a new 32bit number. The randomly generated
// number is masked using this mask such, that  (rnd# and mask), we
// always keep lower 24bits as they are, and 31 to 24 bits masked. So
// the totally generated number used as error counter is xx = (rnd#
// and mask), counter = xx or 0x800000. This is to assure that IRQs do
// not get fired in less than roughly 67ms, otherwise we would
// completely killed the card (IRQ flooding). Hence following is
// applied: (rnd# and mask) or 0x800000, and this value serves as a
// base for failure counter
//
// 6 (r) status register
// --> bit 0: OUT switch value exported
// --> bit 1: IN switch value exported
// --> bit 2: overheat signal current output(generated digitally when enabled in
// --> bit 3: stepper fail (this is when we force it to go beyond
// extremities). This is cleared out only by setting bit 2 of (3)
// configuration)
// --> bit 30: OH fail test in progress
// --> bit 31: Motor fail test in progress
//
// 7 (r) actual motor position
//
//
// MOTOR SWITCHES AND COUNTING:
// if DIRECTION = 1, then the counter of actual position is
// _DECREASED_ until it hits -AMPLITUDE set in register (1), During
// decrease the switch IN gets triggered.
// if DIRECTION = 0, then the counter of actual position is
// _INCREASED_ until it hits +AMPLITUDE set in register (2)
//
// NOTE THAT ALTHOUGH THIS EMULATES A REAL MOTOR, IT IS PART OF THE DESIGN AS IT
// IS USED DURING THE FESA/SOFTWARE DEBUGGING. HAVING THIS LAYER WE AVOID
// NECESSITY OF HAVING FULLY FUNCTIONAL SYSTEM FOR THE SOFTWARE DEVELOPMENT
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;
import MCPkg::*;

module motor_emulator
  #(
    // sets up how many registers the wishbone interface is supposed to
    // implement
    parameter g_NumberOfRegisters = 8,
    // length of the data bus
    parameter g_DataWidth = 32,
    // or-mark value. For real system this should be set up such, that
    // in no way the frequency of IRQs during fail generation can
    // exceed 50ms rate. With 125MHz clock the value of 0x800000 does
    // roughly 67ms. LOWER THIS VALUE ONLY IN THE TESTBENCH
    parameter g_OrMask = 'h800_0000
    )
   (
    // clock/reset combo
    input  ckrs_t ClkRs_ix,
	   // wishbone interface - comes from BI cores
	   t_WbInterface Wb_iot,
	   // random number data generator, use master must use data enable
	   // to cast new number
	   data_x Random_iot,
    // motor input and output interface, note reversed ports direction
    output mcinput_t mc_ox,
    input  mcoutput_t mc_ix
    );

   // initialize here the registers direction. Set '1' at position of register,
   // which is supposed to send the data from this entity to the master. Set it
   // to zero when master writes down something into this entity
   localparam logic [g_NumberOfRegisters-1:0] c_DirectionMiso_b = 8'b1100_0000;
   localparam DEFAULTMOTORSTEPS = 65536;
   localparam BITS = g_DataWidth;



   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   logic 				      data_i;			// To i_getedge of getedge.v
   // End of automatics
   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   logic 				      data_o;			// From i_getedge of getedge.v
   logic 				      falling_o;		// From i_getedge of getedge.v
   logic 				      rising_o;		// From i_getedge of getedge.v
   // End of automatics
   logic [g_DataWidth-1:0] 		      Register_omb [0:g_NumberOfRegisters-1];// From i_WbRegField of WbRegField.v
   logic [g_DataWidth-1:0] 		      Register_imb [0:g_NumberOfRegisters-1];// From i_WbRegField of WbRegField.v

   // instantiates register field
   WbRegField
     #(        .g_NumberOfRegisters(g_NumberOfRegisters),
               .g_DataWidth(g_DataWidth),
               .g_DirectionMiso_b(c_DirectionMiso_b),
	       // let's setup some meaningful values
	       .g_DefaultValue_mb('{
				    // motor total count
				    (g_DataWidth)'(DEFAULTMOTORSTEPS),
				    // position < 100 -> IN switch triggers
				    (g_DataWidth)'(100),
				    // position > 65000 -> OUT switch triggers
				    (g_DataWidth)'(65000),
				    // all OFF
				    (g_DataWidth)'(0),
				    // 125MHz clock, this generates trigger anywhere betwee 1.14second to 3.35seconds
				    (g_DataWidth)'(402653184),
				    // dtto mask for motor fail
				    (g_DataWidth)'(402653184),
				    (g_DataWidth)'(0),
				    (g_DataWidth)'(0)}),
	       .g_AutoClrMask_mb('{(g_DataWidth)'(0),// 0
				   (g_DataWidth)'(0),// 1
				   (g_DataWidth)'(0),// 2
				   (g_DataWidth)'('hC000_0004),// 3 config
				   (g_DataWidth)'(0),// 4
				   (g_DataWidth)'(0),// 5
				   (g_DataWidth)'(0),// 6
				   (g_DataWidth)'(0) // 7
				   })
	       )
   i_WbRegField (/*AUTOINST*/
		 // Interfaces
		 .Wb_iot		(Wb_iot),
		 // Outputs
		 .Register_omb		(Register_omb/*[g_DataWidth-1:0].[0:g_NumberOfRegisters-1]*/),
		 // Inputs
		 .Register_imb		(Register_imb/*[g_DataWidth-1:0].[0:g_NumberOfRegisters-1]*/));


   // make assignments so we can identify registers by their proper names
   logic [g_DataWidth-1:0] 		      MotorTicks_b32,
					      ConfigReg_b32,
					      StatusReg_b32;
   logic signed [g_DataWidth-1:0] 	      MotorPosition_ib32,
	 MotorINSwitchPosition_ib32,
	 MotorOUTSwitchPosition_ib32;



   assign MotorTicks_b32 = Register_omb[0],
     MotorINSwitchPosition_ib32 = Register_omb[1],
     MotorOUTSwitchPosition_ib32 = Register_omb[2],
     ConfigReg_b32 = Register_omb[3];

   logic [g_DataWidth-1:0] 		      OHFailMasks_b [1:0];
   assign OHFailMasks_b[0] = Register_omb[4],
     OHFailMasks_b[1] = Register_omb[5];

   assign Register_imb[6] = StatusReg_b32,
     Register_imb[7] = MotorPosition_ib32;


   logic [g_DataWidth-1:0] 		      MotorTicksCopy_b32;
   always_ff @(posedge ClkRs_ix.clk) MotorTicksCopy_b32 <= MotorTicks_b32;

   logic 				      MotorTicksChanged_m;
   assign MotorTicksChanged_m = (| (MotorTicks_b32 ^
				    MotorTicksCopy_b32));

   logic 				      StepOutP_i;

   get_edge
     i_getedge (
		// Outputs
		.rising_o			(StepOutP_i),
		.falling_o		(),
		.data_o			(),
		// Inputs
		.ClkRs_ix			(ClkRs_ix),
		.data_i			(mc_ix.StepOutP_o));

   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset) begin
      if (ClkRs_ix.reset)
	MotorPosition_ib32 <= '0;
      else begin
	 if (MotorTicksChanged_m || ConfigReg_b32[2])
	   MotorPosition_ib32 <= '0;
	 else if (mc_ix.StepDIR_o &&
		  !mc_ox.StepPFail_i &&
		  !mc_ix.StepDeactivate_o &&
		  StepOutP_i)
	   MotorPosition_ib32 <= MotorPosition_ib32 - 1;
	 else if (!mc_ix.StepDIR_o &&
		  !mc_ox.StepPFail_i &&
		  !mc_ix.StepDeactivate_o &&
		  StepOutP_i)
	   MotorPosition_ib32 <= MotorPosition_ib32 + 1;
      end
   end

   assert property (@(posedge ClkRs_ix.clk)
		    disable iff (ClkRs_ix.reset)
		    MotorTicksChanged_m |=> (MotorPosition_ib32 ==
					     0)) else
     $error("Motor actual position counter does not get loaded properly");


   // generate random numbers for masks, in order to save on chip
   // logic we use one generator and feed values into arrays by
   // multiplexing. Any time we have new random number, we cast it
   // into different source. This assures RND generation separate of
   // OH and fail
   logic RndSelector = 0;
   always_ff @(posedge ClkRs_ix.clk)
     if (Random_iot.enable)
       RndSelector <= ~RndSelector;

   logic [g_DataWidth-1:0] RndStream [1:0];
   always_ff @(posedge ClkRs_ix.clk) begin
      RndStream[RndSelector] <= Random_iot.data;
   end

   // so each time we turn on bit 30 and 31, the data get casted to
   // the two appropriate counters, which at their end generate one of
   // the failures
   genvar bitctr;
   logic [g_DataWidth-1:0] FailCounts_b [1:0];
   logic [1:0] 		   FailSignal_b;
   logic [g_DataWidth-1:0] StatusRegLate_b32;

   always_ff @(posedge ClkRs_ix.clk)
     StatusRegLate_b32 <= StatusReg_b32;

   generate
      begin: twocc
	 for(bitctr = 0; bitctr < 2; bitctr++) begin: myclok
	    // generate counter which loads on bit change
	    always_ff @(posedge ClkRs_ix.clk or
			posedge ClkRs_ix.reset) begin
	       if (ClkRs_ix.reset) begin
		  FailCounts_b[bitctr] <= '0;
		  StatusReg_b32[bitctr+30] <= 0;
	       end
	       else begin
		  // if not currently running, generate random and
		  // fill counter
		  if (ConfigReg_b32[bitctr+30] &&
		      ~StatusReg_b32[bitctr+30]) begin
		     FailCounts_b[bitctr] <= (g_DataWidth)'((RndStream[bitctr] &
							     OHFailMasks_b[bitctr]) |
							    g_OrMask);
		     StatusReg_b32[bitctr+30] <= 1;
		  end
		  // if already running, shorten the number of cycles
		  // to 1 to force immediate stop
		  else if (ConfigReg_b32[bitctr+30] &&
			   StatusReg_b32[bitctr+30]) begin
		     FailCounts_b[bitctr] <= 1;
		     StatusReg_b32[bitctr+30] <= 1;
		  end
		  else if (FailCounts_b[bitctr] != 0) begin
		     FailCounts_b[bitctr] <= FailCounts_b[bitctr] - 1;
		     StatusReg_b32[bitctr+30] <= 1;
		  end
		  else
		    StatusReg_b32[bitctr+30] <= 0;
	       end // else: !if(ClkRs_ix.reset)
	    end // always_ff @ (posedge ClkRs_ix.clk or...

	    // fail signal generation
	    always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset) begin
	       if (ClkRs_ix.reset)
		 FailSignal_b[bitctr] <= 0;
	       else begin
		  // edge detection
		  if ((StatusReg_b32[bitctr+30] ^
		       StatusRegLate_b32[bitctr+30]) &
		      ~StatusReg_b32[bitctr+30])
		    FailSignal_b[bitctr] <= 1;
		  else if (ConfigReg_b32[2])
		    FailSignal_b[bitctr] <= 0;
	       end // else: !if(ClkRs_ix.reset)
	    end // always_ff @ (posedge ClkRs_ix.clk or posedge
	    // ClkRs_ix.reset)
	 end // for (bitctr = 0; bitctr < 2; bitctr++)
      end // block: twocc
   endgenerate

   // OH is just assignment
   assign mc_ox.OH_i = FailSignal_b[0];
   assign StatusReg_b32[2] = FailSignal_b[0];


   // StepPFail_i is more than that because we're looking as well on
   // motor actual position
   always_comb begin
      if ((MotorPosition_ib32 == MotorTicks_b32) ||
	  (MotorPosition_ib32 == -MotorTicks_b32)) begin
	 mc_ox.StepPFail_i = 1;
	 StatusReg_b32[3] = 1;
      end
      else begin
	 mc_ox.StepPFail_i = FailSignal_b[1];
	 StatusReg_b32[3] = FailSignal_b[1];
      end
   end // always_comb

   // handling switches
   always_comb begin
      // SW_OUT is LOCATED IN RawSwitches_b2[0]
      if (MotorPosition_ib32 >= MotorOUTSwitchPosition_ib32)
	mc_ox.RawSwitches_b2[0] = 1 ^ ConfigReg_b32[0];
      else
	mc_ox.RawSwitches_b2[0] = 0 ^ ConfigReg_b32[0];

      if (MotorPosition_ib32 <= MotorINSwitchPosition_ib32)
	mc_ox.RawSwitches_b2[1] = 1 ^ ConfigReg_b32[1];
      else
	mc_ox.RawSwitches_b2[1] = 0 ^ ConfigReg_b32[1];
   end // always_comb

   assign StatusReg_b32[0] = mc_ox.RawSwitches_b2[0];
   assign StatusReg_b32[1] = mc_ox.RawSwitches_b2[1];


   assert property (@(posedge ClkRs_ix.clk)
		    disable iff (ClkRs_ix.reset)
		    $fell(StatusReg_b32[31]) |=> FailSignal_b[1])
     else $error(
		 "At the end of error injection cycle Fail signal must rise");
   assert property (@(posedge ClkRs_ix.clk)
		    disable iff (ClkRs_ix.reset)
		    $fell(StatusReg_b32[30]) |=> FailSignal_b[0])
     else $error(
		 "At the end of error injection cycle OH signal must rise");


   // both counters have to have g_OrMask bit setup immediatelly no
   // matter what.
   genvar bitx;

   generate
      for(bitx=30; bitx<32; bitx++) begin : assertbehaviour
	assert property (@(posedge ClkRs_ix.clk)
   			 disable iff (ClkRs_ix.reset)
   			 ConfigReg_b32[bitx] && ~StatusReg_b32[bitx] |=> (FailCounts_b[bitx-30] & g_OrMask)) else
	  $error("Counter value for bit %d fails masking", bitx);

	 // these trips from the config should be only single cycle long
	 assert property (@(posedge ClkRs_ix.clk)
			  disable iff (ClkRs_ix.reset)
			  ConfigReg_b32[bitx] |=> ~ConfigReg_b32[bitx]) else
	   $error("Start operation of bit %d should be single clock cycle long", bitx);
      end
   endgenerate


   // reset failures should be 1cc long
   assert property (@(posedge ClkRs_ix.clk)
		    disable iff (ClkRs_ix.reset)
		    ConfigReg_b32[2] |=> ~ConfigReg_b32[2]) else
     $error("Reset failure should be 1cc long");




endmodule // motor_emulator
