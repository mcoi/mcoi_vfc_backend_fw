//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) October 2017 CERN

//-----------------------------------------------------------------------------
// @file STEPPINGCONTROLLER.SV
// @brief Implements functionality of the motor action
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 20 October 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import MCPkg::*;
import CKRSPkg::*;
import constants::*;

`include "t_MCIface.sv"

module steppingcontroller (
			   // Interfaces
			   mc_x mc,
			   // Outputs
			   output      stepperstatus_t StepperStatus_x,

			   // Inputs
			   input       ckrs_t ClkRs_ix,
			   input       command_t ICmdParams_x,
			   input       motorcommand_t ITriggerParams_x,
			   input logic DeviceInterlocked,
			   input       cntparam_t CntParam_ix,
			   input       triggers_t triggers,
			   input logic [31:0] reload_ib32,
			   output logic [1:0]     ConfiguredSwitches_b2
			   );

   timeunit 1ns;
   timeprecision 100ps;

   // Declares sub-step counter. This counter needs to overflow in
   // order to update CounterStepOut_c32 by a single tick. Counter_c20
   // value is incremented by LowSpeed_b36/HighSpeed_b36 every time SlowDown_b32
   // counter overflows
   logic [25:0] 				  Counter_c20;
   // step counter
   int unsigned 				  CounterStepOut_c32;
   logic [10:0] 				  Counter_Pulse = '0;
   // ramping down position
   int unsigned 				  RampingDown_r32;
   // current speed
   logic [35:0] 				  Speed_c36;
   logic 					  LaunchMove = 1'b0;
   //Registers to be latched to avoid any change during any changes
   command_t CmdParamLatch;
   int unsigned 				  CountPos_c32; // current position
   logic 					  BusyStep;
   // '1' when this entity requires motor to make 1 step
   logic 					  StepOut;
   logic 					  StepBOOST;
   logic 					  Done;
   logic 					  Interrupt;
   abortstatus_t 	      AbortStatus, AbortStatus_delay;
   abortstatus_t 	      AbortStatus_d;
   logic [31:0] 				  SlowDownCount_b32;
   logic 					  DisableMove;
   // '1' when actual motor position already actuated the extremity
   // switch _and_ exceeded its motion by
   logic 					  StopLimit;
   logic 					  SlowDownCountEn;
   logic 					  SlowDownEn;
   // set to '1' during FSM cycles, which mean that the motor is in
   // motion. This allows to update speed counters
   logic 					  UpdateStepCount;
   // those two are set to '1' in ACCEL and DECEL respectively states
   // in order to change the speed with every cycle
   logic 					  IncSpeed, DecSpeed;

   // fancy way how to define one-hot state machine. We want one hot
   // because we export the state into the output status and we want
   // each state to represent a bit in this register
   localparam LAST_STATE = 6;
   enum 					  {IDLE = 0,
						   START_ACCEL = 1,
						   ACCEL = 2,
						   START_CRUISE_HS = 3,
						   CRUISE_HS = 4,
						   DECEL = 5,
						   CRUISE_LS = 6} StateIndex_t;

   // we want one hot state machine. If something gets quirky, we can catch it here
   assert property (@(posedge ClkRs_ix.clk) $onehot(State));
   assert property (@(posedge ClkRs_ix.clk) $onehot(State_n));
   // we have to initialize state to '1' (corresponds to IDLE) as the properties
   // above will blow up if reset does not come immediatelly in zero 'delta'
   logic [LAST_STATE:0] 			  State = 1,
						  State_n;
   // sampled direction at each stepoutp
   logic 					  LatchedDirection;
   logic 					  StepOutP = '0;



   // assign local signals to status
   assign StepperStatus_x.Done = Done;
   assign StepperStatus_x.Interrupt = Interrupt;
   assign StepperStatus_x.State = State;
   assign StepperStatus_x.PosCounter_b32 = CountPos_c32;
   assign StepperStatus_x.StepsLeft_b32 = CmdParamLatch.StepNumber_b32 - CounterStepOut_c32;
   assign StepperStatus_x.AbortStatus = AbortStatus;
   assign StepperStatus_x.Busy = BusyStep;


   // point of assertion: we need to assure that DIRECITON does not
   // change during StepOutP_o, motor has to get clear indication of
   // which direction to go!. Note the negation in comparison, that's
   // because physical stepDir pin is inverse of logical one (that's
   // how our systems are wired)
   property signal_steady;
      logic sampled_direction;
      @(posedge ClkRs_ix.clk)
	disable iff (ClkRs_ix.reset)
	  ($rose(StepOutP),
	   sampled_direction=CmdParamLatch.Direction) |=>
			     (mc.outvec.StepDIR_o == sampled_direction
			      throughout StepOutP);
   endproperty // nosamenumbers
   direction_changes: assert property (signal_steady) else
     $error("DIRECTION MUST NOT CHANGE WHEN STEPOUT");

   // we use LATCHED version of direction. Latching is done just
   // before pulse goes out, which assures that the signal stays on a
   // single level throughout the step. The DIRECTION PIN IN INVERSED
   // TO BE compatible with old MIDI systems!!
   assign mc.outvec.StepDIR_o = LatchedDirection;

   // any time busy goes up, stepenab must go down, but no vice versa
   assert property (@(posedge ClkRs_ix.clk) BusyStep |-> !mc.outvec.StepDeactivate_o);

   // both triggers are expected to generate a single pulse
   assert property(@(posedge ClkRs_ix.clk) triggers.TrigMoveInt_i |=> !triggers.TrigMoveInt_i)
     else $error("triggers.TrigMoveInt_i is expected as single clock pulse");
   assert property(@(posedge ClkRs_ix.clk) triggers.TrigMoveExt_i |=> !triggers.TrigMoveExt_i)
     else $error("triggers.TrigMoveExt_i is expected as single clock pulse");

   // set to '1' to enable powering of the motor stage. This signal
   // has to be '1' when busystep in progress (motor moves)
   assign mc.outvec.StepDeactivate_o = !((CmdParamLatch.powerconfig.standby ||
				   BusyStep) &&
				  CmdParamLatch.powerconfig.enable_move);

   // debounce stepfail as any little glitch on this could cause the
   // motor to stop in the middle
   logic    StepFailDebounced;
   // we set debouncer to 16 clocks, but additional 2 clocks are
   // required to debounce the input signal and additional 2 to
   // process the debouncing. Hence totally 20 cycles, at 40MHz this
   // makes 500ns required signal stability minimum.
   debouncer
     #(
       // Parameters
       .g_CounterWidth			(4),
       .g_SynchDepth			(2))
   i_debounce
     (
      // Outputs
      .Signal_oq			(StepFailDebounced),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .Signal_ia			(mc.invec.StepPFail_i));


   // signal, which disables any motor movement. True if we hit the
   // limit switch, if requested by VME, if fails, if overheats _or_
   // if the motor move is disable by the power configuration register
   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset) begin
      if (ClkRs_ix.reset) begin
	 DisableMove <= 1;
      end else begin
	 DisableMove <= (StopLimit ||
			 StepFailDebounced ||
			 ITriggerParams_x.StopMove ||
			 mc.invec.OH_i) || !CmdParamLatch.powerconfig.enable_move;
      end
   end

   // disables start of move if external switches announce, that motor
   // is in a given position, and we want to move that direction even further
   logic 			       DisableMoveStart,
				       IsNExtremity,
				       IsPExtremity;

   // these two say whether swiches are reached in given direction:
   // DIRECTION=1 and SWITCH[1] => out
   // DIRECTION=0 and SWITCH[0] => IN
   // CONSIDER AS WELL WHETHER UPPER ANNOUNCES THAT DEVICE IS
   // INTERLOCKED. in this case motor movement has to be immediatelly
   // stopped.
   assign IsNExtremity = (CmdParamLatch.Direction &&
			  ConfiguredSwitches_b2[1]) | DeviceInterlocked;
   assign IsPExtremity = (!CmdParamLatch.Direction &&
			  ConfiguredSwitches_b2[0]) | DeviceInterlocked;

   assign DisableMoveStart =  (IsNExtremity || IsPExtremity ||
			       StepFailDebounced || ITriggerParams_x.StopMove ||
			       mc.invec.OH_i) ||
			      !CmdParamLatch.powerconfig.enable_move;

   logic 			       LaunchMove_delay;
   logic 			       AttemptToStart, AttemptToStart_delay;

   // this signal says that 'we try to start motor' by whatever means.
   // IT IS OF UTMOST IMPORTANCE THAT THIS SIGNAL IS ONLY 1CC long!

   assign AttemptToStart = (
			    // if internal enabled, internal happens and
			    // internal trigger is enabled by instantiator
			    (CmdParamLatch.globaltrigger.internal
			     && triggers.TrigMoveInt_i
			     && triggers.TriggerEnable_i) ||
			    // if external enabled, external happens and
			    // external trigger is enabled by instatiator
			    (CmdParamLatch.globaltrigger.external
			     && triggers.TrigMoveExt_i
			     && triggers.TriggerEnable_i) ||
			    // or if triggered by Wishbone command
			    ITriggerParams_x.StartMove);

   always_ff @(posedge ClkRs_ix.clk)
     AttemptToStart_delay <= AttemptToStart;

   assert property (@(posedge ClkRs_ix.clk) AttemptToStart |=>
			    !AttemptToStart) else
			    $error("AttemptToStart has to be single\
 clock long");

   // set to '1' when a particular command is allowed to run. This is
   // done by triggering, and depending of trigger configuration
   // triggering can come from multiple sources
   always @(posedge ClkRs_ix.clk) begin
      LaunchMove <= AttemptToStart &&
		    // but _ALWAYS_ when move is enabled in powerconfig
		    // and not disabled by some external condition
		    // (switches, fail, overheat...)
		    !DisableMoveStart;
      LaunchMove_delay <= LaunchMove;

   end // always @ (posedge ClkRs_ix.clk)

   assign mc.outvec.StepBOOST_o = StepBOOST;

   always_ff @(posedge ClkRs_ix.clk, posedge ClkRs_ix.reset) begin
      if (ClkRs_ix.reset) begin
	 State <= '0;
	 State[IDLE] <= 1'b1;
      end
      else
	State <= State_n;
   end

   always_comb begin
      // default state is 'none' and we're going to fill in inside
      // this process
      State_n = '0;
      unique case (1'b1)
	State[IDLE]: begin
	   if (DisableMove)
	     State_n[IDLE] = 1'b1;
	   else if (LaunchMove_delay)
	     // if allowed to move, FSM jumps into
	     // acceleration. Allowing to move depends on quite few
	     // parameters, most significant is, that we are not
	     // allowed to move when heading the direction of the
	     // extremity switch, which already actuated
	     State_n[START_ACCEL] = 1'b1;
	   else
	     State_n[IDLE] = 1'b1;
	end // case: State[IDLE]

	State[START_ACCEL]:
	  if (DisableMove)
	    State_n[IDLE] = 1'b1;
	  else
	    State_n[ACCEL] = 1'b1;

	State[ACCEL]: begin
	   if (DisableMove)
	     State_n[IDLE] = 1'b1;
	   // here's deal: if we have already busted half of the
	   // steps, and we are still in acceleration phase, we need
	   // to switch to decceleration as there's no way how to
	   // achieve highspeed within given steps
           else if (CounterStepOut_c32 >=
		    (CmdParamLatch.StepNumber_b32>>1) )
	     State_n[DECEL] = 1'b1;
	   // we have enough of steps available, so the FSM goes into
	   // cruising mode when the high speed is exceeded
           else if (speedShiftDown(Speed_c36) >=
		    CmdParamLatch.HighSpeed_b36)
	     State_n[START_CRUISE_HS] = 1'b1;
	   else
	     State_n[ACCEL] = 1'b1;

	end // case: State[ACCEL]

	State[START_CRUISE_HS]:
	  if (DisableMove)
	    State_n[IDLE] = 1'b1;
	  else
	    State_n[CRUISE_HS] = 1'b1;

	State[CRUISE_HS]: begin
	   if (DisableMove)
	     State_n[IDLE] = 1'b1;
	   // there is no cruising if decel + trail + current step
	   // takes less than total number of steps. We have to
	   // decelerate immediatelly, but to be clear, that it will
	   // not finish on low speed due to low amount of steps
           else if (CounterStepOut_c32 + RampingDown_r32 >=
		    CmdParamLatch.StepNumber_b32)
	     State_n[DECEL] = 1'b1;
	   // and if by chance we have already exceeded number of
	   // steps, we go to idle immediatelly. This however means,
	   // that the guy who programmed the command most likely
	   // did not know what he is doing!
           else if (CounterStepOut_c32 >=
		    CmdParamLatch.StepNumber_b32)
	     State_n[IDLE] = 1'b1;
	   else
	     State_n[CRUISE_HS] = 1'b1;

	end // case: State[CRUISE_HS]

	State[DECEL]: begin
	   if (DisableMove)
	     State_n[IDLE] = 1'b1;
           else if (speedShiftDown(Speed_c36) <=
		    CmdParamLatch.LowSpeed_b36)
	     State_n[CRUISE_LS] = 1'b1;
           else if (CounterStepOut_c32 >=
		    CmdParamLatch.StepNumber_b32)
	     State_n[IDLE] = 1'b1;
	   else
	     State_n[DECEL] = 1'b1;

	end // case: State[DECEL]

	State[CRUISE_LS]: begin
	   if (DisableMove)
	     State_n[IDLE] = 1'b1;
           else if (CounterStepOut_c32 >=
		    CmdParamLatch.StepNumber_b32)
	     State_n[IDLE] = 1'b1;
	   else
	     State_n[CRUISE_LS] = 1'b1;
	end
      endcase // unique case (1'b1)
   end


   assign AbortStatus_d = {CmdParamLatch.Direction,
			   StopLimit,
			   ITriggerParams_x.StopMove,
			   !CmdParamLatch.powerconfig.enable_move,
			   IsNExtremity,
			   IsPExtremity,
			   mc.invec.OH_i ,
			   StepFailDebounced};

   always_ff @(posedge ClkRs_ix.clk) begin
      SlowDownCountEn = (State[START_ACCEL]) ? '0 : '1;
      IncSpeed <= (State[ACCEL]) ? '1 : '0;
      DecSpeed <= (State[DECEL]) ? '1 : '0;

      UpdateStepCount <= (State[ACCEL] ||
			  State[CRUISE_HS] ||
			  State[DECEL] ||
			  State[CRUISE_LS]) ? '1 : '0;

      BusyStep <= (State[IDLE]) ?
		  (Counter_Pulse != 0) : '1;

      StepBOOST <= (State[ACCEL] ||
		    State[DECEL]) ?
		   CmdParamLatch.powerconfig.boost :
		   '0;
   end

   always_ff @(posedge ClkRs_ix.clk) begin
      // 1cc delay because of re-timing
      AbortStatus_delay <= AbortStatus_d;

      case (1'b1)
	State[IDLE]:   begin
	   CounterStepOut_c32 <= 18'b0; // actual steps performed
	   Counter_c20  <= 0; // substep counter
	   Speed_c36 <= speedShiftUp(CmdParamLatch.LowSpeed_b36);	 // initialze speed
	   CmdParamLatch <= ICmdParams_x;// latch command
	end // case: IDLE

	State[START_CRUISE_HS]:  begin
	   // at this point we know, that acceleration totally took
	   // CounterStepOut_c32 steps. Deceleration will take the
	   // same amount, but in addition we need to add trail,
	   // which comes from the command
           RampingDown_r32 <= CounterStepOut_c32 + CmdParamLatch.Trail_b32;
        end

	State[CRUISE_HS]: begin
	   // cruising speed is always high speed, but as the
	   // original Speed_c36 can be higher due to all the
	   // 'increments', we set it up here to the desired parameter
	   Speed_c36 <= speedShiftUp(CmdParamLatch.HighSpeed_b36); // set speed
        end // case: CRUISE_HS
      endcase

      // new speed calculation:
      if (SlowDownEn)
	if (IncSpeed)
	  Speed_c36 <= Speed_c36 + CmdParamLatch.AccDeccRate_b19;
	else if (DecSpeed)
	  Speed_c36 <= Speed_c36 - CmdParamLatch.AccDeccRate_b19;

      // following updates the sub-counter any time the slow-down
      // timer runs away and we're in a motion. Counter_c20 adds to
      // its value a value of LowSpeed_b36 or HighSpeed_b36 (depending of
      // whether we are HS coarsing or not)
      if (UpdateStepCount==1 && SlowDownEn==1) begin
	 Counter_c20 <= Counter_c20 + speedShiftDown(Speed_c36);
      end

      // if subcounter overflows (Counter_c20 is cummulation of
      // Low/HighSpeed_b36 counts each time slowdown counter overflows,
      // the value of 40000 is the maximum allowable unsigned which
      // allows non-overflow due to rounding?)
      if (Counter_c20 >= 20'h40000) begin
	 Counter_c20 <= 0;
	 // increase real step counter
	 CounterStepOut_c32 <= CounterStepOut_c32 + 1'b1;
      end

   end // always_ff @ (posedge ClkRs_ix.clk)

   // abort status handling:
   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset)
     if (ClkRs_ix.reset)
       AbortStatus <= '0;
     else if (State_n != State && State_n[IDLE] ||
	      (AttemptToStart_delay && AbortStatus.StopMove))
       // latch status to output. This is done either at the end of
       // movement of the state machine, or even before start of the
       // state machine after the forced stopmove:
	AbortStatus <= AbortStatus_delay;


   logic [5:0] Counter_LimitStep;

   // registered status signals:
   always_ff @(posedge ClkRs_ix.clk) begin
      // stoplimit identifies if motor has to be stopped due to hittig
      // limit switches:
      StopLimit <= ((IsPExtremity || IsNExtremity) &&
		    (Counter_LimitStep == 0)) ? '1 : '0;
      // done at the end of cycle:
      Done <= (State_n != State && State_n[IDLE]) ? '1 : '0;
      // eventually IRQ - this is generated at the end of each desired
      // cycle (user might specify queue of x commands and e.g. only
      // last of them will trigger IRQ. or say in 2/3 of the queue the
      // IRQ is triggered so one can refill the queue)
      Interrupt <= (
		    // generate interrupt when state machine finished
		    // the operation:
		    (State_n != State &&
		    State_n[IDLE] &&
		    CmdParamLatch.GenInterrupt) ||
		    // or generate IRQ when SMA cannot run because of
		    // one of the fail conditions, but there's is an
		    // attempt to make the motor running. In this case
		    // no operation is performed, IRQ is generated and
		    // status is copied into the abort stats:
		   (AttemptToStart_delay &&
		    DisableMove &&
		    CmdParamLatch.GenInterrupt) ||
		    // or generate IRQ when stopmove was issued last
		    // time _and_ the move is still disabled because
		    // e.g. we're in the limit switch position:
		    (AttemptToStart_delay &&
		     AbortStatus.StopMove &&
		     DisableMoveStart &&
		     CmdParamLatch.GenInterrupt)) ? '1 : '0;
      // generated step:
      StepOut <= (Counter_c20 >= 20'h40000) ? '1 : '0;
   end

   // Count number of steps after limit switch. Consider which
   // direction we're heading and choose appropriate register
   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset) begin
      if (ClkRs_ix.reset) begin
	 Counter_LimitStep <= 0;
      end else if (DeviceInterlocked)
	// no steps allowed after
	Counter_LimitStep <= '0;
      else if (!IsNExtremity &&
	       !IsPExtremity &&
	       BusyStep &&
	       !LatchedDirection)
	// LatchedDirection = 0 => positive direction, load with this
	// value _while not being in extremity position and
	// moving_. Point of this 'and moving' is, that when the
	// gateware was reseted and the motor sticks in the extremity
	// position, the counter has to be reloaded only when
	// moved. Because after reset Counter_LimitStep is cleared and
	// is used to generate IRQ at the end of request even if motor
	// did not move due to switching condition. Not having this
	// would mean that we 'recombine' new pattern of switches, and
	// by default they get enabled, hence counter loads default
	// value and will prevent IRQ to be generated at the very
	// first time motion is engaged into the switch...
	 Counter_LimitStep <= CntParam_ix.StepsAfterLimitSwitchP_b6;
      else if (!IsNExtremity &&
	       !IsPExtremity &&
	       BusyStep &&
	       LatchedDirection)
	// LatchedDirection = 1 => negative direction, load with yet
	// another value, again _while not being in extremity position
	// and moving_
	 Counter_LimitStep <= CntParam_ix.StepsAfterLimitSwitchN_b6;
      else if (Counter_LimitStep != 0 && StepOut)
	// and once in extremity position, count down until zero
	   Counter_LimitStep <= Counter_LimitStep - 6'(1);
   end

   // assert that stepoutp during its positive phase generates exactly
   // STEPPER_PULSE_WIDTH clocks long. Problem we have with this is,
   // that signal driving stepout is a counter, which can due to timer
   // aligment and overflow bust 1 cc. Hence we need to create
   // uncertainty here on count. HENCE THIS ASSERTION MIGHT FAIL WHEN
   // IMPROPER VALUES SPECIFIED
   // synthesis translate_off
   property StepOutP_pulse_width;
      int count;
      @(posedge ClkRs_ix.clk)
	($rose(StepOutP),count=STEPPER_PULSE_WIDTH) |->
	  (StepOutP,count--)[*] ##1 (~StepOutP && count==0);
   endproperty // StepOutP_pulse_width
   assert property (StepOutP_pulse_width);
   // synthesis translate_on

   // any time we stop moving we have 3 cycles to pulse done, but only
   // if move is actually done (it can be suppressed by
   // enablemove). this is indicated by 'busy'
   assert property (@(posedge ClkRs_ix.clk) (BusyStep && $rose(ITriggerParams_x.StopMove)) |->
		    (##[1:3] StepperStatus_x.Done));
   // overheat is the same thing, but only if SM is running!
   assert property (@(posedge ClkRs_ix.clk) ($rose(mc.invec.OH_i) & State[IDLE]!=1) |->
		    (##[1:3] StepperStatus_x.Done));

   logic  CycleStarts;

   // catches start of the cycle, used to latch initial value of direction:
   get_edge
   i_get_edge
     (
      // Outputs
      .rising_o				(CycleStarts),
      .falling_o			(),
      .data_o				(),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .data_i				(BusyStep));

   // we latch the direction before every pulse out to assure that it
   // does not move
   always_ff @(posedge ClkRs_ix.clk)
     StepOutP <= (Counter_Pulse != 0) ? '1 : '0;

   assign mc.outvec.StepOutP_o = !StepOutP;

   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset) begin
      if (ClkRs_ix.reset) begin
	 LatchedDirection <= '0;
      end else begin
	 // we latch the direction at the beginning of the entire
	 // queue cycle, and then each stepout.
	 if (StepOut || CycleStarts)
	   LatchedDirection <= CmdParamLatch.Direction;
      end
   end


   // Generate a step pulse, this one really drives the motor. StepOut
   // is the signal, which starts the pulse generation
   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset) begin
      if (ClkRs_ix.reset)
	Counter_Pulse <= 0;
      else if (StepOut)
	Counter_Pulse <= (11)'(STEPPER_PULSE_WIDTH);
      else if (Counter_Pulse != 0)
	 Counter_Pulse <= Counter_Pulse - 11'(1);
   end

   // serves as clock enable to slowdown counting
   assign SlowDownEn = (~|SlowDownCount_b32)? '1 : '0;

   // Generate slowdown enable - this is general slow down, which
   // slows all the motor motion operations by factor of
   // CntParam_ix.SlowDown_b32
   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset) begin
      if (ClkRs_ix.reset) begin
	 SlowDownCount_b32 <= '0;
      end else begin
	 // let's handle specific cases:
	 // zero and one feed with 'turn off'
	 if (CntParam_ix.SlowDown_b32 == 32'b0 ||
	     CntParam_ix.SlowDown_b32 == 32'b1)
	   SlowDownCount_b32 <= '0;
	 // if zero during enable, feed new value
	 else if (!SlowDownCountEn || ~|SlowDownCount_b32)
	   SlowDownCount_b32 <= CntParam_ix.SlowDown_b32 - 32'(1);
	 // counting down
	 else
	   SlowDownCount_b32 <= SlowDownCount_b32 - 32'(1);
      end
   end


   // Update counter position. Note that whenever
   // ITriggerParams_x.ResetCounterPos is triggered, the value of
   // 'reload' is reloaded into the register. After POR this is zero
   always @(posedge ClkRs_ix.clk) begin
      if (ClkRs_ix.reset)
	CountPos_c32 <= '0;
      else if (ITriggerParams_x.ResetCounterPos)
	CountPos_c32 <= reload_ib32;
      else if (CmdParamLatch.Direction && StepOut)
	CountPos_c32 <= CountPos_c32 - 32'b1;
      else if (!CmdParamLatch.Direction && StepOut)
	CountPos_c32 <= CountPos_c32 + 32'b1;
   end

   // register to store the states in the output enum must have enough
   // depth to accomodate the one-hot encoding of the machine state
   always @(posedge ClkRs_ix.clk) assert (LAST_STATE < 9) else
     $error("IF THERE ARE MORE THAN 8 STATES, YOU HAVE TO MAKE THE REGISTER IN StepperStatus_x.State  LONGER AND MAKE APPROPRIATE VME MAP CHANGES!!!");

   // this instance takes care about switches configuration
   // module implements switching matrix for 2 switches. Number of switches is
   // configurable, but this application uses only 2 switches. Registers allow
   // implementation of up to 16 input switches, which can be casted to 2 output
   // extremity switches. Number of output switches is configurable as well, but
   // currently they would not produce any output. The module allows taking any
   // input switch, connect it to the output switch and eventually invert its
   // polarity. All switches inputs are debounced so it takes some time to
   // propagate switching event from 'actually happening in analogue world' to
   // 'digital value'. ConfiguredSwitches_b2 vector returns switches
   // configuration such, that it corresponds to configured switching
   // logic. This is the signal driving steppingcontroller
   switchcontroller
     i_switchcontroller (
			 .RawSwitches_b2	(mc.invec.RawSwitches_b2),
			 .CntParam_ix(CntParam_ix),
			 /*AUTOINST*/
			 // Outputs
			 .ConfiguredSwitches_b2	(ConfiguredSwitches_b2[1:0]),
			 // Inputs
			 .ClkRs_ix		(ClkRs_ix));



endmodule
