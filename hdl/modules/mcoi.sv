
import CKRSPkg::*;
import MCPkg::*;
import constants::*;


module VfcHdApplication
  #(  parameter g_ApplicationVersion_b8 = 8'h01,
      g_ApplicationReleaseDay_b8   = 8'h02,
      g_ApplicationReleaseMonth_b8 = 8'h03,
      g_ApplicationReleaseYear_b8  = 8'h04,
      // length of the command queue
      parameter   QUEUELENGTH  = 32,
      // optical link frame size is defined in number of bytes
      parameter g_FrameSize = 80,
      // data bus width - NEVER! change this
      parameter g_DataWidth = 32,
      // determine how many bits the wishbone address bus is using
      parameter g_AddressWidth = 25,
      // constant declaring starting address of the first motor
      // control link. 4 links are instantiated as of this
      // address. Note that this is _logical address_, hence
      // identifies INDEX of the register in the address space (a
      // 0,1,2,3,4,5 ... as opposed to physical address, which is
      // always multiple of 4)
      parameter g_fourlinksAddress = 32'h00002000,
      // this is the address of the global register space (currently 4
      // registers), which setup the values for _all_ links and
      // instances of the  motor control
      parameter g_globalRegistersAddress = 32'h00004000,
      // identifies motor emulator blocks
      parameter g_motorEmulatorBlockAddress = 32'h00006000,
      // Up to 352 bits -> 44 ASCII Characters (44 Bytes)
      parameter      g_AppInfo_b352 = "VFC-MCOI-DRB",
      // product number
      parameter g_productID = 32'hab010100)
   (
    // Application SFP MGT Lanes:
    input [ 4:1]   AppSfpRx_ib4, // Differential signal
    output [ 4:1]  AppSfpTx_ob4, // Differential signal
    // TestIo MMCX Connectors:
    inout 	   TestIo1_io,
    inout 	   TestIo2_io,
    // FMC HPC Connector:
    output 	   VfmcEnable_oen,
    output 	   FmcPgC2m_o,
    // DIP Switch (SW1):
    input [ 8:1]   DipSw_ib8,
    // GPIO LEMO Connectors:
    inout [ 4:1]   GpIo_iob4,
    //==== System-Application interface ====\\

    // Reference Clock for the WishBone Interface and GBTs
    input 	   ckrs_t ClkRs_ix,
    // Reference frame clock for GBTs:
    input 	   ckrs_t ClkRsGBT40MHz_ix,
    // Application & Release IDs:
    output [ 7:0]  AppVersion_ob8, AppReleaseDay_ob8, AppReleaseMonth_ob8, AppReleaseYear_ob8,
    output [351:0] AppInfo_ob352,
    // WishBone Bus Interface:
    input 	   WbMasterCyc_i,
    input 	   WbMasterStb_i,
    // The actual width of the address bus is set to 21 for the
    // moment: top bits stuck to 0
    input [ 24:0]  WbMasterAdr_ib25,
    input 	   WbMasterWr_i,
    input [ 31:0]  WbMasterDat_ib32,
    output [ 31:0] WbMasterDat_ob32,
    output 	   WbMasterAck_o,
    // fp leds
    output [ 8:1]  Led_ob8,
    // Interrupt Request:
    output [ 23:0] InterruptRequest_ob24,
    // GPIO Direction Control:
    output 	   GpIo1DirOut_o, // Output -> 1'b1 | Input -> 1'b0
    input 	   GpIo1DirOutStatus_i,
    output 	   GpIo2DirOut_o, // Output -> 1'b1 | Input -> 1'b0
    input 	   GpIo2DirOutStatus_i,
    output 	   GpIo34DirOut_o, // Output -> 1'b1 | Input -> 1'b0
    input 	   GpIo34DirOutStatus_i,
    output 	   GpIo1EnTerm_o, // Enable -> 1'b1 | Disable -> 1'b0
    input 	   GpIo1EnTermStatus_i,
    output 	   GpIo2EnTerm_o, // Enable -> 1'b1 | Disable -> 1'b0
    input 	   GpIo2EnTermStatus_i,
    output 	   GpIo3EnTerm_o, // Enable -> 1'b1 | Disable -> 1'b0
    input 	   GpIo3EnTermStatus_i,
    output 	   GpIo4EnTerm_o, // Enable -> 1'b1 | Disable -> 1'b0
    input 	   GpIo4EnTermStatus_i,
    // AppSfp1 Control:
    input 	   AppSfp1Present_iq,
    input 	   AppSfp1TxFault_iq,
    input 	   AppSfp1Los_iq,
    output 	   AppSfp1TxDisable_o,
    output 	   AppSfp1RateSelect_o,
    input 	   StatusAppSfp1TxDisable_iq,
    input 	   StatusAppSfp1RateSelect_iq,
    // AppSfp2 Control:
    input 	   AppSfp2Present_iq,
    input 	   AppSfp2TxFault_iq,
    input 	   AppSfp2Los_iq,
    output 	   AppSfp2TxDisable_o,
    output 	   AppSfp2RateSelect_o,
    input 	   StatusAppSfp2TxDisable_iq,
    input 	   StatusAppSfp2RateSelect_iq,
    // AppSfp3 Control:
    input 	   AppSfp3Present_iq,
    input 	   AppSfp3TxFault_iq,
    input 	   AppSfp3Los_iq,
    output 	   AppSfp3TxDisable_o,
    output 	   AppSfp3RateSelect_o,
    input 	   StatusAppSfp3TxDisable_iq,
    input 	   StatusAppSfp3RateSelect_iq,
    // AppSfp4 Control:
    input 	   AppSfp4Present_iq,
    input 	   AppSfp4TxFault_iq,
    input 	   AppSfp4Los_iq,
    output 	   AppSfp4TxDisable_o,
    output 	   AppSfp4RateSelect_o,
    input 	   StatusAppSfp4TxDisable_iq,
    input 	   StatusAppSfp4RateSelect_iq,
    //==== WishBone interface for the I2C slaves on the Muxes ====\\
    output 	   I2cWbCyc_o,
    output 	   I2cWbStb_o,
    output 	   I2cWbWe_o,
    output [11:0]  I2cWbAdr_ob12,
    output [ 7:0]  I2cWbDat_ob8,
    input [ 7:0]   I2cWbDat_ib8,
    input 	   I2cWbAck_i
    );

   timeunit 1ns;
   timeprecision 100ps;
   //=======================================  Declarations  =====================================\\
   localparam WISHBONE_SLAVES = 4;
   localparam WB_DEFAULTAPP = 0;
   localparam WB_FOURLINKS = 1;
   localparam WB_GLOBALREGS = 2;
   localparam WB_MOTOR_EMULATOR = 3;
   localparam g_Latency = 2;


   genvar 	   i, j;
   // LEDs - stores PWM amplitudes
   logic [7:0] Led_8b4 [4:1];
   logic [NUMBER_OF_GBT_LINKS:1] IrqTrigger_b4, IrqMKO_b4, LinkIRQ_b4;
   logic 			 HwTrig;


   // WishBone Address Decoder:
   wire 	   WbStbGbtRefClkSch, WbAckGbtRefClkSch;
   wire [ 31:0]    WbDatGbtRefClkSch_b32;
   wire 	   WbStbGbtCtrl, WbAckGbtCtrl;
   wire [ 31:0]    WbDatGbtCtrl_b32;
   wire 	   WbMasterI2cStb, WbMasterI2cAck;
   wire [  7:0]    WbMasterI2cDat_b8;
   // WhiteRabbit:
   wire 	   a_GbtUserClk_k;
   //--
   logic [  1:0] 		  TxDataScIcAppSfpGbtUserData_4b2 [NUMBER_OF_GBT_LINKS:1];
   logic [  1:0] 		  TxDataScEcAppSfpGbtUserData_4b2 [NUMBER_OF_GBT_LINKS:1];
   logic [ 79:0] 		  TxDataAppSfpGbtUserData_4b80    [NUMBER_OF_GBT_LINKS:1];
   wire [  1:0] 		  RxDataScIcAppSfpGbtUserData_4b2 [NUMBER_OF_GBT_LINKS:1];
   wire [  1:0] 		  RxDataScEcAppSfpGbtUserData_4b2 [NUMBER_OF_GBT_LINKS:1];
   wire [ 79:0] 		  RxDataAppSfpGbtUserData_4b80    [NUMBER_OF_GBT_LINKS:1];
   wire [ 79:0] 		  RxDataAppSfpGbtUserDataVME_4b80    [NUMBER_OF_GBT_LINKS:1];
   wire [  NUMBER_OF_GBT_LINKS:1] GbtRxReadyAppSfpGbtUserData_b4;
   logic [  NUMBER_OF_GBT_LINKS:1] TxIsDataSelAppSfpGbtUserData_b4;
   wire [  NUMBER_OF_GBT_LINKS:1] RxIsDataFlagAppSfpGbtUserData_b4;
   wire 			  TxMatchFlagAppSfpGbtUserData;
   reg [  NUMBER_OF_GBT_LINKS:1]  RxMatchFlagAppSfpGbtUserData_b4;
   //--
   wire [  1:0] 		  TestPatterSelAppSfpGbtUserData_b2;
   wire [ 83:0] 		  TxDataAppSfpGbtUserData_b84;
   wire 			  ErrInjAppSfpGbtUserData;
   reg 				  ErrInjAppSfpGbtUserData_q;
   reg [ 83:0] 			  TxDataErrInjAppSfpGbtUserData_qb84;
   //--
   wire [  NUMBER_OF_GBT_LINKS:1] ScEcCheckEnAppSfpGbtUserData_b4;
   wire [  NUMBER_OF_GBT_LINKS:1] TogglingDataCheckSyncAppSfpGbtUserData_b4;
   wire [  NUMBER_OF_GBT_LINKS:1] ResetGbtRxRdyLostFlagAppSfpGbtUserData_b4;
   wire [  NUMBER_OF_GBT_LINKS:1] ResetGbtRxDataErrorsAppSfpGbtUserData_b4;
   wire [  NUMBER_OF_GBT_LINKS:1] GbtRxRdyLostFlagAppSfpGbtUserData_b4;
   wire [  NUMBER_OF_GBT_LINKS:1] RxDataErrFlagAppSfpGbtUserData_b4;
   wire [ 63:0] 		  RxDataErrorCntAppSfpGbtUserData_64b4 [NUMBER_OF_GBT_LINKS:1];
   wire [ 63:0] 		  RxDataFrameCntAppSfpGbtUserData_64b4 [NUMBER_OF_GBT_LINKS:1];
   //--
   wire [ 31:0] 		  AppSfpGbtUserDataCtrlRegs_b32;
   reg [ 31:0] 			  AppSfpGbtUserDataCtrlRegs_qb32;
   //--
   wire [ 31:0] 		  AppSfpGbtUserDataStatRegs_4b32 [3:0];
   reg [  NUMBER_OF_GBT_LINKS:1]  GbtRxReadyAppSfpGbtUserData_qb4;
   reg [  NUMBER_OF_GBT_LINKS:1]  RxIsDataFlagAppSfpGbtUserData_qb4;
   reg [  NUMBER_OF_GBT_LINKS:1]  GbtRxRdyLostFlagAppSfpGbtUserData_qb4;
   reg [  NUMBER_OF_GBT_LINKS:1]  RxDataErrFlagAppSfpGbtUserData_qb4;
   //--
   wire [ 31:0] 		  AppSfpGbtUserDataBertRegs_16b32 [15:0];
   // Signals Forwarding:
   reg 				  TestIo1;
   reg 				  TestIo2;
   // turn on GBT pattern in VME clock domain
   logic [NUMBER_OF_GBT_LINKS:1]  TurnOnGBTPattern_b4;
   // turn on GBT pattern in GBT TX clock domain
   // @TODO achieve this signal through gefe control register
   logic [NUMBER_OF_GBT_LINKS:1]  TurnOnGBTPatternGBTTX_b4;
   // busy signal - informs whether at least one motor on any link
   // does perform an operation:
   logic 			  Busy_o;
   logic 			  IRQEmulatorEnabled;

   // serial register data - from-to-gefe through ScEc bit:
   logic [31:0] 		  GefeCtrl_4x2b32 [NUMBER_OF_GBT_LINKS:1][1:0];
   logic [31:0] 		  GefeStatus_4x2b32 [NUMBER_OF_GBT_LINKS:1][1:0];
   logic [NUMBER_OF_GBT_LINKS:1] RxLocked_2b4 [1:0];
   logic [NUMBER_OF_GBT_LINKS:1] txemptyfifo_2b4 [1:0];
   logic [NUMBER_OF_GBT_LINKS:1] txerror_2b4 [1:0];
   logic [NUMBER_OF_GBT_LINKS:1] rxlol_2b4 [1:0];
   logic [NUMBER_OF_GBT_LINKS:1] SerialLinkUp_2b4 [1:0];


   // signal which goes into gbt core scec lines in transmission path
   logic [1:0] 			  TxGefe_4b2[NUMBER_OF_GBT_LINKS:1];

   //===================================  Application Logic  ====================================\\

   //==== Application Revision ID & Information ====\\

   assign AppVersion_ob8      = g_ApplicationVersion_b8     [7:0];
   assign AppReleaseDay_ob8   = g_ApplicationReleaseDay_b8  [7:0];
   assign AppReleaseMonth_ob8 = g_ApplicationReleaseMonth_b8[7:0];
   assign AppReleaseYear_ob8  = g_ApplicationReleaseYear_b8 [7:0];
   assign AppInfo_ob352       = {g_AppInfo_b352, {352-$size(g_AppInfo_b352){1'b0}}};

   //==== Fixed Assignments ====\\

   // FMC voltages enabled as we have
   // DEBUG board there
   assign VfmcEnable_oen = 1'b0;
   assign FmcPgC2m_o     = ~VfmcEnable_oen;

   //==== Clocks Scheme ====\\

   //==== WishBone Address Decoder ====\\
   // Note that this is RESIDUAL from base application, which uses
   // 'non-interface' style of the registers. This makes the design
   // complicated: the GBTRefClk and SFP core uses non-interface
   // version of the WishBone access, and hence a) there's lots of
   // signals going into those entities, b) they still use
   // 'Generic[XY]InputReg' interface to work with VME. In turn it
   // means that we have to have here this awkward address decoder and
   // lots of ack/strobe/data signals. Until the GBT core and clock
   // generates do not use the wishbone interface, we have to keep it
   // and do the translation from nice-interface-way to
   // notsonice-bloated-entities. The VME targets, which are
   // instantiated directly in this entity (thus are not part of
   // System/Core libraries) we ported to wishbone interface version
   // and are part of GlobalRegisters_4b32
   // HENCEEEEEEEEEE:
   // re-casting top-level wishbon to application default slave interface
   // WbMasterAdr_ib25 -> Slaves_iot[WB_DEFAULTAPP].Adr_b
   // WbMasterStb_i -> Slaves_iot[WB_DEFAULTAPP].Stb
   // WbMasterAck_o -> Slaves_iot[WB_DEFAULTAPP].Ack
   // WbMasterDat_ob32 -> Slaves_iot[WB_DEFAULTAPP].DatMiSo_b
   // WbMasterDat_ib32 -> Slaves_iot[WB_DEFAULTAPP].DatMoSi_b
   // WbMasterWr_i -> Slaves_iot[WB_DEFAULTAPP].We
   // WbMasterCyc_i -> Slaves_iot[WB_DEFAULTAPP].Cyc
   // declare all slaves as interfaces
   t_WbInterface #(.g_DataWidth(g_DataWidth),
   	      	   .g_AddressWidth(g_AddressWidth))
   Slaves_iot[0:WISHBONE_SLAVES-1] (ClkRsGBT40MHz_ix.clk, ClkRsGBT40MHz_ix.reset);

   AddrDecoderWbApp
     i_AddrDecoderWbApp
       (.Clk_ik                           (ClkRsGBT40MHz_ix.clk),
	.Adr_ib21                         (Slaves_iot[WB_DEFAULTAPP].Adr_b[20:0]),
	.Stb_i                            (Slaves_iot[WB_DEFAULTAPP].Stb),
	.Dat_oqb32                        (Slaves_iot[WB_DEFAULTAPP].DatMiSo_b),
	.Ack_oq                           (Slaves_iot[WB_DEFAULTAPP].Ack),
	//--
	.DatCtrlReg_ib32                  (),
	.AckCtrlReg_i                     (),
	.StbCtrlReg_oq                    (),
	//--
	.DatStatReg_ib32                  (),
	.AckStatReg_i                     (),
	.StbStatReg_oq                    (),
	//--
	.DatEthSfpStatReg_ib32            (),
	.AckEthSfpStatReg_i               (),
	.StbEthSfpStatReg_oq              (),
	//--
	.DatGbtRefClkSch_ib32             (WbDatGbtRefClkSch_b32),
	.AckGbtRefClkSch_i                (WbAckGbtRefClkSch),
	.StbGbtRefClkSch_oq               (WbStbGbtRefClkSch),
	//--
	.DatAppSfpGbtUserDataCtrlReg_ib32 (),
	.AckAppSfpGbtUserDataCtrlReg_i    (),
	.StbAppSfpGbtUserDataCtrlReg_oq   (),
	//--
	.DatAppSfpGbtUserDataStatReg_ib32 (),
	.AckAppSfpGbtUserDataStatReg_i    (),
	.StbAppSfpGbtUserDataStatReg_oq   (),
	//--
	.DatAppSfpGbtUserDataBertReg_ib32 (),
	.AckAppSfpGbtUserDataBertReg_i    (),
	.StbAppSfpGbtUserDataBertReg_oq   (),
	//--
	.DatGbtCtrl_ib32                  (WbDatGbtCtrl_b32),
	.AckGbtCtrl_i                     (WbAckGbtCtrl),
	.StbGbtCtrl_oq                    (WbStbGbtCtrl),
	//--
	.DatI2cToWb_ib32                  ({24'h0, WbMasterI2cDat_b8}),
	.AckI2cToWb_i                     (WbMasterI2cAck),
	.StbI2cToWb_oq                    (WbMasterI2cStb));

   //==== WhiteRabbit ====\\

   // Dual Master Arbiter for the I2C Slaves on the I2C Muxes:
   WbBus2M1S #(
	       .g_DataWidth ( 8),
	       .g_AddrWidth (12))
   i_WbBus2M1S(
	       .Clk_ik      (ClkRsGBT40MHz_ix.clk),
	       .CycM1_i     (1'b0),
	       .StbM1_i     (1'b0),
	       .WeM1_i      (1'b0),
	       .AddrM1_ib   ('0),
	       .DataM1_ib   (/* Not connected */),
	       .AckM1_o     (),
	       .DataM1_ob   (),
	       .CycM2_i     (Slaves_iot[WB_DEFAULTAPP].Cyc),
	       .StbM2_i     (WbMasterI2cStb),
	       .WeM2_i      (Slaves_iot[WB_DEFAULTAPP].We),
	       .AddrM2_ib   (Slaves_iot[WB_DEFAULTAPP].Adr_b[11:0]),
	       .DataM2_ib   (Slaves_iot[WB_DEFAULTAPP].DatMoSi_b[ 7:0]),
	       .AckM2_o     (WbMasterI2cAck),
	       .DataM2_ob   (WbMasterI2cDat_b8),
	       .CycS_o      (I2cWbCyc_o),
	       .StbS_o      (I2cWbStb_o),
	       .WeS_o       (I2cWbWe_o),
	       .AddrS_ob    (I2cWbAdr_ob12),
	       .DataS_ob    (I2cWbDat_ob8),
	       .AckS_i      (I2cWbAck_i),
	       .DataS_ib    (I2cWbDat_ib8));

   //==== GBT-FPGA ====\\

   // dummy led outputs (superseded by top)
   assign Led_ob8[8:5] = '0;

   // declare combined fault signals:
   logic [NUMBER_OF_GBT_LINKS:1]  SfpLos_b4,
				  SfpTxFault_b4,
				  SfpTxDisable_b4,
				  SfpPresent_b4;

   logic [NUMBER_OF_GBT_LINKS:1]  IRQSFPFailLvl_b4,
				  IRQSFPFailRise_b4,
				  IRQSFPFailFall_b4;


   // present signals
   assign SfpPresent_b4 = {AppSfp4Present_iq,
			   AppSfp3Present_iq,
			   AppSfp2Present_iq,
			   AppSfp1Present_iq};

   // los signals are in ClkRsGBT40MHz_ix domain
   assign SfpLos_b4 = {AppSfp4Los_iq,
		       AppSfp3Los_iq,
		       AppSfp2Los_iq,
		       AppSfp1Los_iq};
   // txfaults are in ClkRsGBT40MHz_ix domain
   assign SfpTxFault_b4 = {AppSfp4TxFault_iq,
			   AppSfp3TxFault_iq,
			   AppSfp2TxFault_iq,
			   AppSfp1TxFault_iq};
   // all status signals are in ClkRsGBT40MHz_ix domain
   assign SfpTxDisable_b4 = {StatusAppSfp4TxDisable_iq,
			     StatusAppSfp3TxDisable_iq,
			     StatusAppSfp2TxDisable_iq,
			     StatusAppSfp1TxDisable_iq};
   // so the point here: the IRQ indicating links statuses should be
   // triggered any time there's loss of signal, and txfault, but
   // txfault only if TX is actually enabled. Each time such thing
   // happens the IRQ is EDGE detected
   assign IRQSFPFailLvl_b4 = SfpLos_b4 | (~SfpTxDisable_b4 &
					  SfpTxFault_b4);
   wire 	   GbtTxFrameClk40MHz_k;
   assign GbtTxFrameClk40MHz_k = ClkRsGBT40MHz_ix.clk;




   // Application SFPs GBT Bank (4x GBT Links):
   VfcHdGbtCoreX4 #
     (.g_GbtBankId             (1'b0),
      .g_TxLatOp               (1'b0), // '0' -> Standard Latency   | '1' -> Latency Optimized
      .g_RxLatOp               (1'b0), // '0' -> Standard Latency   | '1' -> Latency Optimized
      .g_TxWideBus             (1'b0), // '0' -> GBT Frame Encoding | '1' -> WideBus Encoding
      .g_RxWideBus             (1'b0), // '0' -> GBT Frame Encoding | '1' -> WideBus Encoding
      .g_TxElinksDataAlignEn   ({1'b1, 1'b1, 1'b1, 1'b1}),
      .g_RxElinksDataAlignEn   ({1'b1, 1'b1, 1'b1, 1'b1}),
      .g_TxRxMatchFlagsEn      (1'b1))
   i_AppSfpGbtBank
     (// WishBone Interface:
      .Clk_ik                  (ClkRsGBT40MHz_ix.clk),
      .Rst_ir                  (ClkRsGBT40MHz_ix.reset),
      .Cyc_i                   (Slaves_iot[WB_DEFAULTAPP].Cyc),
      .Stb_i                   (WbStbGbtCtrl),
      .We_i                    (Slaves_iot[WB_DEFAULTAPP].We),
      .Adr_ib7                 (Slaves_iot[WB_DEFAULTAPP].Adr_b[6:0]),
      .Dat_ib32                (Slaves_iot[WB_DEFAULTAPP].DatMoSi_b),
      .Dat_ob32                (WbDatGbtCtrl_b32),
      .Ack_o                   (WbAckGbtCtrl),
      // SFPs Control:
      .SfpPresent_ib4          (SfpPresent_b4),
      .SfpTxFault_ib4          (SfpTxFault_b4),
      .SfpLos_ib4              (SfpLos_b4),
      .SfpTxDisable_oqb4       ({AppSfp4TxDisable_o,
				 AppSfp3TxDisable_o,
				 AppSfp2TxDisable_o,
				 AppSfp1TxDisable_o}),
      .SfpRateSelect_oqb4      ({AppSfp4RateSelect_o,
				 AppSfp3RateSelect_o,
				 AppSfp2RateSelect_o,
				 AppSfp1RateSelect_o}),
      .StatusSfpTxDisable_ib4  (SfpTxDisable_b4),
      .StatusSfpRateSelect_ib4 ({StatusAppSfp4RateSelect_iq,
				 StatusAppSfp3RateSelect_iq,
				 StatusAppSfp2RateSelect_iq,
				 StatusAppSfp1RateSelect_iq}),
      // Clocks Scheme:
      .GbtRefClk120MHz_ik      (ClkRs_ix.clk),
      //--
      .TxFrameClk40Mhz_ik      (GbtTxFrameClk40MHz_k),
      .RxFrameClk40Mhz_okb4    (),
      // GBT Resets Scheme:
      .GbtReset_ir             (ClkRsGBT40MHz_ix.reset),
      .TxGbtReset_irb4         (4'h0),
      .RxGbtReset_irb4         (4'h0),
      // Serial Lanes:
      .MgtTx_ob4               (AppSfpTx_ob4),
      .MgtRx_ib4               (AppSfpRx_ib4),
      // Parallel Data:
      // Note!! TxDataScIc must be set to 2'b11 when not used
      .TxDataScIc_i4b2         (TxDataScIcAppSfpGbtUserData_4b2),
      .TxDataScEc_i4b2         (TxDataScEcAppSfpGbtUserData_4b2),
      .TxData_i4b80            (TxDataAppSfpGbtUserData_4b80),
      // WideBus data not used when using GBT encoding
      .TxDataWb_i4b32          ('{default: 32'h0}),
      //--
      .RxDataScIc_o4b2         (RxDataScIcAppSfpGbtUserData_4b2),
      .RxDataScEc_o4b2         (RxDataScEcAppSfpGbtUserData_4b2),
      .RxData_o4b80            (RxDataAppSfpGbtUserData_4b80),
      // WideBus data not used when using GBT encoding
      .RxDataWb_o4b32          (),
      // Control:
      .GbtTxReady_ob4          (),
      .GbtRxReady_ob4          (GbtRxReadyAppSfpGbtUserData_b4),
      // txisdatasel is '1' any time there are valid MOTOR data coming
      // to the interface.
      .TxIsDataSel_ib4         (TxIsDataSelAppSfpGbtUserData_b4),
      .RxIsDataFlag_ob4        (RxIsDataFlagAppSfpGbtUserData_b4),
      // Latency Test Flags:
      .TxMatchFlag_o           (TxMatchFlagAppSfpGbtUserData),
      .RxMatchFlag_ob4         (RxMatchFlagAppSfpGbtUserData_b4));

   gbt_pattern_generator i_TxDataGenAppSfpGbtUserData
     (.RESET_I                 (ClkRsGBT40MHz_ix.reset),
      .TX_FRAMECLK_I           (ClkRsGBT40MHz_ix.clk),
      // GBT encoding
      .TX_ENCODING_SEL_I       (2'b0),
      .TEST_PATTERN_SEL_I      (TestPatterSelAppSfpGbtUserData_b2),
      .TX_DATA_O               (TxDataAppSfpGbtUserData_b84),
      .TX_EXTRA_DATA_WIDEBUS_O ());


   assign ErrInjAppSfpGbtUserData_q = ErrInjAppSfpGbtUserData;

   always @(posedge ClkRsGBT40MHz_ix.clk)
     TxDataErrInjAppSfpGbtUserData_qb84 <= #1
					   ErrInjAppSfpGbtUserData_q ?
					   {TxDataAppSfpGbtUserData_b84[83:1],
					    ~TxDataAppSfpGbtUserData_b84[0]} :
					   TxDataAppSfpGbtUserData_b84;

   // this is connected to error injection, we check that error
   // injection gives single pulse when ran
   assert property (@(posedge ClkRsGBT40MHz_ix.clk) disable iff (ClkRsGBT40MHz_ix.reset)
		    $rose(AppSfpGbtUserDataCtrlRegs_b32[8]) |=>
		    ~AppSfpGbtUserDataCtrlRegs_b32[8]) else
     $error("ErrInjAppSfpGbtUserData should be single clock when written into register");

   assign AppSfpGbtUserDataCtrlRegs_qb32 =
					  AppSfpGbtUserDataCtrlRegs_b32;


   // FOLLOWING VECTORS ARE LEVEL TRIGGERED, SO IT IS ENOUGH TO GET
   // THEM TO SLOW CLOCK DOMAIN BY TWO FLIP FLOPS
   // Selection of the GBT pattern to send, only one patter for all
   // channels can be selected
   assign TestPatterSelAppSfpGbtUserData_b2         = AppSfpGbtUserDataCtrlRegs_qb32[ 5: 4];
   //     Reserved                                    AppSfpGbtUserDataCtrlRegs_qb32[ 7: 6]
   // rising edge on this will inject single error into GBT stream
   assign ErrInjAppSfpGbtUserData                   = AppSfpGbtUserDataCtrlRegs_qb32[    8];
   //     Reserved                                    AppSfpGbtUserDataCtrlRegs_qb32[11: 9]
   // '1' on each bit allows casting pattern stream into ScEc channels
   assign ScEcCheckEnAppSfpGbtUserData_b4           = AppSfpGbtUserDataCtrlRegs_qb32[15:12];
   // '1' on each input toggles the phase of the generator vs checker
   // for toggling mode
   assign TogglingDataCheckSyncAppSfpGbtUserData_b4 =
						     AppSfpGbtUserDataCtrlRegs_qb32[19:16];

   assert property (@(posedge ClkRsGBT40MHz_ix.clk)
		    disable iff (ClkRsGBT40MHz_ix.reset)
		    $rose(|ResetGbtRxRdyLostFlagAppSfpGbtUserData_b4)
		    |=> ~|ResetGbtRxRdyLostFlagAppSfpGbtUserData_b4)
     else $error("RESETTING GBT FLAG SHOULD BE IN SINGLE CLOCK");

   assert property (@(posedge ClkRsGBT40MHz_ix.clk)
		    disable iff (ClkRsGBT40MHz_ix.reset)
		    $rose(|ResetGbtRxDataErrorsAppSfpGbtUserData_b4)
		    |=> ~|ResetGbtRxDataErrorsAppSfpGbtUserData_b4)
     else $error("RESETTING GBT FLAG SHOULD BE IN SINGLE CLOCK");


   // '1' on each particular bit turns on the loop mode through
   // GEFE. Basically it casts input and output streams from/to
   // pattern generator/checker and causes Ec bit 0 to go high to
   // announce to GEFE that we want it to switch to loop mode.
   always_ff @(posedge ClkRsGBT40MHz_ix.clk or posedge ClkRsGBT40MHz_ix.reset) begin
      if (ClkRsGBT40MHz_ix.reset)
	TurnOnGBTPattern_b4 <= '0;
      else begin
	 if (~Busy_o)
	   TurnOnGBTPattern_b4 <= AppSfpGbtUserDataCtrlRegs_b32[31:28];
      end
   end

   triggers_t triggers;
   // the GBT pattern will inhibit all trigger sources:
   assign triggers.TriggerEnable_i = ~|TurnOnGBTPattern_b4;

   // first two registers handle RxTx in general
   assign AppSfpGbtUserDataStatRegs_4b32[0] = {20'h0,
					       TurnOnGBTPattern_b4,
					       RxIsDataFlagAppSfpGbtUserData_qb4,
					       GbtRxReadyAppSfpGbtUserData_qb4};
   assign AppSfpGbtUserDataStatRegs_4b32[1] = {24'h0,
					       RxDataErrFlagAppSfpGbtUserData_qb4,
					       GbtRxRdyLostFlagAppSfpGbtUserData_qb4};
   // and these handle serial ling through GBT to Gefe and back
   assign AppSfpGbtUserDataStatRegs_4b32[2] = 32'hBEEFCAFE; // Reserved
   assign AppSfpGbtUserDataStatRegs_4b32[3] = 32'hBEEFCAFE; // Reserved

   generate for (i=0;i<=3;i=i+1) begin: Gen_AppSfpGbtUserDataBertCdc
      for (j=0;j<=1;j=j+1) begin: Gen_AppSfpGbtUserDataBertCdcDpram

         generic_dpram_mod
	     #(.aw     ( 1),
	       .dw     (32))
         i_ErrorCntDpram
	     (.rclk  (ClkRsGBT40MHz_ix.clk),
	      .rrst  (ClkRsGBT40MHz_ix.reset),
	      .rce   (1'b1),
	      .oe    (1'b1),
	      .raddr (1'b0),
	      .dout  (AppSfpGbtUserDataBertRegs_16b32[(2*i)+j]),
	      .wclk  (ClkRsGBT40MHz_ix.clk),
	      .wrst  (ClkRsGBT40MHz_ix.reset),
	      .wce   (1'b1),
	      .we    (1'b1),
	      .waddr (1'b0),
	      .di    (RxDataErrorCntAppSfpGbtUserData_64b4[i+1][((32*(j+1))-1):(32*j)]));

         generic_dpram_mod
	   #(.aw     ( 1),
	     .dw     (32))
         i_FrameCntDpram
	   (.rclk  (ClkRsGBT40MHz_ix.clk),
	    .rrst  (ClkRsGBT40MHz_ix.reset),
	    .rce   (1'b1),
	    .oe    (1'b1),
	    .raddr (1'b0),
	    .dout  (AppSfpGbtUserDataBertRegs_16b32[((2*i)+8)+j]),
	    .wclk  (ClkRsGBT40MHz_ix.clk),
	    .wrst  (ClkRsGBT40MHz_ix.reset),
	    .wce   (1'b1),
	    .we    (1'b1),
	    .waddr (1'b0),
	    .di    (RxDataFrameCntAppSfpGbtUserData_64b4[i+1][((32*(j+1))-1):(32*j)]));
      end
   end endgenerate

   //==== Signals Forwarding ====\\

   always @* case(DipSw_ib8[2:1])
	       2'h0: TestIo1 = 1'b0;
	       2'h1: TestIo1 = ClkRs_ix.clk;
	       2'h2: TestIo1 = ClkRsGBT40MHz_ix.clk;
	       2'h3: TestIo1 = TxMatchFlagAppSfpGbtUserData;
	     endcase

   assign TestIo1_io = TestIo1;

   always @* case(DipSw_ib8[4:3])
	       2'h0: TestIo2 = ClkRsGBT40MHz_ix.clk;
	       2'h1: TestIo2 = '0;
	       2'h2: TestIo2 = '0;
	       2'h3: TestIo2 = RxMatchFlagAppSfpGbtUserData_b4[1];
	     endcase
   assign TestIo2_io = TestIo2;

   assign GpIo1DirOut_o  = 1'b0;
   assign GpIo2DirOut_o  = 1'b1;
   assign GpIo34DirOut_o = 1'b1;
   assign GpIo1EnTerm_o  = 1'b0;
   assign GpIo2EnTerm_o  = 1'b0;
   assign GpIo3EnTerm_o  = 1'b0;
   assign GpIo4EnTerm_o  = 1'b0;
   assign GpIo_iob4[2]   = '0;
   assign GpIo_iob4[3]   = '0;
   assign GpIo_iob4[4]   = '0;



   ////////////////////////////////////////////////////////////////////////////////
   // APPLICATION LEVEL - HERE WE IMPLEMENT THE non-vfc-hd-base project part of
   // the application. We keep the base project more-or-less intact so if
   // there's an upgrade of base project, it will be easy to track the changes,
   // however some portions of the code above need to be changed in order to
   // cast correctly fiber optics data channels
   ////////////////////////////////////////////////////////////////////////////////
   // So the point is: all what is above these comments is almost 99% part of
   // the base project. We leave it here as it contains interesting code as
   // e.g. measurement of BERT or lost packets. At the same time it makes bit
   // troubles because it is not using the same interface Wishbone as in the
   // custom part of the code. Custom part uses interface - so pass wishbone
   // through the module is a matter of __one__ (in words 'one') signal as
   // opposed to all the bus thing of the wishbone. Hence in order to keep those
   // two things compatible here goes an instance of cross-bar having two slaves
   // - one slave from 0 -> 1FFF is then injected into the original application
   // as 'wishbone master', and the other master as of 0x2000 is the space for
   // motor links.


   // out of spreaded wires generate interface master
   t_WbInterface #(.g_DataWidth(g_DataWidth),
		   .g_AddressWidth(g_AddressWidth))
   WbMaster_t(ClkRsGBT40MHz_ix.clk, ClkRsGBT40MHz_ix.reset);

   wbsig2wbiface
     i_wbsig2wbiface
       (// Interfaces
	.Wb_iot		(WbMaster_t),
	// Outputs
	.WbMasterDat_ob32	(WbMasterDat_ob32[31:0]),
	.WbMasterAck_o	(WbMasterAck_o),
	// Inputs
	.WbMasterCyc_i	(WbMasterCyc_i),
	.WbMasterStb_i	(WbMasterStb_i),
	.WbMasterAdr_ib25	(WbMasterAdr_ib25[24:0]),
	.WbMasterWr_i	(WbMasterWr_i),
	.WbMasterDat_ib32	(WbMasterDat_ib32[31:0]));

   // instantiate crossbar, which will take into account all the register spaces
   // declared here, and in addition it will serve as master for all other
   // 'non-interface-type' wishbone components.
   // generate wishbone crossbar for all 4 mclinks,
   localparam logic [g_AddressWidth-1:0]  c_SlaveAddresses_mb
					  [0:WISHBONE_SLAVES-1] =
					  '{
					    // default app space
					    (g_AddressWidth)'('h00_0000),
					    // fourlinks address space
					    (g_AddressWidth)'(g_fourlinksAddress),
					    // global registers space
					    (g_AddressWidth)'(g_globalRegistersAddress),
					    // motor emulator block
					    (g_AddressWidth)'(g_motorEmulatorBlockAddress)
					    };

   // declare for each of the registers correct mask
   localparam logic [g_AddressWidth-1:0]  c_SlaveMasks_mb
					  [0:WISHBONE_SLAVES-1] =
					  '{
					    // default app space
					    (g_AddressWidth)'('h00_1FFF),
					    (g_AddressWidth)'('h00_1FFF),
					    (g_AddressWidth)'('h00_1FFF),
					    (g_AddressWidth)'('h00_1FFF)
					    };

   WbCrossbar
     #(.g_Slaves(WISHBONE_SLAVES),
       .g_AddressWidth(g_AddressWidth),
       .g_SlaveMasks_mb(c_SlaveMasks_mb),
       .g_SlaveAddresses_mb(c_SlaveAddresses_mb)
       )
   i_Retranslation
     (.Master_iot		(WbMaster_t),
      .Slaves_iot		(Slaves_iot));


   // instantiate global registers
   localparam NUMBER_OF_GLOBAL_REGISTERS = 49;
   // and declare registers holding the information
   logic [g_DataWidth-1:0] 		  GlobalRegisters_4b32 [0:NUMBER_OF_GLOBAL_REGISTERS-1];
   logic [23:0] 			  IRQEmulator_ob24, InterruptRequest_b24, CurrentIRQ_ob24;
   logic [31:0] 			  GlobalStatus_b32;
   // INTERRUPT REQUESTER here, this contains encoded link and motors
   // which cause current IRQ, has to be sent to interrupts upstream
   logic [$clog2(NUMBER_OF_GBT_LINKS)+NUMBER_OF_MOTORS_PER_FIBER-1:0]
								      IRQ_ob;
   // overflow flag generated if IRQ fifo from particular GBT link is
   // flooded by too many IRQs
   logic [NUMBER_OF_GBT_LINKS-1:0] 				      Overflow_ob;
   // this signal goes to IRQ configuration, allows to rese the state
   // of the overflow lines
   logic 							      ResetOverflow_i;

   // reset of overflows is assigned to GlobalTrigger register (so if
   // wanted, one can reset FIFO overflow and trigger using the same command)
   assign ResetOverflow_i = GlobalRegisters_4b32[0][1];

   assert property(@(posedge ClkRsGBT40MHz_ix.clk)
		   disable iff (ClkRsGBT40MHz_ix.reset)
		   $rose(ResetOverflow_i) |=> !ResetOverflow_i) else
     $error("ResetOverflow_i should be set to 1cc trigger");

   // make global trigger works when not in pattern mode. By default
   // pattern mode inhibits all triggering as it might break motor
   // movements. So:
   assign triggers.TrigMoveInt_i = GlobalRegisters_4b32[0][0] &
				   ~|TurnOnGBTPattern_b4;

   assign IRQEmulatorEnabled = GlobalRegisters_4b32[1][0];

   assign AppSfpGbtUserDataCtrlRegs_b32 = GlobalRegisters_4b32[3];
   assign GefeCtrl_4x2b32[1][0] = GlobalRegisters_4b32[32];
   assign GefeCtrl_4x2b32[2][0] = GlobalRegisters_4b32[33];
   assign GefeCtrl_4x2b32[3][0] = GlobalRegisters_4b32[34];
   assign GefeCtrl_4x2b32[4][0] = GlobalRegisters_4b32[35];
   // these have to be muxed and break the link when the pattern
   // generator is turned on:
   genvar 							      lp;
   generate
      for (lp=1; lp < NUMBER_OF_GBT_LINKS+1; lp++) begin : pg_discardloop
	 always_comb
	   if (TurnOnGBTPattern_b4[lp])
	     GefeCtrl_4x2b32[lp][1] = 32'hdeadbeef;
	   else
	     GefeCtrl_4x2b32[lp][1] = GlobalRegisters_4b32[39+lp];
      end
   endgenerate



   assert property (@(posedge ClkRsGBT40MHz_ix.clk)
   		    disable iff (ClkRsGBT40MHz_ix.reset)
   		    $rose(triggers.TrigMoveInt_i) |->
		    ~|TurnOnGBTPattern_b4)
     else $error("Global trigger must not fire in pattern mode");

   logic [31:0]   SerialRegisterStatus_2b32 [1:0];

   genvar 	  scecchannel;
   generate
      for(scecchannel=0; scecchannel < 2; scecchannel++) begin : scecstat
	 assign SerialRegisterStatus_2b32[scecchannel] = {12'b0,
							  txemptyfifo_2b4[scecchannel],
							  txerror_2b4[scecchannel],
							  rxlol_2b4[scecchannel],
							  SerialLinkUp_2b4[scecchannel],
							  RxLocked_2b4[scecchannel]};
      end
   endgenerate

   ////////////////////////////////////////////////////////////////////////////////
   // GLOBAL REGISTER FIELD - HOW TO ADD REGISTER:
   // FOR WRITE FROM VME:
   // - set g_DirectionMiso_b bit in the field to '0' for the register
   // - assign from the global register to your specific variable, as
   //   e.g.:
   //      assign myvar = GlobalRegisters_4b32[0][0];
   // FOR READING FROM FPGA TO VME:
   // - set g_DirectionMiso_b bit to '1'
   // - add the register into the Register_imb of the WbRegField
   WbRegField
     #(.g_NumberOfRegisters(NUMBER_OF_GLOBAL_REGISTERS),
       .g_DataWidth(g_DataWidth),
       // 0 = register to write from VME
       // 1 = register to read from VME
       .g_DirectionMiso_b(49'b1_11110000_11110000_11111111_11111111_01111111_11110100),
       .g_DefaultValue_mb('{NUMBER_OF_GLOBAL_REGISTERS{(g_DataWidth)'(0)}}),
       // 0 = toggle switch (permanent change)
       // 1 = flip (single clock when written)
       .g_AutoClrMask_mb
       ('{// (0) 0=internal trigger (flip), 1=reset IRQ
	  // overflow flag (flip)
	  (g_DataWidth)'(32'hffffffff),
	  // (1) IRQ emulator on (toggle)
	  (g_DataWidth)'(32'hfffffffe),
	  // (2) [24:0] IRQ barrel shifter
	  // status, [31:24] = IRQ overflow
	  // indicators for each GBT link
	  (g_DataWidth)'(0),
	  // (3)
	  // 8=AppSfpGbtUserDataCtrlRegs_b32 (flip)
	  (g_DataWidth)'(32'h0FF0_0100),
	  // 4,5,6,7
	  (g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),
	  // 8
	  (g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),
	  // 12
	  (g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),
	  // 16
	  (g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),
	  // 20
	  (g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),
	  // 24
	  (g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),
	  // 28
	  (g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),
	  // 32
	  (g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),
	  // 36
	  (g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),
	  // 40
	  (g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),
	  // 44
	  (g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),(g_DataWidth)'(0),
	  // 48
	  (g_DataWidth)'(0)
	  }
	)
       )
   i_WbRegField
     (// Interfaces
      .Wb_iot		(Slaves_iot[WB_GLOBALREGS]),
      // Outputs portion - here's what is written from VME
      // to input global registers
      .Register_omb		(GlobalRegisters_4b32),
      // Inputs - here's what is returned to VME when
      // reading happens on each position
      .Register_imb
      ('{GlobalRegisters_4b32[0], // 0
	 GlobalRegisters_4b32[1], // 1
	 {Overflow_ob, 4'b0,
	  CurrentIRQ_ob24}, // 2
	 AppSfpGbtUserDataCtrlRegs_b32,//3
	 AppSfpGbtUserDataStatRegs_4b32[0], // 4
	 AppSfpGbtUserDataStatRegs_4b32[1], // 5
	 AppSfpGbtUserDataStatRegs_4b32[2], // 6
	 AppSfpGbtUserDataStatRegs_4b32[3], // 7
	 // 8-11 emulation of ETH SFP data
	 // making dead to be clear that functionality is not
	 // implemented in this design!
	 32'hdeaddead, // 8
	 32'hdeaddead, // 9
	 32'hdeaddead, // 10
	 32'hdeaddead, // 11
	 32'haaaaaaaa, // 12
	 SerialRegisterStatus_2b32[0], // 13
	 SerialRegisterStatus_2b32[1], // 14
	 GlobalRegisters_4b32[15], // 15
	 // 16 to 31 is dedicated to
	 // BERT registers
	 AppSfpGbtUserDataBertRegs_16b32[ 0], AppSfpGbtUserDataBertRegs_16b32[ 1],
	 AppSfpGbtUserDataBertRegs_16b32[ 2], AppSfpGbtUserDataBertRegs_16b32[ 3],
	 AppSfpGbtUserDataBertRegs_16b32[ 4], AppSfpGbtUserDataBertRegs_16b32[ 5],
	 AppSfpGbtUserDataBertRegs_16b32[ 6], AppSfpGbtUserDataBertRegs_16b32[ 7],
	 AppSfpGbtUserDataBertRegs_16b32[ 8], AppSfpGbtUserDataBertRegs_16b32[ 9],
	 AppSfpGbtUserDataBertRegs_16b32[10], AppSfpGbtUserDataBertRegs_16b32[11],
	 AppSfpGbtUserDataBertRegs_16b32[12], AppSfpGbtUserDataBertRegs_16b32[13],
	 AppSfpGbtUserDataBertRegs_16b32[14], AppSfpGbtUserDataBertRegs_16b32[15],
	 // 8 registers dedicated to serial link to GEFE
	 // 32 to 35 are gefe control scec 0 - data sent to gefe
	 GefeCtrl_4x2b32[1][0], GefeCtrl_4x2b32[2][0], GefeCtrl_4x2b32[3][0], GefeCtrl_4x2b32[4][0],
	 // 36 to 39 are RxGefe - data read through GEFE
	 GefeStatus_4x2b32[1][0], GefeStatus_4x2b32[2][0], GefeStatus_4x2b32[3][0], GefeStatus_4x2b32[4][0],
	 // ScEc1 channel data:
	 // 40 to 43 are Gefe Control Scec[1] channel
	 GefeCtrl_4x2b32[1][1], GefeCtrl_4x2b32[2][1], GefeCtrl_4x2b32[3][1], GefeCtrl_4x2b32[4][1],
	 // and 44 to 47 are Gefe Status Scec[1] channel
	 GefeStatus_4x2b32[1][1], GefeStatus_4x2b32[2][1], GefeStatus_4x2b32[3][1], GefeStatus_4x2b32[4][1],
	 // 48 is productID
	 g_productID
	 }));

   initial assert ($bits(Overflow_ob) == 4) else begin
      $display("When you change number of GBT links, in the register");
      $display("declaration above as well casting of the Overflow_ob register");
      $display("into the VME map");
   end

   // output packets interface is easygoing, just generation of TX
   // array and assignment of a single clock to the structure. USE GBT
   // CLOCK FOR PACKETS SENT OUT, AND CLKRSIX CLOCK FOR PACKETS SENT
   // IN. THESE ARE TRANSLATED FROM GBTRX domain
   data_x #(.g_DataWidth(g_FrameSize))
   packet_ox[NUMBER_OF_GBT_LINKS-1:0](.ClkRs_ix(ClkRsGBT40MHz_ix)),
     packet_ix[NUMBER_OF_GBT_LINKS-1:0](.ClkRs_ix(ClkRsGBT40MHz_ix));


   genvar 				  linkiface;
   generate
      for(linkiface=1; linkiface < NUMBER_OF_GBT_LINKS+1; linkiface++)
	begin : linkgenerator
	   get_edge i_getedge (
			       // Outputs
			       .rising_o		(IRQSFPFailRise_b4[linkiface]),
			       .falling_o	(IRQSFPFailFall_b4[linkiface]),
			       .data_o		(),
			       // Inputs
			       .ClkRs_ix		(ClkRsGBT40MHz_ix),
			       .data_i		(IRQSFPFailLvl_b4[linkiface]));

	   gbt_pattern_checker i_RxDataCheckAppSfpGbtUserData
	     (.RESET_I                              (ClkRsGBT40MHz_ix.reset),
	      .RX_FRAMECLK_I                        (ClkRsGBT40MHz_ix.clk),
	      .RX_DATA_I	({RxDataScIcAppSfpGbtUserData_4b2[linkiface],
				  RxDataScEcAppSfpGbtUserData_4b2[linkiface],
				  RxDataAppSfpGbtUserData_4b80[linkiface]}),
	      .RX_EXTRA_DATA_WIDEBUS_I              (32'h0),
	      .GBT_RX_READY_I                       (GbtRxReadyAppSfpGbtUserData_b4[linkiface]),
	      .SCEC_CHECK_EN_I                      (ScEcCheckEnAppSfpGbtUserData_b4[linkiface]),
	      .RX_ENCODING_SEL_I                    (2'b0),
	      .TEST_PATTERN_SEL_I                   (TestPatterSelAppSfpGbtUserData_b2),
	      .TOGGLING_DATA_CHECK_SYNC_I           (TogglingDataCheckSyncAppSfpGbtUserData_b4[linkiface]),
	      .RESET_GBTRXREADY_LOST_FLAG_I         (ResetGbtRxRdyLostFlagAppSfpGbtUserData_b4[linkiface]),
	      .RESET_DATA_ERRORS_I                  (ResetGbtRxDataErrorsAppSfpGbtUserData_b4[linkiface]),
	      .GBTRXREADY_LOST_FLAG_O               (GbtRxRdyLostFlagAppSfpGbtUserData_b4[linkiface]),
	      .RXDATA_ERRORSEEN_FLAG_O              (RxDataErrFlagAppSfpGbtUserData_b4[linkiface]),
	      .RXEXTRADATA_WIDEBUS_ERRORSEEN_FLAG_O (),
	      .RXDATA_ERROR_CNT                     (RxDataErrorCntAppSfpGbtUserData_64b4[linkiface]),
	      .RXDATA_FRAME_CNT                     (RxDataFrameCntAppSfpGbtUserData_64b4[linkiface]));

	   // FOLLOWING ARE EDGE TRIGGERED, HENCE WE NEED TO USE PULSE
	   // SYNCHRONIZERS TO PASS THE DATA TO THE TARGET DOMAIN. WE SYNC
	   // FROM FAST TO SLOW DOMAIN HERE
	   // resets stats bits indicating loss of rdy and loss of data
	   // @TODO these have to trigger IRQ fail when it happens! (at some
	   // late stage)
	   assign ResetGbtRxRdyLostFlagAppSfpGbtUserData_b4[linkiface]
	     = AppSfpGbtUserDataCtrlRegs_b32[19+linkiface];
	   assign ResetGbtRxDataErrorsAppSfpGbtUserData_b4[linkiface]
	     = AppSfpGbtUserDataCtrlRegs_b32[23+linkiface];


	   // this property verifies, that when we request pattern generator,
	   // it will be eventually turned on (when busy goes off, but that's
	   // none of a bussiness for this SVA).
	   // ADDENDUM: ok, this does not work in modelsim. The condition
	   // triggers OK when starting and stopping, but does not announce
	   // failure if at the end of simulation the signal in
	   // TurnOnGBTPattern_b4 was never risen. And modelsim triggers some
	   // weird messages that you're not allowed to stop simulation ....
	   // synthesis translate_off
	   mio: assert property (@(posedge ClkRsGBT40MHz_ix.clk)
				 disable iff (ClkRsGBT40MHz_ix.reset)
				 $rose(AppSfpGbtUserDataCtrlRegs_b32[27+linkiface]) |->
				 s_eventually $rose(TurnOnGBTPattern_b4[linkiface])) else
	     $fatal(0, "FAILED TO GET PATTERN PROPAGATION");
	   // synthesis translate_on

	   assign GbtRxReadyAppSfpGbtUserData_qb4[linkiface] =
							      GbtRxReadyAppSfpGbtUserData_b4[linkiface];
	   assign RxIsDataFlagAppSfpGbtUserData_qb4[linkiface] =
							      RxIsDataFlagAppSfpGbtUserData_b4[linkiface];
	   assign GbtRxRdyLostFlagAppSfpGbtUserData_qb4[linkiface] =
							      GbtRxRdyLostFlagAppSfpGbtUserData_b4[linkiface];

	   assign RxDataErrFlagAppSfpGbtUserData_qb4[linkiface] =
								 RxDataErrFlagAppSfpGbtUserData_b4[linkiface];

	   assign RxDataAppSfpGbtUserDataVME_4b80[linkiface] =
							      RxDataAppSfpGbtUserData_4b80[linkiface];

	   assign packet_ix[linkiface-1].enable =
						 ~TurnOnGBTPattern_b4[linkiface];
	   assign packet_ix[linkiface-1].data =
					       RxDataAppSfpGbtUserDataVME_4b80[linkiface];
	   assign TurnOnGBTPatternGBTTX_b4[linkiface] =
						 TurnOnGBTPattern_b4[linkiface];

	   // multiplexer of fiber TX data:
	   always_comb begin
	      if (TurnOnGBTPatternGBTTX_b4[linkiface]) begin
		 TxDataAppSfpGbtUserData_4b80[linkiface] =
							  TxDataErrInjAppSfpGbtUserData_qb84[79:0];
		 // isdatavalid is always false when in loopback or
		 // pattern generator mode
		 TxIsDataSelAppSfpGbtUserData_b4[linkiface] = '0;
	      end else begin
		 TxDataAppSfpGbtUserData_4b80[linkiface] =
							 packet_ox[linkiface-1].data;
		 // isdatavalid for GEFE corresponds to packet enable
		 // signal, generated inside wbmc controls.
		 TxIsDataSelAppSfpGbtUserData_b4[linkiface] = packet_ox[linkiface-1].enable;
	      end

	      // and those two for the moment generic, and WE DO NOT
	      // TEST IC data through pattern generator as they can
	      // kill GEFE communication (Ic is connected to I2C bus
	      // doing reprogramming of GBTx)
	      TxDataScIcAppSfpGbtUserData_4b2[linkiface] = 2'b11;
	      // SC data bit 0 announces to GEFE if we're in
	      // pattern mode. If so, GEFE will turn itself into
	      // loopback mode, i.e. taking all the data, and
	      // returning them back 'as they are'
	      TxDataScEcAppSfpGbtUserData_4b2[linkiface] = TxGefe_4b2[linkiface];
	   end // always_comb

	   // serial register used to control slow-link and pass the
	   // parameters to gefe AND from gefe we get 32bit of build number,
	   // which serves us as information that GEFE is present on the other
	   // side.
	   for(scecchannel=0; scecchannel < 2; scecchannel++) begin : scecgenerator
	      serial_register
			   i_serial_scec0
			   (// Outputs
			    .data_ob32		(GefeStatus_4x2b32[linkiface][scecchannel]),
			    .Tx_o			(TxGefe_4b2[linkiface][scecchannel]),
			    .SerialLinkUp_o		(SerialLinkUp_2b4[scecchannel][linkiface]),
			    .RxLocked_o		(RxLocked_2b4[scecchannel][linkiface]),
			    .TxBusy_o			(),
			    .newdata_o		(),
			    .TxEmptyFifo_o		(txemptyfifo_2b4[scecchannel][linkiface]),
			    .txerror_o		(txerror_2b4[scecchannel][linkiface]),
			    .rxlol_o			(rxlol_2b4[scecchannel][linkiface]),
			    // Inputs
			    .ClkRs_ix			(ClkRsGBT40MHz_ix),
			    .ClkRxGBT_ix		(ClkRsGBT40MHz_ix),
			    .ClkTxGBT_ix		(ClkRsGBT40MHz_ix),
			    .data_ib32		(GefeCtrl_4x2b32[linkiface][scecchannel]),
			    .resetflags_i		(AppSfpGbtUserDataCtrlRegs_b32[23+linkiface]),
			    .Rx_i			(RxDataScEcAppSfpGbtUserData_4b2[linkiface][scecchannel]));
	   end // for (scecchannel=0; scecchannel < 2; scecchannel++)


	   // clock is 40MHz, calculated 5second period
	   led_blinker
	     #(
	       // Parameters
	       .g_totalPeriod		(LED_BLINKER_PERIOD),
	       .g_blinkOn		(LED_BLINKER_ON_TIME),
	       .g_blinkOff		(LED_BLINKER_OFF_TIME))
	   i_led_blinker
	     (
	      // Outputs
	      .led_o			(Led_ob8[linkiface]),
	      .period_o			(),
	      // Inputs
	      .ClkRs_ix			(ClkRsGBT40MHz_ix),
	      .forceOne_i		(IrqMKO_b4[linkiface]),
	      .amount_ib		(Led_8b4[linkiface]));

	   // MKO to detect if link has triggered IRQ. This
	   // information is stored in IRQ_ob - low 16 bits is IRQdata
	   // and high 4 bits are IRQ link. If low are non-zero, high
	   // identify which link broke. In addition we detect IRQ
	   // rise and fail for given link, this will blink diode as
	   // well when link turns on/off
	   mko #(
		 .g_CounterBits			(LED_IRQ_MKO_BITS)) i_extend_diode
	     (
	      // Outputs
	      .q_o				(IrqMKO_b4[linkiface]),
	      .q_on				(),
	      // Inputs
	      .enable_i ('1),
	      .ClkRs_ix				(ClkRsGBT40MHz_ix),
	      .width_ib				((LED_IRQ_MKO_BITS)'(LED_IRQ_MKO_VALUE)),
	      .start_i				(IrqTrigger_b4[linkiface]));


	   assign IrqTrigger_b4[linkiface] =
					    IRQSFPFailRise_b4[linkiface] ||
					    IRQSFPFailFall_b4[linkiface]
					    || LinkIRQ_b4[linkiface];

	   always_ff @(posedge ClkRsGBT40MHz_ix.clk or posedge ClkRsGBT40MHz_ix.reset) begin
	      if (ClkRsGBT40MHz_ix.reset) begin
		 Led_8b4[linkiface] <= '0;
	      end else begin
		 // SFP not present = single blink
		 if (!SfpPresent_b4[linkiface])
		   Led_8b4[linkiface] <= 1;
		 // SFP present, but LOS detected = LINE not present
		 // = 2 blinks
		 else if (SfpPresent_b4[linkiface] &&
			  SfpLos_b4[linkiface])
		   Led_8b4[linkiface] <= 2;
		 // SFP present, no LOS, but linking word is missing,
		 // blink three times:
		 else if (
			  (GefeStatus_4x2b32[linkiface][1] !=
			   GEFE_INTERLOCK)
			  &&
			  (GefeStatus_4x2b32[linkiface][1] !=
			   32'hdeadbeef))
		   Led_8b4[linkiface] <= 3;
		 // all ready, but pattern generator on particular
		 // link is turned on = 4 blinks. This is because
		 // turning the generator on kills the link loop:
		 else if (GefeStatus_4x2b32[linkiface][1] == 32'hdeadbeef)
		   Led_8b4[linkiface] <= 4;
		 // SFP present, no LOS AND linking word is present =
		 // LINE operational, go to 'no  blink' operation as
		 // further indication happens with IRQ. This
		 // condition will fail if pattern generator is on, as
		 // gefestatus value will be 32'hdeadbeef in case of
		 // pattern generator running
		 else if (SfpPresent_b4[linkiface] &&
			  !SfpLos_b4[linkiface] &&
			  GefeStatus_4x2b32[linkiface][1] == GEFE_INTERLOCK)
		   Led_8b4[linkiface] <= '0;
		 // in all other states LED blinks 5 times, this
		 // indicates some error
		 else
		   Led_8b4[linkiface] <= 5;
	      end
	   end
	end // block: linkgenerator
   endgenerate

   fourmclinks
     #(.g_fourlinksAddress		(0),
       /*AUTOINSTPARAM*/
       // Parameters
       .QUEUELENGTH			(QUEUELENGTH),
       .g_FrameSize			(g_FrameSize),
       .g_DataWidth			(g_DataWidth),
       .g_AddressWidth			(g_AddressWidth))
   i_fourmclinks
     (.Wb_iot		(Slaves_iot[WB_FOURLINKS]),
      /*AUTOINST*/
      // Interfaces
      .packet_ox			(packet_ox/*.[NUMBER_OF_GBT_LINKS-1:0]*/),
      .packet_ix			(packet_ix/*.[NUMBER_OF_GBT_LINKS-1:0]*/),
      // Outputs
      .Busy_o				(Busy_o),
      .IRQ_ob				(IRQ_ob[$clog2(NUMBER_OF_GBT_LINKS)+NUMBER_OF_MOTORS_PER_FIBER-1:0]),
      .Overflow_ob			(Overflow_ob[NUMBER_OF_GBT_LINKS-1:0]),
      // Inputs
      .ClkRs_ix				(ClkRsGBT40MHz_ix),
      .triggers				(triggers),
      .ResetOverflow_i			(ResetOverflow_i));


   // TrigMoveExt_i should never go active when we're in pattern mode
   assert property (@(posedge ClkRsGBT40MHz_ix.clk)
   		    disable iff (ClkRsGBT40MHz_ix.reset)
   		    $rose(triggers.TrigMoveExt_i) |-> ~|TurnOnGBTPattern_b4)
     else $error("Caught TrigMoveExt_i while in pattern mode");

   get_edge
     i_getedge
       (// Outputs
	.rising_o			(triggers.TrigMoveExt_i),
	.falling_o		(),
	.data_o			(),
	// Inputs
	.ClkRs_ix			(ClkRsGBT40MHz_ix),
	.data_i			(HwTrig & (~|TurnOnGBTPattern_b4)));

   // hardware trigger processing
   manyff
     #(.g_Latency			(g_Latency))
   i_manyff
     (// Outputs
      .d_o			(HwTrig),
      // Inputs
      .ClkRs_ix			(ClkRsGBT40MHz_ix),
      .d_i			(GpIo_iob4[1]));

   localparam BITSWIDTH = 25;

   irq_emulator
     #(/*AUTOINSTPARAM*/
       // Parameters
       .BITSWIDTH			(BITSWIDTH))
   i_irq_emulator
     (/*AUTOINST*/
      // Outputs
      .IRQEmulator_ob24			(IRQEmulator_ob24[23:0]),
      .CurrentIRQ_ob24			(CurrentIRQ_ob24[23:0]),
      // Inputs
      .ClkRs_ix				(ClkRsGBT40MHz_ix));


   // MUX casting emulator IRQ to the upstream
   always_ff @(posedge ClkRsGBT40MHz_ix.clk) begin
      if (IRQEmulatorEnabled)
	InterruptRequest_b24 <= IRQEmulator_ob24;
      else
	InterruptRequest_b24 <= {2'b0,
				 (IRQSFPFailRise_b4 |
				  IRQSFPFailFall_b4),
				 // size of 18 (bits 0-17)
				 IRQ_ob};
   end
   assign InterruptRequest_ob24 = InterruptRequest_b24;

   // decode link, which caused instantly irq. This has to be
   // registered otherwise timequest will report failing timing
   always_ff @(posedge ClkRsGBT40MHz_ix.clk or posedge ClkRsGBT40MHz_ix.reset) begin
      if (ClkRsGBT40MHz_ix.reset) begin
	 LinkIRQ_b4 <= '0;
      end else begin
	 if (|IRQ_ob[NUMBER_OF_MOTORS_PER_FIBER-1:0])
	   case  (IRQ_ob[$clog2(NUMBER_OF_GBT_LINKS)+
			 NUMBER_OF_MOTORS_PER_FIBER
			 -1:NUMBER_OF_MOTORS_PER_FIBER])
	     0: LinkIRQ_b4 <= 1;
	     1: LinkIRQ_b4 <= 2;
	     2: LinkIRQ_b4 <= 4;
	     3: LinkIRQ_b4 <= 8;
	     default: LinkIRQ_b4 <= 0;
	   endcase // case
	 // (IRQ_ob[$clog2(NUMBER_OF_GBT_LINKS)+NUMBER_OF_MOTORS_PER_FIBER-1:NUMBER_OF_MOTORS_PER_FIBER])
	 else
	   LinkIRQ_b4 <= 0;
      end
   end


endmodule
