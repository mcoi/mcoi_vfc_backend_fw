//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) November 2017 CERN

//-----------------------------------------------------------------------------
// @file WBSIG2WBIFACE.SV
// @brief Converts wishbone signals into its interface counterpart
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 08 November 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module wbsig2wbiface

  (
   // WishBone Bus Interface:
   input 	  WbMasterCyc_i,
   input 	  WbMasterStb_i,
   input [ 24:0]  WbMasterAdr_ib25,
   input 	  WbMasterWr_i,
   input [ 31:0]  WbMasterDat_ib32,
   output [ 31:0] WbMasterDat_ob32,
   output 	  WbMasterAck_o,
   // wishbone interface
   t_WbInterface Wb_iot
   );

   // assign signals to the interface
   assign Wb_iot.Cyc = WbMasterCyc_i;
   assign Wb_iot.Stb = WbMasterStb_i;
   assign Wb_iot.Adr_b = WbMasterAdr_ib25;
   assign Wb_iot.Sel_b = (4)'(-1);
   assign Wb_iot.We = WbMasterWr_i;
   assign Wb_iot.DatMoSi_b = WbMasterDat_ib32;
   assign WbMasterAck_o = Wb_iot.Ack;
   // assign WbMaster? = Wb_iot.Err;
   // assign WbMaster? = Wb_iot.Rty;
   // assign WbMaster? = Wb_iot.Stall;
   assign WbMasterDat_ob32 = Wb_iot.DatMiSo_b;



endmodule // wbsig2wbiface
