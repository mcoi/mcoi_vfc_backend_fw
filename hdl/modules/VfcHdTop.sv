//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: VfcHdTop.sv
//
// Language: SystemVerilog 2013
//
// Targeted device:
//
//     - Vendor:  Intel FPGA (Altera)
//     - Model:   Arria V GX
//
// Description:
//
//     Top level HDL file targeted to the VFC-HD v3
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

import CKRSPkg::*;
import constants::*;



module VfcHdTop #(                           parameter     g_Synthesis = 1'b1)
   //========================================  I/O ports  =======================================//
   (
    // VME Interface:
    (* useioff = 1 *)(* chip_pin = "AE29" *) input         VmeAs_in,
    (* useioff = 1 *)(* chip_pin = "AW31,AU31,AV31,AG30,AH30,AD29" *)
    input  [ 5:0] VmeAm_ib6,
    (* useioff = 1 *)(* chip_pin = "AU28,AV28,AP28,AR28,AC27,AD27,AM28,AB27,AB28,AD28,AE28,AH28,AJ28,AK29,AL29,AF28,AG28,AB29,AC29,AN29,AP29,AN30,AP30,AT29,AU29,AU30,AV30,AR30,AT30,AK30,AL30" *)
    inout  [31:1] VmeA_iob31,
    (* useioff = 1 *)(* chip_pin = "AW30" *) inout         VmeLWord_ion,
    (* useioff = 1 *)(* chip_pin = "AK27" *) output        VmeAOe_oen,
    (* useioff = 1 *)(* chip_pin = "AJ27" *) output        VmeADir_o,
    (* useioff = 1 *)(* chip_pin = "AN22,AP22" *)
    input  [ 1:0] VmeDs_inb2,
    (* useioff = 1 *)(* chip_pin = "AW28" *) input         VmeWrite_in,
    (* useioff = 1 *)(* chip_pin = "AE22,AF22,AG23,AH23,AV21,AW21,AV22,AW22,AT22,AU22,AK23,AL23,AD22,AE23,AN23,AP23,AT23,AU23,AN24,AP24,AW23,AW24,AG24,AH24,AE24,AF24,AK24,AL24,AT24,AU24,AD23,AD24" *)
    inout  [31:0] VmeD_iob32,
    (* useioff = 1 *)(* chip_pin = "AW20" *) output        VmeDOe_oen,
    (* useioff = 1 *)(* chip_pin = "AW19" *) output        VmeDDir_o,
    (* useioff = 1 *)(* chip_pin = "AL22" *) output        VmeDtAckOe_o,
    (* useioff = 1 *)(* chip_pin = "AT20,AU20,AG22,AH22,AR21,AT21,AK22" *)
    output [ 7:1] VmeIrq_ob7,
    (* useioff = 1 *)(* chip_pin = "AN21" *) input         VmeIack_in,
    (* useioff = 1 *)(* chip_pin = "AM21" *) input         VmeIackIn_in,
    (* useioff = 1 *)(* chip_pin = "AK21" *) output        VmeIackOut_on,
    //// (* useioff = 1 *)(* chip_pin = "AL20" *) input         VmeSysClk_ik,
    (* useioff = 1 *)(* chip_pin = "AK20" *) input         VmeSysReset_irn,
    // SFP MGT Lanes:
    (* useioff = 1 *)(* chip_pin = "AW37" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  AppSfpRx1_ib, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AT39" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  AppSfpRx2_ib, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AP39" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  AppSfpRx3_ib, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AM39" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) input  AppSfpRx4_ib, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AU37" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output AppSfpTx1_ob, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AR37" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output AppSfpTx2_ob, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AN37" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output AppSfpTx3_ob, // Comment: Differential signal
    (* useioff = 1 *)(* chip_pin = "AL37" *)(* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *) output AppSfpTx4_ob, // Comment: Differential signal
    // FMC connector:
    (* chip_pin = "AL31" *)(* useioff = 1 *) output        FmcPgC2m_o,
    // HW-FMC-105-DEBUG header
    //    @TODO kill fmc debug header connection if not needed anymore
    //    There are 4 diodes:
    (* chip_pin = "T26"  *)(* useioff = 1 *) output FMCH_LedDS4,
    (* chip_pin = "T25"  *)(* useioff = 1 *) output FMCH_LedDS3,
    (* chip_pin = "M27"  *)(* useioff = 1 *) output FMCH_LedDS2,
    (* chip_pin = "N27"  *)(* useioff = 1 *) output FMCH_LedDS1,
    //    We use J1 connector pins 39, 37, 35, 33, 31, 29 to cast geographic
    //    address including parity - to check if I2C mux read correct
    //    data (GA4, GA3, GA2, GA1, GA0, GAP)
    (* chip_pin = "C27"  *)(* useioff = 1 *) output FMCH_GA4,
    (* chip_pin = "B27"  *)(* useioff = 1 *) output FMCH_GA3,
    (* chip_pin = "F22"  *)(* useioff = 1 *) output FMCH_GA2,
    (* chip_pin = "E22"  *)(* useioff = 1 *) output FMCH_GA1,
    (* chip_pin = "G26"  *)(* useioff = 1 *) output FMCH_GA0,
    (* chip_pin = "F26"  *)(* useioff = 1 *) output FMCH_GAP,

    // FMC Voltage Control
    (* chip_pin = "AU27" *)(* useioff = 1 *) output        VadjCs_o ,
    (* chip_pin = "AT27" *)(* useioff = 1 *) output        VadjSclk_ok,
    (* chip_pin = "AN27" *)(* useioff = 1 *) output        VadjDin_o,
    (* chip_pin = "AM27" *)(* useioff = 1 *) output        VfmcEnable_oen,
    // ADC Voltage monitoring:
    (* chip_pin = "AG27" *)(* useioff = 1 *) input         VAdcDout_i,
    (* chip_pin = "AC25" *)(* useioff = 1 *) output        VAdcDin_o,
    (* chip_pin = "AB25" *)(* useioff = 1 *) output        VAdcCs_o,
    (* chip_pin = "AL33" *)(* useioff = 1 *) output        VAdcSclk_ok ,
    // BST Interface:
    ////(* chip_pin = "AH27" *)(* useioff = 1 *) input         BstDataIn_i,
    ////(* chip_pin = "AU32" *)(* useioff = 1 *) input         CdrClkOut_ik,
    ////(* chip_pin = "AT32" *)(* useioff = 1 *) input         CdrDataOut_i,
    // Clocks Scheme:
    (* chip_pin = "AW29" *)(* useioff = 1 *) output        OeSi57x_oe,
    (* chip_pin = "AM33"  *)(* useioff = 1 *) input Si57xClk100MHz_ik,
    ////(* chip_pin = "AR6"  *)(* useioff = 1 *) input         ClkFbA_ik,
    ////(* chip_pin = "AP6"  *)(* useioff = 1 *) output        ClkFbA_ok,
    ////(* chip_pin = "AW4"  *)(* useioff = 1 *) input         ClkFbB_ik,
    ////(* chip_pin = "AV4"  *)(* useioff = 1 *) output        ClkFbB_ok,
    ////(* chip_pin = "AD20" *)(* useioff = 1 *) input         Clk20VCOx_ik,
    ////(* chip_pin = "AL34" *)(* useioff = 1 *) inout         PllRefSda_ioz,
    ////(* chip_pin = "AK33" *)(* useioff = 1 *) inout         PllRefScl_iokz,
    ////(* chip_pin = "AC21" *)(* useioff = 1 *) output        PllSourceMuxOut_ok,
    ////(* chip_pin = "AG32" *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD LVDS" *) input PllRefClkOut_ik,   //Comment: Differential reference clock for the MGT lines
    ////(* chip_pin = "AF8"  *)(* useioff = 1 *)(* altera_attribute = "-name IO_STANDARD LVDS" *) input Si57xClk100MHz_ik, //Comment: Differential reference clock for the MGT lines
    // I2C Mux & IO expanders:
    (* chip_pin = "AN25" *)(* useioff = 1 *) inout         I2cMuxSda_ioz,
    (* chip_pin = "AM25" *)(* useioff = 1 *) inout         I2cMuxScl_iokz,
    (* chip_pin = "AT25" *)(* useioff = 1 *) input         I2CIoExpIntApp12_in,
    (* chip_pin = "AR25" *)(* useioff = 1 *) input         I2CIoExpIntApp34_in,
    (* chip_pin = "AT26" *)(* useioff = 1 *) input         I2CIoExpIntBstEth_in,
    (* chip_pin = "AU26" *)(* useioff = 1 *) input         I2CIoExpIntBlmIn_in,
    (* chip_pin = "AK25" *)(* useioff = 1 *) input         I2CMuxIntLos_in,
    // DIP Switch (SW1):
    (* chip_pin = "T23,R23,N23,M23,K23,J23,J24,H24" *)(* useioff = 1 *)
    input  [ 8:1] DipSw_ib8, //Comment: contains also the part formely used as No GA
    // PCB Revision Resistor Network:
    (* chip_pin = "T21,R21,A24,A23,M22,L22,T22,R22" *)(* useioff = 1 *)
    input  [ 7:0] PcbRev_ib7,
    // TestIo MMCX Connectors:
    (* chip_pin = "AP20" *)(* useioff = 1 *) inout         TestIo1_io,
    (* chip_pin = "AH20" *)(* useioff = 1 *) inout         TestIo2_io,
    // GPIO LEMO Connectors:
    (* chip_pin = "AV24,AL26,AG21,AH21" *) (* useioff = 1 *)
    inout  [ 4:1] GpIo_iob4,
    // Miscellaneous:
    (* chip_pin = "AW25" *)(* useioff = 1 *) input         PushButtonN_in,
    (* chip_pin = "AV27" *)(* useioff = 1 *) inout         TempIdDq_ioz,
    (* chip_pin = "AD21" *)(* useioff = 1 *) output        ResetFpgaConfigN_orn
    );

   //=======================================  Declarations  =====================================//

   wire [  7:0]   AppVersion_b8, AppReleaseDay_b8, AppReleaseMonth_b8, AppReleaseYear_b8;
   wire [351:0]   AppInfo_b352;
   wire 	  AppReset_r, ResetRequest_r;
   wire 	  Sys125MhzClk_k;
   wire 	  Wb1SysToAppCyc, Wb1SysToAppStb, Wb1SysToAppWr, Wb1AppToSysAck;
   wire [ 24:0]   Wb1SysToAppAdr_b25;
   wire [ 31:0]   Wb1SysToAppDat_b32, Wb1AppToSysDat_b32;
   wire [  8:1]   Led_b8, LedMux_b8;
   wire [ 23:0]   InterruptRequest_b24;
   wire 	  GpIo1DirOut, GpIo1DirOutStatus, GpIo2DirOut, GpIo2DirOutStatus, GpIo34DirOut, GpIo34DirOutStatus;
   wire 	  GpIo1EnTerm, GpIo1EnTermStatus, GpIo2EnTerm, GpIo2EnTermStatus, GpIo3EnTerm, GpIo3EnTermStatus, GpIo4EnTerm, GpIo4EnTermStatus;
   wire [  4:1]   AppSfpPresent_b4, AppSfpTxFault_b4, AppSfpLos_b4, AppSfpTxDisable_b4, AppSfpRateSelect_b4, StatusAppSfpTxDisable_b4, StatusAppSfpRateSelect_b4;
   wire 	  I2cWbCyc, I2cWbStb, I2cWbWe, I2cWbAck;
   wire [ 11:0]   I2cWbAdr_b12;
   wire [  7:0]   I2cWbDatMoSi_b8, I2cWbDatMiSo_b8;

   //=====================================  Top Level Logic  ====================================//

   logic [4:0] 	  VmeGa_onb5;
   logic 	  VmeGaP_on;

   // FMC debug header connection - this is to see if I2C does something
   assign FMCH_LedDS1 = (I2cMuxSda_ioz) ? '1 : '0;
   assign FMCH_LedDS2 = (I2cMuxScl_iokz) ? '1 : '0;
   assign FMCH_LedDS3 = Led_b8[1];
   assign FMCH_LedDS4 = Led_b8[2];
   assign FMCH_GA4 = VmeGa_onb5[4];
   assign FMCH_GA3 = VmeGa_onb5[3];
   assign FMCH_GA2 = VmeGa_onb5[2];
   assign FMCH_GA1 = VmeGa_onb5[1];
   assign FMCH_GA0 = VmeGa_onb5[0];
   assign FMCH_GAP = VmeGaP_on;

   // CLOCKING SCHEME: ONE PLL FOR ALL GBT/SYSTEM AND OTHER PARTS
   // enable Si57x programmable oscillator - we use this for driving
   // GBT lines and all other stuff
   // INSTANTIATION OF THE WISHBONE IFACE, use 120MHz clock
   // as the others parts of thr
   ckrs_t ClkRs_ix, ClkRsGBT40MHz_ix, ClkRs20MHz_ix;
   logic 	  plllocked;
   logic 	  VMEReset, VMEAccess;


   // design reset is a combination of VME reset and push button (the
   // only one) on the VFC
   assign VMEReset = ~PushButtonN_in || ~VmeSysReset_irn;


   assign OeSi57x_oe = 1'b1;
   // takes 100MHz TX clock from external oscillator, and generates
   // 40MHz TXclock for GBT transceivers. THE SAME CLOCK OF 120MHZ HAS
   // TO DRIVE GBT CORE MGMT IFACE OTHERWISE LINK WILL FAIL TO LOCK!
   // BUT HAVING SINGLE CLOCK SOURCE MEANS THAT TX PATH OF
   // THE MOTOR CONTROLS IS COMPLETELY SYNCHRONOUS WITH ClkRx_ix! NO
   // NEED FOR CDCs. THis pll NEVER goes into reset through the VME
   // interface and has to providealways clock output regardless of
   // RST state. That's why reset signal is only casted from
   // VMEReset.
   // AT THE SAME TIME: RESET in ClkRs_ix.reset IS PROVIDED BY SYSTEM,
   // and is combination of VME request for reset _and_
   // VMEReset.
   gbt_pll
     i_gbt_pll
       (// Outputs
	// 120MHz GBT rated clocking (and all circuitry)
	.outclk_0				(ClkRs_ix.clk),
	// 40MHz GBT frame clock
	.outclk_1				(ClkRsGBT40MHz_ix.clk),
	// 20MHz VFC configuration of system part
	.outclk_2 (ClkRs20MHz_ix.clk),
	.locked				(plllocked),
	// Inputs - 100MHz input from Si57x oscillator:
	.refclk				(Si57xClk100MHz_ik),
	// this reset comes either from VME register space, _or_ from
	// VME system reset signal, but in any case is registered in
	// system part to Si57xClk100MHz_ik
	.rst				(VMEReset));

   logic [LED_GLICH_CATCHER_BITWIDTH-1:0] 	  ResetExtender_b;
   logic 	  ResetLong;
   // generate 'extended reset pulse' from the main 120MHz reset
   // (generated in system), this is because we need to propagate
   // reset to slower clock domains. Now, if we extend the counting to
   // some reasonable value, as e.g. 100ms, we can use this signal to
   // drive the RESET diode directly without further messing around
   // with MKOs. Let's use 'generic led on time' shared between IRQs
   // and blinks
   always_ff @(posedge ClkRsGBT40MHz_ix.clk or posedge AppReset_r)  begin
      if (AppReset_r)
	ResetExtender_b <= LED_GLICH_CATCHER_VALUE;
      else if (|ResetExtender_b)
	ResetExtender_b <= ResetExtender_b - (LED_GLICH_CATCHER_BITWIDTH)'(1);
   end
   // this gets 256 clock cycles reset signal
   assign ResetLong = |ResetExtender_b;
   // propagate reset into slower domains
   simple_shift
     #(.g_shiftLength			(8))
   i_simple_shift_125MHz
     (.data_o				(ClkRs_ix.reset),
      .clk_ik				(ClkRs_ix.clk),
      .data_i				(ResetLong));
   simple_shift
     #(.g_shiftLength			(8))
   i_simple_shift_40MHz
     (.data_o				(ClkRsGBT40MHz_ix.reset),
      .clk_ik				(ClkRsGBT40MHz_ix.clk),
      .data_i				(ResetLong));
   simple_shift
     #(.g_shiftLength			(8))
   i_simple_shift_20MHz
     (.data_o				(ClkRs20MHz_ix.reset),
      .clk_ik				(ClkRs20MHz_ix.clk),
      .data_i				(ResetLong));

   // led diodes muxing -
   // LED8 = PLL locked
   // LED7 = VME reset signal asserted (100ms anytime there's reset)
   // LED5 = VME access
   assign LedMux_b8 = {plllocked, ResetLong, 1'b0, VMEAccess, Led_b8[4:1]};

   //==== System Module ====//
   // VME access led hooks to VME DTACK signal:
   mko
     #(
       // Parameters
       .g_CounterBits			(LED_GLICH_CATCHER_BITWIDTH))
   i_mko
     (/*AUTOINST*/
      // Outputs
      .q_o				(VMEAccess),
      .q_on				(),
      // Inputs
      .ClkRs_ix				(ClkRsGBT40MHz_ix),
      .width_ib				(LED_GLICH_CATCHER_VALUE),
      .start_i				(VmeDtAckOe_o),
      .enable_i				('1));

   VfcHdSystem #(
		 .g_Synthesis                (g_Synthesis))
   i_VfcHdSystem (
		  // External Ports:
		  .VmeAs_in                   (VmeAs_in),
		  .VmeAm_ib6                  (VmeAm_ib6),
		  .VmeA_iob31                 (VmeA_iob31),
		  .VmeLWord_ion               (VmeLWord_ion),
		  .VmeAOe_oen                 (VmeAOe_oen),
		  .VmeADir_o                  (VmeADir_o),
		  .VmeDs_inb2                 (VmeDs_inb2),
		  .VmeWrite_in                (VmeWrite_in),
		  .VmeD_iob32                 (VmeD_iob32),
		  .VmeDOe_oen                 (VmeDOe_oen),
		  .VmeDDir_o                  (VmeDDir_o),
		  .VmeDtAckOe_o               (VmeDtAckOe_o),
		  .VmeIrq_ob7                 (VmeIrq_ob7),
		  .VmeIack_in                 (VmeIack_in),
		  .VmeIackIn_in               (VmeIackIn_in),
		  .VmeIackOut_on              (VmeIackOut_on),
		  .VmeSysReset_irn            (~VMEReset),
		  .I2cMuxSda_ioz              (I2cMuxSda_ioz),
		  .I2cMuxScl_iokz             (I2cMuxScl_iokz),
		  .I2CIoExpIntApp12_ian       (I2CIoExpIntApp12_in),
		  .I2CIoExpIntApp34_ian       (I2CIoExpIntApp34_in),
		  .I2CIoExpIntBstEth_ian      (I2CIoExpIntBstEth_in),
		  .I2CIoExpIntBlmIn_ian       (I2CIoExpIntBlmIn_in),
		  .I2CMuxIntLos_ian           (I2CMuxIntLos_in),
		  .VAdcDout_i                 (VAdcDout_i),
		  .VAdcDin_o                  (VAdcDin_o),
		  .VAdcCs_o                   (VAdcCs_o),
		  .VAdcSclk_ok                (VAdcSclk_ok),
		  .VadjCs_o                   (VadjCs_o),
		  .VadjSclk_ok                (VadjSclk_ok),
		  .VadjDin_o                  (VadjDin_o),
		  .PcbRev_ib7                 (PcbRev_ib7),
		  .TempIdDq_ioz               (TempIdDq_ioz),
		  .ResetFpgaConfigN_orn       (ResetFpgaConfigN_orn),
		  // System <-> Application Interface:
		  // !NOTE THAT Si57xClk100MHz_ik in system is driven by
		  // the same clock as all the rest of the design -
		  // i.e. clock used to go to GBT lines as well.
		  .ClkRs_ix (ClkRsGBT40MHz_ix),
		  .ClkRs20MHz_ix(ClkRs20MHz_ix),
		  .AppReset_oqr (AppReset_r),
		  //
		  .AppVersion_ib8             (AppVersion_b8),
		  .AppReleaseDay_ib8          (AppReleaseDay_b8),
		  .AppReleaseMonth_ib8        (AppReleaseMonth_b8),
		  .AppReleaseYear_ib8         (AppReleaseYear_b8),
		  .AppInfo_ib352              (AppInfo_b352),
		  .WbMasterCyc_oq             (Wb1SysToAppCyc),
		  .WbMasterStb_oq             (Wb1SysToAppStb),
		  .WbMasterAdr_oqb25          (Wb1SysToAppAdr_b25),
		  .WbMasterWr_oq              (Wb1SysToAppWr),
		  .WbMasterDat_oqb32          (Wb1SysToAppDat_b32),
		  .WbMasterDat_ib32           (Wb1AppToSysDat_b32),
		  .WbMasterAck_i              (Wb1AppToSysAck),
		  .WbSlaveCyc_i               ('0),
		  .WbSlaveStb_i               ('0),
		  .WbSlaveAdr_ib25            ('0),
		  .WbSlaveWr_i                ('0),
		  .WbSlaveDat_ib32            ('0),
		  .WbSlaveDat_ob32            (),
		  .WbSlaveAck_o               (),
		  .Led_ib8                    (LedMux_b8),
		  .InterruptRequest_ib24      (InterruptRequest_b24),
		  .GpIo1DirOut_i              (GpIo1DirOut),
		  .GpIo1DirOutStatus_o        (GpIo1DirOutStatus),
		  .GpIo2DirOut_i              (GpIo2DirOut),
		  .GpIo2DirOutStatus_o        (GpIo2DirOutStatus),
		  .GpIo34DirOut_i             (GpIo34DirOut),
		  .GpIo34DirOutStatus_o       (GpIo34DirOutStatus),
		  .GpIo1EnTerm_i              (GpIo1EnTerm),
		  .GpIo1EnTermStatus_o        (GpIo1EnTermStatus),
		  .GpIo2EnTerm_i              (GpIo2EnTerm),
		  .GpIo2EnTermStatus_o        (GpIo2EnTermStatus),
		  .GpIo3EnTerm_i              (GpIo3EnTerm),
		  .GpIo3EnTermStatus_o        (GpIo3EnTermStatus),
		  .GpIo4EnTerm_i              (GpIo4EnTerm),
		  .GpIo4EnTermStatus_o        (GpIo4EnTermStatus),
		  .AppSfp1Present_oq          (AppSfpPresent_b4[1]),
		  .AppSfp1TxFault_oq          (AppSfpTxFault_b4[1]),
		  .AppSfp1Los_oq              (AppSfpLos_b4[1]),
		  .AppSfp1TxDisable_i         (AppSfpTxDisable_b4[1]),
		  .AppSfp1RateSelect_i        (AppSfpRateSelect_b4[1]),
		  .StatusAppSfp1TxDisable_oq  (StatusAppSfpTxDisable_b4[1]),
		  .StatusAppSfp1RateSelect_oq (StatusAppSfpRateSelect_b4[1]),
		  .AppSfp2Present_oq          (AppSfpPresent_b4[2]),
		  .AppSfp2TxFault_oq          (AppSfpTxFault_b4[2]),
		  .AppSfp2Los_oq              (AppSfpLos_b4[2]),
		  .AppSfp2TxDisable_i         (AppSfpTxDisable_b4[2]),
		  .AppSfp2RateSelect_i        (AppSfpRateSelect_b4[2]),
		  .StatusAppSfp2TxDisable_oq  (StatusAppSfpTxDisable_b4[2]),
		  .StatusAppSfp2RateSelect_oq (StatusAppSfpRateSelect_b4[2]),
		  .AppSfp3Present_oq          (AppSfpPresent_b4[3]),
		  .AppSfp3TxFault_oq          (AppSfpTxFault_b4[3]),
		  .AppSfp3Los_oq              (AppSfpLos_b4[3]),
		  .AppSfp3TxDisable_i         (AppSfpTxDisable_b4[3]),
		  .AppSfp3RateSelect_i        (AppSfpRateSelect_b4[3]),
		  .StatusAppSfp3TxDisable_oq  (StatusAppSfpTxDisable_b4[3]),
		  .StatusAppSfp3RateSelect_oq (StatusAppSfpRateSelect_b4[3]),
		  .AppSfp4Present_oq          (AppSfpPresent_b4[4]),
		  .AppSfp4TxFault_oq          (AppSfpTxFault_b4[4]),
		  .AppSfp4Los_oq              (AppSfpLos_b4[4]),
		  .AppSfp4TxDisable_i         (AppSfpTxDisable_b4[4]),
		  .AppSfp4RateSelect_i        (AppSfpRateSelect_b4[4]),
		  .StatusAppSfp4TxDisable_oq  (StatusAppSfpTxDisable_b4[4]),
		  .StatusAppSfp4RateSelect_oq (StatusAppSfpRateSelect_b4[4]),
		  .VmeGa_onb5 (VmeGa_onb5),
		  .VmeGaP_on (VmeGaP_on),
		  .I2cWbCyc_i                 (I2cWbCyc),
		  .I2cWbStb_i                 (I2cWbStb),
		  .I2cWbWe_i                  (I2cWbWe),
		  .I2cWbAdr_ib12              (I2cWbAdr_b12),
		  .I2cWbDat_ib8               (I2cWbDatMoSi_b8),
		  .I2cWbDat_ob8               (I2cWbDatMiSo_b8),
		  .I2cWbAck_o                 (I2cWbAck));


   //==== Application Module ====//

   VfcHdApplication i_VfcHdApplication (
					// External Ports:
					.AppSfpRx_ib4               ({AppSfpRx4_ib,AppSfpRx3_ib,AppSfpRx2_ib,AppSfpRx1_ib}),
					.AppSfpTx_ob4               ({AppSfpTx4_ob,AppSfpTx3_ob,AppSfpTx2_ob,AppSfpTx1_ob}),
					.TestIo1_io                 (TestIo1_io),
					.TestIo2_io                 (TestIo2_io),
					.VfmcEnable_oen             (VfmcEnable_oen),
					.FmcPgC2m_o                 (FmcPgC2m_o),
					.DipSw_ib8                  (DipSw_ib8), //Comment: Contains also the part formely used as No GA
					.ClkRs_ix (ClkRs_ix),
					.ClkRsGBT40MHz_ix (ClkRsGBT40MHz_ix),
					.GpIo_iob4                  (GpIo_iob4),
					// Application <-> System Interface:
					.AppVersion_ob8             (AppVersion_b8),
					.AppReleaseDay_ob8          (AppReleaseDay_b8),
					.AppReleaseMonth_ob8        (AppReleaseMonth_b8),
					.AppReleaseYear_ob8         (AppReleaseYear_b8),
					.AppInfo_ob352              (AppInfo_b352),
					.WbMasterCyc_i              (Wb1SysToAppCyc),
					.WbMasterStb_i              (Wb1SysToAppStb),
					.WbMasterAdr_ib25           (Wb1SysToAppAdr_b25),
					.WbMasterWr_i               (Wb1SysToAppWr),
					.WbMasterDat_ib32           (Wb1SysToAppDat_b32),
					.WbMasterDat_ob32           (Wb1AppToSysDat_b32),
					.WbMasterAck_o              (Wb1AppToSysAck),
					.Led_ob8                    (Led_b8),
					.InterruptRequest_ob24      (InterruptRequest_b24),
					.GpIo1DirOut_o              (GpIo1DirOut),
					.GpIo1DirOutStatus_i        (GpIo1DirOutStatus),
					.GpIo2DirOut_o              (GpIo2DirOut),
					.GpIo2DirOutStatus_i        (GpIo2DirOutStatus),
					.GpIo34DirOut_o             (GpIo34DirOut),
					.GpIo34DirOutStatus_i       (GpIo34DirOutStatus),
					.GpIo1EnTerm_o              (GpIo1EnTerm),
					.GpIo1EnTermStatus_i        (GpIo1EnTermStatus),
					.GpIo2EnTerm_o              (GpIo2EnTerm),
					.GpIo2EnTermStatus_i        (GpIo2EnTermStatus),
					.GpIo3EnTerm_o              (GpIo3EnTerm),
					.GpIo3EnTermStatus_i        (GpIo3EnTermStatus),
					.GpIo4EnTerm_o              (GpIo4EnTerm),
					.GpIo4EnTermStatus_i        (GpIo4EnTermStatus),
					.AppSfp1Present_iq          (AppSfpPresent_b4[1]),
					.AppSfp1TxFault_iq          (AppSfpTxFault_b4[1]),
					.AppSfp1Los_iq              (AppSfpLos_b4[1]),
					.AppSfp1TxDisable_o         (AppSfpTxDisable_b4[1]),
					.AppSfp1RateSelect_o        (AppSfpRateSelect_b4[1]),
					.StatusAppSfp1TxDisable_iq  (StatusAppSfpTxDisable_b4[1]),
					.StatusAppSfp1RateSelect_iq (StatusAppSfpRateSelect_b4[1]),
					.AppSfp2Present_iq          (AppSfpPresent_b4[2]),
					.AppSfp2TxFault_iq          (AppSfpTxFault_b4[2]),
					.AppSfp2Los_iq              (AppSfpLos_b4[2]),
					.AppSfp2TxDisable_o         (AppSfpTxDisable_b4[2]),
					.AppSfp2RateSelect_o        (AppSfpRateSelect_b4[2]),
					.StatusAppSfp2TxDisable_iq  (StatusAppSfpTxDisable_b4[2]),
					.StatusAppSfp2RateSelect_iq (StatusAppSfpRateSelect_b4[2]),
					.AppSfp3Present_iq          (AppSfpPresent_b4[3]),
					.AppSfp3TxFault_iq          (AppSfpTxFault_b4[3]),
					.AppSfp3Los_iq              (AppSfpLos_b4[3]),
					.AppSfp3TxDisable_o         (AppSfpTxDisable_b4[3]),
					.AppSfp3RateSelect_o        (AppSfpRateSelect_b4[3]),
					.StatusAppSfp3TxDisable_iq  (StatusAppSfpTxDisable_b4[3]),
					.StatusAppSfp3RateSelect_iq (StatusAppSfpRateSelect_b4[3]),
					.AppSfp4Present_iq          (AppSfpPresent_b4[4]),
					.AppSfp4TxFault_iq          (AppSfpTxFault_b4[4]),
					.AppSfp4Los_iq              (AppSfpLos_b4[4]),
					.AppSfp4TxDisable_o         (AppSfpTxDisable_b4[4]),
					.AppSfp4RateSelect_o        (AppSfpRateSelect_b4[4]),
					.StatusAppSfp4TxDisable_iq  (StatusAppSfpTxDisable_b4[4]),
					.StatusAppSfp4RateSelect_iq (StatusAppSfpRateSelect_b4[4]),
					.I2cWbCyc_o                 (I2cWbCyc),
					.I2cWbStb_o                 (I2cWbStb),
					.I2cWbWe_o                  (I2cWbWe),
					.I2cWbAdr_ob12              (I2cWbAdr_b12),
					.I2cWbDat_ob8               (I2cWbDatMoSi_b8),
					.I2cWbDat_ib8               (I2cWbDatMiSo_b8),
					.I2cWbAck_i (I2cWbAck));

endmodule
