//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) April 2018 CERN

//-----------------------------------------------------------------------------
// @file GLITCH_CATCH.SV
// @brief detects glitch on signal and blankens the output signal
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 12 April 2018
// @details g_CounterBits is bitwidth of pulse generating
// counter. Setup width_ib for a value within the g_CounterBits to
// generate appropriate pulse length. The length is not exact, few
// cycles are added  here and there due to the signal processing, but
// when a single transition occurs, output data are glitch free
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module glitch_catch
  #(
    parameter   g_CounterBits  = 4
    )
   (input ckrs_t ClkRs_ix,
    // input signal
    input logic 		    q_i,
    // sets up whethe blanking should generate '1' or '0' in the
    // signal. That depends on what user wants to do with that
    input 			    blankToLogicLow_i,
    // the length of the counting of the MKO. Each clock cycle is one count
    input logic [g_CounterBits-1:0] width_ib,
    output logic 		    q_o
    );


   logic 			    blanking, blanking_n, data_o;
   logic 			    rising_o, falling_o;


   get_edge
     i_get_edge
       (
	// Outputs
	.rising_o				(rising_o),
	.falling_o			(falling_o),
	.data_o				(data_o),
	// Inputs
	.ClkRs_ix				(ClkRs_ix),
	.data_i				(q_i));

   // any time there's transition, we generate blanking signal
   mko
     #(
       // Parameters
       .g_CounterBits			(g_CounterBits))
   i_mko
     (
      // Outputs
      .q_o				(blanking),
      .q_on				(blanking_n),
      // Inputs
      .ClkRs_ix				(ClkRs_ix),
      .enable_i ('1),
      .width_ib				(width_ib[g_CounterBits-1:0]),
      .start_i				(rising_o | falling_o));


   logic 			    q_a, data;

   always_ff @(posedge ClkRs_ix.clk) data <= data_o;


   always_comb begin
      if (blankToLogicLow_i && |width_ib)
	q_a <= data & blanking_n;
      else if (|width_ib)
	q_a <= data | blanking;
      else
	q_a <= data;
   end

   // and now we have to combine blanking signal with
   // original. depends whether blanking should generate logic '1' or
   // logic '0'
   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset) begin
      if (ClkRs_ix.reset) begin
	 q_o <= '0;
      end else begin
	 q_o <= q_a;

      end
   end

endmodule // glitch_catch
