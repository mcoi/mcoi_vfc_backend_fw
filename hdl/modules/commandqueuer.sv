//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) June 2017 CERN

//-----------------------------------------------------------------------------
// @file commandqueuer.sv
// @brief implements queue for stepper motor commands
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 28 June 2017
// @details The module implements a queue of the commands. This is an
// equivalent of FIFO, where we store all the commands. The upper
// entity can issue the commands into the command queue by presenting
// the command on the ICmdParams_x port while setting a bit DoQueue in
// ITriggerParams_x to '1'. This will make a record in the command queue. Master
// can as well reset the queue completely by setting ResetQueue bit of
// ITriggerParams_x to '1'. By issuing '1' on Done_i the master tells to command
// queuer, that the command currently present at OCmdParams_x was
// performed. This in turn shifts OCmdParams_x to next command in the
// queue, or to ICmdParams_x, if the queue was emptied.
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import MCPkg::*;
import CKRSPkg::*;


module commandqueuer
  #(
    // defines maximum length of the command queue
    parameter QUEUELENGTH = 8
    )

   (   // clock reset combo
       input 	   ckrs_t ClkRs_ix,
       input logic Done_i,
       input 	   motorcommand_t ITriggerParams_x,
       output 	   motorcommand_t OTriggerParams_x,
       input 	   command_t ICmdParams_x,
       output 	   command_t OCmdParams_x,
       output 	   queuestatus_t QueueStatus_x
       );


   assert property (@(posedge ClkRs_ix.clk)
		    {ITriggerParams_x.StartMove} |-> {OTriggerParams_x.StartMove}) else
     $error("When triggered by StartMove, output trigger should follow");

   // instantiate interfaces for FIFO
   data_x #(.g_DataWidth($bits(command_t))) wdata_ix(.ClkRs_ix(ClkRs_ix));
   data_x #(.g_DataWidth($bits(command_t))) rdata_ox(.ClkRs_ix(ClkRs_ix));
   logic 	   FifoFull_o, FifoEmpty_o;
   command_t queued_in, queued_out;
   logic [$clog2(QUEUELENGTH):0] QueuedItems;
   logic 			   QueueFull, QueueEmpty;

   initial begin
      assert ($clog2(QUEUELENGTH) < $bits(QueueStatus_x.QueueItems_u7)) else
	$error("The VME counter of QUEUELENGTH is not wide enough");
   end


   assign QueueFull = (QueuedItems == QUEUELENGTH);
   assign QueueEmpty = (QueuedItems == 0);

   always_ff @(posedge ClkRs_ix.clk or posedge ClkRs_ix.reset)  begin
      if (ClkRs_ix.reset) begin
	 QueuedItems <= 0;
      end else begin
	 if (ITriggerParams_x.ResetQueue) begin
	    QueuedItems <= 0;
	 end else if (ITriggerParams_x.DoQueue && !QueueFull) begin
	    QueuedItems <= QueuedItems + ($clog2(QUEUELENGTH)+1)'(1);
	 end else if (Done_i && !QueueEmpty) begin
	    QueuedItems <= QueuedItems - ($clog2(QUEUELENGTH)+1)'(1);
	 end
      end
   end

   always_comb begin
      queued_out = rdata_ox.data;
   end

   always_comb begin
      OTriggerParams_x = ITriggerParams_x;

      if (FifoEmpty_o) begin
	 OCmdParams_x = ICmdParams_x;
	 QueueStatus_x.QueueItems_u7 = '0;
      end else begin
	 OCmdParams_x = queued_out;
	 QueueStatus_x.QueueItems_u7 = ($bits(QueueStatus_x.QueueItems_u7))'(QueuedItems);
	 if ((queued_out.globaltrigger == 2'b0 &&
	     Done_i) || ITriggerParams_x.StartMove)
	   OTriggerParams_x.StartMove = 1;
	 else
	   OTriggerParams_x.StartMove = 0;
      end

      // nonblocking assignments
      QueueStatus_x.QueueFull = FifoFull_o;

      // prepare flow vector
      queued_in = ICmdParams_x;

      // now fifo feeders
      wdata_ix.data = queued_in;
      wdata_ix.enable = ITriggerParams_x.DoQueue;
      rdata_ox.enable = Done_i;

   end
   // fifo serves as command queue. *ASSURE THAT THIS THING INSTANTIATES AS RAM
   // BLOCK!*
   fifo
     #(
       // Parameters
       .g_FifoWidth			($clog2(QUEUELENGTH)))
   i_fifo (
	   .FifoReset_i			(ITriggerParams_x.ResetQueue),
	   /*AUTOINST*/
	   // Interfaces
	   .wdata_ix			(wdata_ix),
	   .rdata_ox			(rdata_ox),
	   // Outputs
	   .FifoFull_o			(FifoFull_o),
	   .FifoEmpty_o			(FifoEmpty_o));


endmodule
