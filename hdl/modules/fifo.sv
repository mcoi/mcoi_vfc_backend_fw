//-----------------------------------------------------------------------------
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor
// Boston, MA  02110-1301, USA.
//
// You can dowload a copy of the GNU General Public License here:
// http://www.gnu.org/licenses/gpl.txt
//
// Copyright (c) November 2017 CERN

//-----------------------------------------------------------------------------
// @file FIFO.SV
// @brief Generic synchronous fifo implementation
// @author Dr. David Belohrad  <david@belohrad.ch>, CERN
// @date 14 November 2017
// @details
//
//
// @platform Altera Quartus
// @standard IEEE 1800-2012
//-----------------------------------------------------------------------------

import CKRSPkg::*;


module fifo
  #(
    // bus width of the FIFO interface
    parameter   g_FifoWidth  = 4
    )
   (
    // input data interface. Use data enable to write into the FIFO
    data_x wdata_ix,
    // output data interface. Use its data enable to fetch next sample
    data_x rdata_ox,
    // '1' to reset the FIFO
    input logic FifoReset_i,
    // '1' when fifo is full
    output logic FifoFull_o,
    // '1' when fifo is empty
    output logic FifoEmpty_o
    );

   // write and read interfaces
   mem_x #(.g_AddrWidth(g_FifoWidth),
	   .g_DataWidth($size(wdata_ix.data)))
   rmembus_ox(.ClkRs_ix(rdata_ox.ClkRs_ix));

   mem_x #(.g_AddrWidth(g_FifoWidth),
	   .g_DataWidth($size(wdata_ix.data)))
   wmembus_ix(.ClkRs_ix(rdata_ox.ClkRs_ix)) ;

   // few asserts to get the things right from the beginning
   initial begin

      assert($bits(wdata_ix.data) == $bits(rdata_ox.data)) else begin
	 $error("Bus width of input and output interfaces must be equal");
      end
   end


   always_comb begin
      assert(wdata_ix.ClkRs_ix.clk == rdata_ox.ClkRs_ix.clk) else begin
	 $display("Instantiated fifo with data bus %d bits and address %d bits",
		  $bits(wdata_ix.data),
		  g_FifoWidth);
	 $display("Output data bus %d bits and address %d bits",
		  $bits(rdata_ox.data),
		  g_FifoWidth);
	 $display("Sizes: %d, %d",
		  $size(rmembus_ox.datain),
		  $size(wmembus_ix.datain));
	 $error("Single clock source has to be used for both interfaces");
      end
   end

   logic [g_FifoWidth:0] ReadCnt, WriteCnt;

   // NOTE the fancy g_FifoWidth'(1) size casting. That's because if
   // we would specify just count + 1, the '1' is signed integer of
   // 32bits, hence count would be casted first to 32bits signed, then
   // operator '+' would add them, and then the low gFifoWidth:0 bits
   // would be stripped away, which of course leads to fail. By
   // resizing '1' straight away to g_FifoWidth we get correct counting
   property getwritecount;
      logic [g_FifoWidth:0] count;
      @(posedge wdata_ix.ClkRs_ix.clk)
	(wdata_ix.enable & !FifoFull_o,count=WriteCnt) |=>
			    (WriteCnt == count + g_FifoWidth'(1));
   endproperty // getwritecount
   assert property (getwritecount) else $error("Write counter fails");

   property getreadcount;
      logic [g_FifoWidth:0] count;
      @(posedge rdata_ox.ClkRs_ix.clk)
	(rdata_ox.enable & !FifoEmpty_o,count=ReadCnt) |=>
			      (ReadCnt == count+ g_FifoWidth'(1));
   endproperty // getreadcount
   assert property (getreadcount) else $error("Read counter fails");


   always_ff @(posedge wdata_ix.ClkRs_ix.clk or posedge wdata_ix.ClkRs_ix.reset) begin
      if (wdata_ix.ClkRs_ix.reset) begin
	 ReadCnt <= '0;
	 WriteCnt <= '0;
      end else begin
	 if (FifoReset_i) begin
	    ReadCnt <= '0;
	    WriteCnt <= '0;
	 end else begin
	    if (wdata_ix.enable && !FifoFull_o)
	      WriteCnt <= WriteCnt + (g_FifoWidth)'(1);
	    if (rdata_ox.enable && !FifoEmpty_o)
	      ReadCnt <= ReadCnt + (g_FifoWidth)'(1);
	 end
      end
   end // always_ff @ (posedge wdata_ix.ClkRs_ix.clk or posedge
   // wdata_ix.ClkRs_ix.reset)

   always_comb begin
      wmembus_ix.datain = wdata_ix.data;
      wmembus_ix.write = wdata_ix.enable && !FifoFull_o;
      wmembus_ix.addrbus = WriteCnt[g_FifoWidth-1:0];
      rmembus_ox.addrbus = ReadCnt[g_FifoWidth-1:0];
      rmembus_ox.write = 0;
      rdata_ox.data = rmembus_ox.dataout;
   end

   always_comb begin
      FifoEmpty_o = (ReadCnt === WriteCnt);
      FifoFull_o = (WriteCnt[g_FifoWidth] != ReadCnt[g_FifoWidth]) &&
		   (WriteCnt[g_FifoWidth-1:0] == ReadCnt[g_FifoWidth-1:0]);

   end



   dpram_single_clock
     #(
       // Parameters
       .g_AddrWidth			(g_FifoWidth),
       .g_DataWidth			($size(wdata_ix.data)))
     i_dpram_single_clock (.*);







endmodule // fifo
