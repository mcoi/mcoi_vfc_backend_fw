# ##########################################################
#   System timing constraints for VFC-HD
# ##########################################################

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Do not edit this part unless you are completely sure you know what you are doing.
# If you need to add/modify some constraints concerning the Application part, please do it in
#   the "Application timing constraints" section bellow.
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


# ----------------------------------------------------------
#   Create Clocks
# ----------------------------------------------------------

# 100MHz Si57x default clock
create_clock -name Si57xClk100MHz_ik -period 10ns [get_ports {Si57xClk100MHz_ik}]
# 40 MHz VME clock
# create_clock -name cc_VmeClock -period 25ns [get_ports {VmeSysClk_ik}]
# 20 MHz clock from OSC3
# create_clock -name cc_Clk20VCOx -period 50ns [get_ports {Clk20VCOx_ik}]
# 160 MHz from BST
# create_clock -name cc_CdrClkOut -period 6.25ns [get_ports {CdrClkOut_ik}]
# 120 MHz Differential reference clock for the MGT lines
# create_clock -name cc_PllRefClkOut -period 8.333ns [get_ports {PllRefClkOut_ik}]

set cc_SystemClock Si57xClk100MHz_ik

# ----------------------------------------------------------
#   Clock Groups
# ----------------------------------------------------------

set_clock_groups -asynchronous -group {Si57xClk100MHz_ik}
# set_clock_groups -asynchronous -group {cc_VmeClock}
# set_clock_groups -asynchronous -group {cc_Clk20VCOx}
# set_clock_groups -asynchronous -group {cc_CdrClkOut}
# set_clock_groups -asynchronous -group {cc_PllRefClkOut}

# ----------------------------------------------------------
#   Asynchronous Connections
# ----------------------------------------------------------

set_false_path -from [get_ports {I2CIoExpIntApp12_in}]
set_false_path -from [get_ports {I2CIoExpIntApp34_in}]
set_false_path -from [get_ports {I2CIoExpIntBlmIn_in}]
set_false_path -from [get_ports {I2CIoExpIntBstEth_in}]
set_false_path -from [get_ports {I2CMuxIntLos_in}]
set_false_path -from [get_ports {PcbRev_ib7[*]}]
set_false_path -from [get_ports {PushButtonN_in}]
set_false_path -from [get_ports {DipSw_ib8[*]}]
set_false_path -to [get_ports {ResetFpgaConfigN_orn}]
# set_false_path -from [get_ports {FmcPgM2c_i}]
# set_false_path -from [get_ports {FmcPrsntM2c_in}]
set_false_path -to [get_ports {FmcPgC2m_o}]
set_false_path -to [get_ports {VfmcEnable_oen}]
set_false_path -from [get_ports {TempIdDq_ioz}]
set_false_path -from [get_ports {I2cMuxSda_ioz}]


# ----------------------------------------------------------
#   Input/Output Delays
# ----------------------------------------------------------

set c_SpiOutputDelay -2ns

# VME connections
# !!! VME<->WB interface misbehave when these are enabled
# set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeA_iob31[*]}]
# set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeAm_ib6[*]}]
# set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeAs_in}]
# set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeD_iob32[*]}]
# set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeDs_inb2[*]}]
# set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeIackIn_in}]
# set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeIack_in}]
# set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeLWord_ion}]
# set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeSysReset_irn}]
# set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeWrite_in}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeADir_o}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeAOe_oen}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeA_iob31[*]}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeDDir_o}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeDOe_oen}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeD_iob32[*]}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeDtAckOe_o}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeIackOut_on}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeIrq_ob7[*]}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VmeLWord_ion}]

# FMC Voltage Control
set_output_delay -clock [get_clocks $cc_SystemClock] $c_SpiOutputDelay [get_ports {VadjCs_o}]
set_output_delay -clock [get_clocks $cc_SystemClock] $c_SpiOutputDelay [get_ports {VadjDin_o}]
set_output_delay -clock [get_clocks $cc_SystemClock] $c_SpiOutputDelay [get_ports {VadjSclk_ok}]

# ADC Voltage monitoring
set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {VAdcDout_i}]
set_output_delay -clock [get_clocks $cc_SystemClock] $c_SpiOutputDelay [get_ports {VAdcCs_o}]
set_output_delay -clock [get_clocks $cc_SystemClock] $c_SpiOutputDelay [get_ports {VAdcDin_o}]
set_output_delay -clock [get_clocks $cc_SystemClock] $c_SpiOutputDelay [get_ports {VAdcSclk_ok}]

# I2C Mux & IO expanders
set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {I2cMuxScl_iokz}]
set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {I2cMuxSda_ioz}]
set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {I2cMuxScl_iokz}]
set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {I2cMuxSda_ioz}]

# WR PROM
# set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {WrPromSda_io}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {WrPromScl_ok}]
# set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {WrPromSda_io}]

# Miscellaneous
set_input_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {TempIdDq_ioz}]
set_output_delay -clock [get_clocks $cc_SystemClock] 0 [get_ports {TempIdDq_ioz}]

# BST
# set_input_delay -clock [get_clocks {cc_CdrClkOut}] -clock_fall 0 [get_ports {CdrDataOut_i}]


# ----------------------------------------------------------
#   TODOs
# ----------------------------------------------------------

#    // BST Interface:
#    input         BstDataIn_i,



# ##########################################################
#   Application timing constraints
# ##########################################################

# Here you can add/modify constraints concerning the Application part


# ----------------------------------------------------------
#   Create Clocks
# ----------------------------------------------------------

# create_clock -name cc_ClkFbA -period 6.25ns [get_ports {ClkFbA_ik}]
# create_clock -name cc_ClkFbB -period 6.25ns [get_ports {ClkFbB_ik}]
create_clock -name AppSfpGbtBank_GbtLink_RxRecClk1 -period 8.333ns [get_pins {*|i_AppSfpGbtBank|*|*|*|*|*|*|*|*|*|ch[0].inst_av_pcs_ch|*|*|rcvdclkpma}]
create_clock -name AppSfpGbtBank_GbtLink_RxRecClk2 -period 8.333ns [get_pins {*|i_AppSfpGbtBank|*|*|*|*|*|*|*|*|*|ch[1].inst_av_pcs_ch|*|*|rcvdclkpma}]
create_clock -name AppSfpGbtBank_GbtLink_RxRecClk3 -period 8.333ns [get_pins {*|i_AppSfpGbtBank|*|*|*|*|*|*|*|*|*|ch[2].inst_av_pcs_ch|*|*|rcvdclkpma}]
create_clock -name AppSfpGbtBank_GbtLink_RxRecClk4 -period 8.333ns [get_pins {*|i_AppSfpGbtBank|*|*|*|*|*|*|*|*|*|ch[3].inst_av_pcs_ch|*|*|rcvdclkpma}]

# finalize clock creation
derive_pll_clocks -create_base_clocks
derive_clock_uncertainty


# ----------------------------------------------------------
#   Clock Groups
# ----------------------------------------------------------

# set_clock_groups -asynchronous -group [get_clocks {cc_ClkFbA}]
# set_clock_groups -asynchronous -group [get_clocks {cc_ClkFbB}]
set_clock_groups -asynchronous -group [get_clocks {AppSfpGbtBank_GbtLink_RxRecClk1}]
set_clock_groups -asynchronous -group [get_clocks {AppSfpGbtBank_GbtLink_RxRecClk2}]
set_clock_groups -asynchronous -group [get_clocks {AppSfpGbtBank_GbtLink_RxRecClk3}]
set_clock_groups -asynchronous -group [get_clocks {AppSfpGbtBank_GbtLink_RxRecClk4}]


# ----------------------------------------------------------
#   Asynchronous Connections
# ----------------------------------------------------------



# ----------------------------------------------------------
#   Input/Output Delays
# ----------------------------------------------------------
