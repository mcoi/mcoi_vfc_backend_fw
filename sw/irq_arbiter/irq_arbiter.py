#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2006 David Belohrad
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street,
# Fifth Floor, Boston, MA  02110-1301, USA.
#
# You can dowload a copy of the GNU General Public License here:
# http://www.gnu.org/licenses/gpl.txt
#
# Author: David Belohrad
# Email:  david.belohrad@cern.ch
#

"""
this python snippet calculates the content of the IRQ  encoder
memory. The priority encoding works by using a ROM memory, which picks
each time new IRQ which should be handled based on actual overflowing
counter, which gives the highest priority to a given interrupt.

Functionality is as follows:

input to the calculation is number of IRQ queues to be handled. In
case of VFC this is 4, because we have 4 SFP links. That number gives
as well a maximum counting to a free running counter. In this case 2
bits, as we need a counter to count from 0 to 3 and overlap.

The ROM address bus is then composed from 2 MSBs of free running
counter, and 4 bits of IRQ line requests. This gives totally 6 bits of
address bus and 64 combinations of priorities

now, the priority is handled as follows: imagine that in current cycle
the free running counter is on 0 and IRQ 2 and 3 rise the flag. The
ROM content will be such, that next IRQ risen in _actual_ state is 2.

In the next cycle the counter increases from 0 to 1, IRQ 2 is handled
and there is still irq 3 pending, irq 3 is chosen as next IRQ source.

Let's consider another situation: counter is at 2, and IRQ 3 and 1 is
risen. The next IRQ should resolve in 3 being the first (because is it
first next trigger from actual counter), and in the following cycle
the next should be 1 even if the counter will be at 3.

If NONE of the 4 inputs is risen, the resulting next vector is ZERO,
but that should not cause interrupt to be risen as this state is
invalid. Having multiple IRQ sources triggered is perfectly fine condition.
"""

import math
import tabulate

number_of_inputs = 4
counter_bits = math.ceil(math.log(number_of_inputs, 2))
links_mask = int(math.pow(2, number_of_inputs) - 1)
counter_mask = int(math.pow(2, counter_bits) - 1) << number_of_inputs

print("Total number of address bits: %d" % (number_of_inputs +
                                            counter_bits))
print("Links mask: 0x%.2x" % (links_mask))
print("Counter mask: 0x%.2x" % (counter_mask))
print("Total address space: 0x%.2x" % (links_mask | counter_mask))
content = []

bitvec = (1, 2, 4, 8, 1, 2, 4, 8)
reve = { 1: 0,
         2: 1,
         4: 2,
         8: 3}

# let's iterate over the space:
for address in range(int(
        math.pow(2, number_of_inputs +
                 counter_bits))):
    # running through the entire address space we decode what is
    # counter and what is IRQ link triggered:
    link_irqs = address & links_mask
    counter = (address & counter_mask) >> number_of_inputs
    # resolve IRQ: depending of current top irq and triggered IRQs we
    # resolve the output
    dat = "x"
    # if the current IRQ is 0, that means that there is no IRQ
    # triggered. In this case we return as next IRQ value of current
    # counter
    if link_irqs == 0:
        dat = counter
    else:
        # at least one IRQ triggered, let's find the priority, we have
        # to find first bit occuring in curprio:
        curprio = bitvec[counter:]
        for actprio in curprio:
            if link_irqs & actprio:
                dat = reve[actprio]
                break


    content.append( (address, counter, link_irqs,
                     "{0:04b}".format(link_irqs),
                     dat, dat+1) )
print tabulate.tabulate(content, headers=["Address",
                                          "Top-irq",
                                          "Current Irqs",
                                          "Exploded (link4 .. 1)",
                                          "Resolved IRQ",
                                          "Link IRQ to send"])

    # write memory content as 'case-style rom'

with open("irq_rom.sv", "wt") as f:
    f.write("""
// generated ROM irq resolver, see irq_arbiter.py
// connect address[%d:%d] to free running counter
// connect address[%d:0] to next irq source
module irq_rom (
input  wire [%d:0] address , // Address input
output reg  [%d:0] data      // Data output
);

always_comb
begin
  unique case (address)
""" % (number_of_inputs+counter_bits-1,
       number_of_inputs+counter_bits-2,
       number_of_inputs+counter_bits-3,
       number_of_inputs + counter_bits - 1, counter_bits - 1))
    # now explicitly write all the data:
    for dat in content:
        f.write("\t%d : data = %d'h%x;\n" % (dat[0],
                                             counter_bits,
                                             dat[4]))
    f.write("""
  endcase
end
endmodule
""")
print("Generated irq_rom.sv")
