
# MCOI VFC BACKEND FIRMWARE

This project implements functionality of MCOI in the VFC.

**THIS IS A SUBPROJECT! IN ORDER TO MAKE ANY DEVELOPMENTS, CLONE MCOI_PROJECT GIT REPOSITORY AND UPDATE ALL ITS SUBMODULES**

```
git clone ssh://git@gitlab.cern.ch:7999/mcoi/mcoi_project.git
cd mcoi_project
git submodule update --init --recursive
```

and work from within the mcoi_project environment.


Reason for this is, that some testbenches require both GEFE and VFC parts to be present in the same place. Thus a testbench can instantiate both MCOI and GEFE top modules in the test bench, wire them through harness and verify the behaviour of entire project.

## Submodule-style workflow

Do not forget, that when editing a file inside `mcoi_project/mcoi_vfc_backend_fw`, you're working inside a SUBMODULE. Hence when finish the updates, push the head to the server **and stage and commit as well the new commit number in the mcoi_project**. If you forget to do it, mcoi_project will still point to old revision.
